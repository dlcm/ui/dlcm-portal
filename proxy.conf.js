const proxyConfLocal = require("./proxy.conf.local.js")
const proxyConfVariable = require("./scripts/proxy.conf.variable")

const logLevel = proxyConfLocal.logLevel;

module.exports = {
    ...proxyConfVariable.shortDoi(logLevel),
    ...proxyConfVariable.oauth(logLevel, 16100),
    ...proxyConfVariable.shiblogin(logLevel, 16100),
    ...proxyConfVariable.shibloginMfa(logLevel, 16100),
    ...proxyConfLocal.obj(logLevel),
}
