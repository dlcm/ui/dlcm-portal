
### [2.2.10](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.2.9...dlcm-2.2.10) (2024-07-11)


### Features

* improve user mobile menu & add CTS logo ([5547281](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5547281a2140dd2878916457eba57c12b4fc96ff))
* ULB theme ([8b29ec2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8b29ec2816b5642f5d4e1fafc5bb6229aafd603d))


### Bug Fixes

* **deposit:** avoid form blocked by dua with old metadata ([a566b23](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a566b231e3109cde535a910689ef73c850ed3241))
* **home:** disable the ability to sort search result by publication date ([4df9fd0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4df9fd05509696194732c39c0dfa92f81d1dcc47))

### [2.2.9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.2.8...dlcm-2.2.9) (2024-01-15)


### Bug Fixes

* folder tree component to manage specific case ([0db2c21](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0db2c21afcb24473f9970eb6f210d06501e6e79c))

### [2.2.8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.2.7...dlcm-2.2.8) (2023-12-15)


### Features

* update to solidify 4.8.64 to allow to define short url for docs and oai pmh ([00d0017](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/00d0017c55d0e9a2429cfd9ec0ae697a66acea1f))


### Bug Fixes

* **archive detail:** remove untranslated and duplicate type field ([ca4d357](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ca4d3579e64df83213aaf0faf3688205ba69f9a9))
* **deposit:** [DLCM-2640] display correctly when more than 10 contributors ([bff31b2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/bff31b24ba93e07c036f72778306ce0c9e016c39))
* **order:** redirect to aip downloaded instead of storage aip ([b7ba6de](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b7ba6de68810bc6d2b6d362f9ba199e5992a4cb6))

### [2.2.7](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.2.6...dlcm-2.2.7) (2023-12-08)


### Bug Fixes

* **deposit:** allow to manage more than 10 contributors ([1a02748](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1a02748c98b189c4c559875d84701da5d0442cf4))

### [2.2.6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.2.5...dlcm-2.2.6) (2023-12-01)


### Features

* add button to put in-error a data file ([436ee04](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/436ee042a35ca73914f24b8a531b98c475706355))
* add new status for metadata edition pending ([be7b342](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/be7b342d2780d2a84aaa90eb74d8d3193d592154))
* update to solidify 4.8.58 to allow login MFA ([5c87497](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5c874977eba2bdd44426d63e92c87171806354b2))


### Bug Fixes

* **label:** privacy policy and terms of use depending on theme ([87881d0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/87881d0fcdb77d688fdfd95fa6b4b1316258203f))

### [2.2.5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.2.4...dlcm-2.2.5) (2023-11-24)

### [2.2.4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.2.3...dlcm-2.2.4) (2023-11-22)


### Features

* enable static gzip compression ([f35cacd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f35cacd84ea3541102a186e1cd999170e7bdf45d))
* **ssr:** manage active mq to cache evict when needed ([4483054](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4483054e5f590822b5c3240932cb0e5dccff99b3))
* **ssr:** update to solidify 4.8.50 to add SSR pool thread support ([f25832d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f25832d1d7741d7c609ca79461f470a28131fb5c))
* update nginx config to return static version ([5832e48](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5832e4804e0dfeb1806fab7ef2d8d2e5d923185c))
* use filesystem cache instead of memory cache ([ed9d0ff](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ed9d0ff1efc95652d4dd043f73704b080160bae9))


### Bug Fixes

* **doi:** [DLCM-2636] allow to use separator char on is referenced by other doi in deposit ([22f9be8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/22f9be8bc18e5fe1599d419b5f317e1895bae91e))

### [2.2.3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.2.2...dlcm-2.2.3) (2023-10-09)


### Bug Fixes

* disable initial critical css in SSR ([50ce31f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/50ce31fc447450ad18c27386cdbf25210364517f))

### [2.2.2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.2.1...dlcm-2.2.2) (2023-10-03)


### Bug Fixes

* **admin funding agency:** fix wrong label for org unit field ([1567442](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1567442beaf059c1021256ce902831ac6385090a))
* disable warning in SSR ([2db66cb](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2db66cba9977c33e8dbff5a5d0d0b835a6c12700))

### [2.2.1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.2.0...dlcm-2.2.1) (2023-09-28)


### Features

* add button to fix AIP for ROOT users ([c1b2613](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c1b2613f39078c42af76db674e78b7084457ac75))


### Bug Fixes

* sort apis modules alphabetically ([8fbb2f9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8fbb2f97ca88c0f09aaba8dac2882c6099f40eaa))

## [2.2.0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.1.9...dlcm-2.2.0) (2023-09-11)


### Features

* [DLCM-2587] Separate notification status in two enums ([f0e699c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f0e699ca0db864a248af4236a4f1aec196baba7a))
* [DLCM-2616] do not request signed dua for DUA licence and adapt message for DUA external ([dbf3848](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/dbf384814d1b2d989fc11015a7d80a98e1eeea37))
* **aip:** allow bulk action ([20c648a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/20c648a05e91a51703da10ce18ac9c6ca6ac5c50))
* **archive page:** [DLCM-2622] replace citation expandable pannel by a button opening a dialog ([75539d0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/75539d0ad6d1ae8fa6b72dffc32db48f8e1b8907))
* **doc:** replace apis toc by swagger links ([94c59a5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/94c59a5b293c24726954507b5542f748305edc17))
* **notification:** [DLCM-2607] when treat access dataset request, create ACL in all cases ([7a3c534](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7a3c53462f33e25df5b6c5a9ec0b6574b93c596b))
* **notification:** allow bulk update and allow to change status from admin menu ([6c0115e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6c0115eeedcb9ba811a0bacd27628a3c38e77e90))
* **notification:** allow to change status from list and from admin ([ce97e48](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ce97e48758e5c3688f47bd18a539e6d4e05e25af))
* [DLCM-2345] new modal window for citations on completed deposits ([477722f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/477722f44aff79877c5063b85e52c0d1b64f8a6c))
* [DLCM-2535] uniformize file input ([4c321da](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4c321da1d3cb1b3cafd6255838ef9b9369e09547))
* [DLCM-2569] identify archive or org unit request is pending ([f12767c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f12767cd12b3be245ba91c17109a85e4fb9bb659))
* [DLCM-2583] allow user to subscribe to request dataset access notification ([d434b83](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d434b83186aa4707ceb5d37fa7e4363304e2bafd))
* add correct extension to readme when download ([7c72ee1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7c72ee1a7fe200e742104865c4d7deb24b36ef74))
* add correct extension when download dua and thumbnail files ([de565c6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/de565c6e30c40892039d485fcfb438bf94944385))
* **admin:** add user agreement approvals ([b8cc3e0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b8cc3e0313cb953af69794a7c7d79bc6abfb194a))
* **admin:** allow to sort and filter approved submission agreements ([85e08ea](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/85e08eab6e3767fd86ffe81115fefab8ab9ff94b))
* **AdminOrganizationalUnitSubmissionPolicyState:** wait for old default submission policy to be removed before setting the new default one ([de4f088](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/de4f08888b9f5b5880b87cae5ddc61571b990d1f))
* allow to disable twitter timeline ([4805028](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4805028cdf5372a4b622bb41e92cfd11ad366dd1))
* **archive acl:** allow only admin to edit expiration ([387c2db](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/387c2dbab52de99adc62e86b1c56d462a3c16f13))
* **archive detail:** allow to download and preview readme file ([307286f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/307286f7753a1cd2c14a7f960d175376f08d3d50))
* **deposit:** [DLCM-2527] display submission agreement when needed during deposit submission ([abf1baa](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/abf1baa4488dae6eb097f63c6d85fd602ac4eec2))
* support main storage parameter ([6945ed6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6945ed67977e096bc4fa29d4cd3df6e2bef2b9d4))
* [DLCM-2393] allow to display file structure of restricted or close archive ([88eb22e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/88eb22e30a73f2ecb2773e8fdf4456f43fde17af))
* [DLCM-2434] take dua extension from backend and enable preview of dua files ([6ab9a59](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6ab9a5926885c4d40799dbfd7960711c5d05b57e))
* [DLCM-2446] allow to disable request access button ([7053547](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7053547f8886f41902885b34556d42627b464f94))
* [DLCM-2452] check by role superior to visitor for authorized org units ([bd5da81](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/bd5da81f35f9e2cdbdac8f78907810859b5b7dde))
* [DLCM-2499] update interface for dua policy ([2ae26c4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2ae26c45d913eb1ac52bd3d4daaa8b91806844b0))
* **admin submission policy:** allow to set submission agreement on submission policy ([9e07333](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9e07333364cd1689320b83945a94b14b9e9246bb))
* **admin:** [DLCM-2514] manage submission agreements ([5127b49](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5127b49cb184a474d64dfa68c95b1a28a3d75b76))
* **ark:** [DLCM-2507] display ARK identifier in completed deposits and archives ([099361c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/099361c1e6c521f4768f5b816b5cc5f0c178ef36))
* **deposit:** [DLCM-1271] assign research domains at orgunit level ([410c895](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/410c895ad93bf09631abd70207261783a9a8500a))
* **deposit:** [DLCM-1271] assign research domains at orgunit level ([917bfae](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/917bfae8cd2898639dfa44f0b89810b8732e2418))
* **deposit:** [DLCM-2421] archive master type and archive type in deposit form will appear as additional metadata with a tree component ([79b511e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/79b511e0bf53055e8e91db40272bcd9d567d98b4))
* **deposit:** [DLCM-2445] add toggle button to see my deposits ([7fa139d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7fa139dfd489bf2f86d557e8c4d14bcd36c9a6f1))
* **deposit:** [DLCM-2453] clean error between linked inputs (access, data sensitivity, dua type) when other change ([22c7e67](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/22c7e67f3ebb3c52d9061372845d62e723f2ecd2))
* **deposit:** [DLCM-2495] don't allow creator of deposit to approve is own deposit ([940476c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/940476c19d6e9e9de6599f401ca9346b074eadac))
* manage MFA ([dce3ddb](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/dce3ddbc7eb65693111ce2df2347cfe40ef6652b))
* **preservation space org unit:** [DLCM-2516] allow to define external url to request org unit creation ([28f5b7d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/28f5b7d520f642238b876ed23b922937c3eb6655))
* preserve current route after login ([47bee56](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/47bee5651ff97ebce7b0677a54290d8210960782))
* **search:** [DLCM-2406] New toggle button to search as query or simple text mode ([e70bc1b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e70bc1b3b12bc8ed4478ecd3d5892f19f275185c))
* [DLCM-2405] scheduled task in administration ([e26f381](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e26f381079303c81937a3e58bb753d552a42b60f))
* [DLCM-2407] retrieve all authorized institution with role ([debbf76](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/debbf76673c79b61b5a3fa16f3282374e5b6b7a2))
* [DLCM-2420] add support robot.txt file ([aa8ba0b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/aa8ba0b45ef92c9276781d7e7b543e84f5f0f22e))
* [DLCM-2428] upload signed DUA with ACL request and allow to create from notification directly ([31eead1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/31eead1deb63cc65bb1234a3524db79bf9b085e2))
* **admin funding agency:** allow user manager to see funding agencies in readonly ([850cec6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/850cec6cb4b33b9018319c7da1676d808a624b2b))
* **aip:** [DLCM-2301] archive in status DISPOSABLE should be able to reindex ([611483d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/611483de7ec41773d6f5a6d4c49c912246a58271))
* **archive detail:** [DLCM-2425] upload signed dua in notification ([8bb93ad](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8bb93ad8066863a3748e6930f43aa7c881274449))
* **archive detail:** [DLCM-2427] show DUA ([cd48a96](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/cd48a96c31fa8ac2655630b5f354003a0196efe0))
* be able to change name and isPublic properties from Order ([1b43c22](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1b43c22bad6bc1b0c19cba00f699a79934e4029a))
* **deposit:** [DLCM-2417] include the access level and data sensitivity in pre-submission warning message ([8bf9839](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8bf9839b896e7c762f125b6f62cb1dba9e538185))
* **deposit:** [DLCM-2426] add DUA ([e8402c1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e8402c1cbd770ab74f622ed9d23220950b9066dc))
* **deposit:** add license on deposit submission confirm dialog ([109b535](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/109b535e73b567bd7b75afe176c972ef6d3e08b2))
* **DUA:** [DLCM-2475] add preview and download button everywhere is needed ([b168225](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b168225a9e57bf782438149f987c98a95cabcd42))
* manage multi entries in is referenced by doi input ([16f6d25](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/16f6d25b103d7876d0090764bf84b635a65d8937))
* **preservation space org unit:** [DLCM-2416] members of org units can see other member and their corresponding roles ([8874d2f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8874d2fb8ac0dbe1e427ff662de6d3da8b87bae1))
* **preservation space:** [DLCM-2408] add institution in preservation space ([21a6938](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/21a69386776dd39e6e5e15872dac220c456337e7))
* **SSR:** [DLCM-2450] enable Server Side Rendering on public pages ([6f2c58a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6f2c58aa418eeed6b38add7bfac8e8ea0ed2fe19))
* **ssr:** enable cache for ssr routes ([854067e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/854067e282d1d91e49f7b8f0f33674d9dbe36442))
* **ssr:** enable ssr on archive detail page ([a6b164c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a6b164c6b4dd541a195a23bdffa9f8de67db9a80))
* support resume for preservation errors ([51a8d5b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/51a8d5b947f013603fd505a65a6f1a87020400a8))
* [DLCM-2254] allow to manage entity logo asynchronously ([39faafb](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/39faafb34aa49f201756785ae24cf3a5059980c1))
* [DLCM-2380] allow institution manager to access to admin and to create org unit ([a123623](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a1236237f255dd6b4b981f868f7d40dd29c63203))
* [DLCM-2399] preserve toggle states in local storage ([3ce8c21](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3ce8c2175943e3b6c120bb68f80cf62d04ef97e4))
* add meta infos ([7eb9086](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7eb90867d4a5b0fea0771eff5bdfe668dd2801e9))
* **admin institution:** [DLCM-2380] allow manager of institution to edit institution ([2699954](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/26999548cd3fb729034a651ab188e643bd484008))
* **admin institution:** allow to manage person role ([e99c751](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e99c751bfe09b677b457a5eee93f0d0390a45d6d))
* **admin:** [DLCM-2412] display policy in readonly for user manager of institution ([011e1f2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/011e1f2f546b3a54415b0c56a96286b02460a63d))
* change default value of data tag to BLUE ([1a230d8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1a230d861aa87544d821bedfe993c986cd3b8c2b))
* **deposit detail:** [DLCM-2347] add button to navigate on archive ([22958c1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/22958c1c7dc9bd661d1d8abc9caac78cadbbe40b))
* **deposit logo:** [DLCM-2342] allow to upload logo at creation time and upload logo asynchronously ([95bbef2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/95bbef2b60a436bcc20a5d73f1706522970e03b2))
* **deposit:** [DLCM-2346] make it clear that DOI not work yet ([3e99c6c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3e99c6c5b75dbef7f99f12c2df6f2ec2614d77e5))
* **home detail:** [DLCM-2390] display retention date ([7bddbf8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7bddbf8b67d9a87ad6a1994a075a0fbe164b0470))
* **home search:** [DLCM-2259] allow to sort by title ([1b185fd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1b185fdfd5197e5661877e1ac829fb0661c4fe3e))
* improve display of inherited roles in admin detail and preservation space detail ([8392493](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/83924937c02c5581dcfb25f97e9ca9667fd6f41f))
* **oai-pmh:** use oai pmh link on footer and menus in admin from solidify ([356ec56](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/356ec56e19156d86629257dbe7aab0756e3c6b39))
* **order:** [DLCM-2281] prevent user to remove order of other and manage isPublic flag ([8b61954](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8b61954bbcf801f96fef577fd246df242580148f))
* **org unit detail:** [DLCM-2384] display inherited role ([d52e602](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d52e602584ed0ec88574b3bf5f8f9dead4a74146))
* **preservation space org unit:** display role in detail ([e301540](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e3015407f618d4a84caccb525e8b7b3aaf6421eb))
* **table resource role container:** [DLCM-2381] add pagination ([4df2836](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4df2836adbd3c5d975833f3d95a3338118348589))

### Bug Fixes

* [DLCM-2591] pending request notification on org unit access button is not visible ([914029d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/914029d48a7e201e8cb95bba73181663ed16601e))
* **deposit:** [DLCM-2613] approver must not be able to reject its own deposit ([fa9a8ea](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fa9a8ea43f0cb5f4c0b9cf4ae2aab0e3ccd01be6))
* enable disposal approve ([ec9296f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ec9296f6d53d3e11d5f188e1c65931c074b223ee))
* fix to show dialog when requesting access to a dataset in admin ([3394ffd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3394ffd91838d5955184fc66cce24f0b23538ee6))
* [DLCM-2546] update institution filter to show members of institution as well ([fc9e849](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fc9e849197731a2cbe907e09f640182338362fbd))
* [DLCM-2562] fix routing to access to submission agreements menu ([d0d7ccf](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d0d7ccf9796c8aa840e275d64c398676e821833f))
* [DLCM-2571] change message when requesting an archive access ([7a0086d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7a0086d3fb0a12c21d694d900fb652b603214d9c))
* **admin org unit:** [DLCM-2531] change message for closing or deleting org unit accordingly ([d0b9494](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d0b94948c01562a0a1b02c961fe644f9e4c8cc01))
* **archive acl:** allow to upload file when create acl ([7ffe626](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7ffe626e33552358f7c2604ce7354ee8db35a152))
* **archive detail:** [DLCM-2549] avoid error on redirect after request access on archive with no token ([676546b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/676546b280324f82619038dcea16be725ee3264a))
* **archive detail:** allow to request access without dua file ([b36fe3d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b36fe3d2b3e1dd77579619d855c58eb1c0bc1390))
* **archive request access:** [DLCM-2576] add file extension of dua in request access dialog when download ([c0b15a5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c0b15a51d1eb8cca21f47c7916ba86e5c1dfad1a))
* **compliance level:** display null value like not assessed ([6a36bb8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6a36bb87bcc545e94619bba5c66903bad5d67616))
* define type="button" for all buttons to avoid unexpected behavior with forms ([b3d9bbe](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b3d9bbed90300107b55530766ffef92eb5cc280d))
* **deposit:** [DLCM-2554] allow to upload file when backend is setup only in administrativeDataSupported mode ([5ab89d1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5ab89d1d77e26d5466f17b2645c85f01576a28da))
* **deposit:** allow to preview and download submission agreement on create form ([3a19b52](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3a19b523d5c97ec83c9d07ad24069dcf096efb01))
* **deposit:** allow to preview DUA file ([6319a36](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6319a3652543fb404bb5e4b17e1313e50b670ddb))
* **deposit:** allow to upload thumbnail ([beafd38](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/beafd38ce24f29685acb2dd5cdf2273c63741442))
* **deposit:** avoid message unsaved changes when submit deposit with new dua file ([49d0368](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/49d0368c77d44fba934d44b70728053344d0809b))
* **deposit:** do not lose dua file when edit form ([047b850](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/047b8509dbd9cf60537f27716f051d2be8acb6bc))
* **deposit:** form behavior with dua ([e8ec1e8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e8ec1e8c6c58a0d6f5ba8c65d9af9283cf105cb9))
* **deposit:** remove dua file input by default at creation ([8c7c694](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8c7c694e51fb51551217fb36d4df866c890697a9))
* **doi:** [DLCM-2529] enable short DOI only for DLCM DOI ([58dd6f0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/58dd6f09acfc374959acc93248f450abc5539faf))
* **doi/ark:** change order of default identifier according to system properties ([b5143c9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b5143c999b6c9d9b434f01d5834b247a4e20d3f0))
* **file upload input:** manage correctly required file ([6c5c9f3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6c5c9f3b453ccc83c9ad7937e8232669b6892a1d))
* **home archive:** [DLCM-2542] prevent error dua metadata with dua external ([75c9ce7](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/75c9ce7f2bb649375f492877a85e7bef3a19216d))
* **home archive:** cache problem when logout ([956782e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/956782ed3a8268844f5de118a5d1e58db36e26bb))
* **home search:** translate facets value ([f9eb35f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f9eb35fbc7dd6b7937fde9d53cf7fd7b50e8d26a))
* lint ([5a8c6ce](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5a8c6cea2e4cfc83cf422f90d26b650fc1cb6247))
* lint error ([f365cc4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f365cc4505cf76ce4ebabb04818474030b17b902))
* **preservation space org unit:** [DLCM-2415] avoid scroll up when edit ([27d9d5e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/27d9d5e110d089d072999e4dc0d10808bd6afaa5))
* **preservation space:** change column of institution ([21e3e86](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/21e3e865929272a401ee81e4931a968a569ff59c))
* style of home page in SSR ([3f32447](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3f32447afa774a633241703a3c1705b1ec2bbe06))
* submission agreement to work with news apis signatures ([644823f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/644823f4c08b52ade546983773b141ea3304834f))
* translation for approved submission agreements ([d2c4353](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d2c43535dcd368ab94932cc50ed76f14c08bdc1c))
* unit test ([8ad8d32](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8ad8d32e071296003b206a48bf8b06b125096799))
* [DLCM-2528] user with role visitor in an org unit should see other members ([b8da01a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b8da01ac49e311bbe20fcb2db1da0351f852554a))
* **institution:** [DLCM-2532] allow members without role to see other members with role ([7f4e6b4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7f4e6b401a1b09e352e9af77ed8449c658f9fee9))
* [DLCM-2494] ask confirmation when giving access to archives with external DUA ([dbfaa13](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/dbfaa1312c13976071a2efcf3b878ab52853ab39))
* **archive detail:** avoid error when try to extract missing institution on archive ([7b47537](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7b47537706e68ce2b9c25e4960db9fe0b00c9a91))
* **archive detail:** use aip-total-file-number instead of aip-file-number ([127cc0e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/127cc0e59380749607c1ae71cbffe29cb249f270))
* **assets:** typo ALC => ACL ([a5da010](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a5da010fe5c28a7f7c17ca1dd7962653b2d5c1cb))
* **deposit collection period:** display correct format in hint and input as invalid when needed ([8c41f6e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8c41f6ec9b043066d25a49d4e870d44aa49f3bb0))
* **deposit data file upload:** [DLCM-2402] display a global checksum computing indicator ([50b199f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/50b199f9fbdb26ea93bc498e6693399426e7414a))
* **deposit datafile:** [DLCM-2497] use data category and data type enums from backend on all necessary places ([41babb0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/41babb0959405bbd8680accc95ea3199c5d4b32d))
* increase default duration for warning notification ([6f26f07](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6f26f0710ac4d33ca296f1cf4f3f6686a31e3e20))
* lint ([57fdd86](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/57fdd86e86d40ede9fefecf2dbbfeb859bc40ba7))
* lint error ([97c0939](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/97c0939de195f29ae71f618ff5d5c3800019a5c2))
* rename structure-content-public into content-structure-public ([fd6b3f8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fd6b3f8055e197f9d189bfaf866a6fd7e3a72add))
* **SSR:** avoid error at ssr startup ([bf60acf](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/bf60acfdf9ce9d3270fe36c6007fe6a3fc9ae3e3))
* [DLCM-2409] enable to see all archive acls to admin ([4c943b8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4c943b8e8099e6fe538963f59368dfdda917362c))
* [DLCM-2430] be able to not add user when creating an org unit ([fb66b7e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fb66b7e091f0fa31d1a02e0418e46a45117499ef))
* [DLCM-2469] enable toggle display "my orgunit/ my institution" by default if not set in cookie ([d5bd024](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d5bd024456dff4d8ab876e61246c785a6604c028))
* [DLCM-2473] remove signed from download dua signed label ([37aaf30](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/37aaf306650f27b548077ec7a3ee04368a4ac08f))
* [DLCM-2476] change isDeleted property from archiveacl with inverted boolean value ([4956efe](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4956efe045b02811e7316c0eb771b368351c8aed))
* **admin archive acl:** display is deleted in and allow root to edit expiration ([d5253ef](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d5253ef944391e73a0e23854d378b9c844ea0af7))
* **aip:** [DLCM-2463] update way to provide level param in check fixity api request ([414a85d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/414a85dcdb77bcdd6f0a754dbd7f432912eb1e9a))
* **archive acl:** [DLCM-2483] ADMIN should be able to create, delete or update ACL ([fa46b1b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fa46b1b455bcfb2b06ef7521be8a08033b30d8c0))
* **archive request access:** don't allow to provide dua signed when request access on archive with dua type external ([6c56fcc](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6c56fccc92b95901d9c8c0b5c44faac3fee1b837))
* bootstrap application correctly when run from ssr result ([07a3c13](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/07a3c13e922c46252714b03963f2182a2fa3a5ed))
* checksum enum ([94911ce](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/94911ceff0b0101f40a7974f2fdf27f3dc7aed8b))
* **deposit list:** [DLCM-2432] A visitor of org unit should not be able to create a deposit ([41aa77a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/41aa77a0369056c535f751de5a753f02e5c81877))
* **deposit list:** avoid null point exception error when retrieve org unit ([47a64e5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/47a64e5d0409030268322a64b69c340037387542))
* **deposit:** [DLCM-2389] manage new status update rejected ([e2eb62f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e2eb62f0ecd92b2c2a2feaed2b72879002fab73c))
* **deposit:** avoid infinite spinner on dua file when not ready to be previewed ([8fa7b87](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8fa7b87520d4c9bc1342ee4c6f3ad6ac71ffd4fe))
* **deposit:** hide dua input when metadata version not support dua and display dua icon ([fa00332](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fa003323af52f4c67b8126e1e661629b88a8e578))
* don't send authorization token on short doi endpoint ([1f13e4b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1f13e4ba54ec888e38b68760c34233b629e1fc91))
* enable email variable for proxy configuration ([a61c4bd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a61c4bd198a9202cbb677269c94eeb66a3990ace))
* fix filter by dispositionApproval in preservationPolicy ([1998c50](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1998c501b4bd7e9c45d5ed63023cb2c5f0ade2ea))
* **home archive:** allow to request access without dua file when dua type is external ([4f2809b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4f2809b548aad553853b1d4d12010ae4ee06f010))
* **home archive:** avoid error when request access without dua ([2d1f40a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2d1f40a614bc67875d67ed25fbf3d4ef81ea8d02))
* **home archive:** avoid error when retrieve institution on archive with just one institution ([2e75c54](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2e75c54280b183675692d025474f31f8f70d857d))
* lint ([8750240](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8750240db8d26bc48895387e24890f1bbed4d01f))
* **org unit and institution roles:** [DLCM-2485] update right to display person role in org unit and institution ([9e700c4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9e700c4c16db27f9231977992aa46618e0c7a789))
* **preservation space org unit:** display institution member only if current user is memeber of org unit ([5bfcfdc](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5bfcfdc71505f03fef59121f29ce22e883ae4c36))
* proxy for short doi ([fb2c830](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fb2c83076cdab930a6c5e3852ca221336f99c125))
* remove unused model ([21014e0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/21014e049e4f01feab79d5c25b51ada4eb464448))
* **scheduled task:** [DLCM-2475] change labels in scheduled tasks ([c213483](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c213483c3792bec745e61cb7dbff789c7cb4b1b3))
* short doi with base href ([f22ec40](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f22ec4038d2ceec2363ff7a8070df69aa5d4f6c9))
* **short doi:** remove duplicate slash on short doi url ([5b1e098](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5b1e0989c58d392d6e2addbfa887034a98af5558))
* **ssr:** add transfer state for short doi ([c553422](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c55342279c8993b3805bd4f9ff07909a79e9b1e6))
* **ssr:** avoid transferState error with circular structure on carousel ([99bac7c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/99bac7ca6e4e005ed3b3d7abb813ab5a30fe71b2))
* **ssr:** call directly short doi without proxy in ssr mode ([1cdb884](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1cdb88490e0f64e35b3ff5fc4c2743361d473920))
* **ssr:** remove highlight directive on archive detail in server mode ([e659e1c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e659e1c5a864dbffb42f7bc8df296d76bdfb7fca))
* **table org unit and resource role:** detect changes if add line from detail mode ([e9f83fa](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e9f83faa98754abeb5db0cfcbeba3fcd0705a64e))
* **table org unit and resource role:** propagate value when delete line ([9b109d4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9b109d405e4e7727e7e8affdc4b17d1b9e1af3f9))
* **twitter:** avoid error when change dark mode ([e6bbf41](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e6bbf41dd23233e0264e2b10a53b030deca87899))
* wrong import ([953babc](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/953babcb63b6410b465a0464e13d843385f14951))
* **admin form:** hide link to entity not accessible when user is not admin ([e3d7452](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e3d7452d3df7ae1df4802562e803d6ac3f122a07))
* **admin global banner:** add tooltip on ellipsis on forms errors ([1de4cc1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1de4cc10599b487a0dc8aa75f6c3d38a23ff97e0))
* **admin institution:** [DLCM-2414] avoid error when leave last institution as user ([c4f9602](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c4f9602a2deb7cdfa7883f4bef3de06bc4a26a6a))
* **admin institution:** update allowed org unit and institution when update institution ([cf2dea0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/cf2dea053c815850e9b5ba79f96fc54215b96cd5))
* **admin org unit:** allow to save org unit after edition ([0c26b9d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0c26b9d986dcf77429dfb0b98f18cf069ffd9434))
* **admin org unit:** avoid error when edit default policies ([58a03cd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/58a03cd8d7d1500d93f550882c5ec582255907bd))
* **admin org unit:** display correctly institution for manager of institution ([db63520](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/db6352046b7b269884462e9874571b15cd17969c))
* **admin org unit:** remove insitution restriction for manager of institution ([155e3f7](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/155e3f70d1a1849ba0e046c85f45397ff5bda63d))
* **admin org unit:** update allowed org unit and institution when update org unit ([fe1328b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fe1328b6e4b5bc02d28345515609062866e8101f))
* **admin person:** update allowed org unit and institution when update current person ([d8bd349](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d8bd3490ca4e321be836a1391c201b549ff14e76))
* **admin user:** [DLCM-2317] remove the ability to create user ([c216aac](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c216aaca048c0ebf47cd4b0383e3d9a517ced385))
* **admin:** block creation and deletion actions for user ([2fb6ba0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2fb6ba05b4453cada9483c52ea537b04a4a0626a))
* administrator can see 'preservation planning' menu option ([010d01e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/010d01e974316cfa0cd8d00b2f117910e05edd0b))
* change the way to provide mol viewers ([b53b7b3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b53b7b39270daad893c6b92cfc9e692fc7c338e2))
* define size of tile ([73bb87a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/73bb87aff68e7846d9617100543f227a094b6e4c))
* **deposit detail:** [DLCM-2347] use correct archive id for navigation action into archive detail ([58313b0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/58313b0f2efe07b9ddaaf2ee37be45192409466e))
* **deposit edit:** [DLCM-2328] allow to save after change level of access level on deposit with embargo ([022c89e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/022c89ecae1c65845dbc55f4c3a2eb909e6c69e2))
* **deposit embargo:** remove value of embargo if same that access level and preset if only one value available when check embargo ([1559e78](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1559e7823bf0aab1800bcd605c30d026f1610f6f))
* deps ([f3a5e51](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f3a5e51720c55fa07a8a14f2c5525e85345c7434))
* **file upload:** [DLCM-2325] forbidden characters backslash is doubled ([e5cc041](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e5cc0419a21c93124c4f702bac2f07c4a6eb81a6))
* **history dialog:** provide file name in deposit data file list ([d9e5ff2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d9e5ff2cf949bd5393604e4289dbaf24cad1eb60))
* **home:** [DLCM-2316] retrieve cart item only when in cart detail ([691e8e3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/691e8e38695599a0935d62b8042bd2a85dbed1fa))
* lint ([279faba](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/279faba6d4e6b1594815fa0887ff6645742e5ed3))
* lint error ([3148013](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3148013ca026188ed5a39a1dd092b42ea3ac27f1))
* loading problem that break design of app ([f983544](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f983544d134a0e3f56da0b81d5335fd1e4b17c93))
* localStorage enum value ([6cd776b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6cd776beeb6176b24d0b3da458ba85c8cdedcd26))
* manage correctly concern current user when update institution or org unit ([6bffb3f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6bffb3f942f83777dc73d797fb337872415f3c43))
* manage toc when string empty in config ([33df983](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/33df98360dfb552a5673edb76f122dff120e2aa2))
* missing solidify icons ([e7df929](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e7df929797d1fa5646e9ce8282514fb877a35941))
* **notification:** make request emitter email visible to USERs in notification ([6b63006](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6b6300685cf286163a445ec801dd47504c07aecb))
* **org unit create:** do not sent institution action ([0b470f6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0b470f6102411fee360364df5f0c9ec040dcf4e4))
* **org unit creation:** do not add current user as manager by default for user ([edd8ecd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/edd8ecd425d7b8d7d00ec47517582ac416bf2c85))
* **org unit creation:** do not set current person as manager ([d4bb16d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d4bb16dfc223a9beea7de7dec7420c3311c198f0))
* **org unit preservation space:** allow to leave an org unit for all users ([3bf5c15](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3bf5c15fe9fda010b9d4f56e8163530bb84b1a0a))
* **org unit:** avoir error when retrieve inherited role ([158a45e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/158a45efdc92c9bcb82b224957f65f4e902d64db))
* overwrite existing scripts when copied from solidify ([7457794](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/74577945e443c3d10c3feb819f5b8f0f9a98e663))
* postinstall to work on macOS ([97bb793](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/97bb793a72ff2209cbaf0601082259ae6b89d7b1))
* **preservation planning job:** avoid error when create new job ([5d18b1f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5d18b1f15ee2dab5256e9bd025222aeb049df0a2))
* **preservation space org unit:** [DLCM-2297] allow to leave an org unit ([790272c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/790272c6a5be0468181e14e58278420711baf34e))
* **preservation space org unit:** [DLCM-2400] avoid error when leave org unit ([c842f27](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c842f27de47fe5cfa5447beec14d34b1725b2029))
* **preservation space org unit:** display institution for non manager ([feb184f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/feb184f92460e6cd7545d7e32475b3f98d671a9d))
* prevent carousel to change of tiles when video is playing ([d4ff033](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d4ff0333049d709c84b6ea66a12e67a1bf97f1f7))
* **profile:** [DLCM-2397] prevent error when application role is not provided ([16a27be](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/16a27be3888d41feace54af276bdbd5536f03759))
* rename index search condition 'values' property into 'terms' ([096f7ba](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/096f7bacc0a08903e98fdeb6bc7048c19a403f74))
* update copyright date ([a0526a6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a0526a6a650075b73563de752ca97687b94c5a00))
* update url for institution person ([5990a3d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5990a3d77d4a1b0ae57d33ea65943d5fa295f251))
* upload logo at creation time ([fa5c0d5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fa5c0d5651485dfe8d6c1ff0b2fcc38aca09a173))
* user avatar, use combined guard and fix typing ([60e578e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/60e578e4d3152f18b3fb634daafc73aa70f5b349))
* update to solidify 3.9.0 ([76cd231](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/76cd2310afe414a7da2ea8d2b06335cbd67b052a))

### [2.1.9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.1.8...dlcm-2.1.9) (2022-09-29)


### Bug Fixes

* **admin funding agendy:** prevent double ajax call on org unit list ([bd4645d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/bd4645db1a3d81aab34ebb5de84235d7265910a2))
* **admin org unit detail:** prevent double ajax call on policies, sources and org unit ([aba2944](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/aba2944c773705b3564017a0267b95d56a72fbd7))
* **aip:** prevent double ajax call with one with undefined id on collection table ([efaaf08](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/efaaf0814296630cee65d6496bf013df89af25a7))
* **archive detail:** [DLCM-2324] layout problem on landing page when too mutch keywords ([005de73](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/005de7354be99fce0527f1ce30e3f57ca89bdcb7))
* audit vulnerability ([478ec35](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/478ec358238db3abfa85dc4abbd30e8dbec13620))
* avoid to call several time role endpoint but call just once at startup ([3d51b50](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3d51b5080a1fb24d78e84ae32ba1e8f543786861))
* avoid to send oauth token on toc request ([a4fb2ec](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a4fb2ecf65cf624f967e2bb52f19f1e9e10c3d02))
* **deposit list:** prevent double ajax call on deposit list ([1c0fbd1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1c0fbd15527625c932f4f39cc3ec170eb2679bbd))
* don't set unneeded content-type on getting toc and avoid superfluous preflight requests ([cd4e075](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/cd4e075fe14288df76916500d39beeea78b4e282))
* **home search bar:** allow to focus input when click everywhere on input ([a06dda7](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a06dda7eb4fc8511024eb26c791c67944ce7661a))
* **monitoring:** prevent double ajax call on monitoring and cancel previous call to avoid parallel request ([00f30dc](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/00f30dc5dcddd4a137afdea04d330231930fbf93))
* **my order detail:** prevent double ajax call on order detail ([f2665cc](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f2665cc587c111d5b60b49afdad6b0599f48cadb))
* **notification list:** prevent double ajax call on notification list on startup ([46c6cb9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/46c6cb90d9d05e53fd418e270ced3c1384224666))
* remove useless call to person endpoint on admin user detail page ([f1b8245](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f1b82451362d099f8d817b15a0c4daa7fc85aad6))
* **sip:** prevent double ajax call with one with undefined id on collection table ([cdcbe5e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/cdcbe5ecbcc57ae298c95ec34156dfbcb66a0b31))

### [2.1.8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.1.7...dlcm-2.1.8) (2022-07-26)


### Bug Fixes

* **archive access right:** avoid to call twice time download token ([988fbba](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/988fbbac58360350e257c92d1cff4f2e3c1034ae))
* display preservation planning in user menu only for root ([060260f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/060260ff2072fa31a5863c5ae3439f19d0dbb665))
* make service worker working with dynamic base href ([87e2474](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/87e2474efb2ce0e4cd6d93c08e67716355af6435))
* service worker asset fetching with dynamic base href ([24bf59a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/24bf59a68076b891e659338b84a0a35cab71a616))

### [2.1.7](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.1.6...dlcm-2.1.7) (2022-07-20)


### Bug Fixes

* **folder tree:** folder order when folder shared root name of sibling folders ([aac8482](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/aac848240920fe6d7223d323b9748a0fb70c24ef))

### [2.1.6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.1.5...dlcm-2.1.6) (2022-07-15)


### Bug Fixes

* [DLCM-2308] improve error message when trying to start a disabled job ([351a4ca](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/351a4ca632ae0664b5968619690972c1bcabeb34))
* access right on archive closed on home page and order sections ([889701b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/889701bc1a0b9541253a4ce2a477e94353b81a41))
* avoid admin to manage acl ([eda93cf](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/eda93cf7be93f3e281b992e472649c822d5dc3aa))
* **notification:** [DLCM-2310] redirect to org unit managment view when treat access request on restricted archive ([c6ef14e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c6ef14ecf4f1d8eb17302e8fab8f27f30463a8b8))
* **notification:** use default storage index when treat archive aip in error ([42c5f7f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/42c5f7f4aab9914032af67db9fc09ebf8602e31f))

### [2.1.5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.1.4...dlcm-2.1.5) (2022-06-28)


### Bug Fixes

* **admin org unit:** see archive button to redirect to search with selected org unit ([fd8160b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fd8160b25641fd28ac34dd96f1508be032ec6544))

### [2.1.4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.1.2...dlcm-2.1.4) (2022-06-27)


### Features

* add global banner ([47cb2ad](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/47cb2ad0712af9df04dd2551bc86d6f044d5928c))
* improve maintenance mode ([40b8bf2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/40b8bf2a2b561d7690d6bd85608fc9f2a5f84b71))


### Bug Fixes

* add readme for environment runtime ([c2b032a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c2b032adb0c8b5b7b1a2d0f20cb4661741da7a04))
* **deposit:** avoid error on master type when create deposit ([9239699](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/92396993580fed64ee7b377b208fc76a16be30d3))
* service worker excluded routes ([fce083d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fce083de7972f45cd24646bf1284dfc6fe862212))
* **service worker:** [DLCM-2295] allow to auto determine basehref and use it to register service worker scope ([af8cbe5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/af8cbe50734f8bcde88afe08cf829585e2f0518c))

### [2.1.3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.1.2...dlcm-2.1.3) (2022-06-23)


### Features

* add global banner ([47cb2ad](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/47cb2ad0712af9df04dd2551bc86d6f044d5928c))
* improve maintenance mode ([40b8bf2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/40b8bf2a2b561d7690d6bd85608fc9f2a5f84b71))


### Bug Fixes

* **deposit:** avoid error on master type when create deposit ([9239699](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/92396993580fed64ee7b377b208fc76a16be30d3))
* **service worker:** [DLCM-2295] allow to auto determine basehref and use it to register service worker scope ([af8cbe5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/af8cbe50734f8bcde88afe08cf829585e2f0518c))

### [2.1.2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.1.1...dlcm-2.1.2) (2022-06-17)


### Bug Fixes

* docker entrypoint ([f1ba95e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f1ba95e34c9c04bf301321d296478efc37be2c3a))
* docker entrypoint ([120d47d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/120d47dbb638386a39ad55bebdcb913795118c1e))
* maintenance mode with serveur offline ([75f8de3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/75f8de3a6a2bac58264fee06b0c61251b0cde98a))
* revert commit f5de24583cfe1fab621cc118e81ccf6c8904aa89 on service worker register with scope ([6264b2d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6264b2da8ccd6862599e12f38de76634a63f2904))

### [2.1.1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.1.0...dlcm-2.1.1) (2022-06-14)


### Bug Fixes

* excluse some route from service worker ([c940218](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c94021858bd2022d90f23e3845c935f1f297f10e))
* scroll behavior ([72eb58b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/72eb58b4ad4ff08d6711e17325992c60a15d9578))
* **service worker:** [DLCM-2295] allow to auto determine basehref and use it to register service worker scope ([f5de245](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f5de24583cfe1fab621cc118e81ccf6c8904aa89))

## [2.1.0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.0.7...dlcm-2.1.0) (2022-05-06)


### Features

* **deposit:** allow to select archive type depending of master type ([3c8ec18](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3c8ec184a15a3d0811949296fe573aad5e17863d))
* [DLCM-2277] Collections icons for archive title ([fde0549](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fde05496d7fc46950748fe643ea3a99b23d505b0))
* add put in error action on deposit ([780b9cd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/780b9cdc2600bf64d4a8831adaf1d42dd0a9e7dd))
* display institutions information on person overlay ([38d8f37](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/38d8f373da62cfb4f1849a9bd7b57a92f2b152b8))
* **doi menu:** add navigate to DOI option ([76f3e24](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/76f3e24e5ac373403429412fd071d8180481a1a5))
* **overlay:** [DLCM-2271] add FAIR id logo on overlay ([d8ca8e4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d8ca8e43d948d5168d946ff1add88e7e99f62231))
* display page for release notes and changelog ([31c4586](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/31c4586b5810502b3e2624dad58608e9a49674f4))
* [DLCM-2040] add archive type in deposit form ([cf5ff5e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/cf5ff5e3e077e458759d725f1387c9bff4d5835b))
* [DLCM-2044] display changelog into update dialog ([d048f76](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d048f7616e0006de2af98c30c2cf8e0f1374a280))
* [DLCM-2106] allow navigator to send notification to user ([5b3e8a6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5b3e8a6a3955296ea1052102d6be929821d85ad2))
* [DLCM-2108] avoid version file to be stored in browser cache and add date ([aa98075](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/aa980753accd3313c72cdb8fb69d02f279e48b15))
* [DLCM-2114-2123] display license, institution and funding agency to archide detail ([0fdd9c9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0fdd9c9c543669691c4c756fa05d888b65f4bb42))
* [DLCM-2115] display contributor logo on deposit ([fb0bbe5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fb0bbe509168edeae855bdb23924f4ce1115303e))
* [DLCM-2116] allow to define key type for mapping object ([207598e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/207598e6b22327f615a4a1bb4c0091076a9aeef0))
* [DLCM-2118] create button component to allow navigation between packages ([581b75e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/581b75e35a97c75f52975eb8d51700b688f491b0))
* [DLCM-2125] display url and version of backend modules ([2f33cac](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2f33cacd4640eab4c3005ccf292abf26ea73eb6a))
* [DLCM-2152] Add case for updated metadata in archiving status ([c4e2cb6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c4e2cb6f92bb23dacd8d5bc8ee41cf8adc7c3eb3))
* [DLCM-2165] display delete action on list ([de0f60a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/de0f60ad6c5d0f1367bf2149fb259134881c6f5c))
* [DLCM-2167] display orcid differently if authenticated or not and display on all page when mentioned ([5c76c1e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5c76c1e59aa35903618f819182f87f5720d1f70b))
* [DLCM-2221] support language in HTTP request ([7ed2668](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7ed2668304355f455c2415855eed36b972e1ce74))
* [DLCM-708] diplay data citation on archive detail page ([b9abc91](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b9abc9171bce8fa93c45e3ea532083c153407601))
* add SVG image for ORCID & ROR ([eaeff37](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/eaeff377a15f306a9c9137c728bf558f33303504))
* **admin funding agency & institution:** [DLCM-1880] manage ROR and identifiers ([ad47e18](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ad47e18531faa77bd4f4102167ab0226e634f8d7))
* **admin user:** [DLCM-2235] avoid user to downgrade his role ([31130d4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/31130d428d98a7f9f71ede7060e0ff1dafd501cb))
* **admin:** [DLCM-2039] manage archive type ([f9e8f75](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f9e8f753aea23452318639cf52538a9690e66a3a))
* **admin:** [DLCM-2114] display license logo on deposit and access module ([acd3d66](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/acd3d66d5b6fc9fe31f4d562515be429f9ca2de2))
* **admin:** [DLCM-2127] manage logo for funding agencies ([ac2190f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ac2190f0ad6b5600d2818a6f600a27a94bf62c3a))
* **admin:** display avatar on person ([1f55e1d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1f55e1dc1bbfe70d854a4012c741ae353121cf78))
* **archive detail:** [DLCM-2216] display keywords ([5cb9343](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5cb9343b4fc96bb757dc47df557d2fe1639641e5))
* **archive detail:** [DLCM-2234] display overlay on contributor ([e661471](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e6614714a35eb4dc5ebdb335bd55565b2a0ba661))
* **deposit collection:** [DLCM-2131] display more information on modal that allow to select archive ([cffaafa](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/cffaafa1920352e3921a4af98a6116d206898047))
* **deposit collection:** [DLCM-2168] display by default archive of the orgunit ([e109f7e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e109f7e6127843b1cadb997a3abbfb01aa40cc34))
* **deposit:** [DLCM-2169] add relation type DOI to deposit form ([d772335](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d772335750a8c9d75e45745d0cb0fc00ca6a5d7d))
* **deposit:** [DLCM-2170] allow edition of collection in editing metadata ([9c0133d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9c0133da41622592e3894a5c9873fc787b6749f4))
* display liense overlay on admin org unit ([b901a08](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b901a08df96bf725944ba35a283048928b6d39de))
* **org unit:** [DLCM-2123] display logo of institution and funding agencies ([254a5f4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/254a5f43ad478bcb07cd1fa589c7722c1ce58b71))
* **user:** [DLCM-2053] synchronize ORCID with authorization server ([f73a063](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f73a06314f393d08b532a61afaa2d35b45a6374e))
* [DLCM-1755] support OAI metadata prefixes ([4bff5ac](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4bff5ac440bb3e9ef3f1c9741de18a9b53245b66))
* [DLCM-1827] add more info on archives in collections detail
  table ([378a918](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/378a918446338bb8e44cb6c80c42dd0cda664dcd))
* [DLCM-1985] apply sub action intention ([0d91b94](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0d91b94440a22dfc28faf69b203849a959979b26))
* [DLCM-1999] subscription to different notifications into user
  profile ([5a095d1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5a095d14c8d0ad868d95786a2939d635de6fd5a3))
* [DLCM-2000] check compliance level button for deposit
  menu ([1216bc3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1216bc34b0201b9c69f3a532b5f7d034bc836356))
* [DLCM-2002] manage pluralization in translate ([dd9a02c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/dd9a02ca1297c2aa33eff6fcc9c44d9686579b5b))
* [DLCM-2007] manage cookie consent ([bb928dd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/bb928dd2563d9fe5847ff5145f137bab74732dd1))
* [DLCM-2011] implement elasticsearch apm ([7d2faf8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7d2faf8e01bd34a3b27074a8281bfac164a34d58))
* [DLCM-2018] do not close org unit access request dialog if notification cannot be
  created ([aaa2f78](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/aaa2f782ef7f459087cd6430da13ab27b6f2be99))
* [DLCM-2019] add logo to license ([16168f4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/16168f431dbdabd4f028caa98e35b713c404ee2e))
* [DLCM-2021] clearly indicate default ACL expiration ([e77d7ef](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e77d7efbb3fb300398fefb13a3e5b660dee572a0))
* [DLCM-2022] display in list action for copy id and show
  history ([4be861b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4be861b361905bf85bec6a10083141a83c8b1439))
* [DLCM-2029] admin user form replace person select with searchable
  select ([2947dae](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2947dae90a72efbc83745369102a604e7f7a5712))
* [DLCM-2051] allow metadata update ([77354fc](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/77354fcd09eaf3bad7c218dd3ba7521c67f56ded))
* [DLCM-2059] Adapt preservation job for order purge
  job ([1bb85fb](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1bb85fb313249a896e49135f6337c5e27a7afe4a))
* add 'Centella' theme ([b9b59cf](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b9b59cfacfe12c27b9e753c6d491133e4fc90c2c))
* add check compliance level preservation job ([2e63773](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2e6377347f3e3f7f751f437bde37f5aa2499a0c9))
* **Deposit:** add DELETING status ([59cb6d8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/59cb6d885aaaedd65cf4f26016986fc191ec26f9))
* **IndexFieldAlias:** [DLCM-1672] add system property ([6fc4bd1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6fc4bd1181c6328f55bae6e5d1bd15f593648961))
* [DLCM-1931] integrate oai link into footer ([0eb0940](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0eb094004bbe3c3b449470f185f505d7339c91c0))
* [DLCM-1945] support Solidfy index module ([ae54d2b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ae54d2bcf4e1ccb7cc29badc543f65b6766e31e6))
* [DLCM-1955] allow root to remove sip dip and datafile ([a3d1a1c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a3d1a1c3eb9096e96acc157e05b7e4fba3aa39fd))
* [DLCM-1956] list all orgunits of an institution ([6abbab6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6abbab6ca36f2d992103e74458a4b27889c5ce5a))
* [DLCM-1957] update carousel to have demo feature ([b528ed8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b528ed81ef566aa608bfdb0a55ddcd28df60618b))
* [DLCM-1959] add user full name in change log section ([8ea1e57](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8ea1e570728cdf866b6a8597787fb70a4e7c487b))
* [DLCM-1985] improve sequential and parallel dispatch actions to manage intention and
  timeout ([dec9fba](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/dec9fba251743c5301be9ada486823a22e58c7bd))
* [DLCM-1987] adapt preservation job report for
  deposits ([c3e904e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c3e904e428d4389a84424b2ff124aca40403995f))
* **admin language:** [DLCM-1975] translate list language depending of the language of the
  app ([8467710](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/84677107f784ca022cb56a5f15906376f64a31d3))
* **contributor:** [DLCM-1960] avoid call on contributor avatar when
  missing... ([338ba33](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/338ba33886bc5ca65c9bef0b0ba06940d398b62f))
* **deposit upload:** [DLCM-871] allow to upload datafile with
  checksum ([4f2b7a5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4f2b7a576edf99278a875b9622cbd1f3bebd0614))
* **list logo:** [DLCM-1946-1952] add logo in institution and org unit
  list ([99425f6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/99425f606452a35f330d20759c1c447005c90a5d))
* **preview:** [DLCM-1889] manage preview for tiff file ([81d63f7](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/81d63f7a3ba238cc70e5b9101cb829397c199a42))
* required field in red when empty ([822cbbe](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/822cbbed0e5f490a49bf4c88ac55ac09d43c34a4))
* [AOU-1891] migrate TSLint to ESLint ([74f8e32](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/74f8e3262c4cb73bf941f9679f1701f3e06c7e21))
* [DLCM-1774] migrate to angular 11 ([f3682eb](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f3682eba4573d530c1faa2c37c696c690157a993))
* [DLCM-1774] migrate to angular 12 ([7bb6fc9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7bb6fc9441c2d4acb8777ed3b32e4d2f958d993f))
* [DLCM-1776] migration DLCM to last version of solidify
  1.3.0 ([efbc0b9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/efbc0b9325e94f5cd70760586133ea9173337045))
* [DLCM-1907] remove 'OAuth2Client' management ([7f5f9af](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7f5f9af41fb6b0b1b77a7631afeca59a1771752e))
* [DLCM-1908] enable live reload on solidify frontend ([d7e3b5f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d7e3b5f0e268cccb5c8999b86869496ab1e21a89))
* 1816 add embargo information in collection table ([7670325](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7670325b8cc4cbeaa004b59c86eb81ba768b4e9f))
* add 'refresh' action in user list for ROOT ([b0fdc44](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b0fdc44ae06388853a846def219e1f8b0123e9e0))
* add dialog util everywhere ([5d44d5b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5d44d5be975fc39676fd7590a6d64b8ffd338e85))
* **download:** [DLCM-1743] apply download token for all authenticated
  download ([843429e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/843429e9b1f0f6dcca22ac42346c324ee1c16b46))
* in OrgUnit edition, research domain list is interface language
  dependent ([b722490](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b722490762978164f94461a8d7f415aa0be4ab39))
* update URL for OAI Set end point ([a12369f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a12369f4773f62510e33c5fb854e57fbbc8c0504))


### Bug Fixes

* [DLCM-2290] avoid drag and drop of file and delete folder on deposit not editable ([4e66861](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4e66861be35a078b32b2a51501c5be8380b1edb2))
* [DLCM-2291] reset page index when toggle display only my order, my org unit, maser type ([a7ba563](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a7ba563a5897efe74044be9993125d0850891e35))
* typo duplicated messages in deposit banner ([74dfdea](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/74dfdea2856c2e01eac575596629dd98ad3cc308))
* add transparency on ROR image ([80505f9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/80505f938fe1ebfb4cec9595bfec3604bd4f104a))
* **deposit:** unable to see collection detail ([8bb24db](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8bb24db95355d3da9b1562ae012483a38b33225e))
* disable admin permission to certain actions that are not permitted ([93f970b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/93f970b5758fb5c46bf82fdd15a90618dcfa74ed))
* **doi:** [DLCM-2261] display short DOI only when available ([a23d706](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a23d706c8b5a6e83d3b4084d748d4eaf0704e913))
* label ([7ea6d7f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7ea6d7f1752d3ce7ffb45d7af83dace001506524))
* lint error ([16ed252](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/16ed25218401732a16ba48151d1ef2327168a880))
* template with angularCompilerOptions strictTemplates option ([7c4fde6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7c4fde6768d6df3bb72f9763f96fe67351c74c3b))
* [DLCM-2240] do not allow admins to bypass the security for actions in deposit ([ecacf9b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ecacf9bebeeff721478e7d117e26ec1f33c9b0ae))
* [DLCM-2247] Adapt label of request membership button when being member of org unit ([2711240](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2711240b64af83baf0579f04a1ecccc7e81c2f33))
* **archive detail:** [DLCM-2265] display doi identical to, obsoleted by, referenced by ([dcd58cd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/dcd58cdc0668ab2d137445dd8639f6f1f3c8433b))
* changelog version category type ([b26f507](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b26f5073a63f220b7a1952f49cfff27bea7f88b4))
* **deposit contributor orcid input:** [DLCM-2249] display orcid info and action like other places ([1a6cf37](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1a6cf376bbae0f08b1f4158704fa087954fe6590))
* **deposit:** [DLCM-2244] restore initial order of contributor after cancelling metadata update ([1b43a2c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1b43a2c5b5573de589b766021ca6e31d245467f5))
* **oauth:** [DLCM-2260] clean oauth info in session storage when unable to revoke token ([bf19c78](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/bf19c78ad7add97253fb2d0f46c81fa4e018af60))
* **preservation jobs:** [DLCM-2251] refresh list after init ([49febd4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/49febd464f57ad1464b68a99f3038c69fe927c47))
* translation for job in french ([b1fca67](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b1fca6717dd3b9f7f9879b33dd80f02496b598b6))
* typo in fr translation ([78183ef](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/78183ef546f3470d415b681964505c7ab3d4aa8c))
* [DLCM-2141] Wrong deposit list request for status in_validation ([6294bf0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6294bf024c6afae4278e93fc1e85ef92c089e31a))
* [DLCM-2146] display button on archive detail page ([eccb5df](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/eccb5df7d7859d3794a784dc38f9fc855e488e37))
* [DLCM-2150] Fix error when getting back to the list from preservation policy ([985ef7a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/985ef7a84b98090af7cc88bd32f6f69d5554ac30))
* [DLCM-2153] call super on all OnDestroy to unsubscribe of all subscriptions ([f265e5f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f265e5fb16c5ae096d4ec423d7e59da202a2bcea))
* [DLCM-2164] stay in deposit form when cancelling update of metadata ([6e885fe](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6e885fe923b9dc36f23ac89e3e74f8d73af28a0e))
* [DLCM-2175] Avoid request to changeQueryParams when the resource is not yet setted ([cb8e1c2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/cb8e1c227df8d5f6910fffb38b25021c22f7a9d8))
* [DLCM-2176] prevent npm run start to infinity reload ([a0c2a97](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a0c2a9772922e81fe6bafecfc44c9703af24e9df))
* [DLCM-2183] display dedicated error when bad request without validation errors ([0866f7e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0866f7ecc86865ca0e64a1bb7b7846cfe07ff94d))
* [DLCM-2192] reasearch domain on org unit ([0a85db8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0a85db85b6257f72c1bf4fe1d7953f3de2d02c89))
* [DLCM-2193] fix service worker unable to refresh by updating index.html hash in ngsw after edition in docker entrypoint ([82418fc](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/82418fc58911d742a898a3902b99d500f17a7f7c))
* [DLCM-2193] try to ignore index.html from service worker to avoid mismatch on cache ([a6b6ae1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a6b6ae12bf82d0e842cdc7ec8fc1da9790400cdb))
* [DLCM-2199] scroll strange behavior when open panel on bottom pages ([857c2aa](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/857c2aad6586806d5ac00f2788fb7fe840b51266))
* [DLCM-2200] display tooltip on ellipsis on all single select options and selected value ([96a0853](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/96a0853aa50720ea4f5f3e6d2fee9e26db99c394))
* [DLCM-2202] check orgunitId from url in preservation space for org units detail ([8e5e2f4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8e5e2f4e26eab761f618b6ee7aa92427c2344b24))
* [DLCM-2208] new value not updated when change data category or type on datafile detail ([24cc5f7](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/24cc5f7b708b8e7cd483a0ec1590fe389300148d))
* [DLCM-2209] don't open org unit overlay on some screens ([162ee93](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/162ee93db8d38be9174d4bff0eb785254a186395))
* [DLCM-2212] Enable to create orders with closed archives ([40a532c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/40a532c96cdf61842e7b44c51b8ddf16bb63f15a))
* [DLCM-2218] Manage dataset request going to manage acl creation page ([ba13e3e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ba13e3e3a3bb2f8d8f1063ebf2dec25ceb4c466b))
* [DLCM-2219] orcid button in form generate errors ([8d7c9d8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8d7c9d8ccdbceac29f17fa4da0d938dc004c20e0))
* [DLCM-2225] avoid undefined in native notification when order is ready ([6d2b8a0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6d2b8a037668616fe11ef2a20d70083da7a84982))
* [DLCM-2241] thumbnail size ([e28bb8f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e28bb8f2f9a717054ad17046c1692e994fdeb22b))
* adapt sort and filtering of deposit data file ([8e5bf82](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8e5bf821fc684d877827d1348666b83d5ee0eeea))
* Add possibility to inscribe to notification of org units in profile view ([2ab56d6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2ab56d649b7aae362c994bd795ee9ad6d70a2e18))
* **admin preservation policy:** make retention on duration required and with undefined default value ([d865f09](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d865f094a36f8a33075063e071e72659474d5c83))
* allow to switch to dark mode in mobile toolbar ([078fe68](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/078fe687b96070ba3c3aa744b120baee3c6a8259))
* **archive search:** [DLCM-2185] request access when not connected generate an error ([1de2981](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1de29818d7d92596e7aaf9c0b2af77b5fa107c37))
* build ([42cb8e3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/42cb8e3af917dc081af8c71adf4a6aa379a6f93b))
* color of rejected status ([e695480](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e6954802c664b15415fbf6fdaff456458234f8ae))
* delete fields from datafile that does not exits anymore ([100a0de](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/100a0de3e379acd3800d830d545b27c143f815a3))
* **deposit license:** [DLCM-2114] use overlay to display logo of license ([4e4ee49](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4e4ee499227325aba200d02f9a37340de3c276d4))
* **deposit:** [DLCM-2160] refresh thumbnail after upload ([f5503d1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f5503d1046094e0d8224a75448a1c87d51b0157f))
* **deposit:** [DLCM-2238] prevent to lost of organizational unit in state ([540f6c4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/540f6c4ff0edcb6feeafeb4d15aed226197952fb))
* **deposit:** [DLCM-2244] cancelling metadata update do not reload deposit informations ([200901a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/200901a09eefac774919575844f6a9e919ba37af))
* **deposit:** allow to add an aip to a collection ([cf77f95](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/cf77f952b10fd0ab98439c4289200d00b5640791))
* **deposit:** display confirmation dialog before submit an editing metadata ([b83c8f8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b83c8f8ed980f12d4c5d90fbb34bb20b0381978a))
* dipslay org unit overlay in some case ([922d6d7](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/922d6d72715d1dadad01c3a670fa8c400cece1db))
* disable org unit overlay on deposit ([7a7d93d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7a7d93d59c15280595b805c2576678fd9e42ec24))
* enable to navigate from sip to aip when sip is cleaned ([34dcb3b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/34dcb3b79f9b0ccb7632ddb1990e5e30a211c3f3))
* enable update and cancel of metadata for stewars, manager of org units... ([0c9f02c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0c9f02c4032cdda3a6a0bd03c8258cd776f15657))
* error on package list ([97cd4d4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/97cd4d448250a57035d09cadf9ae623810320b87))
* **file detail:** [DLCM-2211] translate data type and category ([cd9b04b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/cd9b04b797f0299e0199661b1685c1a65e5f6fbd))
* **firefox:** display of user menu icons ([3ddb747](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3ddb747ef69fa475a3efd3a9152b439e4f76dde4))
* **highlightjs:** [DLCM-2184] fix mimetype error and mixed content http and https ([e1f9ee2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e1f9ee2acc36108f204ed389ce22fb17f8e1086a))
* improve package button to allow click outside icon ([995599f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/995599f5dbedecb496631637f9470829c356e629))
* improve performance by disablng monkey patch of zone.js on some event ([77b9bd4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/77b9bd48d12ff897f47036c92a913a2a4b789018))
* improve size of overlay icons ([c7d9f91](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c7d9f9107c47becbfbbbe1db38cd8e97d4a974e0))
* label deposit buttons ([20c2e41](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/20c2e41f03a1028f034e251da08142390cbbac7a))
* lint error ([0417223](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0417223d2658cd55f67129823f69fc9c46244e8e))
* mime type error on highligth js theme ([d3724de](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d3724deb56ff5b9d82d2b0443c40d3a9328e19a2))
* move orcid button component on solidify ([3ae142e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3ae142e7f9955358ecd7c7eefc18d723274fa956))
* multiple request sent to retrieve image on list ([c7ce549](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c7ce54947e0bad047e645c8e70df283492c8e558))
* **notification sent:** [DLCM-2196] detail notification sent not loaded after refresh ([5f94f54](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5f94f543d48e31f35abc3485079a6d90370074ac))
* position of overlay in archive detail ([f2caa23](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f2caa23b23ae5328945bed7243f30ab31663d679))
* redirect after authentification with query param ([8d141bd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8d141bd154348c27d2f7d47cef54bd3da151b59d))
* refresh deposit detail when cancelling metadata editing ([c65b3c0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c65b3c0e29f4e032f1eabe5d509425852977fca1))
* remove cursor pointer when hover contributor avatar ([1e0e796](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1e0e796811a388e03ebd84df71ad2ad31a2f0033))
* script docker entrypoint sh to use absolute path to index.html for compute sha1 ([7a8773a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7a8773ade8a489c0564ce1b1e6ea0ee303ca2e87))
* thumbnail not centered ([f2773e1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f2773e122203cfb7d08da696ab7c309ad22da79a))
* translate new deposit status ([1b50420](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1b5042002ce595d998d43852d3ee3bc4d888b53b))
* typo in fr.json ([9eea63a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9eea63a6a961a1cafe8913b61cae0c7e228cc676))
* **typo:** model in field index alias ([e8d4209](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e8d42097a23e69376b072046f09b7d68ccde8002))
* typos in french translations ([7d7e8d4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7d7e8d4eb1cf1ff1f9cf7bc70baf76db48f21a3d))
* [DLCM-1971] allow to sort by publication date ([33185f6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/33185f681375c972674225045b95f405640209c6))
* [DLCM-2020] display org unit logo even when only my org unit is
  toggle ([b98f98c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b98f98c25f1ee94fc769b4486b0c688608f2137a))
* [DLCM-2025] redirect always when read notification info about
  deposit ([28cc944](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/28cc94433cb3c2737ea5d3deafcf05b708635680))
* [DLCM-2030] preson org unit role detect changes
  problem ([895d145](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/895d145cc744d21130ccefb36c8355403c74a556))
* [DLCM-2033] refactor deposit security layer and fix problem of
  cache ([ab893d6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ab893d603b3e43fb38aeeb01ada3df9cf33998fa))
* [DLCM-2036] do not display notification in person dialog when creating a person from
  deposit ([0714b7b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0714b7b4cdefb388a1cc9998d07fe35f09b45e53))
* [DLCM-2042] check if custom ngsw is needed ([5a9a163](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5a9a163fad25e3232114602f91020b1850b0fed9))
* [DLCM-2075, DLCM-2076] report validation errors in order
  form ([33fc7db](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/33fc7dbe6ebe44512f92671ded181d58db2906c3))
* [DLCM-2077-2081] simply css, replace flexbox with grid and fix file detail
  page ([f8db2c0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f8db2c02a526e619454c353e4ec01129b66c8ced))
* [DLCM-2087] Do not show logo when creating new orgunits, institutions and
  licences ([55d8d11](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/55d8d11188815458f03022fdcd315de7f147b306))
* [DLCM-2088] click ignore on disable DOI input field on
  firefox ([923f5a5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/923f5a5f2c9b64ea655ff7beeb9b8516b4389f86))
* [DLCM-2092] display user avatar on create form ([45d66da](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/45d66da1cb97a153a7ba578336c58baf8eff72d0))
* [DLCM-2095] unable to edit deposit when root and not member of org
  unit ([c3b444f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c3b444fc7c9dfb814b6dc5ce067d34d42b77cf46))
* [DLCM-2096] stop polling an order in error ([b7ad643](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b7ad6432ac8a0a921a9b589721dc2ef704a61882))
* add color for package status inprogress and editing
  metadata ([52a49a4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/52a49a498a7ac0ccbe96d8967df454ddbbf12d71))
* **archive download:** do not display preparation package when download token is
  disabled ([4bdcb02](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4bdcb022e32472347d7033c6ab4fe05bb7cdca6d))
* avoid to switch status editing metadata to in-progress when update
  deposit ([9bc421c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9bc421c78a3edcab05d04a9bbec6df971d0b4603))
* build error by removing unwanted comma ([63038ed](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/63038edeaa56af195a11053c5f23e7d9f9b1d4ff))
* checksum not injected in english error ([75d620a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/75d620a5f73affa36d342e191b26ccc3586c19ba))
* **cookie consent:** [DLCM-2094] add management of download
  token ([f1cfa24](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f1cfa24278b65d8476f2bd9d46bd32a6e5c0aa02))
* deposit edit mode computed to late ([42c38ea](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/42c38eaf3153503c2e7e756cb8b58c007caae5c3))
* **deposit list:** filter status on all tab and remove fitler on other
  tabs ([f10f739](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f10f739ef14dcd88887efa183cf84a8783b17ec3))
* **deposit:** avoid to submit twice when status is editing
  metadata ([36c01ca](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/36c01ca884d506347c036b9acdda8009150675c1))
* do not set the deposit status when do an update ([0fb9d78](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0fb9d78e6d3068c62353605f07e7e82780ce04ec))
* highlightjs when live edit solidify ([0431c88](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0431c886bb4fcd22adbd985271ed17e95aa32ce6))
* **http status:** [DLCM-2008] update enum http status
  code ([4a0146c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4a0146cbd238a46304e1094374338d5ede973efa))
* **job:** [DLCM-2060] job execution list not refreshed after job
  started ([518f029](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/518f0294b6302d167aa85276b0ee96dc1e072cb5))
* lint config and lint error ([3f66e65](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3f66e658a500a573c7abf642c5ea3cb255462518))
* lint error ([0d4b3aa](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0d4b3aadfcf2fe0531be44ae1ec90db50a97c59f))
* lint error ([9bc920f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9bc920f9e805ec21698e29f66d765fe49e2f1d3e))
* manage error durring init application and logging ([d03fba5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d03fba576270e217c782e155e6dc60ce6bade2c1))
* **oai prefix:** make description field mandatory ([6d094c3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6d094c31b7c31d63c319c4a0494ca4ec89266b5d))
* **package.json:** use lowercase projects folders names in scripts (as repositories names in
  Gitlab) ([245f8b0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/245f8b0f5c716bd46a8e30c27c8ffd671cd4d8af))
* **preservation job:** [DLCM-2050] unable to resume job in
  error ([6d68d74](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6d68d7442ea3daaf2ac80ecb6f939af7b71bba11))
* production local script ([20cd5d6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/20cd5d67bd48460b47062b360b05b7cd2cfea7e1))
* regenerate package lock ([ba2325c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ba2325cd55547175147f9c4f57ffff25b34b17dd))
* replace lib for extract third party licenses ([76ade0a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/76ade0a583790932c4f1b7e2bcc72b5209ce76f2))
* solidify live edit ([fa79937](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fa7993746db606a285bb7ae3a0f74c8f352b1c3a))
* solidify live reload ([02367a1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/02367a1faee96672b9bb6664227408e1de13dc44))
* ts config for live watch with solidify ([612b24f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/612b24fe8f82654207d90773cee741299f84e0c7))
* [DLCM-1633] add sort for archive list ([d8ece87](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d8ece877416a5fbbd1ebe708735e32cd683471d6))
* [DLCM-1918] remove routing hash strategy ([39290c0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/39290c06103fe177f024da951e071e73fcd4fffe))
* [DLCM-1973] close user profile dialog when validate without validation
  error ([a535e4c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a535e4c58d9b4361698ee6199555f6fd3dcc2e26))
* [DLCM-1979] create missing success and fail actions for association
  state ([07e76de](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/07e76defce3bb56f4b2d1f0227da4a5a86d81fe1))
* [DLCM-1988] unable to preview image in docx on
  firefox ([aadaa93](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/aadaa93f0f0e9e86e9d18eaebbe1a5ab2a98a6cd))
* [DLCM-1991] custom metadata fie upload fail ([06a8730](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/06a87305412948a8000da5948d3cbddfb712bf64))
* [DLCM-1997] detect changes after checksum computed ([aab56ca](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/aab56ca3abbdc5395c496729208389f9c0e5c3bb))
* 1990 allow delete deposit data file from file detail page and add confirmation on
  list ([d6c581d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d6c581d0c41885e634dba41e70c8b6d2b4bc4798))
* **archive search:** [DLCM-2003] facetted search with multiple values on same field cause
  error ([5b5ba41](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5b5ba414a935239e636f7a1a94d437f28b704e48))
* change indicator and message for checksum ([bb5339a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/bb5339a05d6f23ce70635e13be49e29f8e460ecb))
* default value for scope ([1a964c2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1a964c2f84417141b764afadae522a4a41bbb084))
* disable preview in archive detail file ([7475756](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/74757564ea69ca9f3d5c712f79a8d7bf6412f0dc))
* docx preview patch for better workarround ([0d70859](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0d70859ccc4b748500374d48b1c4d1aa54e353a6))
* docx-preview error missing jszip ([85f462b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/85f462bc2cf3539407adbcaa8e45ca2b90caa6bc))
* hide checksum option if not managed by backend ([4574f7b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4574f7b70a1e58329e8ecd969c585eda4275562c))
* lint error ([0f4ab50](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0f4ab50bcdf9e53465adc57897cc0fff4276b157))
* logo on list ([7f2f405](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7f2f40544a8abbe640de00bee9008058f022c863))
* refresh token ([a212d1e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a212d1ecec72c5b953aa7a2a8cd1db90c46f23e6))
* remove unused imports and added navigate method for org units
  links ([a8c0d6c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a8c0d6c67b90e86f0c3385c3ca309b6134a2cfdc))
* **search:** display error message and clean search result when error
  append ([7cbed37](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7cbed373106ca15f1fa26d45d7d2c33ca0bf6bdf))
* set scope for dlcm ([f4fb9a9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f4fb9a99114d56d77a84a78c559a9614e6b838e4))
* translate new notification type ([0a8d5ae](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0a8d5ae0c53da952ce2efd3726bd5633a47bca08))
* update refresh status polling for all package ([5852142](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/585214249b0d8a076dcee4baf6cc12aa866b7b72))
* use operator and for contributor and allow to edit it in
  settings ([279f5d5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/279f5d55dede03ddeb287c819f21aa75672edbb3))
* [AOU-1898] deposit fle can no longer be selected using the
  checkbox ([29cca12](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/29cca12e0b7941271aa0ec81ecb3046c1b11fa66))
* [AOU-1900] remove environment.runtime.json from source code and
  binary ([19e15d5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/19e15d5d1756e4f39ba068e9db10575620788820))
* [DLCM-1719] refresh ranking global once it has been
  rated ([9751b9f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9751b9fb9d68c7791886fe965e89dc0d75ef9112))
* [dlcm-1840] block if needed download button for closed
  archives ([95d31c0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/95d31c0f40f895678a4219542b3d5aece700c9e2))
* [DLCM-1841] refresh deposit status on detail page properly when submit
  it ([f7e2c9c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f7e2c9c977b4473ef93a5a54172be5dcc81187af))
* [DLCM-1849] update to solidify 0.6.9 to fix missing description files excluded or
  ignored ([eb87274](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/eb8727428dc5e1765d406fadb6f38676f72fc5cd))
* [DLCM-1882] file detail toolbar hide behind deposit detail
  bar ([d84e63d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d84e63de1e9cddc53dadf0ffc2a4fd4703dce36c))
* [DLCM-1901] remove override and overrideProperty to use new typescript override
  feature ([848c162](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/848c16240a5c5c3d4b3a539c297c92ddf1c179b7))
* 1819 remove the upload data button from a collection's folder
  view ([4f626a9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4f626a94289cd6fcb30501f94559d83de698e7e7))
* 1820 close mat menu after click on action in datatable three dot
  button ([6997a48](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6997a488a2789586611205a9c574cb8c443e368c))
* add missing english translate ([72b81b8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/72b81b82d537711114416cb3232cd4bcf8bdd333))
* **admin language:** [DLCM-1902] avoid to allow to create duplicate
  language ([03931fb](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/03931fbfa11bdd920e64e7bbeaff37236ec29e54))
* **admin orgunit:** [DLCM-1927] invalid create request sent when create org
  unit ([43fb222](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/43fb222879ea7d2a42b20f876254a7d1405c8138))
* allow to try to upload same picture twice time ([e31e641](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e31e64148684da4620a18bce44e3b06abbd5d0e6))
* **archive detail:** [DLCM-1915] page in error when log in then
  unloggue ([be7130e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/be7130e9c3e526b6a691badf7a849c82911d1363))
* **archive detail:** 1833 license field display sensitivity when no
  license ([ba06d8b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ba06d8b9bdf555be18734bbfb095958e660b5a83))
* avatar in list and avoid 404 when we know that there is no
  avatar ([ce1b088](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ce1b088d86bd8ddda4c7bf01bdd1d239054b75c4))
* **datatable checkbox:** [DLCM-1820] fix action buttons display in the list of files of a
  deposit ([92b0e38](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/92b0e381b2bc04bc063ac0ba652eb7eb084a16ba))
* **deposit:** [AOU-1835] detect change on deposit data sensitivity when
  error ([9b1c940](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9b1c94065502950b5d27efa21eb50fcbf69b18de))
* **deposit:** 1834 warning banner is not updated when counter
  change ([6cd434e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6cd434e0634a89970e905d187d29e18b980a421a))
* display download button for sip aip dip button only when status
  available ([a3ff473](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a3ff473f7a5605f97560123f34bd1127c4c81853))
* display user email for admin in notification detail ([be15df4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/be15df4c94232afbf9f99348ea38cb1c3487aa82))
* download dip data file cause double download ([6f241a8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6f241a8cac651ef664b54d5d303371bc3d39833c))
* error on alternative dialog ([416738e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/416738e4895f8d3e24a40464477af9a46de3c57a))
* error on order all order form ([4bb06c9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4bb06c9e192a45c6c771945ee62b6dc4389610d2))
* **general:** [DLCM-1831] sometimes app load first time in
  german ([ba66d42](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ba66d42056b0b83db0f988d6ba9e1bdd819aa8cf))
* **home archive:** [DLCM-1829] Packages information are not displayed when should and sometime information displayed concen an other
  archive ([7e35d2d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7e35d2d4512e38972b3b16b49eb0d7404c49ee24))
* **job report:** [DLCM-1923] avoid invalid call on job
  excution ([63a4591](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/63a4591306396f8032d147b4bcb0129e3c79854d))
* **keyword:** [DLCM-1809] deposit keyword should accept
  space ([1ff5f79](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1ff5f79b9f4cafd644af4798e353e996ac672735))
* lint error ([c6eb4dc](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c6eb4dc2a26b207432ac869655cfc1279357fb0d))
* metadata version enum ([37dc30d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/37dc30dd82a44ad64a0cbb06a58788c5b2456b7f))
* missing icon start empty ([71ca786](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/71ca786f4258a596dc6682c27896e5da806dafc5))
* **orgunit:** [DLCM-1885] enter in edition when click on add member
  button ([ea0e4d6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ea0e4d6dbd1f1a7c7c0d2a87c55edf98022e84a8))
* preview with backend update ([91401a6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/91401a6c7de0ce4c233848369c66d6a1d5e3239a))
* **preview:** preview dialog missing download url ([2105c1f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2105c1f9db449624dc41e5e5c98a2e21afe4f6f9))
* proxy conf variable by removing prefix ([8365867](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8365867a04ab08667324316d25a565a52f2bd3f4))
* remove nodocker config in package json ([29ea70b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/29ea70b975e951c639ff2efa7535413a540a5732))
* remove unused tour step ([649e30f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/649e30fe2696eb19d1d55646b146ddd61ad596c9))
* remove warning about IE 11 support ([d08d727](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d08d727513e742eb343fa9491bbba32295628fc7))
* **SharedArchiveState:** [DLCM-1821] Show longer time the info message upon preparation of (first)
  DIP ([0599af5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0599af57616dbfa96d83cdb3e774520c62299d99))
* show labels text in research domain ([03ac686](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/03ac686a3667f6e2fa02d316add23a7cb128a60c))
* update status colors ([719cc29](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/719cc29cd6a137c62e0561cf6a15f860ebab9a7f))
* use download service provided by solidify ([94c8925](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/94c8925a220abe6fa6b4d4c869a56ceff9a684ae))
* warning about budget size ([62ae594](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/62ae594d798548bc64b352238c753a260bfe1c61))

## [2.0.7](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.0.6...dlcm-2.0.7) (2021-06-07)

### Features

* 1816 add embargo information in collection table ([fc230fe](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fc230fe07abd400f7679ea49a079b7256f16f421))
* in OrgUnit edition, research domain list is interface language
  dependent ([2168740](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/21687407f86373d83b5f92d5ccea623c7191ccee))

### Bug Fixes

* [dlcm-1840] block if needed download button for closed
  archives ([fc03222](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fc03222412c777d49f6dfd330d382988b537a00b))
* 1819 remove the upload data button from a collection's folder
  view ([1e92851](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1e9285154eaa6c3bc144e4132cc5f82606d81c6a))
* 1820 close mat menu after click on action in datatable three dot
  button ([44949dc](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/44949dcb147cb23eec99e4c2fed502c137b1f511))
* **archive detail:** 1833 license field display sensitivity when no
  license ([4bbb162](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4bbb162021c3300946bafea1676e0729a886a0ef))
* **datatable checkbox:** [DLCM-1820] fix action buttons display in the list of files of a
  deposit ([56a833a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/56a833a991d0a3ef25b3a2a0e6f5346f1fb724dc))
* **deposit:** [AOU-1835] detect change on deposit data sensitivity when
  error ([eda1478](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/eda14785811b765f151f21194724d9d53d533abb))
* **deposit:** 1834 warning banner is not updated when counter
  change ([8a6df76](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8a6df760e6262080e7bf963d7649afc7232df78f))
* display user email for admin in notification detail ([237c4b6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/237c4b68b382ab2c324869155850be5384d32d34))
* **general:** [DLCM-1831] sometimes app load first time in
  german ([ad8617c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ad8617c47dcaf37c090bacfea9ecdec359c172b4))
* **home archive:** [DLCM-1829] Packages information are not displayed when should and sometime information displayed concen an other
  archive ([008ad6b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/008ad6b167902293f93ff11b1ab15a200cd31ef0))
* **SharedArchiveState:** [DLCM-1821] Show longer time the info message upon preparation of (first)
  DIP ([d89364c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d89364c0a09bbd132e6764c9275273f825712a55))
* show labels text in research domain ([d1708ed](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d1708ed54d0d2137816db2ce0933f8d3b93b8fca))
* update status colors ([a6f2253](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a6f2253e1b4f1fa86e0daf174f5209006730bd9a))
* view AIP link in SIP detail when status is 'CLEANED' ([4c24a53](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4c24a539f5cba2318aa9e4b34bd41ca144a7d513))

## [2.0.6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.0.5...dlcm-2.0.6) (2021-03-18)

### Features

* 1144 when drag and drop file, allow to open folder by staying hover the desired
  folder ([dd0883f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/dd0883f31fa09dbba1f61b056ae20b66553a7bc8))
* 1432 add institution on organizational unit preservation
  space ([ef50116](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ef50116fdd6246dc85e7cd0601d6d4c731471762))
* 1621 translate access level chip and data tags on faceted
  search ([06af279](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/06af279ce92cf895d16dfc4e569c4c1dc9ef0ffb))
* 1744 dynamic change webmanifest and logo and color meta depending of theme
  used ([b165c7e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b165c7ec0e65fe028a69e7ff2904b1b5fd42c329))
* 1749 decode user token ([f34ee1f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f34ee1fda32cf1fff8c2bda5d5b5031e067c369b))
* 1754 parameter to enable a flag headband for test env to inform
  user ([7269290](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/726929089b0eb05883fa45f18343e80d093b1446))
* 1761 display institution and person first name last name if different for user searchable
  select ([eafa39f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/eafa39f336ec386b3be3ed3446c3c68b143420d2))
* 1778 deposit delete button inside deposit form rename into delete
  deposit ([10f5d16](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/10f5d164fc7ea565b86586f82f2a3ea3818495d0))
* 1779 depost file list add column for display data
  category ([6404fae](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6404faeea7439360e076c90063e5087499ad30a2))
* 1781 deposit list add button to download ignored file ([11d132f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/11d132f9812e2bad8b10668093557cce2ca7a899))
* 1783 add icon bottom arrow to the rigth of the solidify
  select ([6b32a69](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6b32a69ff39c4d206767c9dfddc4e992533532d7))
* 1785 data sensitivity change display of infos ([13e73e8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/13e73e8c056f2e16c6020594be45a374f1c5c065))
* 1790 deposit warning banner polling use new endpoint ([95eb25e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/95eb25e814dff3b4f88cfe6047d02abed62847a6))
* 1794 deposit bulk task do not ignore ([4adf23c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4adf23cdb58127bf19d693dbd93e6355df58818f))
* 1795 deposit data allow to directly upload a zip ([3aab69d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3aab69dde95057563a248f388fde9fd2ca7d5ceb))
* 1803 add confirmation window asking user accept privacy policy and terms of use at first
  login ([77bb28d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/77bb28ddaee03ab95f7fd46e9a40685add8c6bad))
* 950 upload by default in current location folder ([6667178](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/66671789a287a1797c1d1dd7debf223227794bbd))
* add callback for extra info image and use it for orcid for contributors in deposit detail
  page ([f228b9f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f228b9f9c8e4baf6be086363d0b8510fea36fe9a))
* add creation time into preservation planning job
  table ([cc6ed6c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/cc6ed6c275913f1550a04b9cc15272b3dbad1ac4))
* add message for contributors deposits to specify that only their authorized deposits are
  shown ([8eb9796](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8eb979601c92077325be5f2d6fcdc2da88c0ec9f))
* add redirect link for orcid in archive detail page ([c69cc78](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c69cc7852ca2c240f68145a9fe09b2446cf500dd))
* allow to put in error sip dip aip ([0d78133](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0d78133f9e79006bbb96c2eb7e8b41538d0b77be))
* display default license on deposit license dropdown ([1268ae8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1268ae876dcaa681d5af9f0540f4607dfb3a552b))
* update angular json ([dae9d3f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/dae9d3f45103ee22ee25d3a2f45e16eede19e740))

### Bug Fixes

* 1411 display button to add a person even if not in edit
  mode ([9dd39e6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9dd39e694048528bde8c465b9719cb6ef10a7463))
* 1753 allow to copy field value without enter in edit mode on
  chrome ([03113a3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/03113a31d2b20249d580554f2dcf5da10421a907))
* 1760 archive acl is not accessible twice and add scroll
  service ([aba3598](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/aba3598d906cb454f4d35b5df58af595f9d3fa09))
* 1764 advance search for users ([f35f7a0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f35f7a03306f84da99fe83cb876b15cd182b057a))
* 1768 display all members of orgunit or all org unit of a person after
  save ([a22f4ba](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a22f4baa692d2a64ee00c290ed01d861c19c761e))
* 1770 remove completed from datafile statuses ([93bc1fe](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/93bc1fe784e9c9de978b9e2cd13ebebfb7c6cba0))
* 1788 sonar duplicate code issues ([bb514b2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/bb514b2cc0699008a5300b34d1cb8a6ee5adc75c))
* 1811 contributor filter and sorting problem ([1a13480](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1a13480ccbda3debc655bea893fb0c0b0f301ab5))
* Add a warning when submitting a deposit which indicates that the decision is potentially
  final ([240ac77](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/240ac7704d6327481b92de4571542a2de1c3696f))
* add missing input in a component ([a1add65](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a1add655eefc7eb6441e1117bdc5735d0f8f7903))
* allow funding agencies menu for admin ([5fc4e7e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5fc4e7eb0def41525171818b790029c68761fc10))
* allow to copy field value when disabled on firefox ([0523e1d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0523e1d54dcea93b74988b909dea4d956303f8fd))
* allow to save oauth 2 form change on admin page ([c976703](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c976703bbd522a2641780307300fcf70574fc3fc))
* change status enum used for aip and sip collection
  list ([54adbf2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/54adbf2f2ee484d71114bf7557c2af0404294fb4))
* data table display problems ([236b0ae](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/236b0ae50b333107cf52cf46b691ac40a191ea76))
* deposit sensitivity should be undefined when creating a
  deposit ([fda3326](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fda3326a274a4fce5586522d0d71b9b108041804))
* do not create empty indexFieldAlias labels ([2d87afd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2d87afd98f9fda75bec558b70b2ee66ded5da6b1))
* error on file and collection list for deposit, sip, dip and
  aip ([5307025](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/53070256fbeb7ac779de06036270c83a14014174))
* force to explicit status enum to use in history
  dialog ([1312e5c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1312e5c10af309566e9bfbaad4c05abc9396f599))
* french corrections ([db115f4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/db115f4f47676e0db77df2a96417eab1ab111ccc))
* initialize each time statistics component ([b22cf7d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b22cf7d61519580260ff963929b68557ff65f113))
* lint error ([9a82446](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9a824465ce089666832b22c55fdc24ddc287ad3f))
* lint error ([deecebc](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/deecebcd27a07b2f6bf3b3d391030a84e1038c05))
* missing import ([e5b365d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e5b365dffd256bc47ad578b07e1100232232d3e9))
* missing spinner on deposit list ([bcadce8](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/bcadce833b985334101590fefc3ec076ca2a96ad))
* open orcid link in a new browser tab ([0870f55](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0870f55e3bcc339d8e0e8f4ba4b26577c1e51fc6))
* preview of gif with pronom id ([9af270d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9af270d91f5636bd4dcc4ef99a82bac3cbf17be9))
* remove environment runtime theme ([f9cf52e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f9cf52eedfaabdf4f1a186454e748662547000d3))
* remove unused import ([ba3a78c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ba3a78ce09e465cdd0cb7c1e5416bccffd271407))
* searchable multiple select field should not display pen when ignored or
  cursor ([e680065](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e6800658d97df6bb08af27deaebfb6f05676b8b9))
* show notification when submitting an order in all orders
  page ([edfb814](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/edfb814fd51a978f7de17d7bc3d13ce1cc6cd0ef))
* show sip or aip depending of the type of job ([a1b18c4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a1b18c4668d23dd60aa07a92574c352636aad706))
* spinner overlap data table header ([66b30a0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/66b30a0bfb34969c8e1289f0ad58fd7d572d488b))
* unable to save deposit after edition due to javascript
  error ([1c46de0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1c46de0e3de5c9da4ef31f7c991e2061bc3571dc))
* use Institution instead of org unit in person institution
  action ([6199491](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/61994913a7acc556e84358f67c22a80498654f12))
* when creating a deposit, licence should be undefined by
  default ([97294a2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/97294a27c138ea1bcac51ec431ed202f242504eb))
* wrong template url on searchable select content ([867fcaf](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/867fcaf9ab21912939e08e57316a475feffd3ea1))

## [2.0.5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.0.4...dlcm-2.0.5) (2020-11-30)

### Features

* add archive preloading jobs ([e489b3a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e489b3ac41ddc2f91c76d31f0d50ed6efc91674d))
* add delete button to downloaded AIP + DIP ([7e432ae](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7e432ae589e34bf3aba06500ede4dabdb06f61be))
* allow to dynamically define app language ([92b2d6e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/92b2d6ed122a781b09532215e399d0ede42f0e82))
* remove '<>{}[]()' from forbidden characters ([587edf6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/587edf68ffca2b1dd701bb12b2c2fb2aac878a8c))

### Bug Fixes

* change order button bulk action on deposit data file ([1fedea2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1fedea26bbf717ecd8a3b2c5b3169f92eea443cf))
* glitch on data table header when no data ([1313f3c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1313f3cbf021637e5ef1a29f0874fe3a31a37826))
* package logo displayed on AIP in error ([9b5d787](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9b5d7870a446c0c48ff116090c32119b4ab679a7))
* preserve query parameters when come from deposit detail
  page ([a0d7d96](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a0d7d960e270d2893238ff181c99953c9e11b2fe))
* remove file action buttons while deposit is in error ([8b3fa80](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8b3fa80aedbb7fddd5ab892ece4987d650b60ab7))
* replace final data by source data for sort and filter on deposit data
  file ([c5d9b6c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c5d9b6c1d52e68336c732c73698fe148b1869627))
* truncate spinner on archive detail file list when file
  loading ([cfe12da](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/cfe12da57bed6e41acf003da562a27d5a0a8e0d9))
* user avatar not refresh when change page on admin user
  list ([6ce4fd1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/6ce4fd11c47fad896a0046ab4c84a24c5118e2c0))
* validate uniqueness acronym of a fundingagency before
  create ([7e54bbc](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7e54bbcc5a55d4ed4bb07994bea780e7de897b65))
* validate uniqueness external_uid of an user before
  create ([a8669d9](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a8669d9f8c84f403f2bde58022c5e29637e42200))

## [2.0.4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.0.3...dlcm-2.0.4) (2020-11-19)

### Features

* add guard to avoid load page archive detail with wrong archive
  id ([695e782](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/695e782fe0c7ccf8fe338aedfa0555c95f93ebcd))
* add yareta manifest and icon for pwa ([fe3b36e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fe3b36e0fad1ccb8b0c9db71778c806098b8ce3c))
* display a dedicated page when archive id doesn't
  exist ([489d14a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/489d14a75fe3b4aaf6bdf6e3fe6cb7ee661739bf))

### Bug Fixes

* aip list sort size ([c03c49d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c03c49d7a0931fb551584de4c85eb7e7097fdf5a))
* avoid infinite spinner when invalide archive id in
  url ([698e8d6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/698e8d645284055c479c56025cc51af569e4b405))
* avoid to block access to doc toc when user guide
  error ([87e580e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/87e580ede810e815f8406d811f9d39d866e15cbf))
* display aip logo for aip downloaded file ([d4773b0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d4773b0a9bf7c5dbed57742a053fe3abe6c8026f))
* error when file format is null on data file detail
  page ([d198b01](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d198b0161a4319cc82e4743f388138d1941a905d))
* increase max size textarea deposit description before
  scroll ([4622fc5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4622fc5ed70dc32ef44a2831fcb860ea25c9ab9b))
* injected link path on toc ([362bc55](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/362bc55920f84b981693d31ff68e50ad3992dbd4))
* regenerate model ([39b1bd0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/39b1bd0185600e53b7b5671378bd2db224f74d8a))

## [2.0.3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.0.2...dlcm-2.0.3) (2020-11-18)

### Bug Fixes

* custom service worker to working when scope is root
  server ([74fc6f0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/74fc6f0c643d5bba020ac76db499eb8502f5298f))
* custom service worker to working when scope is root
  server ([3a16fae](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3a16faec10dc556cb73c7019774e3e598f9a8d8c))

## [2.0.2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.0.1...dlcm-2.0.2) (2020-11-13)

### Bug Fixes

* display see detail person button on preservation space org unit member list only when admin or
  root ([b02c826](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b02c826d23d12cb224942b8f6c91bade034f318b))
* text wrap on datafile detail page ([1d03ef4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1d03ef4b980fea549989400ad2a5a9c2c4a5db30))
* update link of user guide toc if toc load from other endpoint than default docs
  folder ([1349007](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1349007d54c91d05c2d053c857fc9476db5fa54c))

## [2.0.1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.0.0...dlcm-2.0.1) (2020-11-06)

### Features

* add button display more option on data table ([b2ab505](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b2ab505207a0b2af057a72d007ddffd3db65a900))
* add current access level (embargo) in archive
  metadata ([44ab04d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/44ab04da9844d9d43c18a1c7112779c4d65623c9))
* add current access level (embargo) in archive
  metadata ([0ca7ee4](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0ca7ee45e5a41563788ff99f4bb53f0afc4ff9cc))
* add header table as sticky ([68cc757](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/68cc757079b34836c3f80ddecc818b89bff0c05f))
* display embargo information on sip aip dip and display icon on embargo access
  level ([3ccdfce](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3ccdfceed0caa05d94b48bcb49a3284b32a2d458))
* display general validation error message on deposit ([30d550a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/30d550a6bba94381aefb44a61eb29068afeed789))

### Bug Fixes

* add a section for embargo on sip aip dip ([50e386b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/50e386bd9fc2bb8e401544c87e93c90906d4f0fe))
* deposit datafile sticky problem depending of status ([9facd77](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9facd77396c7e251d82b11e179f9cb1be35af622))
* error on deposit embargo at deposit creation ([0e2a117](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/0e2a11763f3f6c44484ef6fbd8c9528d52598fa4))
* glitch on data table when not data and not loading ([2ae3b2b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/2ae3b2bff524f3e7beda8fff55d6f1fd9af3a602))
* header collapsed on datatable when no data are loaded ([bdb08fd](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/bdb08fdece02422d981d56748e73f8d4d8b59c45))
* hide options button on datatable when no options
  availaibe ([8f69c94](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8f69c940b5e100639e81037a404c91f9862e50eb))
* main toolbar on small desktop by hidding search bar ([8cd55a0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8cd55a057a61763daae8efd6a705f0b061ec872a))
* make deposit tabs sticky ([57b3b7b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/57b3b7b7967b9a0e230c3b53c9454bc8c21f9eec))
* monitoring by not cancel a pending request ([f1f9174](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f1f917498487d9f9d2a4efa75e01bb0238bf901b))
* preserve data table line hover state when more action menu is
  opened ([5c5584c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5c5584c00cee1ec3bd89f00ee93c5e0540fb0f7e))
* several problem on sticky data table header ([d8c65a3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/d8c65a3b4acd2458daf95963e243dbf7aeb39e21))
* sticky header list on several screens ([043b275](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/043b275d1c85e0f8834fa67cd001b00d0e4aeb1d))
* use translation of facet from backend for active
  facet ([17a0af3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/17a0af30dd84f01f3a61444f00a22eb288db79ac))

## [2.0.0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.0.0-RC3...dlcm-2.0.0) (2020-11-02)

### Features

* 1728 add a grey background for input fields ([7bf94a3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7bf94a3c82efbbfd0fff4d6f6483c83d34ad1ecf))
* 1730 change logos for add to download orders and download
  orders ([b1c823d](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b1c823daf4854f1caca9d402337d3351d34c5e4a))
* 1733 Use system properties web service to get ORCID client
  ID ([399a613](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/399a613584ba67c6c5a44e58a54c93837709523b))
* add confirmation modal before delete a selection of data
  file ([8eb2b9e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8eb2b9e311f59185a3d88a893d9ec086139fba5c))
* add cypress io test on home module when not logged ([909be10](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/909be10f4f26424be6a0594d7567db18ff7aeca4))
* allow fullscreen preview only for certain formats ([af18a86](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/af18a86b67a59331307dee7609e602860ee491c2))
* allow to customized i18n files depending of theme and also of
  environment ([5ee673b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5ee673bafc47d0b4d75cbfd1f48edd0b62cdfc90))
* allow to set google analytics account ([edc55f6](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/edc55f6aa1925bcd403a55bcfff065bf6efff079))
* always display submit button on deposit but disable it when not
  available ([8a406e3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8a406e373e4ae71d5ba7eee498f30dad537b8694))
* detect link into description to display it ([c5f3a44](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c5f3a44067b3a513ced415ccb08aeefe03ac4143))
* display orcid and affiliation on archive detail
  contributor ([04f7128](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/04f7128f7e32e392d99a4afb822b95d053c6fc47))
* google analytics consent usage of cookie ([c95b70a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c95b70a30447b57997c01a06a7c90856f0a9aabc))
* make labels more explicit + corrections ([c1139b5](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c1139b566f5aa4040f2a34fb2320b0cb9bf71f91))
* remove back button for USERs in download order list ([81ce400](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/81ce40073ec37ff02b86561c0cc8136ad9d30a79))
* rename archive orders + more explicit labels ([7445f11](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7445f11e42b9c4a96f9852e6e143eae9c91440e9))

### Bug Fixes

* 1709 move into the preview the button to allow
  fullscreen ([320d200](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/320d2003066bd806bee0e069fb14e6e7836698a3))
* activate user profile button when there is changes in
  avatar ([e7f70b1](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e7f70b143416b5a1a5092dbd4f0e0a10238a78bc))
* add newspace at the end of the file ([b5b973a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b5b973a29285674755b97975af78da42bedbad06))
* added notion of read/unread for notifications ([93a9693](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/93a9693b2d89aef47c077669f465ba8a2d5a847e))
* allow to configure custom endpoint for userguide ([b13065c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b13065c0cb1be65874d943c8ca2cd6baf09dcd17))
* allow to display cookie consent banner when set id google analytics on environment
  runtime ([fc556e7](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/fc556e7ec2cacfd7d5afe8fc5960262435f0b724))
* change display component table person org unit role ([4ccee3a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4ccee3abaec2b4a86dc346803da427e1cc211bc2))
* change logo for login ([abee97a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/abee97ad26e6e6c7ac6c9a3de84555c76e3c3eac))
* change title in home page depending of theme ([09916fb](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/09916fb1d2f5c38031f81545a3870645fed92aae))
* color label of mat input when focus and color marker required when
  focus ([a77a594](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a77a59441c3ef9afeaa1878a0b0dee2ec5ceee7f))
* disable grid max width for archive detail page ([74ab84a](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/74ab84afa50c4afddd9b49caf962e86f2ca6d214))
* display button for fullscreen preview on a dedicated space and not hover
  preview ([34b4923](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/34b4923ed5373dfefd7ab28e81e8af22b5780bb8))
* display button margin botton on add me as contributor ([c4c0c9f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c4c0c9f32a900dea3a2636582515403c6e14cfd0))
* display input as focused when overlay open on single or multiple
  select ([31dcd40](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/31dcd4047763c0902968514745ef78137b77fe4e))
* display of multiple searchable select without value ([b83bb08](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/b83bb08d1d58ed5ce5e8ba611ecd661320a35342))
* fix height and width after zoom ([29c17d3](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/29c17d3f9fe97531193f7da338d129b922e1f37b))
* label considered only as placeholder ([ae45785](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ae45785d0f0dc851ceb0ba4a31451a8825fdc2b9))
* lint error ([9f23e22](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9f23e222580238f3d56aaae18c0a170226cbac94))
* lint error ([8d16a58](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8d16a58974d9d799feea135019f96943421247b0))
* make label more darker ([ea79e65](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/ea79e654441547a799fff3f7665063001a2a5d9d))
* make some adjustment on notification ([7d1336e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7d1336e7b19da7b759818a5aa3c6b1ce7ec00ba0))
* manage back to list for deposit and preservation-planning deposit
  menus ([3bb920f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3bb920f4f861d39d9c8f53c5119d98b24eab06d7))
* move org unit toolbar to right in deposit view ([f2bc80e](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f2bc80ebb30f22571fc244ac5ffbe7f14e42b696))
* move pen on input editable ([5566b88](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5566b889df0d684e91030ced66f08c1c12fb302e))
* not able to save the same deposit form twice ([69b9925](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/69b9925b1066f6ffb16c538eb75b5f1a1614e509))
* on index field alias move translate field on the center
  part ([7a9c33b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7a9c33be4482d0021d5250e4b41b24a5a7c6cef0))
* preserve-query-parameters-in-url-for-notifications ([bbcdc74](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/bbcdc74820bfd3a9224cede3eb19519c54354edd))
* readme ([e03346f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/e03346fc164555a2c59842ce42a6e8177646a5c4))
* regression on data file screen ([9e31f9c](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/9e31f9c22f8962fb185f27b06030c42c6502c326))
* remove autofocus on deposit list org unit selector ([8482f56](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8482f568badd23b05e3b3db20eed21e68db1f85e))
* rename add to order button on archive detail and add
  tooltip ([39ba010](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/39ba010a00657308921f532c7554f501cd114254))
* rename label home search placeholder in fr and de ([c2f10ba](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/c2f10bada8916161e504c6e8381620dd731df34a))
* research archive with & character ([69db84b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/69db84b1246544fd7fc33f4187e35432b2958904))
* research domain filter field not display correctly on readonly
  mode ([1c1557f](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/1c1557fc1f38de28d300912f2118f1322e15f618))
* searchable multi select link button ([7f63875](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/7f638751e0a1f4f343c3e9f94f2899f1fd434a79))
* switch material input mode to outline and allow to configure
  it ([56d0498](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/56d0498fb2ef26af10a822a982576749809e105d))
* use container space for the preview ([3a8a07b](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/3a8a07b07a707a1385fd17e5cc2389d599be9866))
* use object fit contain property to adapt image ([f613856](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/f6138567d3d26873dbed2cd9f45384f0e17f7d7c))

## 0.0.0 (2019-06-24)

### Features

* **dlcm-portal:** add oauth2 code flow ([1d7ad2e](https://gitlab.unige.ch///commit/1d7ad2e))
* **dlcm-portal:** add refresh token flow ([cad9997](https://gitlab.unige.ch///commit/cad9997)), closes [#DLCM-595](https://gitlab.unige.ch///issues/DLCM-595)

### Tests

* **dlcm-portal:** correct jasmine tests ([17433d3](https://gitlab.unige.ch///commit/17433d3))
