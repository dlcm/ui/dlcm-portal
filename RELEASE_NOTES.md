
### [2.2.10](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/compare/dlcm-2.2.9...dlcm-2.2.10) (2024-07-11)


### Features

* improve user mobile menu & add CTS logo ([5547281](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/5547281a2140dd2878916457eba57c12b4fc96ff))
* ULB theme ([8b29ec2](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/8b29ec2816b5642f5d4e1fafc5bb6229aafd603d))


### Bug Fixes

* **deposit:** avoid form blocked by dua with old metadata ([a566b23](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/a566b231e3109cde535a910689ef73c850ed3241))
* **home:** disable the ability to sort search result by publication date ([4df9fd0](https://gitlab.unige.ch/dlcm/ui/dlcm-portal/commit/4df9fd05509696194732c39c0dfa92f81d1dcc47))
