import {Enums} from "@src/app/enums";
import {TestHelper} from "./test-helper";

describe("Admin Page", () => {
  before(() => {
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.root);
  });
});
