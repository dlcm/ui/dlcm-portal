import {HttpStatusCode} from "@angular/common/http";
import {Enums} from "@src/app/enums";
import {DataTestEnum} from "@src/app/shared/enums/data-test.enum";
import {HttpVerbEnum} from "@src/app/shared/enums/http-verb.enum";
import {ModuleLoadedEnum} from "@src/app/shared/enums/module-loaded.enum";
import {
  Guid,
  isNotNullNorUndefined,
} from "solidify-frontend/src/lib/core-resources";
import {TestHelper} from "./test-helper";

export class OrgunitHelper {
  static orgUnitBaseNamePermanent: string = "[Cypress Permanent Test Data] Org Unit";
  static orgUnitBaseNameTemporary: string = "[Cypress Temporary Test Data] Org Unit";

  private static _generateOrgUnitName(role: Enums.Role.RoleEnum | undefined, forPrerequisiteTest: boolean): string {
    let orgUnitName = forPrerequisiteTest ? this.orgUnitBaseNamePermanent : this.orgUnitBaseNameTemporary;
    let forRoleText = "";
    if (isNotNullNorUndefined(role)) {
      forRoleText = " for " + role;
    } else {
      forRoleText = " for " + "NOBODY";
    }
    orgUnitName = orgUnitName + forRoleText;
    if (!forPrerequisiteTest) {
      orgUnitName = orgUnitName + " " + Guid.MakeNew().ToString().toLowerCase();
    }
    return orgUnitName;
  }

  static createIfNotExistOrgUnitForPrerequisiteTest(role: Enums.Role.RoleEnum | undefined): string {
    const orgUnitName = this._generateOrgUnitName(role, true);
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.root);

    // Go to admin page
    cy.get(".avatar-wrapper").first().click();
    TestHelper.getTestData(DataTestEnum.linkMenuAdmin).click();
    TestHelper.waitModuleLoaded(ModuleLoadedEnum.adminModuleLoaded);

    // Go to org unit list page
    TestHelper.getTestData(DataTestEnum.adminTileOrganizationalUnit).click();
    TestHelper.waitModuleLoaded(ModuleLoadedEnum.adminOrganizationalUnitModuleLoaded);

    TestHelper.getTestData(DataTestEnum.adminOrgUnitListSearchName).type(orgUnitName, {delay: 0, release: false});

    cy.get("solidify-data-table").should("attr", "data-test-info-number-line").then((numberElement: number | any) => {
      if (numberElement === "1") {
        return;
      } else if (numberElement === "0") {
        this.createOrgUnit(role, true);
      } else {
        expect(+numberElement).to.be.lte(1);
      }
    });

    return orgUnitName;
  }

  static createOrgUnit(role: Enums.Role.RoleEnum | undefined, forPrerequisiteTest: boolean = false): string {
    const orgUnitName = this._generateOrgUnitName(role, forPrerequisiteTest);
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.root);

    // Go to admin page
    cy.get(".avatar-wrapper").first().click();
    TestHelper.getTestData(DataTestEnum.linkMenuAdmin).click();
    TestHelper.waitModuleLoaded(ModuleLoadedEnum.adminModuleLoaded);

    // Go to org unit list page
    TestHelper.getTestData(DataTestEnum.adminTileOrganizationalUnit).click();

    // Go to org unit creation page
    TestHelper.waitXhrs([
      "admin/preservation-policies?*",
      "admin/submission-policies?*",
      "admin/dissemination-policies?*",
      "admin/roles?*",
    ], () => TestHelper.getTestData(DataTestEnum.create).click());

    TestHelper.getTestData(DataTestEnum.adminOrgUnitName).type(orgUnitName);

    TestHelper.getTestData(DataTestEnum.adminOrgUnitSubmissionPolicy)
      .click()
      .get("solidify-multi-select-default-value-content li")
      .first()
      .click();

    TestHelper.clickBackdrop();

    TestHelper.getTestData(DataTestEnum.adminOrgUnitPreservationPolicy)
      .click()
      .get("solidify-multi-select-default-value-content li")
      .first()
      .click();

    TestHelper.clickBackdrop();

    if (isNotNullNorUndefined(role)) {
      TestHelper.getTestData(DataTestEnum.sharedPersonOrgUnitRoleButtonAdd).click();

      cy.route(TestHelper.xhrBaseUrl + "admin/people?*")
        .as("personInit");
      cy.route(TestHelper.xhrBaseUrl + "admin/people/search?*")
        .as("personSearch");
      TestHelper.getTestData(DataTestEnum.sharedPersonOrgUnitRoleInputPerson)
        .last()
        .click()
        .wait("@personInit")
        .get("solidify-searchable-single-select-content input")
        .type("USER Last Name")
        .wait("@personSearch")
        .get("solidify-searchable-single-select-content li")
        .first()
        .click();

      TestHelper.getTestData(DataTestEnum.sharedPersonOrgUnitRoleInputRole)
        .last()
        .click();
      TestHelper.getTestData(role as any).click();
    }

    TestHelper.waitXhr("admin/organizational-units/*", () => TestHelper.getTestData(DataTestEnum.save).click(), {
      expectedHttpCode: HttpStatusCode.Ok,
      method: HttpVerbEnum.GET,
    });
    return orgUnitName;
  }
}
