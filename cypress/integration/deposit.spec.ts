import {HttpStatusCode} from "@angular/common/http";
import {Enums} from "@src/app/enums";
import {DataTestEnum} from "@src/app/shared/enums/data-test.enum";
import {HttpVerbEnum} from "@src/app/shared/enums/http-verb.enum";
import {ModuleLoadedEnum} from "@src/app/shared/enums/module-loaded.enum";
import {TestHelper} from "./test-helper";

describe("Deposit Page", () => {
  beforeEach(() => {
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.root);
    TestHelper.waitXhr("preingest/deposits?*", () => {
      cy.wait(2000); // MANDATORY FOR NOT RANDOM ERROR...
      TestHelper.getTestData(DataTestEnum.linkMenuDeposit).click();
    }, {
      expectedHttpCode: undefined,
    });

    TestHelper.waitModuleLoaded(ModuleLoadedEnum.depositModuleLoaded);
  });

  it("visit deposit", () => {
    TestHelper.getTestData(DataTestEnum.depositDataTable).should("exist");
  });

  it("should create a deposit", () => {
    cy.get("#add-deposit-btn").click();
    TestHelper.getTestData(DataTestEnum.depositTitle).type("Deposit Test 1");
    TestHelper.getTestData(DataTestEnum.depositDescription).type("Description of deposit");
    TestHelper.getTestData(DataTestEnum.depositPublicationDate).invoke("val").should("not.be.empty");
    // simulate click event on the drop down
    TestHelper.getTestData(DataTestEnum.depositAccessLevel).first().click().get("mat-option").contains("Public").click();
    TestHelper.getTestData(DataTestEnum.depositAddMeAsAuthor).click();

    /*    TestHelper.getTestData(DataTestEnum.depositLicenseId)
              .click()
              .get("solidify-searchable-single-select-content")
              .click()
              .get("li")
              .contains("CC BY-NC 4.0 (Creative Commons Attribution-NonCommercial 4.0 International)")
              .click();*/

    TestHelper.getTestData(DataTestEnum.depositLanguage).first().click().get("mat-option").contains("en").click();

    cy.server();
    TestHelper.waitXhr("preingest/deposits", () => TestHelper.getTestData(DataTestEnum.save).click(), {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    //click submit button to save
    cy.route("GET", TestHelper.xhrBaseUrl + "preingest/deposits/*/data?*").as("depositData");
    cy.route("GET", TestHelper.xhrBaseUrl + "preingest/deposits/*/aip?*").as("depositAip");

    cy.wait("@depositData").then((xhr) => {
      expect(xhr.status).to.eq(200);
    });
    cy.wait("@depositAip").then((xhr) => {
      expect(xhr.status).to.eq(200);
    });

    //open upload file dialog
    cy.get("#deposit-upload-primary-data").click();

    // fill form
    // TestHelper.getTestData(DataTestEnum.depositDataCategory).first().click().get("mat-option").contains("Primary").click();
    TestHelper.getTestData(DataTestEnum.depositDataType).first().click().get("mat-option").contains("Reference").click();

    // load mock data from a fixture or construct here
    const fileName = "example.json";
    cy.fixture(fileName).then(fileContent => {
      cy.get("#file-upload").upload({fileContent, fileName, mimeType: "application/json"});
    });

    cy.get("#deposit-upload-confirm").click();

    // navigate to upload file tab
    cy.get("#tab-files").click();

    cy.route({
      url: TestHelper.xhrBaseUrl + "data?size=10&page=0&relativeLocation=/",
      method: "GET",
    }).as("listFiles");

    cy.wait("@listFiles").then((xhr) => {
      expect(xhr.status).to.eq(200);
    });

    TestHelper.getTestData(DataTestEnum.depositFileDataTable).then($table => {
      const rowsCount = $table.find("tbody").find("tr").length;
      //expect to have at least one row with data
      expect(rowsCount).to.be.greaterThan(1);
    });

  });

});
