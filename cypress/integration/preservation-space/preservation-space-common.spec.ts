import {HttpStatusCode} from "@angular/common/http";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {HttpVerbEnum} from "@shared/enums/http-verb.enum";
import {TestHelper} from "../test-helper";
import {PreservationSpaceCommonHelper} from "./preservation-space-common.helper";

describe("Preservation Space for Common", () => {
  beforeEach(() => {
    PreservationSpaceCommonHelper.goToPreservationSpaceMenu();
  });

  it("Can see contributor", () => {
    TestHelper.waitXhr("preingest/contributors?*", () => {
      TestHelper.getTestData(DataTestEnum.preservationSpaceTabContributor).click();
    });
  });

  it("Can ask creation of org unit", () => {
    TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitButtonAskCreationOrgUnit).click();
    TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitAskCreationName).type("Org unit test common");
    TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitAskCreationMessage).type("Org unit test message");

    TestHelper.waitXhr("admin/notifications", () => {
      TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitAskCreationSubmit).click();
    }, {
      expectedHttpCode: HttpStatusCode.Created,
      method: HttpVerbEnum.POST,
    });

    TestHelper.waitXhr("admin/notifications/sent?*", () => {
      TestHelper.getTestData(DataTestEnum.preservationSpaceTabRequestSent).click();
    });
  });
});
