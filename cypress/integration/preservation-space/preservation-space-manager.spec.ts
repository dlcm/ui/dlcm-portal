import {Enums} from "@enums";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {HttpVerbEnum} from "@shared/enums/http-verb.enum";
import {OrgunitHelper} from "../orgunit-helper";
import {TestHelper} from "../test-helper";
import {PreservationSpaceCommonHelper} from "./preservation-space-common.helper";
import RoleEnum = Enums.Role.RoleEnum;

describe("Preservation Space for Manager", () => {
  let orgUnitName;

  beforeAll(() => {
    orgUnitName = OrgunitHelper.createIfNotExistOrgUnitForPrerequisiteTest(RoleEnum.MANAGER);
  });

  beforeEach(() => {
    PreservationSpaceCommonHelper.goToPreservationSpaceMenu();
  });

  it("Can see org unit and edit org unit", () => {
    PreservationSpaceCommonHelper.goToOrgUnitDetail(orgUnitName);

    TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitName).click({force: true});
    TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitName).clear().type(orgUnitName);
    TestHelper.waitXhr("admin/organizational-units/*", () => {
      TestHelper.getTestData(DataTestEnum.save).click();
    }, {
      method: HttpVerbEnum.PATCH,
    });
  });

  it("Can see received request", () => {
  });
});
