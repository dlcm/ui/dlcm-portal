import {Enums} from "@enums";
import {OrgunitHelper} from "../orgunit-helper";
import {PreservationSpaceCommonHelper} from "./preservation-space-common.helper";
import RoleEnum = Enums.Role.RoleEnum;

describe("Preservation Space for Approver", () => {
  let orgUnitName;

  beforeAll(() => {
    orgUnitName = OrgunitHelper.createIfNotExistOrgUnitForPrerequisiteTest(RoleEnum.APPROVER);
  });

  beforeEach(() => {
    PreservationSpaceCommonHelper.goToPreservationSpaceMenu();
  });

  it("Can't edit org unit", () => {
    PreservationSpaceCommonHelper.shouldNotEditOrgUnit(orgUnitName);
  });
});
