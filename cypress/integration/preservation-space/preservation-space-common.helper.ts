import {Enums} from "@enums";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {TestHelper} from "../test-helper";

export class PreservationSpaceCommonHelper {
  static goToPreservationSpaceMenu(): void {
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.user);
    TestHelper.waitXhr("organizational-units?*", () => {
      cy.wait(1000); // MANDATORY FOR NOT RANDOM ERROR...
      TestHelper.getTestData(DataTestEnum.linkMenuPreservationSpace).click();
    }, {
      expectedHttpCode: undefined,
    });
    cy.wait(1000); // MANDATORY FOR NOT RANDOM ERROR...
    TestHelper.waitModuleLoaded(ModuleLoadedEnum.preservationSpaceModuleLoaded);
    TestHelper.waitModuleLoaded(ModuleLoadedEnum.preservationSpaceOrganizationalUnitModuleLoaded);
  }

  static goToOrgUnitDetail(orgUnitName: string): void {
    TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitListSearchName).type(orgUnitName, {delay: 0, release: false});
    cy.get("solidify-data-table").should("attr", "data-test-info-number-line").should("eq", "1");

    TestHelper.waitXhrs(["organizational-units/*"],
      () => cy.get("tbody").find("tr").first().click());
  }

  static shouldNotEditOrgUnit(orgUnitName: string): void {
    PreservationSpaceCommonHelper.goToOrgUnitDetail(orgUnitName);
    TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitName).click({force: true});
    TestHelper.getTestData(DataTestEnum.save).should("not.exist");
  }
}
