import {Enums} from "@enums";
import {OrgunitHelper} from "../orgunit-helper";
import {PreservationSpaceCommonHelper} from "./preservation-space-common.helper";
import RoleEnum = Enums.Role.RoleEnum;

describe("Preservation Space for Visitor", () => {
  let orgUnitName;

  beforeAll(() => {
    orgUnitName = OrgunitHelper.createIfNotExistOrgUnitForPrerequisiteTest(RoleEnum.VISITOR);
  });

  beforeEach(() => {
    PreservationSpaceCommonHelper.goToPreservationSpaceMenu();
  });

  it("Can see org unit", () => {
  });

  it("Can't edit org unit", () => {

  });
});
