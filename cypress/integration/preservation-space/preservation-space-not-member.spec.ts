import {DataTestEnum} from "@shared/enums/data-test.enum";
import {HttpVerbEnum} from "@shared/enums/http-verb.enum";
import {OrgunitHelper} from "../orgunit-helper";
import {TestHelper} from "../test-helper";
import {PreservationSpaceCommonHelper} from "./preservation-space-common.helper";

describe("Preservation Space for Not Member", () => {
  let orgUnitName;

  beforeAll(() => {
    orgUnitName = OrgunitHelper.createIfNotExistOrgUnitForPrerequisiteTest(undefined);
  });

  beforeEach(() => {
    PreservationSpaceCommonHelper.goToPreservationSpaceMenu();
  });

  it("Can ask to join org unit", () => {
    PreservationSpaceCommonHelper.goToOrgUnitDetail(orgUnitName);

    TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitButtonAskJoinOrgUnit).click();
    TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitAskJoinRole).first().click().get("mat-option").first().click();
    TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitAskJoinMessage).type("Can I join");
    TestHelper.waitXhr("admin/notifications", () => {
      TestHelper.getTestData(DataTestEnum.preservationSpaceOrgUnitAskJoinSubmit).click();
    }, {
      method: HttpVerbEnum.POST,
    });
  });

  it("Can't edit org unit", () => {
    PreservationSpaceCommonHelper.shouldNotEditOrgUnit(orgUnitName);
  });
});
