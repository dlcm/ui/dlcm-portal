/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit.cy.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DataTestEnum} from "@app/shared/enums/data-test.enum";
import {Enums} from "@enums";
import {HttpStatusCode} from "../../enums/http-status-code.enum";
import {TestHelper} from "../../support/test-helper";

describe("Deposit Page", () => {
  beforeEach(() => {
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.root);
    cy.visit("/deposit");
    // TestHelper.waitXhr("preingest/deposits?*", () => {
    //   cy.wait(2000); // MANDATORY FOR NOT RANDOM ERROR...
    //   TestHelper.getTestData(DataTestEnum.linkMenuDeposit).click();
    // }, {
    //   expectedHttpCode: undefined,
    // });

    // TestHelper.waitModuleLoaded(ModuleLoadedEnum.depositModuleLoaded);
  });

  xit("visit deposit", () => {
    TestHelper.getTestData(DataTestEnum.depositDataTable).should("exist");
  });

  xit("should create a deposit", () => {
    cy.get("#add-deposit-btn").click();
    TestHelper.getTestData(DataTestEnum.depositTitle).type("Deposit Test 1");
    TestHelper.getTestData(DataTestEnum.depositDescription).type("Description of deposit");
    TestHelper.getTestData(DataTestEnum.depositAddMeAsAuthor).click();
    TestHelper.getTestData(DataTestEnum.depositPublicationDate).invoke("val").should("not.be.empty");
    TestHelper.getTestData(DataTestEnum.depositAccessLevel).click().get("mat-option").contains("Public").click();

    TestHelper.getTestData(DataTestEnum.depositLicenseId)
      .click()
      .get("solidify-searchable-single-select-content li")
      .first()
      .click();

    TestHelper.getTestData(DataTestEnum.depositLanguage).first().click().get("mat-option").contains("en").click();
    TestHelper.getTestData(DataTestEnum.depositSave).should("not.be.disabled").click();
    // TestHelper.waitXhr("*/preingest/deposits", () => , {
    //   method: HttpVerbEnum.POST,
    //   expectedHttpCode: HttpStatusCode.Created,
    // });

    //click submit button to save
    cy.intercept("GET", "*/preingest/deposits/*/data?*").as("depositData");
    cy.intercept("GET", "*/preingest/deposits/*/aip?*").as("depositAip");

    cy.wait("@depositData").then((xhr) => {
      expect(xhr.response.statusCode).to.eq(HttpStatusCode.Ok);
    });
    cy.wait("@depositAip").then((xhr) => {
      expect(xhr.response.statusCode).to.eq(HttpStatusCode.Ok);
    });

    //open upload file dialog
    cy.get("#deposit-upload-primary-data").click();

    // fill form
    // TestHelper.getTestData(DataTestEnum.depositDataCategory).first().click().get("mat-option").contains("Primary").click();
    TestHelper.getTestData(DataTestEnum.depositDataType).first().click().get("mat-option").contains("Reference").click();

    // load mock data from a fixture or construct here
    const fileName = "example.json";
    cy.fixture(fileName).then(fileContent => {
      // TODO FIX
      // cy.get("#file-upload").upload({fileContent, fileName, mimeType: "application/json"});
    });

    cy.get("#deposit-upload-confirm").click();

    // navigate to upload file tab
    cy.get("#tab-files").click();

    cy.intercept({
      url: "*/data?size=10&page=0&relativeLocation=/",
      method: "GET",
    }).as("listFiles");

    cy.wait("@listFiles").then((xhr) => {
      expect(xhr.response.statusCode).to.eq(HttpStatusCode.Ok);
    });

    TestHelper.getTestData(DataTestEnum.depositFileDataTable).then($table => {
      const rowsCount = $table.find("tbody").find("tr").length;
      //expect to have at least one row with data
      expect(rowsCount).to.be.greaterThan(1);
    });

  });

});
