/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-unlogged.cy.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {HttpVerbEnum} from "@shared/enums/http-verb.enum";
import {HttpStatusCode} from "../../enums/http-status-code.enum";
import {TestHelper} from "../../support/test-helper";

describe("Home Page", () => {
  beforeEach(() => {
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.guest);
    cy.visit("/");
  });

  it("check the title", () => {
    cy.title().should("eq", "DLCM");
  });

  it("check that login is visible", () => {
    TestHelper.getTestData(DataTestEnum.loginHorizontalInput).should("be.visible");
  });

  it("check that search of public archive is working", () => {
    // cy.wait(100);
    // cy.server();
    cy.intercept({method: "POST", url: "*/access/metadata/search?size=10&page=0"}).as("search");

    TestHelper.getTestData(DataTestEnum.homeSearchInput).type("dlcm");

    cy.wait("@search").its("response.statusCode").should("eq", 200);

    // TestHelper.getTestData(DataTestEnum.homeButtonViewList).click();

    TestHelper.waitXhr("*/access/metadata/search?size=*&page=0", () => TestHelper.getTestData(DataTestEnum.homeButtonViewList).click(), {
      method: HttpVerbEnum.POST,
      expectedHttpCode: HttpStatusCode.Ok,
    });

    TestHelper.getTestData(DataTestEnum.homeDataTableSearch).should("exist");

    // cy.get(TestHelper.testData(DataTestEnum.homeFacet + "-access-levels-PUBLIC") + " [type=\"checkbox\"]").not("[disabled]")
    //   .click({force: true}).should("be.checked");

    TestHelper.getTestData(DataTestEnum.homeDataTableSearch).find("tbody").find("tr").as("list");
    cy.get("@list").first().click();
    // TestHelper.getTestData(DataTestEnum.homeDataTableSearch).then($table => {
    //   const list = $table.find("tbody").find("tr");
    //
    //   list.first().click();

    // TestHelper.waitXhr("access/metadata/*", () => list[0].click(), {
    //   method: HttpVerbEnum.GET,
    //   expectedHttpCode: HttpStatusCode.Ok,
    // });
    // });

    TestHelper.getTestData(DataTestEnum.homeArchiveDetailDownloadButton).should("exist");

    TestHelper.getTestData(DataTestEnum.homeArchiveDetailLinkFilesOrCollections).should("exist");
    //     .click({force: true}).should("be.checked");
    // cy.request("POST", TestHelper.xhrBaseUrl + "access/metadata/search?size=*&page=0&query=dlcm").should((response) => {
    //   TestHelper.getTestData(DataTestEnum.homeSearchInput).type("dlcm");
    // });

    // TestHelper.waitXhr("access/metadata/search?size=*&page=0&query=dlcm", () => TestHelper.getTestData(DataTestEnum.homeSearchInput)
    //   .type("dlcm"), {
    //   method: HttpVerbEnum.POST,
    //   expectedHttpCode: HttpStatusCode.Ok,
    // });

    // TestHelper.waitXhr("access/metadata/search?size=*&page=0&query=dlcm", () => TestHelper.getTestData(DataTestEnum.homeButtonViewList).click(), {
    //   method: HttpVerbEnum.POST,
    //   expectedHttpCode: HttpStatusCode.Ok,
    // });
    //
    // TestHelper.getTestData(DataTestEnum.homeDataTableSearch).should("exist");
    //
    // TestHelper.waitXhr("access/metadata/search?size=*&page=0*", () => {
    //   cy.get(TestHelper.testData(DataTestEnum.homeFacet + "-access-levels-PUBLIC") + " [type=\"checkbox\"]").not("[disabled]")
    //     .click({force: true}).should("be.checked");
    // }, {
    //   method: HttpVerbEnum.POST,
    //   expectedHttpCode: HttpStatusCode.Ok,
    // });
    //
    // TestHelper.getTestData(DataTestEnum.homeDataTableSearch).then($table => {
    //   const list = $table.find("tbody").find("tr");
    //   const rowsCount = list.length;
    //   //expect to have at least one row with data
    //   expect(rowsCount).to.be.greaterThan(1);
    //
    //   TestHelper.waitXhr("access/metadata/*", () => list[0].click(), {
    //     method: HttpVerbEnum.GET,
    //     expectedHttpCode: HttpStatusCode.Ok,
    //   });
    // });
    //
    // TestHelper.getTestData(DataTestEnum.homeArchiveDetailDownloadButton).should("exist");
    // TestHelper.getTestData(DataTestEnum.homeArchiveDetailLinkFilesOrCollections).should("exist");
  });
});
