/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin.cy.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DataTestEnum} from "@app/shared/enums/data-test.enum";
import {Enums} from "@enums";
import {TestHelper} from "../../support/test-helper";

describe("Admin Page", () => {
  beforeEach(() => {
    TestHelper.login(Enums.UserApplicationRole.UserApplicationRoleEnum.admin);
  });

  xit("navigate to admin page", () => {
    cy.visit("/");
    TestHelper.getTestData(DataTestEnum.linkUserMenu).click();
    TestHelper.getTestData(DataTestEnum.linkMenuAdmin).click();
    cy.location("pathname").should("eq", "/admin");
  });

  xit("visit admin page", () => {
    cy.visit("/admin");
    cy.get("#admin-home-title").contains("Administration");
    cy.get("mat-card").should("have.length", 10);
  });
});
