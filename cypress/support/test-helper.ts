/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - test-helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {HttpVerbEnum} from "@shared/enums/http-verb.enum";
import {Interception} from "cypress/types/net-stubbing";
import {isNotNullNorUndefined, isNullOrUndefined} from "solidify-frontend/src/lib/core-resources";
import {cypressEnvironment} from "../environments/cypress-environment";
// import {Interception} from "cypress/types/net-stubbing";
// import {
//   isNotNullNorUndefined,
//   isNullOrUndefined,
// } from "solidify-frontend/src/lib/core-resources";
import {HttpStatusCode} from "../enums/http-status-code.enum";
import Chainable = Cypress.Chainable;

// import Chainable = Cypress.Chainable;

export class TestHelper {
  static testData(dataTest: DataTestEnum | string): string {
    return `[data-test=${dataTest}]`;
  }

  static getTestData(dataTest: DataTestEnum): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.get(this.testData(dataTest));
  }

  static login(applicationRole: Enums.UserApplicationRole.UserApplicationRoleEnum): void {
    let token: string | undefined = undefined;
    if (!cypressEnvironment.localAuth) {
      switch (applicationRole) {
        case Enums.UserApplicationRole.UserApplicationRoleEnum.root:
          token = cypressEnvironment.tokenRoot;
          break;
        case Enums.UserApplicationRole.UserApplicationRoleEnum.admin:
          token = cypressEnvironment.tokenAdmin;
          break;
        case Enums.UserApplicationRole.UserApplicationRoleEnum.user:
          token = cypressEnvironment.tokenUser;
          break;
        case Enums.UserApplicationRole.UserApplicationRoleEnum.guest:
        default:
          this.logout();
          return;
      }
      if (isNotNullNorUndefined(token)) {
        sessionStorage.setItem("access_token", token as string);
      }
    }
  }

  static logout(): void {
    sessionStorage.removeItem("access_token");
    sessionStorage.removeItem("refresh_token");
  }

  static waitXhrs(listUrls: string[], triggerCallback: () => any | undefined): Cypress.Chainable<Interception[]> {
    const listAlias: string[] = [];
    const baseNameAliasXhr = "xhr";
    listUrls.forEach((url, index) => {
      const nameAliasXhr = baseNameAliasXhr + index;
      cy.intercept(url).as(nameAliasXhr);
      listAlias.push("@" + nameAliasXhr);
    });
    if (isNotNullNorUndefined(triggerCallback)) {
      triggerCallback();
    }
    return cy.wait(listAlias);
  }

  static waitXhr(url: string, triggerCallback: () => any | undefined, opts?: WaitXhrOptions): Chainable<Interception> {
    let options = new WaitXhrOptions();
    if (isNotNullNorUndefined(opts)) {
      options = opts as WaitXhrOptions;
    }
    if (isNullOrUndefined(options.method)) {
      cy.intercept(url).as("xhr");
    } else {
      cy.intercept(options.method, url).as("xhr");
    }
    if (isNotNullNorUndefined(triggerCallback)) {
      triggerCallback();
    }
    return cy.wait("@xhr").then((xhr) => {
      if (isNotNullNorUndefined(options.expectedHttpCode)) {
        expect(xhr.response.statusCode).to.eq(options.expectedHttpCode);

        expect(xhr.response.statusCode).eq(200);
        // cy.wait("@search").its("response.statusCode").should("eq", 200);

      }
    });
  }

  static clickBackdrop(): void {
    cy.get(".cdk-overlay-backdrop").click();
  }
}

export class WaitXhrOptions {
  method?: HttpVerbEnum = HttpVerbEnum.GET;
  expectedHttpCode?: number = HttpStatusCode.Ok;
}
