#!/bin/sh
# Exit the script if any command fails
set -e

if [ "$1" = 'start' ]; then

  NGINX_FOLDER="/usr/share/nginx"
  ENV_FOLDER="$NGINX_FOLDER/environments"
  BROWSER_FOLDER="$NGINX_FOLDER/html"
  SERVER_FOLDER="$NGINX_FOLDER/server"

  if [ "$WEBCONTEXT" = '/' ]; then
      BROWSER_ENV_CONF_FOLDER="$BROWSER_FOLDER/assets/configurations"
  else
      mkdir -p /tmp/"$WEBCONTEXT"/
      mv -v $BROWSER_FOLDER/* /tmp/"$WEBCONTEXT"/
      rm -rf $BROWSER_FOLDER/*
      mkdir -p $BROWSER_FOLDER/"$WEBCONTEXT"/
      mv -v /tmp/"$WEBCONTEXT"/* $BROWSER_FOLDER/"$WEBCONTEXT"/
      rm -rf /tmp/"$WEBCONTEXT"
      BROWSER_ENV_CONF_FOLDER="$BROWSER_FOLDER/$WEBCONTEXT/assets/configurations"
  fi

  mkdir -p "$BROWSER_ENV_CONF_FOLDER"

  BROWSER_ENV_RUNTIME_SOURCE="$ENV_FOLDER/environment.runtime.json"
  cp $BROWSER_ENV_RUNTIME_SOURCE "$BROWSER_ENV_CONF_FOLDER/environment.runtime.json"

  # Inject cleanCacheEndpointsBasicAuthPassword in server-environment config and copy to the server folder
  SERVER_ENV_RUNTIME_SOURCE="$ENV_FOLDER/server-environment.runtime.json"
  if [ -e $SERVER_ENV_RUNTIME_SOURCE ]; then
    jq '.cleanCacheEndpointsBasicAuthPassword = $value' --arg value "$SERVER_CLEAN_CACHE_PASSWORD" $SERVER_ENV_RUNTIME_SOURCE >"$ENV_FOLDER/server-tmp.$$.json" \
    && cp "$ENV_FOLDER/server-tmp.$$.json" "$SERVER_FOLDER/server-environment.runtime.json" \
    && rm -rf "$ENV_FOLDER/server-tmp.$$.json"
  fi

  # Inject BASE_HREF to index.html <base href/> tag
  chmod a+x $BROWSER_FOLDER/patch-base-href.sh
  $BROWSER_FOLDER/patch-base-href.sh $BROWSER_FOLDER $BASE_HREF

  if [ "$SSR_ENABLED" = 'enabled' ]; then
    # start nodejs
    forever -a -l $LOG_FILE start /usr/share/nginx/server/main.js
  fi

  # Start http server
  exec nginx -g 'daemon off;'
fi

# Used to run a custom command on container start
exec "$@"
