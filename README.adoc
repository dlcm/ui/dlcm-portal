= DLCM Portal

image::DLCM-logo.png[DLCM Technology,100]

This angular project was generated with https://github.com/angular/angular-cli[Angular CLI] version 7.3.8.

== Prerequisites

* Have pnpm installed (use version mentioned in 'packageManager' field in package.json).
pnpm will automatically download node needed for the project.
* Name the folder who host current sources `dlcm-portal`
* Install https://angular.io/cli[Angular CLI] with command `pnpm install -g angular/cli@X.Y.Z (use version mentioned in '@angular/core' field in package.json)
* Install https://www.typescriptlang.org/[typescript] with command `pnpm install -g typescript@X.Y.Z` (use version mentioned in 'typescript' field in package.json)
* For user of Jetbrains IDE (IntelliJ, WebStorm), go to `File > Settings > Appearance & Behavior > System Settings` and disable the option `Use "safe write" (save changes to a temporary file first)`.
* Download is secured by Cookie.
To allow downloading file in dev env, add the following config on dlcm backend `solidify.security.downloadToken.cookie.secure=false` to prevent generation of cookie with flag secure.
Indeed, this flag avoid to use cookie in localhost.
* You should access to https://nexus.unige.ch/, if not please follow instructions in <<install_outside_unige>>

== Install [[install]]

For run the project on a dev server or build the project, you need to install all dependencies with the command

=== Install inside UNIGE Network

----
pnpm install
----

=== Install outside UNIGE Network [[install_outside_unige]]

In case you are not allowed to access to https://nexus.unige.ch/, you need to build 3 dependencies locally in the following order:

. eslint-plugin-solidify (https://gitlab.unige.ch/solidify/solidify-eslint-plugin)
.. Pull the source code of the lib `eslint-plugin-solidify` *with the corresponding tag* used by `dlcm-portal` (see dlcm-portal: package.json > devDependencies > eslint-plugin-solidify)
.. Delete on `solidify-eslint-plugin` the `package-lock.json` file
.. Run `npm install` on `solidify-eslint-plugin`
.. Run `npm run build` on `solidify-eslint-plugin`, the binary should by generated in `dist folder
.. Copy the content of `solidify-eslint-plugin/dist` folder inside `dlcm-portal/local_modules/eslint-plugin-solidify` (create the directories)
.. On `package.json` from `dlcm-portal`, change the version number of `devDependencies` for `eslint-plugin-solidify` by `file:./local_modules/eslint-plugin-solidify`)

. solidify-frontend (https://gitlab.unige.ch/solidify/solidify-frontend)
.. Pull the source code of the lib `solidify-frontend` *with the corresponding tag* used by `dlcm-portal` (see dlcm-portal: package.json > dependencies > solidify-frontend)
.. Delete on `solidify-frontend` the `package-lock.json` file
.. Copy the content of `solidify-eslint-plugin/dist` folder inside `solidify-frontend/local_modules/eslint-plugin-solidify` (create the directories)
.. On `package.json` from `solidify-frontend`, change the version number of `devDependencies` for `eslint-plugin-solidify` by `file:./local_modules/eslint-plugin-solidify`)
.. Run `pnpm install` on `solidify-frontend`
.. Run `pnpm run build` on `solidify-frontend`, the binary should by generated in `dist/solidify-frontend` folder
.. Copy the content of `solidify-frontend/dist/solidify-frontend` folder inside `dlcm-portal/local_modules/solidify-frontend` (create the directories)
.. On `package.json` from `dlcm-portal`, change the version number of `dependencies` for `solidify-frontend` by `file:./local_modules/solidify-frontend`)

. jsmol (https://gitlab.unige.ch/solidify/jsmol)
.. Pull the source code of the lib `jsmol` *with the corresponding tag* used by `dlcm-portal` (see dlcm-portal: package.json > dependencies > jsmol)
.. Copy the content of `jsmol` folder inside `dlcm-portal/local_modules/jsmol` (create the directories)
.. On `package.json` from `dlcm-portal`, change the version number by a relative path that target root lib folder and prefixed with `file:` (`"jsmol": "file:./local_modules/jsmol"`)


To resume, you should have the following line in the `package.json` of `dlcm-portal`:

----
"dependencies": {
    ...
    "solidify-frontend": "file:./local_modules/solidify-frontend",
    ...
    "jsmol": "file:./local_modules/jsmol",
    ...
},
"devDependencies": {
    ...
    "eslint-plugin-solidify": "file:./local_modules/eslint-plugin-solidify"
    ...
}
----

After that, you need to delete `package-lock.json` on `dlcm-portal`
Then you will be able to run the following command

----
pnpm install
----

== Release

=== Prerequesites

1. If developer environment need to make changes to work with the new release, please update `DEV_ENVIRONMENT_UPGRADE.adoc` file.

2. If server environment need to make changes to work with the new release, please update `SERVER_MIGRATION_NOTES.adoc` file.

=== Steps to release

1. We use Jenkins job to release the app (behind it is maven which makes the release).
But, before running the build job you need to prepare the release with command (replace RELEASE_VERSION with the appropriate version number)

----
RELEASE_VERSION=${RELEASE_VERSION} pnpm run prepare-release
----

NB1: you can also define explicitly the tag name with 'TAG_NAME' env variable.
By default it's 'dlcm-${RELEASE_VERSION}'

NB2: you can also define the last tag used to generate release note with RELEASE_FROM_TAG_NAME.
By default, it generate the release note from last tag.

This command will :

- update package.json version number
- update package-lock.json version number
- generate changelog
- generate release notes

2. Commit the changes with the following message `chore: prepare for release ${RELEASE_VERSION}`

3. Then you can run jenkins job to release the project with:
- the checkbox "RELEASE" checked
- RELEASE_VERSION=${RELEASE_VERSION}
- SNAPSHOT_VERSION=${RELEASE_VERSION + 1}-SNAPSHOT
- TAG_NAME=dlcm-${RELEASE_VERSION}

4. Once the jenkins job finished, pull the change.
Run the following command (replace SNAPSHOT_VERSION with the appropriate version number, ex: ${RELEASE_VERSION + 1}-SNAPSHOT)

----
SNAPSHOT_VERSION=${SNAPSHOT_VERSION} pnpm run prepare-next-iteration
----

This command will :

- update package.json version number
- update package-lock.json version number

5. Commit the changes with the message `chore: prepare for next release`

== Development server

Before running the project, you need to setup both things :

* in link:src/environments/environment.local.ts[environment.local.ts], set the url of the backend admin module if different from the default value (http://localhost:16105/dlcm/admin) :

----
admin: "http://localhost:16115/dlcm/admin",
----

* in the link:proxy.conf.local.js[proxy.conf.local.js] update the settings if you want :

----
// --- START SETTINGS ---
const authorizationModulePort = 16100;
const userFirstName = "Marty";
const userLastName = "McFly";
const userEmail = "999999@unige.ch";
exports.logLevel = undefined; // "debug"
// ---- END SETTINGS ----
----

For running the project on a dev server, run the following command

----
pnpm run start
----

Once the server is started, you can access it on the url http://localhost:4200[http://localhost:4200].

The app will automatically reload if you change any of the source files.

This command run a proxy to contact backend services.

== Build

To build the project, run the command :

----
pnpm run build
----

The build artifacts will be stored in the `dist/` directory.

== SSR

See https://gitlab.unige.ch/solidify/solidify-frontend#user-content-ssr[SSR section] in Solidify Readme.

== Dev with solidify-frontend

_DLCM Portal_ is a _UNIGE_ project based on the _UNIGE_ angular library _Solidify-Frontend_.

=== Prerequisites

If you want to dev in the library Solidify-Frontend you need to pull https://gitlab.unige.ch/solidify/solidify-frontend[Solidify-Frontend] project in a folder called `solidify-frontend`.

The folder `solidify_frontend` should be sibling of the current folder `dlcm-portal`

=== Re-build _Solidify-Frontend_

If you edit _Solidify_Frontend_ sources and you want to test it before commit it, you can build _Solidify_Frontend_ and inject binary automatically into _DLCM Portal_ node_modules.
For that, you can run command

----
pnpm run build-solidify
----

The disadvantage of this method is that you have to restart the dev server every time to take into account the new version of solidify_frontend, which is slow.

The solution is to use the live edit method described below

=== Live edit

To dev in live edit mode (_Solidify-Frontend_ sources included directly like internal sources) you can run the dev server with the command :

----
pnpm run start:solidify
----

instead the traditional command `pnpm run start`

NB: Work only for typescript *.ts files.
If you edit html or css files in _Solidify_Frontend_ sources, you need to run the build command `pnpm run build-solidify` described previously.

== Generate client api

If you want to update the model and enums of the app, we use https://github.com/OpenAPITools/openapi-generator[Open Api Generator].

* Pull the master branch of the project _DLCM-Backend_
* On the project DLCM-Backend run `mvn clean package`
* Copy the content of the file `target/DLCM-Solution-2.1.0-SNAPSHOT/WEB-INF/classes/static/docs/openapi/dlcm-api.json`
* Paste it in this project into the file link:src/assets/openapi/dlcm-openapi-3.0.json[dlcm-openapi-3.0.json]
* On this project run the following command to generate the model from the file freshly updated

----
pnpm run generate-models
----

NB : `pnpm install`, `pnpm run start` and `pnpm run build` call systematically `pnpm run generate-models``

== Extract string to translate

Run the command

----
pnpm run extract-translations
----

to extract automatically string to translate into assets/i18n/*.json

== Debug NGXS state

This project is compatible with http://extension.remotedev.io/[Redux Devtools extension].

== Running unit tests

Run the command

----
pnpm run test
----

to execute the unit tests via https://karma-runner.github.io[Karma].

== Running end-to-end tests with Cypress

E2e test can be executed ba running the script :

----
pnpm run cypress
----

who run simultaneously the portal and cypress console

or by running the script :

----
pnpm run cypress-open
----

who just run the cypress console (you need to have a dev server currently executed).

== Further help

To get more help on the Angular CLI use `ng help` or go check out the https://github.com/angular/angular-cli/blob/master/README.md[Angular CLI README].

== CSS Style

The project use https://sass-lang.com/guide[SCSS Preprocessor].

=== Graphical lib

The project use https://material.angular.io/[Material design] as graphical lib.

=== Grid

There is no grid library used in the project but pure CSS.

We mainly use https://css-tricks.com/snippets/css/complete-guide-grid/[CSS Grid] combined with homemade SASS mixins.

For example, if you want to manage specific CSS behavior depends of screen size, you should use the following mixins :

----
@include respond-to-breakpoint-and-bigger('md') {
  // css apply only for size >= md (= md, lg)
}

@include respond-to-smaller-than-breakpoint('md') {
  // css apply only for size < md (= xs and sm)
}
----

=== Icons

The project use two libs for font icons :

* https://material.io/resources/icons/[Material icon]
* https://fontawesome.com/v5.15/icons?d=gallery&m=free[Font awesome (free)]

== Lexical

=== Types of components :

==== Concept

With solidify, we introduce a new convention of component :

* Routables : Component routable.
Can communicate with the store (dispatch actions and retrieve datas from store)
* Containers : Component non routable.
Can communicate with the store (dispatch actions and retrieve datas from store)
* Presentationals : Dumb component (just input / output, no connection with store)
* Dialogs : Component embedded in a material modal.
Can communicate with the store (dispatch actions and retrieve datas from store)

==== Generation

You can use solidify schematics project to generate this types of component.

Please refer to the `README.adoc` of the project `Solidify-Frontend`.
As reminder, the shortcut to generate this new type of components are :

----
ng g solidify-frontend:routable my-routable
ng g solidify-frontend:routable
ng g solidify-frontend:container my-container
ng g solidify-frontend:container
ng g solidify-frontend:presentational my-presentational
ng g solidify-frontend:presentational
ng g solidify-frontend:dialog my-dialog
ng g solidify-frontend:dialog
ng g solidify-frontend:store-resource my-store-resource
ng g solidify-frontend:store-resource
ng g solidify-frontend:store-relation-2-tiers my-store-relation-2-tiers
ng g solidify-frontend:store-relation-2-tiers
ng g solidify-frontend:store-composition my-store-composition
ng g solidify-frontend:store-composition
----

We are also compatible with default angular schematics with the command

----
ng generate component component-name
----

to generate a new component.

You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

More info https://angular.io/cli/generate[here].

== Contributing to project

Please read our link:CONTRIBUTING.adoc[contributing guidelines] before contributing to the project.
