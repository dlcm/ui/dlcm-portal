/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - environment.defaults.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ChemicalMoleculeVisualizationEnum} from "@app/features/molecular-visualization/chemical-molecule-visualization.enum";
import {Enums} from "@enums";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {
  DefaultSolidifyApplicationEnvironment,
  MappingObject,
} from "solidify-frontend";

export interface DlcmEnvironment extends DefaultSolidifyApplicationEnvironment {
  // Base URL For Module Applications (override by AppAction.LoadModules)
  preservation_planning?: string;
  authorization?: string;
  ingest?: string;
  preingest?: string;
  access?: string;
  archivalStorage?: string[];
  dataManagement?: string;
  listNotificationToHideInProfile: Enums.NotificationType.NotificationTypeEnum[];
  disableAccessRequest: boolean;
  swaggerModulesToIgnore: string[];

  defaultStorageIndex: number;
  archivalStorageName: string[];

  // Documentation
  /**
   * The integration guide toc filename
   * (Example: DLCM-IntegrationGuide-toc.html)
   */
  documentationTocIntegrationGuide: string | undefined;
  /**
   * The tools guide toc filename
   * (Example: DLCM-ToolsGuide-toc.html)
   */
  documentationTocToolsGuide: string | undefined;
  /**
   * Define if a rewrite of integration and tools guide toc content links is needed.
   * True (rewrite) by default.
   * If true, the default rewrite path used is the 'adminDocs' module path. Can be overrided by defining 'documentationTocIntegrationGuideAndToolsRewritePath'
   */
  documentationTocIntegrationGuideAndToolsNeedRewritePath: boolean;
  /**
   * The rewrite path used to update integration and tools guide toc links
   * By default use the 'adminDocs' path
   * (Example: https://yareta.unige.ch/doc)
   */
  documentationTocIntegrationGuideAndToolsRewritePath: string | undefined;

  /**
   * The user guide toc filename
   * (Example: Yareta-QuickStartGuide-toc.html)
   */
  documentationTocUserGuide: string | undefined;
  /**
   * The path to the server that host the user guide toc file
   * (Example: https://yareta.unige.ch/doc)
   */
  documentationTocUserGuidePath: string | undefined;
  /**
   * Define if a rewrite of user guide toc content links is needed.
   * True (rewrite) by default.
   * If true, the default rewrite path used is the 'adminDocs' module path. Can be overrided by defining 'documentationTocUserGuideRewritePath'
   */
  documentationTocUserGuideNeedRewritePath: boolean;
  /**
   * The rewrite path used to update user guide toc links
   * By default use the 'adminDocs' path
   * (Example: https://yareta.unige.ch/doc)
   */
  documentationTocUserGuideRewritePath: string | undefined;

  /**
   * Name of the query param used to rerun the intention of request access to an archive after login
   */
  archiveQueryParamAccessRequest: string;

  // Others
  institutionUrl: string;
  spdxUrl: string;

  /**
   * URL to the page where the user can request the creation of an organizational unit by langue
   * If undefined, the button to request the creation of an organizational unit open a modal with a form to request the creation of an organizational unit in-app (default behavior)
   * If defined, the button to request the creation of an organizational unit open a new tab with the url defined corresponding to current language or first language founded
   */
  externalUrlForRequestOrganizationalUnitCreation: MappingObject<Enums.Language.LanguageEnum, string>;

  // Home
  carouselUrl: string | undefined;
  defaultPageSizeHomePage: number;
  defaultHomeOrgUnitSize: number;
  defaultHomeViewModeTableEnum: ViewModeTableEnum;
  frequencyChangeCarouselTileInSecond: number;
  frequencyCleanCacheArchiveAccessRightInSecond: number;

  coreTrustSealEnabled: boolean;
  coreTrustSealUrl: string;

  urlNationalArchivePronom: string;

  doiLink: string;

  // Polling
  refreshTabStatusCounterIntervalInSecond: number;
  refreshOrderAvailableIntervalInSecond: number;
  refreshNotificationInboxAvailableIntervalInSecond: number;
  refreshCleanPendingRequestNotificationIntervalInSecond: number;
  refreshDepositSubmittedIntervalInSecond: number;
  refreshDepositDuaFileIntervalInSecond: number;
  refreshSipProcessIntervalInSecond: number;

  fileSizeLimitForChecksumGenerationInMegabytes: number;

  listFacetUseOperatorAnd: Enums.Facet.Name[];

  // Molecular visualization
  visualizationChemicalMoleculeMode: ChemicalMoleculeVisualizationEnum;
  visualizationChemicalMolecule2dLibs: string[];
  visualizationChemicalMolecule3dLib: string;

  // mol constant
  visualizationMolExtensions: string[];
  visualizationMolContentType: string[];
  visualizationMolMimeType: string[];
  visualizationMolPronomId: string[];
  visualizationMolMaxFileSizeInMegabytes: number | undefined;

  tableResourceRoleDefaultPageSize: number;
  tableResourceRolePageSizeOptions: number[];

  duaFileName: string;
  readmeFileName: string;
}
