/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - environment.defaults.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

// This file should contains all the defaults settings shared in all environment files
// If you want to override properties for specific environment, do it in these files

import {ThemeEnum} from "@app/shared/enums/theme.enum";
import {Enums} from "@enums";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {
  ChemicalMoleculeVisualizationEnum,
  defaultSolidifyApplicationEnvironment,
} from "solidify-frontend";
import {DlcmEnvironment} from "./environment.defaults.model";

export const defaultEnvironment: DlcmEnvironment = {
  ...defaultSolidifyApplicationEnvironment,
  appTitle: "DLCM",
  appDescription: LabelTranslateEnum.metaGeneralDescription,
  imageOpenGraph: "themes/dlcm/toolbar-header-image.png",
  listTagsComponentsWhereClickAllowToEnterInEditMode: [
    "dlcm-shared-table-orgunit-role-container",
    "dlcm-shared-table-person-role-container",
    "dlcm-shared-table-institution-role-container",
    "dlcm-shared-table-orgunit-container",
    "solidify-rich-text-editor",
    "solidify-markdown-viewer-container",
  ],
  defaultLanguage: Enums.Language.LanguageEnum.en,
  appLanguages: [Enums.Language.LanguageEnum.en, Enums.Language.LanguageEnum.fr],
  appLanguagesTranslate: Enums.Language.LanguageEnumTranslate,
  appThemes: [ThemeEnum.dlcm, ThemeEnum.yareta, ThemeEnum.centella, ThemeEnum.olos, ThemeEnum.ulb],
  appThemesTranslate: [
    {key: ThemeEnum.dlcm, value: "DLCM"},
    {key: ThemeEnum.yareta, value: "Yareta"},
    {key: ThemeEnum.olos, value: "OLOS"},
    {key: ThemeEnum.centella, value: "Centella"},
    {key: ThemeEnum.ulb, value: "ULB RD Vault"},
  ],
  theme: ThemeEnum.dlcm,
  routeHomePage: RoutesEnum.homePage,
  routeSegmentEdit: AppRoutesEnum.edit,
  listNotificationToHideInProfile: [],
  fileExtensionToCompleteWithRealExtensionOnDownload: ["dua", "thumbnail", "readme"],
  disableAccessRequest: false,
  aboutModulesToIgnore: ["oaiPmh"],
  swaggerModulesToIgnore: ["oaiInfo", "oaiPmh", "authorization"],

  // Base URL For Module Applications (override by AppAction.LoadModules)
  admin: "http://localhost:16105/dlcm/admin",

  // Documentation
  documentationTocIntegrationGuide: "DLCM-IntegrationGuide-toc.html",
  documentationTocToolsGuide: "DLCM-ToolsGuide-toc.html",
  documentationTocIntegrationGuideAndToolsNeedRewritePath: true,
  documentationTocIntegrationGuideAndToolsRewritePath: undefined,

  documentationTocUserGuide: undefined,
  documentationTocUserGuidePath: undefined,
  documentationTocUserGuideNeedRewritePath: true,
  documentationTocUserGuideRewritePath: undefined,

  // Orcid
  orcidQueryParam: "orcid",

  archiveQueryParamAccessRequest: "accessRequest",

  institutionUrl: "https://www.unige.ch/",
  spdxUrl: "https://spdx.org/licenses/",
  externalUrlForRequestOrganizationalUnitCreation: undefined,

  // Home
  defaultHomeViewModeTableEnum: ViewModeTableEnum.tiles,
  defaultPageSizeHomePage: 10,
  defaultHomeOrgUnitSize: 1000,
  carouselUrl: undefined,
  frequencyChangeCarouselTileInSecond: 5,
  frequencyCleanCacheArchiveAccessRightInSecond: 60,

  twitterEnabled: true,
  twitterAccount: "dlcm_ch",

  coreTrustSealEnabled: false,
  coreTrustSealUrl: "https://amt.coretrustseal.org/certificates",

  searchFacetValuesTranslations: [
    ...Enums.Access.AccessEnumTranslate.map(v => ({
      name: Enums.Facet.Name.ACCESS_LEVEL_FACET,
      value: v.key,
      isFrontendTranslation: true,
      labelToTranslate: v.value,
    })),
    {
      name: Enums.Facet.Name.RETENTION_FACET,
      value: "5 years",
      isFrontendTranslation: false,
      labels: [{language: "en", text: "5 years"}, {language: "fr", text: "5 ans"}],
    },
    {
      name: Enums.Facet.Name.RETENTION_FACET,
      value: "10 years",
      isFrontendTranslation: false,
      labels: [{language: "en", text: "10 years"}, {language: "fr", text: "10 ans"}],
    },
    {
      name: Enums.Facet.Name.RETENTION_FACET,
      value: "forever",
      isFrontendTranslation: true,
      labelToTranslate: LabelTranslateEnum.forever,
    },
    ...Enums.DataSensitivity.DataSensitivityEnumTranslate.map(v => ({
      name: Enums.Facet.Name.DATA_TAG_FACET,
      value: v.key,
      isFrontendTranslation: true,
      labelToTranslate: v.value,
    })),
    ...Enums.Deposit.DataUsePolicyEnumTranslate.map(v => ({
      name: Enums.Facet.Name.DATA_USE_POLICIES_FACET,
      value: v.key,
      isFrontendTranslation: true,
      labelToTranslate: v.value,
    })),
    {
      name: Enums.Facet.Name.ARCHIVE_FACET,
      value: "archive",
      isFrontendTranslation: true,
      labelToTranslate: LabelTranslateEnum.archive,
    },
    {
      name: Enums.Facet.Name.ARCHIVE_FACET,
      value: "collection",
      isFrontendTranslation: true,
      labelToTranslate: LabelTranslateEnum.collection,
    },
  ],

  urlNationalArchivePronom: "https://www.nationalarchives.gov.uk/PRONOM/",

  // Polling
  refreshTabStatusCounterIntervalInSecond: 60,
  refreshOrderAvailableIntervalInSecond: 5,
  refreshNotificationInboxAvailableIntervalInSecond: 60,
  refreshCleanPendingRequestNotificationIntervalInSecond: 60,
  refreshDepositSubmittedIntervalInSecond: 2,
  refreshDepositDuaFileIntervalInSecond: 2,
  refreshSipProcessIntervalInSecond: 2,

  // Errors
  httpErrorKeyToSkipInErrorHandler: [],

  archivalStorageName: [
    "File",
    "S2",
  ],
  defaultStorageIndex: 0,
  fileSizeLimitForChecksumGenerationInMegabytes: 100,

  listFacetUseOperatorAnd: [Enums.Facet.Name.CREATOR_FACET],

  // Molecular visualization
  visualizationChemicalMoleculeMode: ChemicalMoleculeVisualizationEnum.threeAndTwoDimensional,
  visualizationChemicalMolecule2dLibs: ["JSmolJME.js", "jsme.nocache.js"],
  visualizationChemicalMolecule3dLib: "JSmol.min.js",

  // mol constant
  visualizationMolExtensions: ["sdf", "log"],
  visualizationMolContentType: [],
  visualizationMolMimeType: ["text/plain", "chemical/x-jcamp-dx", "chemical/x-sketchel", "chemical/x-kinemage", "chemical/x-macmolecule", "chemical/x-macromodel-input", "chemical/x-daylight-smiles", "chemical/x-isostar", "chemical/x-genbank", "chemical/x-gcg8-sequence", "chemical/x-gaussian-input", "chemical/x-gaussian-cube", "chemical/x-gaussian-checkpoint", "chemical/x-gamess-input", "chemical/x-galactic-spc", "chemical/x-embl-dl-nucleotide", "chemical/x-cxf", "chemical/x-chem3d", "chemical/x-ctx", "chemical/x-csml", "chemical/x-crossfire", "chemical/x-compass", "chemical/x-cml", "chemical/x-cmdf", "chemical/x-cif", "chemical/x-chemdraw", "text/plain", "chemical/x-mdl-sdfile", "chemical/x-mdl-molfile", "chemical/x-alchemy", "chemical/x-cache-csf", "chemical/x-cerius", "chemical/x-cactvs-binary"],
  visualizationMolPronomId: [],
  visualizationMolMaxFileSizeInMegabytes: undefined,

  oaiProviderCustomParam: "smartView=dlcm_oai2.xsl",

  avatarSizeDefault: 300,
  avatarSizeMapping: {
    [ImageDisplayModeEnum.MAIN]: 300,
    [ImageDisplayModeEnum.UPLOAD]: 145,
    [ImageDisplayModeEnum.IN_TILE]: 200,
    [ImageDisplayModeEnum.IN_OVERLAY]: 125,
    [ImageDisplayModeEnum.IN_LIST]: 45,
    [ImageDisplayModeEnum.NEXT_FORM_FIELD]: 45,
  },
  imageSizeDefault: 400,
  imageSizeMapping: {
    [ImageDisplayModeEnum.MAIN]: 400,
    [ImageDisplayModeEnum.UPLOAD]: 200,
    [ImageDisplayModeEnum.IN_TILE]: 200,
    [ImageDisplayModeEnum.IN_OVERLAY]: 125,
    [ImageDisplayModeEnum.IN_ORDER]: 125,
    [ImageDisplayModeEnum.IN_LIST]: 50,
    [ImageDisplayModeEnum.NEXT_FORM_FIELD]: 50,
  },
  uploadImageDefaultAspectRatio: 1,
  uploadAvatarDefaultAspectRatio: 1,

  tableResourceRoleDefaultPageSize: 5,
  tableResourceRolePageSizeOptions: [5, 10, 15, 20],

  duaFileName: "archive.dua",
  readmeFileName: "archive.readme",

  defaultNotificationWarningDurationInSeconds: 10,
};
