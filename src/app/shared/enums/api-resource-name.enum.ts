/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - api-resource-name.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ApiResourceNamePartialEnum} from "solidify-frontend/src/lib/core/enums";

enum ApiResourceNameExtendEnum {
  DEPOSIT = "deposits",
  ORDER = "orders",
  PERSON = "people",
  MEMBER = "members",
  ORG_UNIT = "organizational-units",
  ADDITIONAL_FIELDS_FORM = "additional-fields-forms",
  CURRENT_VERSION = "current-version",
  AUTHORIZED_ORG_UNIT = "authorized-organizational-units",
  AUTHORIZED_INSTITUTION = "authorized-institutions",
  INSTITUTION = "institutions",
  DATAFILE = "data",
  SIP = "sip",
  SIP_FILE = "sip-package",
  AIP = "aip",
  AIP_FILE = "aip-package",
  STORED_AIP = "stored-aip",
  DIP = "dip",
  DI = "di",
  DI_PUBLIC = "public-di",
  DI_PRIVATE = "private-di",
  SETTING = "settings",
  SUBMISSION_AGREEMENT = "submission-agreements",
  PACKAGES = "packages",
  LICENSE = "licenses",
  ROLE = "roles",
  ROLE_APPLICATION = "application-roles",
  FUNDING_AGENCY = "funding-agencies",
  MODULES = "modules",
  ARCHIVE_ACL = "archive-acl",
  ARCHIVE_TYPE = "archive-types",
  INDEX_FIELD_ALIASES = "index-field-aliases",
  USER = "users",
  LANGUAGE = "languages",
  BASIC_AUTH_CLIENT = "basic-auth-clients",
  LOGIN = "shiblogin",
  ITEM = "items",
  MONITOR = "monitor",
  PRES_JOB = "preservation-jobs",
  PRES_JOB_EXECUTION = "executions",
  PRES_JOB_EXECUTION_REPORT = "reports",
  PRES_POLICY = "preservation-policies",
  SUB_POLICY = "submission-policies",
  DISSEMINATION_POLICY = "dissemination-policies",
  POLICY = "policy",
  DIS_POLICY = "dissemination-policies",
  SUBJECT_AREA = "subject-areas",
  METADATA_TYPE = "metadata-types",
  NOTIFICATION = "notifications",
  NOTIFICATION_TYPES = "notification-types",
  CONTRIBUTOR = "contributors",
  MODULE = "modules",
  SYSTEM_PROPERTY = "system-properties",
  SCHEDULED_TASK = "scheduled-tasks",
  GLOBAL_BANNERS = "global-banners",
  ORCID_SYNCHRONIZATION = "orcid-synchronizations",
  ORCID_EXTERNAL_WEBSITES = "external-websites",

  // Miscellaneous
  SCHEMA = "schema",
  PROFILE = "profile",
  XSL = "xsl",
  RESERVE_DOI = "reserve-doi",
  ARCHIVAL_TYPE = "archivalUnit",
  PUBLIC_METADATA = "metadata",
  PRIVATE_METADATA = "private-metadata",
  ALL_METADATA = "all-metadata",
  ARCHIVE = "archives",

  // OAI
  OAI = "oai",
  OAI_PROVIDER = "oai-provider",
  OAI_SETS = "oai-sets",
  OAI_METADATA_PREFIXES = "oai-metadata-prefixes",
}

export const ApiResourceNameEnum = {...ApiResourceNamePartialEnum, ...ApiResourceNameExtendEnum} as Omit<typeof ApiResourceNamePartialEnum, keyof typeof ApiResourceNameExtendEnum> & typeof ApiResourceNameExtendEnum;
export type ApiResourceNameEnum = typeof ApiResourceNameEnum[keyof typeof ApiResourceNameEnum];
