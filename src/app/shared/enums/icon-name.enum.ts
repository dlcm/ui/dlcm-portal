/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - icon-name.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  IconNamePartialEnum,
  PropertyName,
} from "solidify-frontend";

export class IconNameEnum extends IconNamePartialEnum {
  @PropertyName() static organizationalUnit: string;
  @PropertyName() static deposit: string;
  @PropertyName() static home: string;
  @PropertyName() static order: string;
  @PropertyName() static preservationSpace: string;
  @PropertyName() static preservationPlanning: string;
  @PropertyName() static administration: string;
  @PropertyName() static profile: string;
  @PropertyName() static token: string;
  @PropertyName() static logout: string;
  @PropertyName() static login: string;
  @PropertyName() static resId: string;
  @PropertyName() static history: string;
  @PropertyName() static docDev: string;
  @PropertyName() static docUser: string;
  @PropertyName() static about: string;
  @PropertyName() static cart: string;
  @PropertyName() static addToCart: string;
  @PropertyName() static search: string;
  @PropertyName() static back: string;
  @PropertyName() static refresh: string;
  @PropertyName() static synchronize: string;
  @PropertyName() static delete: string;
  @PropertyName() static deleteAll: string;
  @PropertyName() static create: string;
  @PropertyName() static internalLink: string;
  @PropertyName() static externalLink: string;
  @PropertyName() static add: string;
  @PropertyName() static edit: string;
  @PropertyName() static close: string;
  @PropertyName() static archiveBrowsing: string;
  @PropertyName() static download: string;
  @PropertyName() static sendRequest: string;
  @PropertyName() static accessControlled: string;
  @PropertyName() static roleManager: string;
  @PropertyName() static roleSteward: string;
  @PropertyName() static roleApprover: string;
  @PropertyName() static roleCreator: string;
  @PropertyName() static roleVisitor: string;
  @PropertyName() static memberWithoutRole: string;
  @PropertyName() static myOrder: string;
  @PropertyName() static allOrder: string;
  @PropertyName() static metadata: string;
  @PropertyName() static files: string;
  @PropertyName() static associateArchive: string;
  @PropertyName() static collection: string;
  @PropertyName() static navigate: string;
  @PropertyName() static reserveDoi: string;
  @PropertyName() static submit: string;
  @PropertyName() static approve: string;
  @PropertyName() static unapprove: string;
  @PropertyName() static wait: string;
  @PropertyName() static save: string;
  @PropertyName() static uploadFile: string;
  @PropertyName() static uploadStructuredFiles: string;
  @PropertyName() static filesView: string;
  @PropertyName() static foldersView: string;
  @PropertyName() static preview: string;
  @PropertyName() static star: string;
  @PropertyName() static move: string;
  @PropertyName() static information: string;
  @PropertyName() static contributor: string;
  @PropertyName() static duaSigned: string;
  @PropertyName() static duaClickThrough: string;
  @PropertyName() static duaExternal: string;
  @PropertyName() static duaNone: string;
  @PropertyName() static requestInbox: string;
  @PropertyName() static requestSent: string;
  @PropertyName() static sip: string;
  @PropertyName() static dip: string;
  @PropertyName() static aip: string;
  @PropertyName() static monitoring: string;
  @PropertyName() static jobs: string;
  @PropertyName() static archivingStatus: string;
  @PropertyName() static aipDownloaded: string;
  @PropertyName() static storagion: string;
  @PropertyName() static simpleChecksum: string;
  @PropertyName() static doubleChecksum: string;
  @PropertyName() static reindex: string;
  @PropertyName() static check: string;
  @PropertyName() static compliance: string;
  @PropertyName() static init: string;
  @PropertyName() static run: string;
  @PropertyName() static resume: string;
  @PropertyName() static resumeAll: string;
  @PropertyName() static notIgnore: string;
  @PropertyName() static orderReady: string;
  @PropertyName() static orderInProgress: string;
  @PropertyName() static orderInError: string;
  @PropertyName() static orderApprove: string;
  @PropertyName() static archiveTypes: string;
  @PropertyName() static submissionAgreements: string;
  @PropertyName() static submissionAgreementsUser: string;
  @PropertyName() static submissionPolicies: string;
  @PropertyName() static preservationPolicies: string;
  @PropertyName() static disseminationPolicies: string;
  @PropertyName() static license: string;
  @PropertyName() static licenses: string;
  @PropertyName() static globalBanners: string;
  @PropertyName() static institutions: string;
  @PropertyName() static subjectAreas: string;
  @PropertyName() static users: string;
  @PropertyName() static roles: string;
  @PropertyName() static oaiMetadataPrefixes: string;
  @PropertyName() static oaiSets: string;
  @PropertyName() static peoples: string;
  @PropertyName() static fundingAgencies: string;
  @PropertyName() static indexFieldAliases: string;
  @PropertyName() static archiveAcl: string;
  @PropertyName() static languages: string;
  @PropertyName() static metadataTypes: string;
  @PropertyName() static notifications: string;
  @PropertyName() static passwordVisible: string;
  @PropertyName() static passwordHide: string;
  @PropertyName() static testFile: string;
  @PropertyName() static folderEmpty: string;
  @PropertyName() static folderOpened: string;
  @PropertyName() static folderClosed: string;
  @PropertyName() static expandAll: string;
  @PropertyName() static collapseAll: string;
  @PropertyName() static send: string;
  @PropertyName() static clear: string;
  @PropertyName() static done: string;
  @PropertyName() static update: string;
  @PropertyName() static emptyCart: string;
  @PropertyName() static copyToClipboard: string;
  @PropertyName() static notFound: string;
  @PropertyName() static up: string;
  @PropertyName() static down: string;
  @PropertyName() static personAdd: string;
  @PropertyName() static redo: string;
  @PropertyName() static sort: string;
  @PropertyName() static clearCache: string;
  @PropertyName() static warning: string;
  @PropertyName() static autoUpdate: string;
  @PropertyName() static fingerprint: string;
  @PropertyName() static http: string;
  @PropertyName() static closeChip: string;
  @PropertyName() static success: string;
  @PropertyName() static error: string;
  @PropertyName() static zoomOut: string;
  @PropertyName() static zoomIn: string;
  @PropertyName() static menuButtons: string;
  @PropertyName() static ror: string;
  @PropertyName() static spdx: string;
  @PropertyName() static doi: string;
  @PropertyName() static ark: string;
  @PropertyName() static crossref: string;
  @PropertyName() static grid: string;
  @PropertyName() static isni: string;
  @PropertyName() static wikidata: string;
  @PropertyName() static archive: string;
  @PropertyName() static unigeBlack: string;
  @PropertyName() static unigeWhite: string;
  @PropertyName() static theme: string;
  @PropertyName() static darkMode: string;
  @PropertyName() static lightMode: string;
  @PropertyName() static creationDate: string;
  @PropertyName() static retentionDate: string;
  @PropertyName() static defaultValue: string;
  @PropertyName() static change: string;
  @PropertyName() static trueValue: string;
  @PropertyName() static falseValue: string;
  @PropertyName() static dispose: string;
  @PropertyName() static approveDisposal: string;
  @PropertyName() static approveDisposalByOrgUnit: string;
  @PropertyName() static extendRetention: string;
  @PropertyName() static listView: string;
  @PropertyName() static tilesView: string;
  @PropertyName() static accessLevelRestricted: string;
  @PropertyName() static accessLevelClosed: string;
  @PropertyName() static accessLevelPublic: string;
  @PropertyName() static accessLevelUndefined: string;
  @PropertyName() static uploadImage: string;
  @PropertyName() static help: string;
  @PropertyName() static maintenance: string;
  @PropertyName() static offline: string;
  @PropertyName() static viewNumber: string;
  @PropertyName() static downloadNumber: string;
  @PropertyName() static sortUndefined: string;
  @PropertyName() static sortAscending: string;
  @PropertyName() static sortDescending: string;
  @PropertyName() static noPreview: string;
  @PropertyName() static fullScreenEnter: string;
  @PropertyName() static fullScreenLeave: string;
  @PropertyName() static guidedTour: string;
  @PropertyName() static dataSensitivity: string;
  @PropertyName() static plus: string;
  @PropertyName() static minus: string;
  @PropertyName() static rate: string;
  @PropertyName() static markAsUnread: string;
  @PropertyName() static markAsRead: string;
  @PropertyName() static privacy: string;
  @PropertyName() static more: string;
  @PropertyName() static downArrow: string;
  @PropertyName() static putInError: string;
  @PropertyName() static fixAipInfo: string;
  @PropertyName() static next: string;
  @PropertyName() static checksums: string;
  @PropertyName() static checkCompliance: string;
  @PropertyName() static abort: string;
  @PropertyName() static leaveOrganizationalUnit: string;
  @PropertyName() static enabledScheduled: string;
  @PropertyName() static disabledScheduled: string;
  @PropertyName() static stop: string;
  @PropertyName() static keyword: string;
  @PropertyName() static language: string;
  @PropertyName() static contentType: string;
  @PropertyName() static subjectArea: string;
  @PropertyName() static dataCollectionPeriod: string;
  @PropertyName() static readme: string;
  @PropertyName() static quoteRight: string;
  @PropertyName() static reset: string;
  @PropertyName() static archivist: string;
  @PropertyName() static hedera: string;
  @PropertyName() static iiif: string;
  @PropertyName() static anonymousReview: string;
  @PropertyName() static regenerateAnonymousReview: string;
}
