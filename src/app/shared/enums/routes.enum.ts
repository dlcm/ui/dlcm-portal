/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - routes.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AppRoutesPartialEnum,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

export const urlSeparator: string = SOLIDIFY_CONSTANTS.URL_SEPARATOR;

enum AppRoutesExtendEnum {
  deposit = "deposit",
  aip = "aip",
  preservationSpace = "preservation-space",
  admin = "admin",
  preservationPlanning = "preservation-planning",
  order = "order",
  paramIdDataFile = ":idFile",
  paramIdDataFileWithoutPrefixParam = "idFile",
  paramIdOrgUnit = ":idOrgUnit",
  paramIdOrgUnitWithoutPrefixParam = "idOrgUnit",
  paramIdExecution = ":idExecution",
  paramIdExecutionWithoutPrefixParam = "idExecution",
  archives = "archives",
}

export const AppRoutesEnum = {...AppRoutesPartialEnum, ...AppRoutesExtendEnum} as Omit<typeof AppRoutesPartialEnum, keyof typeof AppRoutesExtendEnum> & typeof AppRoutesExtendEnum;
export type AppRoutesEnum = typeof AppRoutesEnum[keyof typeof AppRoutesEnum];

export enum HomePageRoutesEnum {
  detail = "detail",
  notFound = "not-found",
  search = "search",
  files = "files",
  collections = "collections",
}

export enum DepositRoutesEnum {
  create = "create",
  detail = "detail",
  edit = "edit",
  metadata = "metadata",
  readme = "readme",
  data = "data",
  files = "files",
  collections = "collections",
  paramTab = ":tab",
  paramTabWithoutPrefix = "tab",
}

export enum PreservationSpaceOrganizationalUnitRoutesEnum {
  detail = "detail",
  edit = "edit",
  manageAcl = "manage-acl",
  manageAclList = "list",
  manageAclDetail = "detail",
  manageAclEdit = "edit",
  manageAclCreate = "create",
}

export enum PreservationSpaceInstitutionRoutesEnum {
  detail = "detail",
  edit = "edit",
}

export enum PreservationSpaceRoutesEnum {
  organizationalUnit = "organizational-unit",
  institution = "institution",
  contributor = "contributor",
  aipSteward = "aip-steward",
  notificationInbox = "notification-inbox",
  notificationSent = "notification-sent",
  notificationDetail = "detail",
}

export enum PreservationSpaceAipStewardRoutesEnum {
  aipStewardDetail = "detail",
  aipStewardMetadata = "metadata",
  aipStewardFiles = "files",
  aipStewardCollection = "collection",
  aipStewardNotFound = "not-found",
}

export enum ContributorRoutesEnum {
  detail = "detail",
  edit = "edit",
}

export enum AdminRoutesEnum {
  submissionAgreement = "submission-agreement",
  submissionAgreementCreate = "create",
  submissionAgreementDetail = "detail",
  submissionAgreementEdit = "edit",

  submissionAgreementUser = "submission-agreement-user",

  submissionPolicy = "submission-policy",
  submissionPolicyCreate = "create",
  submissionPolicyDetail = "detail",
  submissionPolicyEdit = "edit",

  preservationPolicy = "preservation-policy",
  preservationPolicyDetail = "detail",
  preservationPolicyCreate = "create",
  preservationPolicyEdit = "edit",

  disseminationPolicy = "dissemination-policy",
  disseminationPolicyDetail = "detail",
  disseminationPolicyCreate = "create",
  disseminationPolicyEdit = "edit",

  archiveType = "archive-type",
  archiveTypeCreate = "create",
  archiveTypeDetail = "detail",
  archiveTypeEdit = "edit",

  license = "license",
  licenseCreate = "create",
  licenseEdit = "edit",
  licenseDetail = "detail",

  organizationalUnit = "organizational-unit",
  organizationalUnitCreate = "create",
  organizationalUnitDetail = "detail",
  organizationalUnitData = "data",
  organizationalUnitEdit = "edit",
  organizationalUnitAdditionalFieldsForm = "additional-fields-form",

  institution = "institution",
  institutionCreate = "create",
  institutionDetail = "detail",
  institutionEdit = "edit",

  subjectArea = "subject-area",

  language = "language",

  user = "user",
  userDetail = "detail",
  userEdit = "edit",

  person = "person",
  personDetail = "detail",
  personCreate = "create",
  personEdit = "edit",

  role = "role",
  roleDetail = "detail",
  roleCreate = "create",
  roleEdit = "edit",

  notification = "notification",
  notificationDetail = "detail",
  notificationCreate = "create",
  notificationEdit = "edit",

  fundingAgencies = "funding-agencies",
  fundingAgenciesDetail = "detail",
  fundingAgenciesCreate = "create",
  fundingAgenciesEdit = "edit",

  archiveAcl = "archive-acl",
  archiveAclDetail = "detail",
  archiveAclCreate = "create",
  archiveAclEdit = "edit",

  metadataType = "metadata-type",
  metadataTypeCreate = "create",
  metadataTypeEdit = "edit",
  metadataTypeDetail = "detail",

  scheduledTask = "scheduled-task",
  scheduledTaskCreate = "create",
  scheduledTaskEdit = "edit",
  scheduledTaskDetail = "detail",

}

export enum OrderRoutesEnum {
  order = "order",
  allOrder = "all-order",
  allOrderCreate = "create",
  allOrderDetail = "detail",
  allOrderEdit = "edit",
  myOrder = "my-order",
  myOrderCreate = "create",
  myOrderDetail = "detail",
  myOrderDraft = "draft",
  myOrderEdit = "edit",
}

export enum SharedAipRoutesEnum {
  aipDetail = "detail",
  aipList = "list",
  aipTabAll = "all",
  aipTabUnit = "unit",
  aipTabCollections = "collection",
  aipMetadata = "metadata",
  aipFiles = "files",
  aipCollections = "collections",
  aipNotFound = "not-found",
  tab = ":tab",
  tabWithoutPrefixParam = "tab",
}

export enum PreservationPlanningRoutesEnum {
  monitoring = "monitoring",

  aipStatuses = "aip-statuses",

  job = "job",
  jobCreate = "create",
  jobDetail = "detail",
  jobEdit = "edit",
  execution = "execution",

  aip = "aip",
  aipDownloaded = "aip-downloaded",

  storagionNumber = ":storagion",
  storagionNumberWithoutPrefixParam = "storagion",

  dip = "dip",
  dipDetail = "detail",
  dipEdit = "edit",
  dipMetadata = "metadata",
  dipFiles = "files",

  deposit = "deposit",
  depositDetail = "detail",

  sip = "sip",
  sipDetail = "detail",
  sipMetadata = "metadata",
  sipFiles = "files",
  sipCollections = "collections",
}

export class RoutesEnum implements RoutesEnum {
  static index: string = AppRoutesEnum.index;
  static login: string = AppRoutesEnum.login;
  static maintenance: string = AppRoutesEnum.maintenance;
  static about: string = AppRoutesEnum.about;
  static releaseNotes: string = AppRoutesEnum.releaseNotes;
  static changelog: string = AppRoutesEnum.changelog;
  static serverOffline: string = AppRoutesEnum.serverOffline;
  static unableToLoadApp: string = AppRoutesEnum.unableToLoadApp;
  static colorCheck: string = AppRoutesEnum.colorCheck;
  static archives: string = AppRoutesEnum.archives;

  static homePage: string = AppRoutesEnum.home;
  static homeSearch: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.search;
  static homeDetail: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.detail;
  static homeNotFound: string = AppRoutesEnum.home + urlSeparator + HomePageRoutesEnum.notFound;

  static deposit: string = AppRoutesEnum.deposit;
  static depositCreate: string = AppRoutesEnum.deposit + urlSeparator + DepositRoutesEnum.create;
  static depositDetail: string = AppRoutesEnum.deposit + urlSeparator + DepositRoutesEnum.detail;

  static order: string = AppRoutesEnum.order;

  static orderAllOrder: string = AppRoutesEnum.order + urlSeparator + OrderRoutesEnum.allOrder;
  static orderAllOrderDetail: string = AppRoutesEnum.order + urlSeparator + OrderRoutesEnum.allOrder + urlSeparator + OrderRoutesEnum.allOrderDetail;
  static orderAllOrderCreate: string = AppRoutesEnum.order + urlSeparator + OrderRoutesEnum.allOrder + urlSeparator + OrderRoutesEnum.allOrderCreate;

  static orderMyOrder: string = AppRoutesEnum.order + urlSeparator + OrderRoutesEnum.myOrder;
  static orderMyOrderDraft: string = AppRoutesEnum.order + urlSeparator + OrderRoutesEnum.myOrder + urlSeparator + OrderRoutesEnum.myOrderDraft;
  static orderMyOrderDetail: string = AppRoutesEnum.order + urlSeparator + OrderRoutesEnum.myOrder + urlSeparator + OrderRoutesEnum.myOrderDetail;

  static admin: string = AppRoutesEnum.admin;

  static adminSubmissionAgreement: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.submissionAgreement;
  static adminSubmissionAgreementCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.submissionAgreement + urlSeparator + AdminRoutesEnum.submissionAgreementCreate;
  static adminSubmissionAgreementDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.submissionAgreement + urlSeparator + AdminRoutesEnum.submissionAgreementDetail;

  static adminSubmissionAgreementUser: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.submissionAgreementUser;

  static adminSubmissionPolicy: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.submissionPolicy;
  static adminSubmissionPolicyCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.submissionPolicy + urlSeparator + AdminRoutesEnum.submissionPolicyCreate;
  static adminSubmissionPolicyDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.submissionPolicy + urlSeparator + AdminRoutesEnum.submissionPolicyDetail;

  static adminArchiveType: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.archiveType;
  static adminArchiveTypeCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.archiveType + urlSeparator + AdminRoutesEnum.archiveTypeCreate;
  static adminArchiveTypeDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.archiveType + urlSeparator + AdminRoutesEnum.archiveTypeDetail;

  static adminPreservationPolicy: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.preservationPolicy;
  static adminPreservationPolicyCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.preservationPolicy + urlSeparator + AdminRoutesEnum.preservationPolicyCreate;
  static adminPreservationPolicyDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.preservationPolicy + urlSeparator + AdminRoutesEnum.preservationPolicyDetail;

  static adminDisseminationPolicy: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.disseminationPolicy;
  static adminDisseminationPolicyCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.disseminationPolicy + urlSeparator + AdminRoutesEnum.disseminationPolicyCreate;
  static adminDisseminationPolicyDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.disseminationPolicy + urlSeparator + AdminRoutesEnum.disseminationPolicyDetail;

  static adminLicense: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.license;
  static adminLicenseCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.license + urlSeparator + AdminRoutesEnum.licenseCreate;
  static adminLicenseDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.license + urlSeparator + AdminRoutesEnum.licenseDetail;

  static adminOrganizationalUnit: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.organizationalUnit;
  static adminOrganizationalUnitCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.organizationalUnit + urlSeparator + AdminRoutesEnum.organizationalUnitCreate;
  static adminOrganizationalUnitDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.organizationalUnit + urlSeparator + AdminRoutesEnum.organizationalUnitDetail;

  static adminInstitution: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.institution;
  static adminInstitutionCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.institution + urlSeparator + AdminRoutesEnum.institutionCreate;
  static adminInstitutionDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.institution + urlSeparator + AdminRoutesEnum.institutionDetail;

  static adminSubjectArea: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.subjectArea;

  static adminLanguage: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.language;

  static adminUser: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.user;
  static adminUserDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.user + urlSeparator + AdminRoutesEnum.userDetail;

  static adminPerson: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.person;
  static adminPersonCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.person + urlSeparator + AdminRoutesEnum.personCreate;
  static adminPersonDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.person + urlSeparator + AdminRoutesEnum.personDetail;

  static adminRole: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.role;
  static adminRoleCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.role + urlSeparator + AdminRoutesEnum.roleCreate;
  static adminRoleDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.role + urlSeparator + AdminRoutesEnum.roleDetail;

  static adminNotification: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.notification;
  static adminNotificationCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.notification + urlSeparator + AdminRoutesEnum.notificationCreate;
  static adminNotificationDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.notification + urlSeparator + AdminRoutesEnum.notificationDetail;

  static adminFundingAgencies: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.fundingAgencies;
  static adminFundingAgenciesCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.fundingAgencies + urlSeparator + AdminRoutesEnum.fundingAgenciesCreate;
  static adminFundingAgenciesDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.fundingAgencies + urlSeparator + AdminRoutesEnum.fundingAgenciesDetail;

  static adminArchiveAcl: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.archiveAcl;
  static adminArchiveAclCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.archiveAcl + urlSeparator + AdminRoutesEnum.archiveAclCreate;
  static adminArchiveAclDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.archiveAcl + urlSeparator + AdminRoutesEnum.archiveAclDetail;

  static adminMetadataType: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.metadataType;
  static adminMetadataTypeCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.metadataType + urlSeparator + AdminRoutesEnum.metadataTypeCreate;
  static adminMetadataTypeDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.metadataType + urlSeparator + AdminRoutesEnum.metadataTypeDetail;

  static adminScheduledTask: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.scheduledTask;
  static adminScheduledTaskCreate: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.scheduledTask + urlSeparator + AdminRoutesEnum.scheduledTaskCreate;
  static adminScheduledTaskDetail: string = AppRoutesEnum.admin + urlSeparator + AdminRoutesEnum.scheduledTask + urlSeparator + AdminRoutesEnum.scheduledTaskDetail;

  static preservationPlanning: string = AppRoutesEnum.preservationPlanning;

  static preservationPlanningAip: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.aip;

  static preservationPlanningAipDownloaded: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.aipDownloaded;
  static preservationPlanningAipDownloadedList: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.aipDownloaded;
  static preservationPlanningAipDownloadedDetail: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.aipDownloaded + urlSeparator + SharedAipRoutesEnum.aipDetail;

  static preservationPlanningDeposit: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.deposit;

  static preservationPlanningSip: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.sip;
  static preservationPlanningSipDetail: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.sip + urlSeparator + PreservationPlanningRoutesEnum.sipDetail;

  static preservationPlanningDip: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.dip;
  static preservationPlanningDipDetail: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.dip + urlSeparator + PreservationPlanningRoutesEnum.dipDetail;

  static preservationPlanningMonitoring: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.monitoring;

  static preservationPlanningJob: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.job;
  static preservationPlanningJobCreate: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.job + urlSeparator + PreservationPlanningRoutesEnum.jobCreate;
  static preservationPlanningJobDetail: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.job + urlSeparator + PreservationPlanningRoutesEnum.jobDetail;

  static preservationPlanningAipStatuses: string = AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.aipStatuses;

  static preservationSpace: string = AppRoutesEnum.preservationSpace;

  static preservationSpaceOrganizationalUnit: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.organizationalUnit;
  static preservationSpaceOrganizationalUnitDetail: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.organizationalUnit + urlSeparator + PreservationSpaceOrganizationalUnitRoutesEnum.detail;

  static preservationSpaceInstitution: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.institution;
  static preservationSpaceInstitutionDetail: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.institution + urlSeparator + PreservationSpaceOrganizationalUnitRoutesEnum.detail;

  static preservationSpaceNotificationInbox: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.notificationInbox;
  static preservationSpaceNotificationInboxDetail: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.notificationInbox + urlSeparator + AdminRoutesEnum.notificationDetail;

  static preservationSpaceNotificationSent: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.notificationSent;
  static preservationSpaceNotificationSentDetail: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.notificationSent + urlSeparator + AdminRoutesEnum.notificationDetail;

  static preservationSpaceContributor: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.contributor;
  static preservationSpaceContributorDetail: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.contributor + urlSeparator + ContributorRoutesEnum.detail;

  static preservationSpaceAipSteward: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.aipSteward;
  static preservationSpaceAipStewardList: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.aipSteward + urlSeparator + SharedAipRoutesEnum.aipList;
  static preservationSpaceAipStewardDetail: string = AppRoutesEnum.preservationSpace + urlSeparator + PreservationSpaceRoutesEnum.aipSteward + urlSeparator + SharedAipRoutesEnum.aipDetail;
}
