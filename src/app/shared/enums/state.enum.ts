/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - state.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {StatePartialEnum} from "solidify-frontend";

enum StateExtendEnum {
  router = "router",

  application = "application",
  application_toc = "application_toc",
  application_user = "application_user",
  application_user_logo = "application_user_logo",
  application_person = "application_person",
  application_person_institution = "application_person_institution",
  application_person_notificationType = "application_person_notificationType",
  application_notificationInbox = "application_notificationInbox",
  application_authorizedOrganizationalUnit = "application_authorizedOrganizationalUnit",
  application_memberOrganizationalUnit = "application_memberOrganizationalUnit",
  application_authorizedInstitution = "application_authorizedInstitution",
  application_cart = "application_cart",
  application_cart_archive = "application_cart_archive",
  application_cart_order = "application_cart_order",
  application_cart_dip = "application_cart_dip",
  application_cart_dip_dataFile = "application_cart_dip_dataFile",
  application_archiveAcl = "application_archiveAcl",

  home = "home",
  home_organizationalUnit = "home_organizationalUnit",
  home_archive_dataFile = "home_archive_dataFile",
  home_archive_collection = "home_archive_collection",
  home_archive_package = "home_archive_package",
  home_archive_rating = "home_archive_rating",

  deposit = "deposit",
  deposit_upload = "deposit_upload",
  deposit_person = "deposit_person",
  deposit_organizationalUnit = "deposit_organizationalUnit",
  deposit_organizationalUnit_additionalFieldsForm = "deposit_organizationalUnit_additionalFieldsForm",
  deposit_organizationalUnit_subjectArea = "deposit_organizationalUnit_subjectArea",
  deposit_subjectAreas = "deposit_subjectArea",
  deposit_dataFile = "deposit_dataFile",
  deposit_duaDataFile = "deposit_duaDataFile",
  deposit_dataFile_metadataType = "deposit_dataFile_metadataType",
  deposit_collection = "deposit_collection",
  deposit_collection_statusHistory = "deposit_collection_statusHistory",
  deposit_statusHistory = "deposit_statusHistory",
  deposit_dataFile_statusHistory = "deposit_dataFile_statusHistory",
  deposit_archive = "deposit_archive",
  deposit_sip = "deposit_sip",
  deposit_authorizedOrganizationalUnit = "deposit_authorizedOrganizationalUnit",

  preservationSpace = "preservationSpace",
  preservationSpace_contributor = "preservationSpace_contributor",
  preservationSpace_contributor_deposit = "preservationSpace_contributor_deposit",
  preservationSpace_notification = "preservationSpace_notification",
  preservationSpace_notification_statusHistory = "preservationSpace_notification_statusHistory",
  preservationSpace_organizationalUnit = "preservationSpace_organizationalUnit",
  preservationSpace_organizationalUnit_personRole = "preservationSpace_organizationalUnit_personRole",
  preservationSpace_organizationalUnit_personInheritedRole = "preservationSpace_organizationalUnit_personInheritedRole",
  preservationSpace_organizationalUnit_archiveAcl = "preservationSpace_organizationalUnit_archiveAcl",
  preservationSpace_organizationalUnit_archiveAcl_upload = "preservationSpace_organizationalUnit_archiveAcl_upload",
  preservationSpace_organizationalUnit_submissionPolicy = "preservationSpace_organizationalUnit_submissionPolicy",
  preservationSpace_organizationalUnit_preservationPolicy = "preservationSpace_organizationalUnit_preservationPolicy",
  preservationSpace_organizationalUnit_disseminationPolicy = "preservationSpace_organizationalUnit_disseminationPolicy",
  preservationSpace_organizationalUnit_institution = "preservationSpace_organizationalUnit_institution",
  preservationSpace_organizationalUnit_subjectArea = "preservationSpace_organizationalUnit_subjectArea",
  preservationSpace_institution = "preservationSpace_institution",
  preservationSpace_institution_personRole = "preservationSpace_institution_personRole",
  preservationSpace_institution_organizationalUnit = "preservationSpace_institution_organizationalUnit",

  order = "order",
  order_allOrder = "order_allOrder",
  order_allOrder_statusHistory = "order_allOrder_statusHistory",
  order_allOrder_aip = "order_allOrder_aip",
  order_allOrder_dip = "order_allOrder_dip",
  order_allOrder_orderArchive = "order_allOrder_orderArchive",

  order_myOrder = "order_myOrder",
  order_myOrder_statusHistory = "order_myOrder_statusHistory",
  order_myOrder_aip = "order_myOrder_aip",
  order_myOrder_dip = "order_myOrder_dip",

  shared_archive = "shared_archive",
  shared_archiveAcl = "shared_archiveAcl",
  shared_archiveType = "shared_archiveType",
  shared_language = "shared_language",
  shared_notification = "shared_notification",
  shared_license = "shared_license",
  shared_deposit = "shared_deposit",
  shared_role = "shared_role",
  shared_user = "shared_user",
  shared_contributor = "shared_contributor",
  shared_order = "shared_order",
  shared_metadataType = "shared_metadataType",
  shared_institution = "shared_institution",
  shared_fundingAgency = "shared_fundingAgency",
  shared_person = "shared_person",
  shared_person_institution = "shared_person_institution",
  shared_organizationalUnit = "shared_organizationalUnit",
  shared_preservationPolicy = "shared_preservationPolicy",
  shared_submissionPolicy = "shared_submissionPolicy",
  shared_submissionAgreement = "shared_submissionAgreement",
  shared_disseminationPolicy = "shared_disseminationPolicy",
  shared_subjectArea = "shared_subjectArea",
  shared_organizationalUnit_submissionPolicy = "shared_organizationalUnit_submissionPolicy",
  shared_organizationalUnit_preservationPolicy = "shared_organizationalUnit_preservationPolicy",
  shared_organizationalUnit_disseminationPolicy = "shared_organizationalUnit_disseminationPolicy",
  shared_aip = "shared_aip",
  shared_aip_organizationalUnit = "shared_aip_organizationalUnit",
  shared_aip_statusHistory = "shared_aip_statusHistory",
  shared_aip_collection = "shared_aip_collection",
  shared_aip_collection_statusHistory = "shared_aip_collection_statusHistory",
  shared_aip_dataFile = "shared_aip_dataFile",
  shared_aip_dataFile_statusHistory = "shared_aip_dataFile_statusHistory",
  shared_data_category = "shared_data_category",
  shared_citation = "shared_citation",

  admin_preservationPolicy = "admin_preservationPolicy",
  admin_submissionAgreement = "admin_submissionAgreement",
  admin_submissionAgreementUser = "admin_submissionAgreementUser",
  admin_submissionPolicy = "admin_submissionPolicy",
  admin_scheduledTask = "admin_scheduledTask",
  admin_disseminationPolicy = "admin_disseminationPolicy",
  admin_archiveType = "admin_archiveType",
  admin_license = "admin_license",
  admin_organizationalUnit = "admin_organizationalUnit",
  admin_organizationalUnit_submissionPolicy = "admin_organizationalUnit_submissionPolicy",
  admin_organizationalUnit_preservationPolicy = "admin_organizationalUnit_preservationPolicy",
  admin_organizationalUnit_disseminationPolicy = "admin_organizationalUnit_disseminationPolicy",
  admin_organizationalUnit_personRole = "admin_organizationalUnit_personRole",
  admin_organizationalUnit_fundingAgency = "admin_organizationalUnit_fundingAgency",
  admin_organizationalUnit_institution = "admin_organizationalUnit_institution",
  admin_organizationalUnit_subjectArea = "admin_organizationalUnit_subjectArea",
  admin_organizationalUnit_additionalFieldsForm = "admin_organizationalUnit_additionalFieldsForm",
  admin_organizationalUnit_personInheritedRole = "admin_organizationalUnit_personInheritedRole",
  admin_fundingAgencies_organizationalUnit = "admin_fundingAgencies_organizationalUnit",
  admin_institution = "admin_institution",
  admin_institution_organizationalUnit = "admin_institution_organizationalUnit",
  admin_institution_personRole = "admin_institution_personRole",
  admin_subjectArea = "admin_subjectArea",
  admin_user = "admin_user",
  admin_person = "admin_person",
  admin_person_organizationalUnitRole = "admin_person_organizationalUnitRole",
  admin_person_institutionRole = "admin_person_institutionRole",
  admin_person_institutions = "admin_person_institutions",
  admin_person_organizationalUnit = "admin_person_organizational_unit",
  admin_role = "admin_role",
  admin_notification = "admin_notification",
  admin_notification_statusHistory = "admin_notification_statusHistory",
  admin_fundingAgencies = "admin_fundingAgencies",
  admin_archiveAcl = "admin_archiveAcl",
  admin_archiveAcl_upload = "admin_archiveAcl_upload",
  admin_language = "admin_language",
  admin_metadataType = "admin_metadataType",

  preservationPlanning = "preservationPlanning",
  preservationPlanning_monitoring = "preservationPlanning_monitoring",
  preservationPlanning_monitoring_webService = "preservationPlanning_monitoring_webService",
  preservationPlanning_aipStatus = "preservationPlanning_aipStatus",
  preservationPlanning_job = "preservationPlanning_job",
  preservationPlanning_job_execution = "preservationPlanning_job_execution",
  preservationPlanning_job_execution_report = "preservationPlanning_job_execution_report",
  preservationPlanning_job_execution_report_statusHistory = "preservationPlanning_job_execution_report_statusHistory",

  preservationPlanning_aipDownloaded = "preservationPlanning_aipDownloaded",

  preservationPlanning_sip = "preservationPlanning_sip",
  preservationPlanning_sip_statusHistory = "preservationPlanning_sip_statusHistory",
  preservationPlanning_sip_dataFile = "preservationPlanning_sip_dataFile",
  preservationPlanning_sip_dataFile_statusHistory = "preservationPlanning_sip_dataFile_statusHistory",
  preservationPlanning_sip_collection = "preservationPlanning_sip_collection",
  preservationPlanning_sip_collection_statusHistory = "preservationPlanning_sip_collection_statusHistory",

  preservationPlanning_dip = "preservationPlanning_dip",
  preservationPlanning_dip_statusHistory = "preservationPlanning_dip_statusHistory",
  preservationPlanning_dip_dataFile = "preservationPlanning_dip_dataFile",
  preservationPlanning_dip_dataFile_statusHistory = "preservationPlanning_dip_dataFile_statusHistory",
  preservationPlanning_dip_aip = "preservationPlanning_dip_aip",

  preservationPlanning_order = "preservationPlanning_order",
  preservationPlanning_order_statusHistory = "preservationPlanning_order_statusHistory",

  preservationPlanning_deposit = "preservationPlanning_deposit",
  preservationPlanning_deposit_statusHistory = "preservationPlanning_deposit_statusHistory",
}

export const StateEnum = {...StatePartialEnum, ...StateExtendEnum} as Omit<typeof StatePartialEnum, keyof typeof StateExtendEnum> & typeof StateExtendEnum;
export type StateEnum = typeof StateEnum[keyof typeof StateEnum];
