/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - tour.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

export enum TourEnum {
  commonContent = "common.content",

  mainMenuAvatar = "main.menu.avatar",
  mainMenuPreservationSpace = "main.menu.preservationSpace",
  mainMenuDeposit = "main.menu.deposit",
  mainMenuOrder = "main.menu.order",
  mainSearchBar = "main.searchBar",

  homeSearchSearchBar = "home.search.searchBar",
  homeSearchListTiles = "home.search.listTiles",
  homeSearchFacets = "home.search.facets",

  depositOrgUnitSelector = "deposit.orgUnitSelector",
  depositStatusTabs = "deposit.statusTabs",
  depositCreate = "deposit.create",

  depositDetail = "deposit.metadata.tab",
  depositMetadataThumbnail = "deposit.metadata.thumbnail",

  depositDataUpload = "deposit.data.upload",

  preservationSpaceTabs = "preservationSpace.tabs",

  preservationSpaceOrgUnitTab = "preservationSpace.orgUnit.tab",
  preservationSpaceOrgUnitThumbnail = "preservationSpace.orgUnit.thumbnail",
  preservationSpaceOrgUnitPersonRole = "preservationSpace.orgUnit.personRole",
}
