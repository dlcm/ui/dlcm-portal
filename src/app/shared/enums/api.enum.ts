/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - api.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ApiModuleEnum} from "@app/shared/enums/api-module.enum";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {environment} from "@environments/environment";
import {AipHelper} from "@shared/features/aip/helpers/aip.helper";
import {Storage} from "@shared/models/storage.model";
import {SOLIDIFY_CONSTANTS} from "solidify-frontend";
import {
  ApiActionNameEnum,
  ApiActionNameExtendEnum,
} from "./api-action-name.enum";

export class ApiKeyword {
  static PARENT_ID: string = "parentId";
  static SUB_RESOURCE: string = "subResource";
}

const SEPARATOR: string = SOLIDIFY_CONSTANTS.URL_SEPARATOR;
const subResource: string = `{${ApiKeyword.SUB_RESOURCE}}`;
const parentId: string = `{${ApiKeyword.PARENT_ID}}`;
const parentIdInUrl: string = SEPARATOR + parentId + SEPARATOR;
const search: string = "search";
const api: string = "api";
const apiInUrl: string = SEPARATOR + api + SEPARATOR;

export class ApiEnum {
  static get access(): string {
    return environment.access;
  }

  static get accessDip(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.DIP;
  }

  static get accessPublicMetadata(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.PUBLIC_METADATA;
  }

  static get accessPrivateMetadata(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.PRIVATE_METADATA;
  }

  static get accessOrders(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.ORDER;
  }

  static get accessOrganizationalUnits(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.ORG_UNIT;
  }

  static get accessAipDownloaded(): string {
    return ApiEnum.access + SEPARATOR + ApiResourceNameEnum.AIP;
  }

  static get oaiInfo(): string {
    return environment.oaiInfo;
  }

  static get oaiInfoOaiMetadataPrefixes(): string {
    return ApiEnum.oaiInfo + SEPARATOR + ApiResourceNameEnum.OAI_METADATA_PREFIXES;
  }

  static get oaiInfoOaiSets(): string {
    return ApiEnum.oaiInfo + SEPARATOR + ApiResourceNameEnum.OAI_SETS;
  }

  static get admin(): string {
    return environment.admin;
  }

  static get adminGlobalBanners(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.GLOBAL_BANNERS;
  }

  static get adminArchiveAcl(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.ARCHIVE_ACL;
  }

  static get adminArchiveType(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.ARCHIVE_TYPE;
  }

  static get adminFundingAgencies(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.FUNDING_AGENCY;
  }

  static get adminInstitutions(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.INSTITUTION;
  }

  static get adminLanguages(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.LANGUAGE;
  }

  static get adminLicenses(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.LICENSE;
  }

  static get adminNotifications(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.NOTIFICATION;
  }

  static get adminNotificationsInbox(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.NOTIFICATION + SEPARATOR + ApiActionNameEnum.INBOX;
  }

  static get adminOrcid(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.ORCID;
  }

  static get adminOrcidSynchronizations(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.ORCID_SYNCHRONIZATION;
  }

  static get adminOrganizationalUnits(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.ORG_UNIT;
  }

  static get adminAuthorizedOrganizationalUnits(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.AUTHORIZED_ORG_UNIT;
  }

  static get adminAuthorizedInstitutions(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.AUTHORIZED_INSTITUTION;
  }

  static get adminPeople(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.PERSON;
  }

  static get adminMembers(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.MEMBER;
  }

  static get adminPreservationPolicies(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.PRES_POLICY;
  }

  static get adminRoles(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.ROLE;
  }

  static get adminSubmissionPolicies(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.SUB_POLICY;
  }

  static get adminSubmissionAgreements(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.SUBMISSION_AGREEMENT;
  }

  static get adminDisseminationPolicies(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.DISSEMINATION_POLICY;
  }

  static get adminSubjectAreas(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.SUBJECT_AREA;
  }

  static get adminUsers(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.USER;
  }

  static get adminMetadataTypes(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.METADATA_TYPE;
  }

  static get adminSystemProperties(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.SYSTEM_PROPERTY;
  }

  static get adminScheduledTask(): string {
    return ApiEnum.admin + SEPARATOR + ApiResourceNameEnum.SCHEDULED_TASK;
  }

  static get adminDocs(): string {
    const urlAdmin = environment.admin.substring(0, environment.admin.lastIndexOf("/"));
    return urlAdmin + SEPARATOR + ApiModuleEnum.DOCS;
  }

  static get adminResourceSrv(): string {
    return environment.admin + SEPARATOR + ApiModuleEnum.RES_SRV;
  }

  static get adminPreservationPlanning(): string {
    const urlAdmin = environment.admin.substring(0, environment.admin.lastIndexOf("/"));
    return urlAdmin + SEPARATOR + ApiModuleEnum.PRES_PLANNING;
  }

  static get preservationPlanning(): string {
    return ApiEnum.adminPreservationPlanning;
  }

  static get preservationPlanningModule(): string {
    return ApiEnum.preservationPlanning + SEPARATOR + ApiResourceNameEnum.MODULES;
  }

  static get preservationPlanningPreservationJobs(): string {
    return ApiEnum.adminPreservationPlanning + SEPARATOR + ApiResourceNameEnum.PRES_JOB;
  }

  static get preservationPlanningMonitor(): string {
    return ApiEnum.adminPreservationPlanning + SEPARATOR + ApiResourceNameEnum.MONITOR;
  }

  static get preservationPlanningPreservationAipStatuses(): string {
    return ApiEnum.adminPreservationPlanning + SEPARATOR + ApiResourceNameEnum.AIP;
  }

  static get ingest(): string {
    return environment.ingest;
  }

  static get ingestSip(): string {
    return ApiEnum.ingest + SEPARATOR + ApiResourceNameEnum.SIP;
  }

  static get ingestSipDataFile(): string {
    return ApiEnum.ingest + SEPARATOR + ApiResourceNameEnum.SIP + parentIdInUrl + ApiResourceNameEnum.DATAFILE;
  }

  static get ingestDipDataFile(): string {
    return ApiEnum.ingest + SEPARATOR + ApiResourceNameEnum.DIP + parentIdInUrl + ApiResourceNameEnum.DATAFILE;
  }

  static get preingest(): string {
    return environment.preingest;
  }

  static get preIngestDeposits(): string {
    return ApiEnum.preingest + SEPARATOR + ApiResourceNameEnum.DEPOSIT;
  }

  static get preIngestDepositDataFile(): string {
    return ApiEnum.preingest + SEPARATOR + ApiResourceNameEnum.DEPOSIT + parentIdInUrl + ApiResourceNameEnum.DATAFILE;
  }

  static get preIngestContributors(): string {
    return ApiEnum.preingest + SEPARATOR + ApiResourceNameEnum.CONTRIBUTOR;
  }

  static get preIngestDepositsDataCategory(): string {
    return ApiEnum.preingest + SEPARATOR + ApiResourceNameEnum.DEPOSIT + SEPARATOR + ApiActionNameExtendEnum.LIST_DATA_CATEGORY;
  }

  static get preIngestSubjectArea(): string {
    return ApiEnum.preingest + SEPARATOR + ApiResourceNameEnum.SUBJECT_AREA;
  }

  static get dataMgmt(): string {
    return environment.dataManagement;
  }

  static get dataManagementResource(): string {
    return ApiEnum.dataMgmt;
  }

  static get index(): string {
    return environment.index;
  }

  static get indexResourceIndexFieldAliases(): string {
    return ApiEnum.index + SEPARATOR + ApiResourceNameEnum.INDEX_FIELD_ALIASES;
  }

  static get archivalStorageDefaultUrl(): string {
    return this.archivalStorageDefault.url;
  }

  static get archivalStorageDefault(): Storage {
    const defaultIndex = environment.defaultStorageIndex;
    if (this.archivalStorageList.length < defaultIndex) {
      return this.archivalStorageList[0];
    }
    return this.archivalStorageList[defaultIndex];
  }

  static get archivalStorageList(): Storage[] {
    const newList: Storage[] = [];
    let index = 1;
    environment.archivalStorage.forEach(url => {
      newList.push({
        index: index,
        url: url,
        name: AipHelper.getStoragionNameByIndex(index),
      });
      index++;
    });
    return newList;
  }

  static get archivalStorage(): string {
    return ApiEnum.archivalStorageDefaultUrl;
  }

  static get archivalStorageAip(): string {
    return ApiEnum.archivalStorageDefaultUrl + SEPARATOR + ApiResourceNameEnum.AIP;
  }

  static get archivalStorageAipStorages(): Storage[] {
    const archivalStorageList = ApiEnum.archivalStorageList;
    const newList: Storage[] = [];
    archivalStorageList.forEach(url => {
      newList.push({
        index: url.index,
        url: url.url + SEPARATOR + ApiResourceNameEnum.AIP,
        name: AipHelper.getStoragionNameByIndex(url.index),
      });
    });
    return newList;
  }
}
