/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - api-action-name.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ApiActionNamePartialEnum} from "solidify-frontend";

export enum ApiActionNameExtendEnum {
  LIST_AIP_CONTAINER = "list-aip-containers",
  LIST_ACCESS_TYPE = "list-accesses",
  LIST_DATA_CATEGORY = "list-data-categories",
  LIST_DATA_TYPE = "list-data-types",
  LIST_QUERY_TYPE = "list-query-types",
  LIST_JOB_TYPE = "list-job-types",
  LIST_JOB_RECURRENCE = "list-job-recurrences",
  LIST_OAUTH2_GRANT_TYPE = "list-grant-types",
  LIST_SUBJECT_AREA_SOURCES = "list-sources",
  LIST_STATUS = "list-status",
  LIST_FOLDERS = "list-folders",
  LIST_EXCLUDE_FILES = "list-exclude-files",
  LIST_IGNORE_FILES = "list-ignore-files",

  ARCHIVAL_UNITS = "archivalUnits",
  ARCHIVAL_COLLECTIONS = "archivalCollections",

  ORDER_LIST_CREATED_BY_USER = "list-created-by-user",
  ORDER_LIST_PUBLIC = "list-public",

  REINDEX = "reindex",
  SET_ROLE = "set-role",
  CHECK = "check",
  CHECK_COMPLIANCE = "check-compliance",
  CHECK_FIXITY = "check-fixity",
  SEARCH_DOI = "search-doi",
  SEARCH_WITH_USER = "search-with-user",
  APPROVE_SUBMISSION_AGREEMENT = "approve-submission-agreement",
  APPROVE_DISPOSAL = "approve-disposal",
  APPROVE_DISPOSAL_BY_ORGUNIT = "approve-disposal-by-orgunit",
  CHECK_SUBMISSION_AGREEMENT = "check-submission-agreement",
  EXTEND_RETENTION = "extend-retention",
  UPDATE_COMPLIANCE = "start-compliance-level-update",

  SELF = "self",
  MODULE = "module",
  PARENT = "parent",
  STATUS = "status",
  DASHBOARD = "dashboard",
  RESOURCES = "resources",
  LIST = "list",
  FILE = "file",
  COUNT = "count",
  PREVIOUS = "previous",
  NEXT = "next",
  CREATE = "new",
  READ = "get",
  UPDATE = "edit",
  DELETE = "remove",
  VALUES = "values",
  SEARCH = "search",
  LASTCREATED = "lastCreated",
  LASTUPDATE = "lastUpdated",
  RANGE = "searchRange",
  SIZE = "size",
  PAGE = "page",
  SORT = "sort",
  CREATION = "creation.when",
  UPDATED = "lastUpdate.when",
  ERROR = "error",
  UL = "upload",
  UL_ARCHIVE = "upload-archive",
  DL = "download",
  HISTORY = "history",
  RESUME = "resume",
  START = "start",
  ALL = "All",
  VIEW = "view",
  SAVE = "save",
  IMPORT = "import",
  IMPORTED = "imported",
  CLOSE = "close",
  DELETE_CACHE = "delete-cache",
  DOWNLOAD_STATUS = "download-status",
  LIST_CURRENT_STATUS = "list-current-status",
  PREPARE_DOWNLOAD = "prepare-download",
  RESERVE_DOI = "reserve-doi",
  DELETE_FOLDER = "delete-folder",
  RESUBMIT = "resubmit",
  PUT_IN_ERROR = "put-in-error",
  FIX_AIP_INFO = "fix-aip-info",
  DOWNLOAD_FILE = "download-file",
  UPLOAD_FILE = "upload-file",
  DELETE_FILE = "delete-file",

  LIST_AIP_WITH_STATUS = "list-aip-statuses",
  DL_STATUS = "download-status",
  LIST_DL_STATUS = "list-download-status",

  AUTHORIZE = "authorize",
  TOKEN = "token",
  CHECK_TOKEN = "check_token",
  REVOKE_ALL_TOKENS = "revoke-all-tokens",
  REVOKE_MY_TOKENS = "revoke-my-tokens",
  USER_ID = "externalUid",
  AUTHENTICATED = "authenticated",
  CLEAN = "clean",

  SUBMIT = "submit",
  SUBMIT_FOR_APPROVAL = "submit-for-approval",
  APPROVE = "approve",
  REJECT = "reject",
  ENABLE_REVISION = "enable-revision",
  INIT = "initialize",
  REPORT = "reports",
  DETAIL = "detail",
  VALIDATE = "validate",
  SENT = "sent",
  INBOX = "inbox",
  START_METADATA_EDITING = "start-metadata-editing",
  SET_APPROVED = "set-approved",
  SET_REFUSED = "set-refused",
  SET_PENDING = "set-pending",
  SET_READ = "set-read",
  SET_UNREAD = "set-unread",
  CANCEL_METADATA_EDITING = "cancel-metadata-editing",
  REFRESH = "refresh",
  SYNCHRONIZE = "synchronize",
  UPLOAD_LOGO = "upload-logo",
  DOWNLOAD_LOGO = "download-logo",
  DELETE_LOGO = "delete-logo",
  UPLOAD_AVATAR = "upload-avatar",
  DOWNLOAD_AVATAR = "download-avatar",
  DELETE_AVATAR = "delete-avatar",
  UPLOAD_THUMBNAIL = "upload-thumbnail",
  DOWNLOAD_THUMBNAIL = "download-thumbnail",
  DELETE_THUMBNAIL = "delete-thumbnail",
  THUMBNAIL = "thumbnail",
  DUA = "dua",
  README = "readme",
  STATISTICS = "statistics",
  RATING = "ratings",
  RATING_BY_USER = "list-for-user",
  BY_RATING = "by-rating",
  BIBLIOGRAPHIES = "bibliographies",
  CITATIONS = "citations",
  GET_MY_ACLS = "get-my-acls",
  LIST_REFERENCE_TYPES = "list-reference-types",

  GET_ACTIVE = "get-active",
  LIST_INHERITED_PERSON_ROLES = "list-inherited-person-roles",
  SCHEDULED_ENABLED_TASK = "schedule-enabled-tasks",
  DISABLE_TASKS_SCHEDULING = "disable-tasks-scheduling",
  KILL_TASK = "kill-task",
  LIST_ALL_ARCHIVE_ACL = "list-all-archive-acl",
  CREATE_FROM_NOTIFICATION = "create-from-notification",
  GET_MY_APPROBATIONS = "get-my-approbations",
  GET_ALL_APPROBATIONS = "get-all-approbations",
  GET_ANONYMIZED_DEPOSIT_PAGE = "get-anonymized-deposit-page",
  GENERATE_ANONYMIZED_DEPOSIT_PAGE = "generate-anonymized-deposit-page",
  UPLOAD_SUBMISSION_AGREEMENT = "upload-submission-agreement",
  EXPORT_TO_ORCID = "export-to-orcid",
  READY_FOR_DOWNLOAD = "ready-for-download"
}

export const ApiActionNameEnum = {...ApiActionNamePartialEnum, ...ApiActionNameExtendEnum} as Omit<typeof ApiActionNamePartialEnum, keyof typeof ApiActionNameExtendEnum> & typeof ApiActionNameExtendEnum;
export type ApiActionNameEnum = typeof ApiActionNameEnum[keyof typeof ApiActionNameEnum];
