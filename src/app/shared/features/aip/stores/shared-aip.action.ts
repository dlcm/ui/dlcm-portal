/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Aip} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  ResourceAction,
  ResourceNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.shared_aip;

export namespace SharedAipAction {
  @TypeDefaultAction(state)
  export class LoadResource extends ResourceAction.LoadResource {
  }

  @TypeDefaultAction(state)
  export class LoadResourceSuccess extends ResourceAction.LoadResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class LoadResourceFail extends ResourceAction.LoadResourceFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends ResourceAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends ResourceAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends ResourceAction.GetAllSuccess<Aip> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends ResourceAction.GetAllFail<Aip> {
  }

  @TypeDefaultAction(state)
  export class GetByListId extends ResourceAction.GetByListId {
  }

  @TypeDefaultAction(state)
  export class GetByListIdSuccess extends ResourceAction.GetByListIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetByListIdFail extends ResourceAction.GetByListIdFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends ResourceAction.GetById {
    constructor(public id: string, public keepCurrentContext: boolean = false, public addInListTemp: boolean = false, public storagionNumber: number | undefined = undefined, public url: string | undefined = undefined) {
      super(id, keepCurrentContext, addInListTemp);
    }
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends ResourceAction.GetByIdSuccess<Aip> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends ResourceAction.GetByIdFail<Aip> {
  }

  @TypeDefaultAction(state)
  export class Create extends ResourceAction.Create<Aip> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends ResourceAction.CreateSuccess<Aip> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends ResourceAction.CreateFail<Aip> {
  }

  @TypeDefaultAction(state)
  export class Update extends ResourceAction.Update<Aip> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends ResourceAction.UpdateSuccess<Aip> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends ResourceAction.UpdateFail<Aip> {
  }

  @TypeDefaultAction(state)
  export class Delete extends ResourceAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends ResourceAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends ResourceAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends ResourceAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends ResourceAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends ResourceAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class AddInList extends ResourceAction.AddInList<Aip> {
  }

  @TypeDefaultAction(state)
  export class AddInListById extends ResourceAction.AddInListById {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdSuccess extends ResourceAction.AddInListByIdSuccess<Aip> {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdFail extends ResourceAction.AddInListByIdFail<Aip> {
  }

  @TypeDefaultAction(state)
  export class RemoveInListById extends ResourceAction.RemoveInListById {
  }

  @TypeDefaultAction(state)
  export class RemoveInListByListId extends ResourceAction.RemoveInListByListId {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkList extends ResourceAction.LoadNextChunkList {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListSuccess extends ResourceAction.LoadNextChunkListSuccess<Aip> {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListFail extends ResourceAction.LoadNextChunkListFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends ResourceAction.Clean {
  }

  export class Download {
    static readonly type: string = `[${state}] Download`;

    constructor(public id: string) {
    }
  }

  export class SimpleChecksum extends BaseAction {
    static readonly type: string = `[${state}] Simple Checksums`;

    constructor(public id: string, public viewMode: ViewModeEnum | undefined) {
      super();
    }
  }

  export class SimpleChecksumSuccess extends BaseSubActionSuccess<SimpleChecksum> {
    static readonly type: string = `[${state}] SimpleChecksum Success`;
  }

  export class SimpleChecksumFail extends BaseSubActionFail<SimpleChecksum> {
    static readonly type: string = `[${state}] SimpleChecksum Fail`;
  }

  export class DeepChecksum extends BaseAction {
    static readonly type: string = `[${state}] Deep Checksums`;

    constructor(public id: string, public viewMode: ViewModeEnum | undefined) {
      super();
    }
  }

  export class DeepChecksumSuccess extends BaseSubActionSuccess<DeepChecksum> {
    static readonly type: string = `[${state}] DeepChecksum Success`;
  }

  export class DeepChecksumFail extends BaseSubActionFail<DeepChecksum> {
    static readonly type: string = `[${state}] DeepChecksum Fail`;
  }

  export class Reindex extends BaseAction {
    static readonly type: string = `[${state}] Reindex`;

    constructor(public id: string, public viewMode: ViewModeEnum | undefined) {
      super();
    }
  }

  export class ReindexSuccess extends BaseSubActionSuccess<Reindex> {
    static readonly type: string = `[${state}] Reindex Success`;
  }

  export class ReindexFail extends BaseSubActionFail<Reindex> {
    static readonly type: string = `[${state}] Reindex Fail`;
  }

  export class UpdateCompliance extends BaseAction {
    static readonly type: string = `[${state}] Update Compliance`;

    constructor(public id: string, public viewMode: ViewModeEnum | undefined) {
      super();
    }
  }

  export class UpdateComplianceSuccess extends BaseSubActionSuccess<UpdateCompliance> {
    static readonly type: string = `[${state}] Update Compliance Success`;
  }

  export class UpdateComplianceFail extends BaseSubActionFail<UpdateCompliance> {
    static readonly type: string = `[${state}] Update Compliance Fail`;
  }

  export class Check extends BaseAction {
    static readonly type: string = `[${state}] Check`;

    constructor(public id: string, public viewMode: ViewModeEnum | undefined) {
      super();
    }
  }

  export class CheckSuccess extends BaseSubActionSuccess<Check> {
    static readonly type: string = `[${state}] Check Success`;
  }

  export class CheckFail extends BaseSubActionFail<Check> {
    static readonly type: string = `[${state}] Check Fail`;
  }

  export class Dispose extends BaseAction {
    static readonly type: string = `[${state}] Dispose`;

    constructor(public id: string, public viewMode: ViewModeEnum | undefined) {
      super();
    }
  }

  export class DisposeSuccess extends BaseSubActionSuccess<Dispose> {
    static readonly type: string = `[${state}] Dispose Success`;
  }

  export class DisposeFail extends BaseSubActionFail<Dispose> {
    static readonly type: string = `[${state}] Dispose Fail`;
  }

  export class ApproveDisposal extends BaseAction {
    static readonly type: string = `[${state}] Approve Disposal`;

    constructor(public id: string, public reason: string) {
      super();
    }
  }

  export class ApproveDisposalSuccess extends BaseSubActionSuccess<ApproveDisposal> {
    static readonly type: string = `[${state}] Approve Disposal Success`;
  }

  export class ApproveDisposalFail extends BaseSubActionFail<ApproveDisposal> {
    static readonly type: string = `[${state}] Approve Disposal Fail`;
  }

  export class ApproveDisposalByOrgunit extends BaseAction {
    static readonly type: string = `[${state}] Approve Disposal By Orgunit`;

    constructor(public id: string, public reason: string) {
      super();
    }
  }

  export class ApproveDisposalByOrgunitSuccess extends BaseSubActionSuccess<ApproveDisposalByOrgunit> {
    static readonly type: string = `[${state}] Approve Disposal By Orgunit Success`;
  }

  export class ApproveDisposalByOrgunitFail extends BaseSubActionFail<ApproveDisposalByOrgunit> {
    static readonly type: string = `[${state}] Approve Disposal By Orgunit Fail`;
  }

  export class ExtendRetention extends BaseAction {
    static readonly type: string = `[${state}] Extend Retention`;

    constructor(public id: string, public duration: number) {
      super();
    }
  }

  export class ExtendRetentionSuccess extends BaseSubActionSuccess<ExtendRetention> {
    static readonly type: string = `[${state}] Extend Retention Success`;
  }

  export class ExtendRetentionFail extends BaseSubActionFail<ExtendRetention> {
    static readonly type: string = `[${state}] Extend Retention Fail`;
  }

  export class Resume extends BaseAction {
    static readonly type: string = `[${state}] Resume`;

    constructor(public id: string, public viewMode: ViewModeEnum | undefined) {
      super();
    }
  }

  export class ResumeSuccess extends BaseSubActionSuccess<Resume> {
    static readonly type: string = `[${state}] Resume Success`;
  }

  export class ResumeFail extends BaseSubActionFail<Resume> {
    static readonly type: string = `[${state}] Resume Fail`;
  }

  export class GoToAip extends BaseAction {
    static readonly type: string = `[${state}] GoToAip`;

    constructor(public aip: Aip, public storagion_number: string) {
      super();
    }
  }

  export class FixAipInfo extends BaseAction {
    static readonly type: string = `[${state}] Fix Aip Info`;

    constructor(public id: string, public viewMode: ViewModeEnum | undefined) {
      super();
    }
  }

  export class FixAipInfoSuccess extends BaseSubActionSuccess<FixAipInfo> {
    static readonly type: string = `[${state}] Fix Aip Info Success`;
  }

  export class FixAipInfoFail extends BaseSubActionFail<FixAipInfo> {
    static readonly type: string = `[${state}] Fix Aip Info Fail`;
  }

  export class PutInError extends BaseAction {
    static readonly type: string = `[${state}] Put In Error`;

    constructor(public id: string, public viewMode: ViewModeEnum | undefined) {
      super();
    }
  }

  export class PutInErrorSuccess extends BaseSubActionSuccess<PutInError> {
    static readonly type: string = `[${state}] Put In Error Success`;
  }

  export class PutInErrorFail extends BaseSubActionFail<PutInError> {
    static readonly type: string = `[${state}] Put In Error Fail`;
  }

  export class BulkSimpleChecksum extends BaseAction {
    static readonly type: string = `[${state}] Bulk Simple Checksum`;

    constructor(public listAip: Aip[]) {
      super();
    }
  }

  export class BulkSimpleChecksumSuccess extends BaseSubActionSuccess<BulkSimpleChecksum> {
    static readonly type: string = `[${state}] Bulk Simple Checksum Success`;
  }

  export class BulkSimpleChecksumFail extends BaseSubActionFail<BulkSimpleChecksum> {
    static readonly type: string = `[${state}] Bulk Simple Checksum Fail`;
  }

  export class BulkDeepChecksum extends BaseAction {
    static readonly type: string = `[${state}] Bulk Deep Checksum`;

    constructor(public listAip: Aip[]) {
      super();
    }
  }

  export class BulkDeepChecksumSuccess extends BaseSubActionSuccess<BulkDeepChecksum> {
    static readonly type: string = `[${state}] Bulk Deep Checksum Success`;
  }

  export class BulkDeepChecksumFail extends BaseSubActionFail<BulkDeepChecksum> {
    static readonly type: string = `[${state}] Bulk Deep Checksum Fail`;
  }

  export class BulkReindex extends BaseAction {
    static readonly type: string = `[${state}] Bulk Reindex`;

    constructor(public listAip: Aip[]) {
      super();
    }
  }

  export class BulkReindexSuccess extends BaseSubActionSuccess<BulkReindex> {
    static readonly type: string = `[${state}] Bulk Reindex Success`;
  }

  export class BulkReindexFail extends BaseSubActionFail<BulkReindex> {
    static readonly type: string = `[${state}] Bulk Reindex Fail`;
  }

  export class BulkUpdateCompliance extends BaseAction {
    static readonly type: string = `[${state}] Bulk Update Compliance`;

    constructor(public listAip: Aip[]) {
      super();
    }
  }

  export class BulkUpdateComplianceSuccess extends BaseSubActionSuccess<BulkUpdateCompliance> {
    static readonly type: string = `[${state}] Bulk Update Compliance Success`;
  }

  export class BulkUpdateComplianceFail extends BaseSubActionFail<BulkUpdateCompliance> {
    static readonly type: string = `[${state}] Bulk Update Compliance Fail`;
  }

  export class BulkCheck extends BaseAction {
    static readonly type: string = `[${state}] Bulk Check`;

    constructor(public listAip: Aip[]) {
      super();
    }
  }

  export class BulkCheckSuccess extends BaseSubActionSuccess<BulkCheck> {
    static readonly type: string = `[${state}] Bulk Check Success`;
  }

  export class BulkCheckFail extends BaseSubActionFail<BulkCheck> {
    static readonly type: string = `[${state}] Bulk Check Fail`;
  }

  export class BulkDispose extends BaseAction {
    static readonly type: string = `[${state}] Bulk Dispose`;

    constructor(public listAip: Aip[]) {
      super();
    }
  }

  export class BulkDisposeSuccess extends BaseSubActionSuccess<BulkDispose> {
    static readonly type: string = `[${state}] Bulk Dispose Success`;
  }

  export class BulkDisposeFail extends BaseSubActionFail<BulkDispose> {
    static readonly type: string = `[${state}] Bulk Dispose Fail`;
  }

  export class BulkResume extends BaseAction {
    static readonly type: string = `[${state}] Bulk Resume`;

    constructor(public listAip: Aip[]) {
      super();
    }
  }

  export class BulkResumeSuccess extends BaseSubActionSuccess<BulkResume> {
    static readonly type: string = `[${state}] Bulk Resume Success`;
  }

  export class BulkResumeFail extends BaseSubActionFail<BulkResume> {
    static readonly type: string = `[${state}] Bulk Resume Fail`;
  }

  export class BulkPutInError extends BaseAction {
    static readonly type: string = `[${state}] Bulk Put In Error`;

    constructor(public listAip: Aip[]) {
      super();
    }
  }

  export class BulkPutInErrorSuccess extends BaseSubActionSuccess<BulkPutInError> {
    static readonly type: string = `[${state}] Bulk Put In Error Success`;
  }

  export class BulkPutInErrorFail extends BaseSubActionFail<BulkPutInError> {
    static readonly type: string = `[${state}] Bulk Put In Error Fail`;
  }
}

export const sharedAipActionNameSpace: ResourceNameSpace = SharedAipAction;
