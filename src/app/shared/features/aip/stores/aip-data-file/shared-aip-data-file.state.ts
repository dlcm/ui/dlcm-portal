/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-data-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {AipDataFile} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {SharedAipDataFileStatusHistoryStateModel} from "@preservation-planning/sip/stores/data-file/status-history/preservation-planning-sip-data-file-status-history.state";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {AipHelper} from "@shared/features/aip/helpers/aip.helper";
import {
  SharedAipDataFileAction,
  sharedAipDataFileActionNameSpace,
} from "@shared/features/aip/stores/aip-data-file/shared-aip-data-file.action";
import {SharedAipDataFileStatusHistoryState} from "@shared/features/aip/stores/aip-data-file/status-history/shared-aip-data-file-status-history.state";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  DownloadService,
  NotificationService,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";
import {ApiActionNameEnum} from "../../../../enums/api-action-name.enum";

export const defaultSharedAipDataFileValue: () => SharedAipDataFileStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    [StateEnum.shared_aip_dataFile_statusHistory]: defaultStatusHistoryInitValue(),
  });

export interface SharedAipDataFileStateModel extends CompositionStateModel<AipDataFile> {
  [StateEnum.shared_aip_dataFile_statusHistory]: SharedAipDataFileStatusHistoryStateModel;
}

@Injectable()
@State<SharedAipDataFileStateModel>({
  name: StateEnum.shared_aip_dataFile,
  defaults: {
    ...defaultSharedAipDataFileValue(),
  },
  children: [
    SharedAipDataFileStatusHistoryState,
  ],
})
export class SharedAipDataFileState extends CompositionState<SharedAipDataFileStateModel, AipDataFile> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedAipDataFileActionNameSpace,
      resourceName: ApiResourceNameEnum.DATAFILE,
    });
  }

  protected get _urlResource(): string {
    return AipHelper.generateUrlResource(this._store);
  }

  @Action(SharedAipDataFileAction.Refresh)
  refresh(ctx: SolidifyStateContext<SharedAipDataFileStateModel>, action: SharedAipDataFileAction.Refresh): void {
    ctx.dispatch(new SharedAipDataFileAction.GetAll(action.parentId, undefined, true));
  }

  @Action(SharedAipDataFileAction.Download)
  download(ctx: SolidifyStateContext<SharedAipDataFileStateModel>, action: SharedAipDataFileAction.Download): void {
    const url = `${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFile.resId}/${ApiActionNameEnum.DL}`;
    this._downloadService.download(url, action.dataFile.fileName, action.dataFile.fileSize);
  }

  @Action(SharedAipDataFileAction.Resume)
  resume(ctx: SolidifyStateContext<SharedAipDataFileStateModel>, action: SharedAipDataFileAction.Resume): Observable<Result> {
    return this._apiService.post<any, Result>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFile.resId}/${ApiActionNameEnum.RESUME}`)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            this._notificationService.showInformation(LabelTranslateEnum.resourceResumed);
            ctx.dispatch(new SharedAipDataFileAction.GetAll(action.parentId, undefined, true));
          } else {
            this._notificationService.showInformation(LabelTranslateEnum.unableResumedResource);
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          this._notificationService.showInformation(LabelTranslateEnum.unableResumedResource);
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedAipDataFileAction.PutInError)
  putInError(ctx: SolidifyStateContext<SharedAipDataFileStateModel>, action: SharedAipDataFileAction.PutInError): Observable<Result> {
    return this._apiService.post<any, Result>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFile.resId}/${ApiActionNameEnum.PUT_IN_ERROR}`)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            this._notificationService.showInformation(LabelTranslateEnum.resourceInError);
            ctx.dispatch(new SharedAipDataFileAction.GetAll(action.parentId, undefined, true));
          } else {
            this._notificationService.showInformation(LabelTranslateEnum.unableInErrorResource);
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          this._notificationService.showInformation(LabelTranslateEnum.unableInErrorResource);
          throw new SolidifyStateError(this, error);
        }),
      );
  }
}
