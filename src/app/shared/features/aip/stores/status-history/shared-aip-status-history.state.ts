/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-status-history.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {Aip} from "@models";

import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {AipHelper} from "@shared/features/aip/helpers/aip.helper";
import {sharedAipStatusHistoryNamespace} from "@shared/features/aip/stores/status-history/shared-aip-status-history.action";
import {
  defaultStatusHistoryInitValue,
  StatusHistoryState,
  StatusHistoryStateModel,
} from "@shared/stores/status-history/status-history.state";
import {
  ApiService,
  NotificationService,
} from "solidify-frontend";

export interface SharedAipStatusHistoryStateModel extends StatusHistoryStateModel<Aip> {
}

@Injectable()
@State<SharedAipStatusHistoryStateModel>({
  name: StateEnum.shared_aip_statusHistory,
  defaults: {
    ...defaultStatusHistoryInitValue(),
  },
})
export class SharedAipStatusHistoryState extends StatusHistoryState<SharedAipStatusHistoryStateModel, Aip> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: sharedAipStatusHistoryNamespace,
    });
  }

  protected get _urlResource(): string {
    return AipHelper.generateUrlResource(this._store);
  }
}
