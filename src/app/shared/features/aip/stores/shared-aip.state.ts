/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {environment} from "@environments/environment";
import {Aip} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {
  AppRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {AipHelper} from "@shared/features/aip/helpers/aip.helper";
import {
  defaultSharedAipCollectionValue,
  SharedAipCollectionState,
  SharedAipCollectionStateModel,
} from "@shared/features/aip/stores/aip-collection/shared-aip-collection.state";
import {
  defaultSharedAipDataFileValue,
  SharedAipDataFileState,
  SharedAipDataFileStateModel,
} from "@shared/features/aip/stores/aip-data-file/shared-aip-data-file.state";
import {
  defaultSharedAipOrgUnitValue,
  SharedAipOrganizationalUnitState,
  SharedAipOrganizationalUnitStateModel,
} from "@shared/features/aip/stores/organizational-unit/shared-aip-organizational-unit.state";
import {
  SharedAipAction,
  sharedAipActionNameSpace,
} from "@shared/features/aip/stores/shared-aip.action";
import {
  SharedAipStatusHistoryState,
  SharedAipStatusHistoryStateModel,
} from "@shared/features/aip/stores/status-history/shared-aip-status-history.state";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  defaultResourceStateInitValue,
  DownloadService,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  ResourceState,
  ResourceStateModel,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {ApiActionNameEnum} from "../../../enums/api-action-name.enum";

export interface SharedAipStateModel extends ResourceStateModel<Aip> {
  [StateEnum.shared_aip_dataFile]: SharedAipDataFileStateModel;
  [StateEnum.shared_aip_organizationalUnit]: SharedAipOrganizationalUnitStateModel;
  [StateEnum.shared_aip_collection]: SharedAipCollectionStateModel;
  preservationPlanning_aip_statusHistory: SharedAipStatusHistoryStateModel;
}

@Injectable()
@State<SharedAipStateModel>({
  name: StateEnum.shared_aip,
  defaults: {
    ...defaultResourceStateInitValue(),
    [StateEnum.shared_aip_dataFile]: defaultSharedAipDataFileValue(),
    [StateEnum.shared_aip_organizationalUnit]: defaultSharedAipOrgUnitValue(),
    [StateEnum.shared_aip_collection]: defaultSharedAipCollectionValue(),
    preservationPlanning_aip_statusHistory: {...defaultStatusHistoryInitValue()},
  },
  children: [
    SharedAipOrganizationalUnitState,
    SharedAipCollectionState,
    SharedAipStatusHistoryState,
    SharedAipDataFileState,
  ],
})
export class SharedAipState extends ResourceState<SharedAipStateModel, Aip> {
  private readonly _KEY_QUERY_PARAM_REASON: string = "reason";
  private readonly _KEY_QUERY_PARAM_DURATION: string = "duration";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _router: Router,
              private readonly _route: ActivatedRoute,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedAipActionNameSpace,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.preservationPlanningAipDownloaded,
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("aipDownloaded.notification.resource.delete.success"),
      notificationResourceDeleteFailTextToTranslate: MARK_AS_TRANSLATABLE("aipDownloaded.notification.resource.delete.fail"),
    });
  }

  protected get _urlResource(): string {
    return AipHelper.generateUrlResource(this._store);
  }

  @Selector()
  static currentAipName(state: SharedAipStateModel): string | undefined {
    return state.current?.info?.name;
  }

  @Selector()
  static isLoadingWithDependency(state: SharedAipStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: SharedAipStateModel): boolean {
    return !isNullOrUndefined(state.current);
  }

  @Selector()
  static isLoading(state: SharedAipStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Action(SharedAipAction.Download)
  download(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.Download): void {
    const fileName = "aip_" + action.id + ".zip";
    this._downloadService.download(`${this._urlResource}/${action.id}/${ApiActionNameEnum.DL}`, fileName);
  }

  @OverrideDefaultAction()
  @Action(SharedAipAction.GetById)
  getById(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.GetById): Observable<Aip> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });

    const url = AipHelper.generateUrlResource(this._store, action.storagionNumber, action.url);
    return this._apiService.getById<Aip>(url, action.id)
      .pipe(
        tap((model: Aip) => {
          ctx.dispatch(new SharedAipAction.GetByIdSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.GetByIdFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedAipAction.Reindex)
  reindex(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.Reindex): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.REINDEX}`)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new SharedAipAction.ReindexSuccess(action));
          } else {
            ctx.dispatch(new SharedAipAction.ReindexFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.ReindexFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.ReindexSuccess)
  reindexSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ReindexSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.reindex.success"));
    this._refreshView(ctx, action.parentAction.id, action.parentAction.viewMode);
  }

  @Action(SharedAipAction.ReindexFail)
  reindexFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ReindexFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.reindex.fail"));
  }

  @Action(SharedAipAction.UpdateCompliance)
  updateCompliance(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.UpdateCompliance): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.UPDATE_COMPLIANCE}`)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new SharedAipAction.UpdateComplianceSuccess(action));
          } else {
            ctx.dispatch(new SharedAipAction.UpdateComplianceFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.UpdateComplianceFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.UpdateComplianceSuccess)
  updateComplianceSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.UpdateComplianceSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.updateCompliance.success"));
    this._refreshView(ctx, action.parentAction.id, action.parentAction.viewMode);
  }

  @Action(SharedAipAction.UpdateComplianceFail)
  updateComplianceFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.UpdateComplianceFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.updateCompliance.fail"));
  }

  @Action(SharedAipAction.DeepChecksum)
  deepChecksum(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.DeepChecksum): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<any, Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.CHECK_FIXITY}?level=DEEP_FIXITY`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new SharedAipAction.DeepChecksumSuccess(action));
          } else {
            ctx.dispatch(new SharedAipAction.DeepChecksumFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.DeepChecksumFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.DeepChecksumSuccess)
  deepChecksumSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.DeepChecksumSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.deepChecksum.success"));
    this._refreshView(ctx, action.parentAction.id, action.parentAction.viewMode);
  }

  @Action(SharedAipAction.DeepChecksumFail)
  deepChecksumFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.DeepChecksumFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.deepChecksum.fail"));
  }

  @Action(SharedAipAction.SimpleChecksum)
  simpleChecksum(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.SimpleChecksum): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<any, Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.CHECK_FIXITY}?level=SIMPLE_FIXITY`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new SharedAipAction.SimpleChecksumSuccess(action));
          } else {
            ctx.dispatch(new SharedAipAction.SimpleChecksumFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.SimpleChecksumFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.SimpleChecksumSuccess)
  simpleChecksumSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.SimpleChecksumSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.simpleChecksum.success"));
    this._refreshView(ctx, action.parentAction.id, action.parentAction.viewMode);
  }

  @Action(SharedAipAction.SimpleChecksumFail)
  simpleChecksumFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.SimpleChecksumFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.simpleChecksum.fail"));
  }

  @Action(SharedAipAction.Resume)
  resume(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.Resume): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<any, Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.RESUME}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new SharedAipAction.ResumeSuccess(action));
          } else {
            ctx.dispatch(new SharedAipAction.ResumeFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.ResumeFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.ResumeSuccess)
  resumeSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ResumeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.resume.success"));
    this._refreshView(ctx, action.parentAction.id, action.parentAction.viewMode);
  }

  @Action(SharedAipAction.ResumeFail)
  resumeFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ResumeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.resume.fail"));
  }

  @Action(SharedAipAction.GoToAip)
  goToAip(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.GoToAip): void {
    ctx.patchState({
      current: action.aip,
    });
    const pathAipDetail = RoutesEnum.preservationPlanningAip + urlSeparator + action.storagion_number + urlSeparator + SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator;
    const path = [pathAipDetail, action.aip.resId];
    ctx.dispatch(new Navigate(path));
  }

  @Action(SharedAipAction.Check)
  check(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.Check): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<any, Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.CHECK}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new SharedAipAction.CheckSuccess(action));
          } else {
            ctx.dispatch(new SharedAipAction.CheckFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.CheckFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.CheckSuccess)
  checkSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.CheckSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.check.success"));
    this._refreshView(ctx, action.parentAction.id, action.parentAction.viewMode);
  }

  @Action(SharedAipAction.CheckFail)
  checkFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.CheckFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.check.fail"));
  }

  @Action(SharedAipAction.Dispose)
  dispose(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.Dispose): Observable<string> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.delete<string>(`${this._urlResource}/${action.id}`, null)
      .pipe(
        tap(result => {
          ctx.dispatch(new SharedAipAction.DisposeSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.DisposeFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.DisposeSuccess)
  disposeSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.DisposeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.dispose.success"));
    this._refreshView(ctx, action.parentAction.id, action.parentAction.viewMode);
  }

  @Action(SharedAipAction.DisposeFail)
  disposeFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.DisposeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.dispose.fail"));
  }

  @Action(SharedAipAction.ApproveDisposal)
  approveDisposal(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ApproveDisposal): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const customParams = {
      [this._KEY_QUERY_PARAM_REASON]: action.reason,
    };
    return this._apiService.post<any, Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.APPROVE_DISPOSAL}`, null, customParams)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new SharedAipAction.ApproveDisposalSuccess(action));
          } else {
            ctx.dispatch(new SharedAipAction.ApproveDisposalFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.ApproveDisposalFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.ApproveDisposalSuccess)
  approveDisposalSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ApproveDisposalSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.approveDisposal.success"));
    this._refreshView(ctx, action.parentAction.id, ViewModeEnum.detail);
  }

  @Action(SharedAipAction.ApproveDisposalFail)
  approveDisposalFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ApproveDisposalFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.approveDisposal.fail"));
  }

  @Action(SharedAipAction.ApproveDisposalByOrgunit)
  approveDisposalByOrgunit(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ApproveDisposalByOrgunit): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<any, Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.APPROVE_DISPOSAL_BY_ORGUNIT}`, null, {
      [this._KEY_QUERY_PARAM_REASON]: action.reason,
    })
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new SharedAipAction.ApproveDisposalByOrgunitSuccess(action));
          } else {
            ctx.dispatch(new SharedAipAction.ApproveDisposalByOrgunitFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.ApproveDisposalByOrgunitFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.ApproveDisposalByOrgunitSuccess)
  approveDisposalByOrgunitSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ApproveDisposalByOrgunitSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.approveDisposalByOrgunit.success"));
    this._refreshView(ctx, action.parentAction.id, ViewModeEnum.detail);
  }

  @Action(SharedAipAction.ApproveDisposalByOrgunitFail)
  approveDisposalByOrgunitFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ApproveDisposalByOrgunitFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.approveDisposalByOrgunit.fail"));
  }

  @Action(SharedAipAction.ExtendRetention)
  extendRetention(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ExtendRetention): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<any, Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.EXTEND_RETENTION}`, null, {
      [this._KEY_QUERY_PARAM_DURATION]: action.duration + "",
    })
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new SharedAipAction.ExtendRetentionSuccess(action));
          } else {
            ctx.dispatch(new SharedAipAction.ExtendRetentionFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.ExtendRetentionFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.ExtendRetentionSuccess)
  extendRetentionSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ExtendRetentionSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.extendRetention.success"));
    this._refreshView(ctx, action.parentAction.id, ViewModeEnum.detail);
  }

  @Action(SharedAipAction.ExtendRetentionFail)
  extendRetentionFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.ExtendRetentionFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.extendRetention.fail"));
  }

  @Action(SharedAipAction.FixAipInfo)
  fixAipInfo(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.FixAipInfo): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.FIX_AIP_INFO}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new SharedAipAction.FixAipInfoSuccess(action));
          } else {
            ctx.dispatch(new SharedAipAction.FixAipInfoFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.FixAipInfoFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.FixAipInfoSuccess)
  fixAipInfoSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.FixAipInfoSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.fixAipInfo.success"));
    this._refreshView(ctx, action.parentAction.id, action.parentAction.viewMode);
  }

  @Action(SharedAipAction.FixAipInfoFail)
  fixAipInfoFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.FixAipInfoFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.fixAipInfo.fail"));
  }

  @Action(SharedAipAction.PutInError)
  putInError(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.PutInError): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.PUT_IN_ERROR}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new SharedAipAction.PutInErrorSuccess(action));
          } else {
            ctx.dispatch(new SharedAipAction.PutInErrorFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedAipAction.PutInErrorFail(action));
          throw error;
        }),
      );
  }

  @Action(SharedAipAction.PutInErrorSuccess)
  putInErrorSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.PutInErrorSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.aip.action.putInError.success"));
    this._refreshView(ctx, action.parentAction.id, action.parentAction.viewMode);
  }

  @Action(SharedAipAction.PutInErrorFail)
  putInErrorFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.PutInErrorFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.aip.action.putInError.fail"));
  }

  @Action(SharedAipAction.BulkSimpleChecksum)
  bulkSimpleChecksum(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkSimpleChecksum): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listAip
      .map(aip =>
        ({
          action: new SharedAipAction.SimpleChecksum(aip.resId, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.SimpleChecksumSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.SimpleChecksumFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedAipAction.BulkSimpleChecksumSuccess(action));
        } else {
          ctx.dispatch(new SharedAipAction.BulkSimpleChecksumFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedAipAction.BulkSimpleChecksumSuccess)
  bulkSimpleChecksumSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkSimpleChecksumSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshList(ctx);
  }

  @Action(SharedAipAction.BulkSimpleChecksumFail)
  bulkSimpleChecksumFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkSimpleChecksumFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedAipAction.BulkDeepChecksum)
  bulkDeepChecksum(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkDeepChecksum): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listAip
      .map(aip =>
        ({
          action: new SharedAipAction.DeepChecksum(aip.resId, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.DeepChecksumSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.DeepChecksumFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedAipAction.BulkDeepChecksumSuccess(action));
        } else {
          ctx.dispatch(new SharedAipAction.BulkDeepChecksumFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedAipAction.BulkDeepChecksumSuccess)
  bulkDeepChecksumSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkDeepChecksumSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshList(ctx);
  }

  @Action(SharedAipAction.BulkDeepChecksumFail)
  bulkDeepChecksumFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkDeepChecksumFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedAipAction.BulkReindex)
  bulkReindex(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkReindex): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listAip
      .map(aip =>
        ({
          action: new SharedAipAction.Reindex(aip.resId, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.ReindexSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.ReindexFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedAipAction.BulkReindexSuccess(action));
        } else {
          ctx.dispatch(new SharedAipAction.BulkReindexFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedAipAction.BulkReindexSuccess)
  bulkReindexSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkReindexSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshList(ctx);
  }

  @Action(SharedAipAction.BulkReindexFail)
  bulkReindexFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkReindexFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedAipAction.BulkUpdateCompliance)
  bulkUpdateCompliance(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkUpdateCompliance): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listAip
      .map(aip =>
        ({
          action: new SharedAipAction.UpdateCompliance(aip.resId, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.UpdateComplianceSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.UpdateComplianceFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedAipAction.BulkUpdateComplianceSuccess(action));
        } else {
          ctx.dispatch(new SharedAipAction.BulkUpdateComplianceFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedAipAction.BulkUpdateComplianceSuccess)
  bulkUpdateComplianceSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkUpdateComplianceSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshList(ctx);
  }

  @Action(SharedAipAction.BulkUpdateComplianceFail)
  bulkUpdateComplianceFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkUpdateComplianceFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedAipAction.BulkCheck)
  bulkCheck(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkCheck): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listAip
      .map(aip =>
        ({
          action: new SharedAipAction.Check(aip.resId, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.CheckSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.CheckFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedAipAction.BulkCheckSuccess(action));
        } else {
          ctx.dispatch(new SharedAipAction.BulkCheckFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedAipAction.BulkCheckSuccess)
  bulkCheckSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkCheckSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshList(ctx);
  }

  @Action(SharedAipAction.BulkCheckFail)
  bulkCheckFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkCheckFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedAipAction.BulkDispose)
  bulkDispose(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkDispose): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listAip
      .map(aip =>
        ({
          action: new SharedAipAction.Dispose(aip.resId, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.DisposeSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.DisposeFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedAipAction.BulkDisposeSuccess(action));
        } else {
          ctx.dispatch(new SharedAipAction.BulkDisposeFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedAipAction.BulkDisposeSuccess)
  bulkDisposeSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkDisposeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshList(ctx);
  }

  @Action(SharedAipAction.BulkDisposeFail)
  bulkDisposeFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkDisposeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedAipAction.BulkResume)
  bulkResume(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkResume): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listAip
      .map(aip =>
        ({
          action: new SharedAipAction.Resume(aip.resId, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.ResumeSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.ResumeFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedAipAction.BulkResumeSuccess(action));
        } else {
          ctx.dispatch(new SharedAipAction.BulkResumeFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedAipAction.BulkResumeSuccess)
  bulkResumeSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkResumeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshList(ctx);
  }

  @Action(SharedAipAction.BulkResumeFail)
  bulkResumeFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkResumeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedAipAction.BulkPutInError)
  bulkPutInError(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkPutInError): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listAip
      .map(aip =>
        ({
          action: new SharedAipAction.PutInError(aip.resId, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.PutInErrorSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedAipAction.PutInErrorFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedAipAction.BulkPutInErrorSuccess(action));
        } else {
          ctx.dispatch(new SharedAipAction.BulkPutInErrorFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedAipAction.BulkPutInErrorSuccess)
  bulkPutInErrorSuccess(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkPutInErrorSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshList(ctx);
  }

  @Action(SharedAipAction.BulkPutInErrorFail)
  bulkPutInErrorFail(ctx: SolidifyStateContext<SharedAipStateModel>, action: SharedAipAction.BulkPutInErrorFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  private _refreshView(ctx: SolidifyStateContext<SharedAipStateModel>,
                       aipId: string | undefined,
                       mode: ViewModeEnum | undefined): void {
    if (mode === ViewModeEnum.detail) {
      this._refreshDetail(ctx, aipId);
    } else if (mode === ViewModeEnum.list) {
      this._refreshList(ctx);
    }
  }

  private _refreshDetail(ctx: SolidifyStateContext<SharedAipStateModel>, aipId: string): void {
    ctx.dispatch(new SharedAipAction.GetById(aipId, true));
  }

  private _refreshList(ctx: SolidifyStateContext<SharedAipStateModel>): void {
    const queryParameters = MemoizedUtil.queryParametersSnapshot(this._store, SharedAipState);
    ctx.dispatch(new SharedAipAction.ChangeQueryParameters(queryParameters, true, true));
  }
}
