/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-collection.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {Aip} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {AipHelper} from "@shared/features/aip/helpers/aip.helper";
import {
  SharedAipCollectionAction,
  sharedAipCollectionActionNameSpace,
} from "@shared/features/aip/stores/aip-collection/shared-aip-collection.action";
import {
  SharedAipCollectionStatusHistoryState,
  SharedAipCollectionStatusHistoryStateModel,
} from "@shared/features/aip/stores/aip-collection/status-history/shared-aip-collection-status-history.state";
import {SharedAipAction} from "@shared/features/aip/stores/shared-aip.action";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  DownloadService,
  isNullOrUndefined,
  NotificationService,
  SolidifyStateContext,
} from "solidify-frontend";
import {ApiActionNameEnum} from "../../../../enums/api-action-name.enum";

export const defaultSharedAipCollectionValue: () => SharedAipCollectionStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    [StateEnum.shared_aip_collection_statusHistory]: {...defaultStatusHistoryInitValue()},
  });

export interface SharedAipCollectionStateModel extends CompositionStateModel<Aip> {
  [StateEnum.shared_aip_collection_statusHistory]: SharedAipCollectionStatusHistoryStateModel;
}

@Injectable()
@State<SharedAipCollectionStateModel>({
  name: StateEnum.shared_aip_collection,
  defaults: {
    ...defaultSharedAipCollectionValue(),
    [StateEnum.shared_aip_collection_statusHistory]: {...defaultStatusHistoryInitValue()},
  },
  children: [
    SharedAipCollectionStatusHistoryState,
  ],
})
export class SharedAipCollectionState extends CompositionState<SharedAipCollectionStateModel, Aip> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedAipCollectionActionNameSpace,
      resourceName: ApiResourceNameEnum.AIP,
    });
  }

  protected get _urlResource(): string {
    return AipHelper.generateUrlResource(this._store);
  }

  @Action(SharedAipCollectionAction.Refresh)
  refresh(ctx: SolidifyStateContext<SharedAipCollectionStateModel>, action: SharedAipCollectionAction.Refresh): void {
    ctx.dispatch(new SharedAipCollectionAction.GetAll(action.parentId, undefined, true));
  }

  @Action(SharedAipCollectionAction.DownloadAip)
  downloadAip(ctx: SolidifyStateContext<SharedAipCollectionStateModel>, action: SharedAipCollectionAction.DownloadAip): void {
    const url = `${this._urlResource}/${action.aip.resId}/${ApiActionNameEnum.DL}`;
    this._downloadService.download(url, action.aip.info.name, action.aip.archiveSize);
  }

  @Action(SharedAipCollectionAction.GoToAip)
  goToAip(ctx: SolidifyStateContext<SharedAipCollectionStateModel>, action: SharedAipCollectionAction.GoToAip): void {
    ctx.patchState({
      current: undefined,
      list: undefined,
    });
    ctx.dispatch(new SharedAipAction.GoToAip(action.aip, isNullOrUndefined(action.storagion_number) ? "" + environment.defaultStorageIndex + 1 : action.storagion_number));
  }
}
