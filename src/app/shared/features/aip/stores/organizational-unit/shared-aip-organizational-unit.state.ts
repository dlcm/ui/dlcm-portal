/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-organizational-unit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {OrganizationalUnit} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedAipOrgUnitActionNameSpace} from "@shared/features/aip/stores/organizational-unit/shared-aip-organizational-unit.action";
import {
  ApiService,
  defaultResourceStateInitValue,
  NotificationService,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
} from "solidify-frontend";

export const defaultSharedAipOrgUnitValue: () => SharedAipOrganizationalUnitStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultPageSize),
    search: null,
  });

export interface SharedAipOrganizationalUnitStateModel extends ResourceStateModel<OrganizationalUnit> {
  search: string;
}

@Injectable()
@State<SharedAipOrganizationalUnitStateModel>({
  name: StateEnum.shared_aip_organizationalUnit,
  defaults: {
    ...defaultSharedAipOrgUnitValue(),
  },
})
export class SharedAipOrganizationalUnitState extends ResourceState<SharedAipOrganizationalUnitStateModel, OrganizationalUnit> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedAipOrgUnitActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOrganizationalUnits;
  }

}
