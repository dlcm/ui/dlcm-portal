/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-shared.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ModuleWithProviders,
  NgModule,
} from "@angular/core";
import {
  RouterModule,
  Routes,
} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SharedAipFormPresentational} from "@shared/features/aip/components/presentationals/aip-form/shared-aip-form.presentational";
import {SharedAipCollectionDetailRoutable} from "@shared/features/aip/components/routables/aip-collection-detail/shared-aip-collection-detail.routable";
import {SharedAipCollectionRoutable} from "@shared/features/aip/components/routables/aip-collection/shared-aip-collection.routable";
import {SharedAipDetailEditRoutable} from "@shared/features/aip/components/routables/aip-detail-edit/shared-aip-detail-edit.routable";
import {SharedAipFileDetailRoutable} from "@shared/features/aip/components/routables/aip-file-detail/shared-aip-file-detail.routable";
import {SharedAipFileRoutable} from "@shared/features/aip/components/routables/aip-file/shared-aip-file.routable";
import {SharedAipListRoutable} from "@shared/features/aip/components/routables/aip-list/shared-aip-list.routable";
import {SharedAipMetadataRoutable} from "@shared/features/aip/components/routables/aip-metadata/shared-aip-metadata.routable";
import {SharedAipNotFoundRoutable} from "@shared/features/aip/components/routables/aip-not-found/shared-aip-not-found.routable";
import {SharedAipStoragionRootRoutable} from "@shared/features/aip/components/routables/aip-storagion-root/shared-aip-storagion-root.routable";
import {SharedAipTabsRoutable} from "@shared/features/aip/components/routables/aip-tabs/shared-aip-tabs.routable";
import {SharedAipCollectionState} from "@shared/features/aip/stores/aip-collection/shared-aip-collection.state";
import {SharedAipCollectionStatusHistoryState} from "@shared/features/aip/stores/aip-collection/status-history/shared-aip-collection-status-history.state";
import {SharedAipDataFileState} from "@shared/features/aip/stores/aip-data-file/shared-aip-data-file.state";
import {SharedAipDataFileStatusHistoryState} from "@shared/features/aip/stores/aip-data-file/status-history/shared-aip-data-file-status-history.state";
import {SharedAipOrganizationalUnitState} from "@shared/features/aip/stores/organizational-unit/shared-aip-organizational-unit.state";
import {SharedAipState} from "@shared/features/aip/stores/shared-aip.state";
import {SharedAipStatusHistoryState} from "@shared/features/aip/stores/status-history/shared-aip-status-history.state";
import {SharedModule} from "@shared/shared.module";

const routables = [
  SharedAipTabsRoutable,
  SharedAipListRoutable,
  SharedAipDetailEditRoutable,
  SharedAipStoragionRootRoutable,
  SharedAipMetadataRoutable,
  SharedAipCollectionRoutable,
  SharedAipCollectionDetailRoutable,
  SharedAipFileRoutable,
  SharedAipFileDetailRoutable,
  SharedAipNotFoundRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  SharedAipFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    RouterModule,
    SharedModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      SharedAipState,
      SharedAipStatusHistoryState,
      SharedAipOrganizationalUnitState,
      SharedAipCollectionState,
      SharedAipCollectionStatusHistoryState,
      SharedAipDataFileState,
      SharedAipDataFileStatusHistoryState,
    ]),
  ],
  exports: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
    SharedModule,
    RouterModule,
  ],
  providers: [],
})
export class SharedAipSharedModule {
  static forChild(routes: Routes): ModuleWithProviders<SharedAipSharedModule> {
    return {
      ngModule: SharedAipSharedModule,
      providers: RouterModule.forChild(routes).providers,
    };
  }
}
