/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - aip.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Aip} from "@models";
import {Store} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
  PreservationSpaceRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNumberReal,
  SOLIDIFY_CONSTANTS,
  StringUtil,
} from "solidify-frontend";

export class AipHelper {
  static generateUrlResourceHistoryDataFile(store: Store, storagionNumber: number | undefined = undefined, url: string | undefined = undefined): string {
    if (isNullOrUndefined(store)) {
      return StringUtil.stringEmpty;
    }

    url = isNullOrUndefined(url) ? store.selectSnapshot(state => state.router?.state?.url) : url;

    const urlParts = url.split(AppRoutesEnum.separator);
    const aipId = urlParts[urlParts.indexOf(SharedAipRoutesEnum.aipDetail) + 1];
    const subResourceUrl = urlSeparator + aipId + urlSeparator + ApiResourceNameEnum.DATAFILE;
    return this.generateUrlResource(store, storagionNumber, url) + subResourceUrl;
  }

  static generateUrlResource(store: Store, storagionNumber: number | undefined = undefined, url: string | undefined = undefined): string {
    if (isNullOrUndefined(store)) {
      return StringUtil.stringEmpty;
    }

    url = isNullOrUndefined(url) ? store.selectSnapshot(state => state.router?.state?.url) : url;

    if (url.includes(PreservationPlanningRoutesEnum.aipDownloaded)) {
      return ApiEnum.accessAipDownloaded;
    }

    if (url.includes(PreservationSpaceRoutesEnum.aipSteward)) {
      return ApiEnum.archivalStorageAip;
    }

    if (isNullOrUndefined(storagionNumber)) {
      if (url.includes(PreservationPlanningRoutesEnum.job)) {
        // Do nothing
      } else if (url.includes(SharedAipRoutesEnum.aipDetail)) {
        const urlParts = url.split(AppRoutesEnum.separator);
        const partBeforeAipDetail = urlParts[urlParts.indexOf(SharedAipRoutesEnum.aipDetail) - 1];
        storagionNumber = +partBeforeAipDetail;
      } else {
        storagionNumber = +store.selectSnapshot(state => state.router.state.root.children[0].children[0].children[0].params[PreservationPlanningRoutesEnum.storagionNumberWithoutPrefixParam]);
      }
    }

    if (!isNumberReal(storagionNumber)) {
      storagionNumber = environment.defaultStorageIndex + 1;
    }

    const storagion = ApiEnum.archivalStorageAipStorages.find(aip => aip.index === +storagionNumber);
    if (isNullOrUndefined(storagion)) {
      throw new Error(`The storagion index '${storagionNumber}' is not find in setting`);
    }
    return storagion.url;
  }

  static determineMode(store: Store): AipMode {
    const url = store.selectSnapshot(state => state.router.state.url);
    if (url.includes(PreservationPlanningRoutesEnum.aipDownloaded)) {
      return AipMode.DOWNLOADED_AIP;
    }
    if (url.includes(PreservationSpaceRoutesEnum.aipSteward)) {
      return AipMode.AIP_STEWARD;
    }
    return AipMode.AIP;
  }

  static getStoragionIndex(store: Store): number | undefined {
    if (this.determineMode(store) !== AipMode.AIP) {
      return;
    }
    const url: string = store.selectSnapshot(state => state.router.state.url);
    const suffixUrl = url.substring((SOLIDIFY_CONSTANTS.URL_SEPARATOR + RoutesEnum.preservationPlanningAip).length + 1);
    const indexOfNextUrlSeparator = suffixUrl.indexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR);
    if (indexOfNextUrlSeparator === -1) {
      return +suffixUrl;
    }
    return +suffixUrl.substring(0, indexOfNextUrlSeparator);
  }

  static getStoragionNameByIndex(index: number): string {
    if (environment.archivalStorageName.length < index) {
      return String(index);
    }
    return environment.archivalStorageName[index - 1];
  }

  static isSimpleChecksumAuthorized(aip: Aip, mode: AipMode): boolean {
    return this._isAipAuthorized(aip, mode);
  }

  static isDoubleChecksumAuthorized(aip: Aip, mode: AipMode): boolean {
    return this._isAipAuthorized(aip, mode);
  }

  static isCheckAuthorized(aip: Aip, mode: AipMode): boolean {
    return this._isAipAuthorized(aip, mode);
  }

  private static _isAipAuthorized(aip: Aip, mode: AipMode): boolean {
    return mode === AipMode.AIP
      && isNotNullNorUndefined(aip?.info)
      && Enums.Package.statusIsCompleted(aip.info.status as Enums.Package.StatusEnum);
  }

  static isReindexAuthorized(aip: Aip, mode: AipMode): boolean {
    return mode === AipMode.AIP
      && isNotNullNorUndefined(aip?.info)
      && (Enums.Package.statusIsCompleted(aip.info.status as Enums.Package.StatusEnum)
        || Enums.Package.statusIsDisposable(aip.info.status as Enums.Package.StatusEnum));
  }

  static isComplianceUpdateAuthorized(aip: Aip, mode: AipMode): boolean {
    return mode === AipMode.AIP
      && isNotNullNorUndefined(aip?.info)
      && (Enums.Package.statusIsCompleted(aip.info.status as Enums.Package.StatusEnum));
  }

  static isDisposeAuthorized(aip: Aip, mode: AipMode): boolean {
    return mode === AipMode.AIP
      && isNotNullNorUndefined(aip?.info)
      && aip.info.status === Enums.Package.StatusEnum.COMPLETED;
  }

  static isResumeAuthorized(aip: Aip, mode: AipMode): boolean {
    return (mode === AipMode.AIP || mode === AipMode.DOWNLOADED_AIP)
      && isNotNullNorUndefined(aip?.info)
      && (aip.info.status === Enums.Package.StatusEnum.IN_ERROR || aip.info.status === Enums.Package.StatusEnum.PRESERVATION_ERROR);
  }

  static isPutInErrorAuthorized(aip: Aip, mode: AipMode, isRoot: boolean): boolean {
    return (mode === AipMode.AIP || mode === AipMode.DOWNLOADED_AIP)
      && isNotNullNorUndefined(aip?.info)
      && isRoot
      && !Enums.Package.statusIsCompleted(aip.info.status as Enums.Package.StatusEnum)
      && aip.info.status !== Enums.Package.StatusEnum.IN_ERROR;
  }

  static isFixAipInfoAuthorized(aip: Aip, mode: AipMode, isRoot: boolean): boolean {
    return (mode === AipMode.AIP)
      && isNotNullNorUndefined(aip?.info)
      && isRoot
      && !Enums.Package.statusIsCompleted(aip.info.status as Enums.Package.StatusEnum)
      && (aip.info.status === Enums.Package.StatusEnum.IN_ERROR || aip.info.status === Enums.Package.StatusEnum.PRESERVATION_ERROR);
  }
}

export enum AipMode {
  AIP = "AIP",
  DOWNLOADED_AIP = "DOWNLOADED_AIP",
  AIP_STEWARD = "AIP_STEWARD",
}
