/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {appAuthorizedOrganizationalUnitNameSpace} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {AppAuthorizedOrganizationalUnitState} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Aip,
  ChecksumCheck,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {PackageButtonNavigatorTypeEnum} from "@shared/components/presentationals/shared-package-button-navigator/shared-package-button-navigator.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {AipMode} from "@shared/features/aip/helpers/aip.helper";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {KeyValueInfo} from "@shared/models/key-value-info.model";
import {sharedLicenseActionNameSpace} from "@shared/stores/license/shared-license.action";
import {SharedLicenseState} from "@shared/stores/license/shared-license.state";
import {
  AbstractFormPresentational,
  DateUtil,
  EnumUtil,
  FileUtil,
  isNotNullNorUndefined,
  isNullOrUndefined,
  KeyValue,
  MARK_AS_TRANSLATABLE,
  PropertyName,
  ResourceNameSpace,
  SolidifyValidator,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-aip-form",
  templateUrl: "./shared-aip-form.presentational.html",
  styleUrls: ["./shared-aip-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAipFormPresentational extends AbstractFormPresentational<Aip> {
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;

  accessEnumValues: KeyValue[] = Enums.Access.AccessEnumTranslate;
  packageStatusEnumValuesTranslate: KeyValue[] = Enums.Package.StatusEnumTranslate;

  get packageStatusEnum(): typeof Enums.Package.StatusEnum {
    return Enums.Package.StatusEnum;
  }

  sharedLicenseActionNameSpace: ResourceNameSpace = sharedLicenseActionNameSpace;
  sharedLicenseState: typeof SharedLicenseState = SharedLicenseState;

  appAuthorizedOrganizationalUnitNameSpace: ResourceNameSpace = appAuthorizedOrganizationalUnitNameSpace;
  appAuthorizedOrganizationalUnitState: typeof AppAuthorizedOrganizationalUnitState = AppAuthorizedOrganizationalUnitState;

  public formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  currentAip: Aip;

  private _mode: AipMode;

  @Input()
  set mode(value: AipMode) {
    this._mode = value;
    this._computePackageButtonNavigationViewMode();
  }

  get mode(): AipMode {
    return this._mode;
  }

  @Input()
  currentStorageIndex: number;

  currentPackageButtonNavigatorType: PackageButtonNavigatorTypeEnum = undefined;

  get aipMode(): typeof AipMode {
    return AipMode;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  getSensitivityTooltip(dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum): string {
    return (EnumUtil.getKeyValue(Enums.DataSensitivity.DataSensitivityEnumTranslate, dataSensitivity) as KeyValueInfo)?.infoToTranslate;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              private readonly _translate: TranslateService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({});
  }

  protected _bindFormTo(aip: Aip): void {
    this.currentAip = aip;
    this.form = this._fb.group({
      [this.formDefinition.id]: [aip.resId, []],
      [this.formDefinition.organizationalUnitId]: [aip.info.organizationalUnitId, []],
      [this.formDefinition.name]: [aip.info.name, []],
      [this.formDefinition.description]: [aip.info.description, []],
      [this.formDefinition.yearOfCreation]: [DateUtil.extractYearFromDate(aip.creation.when), []],
      [this.formDefinition.status]: [aip.info.status, []],
      [this.formDefinition.lastchecksumCheck]: [this.extractChecksumCheck(aip.checksumCheck), []],
      [this.formDefinition.access]: [aip.info.access, []],
      [this.formDefinition.dataSensitivity]: [aip.info.dataSensitivity, [SolidifyValidator]],
      [this.formDefinition.licenseId]: [aip.info.licenseId, []],
      [this.formDefinition.complianceLevel]: [aip.info.complianceLevel, []],
      [this.formDefinition.container]: [aip.archiveContainer, []],
      [this.formDefinition.archive]: [aip.archiveId, []],
      [this.formDefinition.smartSize]: [FileUtil.transformFileSize(aip.archiveSize), []],
      [this.formDefinition.collectionSmartSize]: [FileUtil.transformFileSize(aip.collectionArchiveSize), []],
      [this.formDefinition.archiveFileNumber]: [aip.archiveFileNumber, []],
      [this.formDefinition.collectionFileNumber]: [aip.collectionFileNumber, []],
      [this.formDefinition.metadataVersion]: [aip.info.metadataVersion, []],
      [this.formDefinition.lastArchiving]: [DateUtil.convertDateToDateTimeString(new Date(aip.lastArchiving)), []],
      [this.formDefinition.retention]: [aip.smartRetention, []],
      [this.formDefinition.retentionEnd]: [DateUtil.convertDateToDateTimeString(new Date(aip.retentionEnd)), []],
      [this.formDefinition.tombstoneSize]: [aip.smartTombstoneSize, []],
      [this.formDefinition.embargoAccess]: [aip.info.embargo?.access, []],
      [this.formDefinition.embargoNumberMonths]: [aip.info.embargo?.months, []],
      [this.formDefinition.embargoStartDate]: [isNullOrUndefined(aip.info.embargo?.startDate) ? "" : DateUtil.convertDateToDateTimeString(new Date(aip.info.embargo?.startDate))],
      [this.formDefinition.contentStructurePublic]: [aip.info.contentStructurePublic, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(aip: Aip): Aip {
    return aip;
  }

  protected override _modelUpdated(oldValue: Aip | undefined, newValue: Aip): void {
    super._modelUpdated(oldValue, newValue);
    if (isNotNullNorUndefined(newValue) && isNotNullNorUndefined(this.form) && isNotNullNorUndefined(this.form.get(this.formDefinition.status))) {
      this.form.get(this.formDefinition.status).setValue(newValue.info.status);
    }
  }

  private _computePackageButtonNavigationViewMode(): PackageButtonNavigatorTypeEnum {
    if (this.mode === AipMode.AIP) {
      this.currentPackageButtonNavigatorType = PackageButtonNavigatorTypeEnum.storage;
      return;
    }
    this.currentPackageButtonNavigatorType = undefined;
  }

  isAipCollection(): boolean {
    return this.currentAip.collectionSize > 0;
  }

  extractChecksumCheck(checksum: ChecksumCheck): string {
    let lastChecksumCheckNever = StringUtil.stringEmpty;
    let lastChecksumCheckSuccess = StringUtil.stringEmpty;
    let lastChecksumCheckFail = StringUtil.stringEmpty;

    const keyLabelNever = MARK_AS_TRANSLATABLE("aip.form.lastChecksumCheck.never");
    const keyLabelSuccess = MARK_AS_TRANSLATABLE("aip.form.lastChecksumCheck.success");
    const keyLabelFail = MARK_AS_TRANSLATABLE("aip.form.lastChecksumCheck.fail");

    this._translate.get([
      keyLabelNever,
      keyLabelSuccess,
      keyLabelFail,
    ]).subscribe(translation => {
      lastChecksumCheckNever = translation[keyLabelNever];
      lastChecksumCheckSuccess = translation[keyLabelSuccess];
      lastChecksumCheckFail = translation[keyLabelFail];
    });

    if (isNullOrUndefined(checksum)) {
      return lastChecksumCheckNever;
    }

    const strDate = DateUtil.convertToLocalDateDateSimple(checksum.checkDate);
    const str = checksum.checkingSucceed ? lastChecksumCheckSuccess : lastChecksumCheckFail;
    return strDate + " (" + str + ")";
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() id: string;
  @PropertyName() organizationalUnitId: string;
  @PropertyName() name: string;
  @PropertyName() description: string;
  @PropertyName() yearOfCreation: string;
  @PropertyName() status: string;
  @PropertyName() lastchecksumCheck: string;
  @PropertyName() access: string;
  @PropertyName() dataSensitivity: string;
  @PropertyName() licenseId: string;
  @PropertyName() complianceLevel: string;
  @PropertyName() container: string;
  @PropertyName() archive: string;
  @PropertyName() smartSize: string;
  @PropertyName() collectionSmartSize: string;
  @PropertyName() archiveFileNumber: string;
  @PropertyName() collectionFileNumber: string;
  @PropertyName() metadataVersion: string;
  @PropertyName() lastArchiving: string;
  @PropertyName() retention: string;
  @PropertyName() retentionEnd: string;
  @PropertyName() tombstoneSize: string;
  @PropertyName() embargoAccess: string;
  @PropertyName() embargoNumberMonths: string;
  @PropertyName() embargoStartDate: string;
  @PropertyName() contentStructurePublic: string;
}
