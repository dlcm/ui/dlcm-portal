/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-approve-disposal.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {SharedAipAction} from "@shared/features/aip/stores/shared-aip.action";
import {SharedAipState} from "@shared/features/aip/stores/shared-aip.state";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {Observable} from "rxjs";
import {
  FormValidationHelper,
  isEmptyString,
  isNullOrUndefined,
  MemoizedUtil,
  PropertyName,
  SolidifyValidator,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-aip-approve-disposal-dialog",
  templateUrl: "./shared-aip-approve-disposal.dialog.html",
  styleUrls: ["./shared-aip-approve-disposal.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAipApproveDisposalDialog extends SharedAbstractDialog<SharedAipApproveDisposalDialogData, boolean> implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedAipState);

  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<SharedAipApproveDisposalDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: SharedAipApproveDisposalDialogData,
              private readonly _fb: FormBuilder) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.reason]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  onSubmit(): void {
    const reason = this.form.get(this.formDefinition.reason).value;
    if (isNullOrUndefined(reason) || isEmptyString(reason)) {
      return;
    }
    let action = null;
    let actionCompleted = null;
    if (this.data.mode === SharedAipExtendRetentionDialogMode.disposal) {
      action = new SharedAipAction.ApproveDisposal(this.data.resId, reason);
      actionCompleted = SharedAipAction.ApproveDisposalSuccess;
    } else if (this.data.mode === SharedAipExtendRetentionDialogMode.disposalByOrgunit) {
      action = new SharedAipAction.ApproveDisposalByOrgunit(this.data.resId, reason);
      actionCompleted = SharedAipAction.ApproveDisposalByOrgunitSuccess;
    } else {
      throw new Error("Unexpected mode");
    }

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      action,
      actionCompleted,
      resultAction => {
        this.submit(true);
      }));
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() reason: string;
}

export interface SharedAipApproveDisposalDialogData {
  resId: string;
  mode: SharedAipExtendRetentionDialogMode;
}

export enum SharedAipExtendRetentionDialogMode {
  disposal,
  disposalByOrgunit
}
