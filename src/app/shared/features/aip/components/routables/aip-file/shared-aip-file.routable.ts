/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-file.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {AipDataFile} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedAipFormPresentational} from "@shared/features/aip/components/presentationals/aip-form/shared-aip-form.presentational";
import {SharedAipDataFileAction} from "@shared/features/aip/stores/aip-data-file/shared-aip-data-file.action";
import {SharedAipDataFileState} from "@shared/features/aip/stores/aip-data-file/shared-aip-data-file.state";
import {sharedAipDataFileStatusHistoryNamespace} from "@shared/features/aip/stores/aip-data-file/status-history/shared-aip-data-file-status-history.action";
import {SharedAipDataFileStatusHistoryState} from "@shared/features/aip/stores/aip-data-file/status-history/shared-aip-data-file-status-history.state";
import {sharedAipActionNameSpace} from "@shared/features/aip/stores/shared-aip.action";
import {SharedAipStateModel} from "@shared/features/aip/stores/shared-aip.state";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {LocalStateModel} from "@shared/models/local-state.model";
import {Observable} from "rxjs";
import {
  AbstractDetailEditRoutable,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  isNotNullNorUndefined,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  StatusHistory,
  StatusHistoryDialog,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-aip-file-routable",
  templateUrl: "./shared-aip-file.routable.html",
  styleUrls: ["./shared-aip-file.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAipFileRoutable extends AbstractDetailEditRoutable<AipDataFile, SharedAipStateModel> implements OnInit {
  isLoadingDataFileObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedAipDataFileState);
  listDataFileObs: Observable<AipDataFile[]> = MemoizedUtil.list(this._store, SharedAipDataFileState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, SharedAipDataFileState);

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  isInDetailMode: boolean = false;

  @ViewChild("formPresentational")
  readonly formPresentational: SharedAipFormPresentational;

  columns: DataTableColumns<AipDataFile>[];
  actions: DataTableActions<AipDataFile>[];

  readonly KEY_PARAM_NAME: keyof AipDataFile & string = undefined;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.shared_aip, _injector, sharedAipActionNameSpace, StateEnum.shared);

    this.columns = [
      {
        field: "fileName",
        header: LabelTranslateEnum.fileName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        filterEnum: Enums.DataFile.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "smartSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        sortableField: "dataFile.fileSize" as any,
        filterableField: "dataFile.fileSize" as any,
        isSortable: false,
        isFilterable: false,
        translate: true,
        alignment: "right",
        width: "100px",
      },
      {
        field: "complianceLevel",
        header: LabelTranslateEnum.complianceLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        component: DataTableComponentHelper.get(DataTableComponentEnum.conformityLevelStar),
        filterEnum: Enums.ComplianceLevel.ComplianceLevelEnumTranslate,
        alignment: "center",
      },
    ];

    this.actions = [
      {
        logo: IconNameEnum.download,
        callback: (aipDataFile: AipDataFile) => this._downloadFile(this._resId, aipDataFile),
        placeholder: current => LabelTranslateEnum.download,
        displayOnCondition: (current: AipDataFile) => current.status === Enums.DataFile.StatusEnum.PROCESSED || current.status === Enums.DataFile.StatusEnum.READY || current.status === Enums.DataFile.StatusEnum.VIRUS_CHECKED,
      },
      {
        logo: IconNameEnum.resume,
        callback: (aipDataFile: AipDataFile) => this._resumeFile(this._resId, aipDataFile),
        placeholder: current => LabelTranslateEnum.resume,
        displayOnCondition: (aipDataFile: AipDataFile) => isNotNullNorUndefined(aipDataFile) && aipDataFile.status === Enums.DataFile.StatusEnum.IN_ERROR,
      },
    ];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._getCurrentModelOnParent();
  }

  private _getCurrentModelOnParent(): void {
    this._resId = this._route.snapshot.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._getAipById(this._resId);
  }

  private _getAipById(id: string): void {
    this._getSubResourceWithParentId(id);
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new SharedAipDataFileAction.GetAll(id, null, true));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new SharedAipDataFileAction.ChangeQueryParameters(this._resId, queryParameters, true));
  }

  refresh(): void {
    this._store.dispatch(new SharedAipDataFileAction.Refresh(this._resId));
  }

  download($event: AipDataFile): void {
    this._store.dispatch(new SharedAipDataFileAction.Download(this._resId, $event));
  }

  showDetail(dataFile: AipDataFile): void {
    this._store.dispatch(new Navigate([this._store.selectSnapshot((s: LocalStateModel) => s.router.state.url), dataFile.resId]));
  }

  private _downloadFile(parentId: string, dataFile: AipDataFile): void {
    this._store.dispatch(new SharedAipDataFileAction.Download(parentId, dataFile));
  }

  private _resumeFile(parentId: string, dataFile: AipDataFile): void {
    this._store.dispatch(new SharedAipDataFileAction.Resume(parentId, dataFile));
  }

  showHistory(aipDataFile: AipDataFile): void {
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedAipDataFileStatusHistoryState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, SharedAipDataFileStatusHistoryState, state => state.queryParameters);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, SharedAipDataFileStatusHistoryState, state => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: aipDataFile.infoPackage.resId,
      resourceResId: aipDataFile.resId,
      name: StringUtil.convertToPascalCase(StateEnum.shared_aip_dataFile_statusHistory),
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: sharedAipDataFileStatusHistoryNamespace,
      statusEnums: Enums.DataFile.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }
}
