/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-not-found.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from "@angular/core";
import {
  ActivatedRoute,
  NavigationEnd,
  Router,
} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
} from "@shared/enums/routes.enum";
import {Storage} from "@shared/models/storage.model";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";

@Component({
  selector: "dlcm-shared-aip-not-found-routable",
  templateUrl: "./shared-aip-not-found.routable.html",
  styleUrls: ["./shared-aip-not-found.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAipNotFoundRoutable extends SharedAbstractRoutable implements OnInit {
  aipId: string;
  storagion_number: number | undefined;

  listStorage: Storage[];

  constructor(private readonly _store: Store,
              private readonly _route: ActivatedRoute,
              private readonly _router: Router) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.retrieveAttribute();

    this.subscribe(this._router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        distinctUntilChanged(),
        tap(event => {
          this.retrieveAttribute();
        }),
      ),
    );
  }

  retrieveAttribute(): void {
    this.storagion_number = +this._route.snapshot.parent.paramMap.get(PreservationPlanningRoutesEnum.storagionNumberWithoutPrefixParam);
    this.aipId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this.listStorage = ApiEnum.archivalStorageList.filter(storage => storage.index !== this.storagion_number);
  }

  back(): void {
    this._navigate([RoutesEnum.preservationPlanningAip]);
  }

  navigateToStorageAipDetail(storage: Storage): void {
    this._navigate([RoutesEnum.preservationPlanningAip, storage.index, SharedAipRoutesEnum.aipDetail, this.aipId]);
  }

  private _navigate(path: (string | number)[]): void {
    this._store.dispatch(new Navigate(path));
  }
}
