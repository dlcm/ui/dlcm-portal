/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-collection.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Aip} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedAipFormPresentational} from "@shared/features/aip/components/presentationals/aip-form/shared-aip-form.presentational";
import {
  AipHelper,
  AipMode,
} from "@shared/features/aip/helpers/aip.helper";
import {SharedAipCollectionAction} from "@shared/features/aip/stores/aip-collection/shared-aip-collection.action";
import {SharedAipCollectionState} from "@shared/features/aip/stores/aip-collection/shared-aip-collection.state";
import {sharedAipCollectionStatusHistoryNamespace} from "@shared/features/aip/stores/aip-collection/status-history/shared-aip-collection-status-history.action";
import {SharedAipCollectionStatusHistoryState} from "@shared/features/aip/stores/aip-collection/status-history/shared-aip-collection-status-history.state";
import {sharedAipActionNameSpace} from "@shared/features/aip/stores/shared-aip.action";
import {SharedAipStateModel} from "@shared/features/aip/stores/shared-aip.state";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {LocalStateModel} from "@shared/models/local-state.model";
import {Observable} from "rxjs";
import {
  AbstractDetailEditRoutable,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  StatusHistory,
  StatusHistoryDialog,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-aip-collection-routable",
  templateUrl: "./shared-aip-collection.routable.html",
  styleUrls: ["./shared-aip-collection.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAipCollectionRoutable extends AbstractDetailEditRoutable<Aip, SharedAipStateModel> implements OnInit {
  isLoadingDataFileObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedAipCollectionState);
  listDataFileObs: Observable<Aip[]> = MemoizedUtil.list(this._store, SharedAipCollectionState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, SharedAipCollectionState);

  isInDetailMode: boolean = false;

  @ViewChild("formPresentational")
  readonly formPresentational: SharedAipFormPresentational;

  columns: DataTableColumns<Aip>[];
  actions: DataTableActions<Aip>[];

  readonly KEY_PARAM_NAME: keyof Aip & string = undefined;
  mode: AipMode;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.shared_aip, _injector, sharedAipActionNameSpace, StateEnum.shared);

    this.columns = [
      {
        field: "info.name" as any,
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "info.status" as any,
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        filterEnum: Enums.Package.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "smartSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        sortableField: "dataFile.fileSize" as any,
        filterableField: "dataFile.fileSize" as any,
        isSortable: false,
        isFilterable: false,
        translate: true,
        alignment: "right",
        width: "100px",
      },
      {
        field: "info.complianceLevel" as any,
        header: LabelTranslateEnum.complianceLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        component: DataTableComponentHelper.get(DataTableComponentEnum.conformityLevelStar),
        filterEnum: Enums.ComplianceLevel.ComplianceLevelEnumTranslate,
        alignment: "center",
      },
    ];

    this.actions = [
      {
        logo: IconNameEnum.download,
        callback: (aip: Aip) => this._downloadAipAip(this._resId, aip),
        placeholder: current => LabelTranslateEnum.download,
        displayOnCondition: (aip: Aip) => this.mode !== AipMode.DOWNLOADED_AIP,
      },
      {
        logo: IconNameEnum.internalLink,
        callback: (aip: Aip) => this._goToAip(aip),
        placeholder: current => LabelTranslateEnum.goToAip,
        displayOnCondition: (aip: Aip) => true,
      },
    ];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.mode = AipHelper.determineMode(this._store);
    this._getCurrentModelOnParent();
  }

  private _getCurrentModelOnParent(): void {
    this._resId = this._route.snapshot.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._getAipById(this._resId);
  }

  private _getAipById(id: string): void {
    this._getSubResourceWithParentId(id);
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new SharedAipCollectionAction.GetAll(id, null, true));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new SharedAipCollectionAction.ChangeQueryParameters(this._resId, queryParameters, true));
  }

  refresh(): void {
    this._store.dispatch(new SharedAipCollectionAction.Refresh(this._resId));
  }

  download(aip: Aip): void {
    this._store.dispatch(new SharedAipCollectionAction.DownloadAip(this._resId, aip));
  }

  showDetail(aip: Aip): void {
    this._store.dispatch(new Navigate([this._store.selectSnapshot((s: LocalStateModel) => s.router.state.url), aip.resId]));
  }

  private _downloadAipAip(parentId: string, aip: Aip): void {
    this._store.dispatch(new SharedAipCollectionAction.DownloadAip(parentId, aip));
  }

  private _goToAip(aip: Aip): void {
    const storagion_number = this._route.snapshot.parent.parent.paramMap.get(PreservationPlanningRoutesEnum.storagionNumberWithoutPrefixParam);
    this._store.dispatch(new SharedAipCollectionAction.GoToAip(aip, storagion_number));
  }

  showHistory(aip: Aip): void {
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedAipCollectionStatusHistoryState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, SharedAipCollectionStatusHistoryState, state => state.queryParameters);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, SharedAipCollectionStatusHistoryState, state => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: aip.resId,
      name: StringUtil.convertToPascalCase(StateEnum.shared_aip_collection_statusHistory),
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: sharedAipCollectionStatusHistoryNamespace,
      statusEnums: Enums.Package.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }
}
