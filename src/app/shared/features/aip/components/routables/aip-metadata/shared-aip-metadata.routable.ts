/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-metadata.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Aip} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedAipFormPresentational} from "@shared/features/aip/components/presentationals/aip-form/shared-aip-form.presentational";
import {
  AipHelper,
  AipMode,
} from "@shared/features/aip/helpers/aip.helper";
import {sharedAipActionNameSpace} from "@shared/features/aip/stores/shared-aip.action";
import {
  SharedAipState,
  SharedAipStateModel,
} from "@shared/features/aip/stores/shared-aip.state";
import {Observable} from "rxjs";
import {AbstractDetailEditRoutable} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-aip-metadata-routable",
  templateUrl: "./shared-aip-metadata.routable.html",
  styleUrls: ["./shared-aip-metadata.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class SharedAipMetadataRoutable extends AbstractDetailEditRoutable<Aip, SharedAipStateModel> implements OnInit, OnDestroy {
  @Select(SharedAipState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(SharedAipState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  @ViewChild("formPresentational")
  readonly formPresentational: SharedAipFormPresentational;

  mode: AipMode;
  currentStorageIndex: number;

  readonly KEY_PARAM_NAME: keyof Aip & string = undefined;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.shared_aip, _injector, sharedAipActionNameSpace, StateEnum.shared);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.mode = AipHelper.determineMode(this._store);
    this.currentStorageIndex = AipHelper.getStoragionIndex(this._store);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  protected _getSubResourceWithParentId(id: string): void {
  }
}
