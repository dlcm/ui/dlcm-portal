/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-collection-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Aip} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractFileAipDetailRoutable} from "@shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable";
import {PreservationPlanningRoutesEnum} from "@shared/enums/routes.enum";
import {
  SharedAipCollectionAction,
  sharedAipCollectionActionNameSpace,
} from "@shared/features/aip/stores/aip-collection/shared-aip-collection.action";
import {SharedAipCollectionState} from "@shared/features/aip/stores/aip-collection/shared-aip-collection.state";
import {sharedAipCollectionStatusHistoryNamespace} from "@shared/features/aip/stores/aip-collection/status-history/shared-aip-collection-status-history.action";
import {SharedAipCollectionStatusHistoryState} from "@shared/features/aip/stores/aip-collection/status-history/shared-aip-collection-status-history.state";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {SecurityService} from "@shared/services/security.service";

@Component({
  selector: "dlcm-shared-aip-collection-detail",
  templateUrl: "../../../../../../shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable.html",
  styleUrls: ["../../../../../../shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAipCollectionDetailRoutable extends SharedAbstractFileAipDetailRoutable<Aip> implements OnInit {
  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _storeDialogService: StoreDialogService,
              protected readonly _securityService: SecurityService) {
    super(_store,
      _route,
      _actions$,
      _dialog,
      _changeDetector,
      SharedAipCollectionState,
      sharedAipCollectionActionNameSpace,
      SharedAipCollectionStatusHistoryState,
      sharedAipCollectionStatusHistoryNamespace,
      _storeDialogService,
      _securityService);
  }

  protected override _goToAip(): void {
    const storagion_number = this._route.snapshot.parent.parent.parent.paramMap.get(PreservationPlanningRoutesEnum.storagionNumberWithoutPrefixParam);
    this._store.dispatch(new SharedAipCollectionAction.GoToAip(this.data.aip, storagion_number));
  }
}
