/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Location} from "@angular/common";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {MatTabGroup} from "@angular/material/tabs";
import {
  ActivatedRoute,
  NavigationCancel,
  NavigationEnd,
  Router,
} from "@angular/router";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Aip} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  PreservationPlanningRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {
  SharedAipApproveDisposalDialog,
  SharedAipExtendRetentionDialogMode,
} from "@shared/features/aip/components/dialogs/aip-approve-disposal/shared-aip-approve-disposal.dialog";
import {SharedAipExtendRetentionDialog} from "@shared/features/aip/components/dialogs/aip-extend-retention/shared-aip-extend-retention.dialog";
import {
  AipHelper,
  AipMode,
} from "@shared/features/aip/helpers/aip.helper";
import {
  SharedAipAction,
  sharedAipActionNameSpace,
} from "@shared/features/aip/stores/shared-aip.action";
import {
  SharedAipState,
  SharedAipStateModel,
} from "@shared/features/aip/stores/shared-aip.state";
import {SharedAipStatusHistoryAction} from "@shared/features/aip/stores/status-history/shared-aip-status-history.action";
import {SharedAipStatusHistoryState} from "@shared/features/aip/stores/status-history/shared-aip-status-history.state";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  map,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  DialogUtil,
  ExtraButtonToolbar,
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MappingObjectUtil,
  MemoizedUtil,
  PollingHelper,
  QueryParameters,
  QueryParametersUtil,
  RouterExtensionService,
  ScrollService,
  StatusHistory,
  StatusHistoryDialog,
  StringUtil,
  Tab,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-aip-detail-edit-routable",
  templateUrl: "./shared-aip-detail-edit.routable.html",
  styleUrls: ["./shared-aip-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAipDetailEditRoutable extends AbstractDetailEditCommonRoutable<Aip, SharedAipStateModel> implements OnInit {
  private readonly _STATUS_TO_STOP_POLLING_REFRESH_STATUS: Enums.Package.StatusEnum[] = [
    Enums.Package.StatusEnum.IN_ERROR,
    Enums.Package.StatusEnum.COMPLETED,
    Enums.Package.StatusEnum.PRESERVATION_ERROR,
    Enums.Package.StatusEnum.CLEANED,
    Enums.Package.StatusEnum.DISPOSED,
    Enums.Package.StatusEnum.DISPOSABLE,
    Enums.Package.StatusEnum.DISPOSAL_APPROVED,
    Enums.Package.StatusEnum.DISPOSAL_APPROVED_BY_ORGUNIT,
  ];

  @Select(SharedAipState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(SharedAipState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, SharedAipStatusHistoryState, state => state.history);
  isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedAipStatusHistoryState);
  queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, SharedAipStatusHistoryState, state => state.queryParameters);

  override getByIdIfAlreadyInState: boolean = false;

  readonly KEY_PARAM_NAME: keyof Aip & string = "archiveId";

  override readonly editAvailable: boolean = false;

  override readonly deleteAvailable: boolean = false;

  @ViewChild("matTabGroup")
  readonly matTabGroup: MatTabGroup;

  storagion_number: number | undefined = undefined;
  mode: AipMode;
  backButtonLabel: string;

  private readonly _KEY_ARCHIVAL_UNIT: keyof Aip = "archivalUnit";

  private _currentTab: Tab;

  private get _rootUrl(): string[] {
    switch (this.mode) {
      case AipMode.AIP:
        return [RoutesEnum.preservationPlanningAip, this.storagion_number + "", SharedAipRoutesEnum.aipDetail, this._resId];
      case AipMode.AIP_STEWARD:
        return [RoutesEnum.preservationSpaceAipStewardDetail, this._resId];
      case AipMode.DOWNLOADED_AIP:
        return [RoutesEnum.preservationPlanningAipDownloadedDetail, this._resId];
    }
    return [];
  }

  listTabs: Tab[] = [
    {
      id: TabEnum.METADATA,
      suffixUrl: SharedAipRoutesEnum.aipMetadata,
      icon: IconNameEnum.metadata,
      titleToTranslate: LabelTranslateEnum.metadata,
      route: () => [...this._rootUrl, SharedAipRoutesEnum.aipMetadata],
    },
    {
      id: TabEnum.FILES,
      suffixUrl: SharedAipRoutesEnum.aipFiles,
      icon: IconNameEnum.files,
      titleToTranslate: LabelTranslateEnum.files,
      route: () => [...this._rootUrl, SharedAipRoutesEnum.aipFiles],
      conditionDisplay: () => this.currentObs.pipe(map(current => current?.dataFileNumber > 0)),
    },
    {
      id: TabEnum.COLLECTION,
      suffixUrl: SharedAipRoutesEnum.aipCollections,
      icon: IconNameEnum.collection,
      titleToTranslate: LabelTranslateEnum.collection,
      route: () => [...this._rootUrl, SharedAipRoutesEnum.aipCollections],
      conditionDisplay: () => this.currentObs.pipe(map(current => current?.collectionSize > 0)),
    },
  ];

  listExtraButtons: ExtraButtonToolbar<Aip>[] = [
    {
      color: "primary",
      icon: IconNameEnum.simpleChecksum,
      displayCondition: current => AipHelper.isSimpleChecksumAuthorized(current, this.mode),
      callback: () => this._simpleChecksum(),
      labelToTranslate: (current) => LabelTranslateEnum.simpleChecksums,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.doubleChecksum,
      displayCondition: current => AipHelper.isDoubleChecksumAuthorized(current, this.mode),
      callback: () => this._deepChecksum(),
      labelToTranslate: (current) => LabelTranslateEnum.deepChecksums,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.reindex,
      displayCondition: current => AipHelper.isReindexAuthorized(current, this.mode),
      callback: () => this._reIndex(),
      labelToTranslate: (current) => LabelTranslateEnum.reindex,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.check,
      displayCondition: current => AipHelper.isCheckAuthorized(current, this.mode),
      callback: () => this._check(),
      labelToTranslate: (current) => LabelTranslateEnum.check,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.compliance,
      displayCondition: current => AipHelper.isComplianceUpdateAuthorized(current, this.mode),
      callback: () => this._updateCompliance(),
      labelToTranslate: (current) => LabelTranslateEnum.updateCompliance,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.dispose,
      displayCondition: current => AipHelper.isDisposeAuthorized(current, this.mode),
      callback: () => this._dispose(),
      labelToTranslate: (current) => LabelTranslateEnum.disposeOf,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.approveDisposal,
      displayCondition: current => this.mode === AipMode.AIP
        && isNotNullNorUndefined(current?.info)
        && current.info.status === Enums.Package.StatusEnum.DISPOSABLE
        && current.dispositionApproval === false,
      callback: () => this._approveDisposal(),
      labelToTranslate: (current) => LabelTranslateEnum.disposalApproval,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.approveDisposalByOrgUnit,
      displayCondition: current => this.mode === AipMode.AIP
        && isNotNullNorUndefined(current?.info)
        && current.info.status === Enums.Package.StatusEnum.DISPOSABLE
        && current.dispositionApproval === true,
      callback: () => this._approveDisposalByOrgunit(),
      labelToTranslate: (current) => LabelTranslateEnum.disposalApproval,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.approveDisposalByOrgUnit,
      displayCondition: current => this.mode === AipMode.AIP_STEWARD
        && isNotNullNorUndefined(current?.info)
        && current.info.status === Enums.Package.StatusEnum.DISPOSABLE,
      callback: () => this._approveDisposalByOrgunit(),
      labelToTranslate: (current) => LabelTranslateEnum.disposalApproval,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.extendRetention,
      displayCondition: current => (this.mode === AipMode.AIP_STEWARD || this.mode === AipMode.AIP)
        && isNotNullNorUndefined(current?.info)
        && current.info.status === Enums.Package.StatusEnum.DISPOSABLE,
      callback: () => this._extendRetention(),
      labelToTranslate: (current) => LabelTranslateEnum.extendTheRetention,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.download,
      displayCondition: current => this.mode !== AipMode.AIP_STEWARD
        && (this.mode === AipMode.DOWNLOADED_AIP || this._securityService.isRoot())
        && isNotNullNorUndefined(current?.info)
        && Enums.Package.statusIsCompleted(current.info.status as Enums.Package.StatusEnum),
      callback: () => this._download(),
      labelToTranslate: (current) => LabelTranslateEnum.download,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.resume,
      displayCondition: current => AipHelper.isResumeAuthorized(current, this.mode),
      callback: () => this._resume(),
      labelToTranslate: (current) => LabelTranslateEnum.resume,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.putInError,
      displayCondition: current => AipHelper.isPutInErrorAuthorized(current, this.mode, this._securityService.isRoot()),
      callback: () => this._putInError(),
      labelToTranslate: (current) => LabelTranslateEnum.putInError,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.fixAipInfo,
      displayCondition: current => AipHelper.isFixAipInfoAuthorized(current, this.mode, this._securityService.isRoot()),
      callback: () => this._fixAipInfo(),
      labelToTranslate: (current) => LabelTranslateEnum.fixAipInfo,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.delete,
      displayCondition: current => isNotNullNorUndefined(current?.info)
        && this._securityService.isRoot()
        && this.mode === AipMode.DOWNLOADED_AIP,
      callback: () => this.delete(),
      labelToTranslate: (current) => LabelTranslateEnum.delete,
      order: 40,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _router: Router,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _location: Location,
              private readonly _securityService: SecurityService,
              private readonly _scrollService: ScrollService,
              private readonly _routerExt: RouterExtensionService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.shared_aip, _injector, sharedAipActionNameSpace, StateEnum.shared);
  }

  ngOnInit(): void {
    this._scrollService.scrollToTop(true);

    this.mode = AipHelper.determineMode(this._store);
    this._retrieveStoragionNumber();
    super.ngOnInit();
    this._computeBackButtonLabel();

    this.subscribe(this._router.events
      .pipe(
        filter(event => event instanceof NavigationCancel || event instanceof NavigationEnd),
        distinctUntilChanged(),
        tap(event => {
          this.mode = AipHelper.determineMode(this._store);
          this._retrieveResIdFromUrl();
          this._retrieveStoragionNumber();
        }),
      ),
    );

    this._createPollingListenerCompletedStatus();
  }

  private _retrieveStoragionNumber(): void {
    if (this.mode === AipMode.AIP) {
      this.storagion_number = +this._route.snapshot.parent.paramMap.get(PreservationPlanningRoutesEnum.storagionNumberWithoutPrefixParam);
    }
  }

  backToListNavigate(): Navigate {
    switch (this.mode) {
      case AipMode.AIP:
        return new Navigate([`${RoutesEnum.preservationPlanningAip}/${this.storagion_number}/${SharedAipRoutesEnum.aipList}/${this._getTabRoute()}`]);
        break;
      case AipMode.DOWNLOADED_AIP:
        return new Navigate([`${RoutesEnum.preservationPlanningAipDownloaded}/${this._getTabRoute()}`]);
        break;
      case AipMode.AIP_STEWARD:
      default:
        const previousUrl = this._routerExt.getPreviousUrl();
        if (isNotNullNorUndefined(previousUrl)) {
          return new Navigate([previousUrl]);
        }
        break;
    }
  }

  _getSubResourceWithParentId(id: string): void {
  }

  private _getTabRoute(): string {
    const queryParameters = MemoizedUtil.selectSnapshot(this._store, SharedAipState, state => state.queryParameters);
    const searchItems = QueryParametersUtil.getSearchItems(queryParameters);
    const archivalUnit = MappingObjectUtil.get(searchItems, this._KEY_ARCHIVAL_UNIT);
    if (archivalUnit === "true") {
      return SharedAipRoutesEnum.aipTabUnit;
    } else if (archivalUnit === "false") {
      return SharedAipRoutesEnum.aipTabCollections;
    } else {
      return SharedAipRoutesEnum.aipTabAll;
    }
  }

  private _computeBackButtonLabel(): void {
    switch (this.mode) {
      case AipMode.AIP_STEWARD:
        this.backButtonLabel = LabelTranslateEnum.backToNotification;
        break;
      case AipMode.DOWNLOADED_AIP:
      case AipMode.AIP:
      default:
        this.backButtonLabel = LabelTranslateEnum.backToList;
        break;
    }
  }

  private _simpleChecksum(): void {
    this._store.dispatch(new SharedAipAction.SimpleChecksum(this._resId, ViewModeEnum.detail));
  }

  private _deepChecksum(): void {
    this._store.dispatch(new SharedAipAction.DeepChecksum(this._resId, ViewModeEnum.detail));
  }

  private _reIndex(): void {
    this._store.dispatch(new SharedAipAction.Reindex(this._resId, ViewModeEnum.detail));
  }

  private _updateCompliance(): void {
    this._store.dispatch(new SharedAipAction.UpdateCompliance(this._resId, ViewModeEnum.detail));
  }

  private _check(): void {
    this._store.dispatch(new SharedAipAction.Check(this._resId, ViewModeEnum.detail));
  }

  private _dispose(): void {
    this._store.dispatch(new SharedAipAction.Dispose(this._resId, ViewModeEnum.detail));
  }

  private _approveDisposal(): void {
    this.subscribe(DialogUtil.open(this._dialog, SharedAipApproveDisposalDialog, {
      resId: this._resId,
      mode: SharedAipExtendRetentionDialogMode.disposal,
    }, {
      minWidth: "500px",
      takeOne: true,
    }, success => this._refreshIfConfirmedSuccess(success)));
  }

  private _approveDisposalByOrgunit(): void {
    this.subscribe(DialogUtil.open(this._dialog, SharedAipApproveDisposalDialog, {
      resId: this._resId,
      mode: SharedAipExtendRetentionDialogMode.disposalByOrgunit,
    }, {
      minWidth: "500px",
      takeOne: true,
    }, success => this._refreshIfConfirmedSuccess(success)));
  }

  private _refreshIfConfirmedSuccess(success: boolean | undefined): void {
    if (isNullOrUndefined(success) || isFalse(success)) {
      return;
    }
    //refresh here ?
  }

  private _extendRetention(): void {
    this.subscribe(DialogUtil.open(this._dialog, SharedAipExtendRetentionDialog, {
      resId: this._resId,
    }, {
      minWidth: "500px",
      takeOne: true,
    }));
  }

  private _resume(): void {
    this._store.dispatch(new SharedAipAction.Resume(this._resId, ViewModeEnum.detail));
  }

  private _download(): void {
    this._store.dispatch(new SharedAipAction.Download(this._resId));
  }

  setCurrentTab($event: Tab): void {
    this._currentTab = $event;
  }

  showHistory(): void {
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: this._resId,
      name: StringUtil.convertToPascalCase(this._state),
      statusHistory: this.historyObs,
      isLoading: this.isLoadingHistoryObs,
      queryParametersObs: this.queryParametersHistoryObs,
      state: SharedAipStatusHistoryAction,
      statusEnums: Enums.Package.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }

  private _putInError(): void {
    this._store.dispatch(new SharedAipAction.PutInError(this._resId, ViewModeEnum.detail));
  }

  private _fixAipInfo(): void {
    this._store.dispatch(new SharedAipAction.FixAipInfo(this._resId, ViewModeEnum.detail));
  }

  private _createPollingListenerCompletedStatus(): void {
    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: environment.refreshDepositSubmittedIntervalInSecond,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: 60,
      stopRefreshAfterMaximumIntervalReached: true,
      resetIntervalWhenUserMouseEvent: true,
      filter: () => this._shouldContinuePollingWaitCompletedStatus(),
      actionToDo: () => {
        this._store.dispatch(new SharedAipAction.GetById(this._resId, true));
      },
    }));
  }

  private _shouldContinuePollingWaitCompletedStatus(): boolean {
    const status = MemoizedUtil.selectSnapshot(this._store, SharedAipState, state => state.current?.info?.status) as Enums.Package.StatusEnum;
    return isNotNullNorUndefined(status) && this._STATUS_TO_STOP_POLLING_REFRESH_STATUS.indexOf(status) === -1;
  }
}

enum TabEnum {
  METADATA = "METADATA",
  FILES = "FILES",
  COLLECTION = "COLLECTIONS",
}
