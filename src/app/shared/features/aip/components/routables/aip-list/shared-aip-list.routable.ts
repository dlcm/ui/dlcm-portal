/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  NavigationCancel,
  NavigationEnd,
  Router,
} from "@angular/router";
import {appAuthorizedOrganizationalUnitNameSpace} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {AppAuthorizedOrganizationalUnitState} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Aip,
  NotificationDlcm,
  OrganizationalUnit,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  PreservationPlanningRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {
  AipHelper,
  AipMode,
} from "@shared/features/aip/helpers/aip.helper";
import {sharedAipCollectionStatusHistoryNamespace} from "@shared/features/aip/stores/aip-collection/status-history/shared-aip-collection-status-history.action";
import {SharedAipCollectionStatusHistoryState} from "@shared/features/aip/stores/aip-collection/status-history/shared-aip-collection-status-history.state";
import {
  SharedAipAction,
  sharedAipActionNameSpace,
} from "@shared/features/aip/stores/shared-aip.action";
import {
  SharedAipState,
  SharedAipStateModel,
} from "@shared/features/aip/stores/shared-aip.state";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {
  AbstractListRoutable,
  DataTableActions,
  DataTableBulkActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MappingObjectUtil,
  MemoizedUtil,
  OrderEnum,
  QueryParametersUtil,
  ResourceNameSpace,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-aip-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./shared-aip-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAipListRoutable extends AbstractListRoutable<Aip, SharedAipStateModel> implements OnInit, OnDestroy {
  KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.back;
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_PARAM_NAME: keyof Aip & string = "info.name" as any;

  appAuthorizedOrganizationalUnitSort: Sort<OrganizationalUnit> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  appAuthorizedOrganizationalUnitNameSpace: ResourceNameSpace = appAuthorizedOrganizationalUnitNameSpace;
  appAuthorizedOrganizationalUnitState: typeof AppAuthorizedOrganizationalUnitState = AppAuthorizedOrganizationalUnitState;

  mode: AipMode;

  stickyTopPosition: number = environment.defaultStickyDatatableHeight - 1;

  override skipInitialQuery: boolean = true;

  private readonly _KEY_ARCHIVAL_UNIT: keyof Aip = "archivalUnit";

  columnsSkippedToClear: string[] = [
    this._KEY_ARCHIVAL_UNIT,
  ];

  storagion_number: number | undefined = undefined;

  currentTab: SharedAipRoutesEnum | undefined = undefined;

  archivalUnit(): void {
    if (this.currentTab === SharedAipRoutesEnum.aipTabUnit) {
      this.getAipListWithParameter(true);
    } else if (this.currentTab === SharedAipRoutesEnum.aipTabCollections) {
      this.getAipListWithParameter(false);
    } else {
      this.getAipListWithParameter(undefined);
    }
  }

  protected _defineBulkActions(): DataTableBulkActions<Aip>[] {
    return [
      {
        color: "primary",
        icon: IconNameEnum.simpleChecksum,
        displayCondition: list => list?.filter(current => AipHelper.isSimpleChecksumAuthorized(current, this.mode)).length > 0,
        callback: list => this._store.dispatch(new SharedAipAction.BulkSimpleChecksum(list?.filter(aip => AipHelper.isSimpleChecksumAuthorized(aip, this.mode)))),
        labelToTranslate: (current) => LabelTranslateEnum.simpleChecksums,
        order: 40,
      },
      {
        color: "primary",
        icon: IconNameEnum.doubleChecksum,
        displayCondition: list => list?.filter(current => AipHelper.isDoubleChecksumAuthorized(current, this.mode)).length > 0,
        callback: list => this._store.dispatch(new SharedAipAction.BulkDeepChecksum(list?.filter(aip => AipHelper.isDoubleChecksumAuthorized(aip, this.mode)))),
        labelToTranslate: (current) => LabelTranslateEnum.deepChecksums,
        order: 40,
      },
      {
        color: "primary",
        icon: IconNameEnum.reindex,
        displayCondition: list => list?.filter(current => AipHelper.isReindexAuthorized(current, this.mode)).length > 0,
        callback: list => this._store.dispatch(new SharedAipAction.BulkReindex(list?.filter(aip => AipHelper.isReindexAuthorized(aip, this.mode)))),
        labelToTranslate: (current) => LabelTranslateEnum.reindex,
        order: 40,
      },
      {
        color: "primary",
        icon: IconNameEnum.check,
        displayCondition: list => list?.filter(current => AipHelper.isCheckAuthorized(current, this.mode)).length > 0,
        callback: list => this._store.dispatch(new SharedAipAction.BulkCheck(list?.filter(aip => AipHelper.isCheckAuthorized(aip, this.mode)))),
        labelToTranslate: (current) => LabelTranslateEnum.check,
        order: 40,
      },
      {
        color: "primary",
        icon: IconNameEnum.compliance,
        displayCondition: list => list?.filter(current => AipHelper.isComplianceUpdateAuthorized(current, this.mode)).length > 0,
        callback: list => this._store.dispatch(new SharedAipAction.BulkUpdateCompliance(list?.filter(aip => AipHelper.isComplianceUpdateAuthorized(aip, this.mode)))),
        labelToTranslate: (current) => LabelTranslateEnum.updateCompliance,
        order: 40,
      },
      {
        color: "primary",
        icon: IconNameEnum.dispose,
        displayCondition: list => list?.filter(current => AipHelper.isDisposeAuthorized(current, this.mode)).length > 0,
        callback: list => this._store.dispatch(new SharedAipAction.BulkDispose(list?.filter(aip => AipHelper.isDisposeAuthorized(aip, this.mode)))),
        labelToTranslate: (current) => LabelTranslateEnum.disposeOf,
        order: 40,
      },
      {
        color: "primary",
        icon: IconNameEnum.resume,
        displayCondition: list => list?.filter(current => AipHelper.isResumeAuthorized(current, this.mode)).length > 0,
        callback: list => this._store.dispatch(new SharedAipAction.BulkResume(list?.filter(aip => AipHelper.isResumeAuthorized(aip, this.mode)))),
        labelToTranslate: (current) => LabelTranslateEnum.resume,
        order: 40,
      },
      {
        color: "primary",
        icon: IconNameEnum.putInError,
        displayCondition: list => list?.filter(current => AipHelper.isPutInErrorAuthorized(current, this.mode, this._securityService.isRoot())).length > 0,
        callback: list => this._store.dispatch(new SharedAipAction.BulkPutInError(list?.filter(aip => AipHelper.isPutInErrorAuthorized(aip, this.mode, this._securityService.isRoot())))),
        labelToTranslate: (current) => LabelTranslateEnum.putInError,
        order: 40,
      },
    ];
  }

  protected override _defineActions(): DataTableActions<NotificationDlcm>[] {
    return [
      {
        logo: IconNameEnum.simpleChecksum,
        displayOnCondition: current => AipHelper.isSimpleChecksumAuthorized(current, this.mode),
        callback: current => this._store.dispatch(new SharedAipAction.SimpleChecksum(current.resId, ViewModeEnum.list)),
        placeholder: (current) => LabelTranslateEnum.simpleChecksums,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.doubleChecksum,
        displayOnCondition: current => AipHelper.isDoubleChecksumAuthorized(current, this.mode),
        callback: current => this._store.dispatch(new SharedAipAction.DeepChecksum(current.resId, ViewModeEnum.list)),
        placeholder: (current) => LabelTranslateEnum.deepChecksums,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.reindex,
        displayOnCondition: current => AipHelper.isReindexAuthorized(current, this.mode),
        callback: current => this._store.dispatch(new SharedAipAction.Reindex(current.resId, ViewModeEnum.list)),
        placeholder: (current) => LabelTranslateEnum.reindex,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.check,
        displayOnCondition: current => AipHelper.isCheckAuthorized(current, this.mode),
        callback: current => this._store.dispatch(new SharedAipAction.Check(current.resId, ViewModeEnum.list)),
        placeholder: (current) => LabelTranslateEnum.check,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.compliance,
        displayOnCondition: current => AipHelper.isComplianceUpdateAuthorized(current, this.mode),
        callback: current => this._store.dispatch(new SharedAipAction.UpdateCompliance(current.resId, ViewModeEnum.list)),
        placeholder: (current) => LabelTranslateEnum.updateCompliance,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.dispose,
        displayOnCondition: current => AipHelper.isDisposeAuthorized(current, this.mode),
        callback: current => this._store.dispatch(new SharedAipAction.Dispose(current.resId, ViewModeEnum.list)),
        placeholder: (current) => LabelTranslateEnum.disposeOf,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.resume,
        displayOnCondition: current => AipHelper.isResumeAuthorized(current, this.mode),
        callback: current => this._store.dispatch(new SharedAipAction.Resume(current.resId, ViewModeEnum.list)),
        placeholder: (current) => LabelTranslateEnum.resume,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.putInError,
        displayOnCondition: current => AipHelper.isPutInErrorAuthorized(current, this.mode, this._securityService.isRoot()),
        callback: current => this._store.dispatch(new SharedAipAction.PutInError(current.resId, ViewModeEnum.list)),
        placeholder: (current) => LabelTranslateEnum.putInError,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.fixAipInfo,
        displayOnCondition: current => AipHelper.isFixAipInfoAuthorized(current, this.mode, this._securityService.isRoot()),
        callback: current => this._store.dispatch(new SharedAipAction.FixAipInfo(current.resId, ViewModeEnum.list)),
        placeholder: (current) => LabelTranslateEnum.fixAipInfo,
        isWrapped: true,
      },
    ];
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _router: Router,
              protected readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.shared_aip, sharedAipActionNameSpace, _injector, {
      canCreate: false,
      historyState: SharedAipCollectionStatusHistoryState,
      historyStateAction: sharedAipCollectionStatusHistoryNamespace,
      historyStatusEnumTranslate: Enums.Package.StatusEnumTranslate,
    }, StateEnum.shared);
    this.cleanIsNeeded = true;
  }

  ngOnInit(): void {
    //only clean the query-parameters if coming from the main menu but not from aip-detail since it already calls the clean in ngOnDestroy()
    if (isNotNullNorUndefined(this._routerExt.getPreviousUrl())) {
      if (this._routerExt.getPreviousUrl().includes(SharedAipRoutesEnum.aipDetail)) {
        this.cleanIsNeeded = false;
      } else {
        this.clean();
      }
    }
    super.ngOnInit();
    this.mode = AipHelper.determineMode(this._store);
    this._computeBackButtonLabel();
    this._updateListWithCurrentTab();
    this.subscribe(this._observeRoutingUpdate());
    if (this.mode === AipMode.AIP) {
      this.storagion_number = +this._route.snapshot.parent.parent.paramMap.get(PreservationPlanningRoutesEnum.storagionNumberWithoutPrefixParam);
    }
    this._computeDetail();
  }

  private _computeDetail(): void {
    switch (this.mode) {
      case AipMode.AIP:
        this.detailRouteToInterpolate = `${RoutesEnum.preservationPlanningAip}/${this.storagion_number}/${SharedAipRoutesEnum.aipDetail}/\{${SOLIDIFY_CONSTANTS.RES_ID}\}`;
        break;
      case AipMode.DOWNLOADED_AIP:
        this.detailRouteToInterpolate = `${RoutesEnum.preservationPlanningAipDownloadedDetail}/\{${SOLIDIFY_CONSTANTS.RES_ID}\}`;
        break;
      default:
        break;
    }
  }

  private _computeBackButtonLabel(): void {
    switch (this.mode) {
      case AipMode.AIP:
        this.KEY_BACK_BUTTON = LabelTranslateEnum.backToStoragionList;
        break;
      case AipMode.DOWNLOADED_AIP:
        this.KEY_BACK_BUTTON = LabelTranslateEnum.backToPreservationPlanning;
        break;
      case AipMode.AIP_STEWARD:
      default:
        this.KEY_BACK_BUTTON = LabelTranslateEnum.back;
        break;
    }
  }

  private _observeRoutingUpdate(): Observable<any> {
    return this._router.events
      .pipe(
        filter(event => event instanceof NavigationCancel || event instanceof NavigationEnd),
        distinctUntilChanged(),
        tap(event => {
          this._updateListWithCurrentTab();
        }),
      );
  }

  private _updateListWithCurrentTab(): void {
    this.currentTab = this._route.snapshot.paramMap.get(SharedAipRoutesEnum.tabWithoutPrefixParam) as SharedAipRoutesEnum;
    this.archivalUnit();
    this.defineColumns();
  }

  override showDetail(model: Aip): void {
    switch (this.mode) {
      case AipMode.AIP:
        this._store.dispatch(new Navigate([`${RoutesEnum.preservationPlanningAip}/${this.storagion_number}/${SharedAipRoutesEnum.aipDetail}`, model.resId]));
        break;
      case AipMode.DOWNLOADED_AIP:
        this._store.dispatch(new Navigate([RoutesEnum.preservationPlanningAipDownloadedDetail, model.resId]));
        break;
      default:
        break;
    }
  }

  private _getSizeColumn(): DataTableColumns<Aip>[] {
    if (this.currentTab === SharedAipRoutesEnum.aipTabCollections) {
      return [{
        field: "smartCollectionArchiveSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        filterableField: "archiveSize",
        sortableField: "archiveSize",
        isFilterable: true,
        isSortable: true,
        alignment: "right",
        width: "100px",
      }];
    } else {
      return [{
        field: "smartSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        filterableField: "archiveSize",
        sortableField: "archiveSize",
        isFilterable: true,
        isSortable: true,
        alignment: "right",
        width: "100px",
      }];
    }

  }

  defineColumns(): void {
    this.columns = [
      {
        field: "info.name" as any,
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      ...this._getSizeColumn(),
      {
        field: "archiveFileNumber",
        header: LabelTranslateEnum.files,
        type: DataTableFieldTypeEnum.number,
        order: OrderEnum.none,
        filterableField: "archiveFileNumber",
        isFilterable: true,
        isSortable: true,
        width: "80px",
      },
      {
        field: "info.status" as any,
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Package.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "info.organizationalUnitId" as any,
        header: LabelTranslateEnum.organizationalUnit,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        component: DataTableComponentHelper.get(DataTableComponentEnum.organizationalUnitName),
        isFilterable: true,
        isSortable: true,
        resourceNameSpace: this.appAuthorizedOrganizationalUnitNameSpace,
        resourceState: this.appAuthorizedOrganizationalUnitState as any,
        searchableSingleSelectSort: this.appAuthorizedOrganizationalUnitSort,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
        width: "100px",
      },
      {
        field: "lastArchiving" as any,
        header: LabelTranslateEnum.archived,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: false,
        isSortable: true,
        width: "100px",
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: false,
        isSortable: true,
        width: "100px",
      },
    ];
  }

  getAipListWithParameter(isArchivalUnit: boolean | undefined): void {
    let queryParameter = MemoizedUtil.queryParametersSnapshot(this._store, SharedAipState);
    queryParameter = QueryParametersUtil.clone(queryParameter);
    if (isNullOrUndefined(queryParameter.sort?.field)) {
      const columnToSort = this.columns.find(c => c.order !== OrderEnum.none);
      if (isNotNullNorUndefined(columnToSort)) {
        queryParameter.sort = {
          field: columnToSort.field,
          order: columnToSort.order,
        };
      }
    }
    if (isNullOrUndefined(isArchivalUnit)) {
      MappingObjectUtil.delete(queryParameter.search.searchItems, this._KEY_ARCHIVAL_UNIT);
    } else {
      MappingObjectUtil.set(queryParameter.search.searchItems, this._KEY_ARCHIVAL_UNIT, isArchivalUnit.toString());
    }
    this._store.dispatch(new SharedAipAction.GetAll(queryParameter, false));
  }

  conditionDisplayEditButton(model: Aip | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: Aip | undefined): boolean {
    return this._securityService.isRoot()
      && this.mode === AipMode.DOWNLOADED_AIP;
  }

  backNavigate(): Navigate {
    switch (this.mode) {
      case AipMode.AIP:
        return new Navigate([RoutesEnum.preservationPlanningAip]);
        break;
      case AipMode.DOWNLOADED_AIP:
        return new Navigate([RoutesEnum.preservationPlanning]);
        break;
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
