/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-tabs.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from "@angular/core";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {Store} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  PreservationPlanningRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
} from "@shared/enums/routes.enum";
import {
  AipHelper,
  AipMode,
} from "@shared/features/aip/helpers/aip.helper";
import {Tab} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-aip-tabs-list-routable",
  templateUrl: "./shared-aip-tabs.routable.html",
  styleUrls: ["./shared-aip-tabs.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAipTabsRoutable extends SharedAbstractRoutable implements OnInit {
  private _storagionNode: number | undefined = undefined;
  private _currentTab: Tab;

  mode: AipMode;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _router: Router) {
    super();
  }

  ngOnInit(): void {
    this.mode = AipHelper.determineMode(this._store);
    if (this.mode === AipMode.AIP) {
      this._storagionNode = +this._route.snapshot.parent.paramMap.get(PreservationPlanningRoutesEnum.storagionNumberWithoutPrefixParam);
    }
  }

  private get _rootUrl(): string[] {
    switch (this.mode) {
      case AipMode.AIP:
        return [RoutesEnum.preservationPlanningAip, this._storagionNode + "", SharedAipRoutesEnum.aipList];
      case AipMode.DOWNLOADED_AIP:
        return [RoutesEnum.preservationPlanningAipDownloadedList];
      case AipMode.AIP_STEWARD:
        return [RoutesEnum.preservationSpaceAipStewardList];
    }
    return [];
  }

  listTabs: Tab[] = [
    {
      id: TabEnum.ALL_TAB_INDEX,
      suffixUrl: SharedAipRoutesEnum.aipTabAll,
      icon: IconNameEnum.metadata,
      titleToTranslate: LabelTranslateEnum.all,
      route: () => [...this._rootUrl, SharedAipRoutesEnum.aipTabAll],
    },
    {
      id: TabEnum.UNIT_TAB_INDEX,
      suffixUrl: SharedAipRoutesEnum.aipTabUnit,
      icon: IconNameEnum.files,
      titleToTranslate: LabelTranslateEnum.units,
      route: () => [...this._rootUrl, SharedAipRoutesEnum.aipTabUnit],
    },
    {
      id: TabEnum.COLLECTION_TAB_INDEX,
      suffixUrl: SharedAipRoutesEnum.aipTabCollections,
      icon: IconNameEnum.collection,
      titleToTranslate: LabelTranslateEnum.collection,
      route: () => [...this._rootUrl, SharedAipRoutesEnum.aipTabCollections],
    },
  ];

  setCurrentTab($event: Tab): void {
    this._currentTab = $event;
  }
}

enum TabEnum {
  ALL_TAB_INDEX = "ALL_TAB_INDEX",
  UNIT_TAB_INDEX = "UNIT_TAB_INDEX",
  COLLECTION_TAB_INDEX = "COLLECTION_TAB_INDEX",
}
