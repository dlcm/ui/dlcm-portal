/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DragDropModule} from "@angular/cdk/drag-drop";
import {LayoutModule} from "@angular/cdk/layout";
import {TextFieldModule} from "@angular/cdk/text-field";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {
  FormsModule,
  ReactiveFormsModule,
} from "@angular/forms";
import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatDialogConfig,
} from "@angular/material/dialog";
import {RouterModule} from "@angular/router";
import {MaterialModule} from "@app/material.module";
import {SharedInstitutionState} from "@app/shared/stores/institution/shared-institution.state";
import {SharedLanguageState} from "@app/shared/stores/language/shared-language.state";
import {SharedLicenseState} from "@app/shared/stores/license/shared-license.state";
import {SharedOrganizationalUnitState} from "@app/shared/stores/organizational-unit/shared-organizational-unit.state";
import {SharedPersonState} from "@app/shared/stores/person/shared-person.state";
import {SharedPreservationPolicyState} from "@app/shared/stores/preservation-policy/shared-preservation-policy.state";
import {SharedState} from "@app/shared/stores/shared.state";
import {SharedSubmissionPolicyState} from "@app/shared/stores/submission-policy/shared-submission-policy.state";
import {
  FaIconLibrary,
  FontAwesomeModule,
} from "@fortawesome/angular-fontawesome";
import {fas} from "@fortawesome/free-solid-svg-icons";
import {FormlyModule} from "@ngx-formly/core";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SharedAdditionalInformationPanelContainer} from "@shared/components/containers/shared-additional-information-panel/shared-additional-information-panel.container";
import {SharedArchiveNameContainer} from "@shared/components/containers/shared-archive-name/shared-archive-name.container";
import {SharedFileAndAipInformationContainer} from "@shared/components/containers/shared-file-and-aip-information/shared-file-and-aip-information.container";
import {SharedOrganizationalUnitNameContainer} from "@shared/components/containers/shared-organizational-unit-name/shared-organizational-unit-name.container";
import {SharedResourceRoleMemberContainer} from "@shared/components/containers/shared-resource-role-member/shared-resource-role-member-container.component";
import {SharedTableInstitutionRoleContainer} from "@shared/components/containers/shared-table-institution-role/shared-table-institution-role.container";
import {SharedTableOrgunitRoleContainer} from "@shared/components/containers/shared-table-orgunit-role/shared-table-orgunit-role.container";
import {SharedTableOrgunitContainer} from "@shared/components/containers/shared-table-orgunit/shared-table-orgunit.container";
import {SharedTablePersonRoleContainer} from "@shared/components/containers/shared-table-person-role/shared-table-person-role-container.component";
import {SharedUserguideSidebarContainer} from "@shared/components/containers/shared-userguide-sidebar/shared-userguide-sidebar.container";
import {SharedCitationDialog} from "@shared/components/dialogs/shared-citation/shared-citation.dialog";
import {SharedInfoExcludedIgnoredFileDialog} from "@shared/components/dialogs/shared-info-excluded-ignored-file/shared-info-excluded-ignored-file.dialog";
import {SharedAccessLevelWithEmbargoPresentational} from "@shared/components/presentationals/shared-access-level-with-embargo/shared-access-level-with-embargo.presentational";
import {SharedAccessLevelPresentational} from "@shared/components/presentationals/shared-access-level/shared-access-level.presentational";
import {SharedAdminSubjectAreaLabelPresentational} from "@shared/components/presentationals/shared-admin-subject-area-label/shared-admin-subject-area-label-presentational.component";
import {SharedAipStatusNamePresentational} from "@shared/components/presentationals/shared-aip-status-name/shared-aip-status-name.presentational";
import {SharedAipStatusOrgunitNamePresentational} from "@shared/components/presentationals/shared-aip-status-orgunit-name/shared-aip-status-orgunit-name.presentational";
import {SharedAipStatusSummaryPresentational} from "@shared/components/presentationals/shared-aip-status-summary/shared-aip-status-summary.presentational";
import {SharedArchivePublicationDatePresentational} from "@shared/components/presentationals/shared-archive-publication-date/shared-archive-publication-date.presentational";
import {SharedArchiveRatingStarPresentational} from "@shared/components/presentationals/shared-archive-rating-star/shared-archive-rating-star.presentational";
import {SharedArchiveTilePresentational} from "@shared/components/presentationals/shared-archive-tile/shared-archive-tile.presentational";
import {SharedChecksumItemPresentational} from "@shared/components/presentationals/shared-checksum-item/shared-checksum-item.presentational";
import {SharedChecksumPresentational} from "@shared/components/presentationals/shared-checksum/shared-checksum.presentational";
import {SharedComplianceLevelPresentational} from "@shared/components/presentationals/shared-compliance-level/shared-compliance-level.presentational";
import {SharedDataSensitivityInputPresentational} from "@shared/components/presentationals/shared-data-sensitivity-input/shared-data-sensitivity-input.presentational";
import {SharedDataSensitivityPresentational} from "@shared/components/presentationals/shared-data-sensitivity/shared-data-sensitivity.presentational";
import {SharedDataUsePolicyPresentational} from "@shared/components/presentationals/shared-data-use-policy/shared-data-use-policy.presentational";
import {SharedDatafileQuickStatusPresentational} from "@shared/components/presentationals/shared-datafile-quick-status/shared-datafile-quick-status.presentational";
import {SharedDuaTypePresentational} from "@shared/components/presentationals/shared-dua-type/shared-dua-type.presentational";
import {SharedFundingAgencyOverlayPresentational} from "@shared/components/presentationals/shared-funding-agency-overlay/shared-funding-agency-overlay.presentational";
import {SharedIdentifiersPresentational} from "@shared/components/presentationals/shared-identifiers/shared-identifiers.presentational";
import {SharedInstitutionOverlayPresentational} from "@shared/components/presentationals/shared-institution-overlay/shared-institution-overlay.presentational";
import {SharedLicenseOverlayPresentational} from "@shared/components/presentationals/shared-license-overlay/shared-license-overlay.presentational";
import {SharedNotificationFormPresentational} from "@shared/components/presentationals/shared-notification-form/shared-notification-form.presentational";
import {SharedNotificationInputPresentational} from "@shared/components/presentationals/shared-notification-input/shared-notification-input.presentational";
import {SharedOrganizationalUnitDisseminationPolicyOverlayPresentational} from "@shared/components/presentationals/shared-organizational-unit-dissemination-policy-overlay/shared-organizational-unit-dissemination-policy-overlay.presentational";
import {SharedOrganizationalUnitDisseminationPolicyParametersPresentational} from "@shared/components/presentationals/shared-organizational-unit-dissemination-policy-parameters/shared-organizational-unit-dissemination-policy-parameters.presentational";
import {SharedOrganizationalUnitOverlayPresentational} from "@shared/components/presentationals/shared-organizational-unit-overlay/shared-organizational-unit-overlay.presentational";
import {SharedPackageButtonNavigatorPresentational} from "@shared/components/presentationals/shared-package-button-navigator/shared-package-button-navigator.presentational";
import {SharedPendingAccessRequestPresentational} from "@shared/components/presentationals/shared-pending-access-request/shared-pending-access-request.presentational";
import {SharedPersonOverlayPresentational} from "@shared/components/presentationals/shared-person-overlay/shared-person-overlay.presentational";
import {SharedPreservationDurationPresentational} from "@shared/components/presentationals/shared-preservation-duration/shared-preservation-duration.presentational";
import {SharedProgressBarPresentational} from "@shared/components/presentationals/shared-progress-bar/shared-progress-bar.presentational";
import {SharedTocPresentational} from "@shared/components/presentationals/shared-toc/shared-toc.presentational";
import {SharedPendingAccessRequestButtonDirective} from "@shared/directives/shared-pending-access-request-button/shared-pending-access-request-button.directive";
import {SharedAipCollectionState} from "@shared/features/aip/stores/aip-collection/shared-aip-collection.state";
import {SharedAipCollectionStatusHistoryState} from "@shared/features/aip/stores/aip-collection/status-history/shared-aip-collection-status-history.state";
import {SharedAipDataFileState} from "@shared/features/aip/stores/aip-data-file/shared-aip-data-file.state";
import {SharedAipDataFileStatusHistoryState} from "@shared/features/aip/stores/aip-data-file/status-history/shared-aip-data-file-status-history.state";
import {SharedAipOrganizationalUnitState} from "@shared/features/aip/stores/organizational-unit/shared-aip-organizational-unit.state";
import {SharedAipState} from "@shared/features/aip/stores/shared-aip.state";
import {SharedAipStatusHistoryState} from "@shared/features/aip/stores/status-history/shared-aip-status-history.state";
import {SharedArchiveAclState} from "@shared/stores/archive-acl/shared-archive-acl.state";
import {SharedArchiveTypeState} from "@shared/stores/archive-type/shared-archive-type.state";
import {SharedArchiveState} from "@shared/stores/archive/shared-archive.state";
import {SharedCitationState} from "@shared/stores/citations/shared-citation.state";
import {SharedContributorState} from "@shared/stores/contributor/shared-contributor.state";
import {SharedDataCategoryState} from "@shared/stores/data-category/shared-data-category.state";
import {SharedDepositState} from "@shared/stores/deposit/shared-deposit.state";
import {SharedDisseminationPolicyState} from "@shared/stores/dissemination-policy/shared-dissemination-policy.state";
import {SharedFundingAgencyState} from "@shared/stores/funding-agency/shared-funding-agency.state";
import {SharedMetadataTypeState} from "@shared/stores/metadata-type/shared-metadata-type.state";
import {SharedNotificationState} from "@shared/stores/notification/shared-notification.state";
import {SharedOrderState} from "@shared/stores/order/shared-order.state";
import {SharedOrganizationalUnitDisseminationPolicyState} from "@shared/stores/organizational-unit/dissemination-policy/shared-organizational-unit-dissemination-policy.state";
import {SharedOrganizationalUnitPreservationPolicyState} from "@shared/stores/organizational-unit/preservation-policy/shared-organizational-unit-preservation-policy.state";
import {SharedOrganizationalUnitSubmissionPolicyState} from "@shared/stores/organizational-unit/submission-policy/shared-organizational-unit-submission-policy.state";
import {SharedPersonInstitutionState} from "@shared/stores/person/institution/shared-people-institution.state";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {SharedSubjectAreaState} from "@shared/stores/subject-area/shared-subject-area.state";
import {SharedSubmissionAgreementState} from "@shared/stores/submission-agreement/shared-submission-agreement.state";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {HighlightModule} from "ngx-highlightjs";
import {ImageCropperComponent} from "ngx-image-cropper";
import {TourMatMenuModule} from "ngx-ui-tour-md-menu";
import {
  SharedGlobalBannerState,
  SharedIndexFieldAliasState,
  SharedOaiMetadataPrefixState,
  SharedOaiSetState,
  SolidifyFrontendApplicationModule,
  SolidifyFrontendRichTextEditorModule,
  SolidifyFrontendTwitterModule,
} from "solidify-frontend";
import {SharedOrganizationalUnitDisseminationPolicyParametersDialog} from "./components/dialogs/shared-organizational-unit-dissemination-policy-parameters/shared-organizational-unit-dissemination-policy-parameters.dialog";

const routables = [
  SharedFileAndAipInformationContainer,
];
const containers = [
  SharedTableOrgunitContainer,
  SharedOrganizationalUnitNameContainer,
  SharedResourceRoleMemberContainer,
  SharedArchiveNameContainer,
  SharedUserguideSidebarContainer,
  SharedTableInstitutionRoleContainer,
  SharedTableOrgunitRoleContainer,
  SharedTablePersonRoleContainer,
  SharedAdditionalInformationPanelContainer,
];
const dialogs = [
  SharedInfoExcludedIgnoredFileDialog,
  SharedCitationDialog,
  SharedOrganizationalUnitDisseminationPolicyParametersDialog,
];
const contents = [];
const presentationals = [
  SharedArchiveRatingStarPresentational,
  SharedNotificationFormPresentational,
  SharedTocPresentational,
  SharedComplianceLevelPresentational,
  SharedAipStatusSummaryPresentational,
  SharedAipStatusNamePresentational,
  SharedAipStatusOrgunitNamePresentational,
  SharedChecksumPresentational,
  SharedChecksumItemPresentational,
  SharedDatafileQuickStatusPresentational,
  SharedAdminSubjectAreaLabelPresentational,
  SharedPreservationDurationPresentational,
  SharedAccessLevelPresentational,
  SharedArchivePublicationDatePresentational,
  SharedDataSensitivityPresentational,
  SharedArchiveTilePresentational,
  SharedDataSensitivityInputPresentational,
  SharedAccessLevelWithEmbargoPresentational,
  SharedProgressBarPresentational,
  SharedNotificationInputPresentational,
  SharedPersonOverlayPresentational,
  SharedInstitutionOverlayPresentational,
  SharedLicenseOverlayPresentational,
  SharedOrganizationalUnitOverlayPresentational,
  SharedOrganizationalUnitDisseminationPolicyOverlayPresentational,
  SharedFundingAgencyOverlayPresentational,
  SharedPackageButtonNavigatorPresentational,
  SharedIdentifiersPresentational,
  SharedDataUsePolicyPresentational,
  SharedDuaTypePresentational,
  SharedPendingAccessRequestPresentational,
  SharedOrganizationalUnitDisseminationPolicyParametersPresentational,
];
const directives = [
  SharedPendingAccessRequestButtonDirective,
];
const pipes = [];
const modules = [
  CommonModule,
  FormsModule,
  RouterModule,
  ReactiveFormsModule,
  FormlyModule,
  MaterialModule,
  FontAwesomeModule,
  TranslateModule,
  TextFieldModule,
  LayoutModule,
  DragDropModule,
  HighlightModule,
  TourMatMenuModule,
  SolidifyFrontendApplicationModule,
  SolidifyFrontendRichTextEditorModule,
  SolidifyFrontendTwitterModule,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...contents,
    ...presentationals,
    ...directives,
    ...pipes,
  ],
  imports: [
    ...modules,
    ImageCropperComponent,
    NgxsModule.forFeature([
      SharedState,
      SharedArchiveAclState,
      SharedArchiveTypeState,
      SharedLicenseState,
      SharedGlobalBannerState,
      SharedDepositState,
      SharedLanguageState,
      SharedPreservationPolicyState,
      SharedSubmissionPolicyState,
      SharedOrganizationalUnitState,
      SharedDisseminationPolicyState,
      SharedPersonState,
      SharedPersonInstitutionState,
      SharedInstitutionState,
      SharedRoleState,
      SharedOrganizationalUnitSubmissionPolicyState,
      SharedOrganizationalUnitPreservationPolicyState,
      SharedOrganizationalUnitDisseminationPolicyState,
      SharedFundingAgencyState,
      SharedSubjectAreaState,
      SharedNotificationState,
      SharedUserState,
      SharedContributorState,
      SharedOrderState,
      SharedOaiSetState,
      SharedOaiMetadataPrefixState,
      SharedMetadataTypeState,
      SharedAipState,
      SharedAipOrganizationalUnitState,
      SharedAipCollectionState,
      SharedAipStatusHistoryState,
      SharedAipCollectionStatusHistoryState,
      SharedAipDataFileState,
      SharedAipDataFileStatusHistoryState,
      SharedArchiveState,
      SharedIndexFieldAliasState,
      SharedDataCategoryState,
      SharedSubmissionAgreementState,
      SharedCitationState,
    ]),
  ],
  exports: [
    ...modules,
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
    ...contents,
    ...directives,
    ...pipes,
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {panelClass: "solidify-dialogs", hasBackdrop: true} as MatDialogConfig},
  ],
})
export class SharedModule {
  constructor(private readonly _library: FaIconLibrary) {
    // Fontawesome library
    _library.addIconPacks(fas);
  }
}
