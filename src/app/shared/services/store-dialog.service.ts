/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - store-dialog.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminArchiveAclActionNameSpace} from "@admin/archive-acl/stores/admin-archive-acl.action";
import {adminArchiveTypeActionNameSpace} from "@admin/archive-type/stores/admin-archive-type.action";
import {adminDisseminationPolicyActionNameSpace} from "@admin/dissemination-policy/stores/admin-dissemination-policy.action";
import {adminFundingAgenciesActionNameSpace} from "@admin/funding-agencies/stores/admin-funding-agencies.action";
import {adminInstitutionActionNameSpace} from "@admin/institution/stores/admin-institution.action";
import {adminLanguageActionNameSpace} from "@admin/language/stores/admin-language.action";
import {adminLicenseActionNameSpace} from "@admin/license/stores/admin-license.action";
import {adminMetadataTypeActionNameSpace} from "@admin/metadata-type/stores/admin-metadata-type.action";
import {adminOrganizationalUnitActionNameSpace} from "@admin/organizational-unit/stores/admin-organizational-unit.action";
import {adminPersonActionNameSpace} from "@admin/person/stores/admin-person.action";
import {adminPreservationPolicyActionNameSpace} from "@admin/preservation-policy/stores/admin-preservation-policy.action";
import {adminScheduledTaskActionNameSpace} from "@admin/scheduled-task/stores/admin-scheduled-task.action";
import {adminSubjectAreaActionNameSpace} from "@admin/subject-area/stores/admin-subject-area.action";
import {adminSubmissionAgreementActionNameSpace} from "@admin/submission-agreement/stores/admin-submission-agreement.action";
import {adminSubmissionPolicyActionNameSpace} from "@admin/submission-policy/stores/admin-submission-policy.action";
import {adminUserActionNameSpace} from "@admin/user/stores/admin-user.action";
import {
  Inject,
  Injectable,
} from "@angular/core";
import {depositActionNameSpace} from "@app/features/deposit/stores/deposit.action";
import {environment} from "@environments/environment";
import {orderAllOrderActionNameSpace} from "@order/features/all-order/stores/order-all-order.action";
import {orderMyOrderActionNameSpace} from "@order/features/my-order/stores/order-my-order.action";
import {preservationPlanningDipActionNameSpace} from "@preservation-planning/dip/stores/preservation-planning-dip.action";
import {preservationPlanningJobActionNameSpace} from "@preservation-planning/job/stores/preservation-planning-job.action";
import {preservationPlanningSipActionNameSpace} from "@preservation-planning/sip/stores/preservation-planning-sip.action";
import {preservationSpaceOrganizationalUnitArchiveAclActionNameSpace} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.action";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedAipActionNameSpace} from "@shared/features/aip/stores/shared-aip.action";
import {
  AbstractStoreDialogService,
  DeleteDialogData,
  isNotNullNorUndefined,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class StoreDialogService extends AbstractStoreDialogService {
  constructor(@Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface) {
    super(environment, _labelTranslate);
  }

  protected _deleteDataInternal(state: StateEnum): DeleteDialogData | undefined {
    const sharedDeleteDialogData = {} as DeleteDialogData;

    if (state === StateEnum.deposit) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("deposit.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = depositActionNameSpace;
    }
    if (state === StateEnum.deposit_dataFile) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("depositDataFile.dialog.delete.message");
    }
    if (state === StateEnum.deposit_collection) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("depositCollection.dialog.delete.message");
    }
    if (state === StateEnum.admin_organizationalUnit) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.organizationalUnit.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminOrganizationalUnitActionNameSpace;
    }
    if (state === StateEnum.admin_submissionAgreement) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.submissionAgreement.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminSubmissionAgreementActionNameSpace;
    }
    if (state === StateEnum.admin_submissionPolicy) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.submissionPolicy.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminSubmissionPolicyActionNameSpace;
    }
    if (state === StateEnum.admin_preservationPolicy) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.preservationPolicy.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminPreservationPolicyActionNameSpace;
    }
    if (state === StateEnum.admin_disseminationPolicy) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.disseminationPolicy.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminDisseminationPolicyActionNameSpace;
    }
    if (state === StateEnum.admin_license) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.license.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminLicenseActionNameSpace;
    }
    if (state === StateEnum.admin_institution) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.institution.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminInstitutionActionNameSpace;
    }
    if (state === StateEnum.admin_subjectArea) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.subjectArea.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminSubjectAreaActionNameSpace;
    }
    if (state === StateEnum.admin_user) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.user.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminUserActionNameSpace;
    }
    if (state === StateEnum.admin_person) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.person.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminPersonActionNameSpace;
    }
    if (state === StateEnum.admin_fundingAgencies) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.funding-agencies.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminFundingAgenciesActionNameSpace;
    }
    if (state === StateEnum.admin_archiveAcl) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.archive-acl.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminArchiveAclActionNameSpace;
    }
    if (state === StateEnum.admin_archiveType) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.archiveType.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminArchiveTypeActionNameSpace;
    }
    if (state === StateEnum.preservationSpace_organizationalUnit_archiveAcl) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.archive-acl.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = preservationSpaceOrganizationalUnitArchiveAclActionNameSpace;
    }
    if (state === StateEnum.admin_language) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.language.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminLanguageActionNameSpace;
    }
    if (state === StateEnum.admin_metadataType) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.metadataType.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminMetadataTypeActionNameSpace;
    }
    if (state === StateEnum.admin_scheduledTask) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("admin.scheduledTask.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = adminScheduledTaskActionNameSpace;
    }
    if (state === StateEnum.preservationPlanning_job) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("preservation.job.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = preservationPlanningJobActionNameSpace;
    }
    if (state === StateEnum.order_allOrder) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("order.allOrder.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = orderAllOrderActionNameSpace;
    }
    if (state === StateEnum.order_myOrder) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("order.myOrder.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = orderMyOrderActionNameSpace;
    }
    if (state === StateEnum.preservationPlanning_sip) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("preservation.sip.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = preservationPlanningSipActionNameSpace;
    }
    if (state === StateEnum.preservationPlanning_sip_dataFile) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("preservation.sipDataFile.dialog.delete.message");
    }
    if (state === StateEnum.preservationPlanning_dip) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("preservation.dip.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = preservationPlanningDipActionNameSpace;
    }
    if (state === StateEnum.preservationPlanning_dip_dataFile) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("preservation.dipDataFile.dialog.delete.message");
    }
    if (state === StateEnum.shared_aip) {
      sharedDeleteDialogData.message = MARK_AS_TRANSLATABLE("shared.aip.dialog.delete.message");
      sharedDeleteDialogData.resourceNameSpace = sharedAipActionNameSpace;
    }
    if (isNotNullNorUndefined(sharedDeleteDialogData.message)) {
      return sharedDeleteDialogData;
    }
    return undefined;
  }

}
