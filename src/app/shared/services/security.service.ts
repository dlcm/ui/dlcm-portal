/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - security.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {AppArchiveAclState} from "@app/stores/archive-acl/app-archive-acl.state";
import {AppAuthorizedInstitutionState} from "@app/stores/authorized-institution/app-authorized-institution.state";
import {AppMemberOrganizationalUnitState} from "@app/stores/member-organizational-unit/app-member-organizational-unit.state";
import {AppPersonInstitutionState} from "@app/stores/person/institution/app-people-institution.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Deposit,
  Institution,
  OrganizationalUnit,
  Role,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedDepositAction} from "@shared/stores/deposit/shared-deposit.action";
import {
  Observable,
  of,
} from "rxjs";
import {map} from "rxjs/operators";
import {
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  SolidifySecurityService,
  StoreUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class SecurityService extends SolidifySecurityService {

  private readonly _ORGUNIT_ROLE_NEED_ACCESS_TO_CLOSED_DATASET: Enums.Role.RoleEnum[] = [Enums.Role.RoleEnum.STEWARD, Enums.Role.RoleEnum.MANAGER];
  private readonly _ORGUNIT_ROLE_NEED_TO_EDIT: Enums.Role.RoleEnum[] = [Enums.Role.RoleEnum.MANAGER];
  private readonly _DEPOSIT_ROLE_NEED_TO_CREATE: Enums.Role.RoleEnum[] = [Enums.Role.RoleEnum.MANAGER, Enums.Role.RoleEnum.STEWARD, Enums.Role.RoleEnum.APPROVER, Enums.Role.RoleEnum.CREATOR];
  readonly DEPOSIT_ROLE_NEED_TO_EDIT: Enums.Role.RoleEnum[] = [Enums.Role.RoleEnum.MANAGER, Enums.Role.RoleEnum.STEWARD, Enums.Role.RoleEnum.APPROVER, Enums.Role.RoleEnum.CREATOR];
  private readonly _DEPOSIT_ROLE_NEED_TO_VALIDATE: Enums.Role.RoleEnum[] = [Enums.Role.RoleEnum.MANAGER, Enums.Role.RoleEnum.STEWARD, Enums.Role.RoleEnum.APPROVER];
  private readonly _DEPOSIT_STATUS_ALLOW_FULL_ALTERATION: Enums.Deposit.StatusEnum[] = [Enums.Deposit.StatusEnum.IN_PROGRESS];
  private readonly _DEPOSIT_STATUS_ALLOW_METADATA_ALTERATION: Enums.Deposit.StatusEnum[] = [Enums.Deposit.StatusEnum.EDITING_METADATA];
  private readonly _DEPOSIT_STATUS_ALLOW_VALIDATION: Enums.Deposit.StatusEnum[] = [Enums.Deposit.StatusEnum.IN_VALIDATION];

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions) {
    super(_store, _actions$, environment);
  }

  canCreateDepositOnOrgUnit(orgUnitId: string, adminBypassOrgUnitRole: boolean = true): boolean {
    if (isNullOrUndefinedOrWhiteString(orgUnitId)) {
      return false;
    }
    if (this.isRootOrAdminIfBypassRole(adminBypassOrgUnitRole)) {
      return true;
    }
    if (!this.isMemberOfOrgUnitAboveVisitor(orgUnitId)) {
      return false;
    }
    return this.currentUserHaveRoleInListForOrgUnit(orgUnitId, this._DEPOSIT_ROLE_NEED_TO_CREATE);
  }

  isManagerOfAnyInstitution(institutionList: Institution[] = this.getListAuthorizedInstitutions()): boolean {
    return institutionList?.some(o => this.isManagerOfInstitution(o.resId));
  }

  isStewardOfAnyOrgUnit(orgUnitList: OrganizationalUnit[] = this.getListMemberOrgUnits()): boolean {
    return orgUnitList?.some(o => this.isStewardOfOrgUnit(o.resId));
  }

  isApproverOfAnyOrgUnit(orgUnitList: OrganizationalUnit[] = this.getListMemberOrgUnits()): boolean {
    return orgUnitList?.some(o => this.isApproverOfOrgUnit(o.resId));
  }

  private _isRoleInOrgUnit(expectedRole: Enums.Role.RoleEnum[], orgUnitId: string, adminBypassOrgUnitRole: boolean = true): boolean {
    if (isNullOrUndefinedOrWhiteString(orgUnitId)) {
      return false;
    }
    if (this.isRootOrAdminIfBypassRole(adminBypassOrgUnitRole)) {
      return true;
    }
    if (!this.isMemberOfOrgUnit(orgUnitId)) {
      return false;
    }
    return this.currentUserHaveRoleInListForOrgUnit(orgUnitId, expectedRole);
  }

  private _isRoleInInstitution(expectedRole: Enums.Role.RoleEnum[], institutionId: string, adminBypassOrgUnitRole: boolean = true): boolean {
    if (isNullOrUndefinedOrWhiteString(institutionId)) {
      return false;
    }
    if (this.isRootOrAdminIfBypassRole(adminBypassOrgUnitRole)) {
      return true;
    }
    if (!this.isMemberWithRoleOnInstitution(institutionId)) {
      return false;
    }
    return this._currentUserHaveRoleInListForInstitution(institutionId, expectedRole);
  }

  isManagerOfOrgUnit(orgUnitId: string, adminBypassOrgUnitRole: boolean = true): boolean {
    return this._isRoleInOrgUnit([Enums.Role.RoleEnum.MANAGER], orgUnitId, adminBypassOrgUnitRole);
  }

  isManagerOfInstitution(institutionId: string, adminBypassOrgUnitRole: boolean = true): boolean {
    return this._isRoleInInstitution([Enums.Role.RoleEnum.MANAGER], institutionId, adminBypassOrgUnitRole);
  }

  isStewardOfOrgUnit(orgUnitId: string, adminBypassOrgUnitRole: boolean = true): boolean {
    if (this.isRootOrAdminIfBypassRole(adminBypassOrgUnitRole)) {
      return true;
    }
    return this.currentUserHaveRoleInListForOrgUnit(orgUnitId, [Enums.Role.RoleEnum.STEWARD, Enums.Role.RoleEnum.MANAGER]);
  }

  isApproverOfOrgUnit(orgUnitId: string, adminBypassOrgUnitRole: boolean = true): boolean {
    if (this.isRootOrAdminIfBypassRole(adminBypassOrgUnitRole)) {
      return true;
    }
    return this.currentUserHaveRoleInListForOrgUnit(orgUnitId, [Enums.Role.RoleEnum.APPROVER, Enums.Role.RoleEnum.STEWARD, Enums.Role.RoleEnum.MANAGER]);
  }

  isActiveArchiveAcl(archiveId: string): boolean {
    const listAcl = MemoizedUtil.listSnapshot(this._store, AppArchiveAclState);
    const archiveAcl = listAcl.find(a => a.aipId === archiveId);
    return isNotNullNorUndefined(archiveAcl) && isFalse(archiveAcl.expired);
  }

  canSeeDetailDeposit(deposit: Deposit, adminBypassOrgUnitRole: boolean = true): boolean {
    if (this.isRootOrAdminIfBypassRole(adminBypassOrgUnitRole)) {
      return true;
    }
    return this.isMemberOfOrgUnit(deposit.organizationalUnitId);
  }

  canSeeDetailDepositByIdObs(depositId: string, adminBypassOrgUnitRole: boolean = true): Observable<boolean> {
    if (this.isRootOrAdminIfBypassRole(adminBypassOrgUnitRole)) {
      return of(true);
    }

    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(this._store, [{
      action: new SharedDepositAction.GetById(depositId),
      subActionCompletions: [
        this._actions$.pipe(ofSolidifyActionCompleted(SharedDepositAction.GetByIdSuccess)),
        this._actions$.pipe(ofSolidifyActionCompleted(SharedDepositAction.GetByIdFail)),
      ],
    }]).pipe(
      map(result => {
        if (result.success) {
          const organizationalUnitId = (result.listActionSuccess[0] as SharedDepositAction.GetByIdSuccess).model.organizationalUnitId;
          return this.isMemberOfOrgUnit(organizationalUnitId);
        }
        return false;
      }),
    );
  }

  canEditDeposit(deposit: Deposit, adminBypassOrgUnitRole: boolean = true): DepositEditModeEnum {
    const depositEditMode = this.depositEditionModeStep(deposit);
    if (depositEditMode === DepositEditModeEnum.none) {
      return DepositEditModeEnum.none;
    }
    if (this.isRootOrAdminIfBypassRole(adminBypassOrgUnitRole)) {
      return depositEditMode;
    }
    const currentRoleOnOrgUnit = this.getRoleInOrgUnit(deposit.organizationalUnitId);
    if (isNullOrUndefined(currentRoleOnOrgUnit)) {
      return DepositEditModeEnum.none;
    }
    if (SecurityService._isExpectedRoleOnListAuthorized(currentRoleOnOrgUnit, this.DEPOSIT_ROLE_NEED_TO_EDIT)) {
      return depositEditMode;
    }
    return DepositEditModeEnum.none;
  }

  isRootOrAdminIfBypassRole(adminBypassOrgUnitRole: boolean = true): boolean {
    if (adminBypassOrgUnitRole) {
      return super.isRootOrAdmin();
    }
    return super.isRoot();
  }

  canApproveOrRejectDeposit(deposit: Deposit, adminBypassOrgUnitRole: boolean = true): boolean {
    if (isFalse(this._depositInValidationStep(deposit))) {
      return false;
    }
    if (this.isRootOrAdminIfBypassRole(adminBypassOrgUnitRole)) {
      return true;
    }
    const currentRoleOnOrgUnit = this.getRoleInOrgUnit(deposit.organizationalUnitId);
    if (isNullOrUndefined(currentRoleOnOrgUnit)) {
      return false;
    }
    return SecurityService._isExpectedRoleOnListAuthorized(currentRoleOnOrgUnit, this._DEPOSIT_ROLE_NEED_TO_VALIDATE);
  }

  private _depositInValidationStep(deposit: Deposit): boolean {
    return SecurityService._depositInState(deposit, this._DEPOSIT_STATUS_ALLOW_VALIDATION);
  }

  depositEditionModeStep(deposit: Deposit): DepositEditModeEnum {
    if (SecurityService._depositInState(deposit, this._DEPOSIT_STATUS_ALLOW_METADATA_ALTERATION)) {
      return DepositEditModeEnum.metadata;
    }
    if (SecurityService._depositInState(deposit, this._DEPOSIT_STATUS_ALLOW_FULL_ALTERATION)) {
      return DepositEditModeEnum.full;
    }
    return DepositEditModeEnum.none;
  }

  private static _depositInState(deposit: Deposit, inState: Enums.Deposit.StatusEnum[]): boolean {
    return inState.includes(deposit.status);
  }

  private static _isExpectedRoleOnListAuthorized(currentRoleOnOrgUnit: Role, listRolesAuthorized: Enums.Role.RoleEnum[]): boolean {
    return listRolesAuthorized.includes(currentRoleOnOrgUnit?.resId as Enums.Role.RoleEnum);
  }

  currentUserHaveRoleInListForOrgUnit(orgUnitId: string, listRolesAuthorized: Enums.Role.RoleEnum[]): boolean {
    const currentRoleInOrgUnit = this.getRoleInOrgUnit(orgUnitId);
    if (isNullOrUndefined(currentRoleInOrgUnit)) {
      return false;
    }
    return SecurityService._isExpectedRoleOnListAuthorized(currentRoleInOrgUnit, listRolesAuthorized);
  }

  private _currentUserHaveRoleInListForInstitution(institutionId: string, listRolesAuthorized: Enums.Role.RoleEnum[]): boolean {
    const currentRoleInInstitution = this.getRoleInInstitution(institutionId);
    if (isNullOrUndefined(currentRoleInInstitution)) {
      return false;
    }
    return SecurityService._isExpectedRoleOnListAuthorized(currentRoleInInstitution, listRolesAuthorized);
  }

  getRoleInOrgUnit(orgUnitId: string): Role | undefined {
    return this.getListMemberOrgUnits()?.find(o => isNotNullNorUndefined(o.role) && o.resId === orgUnitId)?.role;
  }

  getRoleInInstitution(institutionId: string): Role | undefined {
    return this.getListAuthorizedInstitutions()?.find(o => isNotNullNorUndefined(o.role) && o.resId === institutionId)?.role;
  }

  getListMemberOrgUnits(): OrganizationalUnit[] {
    return MemoizedUtil.listSnapshot(this._store, AppMemberOrganizationalUnitState);
  }

  getListAuthorizedInstitutions(): Institution[] {
    return MemoizedUtil.listSnapshot(this._store, AppAuthorizedInstitutionState);
  }

  getListMemberInstitutions(): Institution[] {
    return MemoizedUtil.selectedSnapshot(this._store, AppPersonInstitutionState);
  }

  getRoleEnumInOrgUnit(orgUnitId: string): Enums.Role.RoleEnum | undefined {
    return this.getRoleInOrgUnit(orgUnitId)?.resId as Enums.Role.RoleEnum;
  }

  getRoleEnumInInstitution(institutionId: string): Enums.Role.RoleEnum | undefined {
    return this.getRoleInInstitution(institutionId)?.resId as Enums.Role.RoleEnum;
  }

  isMemberOfOrgUnit(orgUnitId: string): boolean {
    return isNotNullNorUndefined(this.getRoleInOrgUnit(orgUnitId));
  }

  isMemberOfOrgUnitAboveVisitor(orgUnitId: string): boolean {
    const role: Role = this.getRoleInOrgUnit(orgUnitId);
    return isNotNullNorUndefined(role) && role.resId !== Enums.Role.RoleEnum.VISITOR;
  }

  isMemberWithRoleOnInstitution(institutionId: string): boolean {
    const role = this.getRoleInInstitution(institutionId);
    return isNotNullNorUndefined(role);
  }

  isMemberWithoutRoleOnInstitution(institutionId: string): boolean {
    return this.getListMemberInstitutions()?.findIndex(o => o.resId === institutionId) >= 0;
  }

  isMemberWithOrWithoutRoleOnInstitution(institutionId: string): boolean {
    return this.isMemberWithRoleOnInstitution(institutionId) || this.isMemberWithoutRoleOnInstitution(institutionId);
  }

  verifiedOrcid(): boolean {
    const currentUser = MemoizedUtil.currentSnapshot(this._store, AppUserState);
    return isTrue(currentUser?.person?.verifiedOrcid);
  }
}
