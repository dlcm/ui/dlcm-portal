/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app-tour.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {DepositTabStatusEnum} from "@deposit/enums/deposit-tab-status.enum";
import {Store} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  DepositRoutesEnum,
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StepTourSectionNameEnum} from "@shared/enums/step-tour-section-name.enum";
import {TourRouteIdEnum} from "@shared/enums/tour-route-id.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {
  IStepOption,
  TourService,
} from "ngx-ui-tour-md-menu";
import {
  AbstractAppTourService,
  DefaultSolidifyEnvironment,
  ENVIRONMENT,
  ITourSection,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class AppTourService extends AbstractAppTourService {
  constructor(protected readonly _store: Store,
              protected readonly _matDialog: MatDialog,
              protected readonly _tourService: TourService,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface,
              @Inject(ENVIRONMENT) protected readonly _environment: DefaultSolidifyEnvironment) {
    super(_store, _matDialog, _tourService, tourSteps, _labelTranslate, _environment);
  }
}

const mainTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.mainMenuOrder,
    content: MARK_AS_TRANSLATABLE("tour.main.menuArchiveOrders.content"),
  },
  {
    anchorId: TourEnum.mainMenuDeposit,
    content: MARK_AS_TRANSLATABLE("tour.main.menuDeposit.content"),
  },
  {
    anchorId: TourEnum.mainMenuPreservationSpace,
    content: MARK_AS_TRANSLATABLE("tour.main.menuPreservationSpace.content"),
  },
  {
    anchorId: TourEnum.mainSearchBar,
    content: MARK_AS_TRANSLATABLE("tour.main.searchBar.content"),
    route: RoutesEnum.homePage,
  },
];

const depositListTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.commonContent,
    title: LabelTranslateEnum.deposit,
    content: MARK_AS_TRANSLATABLE("tour.deposit.list.intro.content"),
    route: RoutesEnum.deposit + urlSeparator + TourRouteIdEnum.tourOrgUnitId + urlSeparator + DepositTabStatusEnum.inProgress,
  },
  {
    anchorId: TourEnum.depositCreate,
    content: MARK_AS_TRANSLATABLE("tour.deposit.list.create.content"),
  },
  {
    anchorId: TourEnum.depositStatusTabs,
    content: MARK_AS_TRANSLATABLE("tour.deposit.list.statusTabs.content"),
  },
  {
    anchorId: TourEnum.depositOrgUnitSelector,
    content: MARK_AS_TRANSLATABLE("tour.deposit.list.orgunitSelector.content"),
  },
];

const depositMetadataTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.depositDetail,
    content: MARK_AS_TRANSLATABLE("tour.deposit.metadata.intro.content"),
    route: RoutesEnum.deposit + urlSeparator + TourRouteIdEnum.tourOrgUnitId + urlSeparator + DepositRoutesEnum.detail + urlSeparator + TourRouteIdEnum.tourDepositId + urlSeparator + DepositRoutesEnum.metadata,
  },
  {
    anchorId: TourEnum.depositMetadataThumbnail,
    content: MARK_AS_TRANSLATABLE("tour.deposit.metadata.thumbnail.content"),
  },
];

const depositDataTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.depositDataUpload,
    content: MARK_AS_TRANSLATABLE("tour.deposit.data.upload.content"),
    route: RoutesEnum.deposit + urlSeparator + TourRouteIdEnum.tourOrgUnitId + urlSeparator + DepositRoutesEnum.detail + urlSeparator + TourRouteIdEnum.tourDepositId + urlSeparator + DepositRoutesEnum.data,
  },
  {
    anchorId: TourEnum.depositDetail,
    content: MARK_AS_TRANSLATABLE("tour.deposit.data.remindTooltip.content"),
  },
];

const preservationSpaceTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.preservationSpaceTabs,
    title: LabelTranslateEnum.preservationSpace,
    content: MARK_AS_TRANSLATABLE("tour.preservationSpace.tabs.content"),
    route: RoutesEnum.preservationSpace,
  },
];

const preservationSpaceOrgUnitTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.preservationSpaceOrgUnitTab,
    title: LabelTranslateEnum.organizationalUnit,
    content: MARK_AS_TRANSLATABLE("tour.preservationSpace.orgUnit.tab.content"),
    route: RoutesEnum.preservationSpaceOrganizationalUnitDetail + urlSeparator + TourRouteIdEnum.tourOrgUnitId,
  },
  {
    anchorId: TourEnum.preservationSpaceOrgUnitThumbnail,
    content: MARK_AS_TRANSLATABLE("tour.preservationSpace.orgUnit.thumbnail.content"),
  },
  {
    anchorId: TourEnum.preservationSpaceOrgUnitPersonRole,
    content: MARK_AS_TRANSLATABLE("tour.preservationSpace.orgUnit.personRole.content"),
  },
];

const homeSearchTourSteps: IStepOption[] = [
  {
    anchorId: TourEnum.commonContent,
    title: LabelTranslateEnum.search,
    content: MARK_AS_TRANSLATABLE("tour.home.intro.content"),
    route: RoutesEnum.homeSearch,
  },
  {
    anchorId: TourEnum.homeSearchSearchBar,
    content: MARK_AS_TRANSLATABLE("tour.home.searchBar.content"),
  },
  {
    anchorId: TourEnum.homeSearchListTiles,
    content: MARK_AS_TRANSLATABLE("tour.home.listTiles.content"),
  },
  {
    anchorId: TourEnum.homeSearchFacets,
    content: MARK_AS_TRANSLATABLE("tour.home.facets.content"),
  },
];

const tourSteps: ITourSection[] = [
  {
    key: StepTourSectionNameEnum.main,
    stepTour: mainTourSteps,
  },
  {
    key: StepTourSectionNameEnum.deposit,
    stepTour: depositListTourSteps,
  },
  {
    key: StepTourSectionNameEnum.depositMetadata,
    stepTour: depositMetadataTourSteps,
  },
  {
    key: StepTourSectionNameEnum.depositData,
    stepTour: depositDataTourSteps,
  },
  {
    key: StepTourSectionNameEnum.preservationSpace,
    stepTour: preservationSpaceTourSteps,
  },
  {
    key: StepTourSectionNameEnum.organizationalUnit,
    stepTour: preservationSpaceOrgUnitTourSteps,
  },
  {
    key: StepTourSectionNameEnum.search,
    stepTour: homeSearchTourSteps,
  },
];
