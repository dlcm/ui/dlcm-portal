/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - archive-dissemination-policy.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {DLCM_CONSTANTS} from "@app/constants";
import {Enums} from "@enums";
import {Archive} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {OrganizationalUnitDisseminationPolicyHelper} from "@shared/helpers/organizational-unit-dissemination-policy.helper";
import {SharedOrganizationalUnitDisseminationPolicyAction} from "@shared/stores/organizational-unit/dissemination-policy/shared-organizational-unit-dissemination-policy.action";
import {KeyValueOrgUnitDisseminationPolicyType} from "@shared/types/key-value-org-unit-dissemination-policy.type";
import {
  Observable,
  of,
  switchMap,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  map,
} from "rxjs/operators";
import {
  ArrayUtil,
  isNotNullNorUndefinedNorEmptyArray,
  isNullOrUndefinedOrEmptyArray,
  StoreUtil,
} from "solidify-frontend";

@Injectable({providedIn: "root"})
export class ArchiveDisseminationPolicyService {
  readonly LIST_DEFAULT_DISSEMINATION_POLICIES_OPTIONS: KeyValueOrgUnitDisseminationPolicyType[] = [
    {
      key: DLCM_CONSTANTS.DISSEMINATION_POLICY_BASIC_ID,
      value: LabelTranslateEnum.basic,
      placeholderToTranslate: this._getPlaceholderForDisseminationPolicy(Enums.DisseminationPolicy.TypeEnum.BASIC),
      icon: this._getIconForDisseminationPolicy(Enums.DisseminationPolicy.TypeEnum.BASIC),
    },
    {
      key: DLCM_CONSTANTS.DISSEMINATION_POLICY_OAIS_ID,
      value: LabelTranslateEnum.oais,
      placeholderToTranslate: this._getPlaceholderForDisseminationPolicy(Enums.DisseminationPolicy.TypeEnum.OAIS),
      icon: this._getIconForDisseminationPolicy(Enums.DisseminationPolicy.TypeEnum.OAIS),
    },
  ];

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _translateService: TranslateService) {
  }

  getListDisseminationPolicyOptionsFromListArchive(listArchivesObs: Observable<Archive[]>): Observable<KeyValueOrgUnitDisseminationPolicyType[]> {
    return listArchivesObs.pipe(
      distinctUntilChanged(),
      filter(list => isNotNullNorUndefinedNorEmptyArray(list)),
      map(list => ArrayUtil.distinct(list.map(a => a.organizationalUnitId))),
      distinctUntilChanged((a, b) => {
        const diff = ArrayUtil.diff(a, b).diff;
        return isNullOrUndefinedOrEmptyArray(diff);
      }),
      switchMap(listConcernedOrgUnit => {
        if (listConcernedOrgUnit.length === 1) {
          return this.getListDisseminationPolicyOptionsForOrganizationalUnitId(listConcernedOrgUnit[0]);
        }
        return of(this.LIST_DEFAULT_DISSEMINATION_POLICIES_OPTIONS);
      }),
    );
  }

  getListDisseminationPolicyOptionsForOrganizationalUnitId(organizationalUnitId: string): Observable<KeyValueOrgUnitDisseminationPolicyType[]> {
    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedOrganizationalUnitDisseminationPolicyAction.GetAll(organizationalUnitId),
      SharedOrganizationalUnitDisseminationPolicyAction.GetAllSuccess,
      result => {}).pipe(
      map((resultAction: SharedOrganizationalUnitDisseminationPolicyAction.GetAllSuccess) => {
        const list = OrganizationalUnitDisseminationPolicyHelper.fillComputedNameInList(this._translateService, resultAction.list._data);
        return [
          ...this.LIST_DEFAULT_DISSEMINATION_POLICIES_OPTIONS,
          ...list.map(r => ({
            key: r.resId,
            value: r.computedName ?? r.name,
            placeholderToTranslate: this._getPlaceholderForDisseminationPolicy(r.type),
            icon: this._getIconForDisseminationPolicy(r.type),
            object: r,
          })),
        ];
      }));
  }

  private _getPlaceholderForDisseminationPolicy(disseminationPolicyType: Enums.DisseminationPolicy.TypeEnum): string | undefined {
    switch (disseminationPolicyType) {
      case Enums.DisseminationPolicy.TypeEnum.BASIC:
        return LabelTranslateEnum.formatForBasic;
      case Enums.DisseminationPolicy.TypeEnum.OAIS:
        return LabelTranslateEnum.formatForOais;
      case Enums.DisseminationPolicy.TypeEnum.HEDERA:
        return LabelTranslateEnum.formatForHedera;
      case Enums.DisseminationPolicy.TypeEnum.IIIF:
        return LabelTranslateEnum.formatForIiif;
      default:
        return undefined;
    }
  }

  private _getIconForDisseminationPolicy(disseminationPolicyType: Enums.DisseminationPolicy.TypeEnum): IconNameEnum | undefined {
    switch (disseminationPolicyType) {
      case Enums.DisseminationPolicy.TypeEnum.BASIC:
        return IconNameEnum.files;
      case Enums.DisseminationPolicy.TypeEnum.OAIS:
        return IconNameEnum.archivist;
      case Enums.DisseminationPolicy.TypeEnum.HEDERA:
        return IconNameEnum.hedera;
      case Enums.DisseminationPolicy.TypeEnum.IIIF:
        return IconNameEnum.iiif;
      default:
        return undefined;
    }
  }
}
