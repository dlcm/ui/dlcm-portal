/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - store-route.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {AbstractStoreRouteService} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class StoreRouteService extends AbstractStoreRouteService {
  constructor() {
    super(environment);
  }

  protected _getCreateRouteInternal(state: StateEnum): string | undefined {
    if (state === StateEnum.deposit) {
      return RoutesEnum.depositCreate;
    }
    if (state === StateEnum.admin_organizationalUnit) {
      return RoutesEnum.adminOrganizationalUnitCreate;
    }
    if (state === StateEnum.admin_submissionAgreement) {
      return RoutesEnum.adminSubmissionAgreementCreate;
    }
    if (state === StateEnum.admin_submissionPolicy) {
      return RoutesEnum.adminSubmissionPolicyCreate;
    }
    if (state === StateEnum.admin_preservationPolicy) {
      return RoutesEnum.adminPreservationPolicyCreate;
    }
    if (state === StateEnum.admin_disseminationPolicy) {
      return RoutesEnum.adminDisseminationPolicyCreate;
    }
    if (state === StateEnum.admin_license) {
      return RoutesEnum.adminLicenseCreate;
    }
    if (state === StateEnum.admin_institution) {
      return RoutesEnum.adminInstitutionCreate;
    }
    if (state === StateEnum.admin_person) {
      return RoutesEnum.adminPersonCreate;
    }
    if (state === StateEnum.admin_role) {
      return RoutesEnum.adminRoleCreate;
    }
    if (state === StateEnum.admin_notification) {
      return RoutesEnum.adminNotificationCreate;
    }
    if (state === StateEnum.admin_fundingAgencies) {
      return RoutesEnum.adminFundingAgenciesCreate;
    }
    if (state === StateEnum.admin_archiveAcl) {
      return RoutesEnum.adminArchiveAclCreate;
    }
    if (state === StateEnum.admin_archiveType) {
      return RoutesEnum.adminArchiveTypeCreate;
    }
    if (state === StateEnum.admin_metadataType) {
      return RoutesEnum.adminMetadataTypeCreate;
    }
    if (state === StateEnum.admin_scheduledTask) {
      return RoutesEnum.adminScheduledTaskCreate;
    }
    if (state === StateEnum.preservationPlanning_job) {
      return RoutesEnum.preservationPlanningJobCreate;
    }
    if (state === StateEnum.order_allOrder) {
      return RoutesEnum.orderAllOrderCreate;
    }
    return undefined;
  }

  protected _getDetailRouteInternal(state: StateEnum): string | undefined {
    if (state === StateEnum.deposit) {
      return RoutesEnum.depositDetail;
    }
    if (state === StateEnum.preservationSpace_organizationalUnit) {
      return RoutesEnum.preservationSpaceOrganizationalUnitDetail;
    }
    if (state === StateEnum.preservationSpace_institution) {
      return RoutesEnum.preservationSpaceInstitutionDetail;
    }
    if (state === StateEnum.admin_organizationalUnit) {
      return RoutesEnum.adminOrganizationalUnitDetail;
    }
    if (state === StateEnum.admin_submissionAgreement) {
      return RoutesEnum.adminSubmissionAgreementDetail;
    }
    if (state === StateEnum.admin_submissionPolicy) {
      return RoutesEnum.adminSubmissionPolicyDetail;
    }
    if (state === StateEnum.admin_preservationPolicy) {
      return RoutesEnum.adminPreservationPolicyDetail;
    }
    if (state === StateEnum.admin_disseminationPolicy) {
      return RoutesEnum.adminDisseminationPolicyDetail;
    }
    if (state === StateEnum.admin_license) {
      return RoutesEnum.adminLicenseDetail;
    }
    if (state === StateEnum.admin_institution) {
      return RoutesEnum.adminInstitutionDetail;
    }
    if (state === StateEnum.admin_user) {
      return RoutesEnum.adminUserDetail;
    }
    if (state === StateEnum.admin_person) {
      return RoutesEnum.adminPersonDetail;
    }
    if (state === StateEnum.admin_role) {
      return RoutesEnum.adminRoleDetail;
    }
    if (state === StateEnum.admin_notification) {
      return RoutesEnum.adminNotificationDetail;
    }
    if (state === StateEnum.admin_fundingAgencies) {
      return RoutesEnum.adminFundingAgenciesDetail;
    }
    if (state === StateEnum.admin_archiveAcl) {
      return RoutesEnum.adminArchiveAclDetail;
    }
    if (state === StateEnum.admin_archiveType) {
      return RoutesEnum.adminArchiveTypeDetail;
    }
    if (state === StateEnum.admin_metadataType) {
      return RoutesEnum.adminMetadataTypeDetail;
    }
    if (state === StateEnum.admin_scheduledTask) {
      return RoutesEnum.adminScheduledTaskDetail;
    }
    if (state === StateEnum.preservationPlanning_job) {
      return RoutesEnum.preservationPlanningJobDetail;
    }
    if (state === StateEnum.preservationPlanning_sip) {
      return RoutesEnum.preservationPlanningSipDetail;
    }
    if (state === StateEnum.preservationPlanning_dip) {
      return RoutesEnum.preservationPlanningDipDetail;
    }
    if (state === StateEnum.preservationPlanning_aipDownloaded) {
      return RoutesEnum.preservationPlanningAipDownloadedDetail;
    }
    if (state === StateEnum.order_allOrder) {
      return RoutesEnum.orderAllOrderDetail;
    }
    if (state === StateEnum.order_myOrder) {
      return RoutesEnum.orderMyOrderDetail;
    }
    if (state === StateEnum.preservationSpace_contributor) {
      return RoutesEnum.preservationSpaceContributorDetail;
    }
    if (state === StateEnum.preservationSpace_notification) {
      return RoutesEnum.preservationSpaceNotificationInboxDetail;
    }
    if (state === StateEnum.shared_aip) {
      return ""; // override in component routable depending of mode : AIP / AIP Download / AIP Steward
    }
    return undefined;
  }

  protected _getRootRouteInternal(state: StateEnum): string | undefined {
    if (state === StateEnum.deposit) {
      return RoutesEnum.deposit;
    }
    if (state === StateEnum.preservationSpace_organizationalUnit) {
      return RoutesEnum.preservationSpaceOrganizationalUnit;
    }
    if (state === StateEnum.preservationSpace_institution) {
      return RoutesEnum.preservationSpaceInstitution;
    }
    if (state === StateEnum.admin_organizationalUnit) {
      return RoutesEnum.adminOrganizationalUnit;
    }
    if (state === StateEnum.admin_submissionAgreement) {
      return RoutesEnum.adminSubmissionAgreement;
    }
    if (state === StateEnum.admin_submissionPolicy) {
      return RoutesEnum.adminSubmissionPolicy;
    }
    if (state === StateEnum.admin_preservationPolicy) {
      return RoutesEnum.adminPreservationPolicy;
    }
    if (state === StateEnum.admin_disseminationPolicy) {
      return RoutesEnum.adminDisseminationPolicy;
    }
    if (state === StateEnum.admin_license) {
      return RoutesEnum.adminLicense;
    }
    if (state === StateEnum.admin_institution) {
      return RoutesEnum.adminInstitution;
    }
    if (state === StateEnum.admin_subjectArea) {
      return RoutesEnum.adminSubjectArea;
    }
    if (state === StateEnum.admin_user) {
      return RoutesEnum.adminUser;
    }
    if (state === StateEnum.admin_person) {
      return RoutesEnum.adminPerson;
    }
    if (state === StateEnum.admin_role) {
      return RoutesEnum.adminRole;
    }
    if (state === StateEnum.admin_notification) {
      return RoutesEnum.adminNotification;
    }
    if (state === StateEnum.admin_fundingAgencies) {
      return RoutesEnum.adminFundingAgencies;
    }
    if (state === StateEnum.admin_archiveAcl) {
      return RoutesEnum.adminArchiveAcl;
    }
    if (state === StateEnum.admin_archiveType) {
      return RoutesEnum.adminArchiveType;
    }
    if (state === StateEnum.admin_metadataType) {
      return RoutesEnum.adminMetadataType;
    }
    if (state === StateEnum.admin_scheduledTask) {
      return RoutesEnum.adminScheduledTask;
    }
    if (state === StateEnum.preservationPlanning_job) {
      return RoutesEnum.preservationPlanningJob;
    }
    if (state === StateEnum.preservationPlanning_sip) {
      return RoutesEnum.preservationPlanningSip;
    }
    if (state === StateEnum.preservationPlanning_deposit) {
      return RoutesEnum.preservationPlanningDeposit;
    }
    if (state === StateEnum.order_allOrder) {
      return RoutesEnum.orderAllOrder;
    }
    if (state === StateEnum.order_myOrder) {
      return RoutesEnum.orderMyOrder;
    }
    if (state === StateEnum.preservationPlanning_dip) {
      return RoutesEnum.preservationPlanningDip;
    }
    if (state === StateEnum.preservationPlanning_aipDownloaded) {
      return RoutesEnum.preservationPlanningAipDownloaded;
    }
    if (state === StateEnum.preservationSpace_contributor) {
      return RoutesEnum.preservationSpaceContributor;
    }
    if (state === StateEnum.preservationSpace_notification) {
      return RoutesEnum.preservationSpaceNotificationInbox;
    }
    if (state === StateEnum.shared_aip) {
      return ""; // override in component routable depending of mode : AIP / AIP Download / AIP Steward
    }
    return undefined;
  }
}
