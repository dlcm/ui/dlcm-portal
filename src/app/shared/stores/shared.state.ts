/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  SharedInstitutionState,
  SharedInstitutionStateModel,
} from "@app/shared/stores/institution/shared-institution.state";
import {
  SharedLanguageState,
  SharedLanguageStateModel,
} from "@app/shared/stores/language/shared-language.state";
import {
  SharedLicenseState,
  SharedLicenseStateModel,
} from "@app/shared/stores/license/shared-license.state";
import {
  SharedOrganizationalUnitState,
  SharedOrganizationalUnitStateModel,
} from "@app/shared/stores/organizational-unit/shared-organizational-unit.state";
import {
  SharedPersonState,
  SharedPersonStateModel,
} from "@app/shared/stores/person/shared-person.state";
import {
  SharedPreservationPolicyState,
  SharedPreservationPolicyStateModel,
} from "@app/shared/stores/preservation-policy/shared-preservation-policy.state";
import {
  SharedSubmissionPolicyState,
  SharedSubmissionPolicyStateModel,
} from "@app/shared/stores/submission-policy/shared-submission-policy.state";
import {State} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedAipState} from "@shared/features/aip/stores/shared-aip.state";
import {
  SharedArchiveAclState,
  SharedArchiveAclStateModel,
} from "@shared/stores/archive-acl/shared-archive-acl.state";
import {
  SharedArchiveTypeState,
  SharedArchiveTypeStateModel,
} from "@shared/stores/archive-type/shared-archive-type.state";
import {
  SharedArchiveState,
  SharedArchiveStateModel,
} from "@shared/stores/archive/shared-archive.state";
import {
  SharedCitationState,
  SharedCitationStateModel,
} from "@shared/stores/citations/shared-citation.state";
import {SharedContributorState} from "@shared/stores/contributor/shared-contributor.state";
import {
  SharedDataCategoryState,
  SharedDataCategoryStateModel,
} from "@shared/stores/data-category/shared-data-category.state";
import {
  SharedDepositState,
  SharedDepositStateModel,
} from "@shared/stores/deposit/shared-deposit.state";
import {
  SharedDisseminationPolicyState,
  SharedDisseminationPolicyStateModel,
} from "@shared/stores/dissemination-policy/shared-dissemination-policy.state";
import {
  SharedFundingAgencyState,
  SharedFundingAgencyStateModel,
} from "@shared/stores/funding-agency/shared-funding-agency.state";
import {SharedMetadataTypeState} from "@shared/stores/metadata-type/shared-metadata-type.state";
import {
  SharedNotificationState,
  SharedNotificationStateModel,
} from "@shared/stores/notification/shared-notification.state";
import {SharedOrderState} from "@shared/stores/order/shared-order.state";
import {
  SharedRoleState,
  SharedRoleStateModel,
} from "@shared/stores/role/shared-role.state";
import {
  SharedSubjectAreaState,
  SharedSubjectAreaStateModel,
} from "@shared/stores/subject-area/shared-subject-area.state";
import {
  SharedSubmissionAgreementState,
  SharedSubmissionAgreementStateModel,
} from "@shared/stores/submission-agreement/shared-submission-agreement.state";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {
  BaseStateModel,
  SharedGlobalBannerState,
  SharedGlobalBannerStateModel,
  SharedIndexFieldAliasState,
  SharedIndexFieldAliasStateModel,
  SharedOaiMetadataPrefixState,
  SharedOaiMetadataPrefixStateModel,
  SharedOaiSetState,
  SharedOaiSetStateModel,
} from "solidify-frontend";

export interface SharedStateModel extends BaseStateModel {
  [StateEnum.shared_organizationalUnit]: SharedOrganizationalUnitStateModel;
  [StateEnum.shared_deposit]: SharedDepositStateModel;
  [StateEnum.shared_language]: SharedLanguageStateModel;
  [StateEnum.shared_license]: SharedLicenseStateModel;
  [StateEnum.shared_globalBanner]: SharedGlobalBannerStateModel;
  [StateEnum.shared_submissionPolicy]: SharedSubmissionPolicyStateModel;
  [StateEnum.shared_submissionAgreement]: SharedSubmissionAgreementStateModel;
  [StateEnum.shared_preservationPolicy]: SharedPreservationPolicyStateModel;
  [StateEnum.shared_disseminationPolicy]: SharedDisseminationPolicyStateModel;
  [StateEnum.shared_person]: SharedPersonStateModel;
  [StateEnum.shared_institution]: SharedInstitutionStateModel;
  [StateEnum.shared_role]: SharedRoleStateModel;
  [StateEnum.shared_fundingAgency]: SharedFundingAgencyStateModel;
  [StateEnum.shared_subjectArea]: SharedSubjectAreaStateModel;
  [StateEnum.shared_notification]: SharedNotificationStateModel;
  [StateEnum.shared_archive]: SharedArchiveStateModel;
  [StateEnum.shared_archiveAcl]: SharedArchiveAclStateModel;
  [StateEnum.shared_archiveType]: SharedArchiveTypeStateModel;
  [StateEnum.shared_indexFieldAlias]: SharedIndexFieldAliasStateModel;
  [StateEnum.shared_oaiSet]: SharedOaiSetStateModel;
  [StateEnum.shared_oaiMetadataPrefix]: SharedOaiMetadataPrefixStateModel;
  [StateEnum.shared_data_category]: SharedDataCategoryStateModel;
  [StateEnum.shared_citation]: SharedCitationStateModel;
}

@Injectable()
@State<SharedStateModel>({
  name: StateEnum.shared,
  defaults: {
    isLoadingCounter: 0,
    [StateEnum.shared_organizationalUnit]: null,
    [StateEnum.shared_deposit]: null,
    [StateEnum.shared_language]: null,
    [StateEnum.shared_license]: null,
    [StateEnum.shared_globalBanner]: undefined,
    [StateEnum.shared_preservationPolicy]: null,
    [StateEnum.shared_submissionPolicy]: null,
    [StateEnum.shared_submissionAgreement]: null,
    [StateEnum.shared_disseminationPolicy]: null,
    [StateEnum.shared_person]: null,
    [StateEnum.shared_institution]: null,
    [StateEnum.shared_role]: null,
    [StateEnum.shared_fundingAgency]: null,
    [StateEnum.shared_subjectArea]: null,
    [StateEnum.shared_notification]: null,
    [StateEnum.shared_archive]: null,
    [StateEnum.shared_archiveAcl]: null,
    [StateEnum.shared_archiveType]: null,
    [StateEnum.shared_indexFieldAlias]: null,
    [StateEnum.shared_oaiSet]: undefined,
    [StateEnum.shared_oaiMetadataPrefix]: undefined,
    [StateEnum.shared_data_category]: undefined,
    [StateEnum.shared_citation]: undefined,
  },
  children: [
    SharedArchiveAclState,
    SharedOrganizationalUnitState,
    SharedDepositState,
    SharedLanguageState,
    SharedLicenseState,
    SharedGlobalBannerState,
    SharedSubmissionPolicyState,
    SharedSubmissionAgreementState,
    SharedPreservationPolicyState,
    SharedDisseminationPolicyState,
    SharedPersonState,
    SharedInstitutionState,
    SharedRoleState,
    SharedFundingAgencyState,
    SharedSubjectAreaState,
    SharedNotificationState,
    SharedUserState,
    SharedContributorState,
    SharedOrderState,
    SharedOaiSetState,
    SharedOaiMetadataPrefixState,
    SharedMetadataTypeState,
    SharedAipState,
    SharedArchiveState,
    SharedArchiveTypeState,
    SharedIndexFieldAliasState,
    SharedDataCategoryState,
    SharedCitationState,
  ],
})
export class SharedState {
}
