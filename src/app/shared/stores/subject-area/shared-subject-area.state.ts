/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-subject-area.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {SubjectArea} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  SharedSubjectAreaAction,
  sharedSubjectAreaActionNameSpace,
} from "@shared/stores/subject-area/shared-subject-area.action";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  isWhiteString,
  MappingObjectUtil,
  NotificationService,
  OverrideDefaultAction,
  QueryParametersUtil,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {ApiActionNameEnum} from "../../enums/api-action-name.enum";

export interface SharedSubjectAreaStateModel extends ResourceStateModel<SubjectArea> {
  sources: string[] | undefined;
}

@Injectable()
@State<SharedSubjectAreaStateModel>({
  name: StateEnum.shared_subjectArea,
  defaults: {
    ...defaultResourceStateInitValue(),
    sources: undefined,
  },
})
export class SharedSubjectAreaState extends ResourceState<SharedSubjectAreaStateModel, SubjectArea> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedSubjectAreaActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminSubjectAreas;
  }

  @Action(SharedSubjectAreaAction.GetSource)
  getSource(ctx: SolidifyStateContext<SharedSubjectAreaStateModel>, action: SharedSubjectAreaAction.GetSource): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.get<string[]>(`${this._urlResource}/${ApiActionNameEnum.LIST_SUBJECT_AREA_SOURCES}`)
      .pipe(
        tap(result => {
          ctx.dispatch(new SharedSubjectAreaAction.GetSourceSuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedSubjectAreaAction.GetSourceFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedSubjectAreaAction.GetSourceSuccess)
  getSourceSuccess(ctx: SolidifyStateContext<SharedSubjectAreaStateModel>, action: SharedSubjectAreaAction.GetSourceSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      sources: action.sources,
    });
  }

  @Action(SharedSubjectAreaAction.GetSourceFail)
  getSourceFail(ctx: SolidifyStateContext<SharedSubjectAreaStateModel>, action: SharedSubjectAreaAction.GetSourceFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @OverrideDefaultAction()
  @Action(SharedSubjectAreaAction.GetAll)
  getAll(ctx: SolidifyStateContext<SharedSubjectAreaStateModel>, action: SharedSubjectAreaAction.GetAll): Observable<CollectionTyped<SubjectArea>> {
    let url = this._urlResource;
    const searchItems = QueryParametersUtil.getSearchItems(action.queryParameters);
    const nameOrCode = MappingObjectUtil.get(searchItems, "nameOrCode");
    if (!isNullOrUndefined(nameOrCode) && !isWhiteString(nameOrCode)) {
      MappingObjectUtil.set(searchItems, "search", `i-name~${nameOrCode},i-code~${nameOrCode}`);
      MappingObjectUtil.set(searchItems, "match", "any");
      MappingObjectUtil.delete(searchItems, "nameOrCode");
      url = this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH;
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });
    return this._apiService.getCollection<SubjectArea>(url, ctx.getState().queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [SharedSubjectAreaAction.GetAll]),
        tap((collection: CollectionTyped<SubjectArea>) => {
          ctx.dispatch(new SharedSubjectAreaAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedSubjectAreaAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }
}
