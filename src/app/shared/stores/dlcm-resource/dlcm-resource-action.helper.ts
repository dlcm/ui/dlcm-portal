/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - dlcm-resource-action.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ResourceActionHelper} from "solidify-frontend";
import {DlcmResourceNameSpace} from "./dlcm-resource-namespace.model";
import {DlcmResourceAction} from "./dlcm-resource.action";

export class DlcmResourceActionHelper extends ResourceActionHelper {
  static history(dlcmResourceNameSpace: DlcmResourceNameSpace, ...args: ConstructorParameters<typeof DlcmResourceAction.History>): DlcmResourceAction.History {
    return new dlcmResourceNameSpace.History(...args);
  }

  static historySuccess(dlcmResourceNameSpace: DlcmResourceNameSpace, ...args: ConstructorParameters<typeof DlcmResourceAction.HistorySuccess>): DlcmResourceAction.HistorySuccess {
    return new dlcmResourceNameSpace.HistorySuccess(...args);
  }

  static historyFail(dlcmResourceNameSpace: DlcmResourceNameSpace, ...args: ConstructorParameters<typeof DlcmResourceAction.HistoryFail>): DlcmResourceAction.HistoryFail {
    return new dlcmResourceNameSpace.HistoryFail(...args);
  }
}
