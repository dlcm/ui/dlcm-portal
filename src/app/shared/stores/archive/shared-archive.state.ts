/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-archive.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpStatusCode,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {DLCM_CONSTANTS} from "@app/constants";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Archive,
  ArchiveMetadata,
  DisseminationPolicyDto,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {CookieEnum} from "@shared/enums/cookie.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ArchiveHelper} from "@shared/helpers/archive.helper";
import {
  SharedArchiveAction,
  sharedArchiveActionNameSpace,
} from "@shared/stores/archive/shared-archive.action";
import {
  interval,
  Observable,
  of,
  pipe,
} from "rxjs";
import {
  catchError,
  filter,
  map,
  mergeMap,
  startWith,
  switchMap,
  takeWhile,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  CookieConsentService,
  CookieConsentUtil,
  CookiePartialEnum,
  CookieType,
  defaultResourceStateInitValue,
  DownloadService,
  ErrorDto,
  Guid,
  isFalse,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isTrue,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  NotificationTypeEnum,
  OauthHelper,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
  StringUtil,
} from "solidify-frontend";
import {ApiActionNameEnum} from "../../enums/api-action-name.enum";

export const PARAM_QUERY_ORGUNIT: string = "queryOrganizationalUnit";
export const PARAM_QUERY_SEARCH: string = "querySearch";
const DEFAULT_SEARCH: string = "*";

export interface SharedArchiveStateModel extends ResourceStateModel<Archive> {
  isPooling: string[];
  isPendingRequest: string[];
  isFirstTimeError: string[];
  listRelativeArchive: Archive[];
}

@Injectable()
@State<SharedArchiveStateModel>({
  name: StateEnum.shared_archive,
  defaults: {
    ...defaultResourceStateInitValue(),
    isPooling: [],
    isPendingRequest: [],
    isFirstTimeError: [],
    listRelativeArchive: undefined,
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
  children: [],
})
export class SharedArchiveState extends ResourceState<SharedArchiveStateModel, Archive> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _httpClient: HttpClient,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _downloadService: DownloadService,
              private readonly _cookieConsentService: CookieConsentService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedArchiveActionNameSpace,
    });
  }

  private readonly _PATH_FILTER: string = "query";
  private readonly _PARAM_ORDER_ID: string = "orderId";

  protected get _urlResource(): string {
    return ApiEnum.accessPublicMetadata;
  }

  private readonly _IS_POOLING_KEY: keyof SharedArchiveStateModel = "isPooling";
  private readonly _IS_PENDING_REQUEST_KEY: keyof SharedArchiveStateModel = "isPendingRequest";
  private readonly _IS_FIRST_TIME_ERROR_KEY: keyof SharedArchiveStateModel = "isFirstTimeError";

  @Selector()
  static isLoading(state: SharedArchiveStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  static isDownloadPreparation(store: Store, archiveResId: string): Observable<boolean> {
    return MemoizedUtil.select(store, SharedArchiveState, state => state.isPooling).pipe(
      map(list => list.includes(archiveResId)),
    );
  }

  @OverrideDefaultAction()
  @Action(SharedArchiveAction.GetAll)
  getAll(ctx: SolidifyStateContext<ResourceStateModel<Archive>>, action: SharedArchiveAction.GetAll): Observable<CollectionTyped<Archive>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        list: undefined,
        total: 0,
      };
    }

    let queryParameters = StoreUtil.getQueryParametersToApply(action.queryParameters, ctx);
    queryParameters = QueryParametersUtil.clone(queryParameters);
    let searchValue = DEFAULT_SEARCH;
    const search = MappingObjectUtil.get(queryParameters.search.searchItems, PARAM_QUERY_SEARCH);
    if (isNonEmptyString(search)) {
      searchValue = search;
    }
    const orgUnitParam = MappingObjectUtil.get(queryParameters.search.searchItems, PARAM_QUERY_ORGUNIT);
    // let facetsSelected: SearchFacet[] = [];
    if (isNonEmptyString(orgUnitParam)) {
      searchValue = searchValue + " AND " + "aip-organizational-unit:" + orgUnitParam;
      // facetsSelected = [{
      //   field: "organizational-units-id",
      //   equalValue: orgUnitParam,
      //   type: undefined,
      // }];
    }

    MappingObjectUtil.set(queryParameters.search.searchItems, this._PATH_FILTER, searchValue);
    MappingObjectUtil.delete(queryParameters.search.searchItems, PARAM_QUERY_SEARCH);
    MappingObjectUtil.delete(queryParameters.search.searchItems, PARAM_QUERY_ORGUNIT);
    queryParameters.paging.pageSize = 5;
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: queryParameters,
      ...reset,
    });

    // const json = JSON.stringify(facetsSelected);
    // let headers: HttpHeaders = new HttpHeaders();
    // headers = headers.set("Content-Type", "application/x-www-form-urlencoded");
    // headers = headers.set("Accept", "application/json");
    // const queryParametersHttp = ApiService.getQueryParameters(undefined, queryParameters);
    // let body = new HttpParams();
    // body = body.set("conditions", json);
    // return this.httpClient.post<CollectionTyped<ArchiveMetadata>>(ApiEnum.publicMetadata + urlSeparator + ApiActionEnum.SEARCH,
    //   body,
    //   {
    //     headers,
    //     params: queryParametersHttp,
    //   },
    // ).pipe(
    return this._apiService.getCollection<ArchiveMetadata>(this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH, queryParameters)
      .pipe(
        action.cancelIncomplete ? StoreUtil.cancelUncompleted(action, ctx, this._actions$, [this._nameSpace.GetAll, Navigate]) : pipe(),
        map((collection: CollectionTyped<ArchiveMetadata>) => {
          // TODO ADD ON SOLIDIFY METHOD THAT ALLOW TO TRANSFORM RESULT RETURNED FROM BACKEND
          const collectionArchive = {
            _links: collection._links,
            _page: collection._page,
            _facets: collection._facets,
          } as CollectionTyped<Archive>;
          collectionArchive._data = ArchiveHelper.adaptListArchivesMetadataInArchive(collection._data);
          ctx.dispatch(new SharedArchiveAction.GetAllSuccess(action, collectionArchive));
          return collectionArchive;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedArchiveAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(SharedArchiveAction.LoadNextChunkList)
  loadNextChunkList(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.LoadNextChunkList): Observable<CollectionTyped<Archive>> {
    const queryParameters = QueryParametersUtil.clone(ctx.getState().queryParameters);
    if (!StoreUtil.isNextChunkAvailable(queryParameters)) {
      return;
    }

    queryParameters.paging.pageIndex = queryParameters.paging.pageIndex + 1;
    ctx.patchState({
      isLoadingChunk: true,
      queryParameters: queryParameters,
    });

    return this._apiService.getCollection<ArchiveMetadata>(this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH, ctx.getState().queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [this._nameSpace.GetAll, Navigate]),
        map((collection: CollectionTyped<ArchiveMetadata>) => {
          // TODO ADD ON SOLIDIFY METHOD THAT ALLOW TO TRANSFORM RESULT RETURNED FROM BACKEND
          const collectionArchive = {
            _links: collection._links,
            _page: collection._page,
            _facets: collection._facets,
          } as CollectionTyped<Archive>;
          collectionArchive._data = ArchiveHelper.adaptListArchivesMetadataInArchive(collection._data);
          ctx.dispatch(new SharedArchiveAction.LoadNextChunkListSuccess(action, collectionArchive));
          return collectionArchive;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedArchiveAction.LoadNextChunkListFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(SharedArchiveAction.GetById)
  getById(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.GetById): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      current: undefined,
    });

    return this._apiService.getById<ArchiveMetadata>(this._urlResource, action.id)
      .pipe(
        tap(model => ctx.dispatch(new SharedArchiveAction.GetByIdSuccess(action, ArchiveHelper.adaptArchiveMetadataInArchive(model)))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedArchiveAction.GetByIdFail(action));
          throw error;
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(SharedArchiveAction.AddInListById)
  addInListById(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.AddInListById): Observable<Archive> {
    let indexAlreadyExisting = -1;
    if (!isNullOrUndefined(ctx.getState().list) && isTrue(action.avoidDuplicate)) {
      indexAlreadyExisting = ctx.getState().list.findIndex(item => item.resId === action.resId);
      if (indexAlreadyExisting !== -1 && isFalse(action.replace)) {
        return;
      }
    }
    if (ctx.getState().listPendingResId.find(pending => pending === action.resId)) {
      return;
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      listPendingResId: [...(isNullOrUndefined(ctx.getState().listPendingResId) ? [] : ctx.getState().listPendingResId), action.resId],
    });
    return this._apiService.getById<ArchiveMetadata>(this._urlResource, action.resId)
      .pipe(
        map((model: ArchiveMetadata) => {
          const archive = ArchiveHelper.adaptArchiveMetadataInArchive(model);
          ctx.dispatch(new SharedArchiveAction.AddInListByIdSuccess(action, archive, indexAlreadyExisting));
          return archive;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedArchiveAction.AddInListByIdFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedArchiveAction.Download)
  download(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.Download): any {
    if (isTrue(environment.useDownloadToken) && !CookieConsentUtil.isFeatureAuthorized(CookieType.cookie, CookieEnum.downloadToken)) {
      this._cookieConsentService.notifyFeatureDisabledBecauseCookieDeclined(CookieType.cookie, CookiePartialEnum.downloadToken);
      return;
    }
    this._notificationService.show({
      durationInSecond: 10,
      data: {
        message: MARK_AS_TRANSLATABLE("home.archive.browsing.download.notification.inPreparation"),
        category: NotificationTypeEnum.information,
      },
      component: environment.defaultNotificationComponent,
    });

    const downloadRequest = new DownloadRequest(action);

    this._setIsPooling(ctx, downloadRequest, true);
    this._setIsPendingRequest(ctx, downloadRequest, false);
    this._setIsFirstTimeError(ctx, downloadRequest, false);

    this._setIsPendingRequest(ctx, downloadRequest, true);
    this.subscribe(this._prepareDownload(ctx, action.archive.resId, action.disseminationPolicyDto).pipe(
      switchMap((orderId: string) => {
        downloadRequest.orderId = orderId;
        this._setIsPendingRequest(ctx, downloadRequest, false);
        return interval(2000)
          .pipe(
            takeWhile(() => this._getIsPooling(ctx, downloadRequest)),
            filter(() => !this._getIsPendingRequest(ctx, downloadRequest)),
            startWith(0),
            mergeMap(() => {
              this._setIsPendingRequest(ctx, downloadRequest, true);
              return this._getDownloadStatus(ctx, orderId).pipe(
                tap(() => this._setIsPendingRequest(ctx, downloadRequest, false)),
              );
            }),
            tap((res: string) => {
              if (res === StringUtil.stringEmpty) {
                this._setIsPendingRequest(ctx, downloadRequest, true);
                return;
              }
              if (res === Enums.Order.StatusEnum.READY) {
                this._setIsPooling(ctx, downloadRequest, false);
                ctx.dispatch(new SharedArchiveAction.DownloadStart(action, orderId));
                return;
              }
              if (res === Enums.Order.StatusEnum.IN_ERROR) {
                this._setIsPooling(ctx, downloadRequest, false);
                if (this._getIsFirstTimeError(ctx, downloadRequest) === false) {
                  ctx.dispatch(new SharedArchiveAction.DownloadFail(action as any));
                  this._setIsFirstTimeError(ctx, downloadRequest, true);
                }
              }
            }),
            catchError((error: SolidifyHttpErrorResponseModel) => {
              this._setIsPooling(ctx, downloadRequest, false);
              throw error;
            }),
          );
      }),
      catchError((e: SolidifyHttpErrorResponseModel) => {
        if (e.status === HttpStatusCode.Forbidden) {
          this._setIsPendingRequest(ctx, downloadRequest, false);
          this._setIsPooling(ctx, downloadRequest, false);
          ctx.dispatch(new SharedArchiveAction.DownloadFail(action as any, LabelTranslateEnum.fileDownloadForbidden));
          throw e;
        } else if (e.status === HttpStatusCode.Unauthorized) {
          this._setIsPendingRequest(ctx, downloadRequest, false);
          this._setIsPooling(ctx, downloadRequest, false);
          if (e.error?.error !== OauthHelper.MFA_NEEDED) {
            ctx.dispatch(new SharedArchiveAction.DownloadFail(action as any, LabelTranslateEnum.fileDownloadUnauthorized));
          }
          throw e;
        }
        return of(StringUtil.stringEmpty);
      }),
    ));
    return;
  }

  private _getDownloadStatus(ctx: SolidifyStateContext<SharedArchiveStateModel>, orderId: string, disseminationPolicyId: string = DLCM_CONSTANTS.DISSEMINATION_POLICY_BASIC_ID): Observable<Enums.Order.StatusEnum> {
    return this._httpClient.get<Enums.Order.StatusEnum>(`${this._urlResource}/${orderId}/${ApiActionNameEnum.DOWNLOAD_STATUS}`, {
      params: ApiService.getQueryParameters({
        disseminationPolicyId: disseminationPolicyId,
      }),
    }).pipe(
      catchError((e: ErrorDto) => {
        if (e.status === HttpStatusCode.InternalServerError) {
          return of(Enums.Order.StatusEnum.IN_ERROR);
        }
        return of(StringUtil.stringEmpty);
      }),
    ) as Observable<any>;
  }

  private _prepareDownload(ctx: SolidifyStateContext<SharedArchiveStateModel>, aipId: string, disseminationPolicyDto?: DisseminationPolicyDto): Observable<string> {
    return this._httpClient.post(`${this._urlResource}/${aipId}/${ApiActionNameEnum.PREPARE_DOWNLOAD}`,
      disseminationPolicyDto,
      {
        responseType: "text",
      })
      .pipe(
        map((orderId: string) => orderId),
      );
  }

  @Action(SharedArchiveAction.DownloadStart)
  downloadStart(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.DownloadStart): void {
    const archive = action.parentAction.archive;
    const isPartial = isNonEmptyArray(action.parentAction.disseminationPolicyDto?.subsetItemList);
    const fileName = archive.title + (isPartial ? "_partial" : "") + ".zip";
    const fileSize = isPartial ? undefined : archive.size;
    const param = isNotNullNorUndefinedNorWhiteString(action.orderId) ? `?${this._PARAM_ORDER_ID}=${action.orderId}` : "";
    this._downloadService.download(`${this._urlResource}/${archive.resId}/${ApiActionNameEnum.DL}${param}`, fileName, fileSize);
  }

  @Action(SharedArchiveAction.DownloadFail)
  downloadFail(ctx: SolidifyStateContext<SharedArchiveStateModel>, action: SharedArchiveAction.DownloadFail): void {
    this._notificationService.showError(action.customMessageToTranslate ?? LabelTranslateEnum.fileDownloadFail);
  }

  private _getIsPooling(ctx: SolidifyStateContext<SharedArchiveStateModel>, downloadRequest: DownloadRequest): boolean {
    return this._isIncludedInList(ctx, downloadRequest, this._IS_POOLING_KEY);
  }

  private _setIsPooling(ctx: SolidifyStateContext<SharedArchiveStateModel>, downloadRequest: DownloadRequest, toAdd: boolean): void {
    return this._setInList(ctx, downloadRequest, this._IS_POOLING_KEY, toAdd);
  }

  private _getIsPendingRequest(ctx: SolidifyStateContext<SharedArchiveStateModel>, downloadRequest: DownloadRequest): boolean {
    return this._isIncludedInList(ctx, downloadRequest, this._IS_PENDING_REQUEST_KEY);
  }

  private _setIsPendingRequest(ctx: SolidifyStateContext<SharedArchiveStateModel>, downloadRequest: DownloadRequest, toAdd: boolean): void {
    return this._setInList(ctx, downloadRequest, this._IS_PENDING_REQUEST_KEY, toAdd);
  }

  private _getIsFirstTimeError(ctx: SolidifyStateContext<SharedArchiveStateModel>, downloadRequest: DownloadRequest): boolean {
    return this._isIncludedInList(ctx, downloadRequest, this._IS_FIRST_TIME_ERROR_KEY);
  }

  private _setIsFirstTimeError(ctx: SolidifyStateContext<SharedArchiveStateModel>, downloadRequest: DownloadRequest, toAdd: boolean): void {
    return this._setInList(ctx, downloadRequest, this._IS_FIRST_TIME_ERROR_KEY, toAdd);
  }

  private _isIncludedInList(ctx: SolidifyStateContext<SharedArchiveStateModel>, downloadRequest: DownloadRequest, key: keyof SharedArchiveStateModel): boolean {
    return (ctx.getState()[key] as string[]).includes(downloadRequest.guid);
  }

  private _setInList(ctx: SolidifyStateContext<SharedArchiveStateModel>, downloadRequest: DownloadRequest, key: keyof SharedArchiveStateModel, toAdd: boolean): void {
    if (toAdd) {
      this._addInList(ctx, downloadRequest, key);
    } else {
      this._removeInList(ctx, downloadRequest, key);
    }
  }

  private _addInList(ctx: SolidifyStateContext<SharedArchiveStateModel>, downloadRequest: DownloadRequest, key: keyof SharedArchiveStateModel): void {
    const list = ctx.getState()[key] as string[];
    if (!list.includes(downloadRequest.guid)) {
      ctx.patchState({
        [key]: [...list, downloadRequest.guid],
      });
    }
  }

  private _removeInList(ctx: SolidifyStateContext<SharedArchiveStateModel>, downloadRequest: DownloadRequest, key: keyof SharedArchiveStateModel): void {
    const list = ctx.getState()[key] as string[];
    const indexOf = list.indexOf(downloadRequest.guid);
    if (indexOf !== -1) {
      const listPending = [...list];
      listPending.splice(indexOf, 1);
      ctx.patchState({
        [key]: listPending,
      });
    }
  }
}

class DownloadRequest {
  guid: string;
  orderId: string;
  action: SharedArchiveAction.Download;

  constructor(private readonly _action: SharedArchiveAction.Download) {
    this.guid = Guid.MakeNew().toString();
    this.orderId = undefined;
    this.action = _action;
  }
}
