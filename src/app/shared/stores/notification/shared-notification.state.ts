/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-notification.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {appActionNameSpace} from "@app/stores/app.action";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {HomeAction} from "@home/stores/home.action";
import {NotificationDlcm} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {
  SharedNotificationAction,
  sharedNotificationActionNameSpace,
} from "@shared/stores/notification/shared-notification.action";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  CollectionTyped,
  CookieConsentUtil,
  CookieType,
  defaultResourceFileStateInitValue,
  DownloadService,
  isNonEmptyArray,
  LocalStorageHelper,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  OrderEnum,
  OverrideDefaultAction,
  QueryParameters,
  QueryParametersUtil,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  ResourceStateModel,
  Result,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

const ATTRIBUTE_SIGNED_DUA_FILE: keyof NotificationDlcm = "signedDuaFile";

export interface SharedNotificationStateModel extends ResourceFileStateModel<NotificationDlcm> {
  mapPendingNotificationOnDatasetAccessRequest: MappingObject<string, NotificationDlcm>;
  mapPendingNotificationOnOrgUnitAccessRequest: MappingObject<string, NotificationDlcm>;
}

@Injectable()
@State<SharedNotificationStateModel>({
  name: StateEnum.shared_notification,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
    mapPendingNotificationOnDatasetAccessRequest: {},
    mapPendingNotificationOnOrgUnitAccessRequest: {},
  },
})
export class SharedNotificationState extends ResourceFileState<SharedNotificationStateModel, NotificationDlcm> {
  private readonly _ATTRIBUTE_OBJECT_ID: keyof NotificationDlcm = "objectId";
  private readonly _ATTRIBUTE_ORG_UNIT_ID: keyof NotificationDlcm | string = "notifiedOrgUnit.resId";
  private readonly _ATTRIBUTE_NOTIFICATION_TYPE: keyof NotificationDlcm = "notificationType";
  private readonly _ATTRIBUTE_NOTIFICATION_STATUS: keyof NotificationDlcm = "notificationStatus";
  private readonly _ATTRIBUTE_NOTIFICATION_REJECT_REASON: string = "reason";
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: sharedNotificationActionNameSpace,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("notification.notification.requestSendWithSuccess"),
      notificationResourceCreateFailTextToTranslate: MARK_AS_TRANSLATABLE("notification.notification.unableToSendRequest"),
      downloadInMemory: false,
      resourceFileApiActionNameUploadCustom: ApiActionNameEnum.UPLOAD_DUA,
      resourceFileApiActionNameDownloadCustom: ApiActionNameEnum.DOWNLOAD_DUA,
      resourceFileApiActionNameDeleteCustom: ApiActionNameEnum.DELETE_DUA,
      customFileAttribute: ATTRIBUTE_SIGNED_DUA_FILE,
    }, _downloadService, ResourceFileStateModeEnum.custom, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminNotifications;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @OverrideDefaultAction()
  @Action(SharedNotificationAction.Create)
  create(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.Create): Observable<NotificationDlcm> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const url = this._urlResource;
    return this._apiService.post<NotificationDlcm>(url, action.modelFormControlEvent.model)
      .pipe(
        tap(notification => {
          ctx.dispatch(new SharedNotificationAction.CreateSuccess(action, notification));
        }),
        catchError((error) => {
          ctx.dispatch(new SharedNotificationAction.CreateFail(action, error));
          throw new SolidifyStateError(this, error);
        }),
        StoreUtil.catchValidationErrors(ctx, action.modelFormControlEvent, this._notificationService, this._optionsState.autoScrollToFirstValidationError),
      );
  }

  static getMapPendingNotificationName(notificationType: Enums.Notification.TypeEnum): keyof SharedNotificationStateModel {
    switch (notificationType) {
      case Enums.Notification.TypeEnum.ACCESS_DATASET_REQUEST:
        return "mapPendingNotificationOnDatasetAccessRequest";
      case Enums.Notification.TypeEnum.JOIN_ORGUNIT_REQUEST:
        return "mapPendingNotificationOnOrgUnitAccessRequest";
    }
    throw new Error(`Unmanaged type ${notificationType}`);
  }

  private _getKey(notificationType: Enums.Notification.TypeEnum): keyof NotificationDlcm | string {
    switch (notificationType) {
      case Enums.Notification.TypeEnum.ACCESS_DATASET_REQUEST:
        return this._ATTRIBUTE_OBJECT_ID;
      case Enums.Notification.TypeEnum.JOIN_ORGUNIT_REQUEST:
        return this._ATTRIBUTE_ORG_UNIT_ID;
    }
    throw new Error(`Unmanaged type ${notificationType}`);
  }

  @Action(SharedNotificationAction.GetPendingNotification)
  getPendingNotification(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.GetPendingNotification): Observable<NotificationDlcm> {
    const mapName = SharedNotificationState.getMapPendingNotificationName(action.notificationType) as string;
    const mapNotification = ctx.getState()[mapName];

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      current: undefined,
    });

    if (MappingObjectUtil.has(mapNotification, action.id)) {
      const result = MappingObjectUtil.get(mapNotification, action.id);
      setTimeout(() => {
        ctx.dispatch(new SharedNotificationAction.GetPendingNotificationSuccess(action, result));
      }, 0);
      return of(result);
    }

    const queryParameters = new QueryParameters(environment.minimalPageSizeForPageInfos, {
      field: "lastUpdate.when",
      order: OrderEnum.descending,
    });
    const search = QueryParametersUtil.getSearchItems(queryParameters);
    MappingObjectUtil.set(search, this._getKey(action.notificationType) as keyof NotificationDlcm, action.id);
    MappingObjectUtil.set(search, this._ATTRIBUTE_NOTIFICATION_TYPE, action.notificationType);
    MappingObjectUtil.set(search, this._ATTRIBUTE_NOTIFICATION_STATUS, Enums.Notification.StatusEnum.PENDING);
    return this._apiService.getCollection<NotificationDlcm>(this._urlResource + urlSeparator + ApiActionNameEnum.SENT, queryParameters)
      .pipe(
        map((collection: CollectionTyped<NotificationDlcm>) => {
          const pendingNotification = isNonEmptyArray(collection._data) ? collection._data[0] : null;
          ctx.dispatch(new SharedNotificationAction.GetPendingNotificationSuccess(action, pendingNotification));
          return pendingNotification;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedNotificationAction.GetPendingNotificationFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedNotificationAction.GetPendingNotificationSuccess)
  getPendingNotificationSuccess(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.GetPendingNotificationSuccess): void {
    const mapName = SharedNotificationState.getMapPendingNotificationName(action.parentAction.notificationType) as string;
    const mapNotification = ctx.getState()[mapName];

    const statePatch = {
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    };

    if (!MappingObjectUtil.has(mapNotification, action.parentAction.id)) {
      const mapNotificationCopy = MappingObjectUtil.copy(mapNotification);
      MappingObjectUtil.set(mapNotificationCopy, action.parentAction.id, action.notification);
      statePatch[mapName] = mapNotificationCopy;
      this._afterUpdatePendingNotification(action.parentAction.notificationType, action.parentAction.id);
    }

    ctx.patchState(statePatch);
  }

  @Action(SharedNotificationAction.GetPendingNotificationFail)
  getPendingNotificationFail(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.GetPendingNotificationFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedNotificationAction.CleanPendingRequestNotification)
  cleanPendingRequestNotification(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.CleanPendingRequestNotification): void {
    ctx.patchState({
      mapPendingNotificationOnDatasetAccessRequest: {},
      mapPendingNotificationOnOrgUnitAccessRequest: {},
    });
  }

  @Action(SharedNotificationAction.UpdatePendingNotification)
  updatePendingNotification(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.UpdatePendingNotification): void {
    const mapName = SharedNotificationState.getMapPendingNotificationName(action.notification.notificationType.resId) as string;
    const mapNotification = MappingObjectUtil.copy(ctx.getState()[mapName]);
    MappingObjectUtil.set(mapNotification, action.id, action.notification);
    this._afterUpdatePendingNotification(action.notification.notificationType.resId, action.id);
    ctx.patchState({
      [mapName]: mapNotification,
    });
  }

  @Action(SharedNotificationAction.SetApproved)
  setApproved(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetApproved): Observable<Result> {
    return this._apiService.post<Result>(this._urlResource + urlSeparator + action.notificationId + urlSeparator + ApiActionNameEnum.SET_APPROVED)
      .pipe(
        tap(result => ctx.dispatch(new SharedNotificationAction.SetApprovedSuccess(action))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedNotificationAction.SetApprovedFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedNotificationAction.SetApprovedSuccess)
  setApprovedSuccess(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetApprovedSuccess): void {
    this._removeNotificationPendingListAndRefresh(ctx, action.parentAction.notificationId, action.parentAction.mode);
    const notificationCategory = action.parentAction.notificationCategory;
    if (notificationCategory === Enums.Notification.CategoryEnum.REQUEST) {
      this._notificationService.showSuccess(LabelTranslateEnum.notificationSetProcessedSuccess);
    } else if (notificationCategory === Enums.Notification.CategoryEnum.INFO) {
      this._notificationService.showSuccess(LabelTranslateEnum.notificationSetReadSuccess);
    }
    LocalStorageHelper.removeItemInList(LocalStorageEnum.notificationInboxPending, action.parentAction.notificationId);
    this._store.dispatch(new appActionNameSpace.UpdateNotificationInbox());
  }

  @Action(SharedNotificationAction.SetApprovedFail)
  setApprovedFail(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetApprovedFail): void {
    const notificationCategory = action.parentAction.notificationCategory;
    if (notificationCategory === Enums.Notification.CategoryEnum.REQUEST) {
      this._notificationService.showError(LabelTranslateEnum.notificationSetProcessedFail);
    } else if (notificationCategory === Enums.Notification.CategoryEnum.INFO) {
      this._notificationService.showError(LabelTranslateEnum.notificationSetReadFail);
    }
  }

  @Action(SharedNotificationAction.SetRefuse)
  setRefuse(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetRefuse): Observable<Result> {

    const customParams = {
      [this._ATTRIBUTE_NOTIFICATION_REJECT_REASON]: action.responseMessage,
    };

    return this._apiService.post<Result>(this._urlResource + urlSeparator + action.notificationId + urlSeparator + ApiActionNameEnum.SET_REFUSED, null, customParams)
      .pipe(
        tap(result => ctx.dispatch(new SharedNotificationAction.SetRefuseSuccess(action))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedNotificationAction.SetRefuseFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedNotificationAction.SetRefuseSuccess)
  setRefuseSuccess(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetRefuseSuccess): void {
    this._removeNotificationPendingListAndRefresh(ctx, action.parentAction.notificationId, action.parentAction.mode);
    this._notificationService.showSuccess(LabelTranslateEnum.notificationSetRefuseSuccess);
    LocalStorageHelper.removeItemInList(LocalStorageEnum.notificationInboxPending, action.parentAction.notificationId);
    this._store.dispatch(new appActionNameSpace.UpdateNotificationInbox());
  }

  @Action(SharedNotificationAction.SetRefuseFail)
  setRefuseFail(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetRefuseFail): void {
    this._notificationService.showError(LabelTranslateEnum.notificationSetRefuseFail);
  }

  @Action(SharedNotificationAction.SetPending)
  setPending(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetPending): Observable<Result> {
    return this._apiService.post<Result>(this._urlResource + urlSeparator + action.notificationId + urlSeparator + ApiActionNameEnum.SET_PENDING)
      .pipe(
        tap(result => ctx.dispatch(new SharedNotificationAction.SetPendingSuccess(action))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedNotificationAction.SetPendingFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedNotificationAction.SetPendingSuccess)
  setPendingSuccess(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetPendingSuccess): void {
    this._removeNotificationPendingListAndRefresh(ctx, action.parentAction.notificationId, action.parentAction.mode);
    const notificationCategory = action.parentAction.notificationCategory;
    if (notificationCategory === Enums.Notification.CategoryEnum.REQUEST) {
      this._notificationService.showSuccess(LabelTranslateEnum.notificationSetPendingSuccess);
    } else if (notificationCategory === Enums.Notification.CategoryEnum.INFO) {
      this._notificationService.showSuccess(LabelTranslateEnum.notificationSetUnreadSuccess);
    }
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.notificationInboxPending)) {
      LocalStorageHelper.addItemInList(LocalStorageEnum.notificationInboxPending, action.parentAction.notificationId);
    }
    this._store.dispatch(new appActionNameSpace.UpdateNotificationInbox());
  }

  private _removeNotificationPendingListAndRefresh(ctx: SolidifyStateContext<SharedNotificationStateModel>,
                                                   notificationId: string,
                                                   mode: ViewModeEnum | undefined): void {
    LocalStorageHelper.removeItemInList(LocalStorageEnum.notificationInboxPending, notificationId);
    if (mode === ViewModeEnum.detail) {
      ctx.dispatch(new SharedNotificationAction.RefreshDetailEvent(notificationId));
    } else if (mode === ViewModeEnum.list) {
      this._refreshListEvent(ctx);
    }
  }

  private _refreshListEvent(ctx: SolidifyStateContext<SharedNotificationStateModel>): void {
    ctx.dispatch(new SharedNotificationAction.RefreshListEvent());
  }

  @Action(SharedNotificationAction.SetPendingFail)
  setPendingFail(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetPendingFail): void {
    const notificationCategory = action.parentAction.notificationCategory;
    if (notificationCategory === Enums.Notification.CategoryEnum.REQUEST) {
      this._notificationService.showError(LabelTranslateEnum.notificationSetPendingFail);
    } else if (notificationCategory === Enums.Notification.CategoryEnum.INFO) {
      this._notificationService.showError(LabelTranslateEnum.notificationSetUnreadFail);
    }
  }

  @Action(SharedNotificationAction.SetRead)
  setRead(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetRead): Observable<Result> {
    return this._apiService.post<Result>(this._urlResource + urlSeparator + action.notificationId + urlSeparator + ApiActionNameEnum.SET_READ)
      .pipe(
        tap(result => ctx.dispatch(new SharedNotificationAction.SetReadSuccess(action))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedNotificationAction.SetReadFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedNotificationAction.SetReadSuccess)
  setReadSuccess(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetReadSuccess): void {
    this._removeNotificationPendingListAndRefresh(ctx, action.parentAction.notificationId, action.parentAction.mode);
    this._notificationService.showSuccess(LabelTranslateEnum.notificationSetReadSuccess);
    LocalStorageHelper.removeItemInList(LocalStorageEnum.notificationInboxPending, action.parentAction.notificationId);
    this._store.dispatch(new appActionNameSpace.UpdateNotificationInbox());
    this._refreshListEvent(ctx);
  }

  @Action(SharedNotificationAction.SetReadFail)
  setReadFail(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetReadFail): void {
    this._notificationService.showError(LabelTranslateEnum.notificationSetReadFail);
  }

  @Action(SharedNotificationAction.BulkSetRead)
  bulkSetRead(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetRead): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listNotification
      .filter(n => n?.notificationMark === Enums.Notification.MarkEnum.UNREAD)
      .map(notification =>
        ({
          action: new SharedNotificationAction.SetRead(notification.resId, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedNotificationAction.SetReadSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedNotificationAction.SetReadFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedNotificationAction.BulkSetReadSuccess(action));
        } else {
          ctx.dispatch(new SharedNotificationAction.BulkSetReadFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedNotificationAction.BulkSetReadSuccess)
  bulkSetReadSuccess(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetReadSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshListEvent(ctx);
  }

  @Action(SharedNotificationAction.BulkSetReadFail)
  bulkSetReadFail(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetReadFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedNotificationAction.SetUnread)
  setUnread(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetUnread): Observable<Result> {
    return this._apiService.post<Result>(this._urlResource + urlSeparator + action.notificationId + urlSeparator + ApiActionNameEnum.SET_UNREAD)
      .pipe(
        tap(result => ctx.dispatch(new SharedNotificationAction.SetUnreadSuccess(action))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedNotificationAction.SetUnreadFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedNotificationAction.SetUnreadSuccess)
  setUnreadSuccess(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetUnreadSuccess): void {
    this._notificationService.showSuccess(LabelTranslateEnum.notificationSetUnreadSuccess);
    LocalStorageHelper.addItemInList(LocalStorageEnum.notificationInboxPending, action.parentAction.notificationId);
    this._store.dispatch(new appActionNameSpace.UpdateNotificationInbox());
    this._refreshListEvent(ctx);
  }

  @Action(SharedNotificationAction.SetUnreadFail)
  setUnreadFail(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.SetUnreadFail): void {
    this._notificationService.showError(LabelTranslateEnum.notificationSetUnreadFail);
  }

  @Action(SharedNotificationAction.BulkSetUnread)
  bulkSetUnread(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetUnread): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listNotification
      .filter(n => n?.notificationMark === Enums.Notification.MarkEnum.READ)
      .map(notification =>
        ({
          action: new SharedNotificationAction.SetUnread(notification.resId, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedNotificationAction.SetUnreadSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedNotificationAction.SetUnreadFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedNotificationAction.BulkSetUnreadSuccess(action));
        } else {
          ctx.dispatch(new SharedNotificationAction.BulkSetUnreadFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedNotificationAction.BulkSetUnreadSuccess)
  bulkSetUnreadSuccess(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetUnreadSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshListEvent(ctx);
  }

  @Action(SharedNotificationAction.BulkSetUnreadFail)
  bulkSetUnreadFail(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetUnreadFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedNotificationAction.BulkSetApproved)
  bulkSetApproved(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetApproved): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listNotification
      .filter(n => n?.notificationStatus === Enums.Notification.StatusEnum.PENDING)
      .map(notification =>
        ({
          action: new SharedNotificationAction.SetApproved(notification.resId, notification.notificationType.notificationCategory, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedNotificationAction.SetApprovedSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedNotificationAction.SetApprovedFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedNotificationAction.BulkSetApprovedSuccess(action));
        } else {
          ctx.dispatch(new SharedNotificationAction.BulkSetApprovedFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedNotificationAction.BulkSetApprovedSuccess)
  bulkSetApprovedSuccess(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetApprovedSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshListEvent(ctx);
  }

  @Action(SharedNotificationAction.BulkSetApprovedFail)
  bulkSetApprovedFail(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetApprovedFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedNotificationAction.BulkSetRefuse)
  bulkSetRefuse(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetRefuse): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listNotification
      .filter(n => n?.notificationStatus === Enums.Notification.StatusEnum.PENDING)
      .map(notification =>
        ({
          action: new SharedNotificationAction.SetRefuse(notification.resId, notification.notificationType.notificationCategory, undefined, action.responseMessage),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedNotificationAction.SetRefuseSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedNotificationAction.SetRefuseFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedNotificationAction.BulkSetRefuseSuccess(action));
        } else {
          ctx.dispatch(new SharedNotificationAction.BulkSetRefuseFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedNotificationAction.BulkSetRefuseSuccess)
  bulkSetRefuseSuccess(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetRefuseSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshListEvent(ctx);
  }

  @Action(SharedNotificationAction.BulkSetRefuseFail)
  bulkSetRefuseFail(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetRefuseFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedNotificationAction.BulkSetPending)
  bulkSetPending(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetPending): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const actionSubActionCompletionsWrappers: ActionSubActionCompletionsWrapper[] = action.listNotification
      .filter(n => n?.notificationStatus !== Enums.Notification.StatusEnum.PENDING)
      .map(notification =>
        ({
          action: new SharedNotificationAction.SetPending(notification.resId, notification.notificationType.notificationCategory, undefined),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(SharedNotificationAction.SetPendingSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(SharedNotificationAction.SetPendingFail)),
          ],
        }),
      );
    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, actionSubActionCompletionsWrappers).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new SharedNotificationAction.BulkSetPendingSuccess(action));
        } else {
          ctx.dispatch(new SharedNotificationAction.BulkSetPendingFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(SharedNotificationAction.BulkSetPendingSuccess)
  bulkSetPendingSuccess(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetPendingSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._refreshListEvent(ctx);
  }

  @Action(SharedNotificationAction.BulkSetPendingFail)
  bulkSetPendingFail(ctx: SolidifyStateContext<SharedNotificationStateModel>, action: SharedNotificationAction.BulkSetPendingFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Selector()
  static isLoading<T>(state: ResourceStateModel<T>): boolean {
    return StoreUtil.isLoadingState(state);
  }

  private _afterUpdatePendingNotification(notificationType: Enums.Notification.TypeEnum, id: string): void {
    if (notificationType === Enums.Notification.TypeEnum.ACCESS_DATASET_REQUEST) {
      this._store.dispatch(new HomeAction.ForceRefreshIsDownloadAuthorized(id));
    }
  }
}
