/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-notification.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {NotificationDlcm} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  ResourceAction,
  ResourceFileAction,
  ResourceNameSpace,
  SolidifyHttpErrorResponseModel,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.shared_notification;

export namespace SharedNotificationAction {
  @TypeDefaultAction(state)
  export class LoadResource extends ResourceAction.LoadResource {
  }

  @TypeDefaultAction(state)
  export class LoadResourceSuccess extends ResourceAction.LoadResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class LoadResourceFail extends ResourceAction.LoadResourceFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends ResourceAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends ResourceAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends ResourceAction.GetAllSuccess<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends ResourceAction.GetAllFail<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class GetByListId extends ResourceAction.GetByListId {
  }

  @TypeDefaultAction(state)
  export class GetByListIdSuccess extends ResourceAction.GetByListIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetByListIdFail extends ResourceAction.GetByListIdFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends ResourceAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends ResourceAction.GetByIdSuccess<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends ResourceAction.GetByIdFail<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class Create extends ResourceAction.Create<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends ResourceAction.CreateSuccess<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends ResourceAction.CreateFail<NotificationDlcm> {
    constructor(public parentAction: ResourceAction.Create<NotificationDlcm>, public error: SolidifyHttpErrorResponseModel) {
      super(parentAction);
    }
  }

  @TypeDefaultAction(state)
  export class Update extends ResourceAction.Update<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends ResourceAction.UpdateSuccess<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends ResourceAction.UpdateFail<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class Delete extends ResourceAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends ResourceAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends ResourceAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends ResourceAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends ResourceAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends ResourceAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class AddInList extends ResourceAction.AddInList<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class AddInListById extends ResourceAction.AddInListById {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdSuccess extends ResourceAction.AddInListByIdSuccess<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdFail extends ResourceAction.AddInListByIdFail<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class RemoveInListById extends ResourceAction.RemoveInListById {
  }

  @TypeDefaultAction(state)
  export class RemoveInListByListId extends ResourceAction.RemoveInListByListId {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkList extends ResourceAction.LoadNextChunkList {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListSuccess extends ResourceAction.LoadNextChunkListSuccess<NotificationDlcm> {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListFail extends ResourceAction.LoadNextChunkListFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends ResourceAction.Clean {
  }

  @TypeDefaultAction(state)
  export class GetFile extends ResourceFileAction.GetFile {
  }

  @TypeDefaultAction(state)
  export class GetFileSuccess extends ResourceFileAction.GetFileSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileFail extends ResourceFileAction.GetFileFail {
  }

  @TypeDefaultAction(state)
  export class GetFileByResId extends ResourceFileAction.GetFileByResId {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdSuccess extends ResourceFileAction.GetFileByResIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdFail extends ResourceFileAction.GetFileByResIdFail {
  }

  @TypeDefaultAction(state)
  export class UploadFile extends ResourceFileAction.UploadFile {
  }

  @TypeDefaultAction(state)
  export class UploadFileSuccess extends ResourceFileAction.UploadFileSuccess {
  }

  @TypeDefaultAction(state)
  export class UploadFileFail extends ResourceFileAction.UploadFileFail {
  }

  @TypeDefaultAction(state)
  export class DeleteFile extends ResourceFileAction.DeleteFile {
  }

  @TypeDefaultAction(state)
  export class DeleteFileSuccess extends ResourceFileAction.DeleteFileSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFileFail extends ResourceFileAction.DeleteFileFail {
  }

  export class GetPendingNotification extends BaseAction {
    static readonly type: string = `[${state}] Get Pending Notification`;

    constructor(public notificationType: Enums.Notification.TypeEnum, public id: string) {
      super();
    }
  }

  export class GetPendingNotificationSuccess extends BaseSubActionSuccess<GetPendingNotification> {
    static readonly type: string = `[${state}] Get Pending Notification Success`;

    constructor(public parentAction: GetPendingNotification, public notification: NotificationDlcm | null) {
      super(parentAction);
    }
  }

  export class GetPendingNotificationFail extends BaseSubActionFail<GetPendingNotification> {
    static readonly type: string = `[${state}] Get Pending Notification Fail`;
  }

  export class CleanPendingRequestNotification extends BaseAction {
    static readonly type: string = `[${state}] Clean Pending Request Notification`;
  }

  export class UpdatePendingNotification extends BaseAction {
    static readonly type: string = `[${state}] Update Pending Notification`;

    constructor(public id: string, public notification: NotificationDlcm) {
      super();
    }
  }

  export class SetApproved extends BaseAction {
    static readonly type: string = `[${state}] Set Approved`;

    constructor(public notificationId: string, public notificationCategory: Enums.Notification.CategoryEnum, public mode: ViewModeEnum) {
      super();
    }
  }

  export class SetApprovedSuccess extends BaseSubActionSuccess<SetApproved> {
    static readonly type: string = `[${state}] Set Approved Success`;
  }

  export class SetApprovedFail extends BaseSubActionFail<SetApproved> {
    static readonly type: string = `[${state}] Set Approved Fail`;
  }

  export class SetRefuse extends BaseAction {
    static readonly type: string = `[${state}] Set Refuse`;

    constructor(public notificationId: string, public notificationCategory: Enums.Notification.CategoryEnum, public mode: ViewModeEnum | undefined, public responseMessage: string) {
      super();
    }
  }

  export class SetRefuseSuccess extends BaseSubActionSuccess<SetRefuse> {
    static readonly type: string = `[${state}] Set Refuse Success`;
  }

  export class SetRefuseFail extends BaseSubActionFail<SetRefuse> {
    static readonly type: string = `[${state}] Set Refuse Fail`;
  }

  export class SetPending extends BaseAction {
    static readonly type: string = `[${state}] Set Pending`;

    constructor(public notificationId: string, public notificationCategory: Enums.Notification.CategoryEnum, public mode: ViewModeEnum) {
      super();
    }
  }

  export class SetPendingSuccess extends BaseSubActionSuccess<SetPending> {
    static readonly type: string = `[${state}] Set Pending Success`;
  }

  export class SetPendingFail extends BaseSubActionFail<SetPending> {
    static readonly type: string = `[${state}] Set Pending Fail`;
  }

  export class SetRead extends BaseAction {
    static readonly type: string = `[${state}] Set Read`;

    constructor(public notificationId: string, public mode: ViewModeEnum) {
      super();
    }
  }

  export class SetReadSuccess extends BaseSubActionSuccess<SetRead> {
    static readonly type: string = `[${state}] Set Read Success`;
  }

  export class SetReadFail extends BaseSubActionFail<SetRead> {
    static readonly type: string = `[${state}] Set Read Fail`;
  }

  export class SetUnread extends BaseAction {
    static readonly type: string = `[${state}] Set Unread`;

    constructor(public notificationId: string, public mode: ViewModeEnum) {
      super();
    }
  }

  export class SetUnreadSuccess extends BaseSubActionSuccess<SetUnread> {
    static readonly type: string = `[${state}] Set Unread Success`;
  }

  export class SetUnreadFail extends BaseSubActionFail<SetUnread> {
    static readonly type: string = `[${state}] Set Unread Fail`;
  }

  export class BulkSetApproved extends BaseAction {
    static readonly type: string = `[${state}] Bulk Set Approved`;

    constructor(public listNotification: NotificationDlcm[]) {
      super();
    }
  }

  export class BulkSetApprovedSuccess extends BaseSubActionSuccess<BulkSetApproved> {
    static readonly type: string = `[${state}] Bulk Set Approved Success`;
  }

  export class BulkSetApprovedFail extends BaseSubActionFail<BulkSetApproved> {
    static readonly type: string = `[${state}] Bulk Set Approved Fail`;
  }

  export class BulkSetRefuse extends BaseAction {
    static readonly type: string = `[${state}] Bulk Set Refuse`;

    constructor(public listNotification: NotificationDlcm[], public responseMessage: string) {
      super();
    }
  }

  export class BulkSetRefuseSuccess extends BaseSubActionSuccess<BulkSetRefuse> {
    static readonly type: string = `[${state}] Bulk Set Refuse Success`;
  }

  export class BulkSetRefuseFail extends BaseSubActionFail<BulkSetRefuse> {
    static readonly type: string = `[${state}] Bulk Set Refuse Fail`;
  }

  export class BulkSetPending extends BaseAction {
    static readonly type: string = `[${state}] Bulk Set Pending`;

    constructor(public listNotification: NotificationDlcm[]) {
      super();
    }
  }

  export class BulkSetPendingSuccess extends BaseSubActionSuccess<BulkSetPending> {
    static readonly type: string = `[${state}] Bulk Set Pending Success`;
  }

  export class BulkSetPendingFail extends BaseSubActionFail<BulkSetPending> {
    static readonly type: string = `[${state}] Bulk Set Pending Fail`;
  }

  export class BulkSetRead extends BaseAction {
    static readonly type: string = `[${state}] Bulk Set Read`;

    constructor(public listNotification: NotificationDlcm[]) {
      super();
    }
  }

  export class BulkSetReadSuccess extends BaseSubActionSuccess<BulkSetRead> {
    static readonly type: string = `[${state}] Bulk Set Read Success`;
  }

  export class BulkSetReadFail extends BaseSubActionFail<BulkSetRead> {
    static readonly type: string = `[${state}] Bulk Set Read Fail`;
  }

  export class BulkSetUnread extends BaseAction {
    static readonly type: string = `[${state}] Bulk Set Unread`;

    constructor(public listNotification: NotificationDlcm[]) {
      super();
    }
  }

  export class BulkSetUnreadSuccess extends BaseSubActionSuccess<BulkSetUnread> {
    static readonly type: string = `[${state}] Bulk Set Unread Success`;
  }

  export class BulkSetUnreadFail extends BaseSubActionFail<BulkSetUnread> {
    static readonly type: string = `[${state}] Bulk Set Unread Fail`;
  }

  export class RefreshDetailEvent extends BaseAction {
    static readonly type: string = `[${state}] Refresh Detail Event`;

    constructor(public notificationId: string) {
      super();
    }
  }

  export class RefreshListEvent extends BaseAction {
    static readonly type: string = `[${state}] Refresh List Event`;
  }
}

export const sharedNotificationActionNameSpace: ResourceNameSpace = SharedNotificationAction;
