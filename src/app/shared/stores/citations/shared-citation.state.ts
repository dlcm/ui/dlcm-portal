/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-citation.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Injectable,
  makeStateKey,
} from "@angular/core";
import {HomeStateModel} from "@home/stores/home.state";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";

import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  accessCitationNamespace,
  SharedCitationAction,
} from "@shared/stores/citations/shared-citation.action";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseState,
  BaseStateModel,
  Citation,
  defaultBaseStateInitValue,
  NotificationService,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  TransferStateService,
} from "solidify-frontend";

export const defaultCitationInitValue: () => SharedCitationStateModel = () =>
  ({
    ...defaultBaseStateInitValue(),
    citations: [],
    bibliographies: [],
  });

export interface SharedCitationStateModel extends BaseStateModel {
  citations: Citation[];
  bibliographies: Citation[];
}

@Injectable()
@State<SharedCitationStateModel>({
  name: StateEnum.shared_citation,
  defaults: {
    ...defaultCitationInitValue(),
  },
  children: [],
})
export class SharedCitationState extends BaseState<SharedCitationStateModel> {
  // protected readonly _nameSpace: CitationNamespace;

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _transferState: TransferStateService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: accessCitationNamespace,
    }, SharedCitationState);
  }

  protected get _urlResource(): string {
    return ApiEnum.accessPublicMetadata;
  }

  @Action(SharedCitationAction.GetBibliographies)
  getBibliographies(ctx: SolidifyStateContext<HomeStateModel>, action: SharedCitationAction.GetBibliographies): Observable<Citation[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      bibliographies: undefined,
    });

    const STATE_KEY = makeStateKey<Citation[]>(`SharedCitationState-Bibliographies-${action.archiveId}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const bibliographiesTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new SharedCitationAction.GetBibliographiesSuccess(action, bibliographiesTransferState));
      return of(bibliographiesTransferState);
    }

    return this._apiService.get<Citation[]>(`${this._urlResource}/${action.archiveId}/${ApiActionNameEnum.BIBLIOGRAPHIES}`)
      .pipe(
        tap((model: Citation[]) => {
          this._transferState.set(STATE_KEY, model);
          ctx.dispatch(new SharedCitationAction.GetBibliographiesSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedCitationAction.GetBibliographiesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedCitationAction.GetBibliographiesSuccess)
  getBibliographiesSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: SharedCitationAction.GetBibliographiesSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      bibliographies: action.bibliographies,
    });
  }

  @Action(SharedCitationAction.GetBibliographiesFail)
  getBibliographiesFail(ctx: SolidifyStateContext<HomeStateModel>, action: SharedCitationAction.GetBibliographiesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedCitationAction.GetCitations)
  getCitations(ctx: SolidifyStateContext<HomeStateModel>, action: SharedCitationAction.GetCitations): Observable<Citation[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      citations: undefined,
    });

    const STATE_KEY = makeStateKey<Citation[]>(`SharedCitationState-Citations-${action.archiveId}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const citationsTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new SharedCitationAction.GetCitationsSuccess(action, citationsTransferState));
      return of(citationsTransferState);
    }

    return this._apiService.get<Citation[]>(`${this._urlResource}/${action.archiveId}/${ApiActionNameEnum.CITATIONS}`)
      .pipe(
        tap((model: Citation[]) => {
          this._transferState.set(STATE_KEY, model);
          ctx.dispatch(new SharedCitationAction.GetCitationsSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedCitationAction.GetCitationsFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedCitationAction.GetCitationsSuccess)
  getCitationsSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: SharedCitationAction.GetCitationsSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      citations: action.citations,
    });
  }

  @Action(SharedCitationAction.GetCitationsFail)
  getCitationsFail(ctx: SolidifyStateContext<HomeStateModel>, action: SharedCitationAction.GetCitationsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
