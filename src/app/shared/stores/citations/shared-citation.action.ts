/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-citation.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseStoreNameSpace,
  BaseSubActionFail,
  BaseSubActionSuccess,
  Citation,
} from "solidify-frontend";

const state = StateEnum.shared_citation;

export namespace SharedCitationAction {
  export class GetBibliographies extends BaseAction {
    static readonly type: string = `[${state}] Get Bibliographies`;

    constructor(public archiveId: string) {
      super();
    }
  }

  export class GetBibliographiesSuccess extends BaseSubActionSuccess<GetBibliographies> {
    static readonly type: string = `[${state}] Get Bibliographies Success`;

    constructor(public parentAction: GetBibliographies, public bibliographies: Citation[]) {
      super(parentAction);
    }
  }

  export class GetBibliographiesFail extends BaseSubActionFail<GetBibliographies> {
    static readonly type: string = `[${state}] Get Bibliographies Fail`;
  }

  export class GetCitations extends BaseAction {
    static readonly type: string = `[${state}] Get Citations`;

    constructor(public archiveId: string) {
      super();
    }
  }

  export class GetCitationsSuccess extends BaseSubActionSuccess<GetCitations> {
    static readonly type: string = `[${state}] Get Citations Success`;

    constructor(public parentAction: GetCitations, public citations: Citation[]) {
      super(parentAction);
    }
  }

  export class GetCitationsFail extends BaseSubActionFail<GetCitations> {
    static readonly type: string = `[${state}] Get Citations Fail`;
  }
}

export const accessCitationNamespace: BaseStoreNameSpace = SharedCitationAction;
