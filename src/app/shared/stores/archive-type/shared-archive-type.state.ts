/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-archive-type.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {ArchiveType} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  SharedArchiveTypeAction,
  sharedArchiveTypeActionNameSpace,
} from "@shared/stores/archive-type/shared-archive-type.action";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  MappingObjectUtil,
  NotificationService,
  QueryParameters,
  QueryParametersUtil,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";

export const defaultSharedArchiveTypeStateModel: () => SharedArchiveTypeStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
    listMasterTypesNames: [],
  });

export interface SharedArchiveTypeStateModel extends ResourceStateModel<ArchiveType> {
  listMasterTypesNames: string[] | undefined;
}

@Injectable()
@State<SharedArchiveTypeStateModel>({
  name: StateEnum.shared_archiveType,
  defaults: {
    ...defaultSharedArchiveTypeStateModel(),
  },
  children: [],
})
export class SharedArchiveTypeState extends ResourceState<SharedArchiveTypeStateModel, ArchiveType> {
  static readonly FIELD_SEARCH_NAME: keyof ArchiveType = "typeName";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedArchiveTypeActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminArchiveType;
  }

  @Action(SharedArchiveTypeAction.GetListMasterTypesNames)
  getListMasterTypesNames(ctx: SolidifyStateContext<SharedArchiveTypeStateModel>, action: SharedArchiveTypeAction.GetListMasterTypesNames): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.get<string[]>(this._urlResource + urlSeparator + ApiActionNameEnum.LIST_REFERENCE_TYPES)
      .pipe(
        tap(listMasterTypesNames => ctx.dispatch(new SharedArchiveTypeAction.GetListMasterTypesNamesSuccess(action, listMasterTypesNames))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedArchiveTypeAction.GetListMasterTypesNamesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedArchiveTypeAction.GetListMasterTypesNamesSuccess)
  getListMasterTypesNamesSuccess(ctx: SolidifyStateContext<SharedArchiveTypeStateModel>, action: SharedArchiveTypeAction.GetListMasterTypesNamesSuccess): void {
    ctx.patchState({
      listMasterTypesNames: action.listMasterTypesNames,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedArchiveTypeAction.GetListMasterTypesNamesFail)
  getListMasterTypesNamesFail(ctx: SolidifyStateContext<SharedArchiveTypeStateModel>, action: SharedArchiveTypeAction.GetListMasterTypesNamesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedArchiveTypeAction.GetListMasterTypes)
  getListMasterTypes(ctx: SolidifyStateContext<SharedArchiveTypeStateModel>, action: SharedArchiveTypeAction.GetListMasterTypes): Observable<CollectionTyped<ArchiveType>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo);
    const search = QueryParametersUtil.getSearchItems(queryParameters);
    MappingObjectUtil.set(search, "masterType.resId", "true");
    return this._apiService.getCollection<ArchiveType>(this._urlResource, queryParameters)
      .pipe(
        tap(listMasterTypes => ctx.dispatch(new SharedArchiveTypeAction.GetListMasterTypesSuccess(action, listMasterTypes._data))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedArchiveTypeAction.GetListMasterTypesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedArchiveTypeAction.GetListMasterTypesSuccess)
  getListMasterTypesSuccess(ctx: SolidifyStateContext<SharedArchiveTypeStateModel>, action: SharedArchiveTypeAction.GetListMasterTypesSuccess): void {
    ctx.patchState({
      list: action.listMasterTypes,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedArchiveTypeAction.GetListMasterTypesFail)
  getListMasterTypesFail(ctx: SolidifyStateContext<SharedArchiveTypeStateModel>, action: SharedArchiveTypeAction.GetListMasterTypesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  static labelCallback(value: ArchiveType, translateService: TranslateService): string {
    return value[SharedArchiveTypeState.FIELD_SEARCH_NAME] as string;
  }
}
