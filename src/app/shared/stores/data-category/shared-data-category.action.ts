/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-data-category.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  QueryParameters,
} from "solidify-frontend";

const state = StateEnum.shared_data_category;

export namespace SharedDataCategoryAction {

  export class GetAllDataCategories extends BaseAction {
    static readonly type: string = `[${state}] Get All Data Categories`;

    constructor(public queryParameters?: QueryParameters, public keepCurrentContext: boolean = false, public cancelIncomplete: boolean = true) {
      super();
    }
  }

  export class GetAllDataCategoriesSuccess extends BaseSubActionSuccess<GetAllDataCategories> {
    static readonly type: string = `[${state}] Get All Data Categories Success`;

    constructor(public parentAction: GetAllDataCategories, public list: string[]) {
      super(parentAction);
    }
  }

  export class GetAllDataCategoriesFail extends BaseSubActionFail<GetAllDataCategories> {
    static readonly type: string = `[${state}] Get All Data Categories Fail`;
  }

  export class GetAllDataTypesByCategory extends BaseAction {
    static readonly type: string = `[${state}] Get All Data Types By Category`;

    constructor(public dataCategory: string, public queryParameters?: QueryParameters, public keepCurrentContext: boolean = false, public cancelIncomplete: boolean = true) {
      super();
    }
  }

  export class GetAllDataTypesByCategorySuccess extends BaseSubActionSuccess<GetAllDataTypesByCategory> {
    static readonly type: string = `[${state}] Get All Data Types By Category Success`;

    constructor(public parentAction: GetAllDataTypesByCategory, public list: string[]) {
      super(parentAction);
    }
  }

  export class GetAllDataTypesByCategoryFail extends BaseSubActionFail<GetAllDataTypesByCategory> {
    static readonly type: string = `[${state}] Get All Data Types By Category Fail`;
  }
}
