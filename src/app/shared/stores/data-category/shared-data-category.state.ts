/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-data-category.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameExtendEnum} from "@shared/enums/api-action-name.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedDataCategoryAction} from "@shared/stores/data-category/shared-data-category.action";
import {
  Observable,
  of,
  pipe,
} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BasicState,
  isNonEmptyArray,
  isNullOrUndefinedOrEmptyArray,
  MappingObject,
  MappingObjectUtil,
  NotificationService,
  QueryParameters,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
  TransferStateService,
} from "solidify-frontend";

export interface SharedDataCategoryStateModel {
  listDataCategories: string[] | undefined;
  listDataTypes: string[] | undefined;
  mapListDataTypesByCategory: MappingObject<string, string[]>;
  isLoadingCounter: number;
  queryParameters: QueryParameters;
}

@Injectable()
@State<SharedDataCategoryStateModel>({
  name: StateEnum.shared_data_category,
  defaults: {
    listDataCategories: undefined,
    listDataTypes: undefined,
    mapListDataTypesByCategory: {} as MappingObject<string, string[]>,
    isLoadingCounter: 0,
    queryParameters: new QueryParameters(),
  },
})
export class SharedDataCategoryState extends BasicState<SharedDataCategoryStateModel> {
  protected get _urlResource(): string {
    return ApiEnum.preIngestDepositsDataCategory;
  }

  constructor(private readonly _apiService: ApiService,
              private readonly _store: Store,
              private readonly _httpClient: HttpClient,
              private readonly _notificationService: NotificationService,
              private readonly _actions$: Actions,
              private readonly _transferState: TransferStateService) {
    super();
  }

  @Selector()
  static isLoading(state: SharedDataCategoryStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Action(SharedDataCategoryAction.GetAllDataCategories)
  getAllDataCategories(ctx: SolidifyStateContext<SharedDataCategoryStateModel>, action: SharedDataCategoryAction.GetAllDataCategories): Observable<string[]> {
    const listDataCategoriesInState = ctx.getState().listDataCategories;
    if (isNonEmptyArray(listDataCategoriesInState)) {
      ctx.patchState({
        isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      });
      ctx.dispatch(new SharedDataCategoryAction.GetAllDataCategoriesSuccess(action, listDataCategoriesInState));
      return of(listDataCategoriesInState);
    }
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        list: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });
    return this._apiService.get<string[]>(this._urlResource)
      .pipe(
        action.cancelIncomplete ? StoreUtil.cancelUncompleted(action, ctx, this._actions$, [SharedDataCategoryAction.GetAllDataCategories, Navigate]) : pipe(),
        tap((list: string[]) => {
          ctx.dispatch(new SharedDataCategoryAction.GetAllDataCategoriesSuccess(action, list));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedDataCategoryAction.GetAllDataCategoriesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedDataCategoryAction.GetAllDataCategoriesSuccess)
  getAllDataCategoriesSuccess(ctx: SolidifyStateContext<SharedDataCategoryStateModel>, action: SharedDataCategoryAction.GetAllDataCategoriesSuccess): void {
    ctx.patchState({
      listDataCategories: action.list,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedDataCategoryAction.GetAllDataCategoriesFail)
  getAllDataCategoriesFail(ctx: SolidifyStateContext<SharedDataCategoryStateModel>, action: SharedDataCategoryAction.GetAllDataCategoriesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(SharedDataCategoryAction.GetAllDataTypesByCategory)
  getAllDataTypesByCategory(ctx: SolidifyStateContext<SharedDataCategoryStateModel>, action: SharedDataCategoryAction.GetAllDataTypesByCategory): Observable<string[]> {
    const mapListDataTypesByCategory = ctx.getState().mapListDataTypesByCategory;
    const listDataTypeByCategory = MappingObjectUtil.get(mapListDataTypesByCategory, action.dataCategory);
    if (isNonEmptyArray(listDataTypeByCategory)) {
      ctx.patchState({
        isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      });
      ctx.dispatch(new SharedDataCategoryAction.GetAllDataTypesByCategorySuccess(action, listDataTypeByCategory));
      return of(listDataTypeByCategory);
    }

    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        list: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });
    const baseUrl = ApiEnum.preIngestDeposits + SOLIDIFY_CONSTANTS.URL_SEPARATOR + ApiActionNameExtendEnum.LIST_DATA_TYPE + "?category=" + action.dataCategory;
    return this._apiService.get<string[]>(baseUrl)
      .pipe(
        action.cancelIncomplete ? StoreUtil.cancelUncompleted(action, ctx, this._actions$, [SharedDataCategoryAction.GetAllDataCategories, Navigate]) : pipe(),
        tap((list: string[]) => {
          ctx.dispatch(new SharedDataCategoryAction.GetAllDataTypesByCategorySuccess(action, list));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new SharedDataCategoryAction.GetAllDataTypesByCategoryFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(SharedDataCategoryAction.GetAllDataTypesByCategorySuccess)
  getAllDataTypesByCategorySuccess(ctx: SolidifyStateContext<SharedDataCategoryStateModel>, action: SharedDataCategoryAction.GetAllDataTypesByCategorySuccess): void {
    const obj = {
      listDataTypes: action.list,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    } as Partial<SharedDataCategoryStateModel>;
    const mapListDataTypesByCategory = MappingObjectUtil.copy(ctx.getState().mapListDataTypesByCategory);
    const listDataTypeByCategory = MappingObjectUtil.get(mapListDataTypesByCategory, action.parentAction.dataCategory);

    if (isNullOrUndefinedOrEmptyArray(listDataTypeByCategory)) {
      MappingObjectUtil.set(mapListDataTypesByCategory, action.parentAction.dataCategory, action.list);
      obj.mapListDataTypesByCategory = mapListDataTypesByCategory;
    }

    ctx.patchState(obj);
  }

  @Action(SharedDataCategoryAction.GetAllDataTypesByCategoryFail)
  getAllDataTypesByCategoryFail(ctx: SolidifyStateContext<SharedDataCategoryStateModel>, action: SharedDataCategoryAction.GetAllDataTypesByCategoryFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

}
