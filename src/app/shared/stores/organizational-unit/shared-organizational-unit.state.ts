/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-organizational-unit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  sharedOrganizationalUnitActionNameSpace,
  SharedOrgUnitAction,
} from "@app/shared/stores/organizational-unit/shared-organizational-unit.action";
import {AppState} from "@app/stores/app.state";
import {environment} from "@environments/environment";
import {OrganizationalUnit} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  SharedOrganizationalUnitDisseminationPolicyState,
  SharedOrganizationalUnitDisseminationPolicyStateModel,
} from "@shared/stores/organizational-unit/dissemination-policy/shared-organizational-unit-dissemination-policy.state";
import {
  SharedOrganizationalUnitPreservationPolicyState,
  SharedOrganizationalUnitPreservationPolicyStateModel,
} from "@shared/stores/organizational-unit/preservation-policy/shared-organizational-unit-preservation-policy.state";
import {
  SharedOrganizationalUnitSubmissionPolicyState,
  SharedOrganizationalUnitSubmissionPolicyStateModel,
} from "@shared/stores/organizational-unit/submission-policy/shared-organizational-unit-submission-policy.state";
import {Observable} from "rxjs";
import {
  ApiService,
  defaultRelation2TiersStateInitValue,
  defaultResourceFileStateInitValue,
  DownloadService,
  isNullOrUndefined,
  MemoizedUtil,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface SharedOrganizationalUnitStateModel extends ResourceFileStateModel<OrganizationalUnit> {
  shared_organizationalUnit_preservationPolicy: SharedOrganizationalUnitPreservationPolicyStateModel;
  shared_organizationalUnit_submissionPolicy: SharedOrganizationalUnitSubmissionPolicyStateModel;
  shared_organizationalUnit_disseminationPolicy: SharedOrganizationalUnitDisseminationPolicyStateModel;
}

@Injectable()
@State<SharedOrganizationalUnitStateModel>({
  name: StateEnum.shared_organizationalUnit,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    shared_organizationalUnit_preservationPolicy: {...defaultRelation2TiersStateInitValue()},
    shared_organizationalUnit_submissionPolicy: {...defaultRelation2TiersStateInitValue()},
    shared_organizationalUnit_disseminationPolicy: {...defaultRelation2TiersStateInitValue()},
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  },
  children: [
    SharedOrganizationalUnitPreservationPolicyState,
    SharedOrganizationalUnitSubmissionPolicyState,
    SharedOrganizationalUnitDisseminationPolicyState,
  ],
})
export class SharedOrganizationalUnitState extends ResourceFileState<SharedOrganizationalUnitStateModel, OrganizationalUnit> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: sharedOrganizationalUnitActionNameSpace,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    let isLogged = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn);
    if (isNullOrUndefined(isLogged)) {
      isLogged = false;
    }
    return isLogged ? ApiEnum.adminOrganizationalUnits : ApiEnum.accessOrganizationalUnits;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @OverrideDefaultAction()
  @Action(SharedOrgUnitAction.GetById)
  getById(ctx: SolidifyStateContext<SharedOrganizationalUnitStateModel>, action: SharedOrgUnitAction.GetById): Observable<OrganizationalUnit> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return super.getById(ctx, action);
  }

  @OverrideDefaultAction()
  @Action(SharedOrgUnitAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<SharedOrganizationalUnitStateModel>, action: SharedOrgUnitAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @OverrideDefaultAction()
  @Action(SharedOrgUnitAction.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<SharedOrganizationalUnitStateModel>, action: SharedOrgUnitAction.GetByIdFail): void {
    super.getByIdFail(ctx, action);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
