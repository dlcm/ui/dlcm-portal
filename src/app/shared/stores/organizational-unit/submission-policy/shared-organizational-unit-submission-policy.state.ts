/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-organizational-unit-submission-policy.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {OrganizationalUnitSubmissionPolicy} from "@admin/models/organizational-unit-submission-policy.model";
import {Injectable} from "@angular/core";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {SubmissionPolicy} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  SharedOrgUnitSubmissionPolicyAction,
  sharedOrgUnitSubmissionPolicyStateNamespace,
} from "@shared/stores/organizational-unit/submission-policy/shared-organizational-unit-submission-policy.action";
import {
  ApiService,
  defaultRelation2TiersStateInitValue,
  NotificationService,
  Relation2TiersState,
  Relation2TiersStateModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface SharedOrganizationalUnitSubmissionPolicyStateModel extends Relation2TiersStateModel<SubmissionPolicy> {
}

@Injectable()
@State<SharedOrganizationalUnitSubmissionPolicyStateModel>({
  name: StateEnum.shared_organizationalUnit_submissionPolicy,
  defaults: {
    ...defaultRelation2TiersStateInitValue(),
  },
})
export class SharedOrganizationalUnitSubmissionPolicyState extends Relation2TiersState<SharedOrganizationalUnitSubmissionPolicyStateModel, SubmissionPolicy, OrganizationalUnitSubmissionPolicy> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedOrgUnitSubmissionPolicyStateNamespace,
      resourceName: ApiResourceNameEnum.SUB_POLICY,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOrganizationalUnits;
  }

  @Action(SharedOrgUnitSubmissionPolicyAction.Clear)
  clear(ctx: SolidifyStateContext<SharedOrganizationalUnitSubmissionPolicyStateModel>, action: SharedOrgUnitSubmissionPolicyAction.Clear): void {
    ctx.patchState({
      selected: null,
    });
  }
}
