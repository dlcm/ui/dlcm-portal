/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-organizational-unit-preservation-policy.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {OrganizationalUnitPreservationPolicy} from "@app/features/admin/models/organizational-unit-preservation-policy.model";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {PreservationPolicy} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  SharedOrgUnitPreservationPolicyAction,
  sharedOrgUnitPreservationPolicyActionNameSpace,
} from "@shared/stores/organizational-unit/preservation-policy/shared-organizational-unit-preservation-policy.action";
import {
  ApiService,
  defaultRelation2TiersStateInitValue,
  NotificationService,
  Relation2TiersState,
  Relation2TiersStateModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface SharedOrganizationalUnitPreservationPolicyStateModel extends Relation2TiersStateModel<PreservationPolicy> {
}

@Injectable()
@State<SharedOrganizationalUnitPreservationPolicyStateModel>({
  name: StateEnum.shared_organizationalUnit_preservationPolicy,
  defaults: {
    ...defaultRelation2TiersStateInitValue(),
  },
})
export class SharedOrganizationalUnitPreservationPolicyState extends Relation2TiersState<SharedOrganizationalUnitPreservationPolicyStateModel, PreservationPolicy, OrganizationalUnitPreservationPolicy> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedOrgUnitPreservationPolicyActionNameSpace,
      resourceName: ApiResourceNameEnum.PRES_POLICY,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOrganizationalUnits;
  }

  @Action(SharedOrgUnitPreservationPolicyAction.Clear)
  clear(ctx: SolidifyStateContext<SharedOrganizationalUnitPreservationPolicyStateModel>, action: SharedOrgUnitPreservationPolicyAction.Clear): void {
    ctx.patchState({
      selected: null,
    });
  }
}
