/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-archive-acl.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {ArchiveACL} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedArchiveAclActionNameSpace} from "@shared/stores/archive-acl/shared-archive-acl.action";
import {
  ApiService,
  defaultResourceStateInitValue,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceState,
  ResourceStateModel,
} from "solidify-frontend";

export interface SharedArchiveAclStateModel extends ResourceStateModel<ArchiveACL> {
}

@Injectable()
@State<SharedArchiveAclStateModel>({
  name: StateEnum.shared_archiveAcl,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
  children: [],
})
export class SharedArchiveAclState extends ResourceState<SharedArchiveAclStateModel, ArchiveACL> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedArchiveAclActionNameSpace,
      notificationResourceCreateFailTextToTranslate: MARK_AS_TRANSLATABLE("archiveAcl.create.notification.fail"),
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("archiveAcl.create.notification.success"),
      notificationResourceUpdateFailTextToTranslate: MARK_AS_TRANSLATABLE("archiveAcl.update.notification.fail"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("archiveAcl.update.notification.success"),
      notificationResourceDeleteFailTextToTranslate: MARK_AS_TRANSLATABLE("archiveAcl.delete.notification.fail"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("archiveAcl.delete.notification.success"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminArchiveAcl;
  }
}
