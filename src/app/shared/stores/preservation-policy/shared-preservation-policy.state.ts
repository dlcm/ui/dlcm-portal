/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-preservation-policy.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {sharedPreservationPolicyActionNameSpace} from "@app/shared/stores/preservation-policy/shared-preservation-policy.action";
import {environment} from "@environments/environment";
import {PreservationPolicy} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceStateInitValue,
  NotificationService,
  OrderEnum,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
} from "solidify-frontend";

export interface SharedPreservationPolicyStateModel extends ResourceStateModel<PreservationPolicy> {
}

@Injectable()
@State<SharedPreservationPolicyStateModel>({
  name: StateEnum.shared_preservationPolicy,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption, {
      field: "name",
      order: OrderEnum.ascending,
    }),
  },
})
export class SharedPreservationPolicyState extends ResourceState<SharedPreservationPolicyStateModel, PreservationPolicy> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: sharedPreservationPolicyActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPreservationPolicies;
  }
}
