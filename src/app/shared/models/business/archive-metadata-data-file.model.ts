/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - archive-metadata-data-file.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {SolidifyObject} from "solidify-frontend";

export interface ArchiveMetadataDataFile {
  resId?: string;
  index: string;
  type: string;
  metadata: ArchiveMetadataDataFile.Metadata;
  _links?: SolidifyObject;
}

export enum MetadataDataFileEnum {
  aipAccessLevel = "aip-access-level",
  aipEmbargoDate = "aip-embargo-date",
  aipEmbargoLevel = "aip-embargo-level",
  aipOrganizationalUnit = "aip-organizational-unit",
  aipComplianceLevel = "aip-compliance-level",
  aipDataTag = "aip-data-tag",
  aipTitle = "aip-title",
  checksumsSha1 = "SHA-1",
  checksumsSha256 = "SHA-256",
  checksumsMd5 = "MD5",
  checksumsCrc32 = "CRC32",
  eventsFormatIdentification = "format-identification",
  eventsVirusCheck = "virusCheck",
  technicalMetadata = "technical-metadata",
  technicalMetadataImageColorSpace = "color-space",
}

export namespace ArchiveMetadataDataFile {
  export interface Metadata {
    [MetadataDataFileEnum.aipAccessLevel]: Enums.Access.AccessEnum;
    [MetadataDataFileEnum.aipComplianceLevel]: Enums.ComplianceLevel.ComplianceLevelEnum;
    [MetadataDataFileEnum.aipDataTag]: Enums.DataSensitivity.DataSensitivityEnum;
    [MetadataDataFileEnum.aipOrganizationalUnit]: string;
    [MetadataDataFileEnum.aipTitle]: string;
    archiveId: string;
    checksums: Checksums;
    dlcm: Dlcm;
    events: Events;
    file: File;
    aip: Aip;
    format: Format;
    [MetadataDataFileEnum.technicalMetadata]: TechnicalMetadata;
    type: string;
  }

  export interface Checksums {
    [MetadataDataFileEnum.checksumsMd5]: string;
    [MetadataDataFileEnum.checksumsSha1]: string;
    [MetadataDataFileEnum.checksumsSha256]: string;
  }

  export interface Dlcm {
    category: Enums.DataFile.DataCategoryAndType.DataCategoryEnum;
    complianceLevel: Enums.ComplianceLevel.ComplianceLevelNumberEnum;
    type: Enums.DataFile.DataCategoryAndType.DataTypeEnum;
  }

  export interface Events {
    creation: EventsDate;
    [MetadataDataFileEnum.eventsFormatIdentification]: EventsDate;
    [MetadataDataFileEnum.eventsVirusCheck]: EventsDate;
  }

  export interface EventsDate {
    date: Date;
  }

  export interface File {
    fullName: string;
    name: string;
    path: string;
    size: number;
    contentType?: string;
  }

  export interface Aip {
    id: string;
    size: number;
  }

  export interface Format {
    PRONOM: string;
    description: string;
    version: number | string;
  }

  export interface TechnicalMetadata {
    image: TechnicalMetadataImage;
  }

  export interface TechnicalMetadataImage {
    [MetadataDataFileEnum.technicalMetadataImageColorSpace]: string;
  }
}

