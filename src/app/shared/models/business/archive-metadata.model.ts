/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - archive-metadata.model.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";

export enum MetadataEnum {
  aipDispositionApproval = "aip-disposition-approval",
  aipOrganizationalUnit = "aip-organizational-unit",
  aipTitle = "aip-title",
  aipThumbnail = "aip-thumbnail",
  aipDataTag = "aip-data-tag",
  aipRetention = "aip-retention",
  aipRetentionDuration = "aip-retention-duration",
  aipRetentionEnd = "aip-retention-end",
  aipSize = "aip-size",
  aipFileNumber = "aip-file-number",
  dataciteXml = "datacite.xml",
  dlcmXml = "dlcm.xml",
  aipContainer = "aip-container",
  aipAccessLevel = "aip-access-level",
  aipComplianceLevel = "aip-compliance-level",
  aipCreation = "aip-creation",
  aipDataUsePolicy = "aip-data-use-policy",
  aipDua = "aip-dua",
  aipDuaContentType = "aip-dua-content-type",
  aipEmbargoDate = "aip-embargo-date",
  aipEmbargoLevel = "aip-embargo-level",
  aipInstitutionDescriptions = "aip-institution-descriptions",
  aipInstitution = "aip-institution",
  aipLastUpdate = "aip-last-update",
  aipMetadataFileNumber = "aip-metadata-file-number",
  aipOrganizationalUnitName = "aip-organizational-unit-name",
  aipReadme = "aip-readme",
  aipReadmeContentType = "aip-readme-content-type",
  aipTotalFileNumber = "aip-total-file-number",
  aipType = "aip-type",
  aipContentStructurePublic = "aip-content-structure-public",

  /* PRIVATE */
  premis = "premis",
  premisComplexType = "premisComplexType",

  premisAgent = "premis:agent",
  premisAgentIdentifier = "premis:agentIdentifier",
  premisAgentIdentifierType = "premis:agentIdentifierType",
  premisAgentIdentifierValue = "premis:agentIdentifierValue",
  premisAgentType = "premis:agentType",
  premisAgentVersion = "premis:agentVersion",

  premisEvent = "premis:event",
  premisEventDateTime = "premis:eventDateTime",
  premisEventIdentifier = "premis:eventIdentifier",
  premisEventIdentifierType = "premis:eventIdentifierType",
  premisEventIdentifierValue = "premis:eventIdentifierValue",
  premisEventType = "premis:eventType",
  premisLinkingObjectIdentifier = "premis:LinkingObjectIdentifier",
  premisLinkingObjectIdentifierType = "premis:LinkingObjectIdentifierType",
  premisLinkingObjectIdentifierValue = "premis:LinkingObjectIdentifierValue",

  premisObject = "premis:object",
  premisOriginalName = "premis:originalMail",
  premisObjectIdentifier = "premis:objectIdentifier",
  premisObjectIdentifierType = "premis:objectIdentifierType",
  premisObjectIdentifierValue = "premis:objectIdentifierValue",

  version = "version",

  /* PUBLIC */
  dataciteResource = "datacite:resource",
  datacite = "datacite",
  xmlnsDatacite = "xmlns:datacite",
  dataciteDates = "datacite:dates",
  dataciteDate = "datacite:date",
  dataciteTitles = "datacite:titles",
  dataciteTitle = "datacite:title",
  dataciteResourceType = "datacite:resourceType",
  content = "content",
  contributorType = "contributorType",
  dataciteRelatedIdentifiers = "datacite:relatedIdentifiers",
  dataciteRelatedIdentifier = "datacite:relatedIdentifier",
  dataciteSubjects = "datacite:subjects",
  dataciteSubject = "datacite:subject",
  dataciteFormats = "datacite:formats",
  dataciteFormat = "datacite:format",
  dataciteIdentifier = "datacite:identifier",
  identifierType = "identifierType",
  dataciteRightsList = "datacite:rightsList",
  dataciteRights = "datacite:rights",
  datacitePublicationYear = "datacite:publicationYear",
  datacitePublisher = "datacite:publisher",
  dataciteDescriptions = "datacite:descriptions",
  dataciteDescription = "datacite:description",
  descriptionType = "descriptionType",
  dataciteFundingReferences = "datacite:fundingReferences",
  dataciteFundingReference = "datacite:fundingReference",
  dataciteFunderIdentifier = "datacite:funderIdentifier",
  dataciteFunderName = "datacite:funderName",
  dataciteContributors = "datacite:contributors",
  dataciteContributor = "datacite:contributor",
  dataciteCreators = "datacite:creators",
  dataciteCreator = "datacite:creator",
  dataciteCreatorName = "datacite:creatorName",
  dataciteFamilyName = "datacite:familyName",
  dataciteGivenName = "datacite:givenName",
  dataciteNameIdentifier = "datacite:nameIdentifier",
  dataciteAffiliation = "datacite:affiliation",
  dataciteContributorName = "datacite:contributorName",
  xmlnsXsi = "xmlns:xsi",
  xsiType = "xsi:type",
  xmlLang = "xml:lang",
  aipUnit = "aip-unit",
  dateType = "dateType",
}

export namespace ArchiveMetadataNamespace {

  export abstract class Metadata {
    [MetadataEnum.aipAccessLevel]: string;
    [MetadataEnum.aipComplianceLevel]: string;
    [MetadataEnum.aipContainer]: string;
    [MetadataEnum.aipCreation]: string;
    [MetadataEnum.aipDataTag]: string;
    [MetadataEnum.aipDispositionApproval]: boolean;
    [MetadataEnum.aipDataUsePolicy]: Enums.Deposit.DataUsePolicyEnum;
    [MetadataEnum.aipDua]: boolean;
    [MetadataEnum.aipDuaContentType]: string;
    [MetadataEnum.aipEmbargoDate]: string;
    [MetadataEnum.aipEmbargoLevel]: string;
    [MetadataEnum.aipFileNumber]: number;
    [MetadataEnum.aipInstitutionDescriptions]: string[];
    [MetadataEnum.aipInstitution]: string[];
    [MetadataEnum.aipLastUpdate]: string;
    [MetadataEnum.aipMetadataFileNumber]: number;
    [MetadataEnum.aipOrganizationalUnit]: string;
    [MetadataEnum.aipOrganizationalUnitName]: string;
    [MetadataEnum.aipReadme]: boolean;
    [MetadataEnum.aipReadmeContentType]: string;
    [MetadataEnum.aipRetention]: number;
    [MetadataEnum.aipRetentionDuration]: string;
    [MetadataEnum.aipRetentionEnd]: string;
    [MetadataEnum.aipSize]: number;
    [MetadataEnum.aipThumbnail]: number;
    [MetadataEnum.aipTitle]: string;
    [MetadataEnum.aipTotalFileNumber]: number;
    [MetadataEnum.aipType]: string;
    [MetadataEnum.aipUnit]: boolean;
    [MetadataEnum.aipContentStructurePublic]: boolean;
    creation: string;
    ark: string;
  }

  export interface PublicMetadata extends Metadata {
    [MetadataEnum.datacite]: Datacite;
    [MetadataEnum.dataciteXml]: string;
  }

  export interface PrivateMetadata extends Metadata {
    [MetadataEnum.premis]: Premis;
    [MetadataEnum.dlcmXml]: string;
  }

  /* PRIVATE */
  export interface Premis {
    [MetadataEnum.premisComplexType]: PremisComplexType;
  }

  export interface PremisComplexType {
    [MetadataEnum.premisAgent]: PremisAgent[];
    [MetadataEnum.premisEvent]: PremisEvent[];
    [MetadataEnum.premisObject]: PremisObject[];
    [MetadataEnum.version]: number;
  }

  export interface PremisAgent {
    [MetadataEnum.premisAgentIdentifier]: PremisAgentIdentifier;
    [MetadataEnum.premisAgentType]: string;
    [MetadataEnum.premisAgentVersion]: string;
  }

  export interface PremisAgentIdentifier {
    [MetadataEnum.premisAgentIdentifierType]: string;
    [MetadataEnum.premisAgentIdentifierValue]: string;
  }

  export interface PremisEvent {
    [MetadataEnum.premisEventIdentifier]: PremisEventIdentifier;
    [MetadataEnum.premisEventType]: string;
    [MetadataEnum.premisEventDateTime]: string;
    [MetadataEnum.premisLinkingObjectIdentifier]: PremisLinkingObjectIdentifier;
  }

  export interface PremisEventIdentifier {
    [MetadataEnum.premisEventIdentifierType]: string;
    [MetadataEnum.premisEventIdentifierValue]: string;
  }

  export interface PremisLinkingObjectIdentifier {
    [MetadataEnum.premisLinkingObjectIdentifierType]: string;
    [MetadataEnum.premisLinkingObjectIdentifierValue]: string;
  }

  export interface PremisObject {
    [MetadataEnum.premisObjectIdentifier]: PremisObjectIdentifier;
    [MetadataEnum.premisOriginalName]: string;
  }

  export interface PremisObjectIdentifier {
    [MetadataEnum.premisObjectIdentifierType]: string;
    [MetadataEnum.premisObjectIdentifierValue]: string;
  }

  /* PUBLIC */
  export interface Datacite {
    [MetadataEnum.dataciteResource]: DataciteResource;
  }

  export interface DataciteResource {
    [MetadataEnum.dataciteContributors]: DataciteContributors;
    [MetadataEnum.dataciteCreators]: DataciteCreators;
    [MetadataEnum.dataciteDates]: DataciteDates;
    [MetadataEnum.dataciteDescriptions]: DataciteDescriptions;
    [MetadataEnum.dataciteFormats]: DataciteFormats;
    [MetadataEnum.dataciteFundingReferences]: DataciteFundingReferences;
    [MetadataEnum.dataciteIdentifier]: DataciteIdentifier;
    [MetadataEnum.datacitePublicationYear]: number;
    [MetadataEnum.datacitePublisher]: string;
    [MetadataEnum.dataciteRelatedIdentifiers]: DataciteRelatedIdentifiers;
    [MetadataEnum.dataciteResourceType]: DataciteResourceType;
    [MetadataEnum.dataciteRightsList]: DataciteRightsList;
    [MetadataEnum.dataciteTitles]: DataciteTitles;
    [MetadataEnum.dataciteSubjects]: DataciteSubjects;
  }

  export interface DataciteContributors {
    [MetadataEnum.dataciteContributor]: DataciteContributor;
  }

  export interface DataciteContributor {
    contributorType: string;
    [MetadataEnum.dataciteAffiliation]: DataciteAffiliation[];
    [MetadataEnum.dataciteContributorName]: DataciteContributorName;
  }

  export interface DataciteFundingReferences {
    [MetadataEnum.dataciteFundingReference]: DataciteFundingReference;
  }

  export interface DataciteFundingReference {
    [MetadataEnum.dataciteFunderIdentifier]: DataciteFunderIdentifier;
    [MetadataEnum.dataciteFunderName]: string;
  }

  export interface DataciteFunderIdentifier {
    content: string;
    funderIdentifierType: string;
  }

  export interface DataciteAffiliation {
    affiliationIdentifier: string;
    affiliationIdentifierScheme: string;
    content: string;
    [MetadataEnum.xmlnsXsi]: string;
    [MetadataEnum.xsiType]: string;
  }

  export interface DataciteNameIdentifier {
    content: string;
    nameIdentifierScheme: string;
    schemeURI: string;
    [MetadataEnum.xmlnsXsi]: string;
    [MetadataEnum.xsiType]: string;
  }

  export interface DataciteCreatorName {
    content: string;
    nameType: string;
  }

  export interface DataciteContributorName {
    content: string;
    nameType: string;
  }

  export interface DataciteCreators {
    [MetadataEnum.dataciteCreator]: DataciteCreator | DataciteCreator[];
  }

  export interface DataciteCreator {
    [MetadataEnum.dataciteAffiliation]: DataciteAffiliation[] | DataciteAffiliation;
    [MetadataEnum.dataciteCreatorName]: DataciteCreatorName;
    [MetadataEnum.dataciteFamilyName]: string;
    [MetadataEnum.dataciteGivenName]: string;
    [MetadataEnum.dataciteNameIdentifier]: DataciteNameIdentifier;
  }

  export interface DataciteDates {
    [MetadataEnum.dataciteDate]: DataciteDate;
  }

  export interface DataciteDate {
    content: string;
    dateType: string;
  }

  export interface DataciteDescriptions {
    [MetadataEnum.dataciteDescription]: DataciteDescription[];
  }

  export interface DataciteDescription {
    descriptionType: string;
    content?: string;
  }

  export interface DataciteFormats {
    [MetadataEnum.dataciteFormat]: string[];
  }

  export interface DataciteIdentifier {
    content: string;
    [MetadataEnum.identifierType]: string;
  }

  export interface DataciteRelatedIdentifiers {
    [MetadataEnum.dataciteRelatedIdentifier]: DataciteRelatedIdentifier[];
  }

  export interface DataciteRelatedIdentifier {
    content: string;
    relatedIdentifierType: string;
    relationType: string;
  }

  export interface DataciteResourceType {
    content: string;
    resourceTypeGeneral: string;
  }

  export interface DataciteRightsList {
    [MetadataEnum.dataciteRights]: DataciteRights[];
  }

  export interface DataciteRights {
    content: string;
    rightsIdentifier: string;
    rightsIdentifierScheme: string;
    rightsURI: string;
  }

  export interface DataciteTitles {
    [MetadataEnum.dataciteTitle]: string[];
  }

  export interface DataciteSubjects {
    [MetadataEnum.dataciteSubject]: DataciteSubject;
  }

  export interface DataciteSubject {
    content: string;
    subjectScheme: string;
    [MetadataEnum.xmlLang]: string;
  }
}

