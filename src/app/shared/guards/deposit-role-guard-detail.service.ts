/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-role-guard-detail.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {AppState} from "@app/stores/app.state";
import {DepositAction} from "@deposit/stores/deposit.action";
import {DepositState} from "@deposit/stores/deposit.state";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {TourRouteIdEnum} from "@shared/enums/tour-route-id.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  Observable,
  of,
} from "rxjs";
import {
  map,
  take,
} from "rxjs/operators";
import {
  isNullOrUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class DepositRoleGuardDetailService implements CanActivate {
  constructor(public readonly router: Router,
              public readonly store: Store,
              private readonly _securityService: SecurityService) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const depositId: string = route.params[AppRoutesEnum.paramIdWithoutPrefixParam];
    const isInTourMode = MemoizedUtil.selectSnapshot(this.store, AppState, state => state.isInTourMode);
    const isDepositIdForTour = depositId === TourRouteIdEnum.tourDepositId;
    if (isDepositIdForTour || isInTourMode) {
      const isAuthorizedToDisplayTour = isDepositIdForTour && isInTourMode;
      if (!isAuthorizedToDisplayTour) {
        this.store.dispatch(new Navigate([RoutesEnum.deposit]));
      }
      return of(isAuthorizedToDisplayTour);
    }
    return this.canSeeDetailDeposit(depositId).pipe(take(1));
  }

  canSeeDetailDeposit(depositId: string, adminBypassOrgUnitRole: boolean = true): Observable<boolean> {
    if (this._securityService.isRootOrAdminIfBypassRole(adminBypassOrgUnitRole)) {
      return of(true);
    }

    let currentDeposit = MemoizedUtil.currentSnapshot(this.store, DepositState);
    if (isNullOrUndefined(currentDeposit) || currentDeposit.resId !== depositId) {
      return this.store.dispatch(new DepositAction.GetById(depositId)).pipe(
        map(state => {
          currentDeposit = MemoizedUtil.currentSnapshot(this.store, DepositState);
          return this._securityService.isMemberOfOrgUnit(currentDeposit.organizationalUnitId);
        }),
      );
    } else {
      return of(this._securityService.isMemberOfOrgUnit(currentDeposit.organizationalUnitId));
    }
  }
}
