/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - organizational-unit-compute-is-manager.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {PreservationSpaceOrganizationalUnitAction} from "@app/features/preservation-space/organizational-unit/stores/preservation-space-organizational-unit.action";
import {AppState} from "@app/stores/app.state";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {TourRouteIdEnum} from "@shared/enums/tour-route-id.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  ApiService,
  MemoizedUtil,
  NotificationService,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class OrganizationalUnitComputeIsManagerGuardService implements CanActivate {
  constructor(public readonly router: Router,
              public readonly store: Store,
              public readonly apiService: ApiService,
              private readonly _securityService: SecurityService,
              private readonly _notificationService: NotificationService,
              private readonly _actions$: Actions) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const orgUnitId = route.params[AppRoutesEnum.paramIdWithoutPrefixParam];
    const isInTourMode = MemoizedUtil.selectSnapshot(this.store, AppState, state => state.isInTourMode);
    const isOrgUnitIdForTour = orgUnitId === TourRouteIdEnum.tourOrgUnitId;
    if (isOrgUnitIdForTour || isInTourMode) {
      const isAuthorizedToDisplayTour = isOrgUnitIdForTour && isInTourMode;
      if (!isAuthorizedToDisplayTour) {
        this.store.dispatch(new Navigate([RoutesEnum.preservationSpaceOrganizationalUnit]));
      }
      return isAuthorizedToDisplayTour;
    }
    const isMemberOrgUnit = this._securityService.isRootOrAdmin() || this._securityService.isMemberOfOrgUnit(orgUnitId);
    const isManager = this._securityService.isManagerOfOrgUnit(orgUnitId);
    this.store.dispatch(new PreservationSpaceOrganizationalUnitAction.SaveCurrentUserAffiliation(isManager, isMemberOrgUnit));
    return true;
  }
}
