/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - organizational-unit-role-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {Store} from "@ngxs/store";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {DlcmRouteData} from "@shared/models/dlcm-route.model";
import {SecurityService} from "@shared/services/security.service";
import {
  ApiService,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class OrganizationalUnitRoleGuardService implements CanActivate {
  constructor(public readonly router: Router,
              public readonly store: Store,
              public readonly apiService: ApiService,
              private readonly _securityService: SecurityService,
              private readonly _notificationService: NotificationService) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const data = route.data as DlcmRouteData;
    const orgUnitPermissionNeed = isNullOrUndefined(data) ? [] : data.orgUnitPermissionNeed;
    let orgUnitId = route.parent.params[AppRoutesEnum.paramIdWithoutPrefixParam];
    if (isNullOrUndefined(orgUnitId)) {
      orgUnitId = route.parent.parent.params[AppRoutesEnum.paramIdWithoutPrefixParam];
    }
    if (isNullOrUndefined(orgUnitId)) {
      // eslint-disable-next-line no-console
      console.warn("Unable to extract orgUnitId from url");
      return false;
    }
    if (orgUnitPermissionNeed.length === 0) {
      return true;
    }
    if (this._securityService.isRootOrAdmin()) {
      return true;
    }
    const authorized = orgUnitPermissionNeed.indexOf(this._securityService.getRoleEnumInOrgUnit(orgUnitId)) !== -1;
    if (authorized === false) {
      this._notificationService.showWarning(MARK_AS_TRANSLATABLE("organizationalUnit.security.notification.noRightForThisAction"));
    }
    return authorized;
  }
}
