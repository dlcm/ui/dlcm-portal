/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - archive-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {HomeAction} from "@home/stores/home.action";
import {HomeState} from "@home/stores/home.state";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {Observable} from "rxjs";
import {
  map,
  take,
} from "rxjs/operators";
import {
  ApiService,
  isNullOrUndefined,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  StoreUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class ArchiveGuardService implements CanActivate {
  constructor(public readonly router: Router,
              private readonly _store: Store,
              public readonly apiService: ApiService,
              private readonly _actions$: Actions) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    const archiveId: string = route.params[AppRoutesEnum.paramIdWithoutPrefixParam];

    return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(this._store, [
      {
        action: new HomeAction.SearchDetail(archiveId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(HomeAction.SearchDetailSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(HomeAction.SearchDetailFail)),
        ],
      },
    ]).pipe(
      take(1),
      map(result => {
        if (result.success) {
          const archive = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.current);

          if (isNullOrUndefined(archive) || archive.resId !== archiveId) {
            return this._redirectToPageNotFound(archiveId);
          }
          return true;
        } else {
          return this._redirectToPageNotFound(archiveId);
        }
      }),
    );
  }

  private _redirectToPageNotFound(id: string): false {
    this._store.dispatch(new Navigate([RoutesEnum.homeNotFound, id]));
    return false;
  }
}
