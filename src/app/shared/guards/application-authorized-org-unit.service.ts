/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - application-authorized-org-unit.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import {AppAuthorizedOrganizationalUnitState} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.state";
import {Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {
  filter,
  map,
  take,
} from "rxjs/operators";
import {
  isFalse,
  MemoizedUtil,
  OAuth2Service,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class ApplicationAuthorizedOrgUnitService implements CanActivate {
  constructor(public readonly router: Router, public readonly store: Store, private readonly _oauthService: OAuth2Service) {
  }

  canActivate(activatedRoute: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): Observable<boolean> {
    return MemoizedUtil.isLoading(this.store, AppAuthorizedOrganizationalUnitState).pipe(
      filter(isLoading => isFalse(isLoading)),
      take(1),
      map(isLoading => {
        const isAuthorizedOrganizationalUnitFinishedToLoad = isFalse(isLoading);
        return isAuthorizedOrganizationalUnitFinishedToLoad;
      }),
    );
  }
}
