/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-role-guard-edit.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {DepositState} from "@deposit/stores/deposit.state";
import {Store} from "@ngxs/store";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  Observable,
  of,
} from "rxjs";
import {
  map,
  take,
} from "rxjs/operators";
import {
  ApiService,
  isFalse,
  isNullOrUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class DepositRoleGuardEditService implements CanActivate {
  constructor(public readonly router: Router,
              public readonly store: Store,
              public readonly apiService: ApiService,
              private readonly _securityService: SecurityService) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    let depositId: string = route.parent.parent.params[AppRoutesEnum.paramIdWithoutPrefixParam];
    if (isNullOrUndefined(depositId)) {
      depositId = route.parent.params[AppRoutesEnum.paramIdWithoutPrefixParam];
    }
    return this.canEditDepositById(depositId).pipe(take(1));
  }

  canEditDepositById(depositId: string, adminBypassOrgUnitRole: boolean = true): Observable<boolean> {
    if (this._securityService.isRootOrAdminIfBypassRole(adminBypassOrgUnitRole)) {
      return of(true);
    }

    return this._securityService.canSeeDetailDepositByIdObs(depositId).pipe(
      map((isAuthorized: boolean) => {
        if (isFalse(isAuthorized)) {
          return false;
        }
        const currentDeposit = MemoizedUtil.currentSnapshot(this.store, DepositState);
        if (!this._securityService.depositEditionModeStep(currentDeposit)) {
          return false;
        }
        return this._securityService.currentUserHaveRoleInListForOrgUnit(currentDeposit.organizationalUnitId, this._securityService.DEPOSIT_ROLE_NEED_TO_EDIT);
      }),
    );
  }
}
