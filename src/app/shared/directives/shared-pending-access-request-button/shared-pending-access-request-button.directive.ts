/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-pending-access-request-button.directive.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ComponentFactoryResolver,
  Directive,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  ViewContainerRef,
} from "@angular/core";
import {SecurityService} from "@app/shared/services/security.service";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {NotificationDlcm} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  SharedPendingAccessRequestPresentational,
  sharedPendingAccessRequestPresentationalSelector,
} from "@shared/components/presentationals/shared-pending-access-request/shared-pending-access-request.presentational";
import {SharedAbstractDirective} from "@shared/directives/shared-abstract/shared-abstract.directive";
import {SharedNotificationAction} from "@shared/stores/notification/shared-notification.action";
import {SharedNotificationState} from "@shared/stores/notification/shared-notification.state";
import {tap} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  PollingHelper,
  StoreUtil,
} from "solidify-frontend";

@Directive({
  selector: "[dlcmPendingAccessRequestButton]",
})
export class SharedPendingAccessRequestButtonDirective extends SharedAbstractDirective implements OnInit {
  @Input("dlcmPendingAccessRequestButtonType")
  type: "archive" | "orgUnit";

  @Input("dlcmPendingAccessRequestButtonResourceId")
  resourceId: string;

  constructor(private readonly _element: ElementRef,
              private readonly _viewContainerRef: ViewContainerRef,
              private readonly _componentFactoryResolver: ComponentFactoryResolver,
              private readonly _store: Store,
              private readonly _renderer: Renderer2,
              private readonly _securityService: SecurityService,
              private readonly _actions$: Actions,
  ) {
    super();
  }

  private _getNotificationType(): Enums.Notification.TypeEnum {
    if (this.type === "archive") {
      return Enums.Notification.TypeEnum.ACCESS_DATASET_REQUEST;
    }
    if (this.type === "orgUnit") {
      return Enums.Notification.TypeEnum.JOIN_ORGUNIT_REQUEST;
    }
    throw new Error(`Unmanaged type '${this.type}'`);
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (!this._securityService.isLoggedIn()) {
      return;
    }

    this._watchChangeToUpdateDisplay();
    this._updateRecurrently();
  }

  private _watchChangeToUpdateDisplay(): void {
    const keyMapPendingNotification = SharedNotificationState.getMapPendingNotificationName(this._getNotificationType());
    const mapNotificationPendingObs = MemoizedUtil.select(this._store, SharedNotificationState, state => state[keyMapPendingNotification] as MappingObject<string, NotificationDlcm>);
    this.subscribe(mapNotificationPendingObs.pipe(tap(map => {
      const notification = MappingObjectUtil.get(map, this.resourceId);
      this._updateDisplayButton(notification);
    })));
  }

  private _updateRecurrently(): void {
    this.subscribe(PollingHelper.startPollingObs({
      waitingTimeBeforeStartInSecond: 0,
      initialIntervalRefreshInSecond: environment.refreshCleanPendingRequestNotificationIntervalInSecond,
      incrementInterval: false,
      resetIntervalWhenUserMouseEvent: false,
      maximumIntervalRefreshInSecond: environment.refreshCleanPendingRequestNotificationIntervalInSecond * 10,
      actionToDo: () => {
        this._getPendingNotification();
      },
    }));
  }

  private _getPendingNotification(): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedNotificationAction.GetPendingNotification(this._getNotificationType(), this.resourceId),
      SharedNotificationAction.GetPendingNotificationSuccess,
      successAction => {
        const notification = successAction.notification;
        this._updateDisplayButton(notification);
      },
    ));
  }

  private _updateDisplayButton(notification: NotificationDlcm): void {
    if (isNullOrUndefined(notification)) {
      return;
    }
    if (notification.notificationStatus === Enums.Notification.StatusEnum.PENDING) {
      this._renderer.addClass(this._element.nativeElement, "mat-button-disabled");
      this._renderer.addClass(this._element.nativeElement, "disabled");
      this._element.nativeElement.disabled = true;
    } else {
      this._renderer.removeClass(this._element.nativeElement, "mat-button-disabled");
      this._renderer.removeClass(this._element.nativeElement, "disabled");
      this._element.nativeElement.disabled = false;
    }
    const componentFactory = this._componentFactoryResolver
      .resolveComponentFactory(SharedPendingAccessRequestPresentational);

    const componentRef = this._viewContainerRef.createComponent(componentFactory);

    const instance = componentRef.instance;
    instance.notification = notification;

    const host = this._element.nativeElement;
    const previousComponent = host.querySelector(sharedPendingAccessRequestPresentationalSelector);
    if (isNotNullNorUndefined(previousComponent)) {
      host.removeChild(previousComponent);
    }
    host.insertBefore(componentRef.location.nativeElement, host.firstChild);
    instance.externalChangeDetector();
  }
}
