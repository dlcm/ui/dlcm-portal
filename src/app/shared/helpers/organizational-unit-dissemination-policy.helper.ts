/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - organizational-unit-dissemination-policy.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DLCM_CONSTANTS} from "@app/constants";
import {OrganizationalUnitDisseminationPolicyContainer} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  isNotNullNorUndefinedNorWhiteString,
  MappingObject,
  MappingObjectUtil,
} from "solidify-frontend";

export class OrganizationalUnitDisseminationPolicyHelper {
  static getComputedName(translateService: TranslateService, organizationalUnitDisseminationPolicyContainer: OrganizationalUnitDisseminationPolicyContainer): string {
    const parametersString = organizationalUnitDisseminationPolicyContainer?.joinResource?.parameters;
    if (isNotNullNorUndefinedNorWhiteString(parametersString)) {
      const parameters: MappingObject<string> = JSON.parse(parametersString);
      const name: string = MappingObjectUtil.get(parameters, DLCM_CONSTANTS.ORGANIZATIONAL_UNIT_DISSEMINATION_POLICY_PARAMETER_NAME);
      if (isNotNullNorUndefinedNorWhiteString(name)
        && isNotNullNorUndefinedNorWhiteString(organizationalUnitDisseminationPolicyContainer.name)
        && name.toLowerCase() !== organizationalUnitDisseminationPolicyContainer.name.toLowerCase()) {
        return `[${name}] - ${organizationalUnitDisseminationPolicyContainer.name}`;
      }
    }
    return `[${organizationalUnitDisseminationPolicyContainer.name}] ${translateService.instant(LabelTranslateEnum.default)}`;
  }

  static fillComputedName(translateService: TranslateService, organizationalUnitDisseminationPolicyContainer: OrganizationalUnitDisseminationPolicyContainer): OrganizationalUnitDisseminationPolicyContainer {
    return {
      ...organizationalUnitDisseminationPolicyContainer,
      computedName: OrganizationalUnitDisseminationPolicyHelper.getComputedName(translateService, organizationalUnitDisseminationPolicyContainer),
    };
  }

  static fillComputedNameInList(translateService: TranslateService, listOrganizationalUnitDisseminationPolicyContainer: OrganizationalUnitDisseminationPolicyContainer[]): OrganizationalUnitDisseminationPolicyContainer[] {
    return listOrganizationalUnitDisseminationPolicyContainer?.map(d => OrganizationalUnitDisseminationPolicyHelper.fillComputedName(translateService, d));
  }
}
