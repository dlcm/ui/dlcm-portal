/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-notification.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MatDialog} from "@angular/material/dialog";
import {Enums} from "@enums";
import {NotificationDlcm} from "@models";
import {Store} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {SharedNotificationAction} from "@shared/stores/notification/shared-notification.action";
import {Observable} from "rxjs";
import {
  ButtonColorEnum,
  ConfirmDialog,
  ConfirmDialogInputEnum,
  DialogUtil,
  EnumUtil,
  isEmptyString,
  isString,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

export class SharedNotificationHelper {

  public static getRefuseDialog(dialog: MatDialog, store: Store, mode: ViewModeEnum | undefined, notification: NotificationDlcm): Observable<any> {

    return DialogUtil.open(dialog, ConfirmDialog, {
      titleToTranslate: EnumUtil.getLabel(Enums.Notification.TypeEnumTranslate, notification.notificationType.resId),
      messageToTranslate: MARK_AS_TRANSLATABLE("notification.notification.pleaseExplainRefusalReason"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      withInput: ConfirmDialogInputEnum.TEXTAREA,
      inputRequired: true,
      inputLabelToTranslate: LabelTranslateEnum.reasonForRejection,
      colorConfirm: ButtonColorEnum.warn,
      colorCancel: ButtonColorEnum.primary,
    }, {
      minWidth: "500px",
      maxWidth: "90vw",
      takeOne: true,
    }, responseMessage => {
      if (!isString(responseMessage) || isEmptyString(responseMessage)) {
        return;
      }
      store.dispatch(new SharedNotificationAction.SetRefuse(notification.resId, notification.notificationType.notificationCategory, mode, responseMessage));
    });
  }
}
