/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - toc.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Renderer2} from "@angular/core";
import {ApiEnum} from "@shared/enums/api.enum";
import {urlSeparator} from "@shared/enums/routes.enum";

export class TocHelper {
  static rewriteLink(document: Document, renderer: Renderer2, classContainerLink: string, baseUrl: string = ApiEnum.adminDocs): void {
    const listA = document.querySelectorAll(`.${classContainerLink} a:not([active="true"])`);
    listA.forEach(a => {
      const existingHref = a.attributes["href"].value;
      renderer.setAttribute(a, "href", baseUrl + urlSeparator + existingHref);
      renderer.setAttribute(a, "active", "true");
    });
  }
}
