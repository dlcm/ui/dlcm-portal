/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - archive-data-file.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {
  ArchiveDataFile,
  DataFileChecksum,
} from "@models";
import {
  ArchiveMetadataDataFile,
  MetadataDataFileEnum,
} from "@shared/models/business/archive-metadata-data-file.model";
import {
  FileSizePipe,
  isNullOrUndefined,
  isTruthyObject,
  MappingObjectUtil,
} from "solidify-frontend";

export class ArchiveDataFileHelper {
  private static _fileSizePipe: FileSizePipe = new FileSizePipe();

  static adaptListArchivesMetadataInArchive(listArchivesMetadata: ArchiveMetadataDataFile[]): ArchiveDataFile[] {
    const newList: ArchiveDataFile[] = [];

    listArchivesMetadata.forEach((archive: ArchiveMetadataDataFile) => {
      newList.push(this.adaptArchiveMetadataInArchiveDataFile(archive));
    });
    return newList;
  }

  static adaptArchiveMetadataInArchiveDataFile(archiveMetadata: ArchiveMetadataDataFile): ArchiveDataFile {
    if (isNullOrUndefined(archiveMetadata)) {
      return undefined;
    }
    if (archiveMetadata.metadata.type === "aip") {
      return this._adaptArchiveMetadataInArchiveDataFileCollection(archiveMetadata);
    } else {
      return this._adaptArchiveMetadataInArchiveDataFileFile(archiveMetadata);
    }
  }

  private static _adaptArchiveMetadataInArchiveDataFileFile(archiveMetadata: ArchiveMetadataDataFile): ArchiveDataFile {
    const archiveDataFile = {
      resId: archiveMetadata.resId,
      creation: {
        when: archiveMetadata.metadata.events?.creation?.date,
      } as any,
      lastUpdate: null,
      sourceData: null,
      status: null,
      statusMessage: null,
      virusCheck: {
        checkDate: archiveMetadata.metadata.events?.[MetadataDataFileEnum.eventsVirusCheck]?.date,
      } as any,
      smartSize: this._fileSizePipe.transform(archiveMetadata.metadata?.file?.size),
      relativeLocation: archiveMetadata.metadata?.file?.path,
      infoPackage: {
        resId: archiveMetadata.metadata?.archiveId,
      },
      finalData: null,
      fileSize: archiveMetadata.metadata?.file?.size,
      fileName: archiveMetadata.metadata?.file?.name,
      fileFormat: {
        format: archiveMetadata.metadata?.format?.description,
        md5: archiveMetadata.metadata?.checksums?.MD5,
        puid: archiveMetadata.metadata?.format?.PRONOM,
        version: archiveMetadata.metadata?.format?.version,
      },
      dataCategory: archiveMetadata.metadata?.dlcm?.category,
      dataType: archiveMetadata.metadata?.dlcm?.type,
      metadataType: null,
      complianceLevel: Enums.ComplianceLevel.ConvertComplianceLevel.convertNumberToString(archiveMetadata.metadata?.dlcm?.complianceLevel),
      checksums: this._getCheckums(archiveMetadata),
      contentType: archiveMetadata.metadata?.file?.contentType,
    } as ArchiveDataFile;
    if (MappingObjectUtil.size(archiveDataFile.virusCheck as any) === 0) {
      archiveDataFile.virusCheck = undefined;
    }
    return archiveDataFile;
  }

  private static _adaptArchiveMetadataInArchiveDataFileCollection(archiveMetadata: ArchiveMetadataDataFile): ArchiveDataFile {
    return {
      resId: archiveMetadata.metadata?.aip?.id,
      lastUpdate: null,
      sourceData: null,
      status: null,
      statusMessage: null,
      smartSize: this._fileSizePipe.transform(archiveMetadata.metadata?.aip?.size),
      relativeLocation: null,
      infoPackage: {
        resId: archiveMetadata.metadata?.archiveId,
        accessLevel: archiveMetadata.metadata?.[MetadataDataFileEnum.aipAccessLevel],
        title: archiveMetadata.metadata?.[MetadataDataFileEnum.aipTitle],
        dataSensitivity: archiveMetadata.metadata?.[MetadataDataFileEnum.aipDataTag],
      },
      finalData: null,
      fileSize: archiveMetadata.metadata?.aip?.size,
      fileName: archiveMetadata.resId,
      metadataType: null,
      complianceLevel: archiveMetadata.metadata?.[MetadataDataFileEnum.aipComplianceLevel],
      checksums: this._getCheckums(archiveMetadata),
      info: {
        embargo: {
          access: archiveMetadata.metadata?.[MetadataDataFileEnum.aipEmbargoLevel],
          endDate: archiveMetadata.metadata?.[MetadataDataFileEnum.aipEmbargoDate],
        },
      },
    } as ArchiveDataFile | any;
  }

  private static _getCheckums(archiveMetadata: ArchiveMetadataDataFile): DataFileChecksum[] {
    const checksums: DataFileChecksum[] = [];

    if (isTruthyObject(archiveMetadata.metadata?.checksums)) {
      const map = MappingObjectUtil.toMap<MetadataDataFileEnum, string>(archiveMetadata.metadata.checksums as any);
      MappingObjectUtil.forEach(map, (value: string, key, mappingObject) => {
        checksums.push({
          checksum: value,
          checksumAlgo: this.getAlgoName(key),
        });
      });
    }

    return checksums;
  }

  static getAlgoName(algoNameInArchive: MetadataDataFileEnum): Enums.DataFile.Checksum.AlgoEnum {
    switch (algoNameInArchive) {
      case MetadataDataFileEnum.checksumsMd5:
        return Enums.DataFile.Checksum.AlgoEnum.MD5;
      case MetadataDataFileEnum.checksumsSha1:
        return Enums.DataFile.Checksum.AlgoEnum.SHA1;
      case MetadataDataFileEnum.checksumsSha256:
        return Enums.DataFile.Checksum.AlgoEnum.SHA256;
      case MetadataDataFileEnum.checksumsCrc32:
        return Enums.DataFile.Checksum.AlgoEnum.CRC32;
      default:
        return algoNameInArchive as Enums.DataFile.Checksum.AlgoEnum;
    }
  }
}
