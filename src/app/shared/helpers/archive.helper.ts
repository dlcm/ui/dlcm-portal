/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - archive.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ArchiveMetadata} from "@app/models";
import {Enums} from "@enums";
import {Archive} from "@models";
import {MetadataDataFileEnum} from "@shared/models/business/archive-metadata-data-file.model";
import {MetadataEnum} from "@shared/models/business/archive-metadata.model";
import {MetadataUtil} from "@shared/utils/metadata.util";
import {
  EnumUtil,
  FileSizePipe,
  isNullOrUndefined,
} from "solidify-frontend";

export class ArchiveHelper {
  private static _fileSizePipe: FileSizePipe = new FileSizePipe();

  static adaptListArchivesMetadataInArchive(listArchivesMetadata: ArchiveMetadata[]): Archive[] {
    const newList: Archive[] = [];

    listArchivesMetadata.forEach((archive: ArchiveMetadata) => {
      newList.push(this.adaptArchiveMetadataInArchive(archive));
    });
    return newList;
  }

  static adaptArchiveMetadataInArchive(archiveMetadata: ArchiveMetadata): Archive {
    if (isNullOrUndefined(archiveMetadata)) {
      return undefined;
    }
    const size = MetadataUtil.getSize(archiveMetadata.metadata);
    const currentAccessLevel = archiveMetadata.currentAccess;
    const accessLevelDuringEmbargo = MetadataUtil.getEmbargoAccessLevel(archiveMetadata.metadata);
    const accessLevelAfterEmbargo = MetadataUtil.getFinalAccessLevel(archiveMetadata.metadata);
    const dataSensibility = MetadataUtil.getDataSensibility(archiveMetadata.metadata);
    return {
      resId: archiveMetadata.resId,
      currentAccessLevel: archiveMetadata.currentAccess,
      organizationalUnitId: archiveMetadata.metadata[MetadataEnum.aipOrganizationalUnit],
      title: MetadataUtil.getTitle(archiveMetadata.metadata),
      description: MetadataUtil.getDescriptionAbstract(archiveMetadata.metadata),
      yearPublicationDate: MetadataUtil.getPublicationYear(archiveMetadata.metadata),
      retentionDate: MetadataUtil.getRetentionDate(archiveMetadata.metadata),
      retentionDay: MetadataUtil.getRetentionDay(archiveMetadata.metadata),
      retentionDuration: archiveMetadata.metadata[MetadataEnum.aipRetentionDuration],
      dataSensitivity: dataSensibility,
      dataSensitivityToTranslate: EnumUtil.getLabel(Enums.DataSensitivity.DataSensitivityEnumTranslate, dataSensibility),
      accessLevel: currentAccessLevel,
      accessLevelToTranslate: EnumUtil.getLabel(Enums.Access.AccessEnumTranslate, currentAccessLevel),
      sizeDisplay: isNullOrUndefined(size) ? undefined : this._fileSizePipe.transform(size),
      size: isNullOrUndefined(size) ? undefined : size,
      files: MetadataUtil.getTotalFileNumber(archiveMetadata.metadata),
      contributors: MetadataUtil.getContributors(archiveMetadata.metadata),
      institutions: MetadataUtil.getInstitutions(archiveMetadata.metadata),
      fundingAgencies: MetadataUtil.getFundingAgencies(archiveMetadata.metadata),
      license: MetadataUtil.getLicense(archiveMetadata.metadata),
      keywords: MetadataUtil.getKeywords(archiveMetadata.metadata),
      archiveMetadata: archiveMetadata,
      withThumbnail: MetadataUtil.getWithThumbnail(archiveMetadata.metadata),
      withDua: MetadataUtil.getWithDua(archiveMetadata.metadata),
      withDuaFile: MetadataUtil.getWithDuaFile(archiveMetadata.metadata),
      duaContentType: MetadataUtil.getDuaContentType(archiveMetadata.metadata),
      readmeContentType: MetadataUtil.getReadmeContentType(archiveMetadata.metadata),
      dataUsePolicy: MetadataUtil.getDataUsePolicy(archiveMetadata.metadata),
      isCollection: MetadataUtil.isCollection(archiveMetadata.metadata),
      withReadme: MetadataUtil.withReadme(archiveMetadata.metadata),
      info: {
        embargo: {
          access: accessLevelDuringEmbargo,
          endDate: archiveMetadata.metadata?.[MetadataDataFileEnum.aipEmbargoDate],
          accessAfterEmbargo: accessLevelAfterEmbargo,
        },
      },
      ark: archiveMetadata.metadata.ark,
    } as Archive;
  }
}
