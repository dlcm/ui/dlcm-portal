/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - metadata.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ArchiveMetadataNamespace,
  MetadataEnum,
} from "@app/shared/models/business/archive-metadata.model";
import {Enums} from "@enums";
import {
  ArchiveAffiliation,
  ArchiveContributor,
  ArchiveFundingAgency,
  ArchiveInstitution,
  ArchiveLicense,
} from "@models";
import {
  DateUtil,
  isArray,
  isFalse,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";
import Metadata = ArchiveMetadataNamespace.PublicMetadata;
import DataUsePolicyEnum = Enums.Deposit.DataUsePolicyEnum;

export class MetadataUtil {
  private static _DLCM_URI_FINAL_ACCESS_LEVEL: string = "https://www.dlcm.ch/access-level/final";
  private static _DLCM_URI_EMBARGO_ACCESS_LEVEL: string = "https://www.dlcm.ch/access-level/embargo";
  private static _DLCM_URI_DATA_USE_POLICY: string = "https://www.dlcm.ch/data-use-policy";
  private static _DLCM_URI_DATA_TAG: string = "https://www.dlcm.ch/data-tag";

  static getTitle(metadata: Metadata): string {
    return metadata[MetadataEnum.aipTitle];
  }

  static getWithThumbnail(metadata: Metadata): boolean {
    const withThumbnail = metadata[MetadataEnum.aipThumbnail];
    return isTrue(withThumbnail);
  }

  static getWithDua(metadata: Metadata): boolean {
    return metadata[MetadataEnum.aipDua];
  }

  static getDuaContentType(metadata: Metadata): string {
    return metadata[MetadataEnum.aipDuaContentType];
  }

  static getReadmeContentType(metadata: Metadata): string {
    return metadata[MetadataEnum.aipReadmeContentType];
  }

  static getWithDuaFile(metadata: Metadata): boolean {
    const aipDua = metadata[MetadataEnum.aipDataUsePolicy];
    return this.getWithDua(metadata) && aipDua !== DataUsePolicyEnum.EXTERNAL_DUA.toString();
  }

  static getDataUsePolicy(metadata: Metadata): Enums.Deposit.DataUsePolicyEnum {
    return metadata[MetadataEnum.aipDataUsePolicy];
  }

  static isCollection(metadata: Metadata): boolean {
    const aipUnit = metadata[MetadataEnum.aipUnit];
    return isFalse(aipUnit);
  }

  static withReadme(metadata: Metadata): boolean {
    return metadata[MetadataEnum.aipReadme];
  }

  static getPublicationYear(metadata: Metadata): string {
    const publicationYear = this._getDataciteResource(metadata)?.[MetadataEnum.datacitePublicationYear];
    if (isNullOrUndefined(publicationYear)) {
      return undefined;
    }
    return publicationYear + "";
  }

  static getRetentionDate(metadata: Metadata): string {
    const retentionEnd = metadata[MetadataEnum.aipRetentionEnd];
    if (isNullOrUndefinedOrWhiteString(retentionEnd)) {
      return;
    }
    return DateUtil.convertDateToDateString(new Date(retentionEnd));
  }

  static getRetentionDay(metadata: Metadata): number {
    return +metadata[MetadataEnum.aipRetention];
  }

  static getFinalAccessLevel(metadata: Metadata): string {
    return metadata[MetadataEnum.aipAccessLevel];
  }

  static getDataSensibility(metadata: Metadata): string {
    const dataSensibility = metadata[MetadataEnum.aipDataTag];
    return isNullOrUndefined(dataSensibility) ? Enums.DataSensitivity.DataSensitivityEnum.UNDEFINED : dataSensibility;
  }

  static getEmbargoAccessLevel(metadata: Metadata): string {
    return metadata[MetadataEnum.aipEmbargoLevel];
  }

  static getLicense(metadata: Metadata): ArchiveLicense {
    let rights: ArchiveMetadataNamespace.DataciteRights[] = this._getDataciteResource(metadata)?.[MetadataEnum.dataciteRightsList]?.[MetadataEnum.dataciteRights];
    rights = this._getNormalizeInArray<ArchiveMetadataNamespace.DataciteRights>(rights);
    const rightLicense = rights?.find(r =>
      r.rightsURI !== MetadataUtil._DLCM_URI_FINAL_ACCESS_LEVEL &&
      r.rightsURI !== MetadataUtil._DLCM_URI_EMBARGO_ACCESS_LEVEL &&
      r.rightsURI !== MetadataUtil._DLCM_URI_DATA_USE_POLICY &&
      r.rightsURI !== MetadataUtil._DLCM_URI_DATA_TAG);
    if (isNullOrUndefined(rightLicense)) {
      return undefined;
    }
    return {
      name: rightLicense.content,
      url: rightLicense.rightsURI,
      identifier: rightLicense.rightsIdentifier,
      identifierType: rightLicense.rightsIdentifierScheme,
    } as ArchiveLicense;
  }

  static getKeywords(metadata: Metadata): string[] {
    const subject: ArchiveMetadataNamespace.DataciteSubject = this._getDataciteResource(metadata)?.[MetadataEnum.dataciteSubjects]?.[MetadataEnum.dataciteSubject];
    const keywords = subject?.content;
    if (isNullOrUndefinedOrWhiteString(keywords)) {
      return [];
    }
    return keywords.split(SOLIDIFY_CONSTANTS.SEMICOLON);
  }

  static getFundingAgencies(metadata: Metadata): ArchiveFundingAgency[] {
    const fundingReferences: ArchiveMetadataNamespace.DataciteFundingReference = this._getDataciteResource(metadata)?.[MetadataEnum.dataciteFundingReferences]?.[MetadataEnum.dataciteFundingReference];
    const listFundingReferences = this._getNormalizeInArray<ArchiveMetadataNamespace.DataciteFundingReference>(fundingReferences);

    const results = [];
    listFundingReferences?.forEach(fundingReference => {
      const fundingAgency = {} as ArchiveFundingAgency;
      fundingAgency.name = fundingReference[MetadataEnum.dataciteFunderName];
      fundingAgency.identifierUrl = fundingReference[MetadataEnum.dataciteFunderIdentifier]?.content;
      fundingAgency.identifierType = fundingReference[MetadataEnum.dataciteFunderIdentifier]?.funderIdentifierType;
      fundingAgency.identifier = this._extractIdentifierFromUrl(fundingAgency.identifierType as Enums.IdentifiersEnum, fundingAgency.identifierUrl);
      results.push(fundingAgency);
    });
    return results;
  }

  private static _getNormalizeInArray<T>(element: T | T[] | undefined): T[] {
    if (isNullOrUndefined(element)) {
      return [];
    } else if (isArray(element)) {
      return element;
    } else {
      return [element];
    }
  }

  private static _getItemInListBasedOnAttribute<T>(targetItems: T | T[] | undefined, attributeKey: keyof T, attributeValue: string): T {
    const items = this._getNormalizeInArray<T>(targetItems);
    return items?.find(d => d[attributeKey] === attributeValue);
  }

  private static _getDescriptionContent(metadata: Metadata, descriptionType: string): string {
    const descriptions: ArchiveMetadataNamespace.DataciteDescription[] = this._getDataciteResource(metadata)?.[MetadataEnum.dataciteDescriptions]?.[MetadataEnum.dataciteDescription];
    return this._getItemInListBasedOnAttribute(descriptions, MetadataEnum.descriptionType, descriptionType)?.content;
  }

  static getDescriptionAbstract(metadata: Metadata): string {
    return this._getDescriptionContent(metadata, "Abstract");
  }

  static getDescriptionOther(metadata: Metadata): string {
    return this._getDescriptionContent(metadata, "Other");
  }

  static getDescriptionTechnicalInfo(metadata: Metadata): string {
    return this._getDescriptionContent(metadata, "TechnicalInfo");
  }

  static getDescriptionTableOfContents(metadata: Metadata): string {
    return this._getDescriptionContent(metadata, "TableOfContents");
  }

  static getDescriptionSeriesInformation(metadata: Metadata): string {
    return this._getDescriptionContent(metadata, "SeriesInformation");
  }

  static getDescriptionMethods(metadata: Metadata): string {
    return this._getDescriptionContent(metadata, "Methods");
  }

  static getDOI(metadata: Metadata): string {
    const identifiers: ArchiveMetadataNamespace.DataciteIdentifier = this._getDataciteResource(metadata)?.[MetadataEnum.dataciteIdentifier];
    return this._getItemInListBasedOnAttribute(identifiers, MetadataEnum.identifierType, "DOI")?.content;
  }

  static getDoiIsIdenticalTo(metadata: Metadata): string[] {
    return this._getDoiListByRelationType(metadata, "IsIdenticalTo");
  }

  static getDoiIsReferencedBy(metadata: Metadata): string[] | undefined {
    return this._getDoiListByRelationType(metadata, "IsReferencedBy");
  }

  static getDoiIsObsoletedBy(metadata: Metadata): string[] | undefined {
    return this._getDoiListByRelationType(metadata, "IsObsoletedBy");
  }

  private static _getDoiListByRelationType(metadata: Metadata, relationType: string): string[] | undefined {
    let relatedIdentifiers: ArchiveMetadataNamespace.DataciteRelatedIdentifier[] = this._getDataciteResource(metadata)?.[MetadataEnum.dataciteRelatedIdentifiers]?.[MetadataEnum.dataciteRelatedIdentifier];
    relatedIdentifiers = this._getNormalizeInArray<ArchiveMetadataNamespace.DataciteRelatedIdentifier>(relatedIdentifiers);
    return relatedIdentifiers?.filter(r => r.relatedIdentifierType === "DOI" && r.relationType === relationType)?.map(r => r.content);
  }

  static getContributors(metadata: Metadata): ArchiveContributor[] {
    const dataciteCreators: ArchiveMetadataNamespace.DataciteCreator[] = this._getNormalizeInArray<ArchiveMetadataNamespace.DataciteCreator>(this._getDataciteResource(metadata)?.[MetadataEnum.dataciteCreators]?.[MetadataEnum.dataciteCreator]);
    const results = [];
    dataciteCreators?.forEach(creator => {
      const contributor = {} as ArchiveContributor;
      contributor.creatorName = creator[MetadataEnum.dataciteCreatorName]?.content;
      contributor.givenName = creator[MetadataEnum.dataciteGivenName];
      contributor.familyName = creator[MetadataEnum.dataciteFamilyName];
      if (creator[MetadataEnum.dataciteNameIdentifier]?.nameIdentifierScheme === "ORCID") {
        contributor.orcid = creator[MetadataEnum.dataciteNameIdentifier]?.content;
      }
      const dataciteAffiliations: ArchiveMetadataNamespace.DataciteAffiliation[] = this._getNormalizeInArray<ArchiveMetadataNamespace.DataciteAffiliation>(creator[MetadataEnum.dataciteAffiliation]);
      contributor.affiliations = [];
      dataciteAffiliations?.forEach(dataciteAffiliation => {
        let rorId = undefined;
        const identifier = dataciteAffiliation.affiliationIdentifier;
        const identifierSchema = dataciteAffiliation.affiliationIdentifierScheme;
        if (identifierSchema === Enums.IdentifiersEnum.ROR && isNotNullNorUndefinedNorWhiteString(identifier)) {
          rorId = identifier;
        }
        const affiliation = {
          name: dataciteAffiliation.content,
          rorIdLink: rorId,
        } as ArchiveAffiliation;
        contributor.affiliations.push(affiliation);
      });
      results.push(contributor);
    });
    return results;
  }

  static getInstitutions(metadata: Metadata): ArchiveInstitution[] {
    const contributor: ArchiveMetadataNamespace.DataciteContributor = this._getDataciteResource(metadata)?.[MetadataEnum.dataciteContributors]?.[MetadataEnum.dataciteContributor];
    const researchGroup = this._getItemInListBasedOnAttribute(contributor, MetadataEnum.contributorType, "ResearchGroup");
    const results = [];
    if (isNotNullNorUndefined(researchGroup) && isNotNullNorUndefinedNorWhiteString(researchGroup[MetadataEnum.dataciteAffiliation])) {
      const affiliations = this._getNormalizeInArray(researchGroup[MetadataEnum.dataciteAffiliation]);
      affiliations?.forEach(affiliation => {
        const institution = {} as ArchiveInstitution;
        institution.name = affiliation.content;
        institution.identifierUrl = affiliation.affiliationIdentifier;
        institution.identifierType = affiliation.affiliationIdentifierScheme;
        institution.identifier = this._extractIdentifierFromUrl(institution.identifierType as Enums.IdentifiersEnum, institution.identifierUrl);
        results.push(institution);
      });
    }
    return results;
  }

  private static _extractIdentifierFromUrl(identifierType: Enums.IdentifiersEnum, identifierUrl: string): string | undefined {
    if (identifierType === Enums.IdentifiersEnum.ROR && isNotNullNorUndefinedNorWhiteString(identifierUrl)) {
      const lastIndexOfSeparator = identifierUrl.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR);
      return lastIndexOfSeparator !== -1 ? identifierUrl.substring(lastIndexOfSeparator + 1) : undefined;
    }
    return undefined;
  }

  static getType(metadata: Metadata): string {
    const types: ArchiveMetadataNamespace.DataciteResourceType = this._getDataciteResource(metadata)?.[MetadataEnum.dataciteResourceType];
    return types?.[MetadataEnum.content];
  }

  static getEmbargoEndDate(metadata: Metadata): Date | null {
    return this._getDate(metadata, "Available");
  }

  static getAcceptedDate(metadata: Metadata): Date | null {
    return this._getDate(metadata, "Accepted");
  }

  static getIssuedDate(metadata: Metadata): Date | null {
    return this._getDate(metadata, "Issued");
  }

  static getSubmittedDate(metadata: Metadata): Date | null {
    return this._getDate(metadata, "Submitted");
  }

  static getSize(metadata: Metadata): number | null {
    return metadata[MetadataEnum.aipSize];
  }

  static getFileNumber(metadata: Metadata): string | null {
    if (isNullOrUndefined(metadata[MetadataEnum.aipFileNumber])) {
      return null;
    }
    return metadata[MetadataEnum.aipFileNumber].toString();
  }

  static getTotalFileNumber(metadata: Metadata): string | null {
    if (isNullOrUndefined(metadata[MetadataEnum.aipTotalFileNumber])) {
      return null;
    }
    return metadata[MetadataEnum.aipTotalFileNumber].toString();
  }

  private static _getDate(metadata: Metadata, name: string): Date | null {
    if (isNullOrUndefined(metadata)) {
      return null;
    }
    const dates: ArchiveMetadataNamespace.DataciteDate = this._getDataciteResource(metadata)?.[MetadataEnum.dataciteDates]?.[MetadataEnum.dataciteDate];
    const result = this._getItemInListBasedOnAttribute(dates, MetadataEnum.dateType, name)?.content;
    if (isNullOrUndefined(result)) {
      return null;
    }
    const date = new Date(result);
    if (date.toString() === "Invalid Date") {
      return null;
    }
    return date;
  }

  private static _getDataciteResource(metadata: Metadata): ArchiveMetadataNamespace.DataciteResource {
    return metadata[MetadataEnum.datacite]?.[MetadataEnum.dataciteResource];
  }

  static getDatacite(metadata: Metadata): string {
    return metadata[MetadataEnum.dataciteXml];
  }
}
