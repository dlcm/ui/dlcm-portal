/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - dlcm-user-preferences.util.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {OrganizationalUnit} from "@models";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {
  CookieConsentUtil,
  CookieType,
  isNullOrUndefined,
  LocalStorageHelper,
  UserPreferencesUtil,
} from "solidify-frontend";

export class DlcmUserPreferencesUtil extends UserPreferencesUtil {
  static getPreferredOrgUnitInDepositMenu(listAuthorizedOrgUnit: OrganizationalUnit[]): OrganizationalUnit | undefined {
    const orgUnitId = LocalStorageHelper.getItem(LocalStorageEnum.orgUnitForDepositMenu);
    if (isNullOrUndefined(listAuthorizedOrgUnit) || listAuthorizedOrgUnit.length === 0) {
      return undefined;
    }
    const defaultOrgUnit = listAuthorizedOrgUnit[0];
    if (isNullOrUndefined(orgUnitId)) {
      return defaultOrgUnit;
    }
    let authorizedOrgUnit = listAuthorizedOrgUnit.find(o => o.resId === orgUnitId);
    if (isNullOrUndefined(authorizedOrgUnit)) {
      authorizedOrgUnit = defaultOrgUnit;
    }
    return authorizedOrgUnit;
  }

  static setPreferredOrgUnitInDepositMenu(orgUnitId: string): string | undefined {
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.orgUnitForDepositMenu)) {
      LocalStorageHelper.setItem(LocalStorageEnum.orgUnitForDepositMenu, orgUnitId);
    }
    if (isNullOrUndefined(orgUnitId)) {
      return undefined;
    }
    return orgUnitId;
  }
}
