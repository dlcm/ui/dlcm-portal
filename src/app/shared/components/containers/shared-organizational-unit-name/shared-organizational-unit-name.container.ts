/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-organizational-unit-name.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {environment} from "@environments/environment";
import {OrganizationalUnit} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {SharedOrganizationalUnitOverlayPresentational} from "@shared/components/presentationals/shared-organizational-unit-overlay/shared-organizational-unit-overlay.presentational";
import {SharedOrgUnitAction} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedOrganizationalUnitState} from "@shared/stores/organizational-unit/shared-organizational-unit.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
  StringUtil,
  Type,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-organizational-unit-name-container",
  templateUrl: "./shared-organizational-unit-name.container.html",
  styleUrls: ["./shared-organizational-unit-name.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedOrganizationalUnitNameContainer extends SharedAbstractContainer {
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;

  organizationalUnitOverlayComponent: Type<SharedOrganizationalUnitOverlayPresentational> = SharedOrganizationalUnitOverlayPresentational;

  organizationalUnit: OrganizationalUnit | undefined;

  get name(): string {
    if (isNotNullNorUndefined(this.organizationalUnit)) {
      return this.organizationalUnit.name;
    }
    return StringUtil.stringEmpty;
  }

  listOrgUnitObs: Observable<OrganizationalUnit[]> = MemoizedUtil.list(this._store, SharedOrganizationalUnitState);
  isLoading: boolean = false;

  _id: string;

  @Input()
  set id(id: string) {
    this._id = id;
    if (isNullOrUndefined(id)) {
      return;
    }
    const listSnapshot = MemoizedUtil.listSnapshot(this._store, SharedOrganizationalUnitState);
    this.organizationalUnit = this._getOrgUnit(listSnapshot);
    if (isNullOrUndefined(this.organizationalUnit)) {
      this.subscribe(this.listOrgUnitObs.pipe(
        distinctUntilChanged(),
        filter(list => isNotNullNorUndefined(this._getOrgUnit(list))),
        take(1),
      ), list => {
        this.organizationalUnit = this._getOrgUnit(list);
        this.isLoading = false;
        this._changeDetector.detectChanges();
      });
      this._store.dispatch(new SharedOrgUnitAction.AddInListById(id, true));
      this.isLoading = true;
    }
  }

  get id(): string {
    return this._id;
  }

  @Input()
  inline: boolean = false;

  @Input()
  displayOverlay: boolean = false;

  constructor(public readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  private _getOrgUnit(list: OrganizationalUnit[]): OrganizationalUnit {
    if (isNullOrUndefined(list)) {
      return undefined;
    }
    const organizationalUnit = list.find(o => o.resId === this._id);
    if (isNullOrUndefined(organizationalUnit)) {
      return undefined;
    }
    return organizationalUnit;
  }
}
