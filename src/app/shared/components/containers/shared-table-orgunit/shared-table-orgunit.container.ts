/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-table-orgunit.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  AbstractControl,
  ControlValueAccessor,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
  Validators,
} from "@angular/forms";
import {OrganizationalUnit} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {sharedOrganizationalUnitActionNameSpace} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedOrganizationalUnitState} from "@shared/stores/organizational-unit/shared-organizational-unit.state";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {tap} from "rxjs/operators";
import {
  BaseResource,
  DataTableColumns,
  DataTableFieldTypeEnum,
  FormValidationHelper,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  ObservableUtil,
  OrderEnum,
  PropertyName,
  ResourceNameSpace,
  SOLIDIFY_CONSTANTS,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-table-orgunit-container",
  templateUrl: "./shared-table-orgunit.container.html",
  styleUrls: ["./shared-table-orgunit.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SharedTableOrgunitContainer,
    },
  ],
})
export class SharedTableOrgunitContainer extends SharedAbstractContainer implements ControlValueAccessor, OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  formArray: FormArray;

  columns: DataTableColumns<BaseResource>[] = [
    {
      field: "name" as any,
      header: LabelTranslateEnum.organizationalUnit,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.descending,
      isFilterable: false,
      isSortable: true,
    },
  ];

  @HostBinding("class.disabled") disabled: boolean = false;

  @Input()
  listOrgUnit: OrganizationalUnit[];

  @Input()
  selectedOrgUnit: OrganizationalUnit[];

  @Input()
  isWithLinkToAdmin: boolean = true;

  sharedOrgUnitSort: Sort = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedOrgUnitActionNameSpace: ResourceNameSpace = sharedOrganizationalUnitActionNameSpace;
  sharedOrgUnitState: typeof SharedOrganizationalUnitState = SharedOrganizationalUnitState;

  @Input()
  formControl: FormControl;

  @Input()
  preventEdition: boolean = false;

  _readonly: boolean;

  @Input()
  set readonly(value: boolean) {
    this._readonly = value;
    this._updateFormReadonlyState();
  }

  get readonly(): boolean {
    return this._readonly;
  }

  private readonly _valueBS: BehaviorSubject<any[] | undefined> = new BehaviorSubject<any[] | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<any[] | undefined> = ObservableUtil.asObservable(this._valueBS);

  protected readonly _navigateBS: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(undefined);
  @Output("navigate")
  readonly navigateObs: Observable<string[]> = ObservableUtil.asObservable(this._navigateBS);

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  getFormControl(formGroup: FormGroup, key: string): FormControl {
    return FormValidationHelper.getFormControl(formGroup, key);
  }

  constructor(private readonly _translate: TranslateService,
              private readonly _store: Store,
              private readonly _fb: FormBuilder,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _elementRef: ElementRef) {
    super();
  }

  writeValue(value: string[]): void {
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState?(isDisabled: boolean): void {
  }

  private _markAsTouched(value: OrgUnitModel[]): void {
    this.propagateChange(value);
    this._valueBS.next(value);
    this.formControl.markAllAsTouched();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.formArray = this._fb.array([]);

    if (!isNullOrUndefined(this.selectedOrgUnit)) {
      this.selectedOrgUnit.forEach(p => {
        this.formArray.push(this.createOrgUnit(p));
      });
    }

    this._updateFormReadonlyState();
    this.formControl.setValue(this.formArray.value);

    const valueBefore: OrgUnitModel[] = this.formArray.value;
    this.subscribe(this.formArray.valueChanges.pipe(
      tap((value: OrgUnitModel[]) => {
        if (valueBefore.length !== value.length) {
          this._markAsTouched(value);
          return;
        }

        let isDiff = false;
        valueBefore.forEach((valBefore, index) => {
          if (isDiff) {
            return;
          }
          if (valBefore.id !== value[index].id) {
            this._markAsTouched(value);
            isDiff = true;
            return;
          }
        });

        if (!isDiff) {
          this.formControl.markAsPristine();
        }
      }),
    ));

    this.subscribe(this.formArray.statusChanges.pipe(
      tap(status => {
        if (status === SOLIDIFY_CONSTANTS.FORM_STATUS_VALID) {
          this.formControl.setErrors(null);
        } else if (status === SOLIDIFY_CONSTANTS.FORM_STATUS_INVALID) {
          this.formControl.setErrors({invalid: true});
        }
      }),
    ));
  }

  private _updateFormReadonlyState(): void {
    if (isNullOrUndefined(this.formArray) || isNullOrUndefined(this.formControl)) {
      return;
    }
    if (this._readonly) {
      this.disabled = true;
      this.formArray.disable();
      this.formControl.disable();
    } else {
      this.disabled = false;
      this.formArray.enable();
      this.formControl.enable();
    }
  }

  createOrgUnit(orgUnit: OrganizationalUnit | undefined = undefined): FormGroup {
    return this._fb.group({
      [this.formDefinition.id]: [isNullOrUndefined(orgUnit) ? "" : orgUnit.resId, [Validators.required]],
    });
  }

  addOrgUnit(tryLater: boolean = true): void {
    if (!this.formControl.disabled) {
      this.formArray.push(this.createOrgUnit());
    } else {
      if (tryLater) {
        setTimeout(() => {
          this.addOrgUnit(false);
          this._changeDetector.detectChanges();
        }, 10);
      }
    }
  }

  delete(index: number): void {
    this.formArray.removeAt(index);
    this._markAsTouched(this.formArray.value);
  }

  propagateChange: (__: any) => void = (__: any) => {};

  goToOrgUnit(orgUnitId: string): void {
    this._navigateBS.next([RoutesEnum.adminOrganizationalUnitDetail, orgUnitId]);
  }

  getMessageNoItem(): string {
    return MARK_AS_TRANSLATABLE("shared.orgUnit.noItem");
  }

  asFormGroup(abstractControl: AbstractControl): FormGroup {
    return abstractControl as FormGroup;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() id: string;
}

export interface OrgUnitModel {
  id: string;
}
