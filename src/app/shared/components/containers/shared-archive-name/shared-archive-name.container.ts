/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-archive-name.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {Archive} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {SharedArchiveAction} from "@shared/stores/archive/shared-archive.action";
import {SharedArchiveState} from "@shared/stores/archive/shared-archive.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
} from "rxjs/operators";
import {
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
  StringUtil,
} from "solidify-frontend";

// TODO Can be merge with shared organizational unit name => generic component that take State, Action and key for name
@Component({
  selector: "dlcm-shared-archive-name-container",
  templateUrl: "./shared-archive-name.container.html",
  styleUrls: ["./shared-archive-name.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedArchiveNameContainer extends SharedAbstractContainer {
  name: string;
  listArchiveObs: Observable<Archive[]> = MemoizedUtil.list(this._store, SharedArchiveState);

  isLoading: boolean = false;

  _id: string;

  @Input()
  set id(id: string) {
    this._id = id;
    if (isNullOrUndefined(id)) {
      this.name = StringUtil.stringEmpty;
      return;
    }
    const listSnapshot = MemoizedUtil.listSnapshot(this._store, SharedArchiveState);
    const archive = this._getArchive(listSnapshot);
    if (isNullOrUndefined(archive)) {
      this.name = StringUtil.stringEmpty;
      this.subscribe(this.listArchiveObs.pipe(
        distinctUntilChanged(),
        filter(list => isNotNullNorUndefined(this._getArchive(list))),
        take(1),
      ), list => {
        this.name = this._getArchive(list)?.title;
        this.isLoading = false;
        this._changeDetector.detectChanges();
      });
      this._store.dispatch(new SharedArchiveAction.AddInListById(id, true));
      this.isLoading = true;
    } else {
      this.name = archive.title;
    }
  }

  get id(): string {
    return this._id;
  }

  constructor(public readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  private _getArchive(list: Archive[]): Archive {
    if (isNullOrUndefined(list)) {
      return undefined;
    }
    const archive = list.find(o => o.resId === this._id);
    if (isNullOrUndefined(archive)) {
      return undefined;
    }
    return archive;
  }
}
