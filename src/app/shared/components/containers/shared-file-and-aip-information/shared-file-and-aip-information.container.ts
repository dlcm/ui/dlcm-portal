/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-file-and-aip-information.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnInit,
  Output,
  Type,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Aip,
  DataFile,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractContainer} from "@shared/components/containers/shared-abstract/shared-abstract.container";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {LocalStateModel} from "@shared/models/local-state.model";
import {StatusHistoryNamespace} from "@shared/stores/status-history/status-history-namespace.model";
import {
  StatusHistoryState,
  StatusHistoryStateModel,
} from "@shared/stores/status-history/status-history.state";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  BaseResource,
  BreakpointService,
  ClipboardUtil,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DateUtil,
  DialogUtil,
  ExtraButtonToolbar,
  FileInput,
  FileVisualizerHelper,
  FileVisualizerService,
  isNullOrUndefined,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ObservableUtil,
  OrderEnum,
  QueryParameters,
  StatusHistoryDialog,
  StatusModel,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-file-and-aip-information-container",
  templateUrl: "./shared-file-and-aip-information.container.html",
  styleUrls: ["./shared-file-and-aip-information.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedFileAndAipInformationContainer<TResource extends BaseResource> extends SharedAbstractContainer implements OnInit {
  columns: DataTableColumns[];
  previewAvailable: boolean;
  visualizationErrorMessage: string;

  private readonly _LOGO_SIP: string = "SIP";
  private readonly _LOGO_AIP: string = "AIP";
  private readonly _LOGO_DIP: string = "DIP";
  private readonly _FILENAME_SIP_PACKAGE: string = "sip-package";
  private readonly _FILENAME_AIP_PACKAGE: string = "aip-package";
  private readonly _FILENAME_DOWNLOAD: string = "download";

  private readonly _fullScreenBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean | undefined>(undefined);
  readonly fullScreenObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._fullScreenBS);

  _data: SharedFileAndAipInformationDialogData<TResource>;

  @Input()
  set data(value: SharedFileAndAipInformationDialogData<TResource>) {
    this._data = value;
    this.previewAvailable = this._getPreviewAvailable();
  }

  get data(): SharedFileAndAipInformationDialogData<TResource> {
    return this._data;
  }

  @Input()
  canEdit: boolean = false;

  @Input()
  canDelete: boolean = false;

  @Input()
  canDownload: boolean = true;

  @Input()
  readyForDownload: boolean = true;

  @Input()
  module: "deposit" | "home" = "deposit";

  get modeEnum(): typeof SharedFileAndAipDetailDialogModeEnum {
    return SharedFileAndAipDetailDialogModeEnum;
  }

  get dateUtil(): typeof DateUtil {
    return DateUtil;
  }

  get enumsDataCategoryAndType(): typeof Enums.DataFile.DataCategoryAndType {
    return Enums.DataFile.DataCategoryAndType;
  }

  get dataCategoryEnum(): typeof Enums.DataFile.DataCategoryAndType.DataCategoryEnum {
    return Enums.DataFile.DataCategoryAndType.DataCategoryEnum;
  }

  get dataTypeEnum(): typeof Enums.DataFile.DataCategoryAndType.DataTypeEnum {
    return Enums.DataFile.DataCategoryAndType.DataTypeEnum;
  }

  get enumsDataFile(): typeof Enums.DataFile {
    return Enums.DataFile;
  }

  get statusEnum(): typeof Enums.DataFile.StatusEnum {
    return Enums.DataFile.StatusEnum;
  }

  get fileDownloadUrl(): string {
    return `${this.backendEndpoint}/${this.data.dataFile.infoPackage.resId}/${ApiResourceNameEnum.DATAFILE}/${this.data.dataFile.resId}/${ApiActionNameEnum.DL}`;
  }

  get backendEndpoint(): string {
    if (this.module === "home") {
      return ApiEnum.accessPublicMetadata;
    }
    return ApiEnum.preIngestDeposits;
  }

  private readonly _locationBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("locationChange")
  readonly locationObs: Observable<void> = ObservableUtil.asObservable(this._locationBS);

  private readonly _dataCategoryTypeBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("dataCategoryTypeChange")
  readonly dataCategoryTypeObs: Observable<void> = ObservableUtil.asObservable(this._dataCategoryTypeBS);

  private readonly _deleteBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("deleteChange")
  readonly deleteObs: Observable<void> = ObservableUtil.asObservable(this._deleteBS);

  isInFullscreenMode: boolean = false;
  zoomEnable: boolean = false;

  smallButtons: ExtraButtonToolbar<TResource>[] = [
    {
      displayCondition: () => this.previewAvailable && this.isInFullscreenMode,
      icon: IconNameEnum.fullScreenLeave,
      callback: () => this.leaveFullScreen(),
      labelToTranslate: () => LabelTranslateEnum.exitFullscreen,
      color: "primary",
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _dialog: MatDialog,
              protected readonly _actions$: Actions,
              protected readonly _breakpointService: BreakpointService,
              protected readonly _notificationService: NotificationService,
              protected readonly _changeDetector: ChangeDetectorRef,
              private readonly _fileVisualizerService: FileVisualizerService,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslateInterface: LabelTranslateInterface,
  ) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.columns = [
      {
        field: "changeTime",
        header: LabelTranslateEnum.changeTime,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isSortable: true,
        isFilterable: false,
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        filterEnum: this.getStatusEnum(),
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        translate: true,
        width: "110px",
        alignment: "center",
      },
      {
        field: "description",
        header: LabelTranslateEnum.description,
        type: DataTableFieldTypeEnum.number,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
      },
      {
        field: "creatorName",
        header: LabelTranslateEnum.createdBy,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        width: "140px",
      },
    ];
    this._computePreviewVariables();
  }

  private _computePreviewVariables(): void {
    this.visualizationErrorMessage = undefined;
    let previewAvailable = this._getPreviewAvailable();
    if (this.canDownload && !previewAvailable) {
      this.visualizationErrorMessage = this._getVisualizationErrorMessage();
    }
    if (!this.readyForDownload) {
      // Do not allow to preview if file is not ready (could be the case in 'home' module)
      previewAvailable = false;
    }
    this.previewAvailable = previewAvailable;
  }

  private _getPreviewAvailable(): boolean {
    if (isNullOrUndefined(this.data) ||
      this.data.mode !== SharedFileAndAipDetailDialogModeEnum.file
      || !this.canDownload
      || isNullOrUndefined(this.data.dataFile?.fileFormat)) {
      return false;
    }
    return FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
      dataFile: DataFileHelper.dataFileAdapter(this.data.dataFile),
      fileExtension: FileVisualizerHelper.getFileExtension(this.data.dataFile.fileName),
    });
  }

  private _getVisualizationErrorMessage(): string | undefined {
    return FileVisualizerHelper.errorMessage(this._fileVisualizerService.listFileVisualizer, {
      dataFile: DataFileHelper.dataFileAdapter(this.data.dataFile),
      fileExtension: FileVisualizerHelper.getFileExtension(this.data.dataFile.fileName),
    }, this._labelTranslateInterface);
  }

  enterFullScreen(): void {
    this.isInFullscreenMode = true;
    this.zoomEnable = true;
    this._changeDetector.detectChanges();
  }

  leaveFullScreen(): void {
    this.isInFullscreenMode = false;
    this.zoomEnable = false;
    this._changeDetector.detectChanges();
  }

  get breakpointService(): BreakpointService {
    return this._breakpointService;
  }

  getFileInput(): FileInput {
    return {
      dataFile: DataFileHelper.dataFileAdapter(this.data.dataFile),
    };
  }

  getResId(): string | undefined {
    if (this.data.mode === SharedFileAndAipDetailDialogModeEnum.file) {
      return this.data.dataFile.resId;
    }
    if (this.data.mode === SharedFileAndAipDetailDialogModeEnum.aip) {
      return this.data.aip.resId;
    }
    return undefined;
  }

  getData(): DataFile | Aip | undefined {
    if (this.data.mode === SharedFileAndAipDetailDialogModeEnum.file) {
      return this.data.dataFile;
    }
    if (this.data.mode === SharedFileAndAipDetailDialogModeEnum.aip) {
      return this.data.aip;
    }
    return undefined;
  }

  getDataFileName(): string {
    if (this.data.mode === SharedFileAndAipDetailDialogModeEnum.file) {
      return this.data.dataFile.fileName;
    }
    if (this.data.mode === SharedFileAndAipDetailDialogModeEnum.aip) {
      return this.data.aip.info.name;
    }
  }

  getComplianceLevel(): Enums.ComplianceLevel.ComplianceLevelEnum {
    if (this.data.mode === SharedFileAndAipDetailDialogModeEnum.file) {
      return this.data.dataFile.complianceLevel;
    }
    if (this.data.mode === SharedFileAndAipDetailDialogModeEnum.aip) {
      return this.data.aip.info.complianceLevel;
    }
  }

  getStatusEnum(): StatusModel[] {
    if (this.data?.mode === SharedFileAndAipDetailDialogModeEnum.file) {
      return Enums.DataFile.StatusEnumTranslate;
    }
    if (this.data?.mode === SharedFileAndAipDetailDialogModeEnum.aip) {
      return Enums.Package.StatusEnumTranslate;
    }
    return [];
  }

  showHistory(): void {
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: this.data.parentId,
      resourceResId: this.getResId(),
      name: this.getDataFileName(),
      statusHistory: MemoizedUtil.select(this._store, this.data.statusHistoryState, state => state.history),
      isLoading: MemoizedUtil.isLoading(this._store, this.data.statusHistoryState),
      queryParametersObs: MemoizedUtil.select(this._store, this.data.statusHistoryState, state => state.queryParameters),
      state: this.data.statusHistoryNamespace,
      statusEnums: this.getStatusEnum(),
    }, {
      width: environment.modalWidth,
    });
  }

  getPuidLink(puid: string): string {
    return environment.urlNationalArchivePronom + puid;
  }

  copyKey(): void {
    ClipboardUtil.copyStringToClipboard(this.getResId());
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("app.notification.idCopyToClipboard"));
  }

  pagination($event: QueryParameters): void {
    this._store.dispatch(new this.data.statusHistoryNamespace.ChangeQueryParameters($event, this.getResId(), this.data.parentId));
  }

  backToListNavigate(): Navigate {
    const currentUrl = this._store.selectSnapshot((s: LocalStateModel) => s.router.state.url);
    const previousUrl = currentUrl.substring(0, currentUrl.lastIndexOf(urlSeparator));
    return new Navigate([previousUrl]);
  }

  editLocation(): void {
    this._locationBS.next();
  }

  editCategory(): void {
    this._dataCategoryTypeBS.next();
  }

  delete(): void {
    this._deleteBS.next();
  }

  changeFullScreen($event: boolean): void {
    this._fullScreenBS.next($event);
  }

  getLogoPackage(): string {
    const mode = this.data.mode;
    switch (mode) {
      case SharedFileAndAipDetailDialogModeEnum.aip:
        return this._LOGO_AIP;
      case SharedFileAndAipDetailDialogModeEnum.file:
        const fileName = (this.getData() as DataFile)?.fileName;
        if (fileName === this._FILENAME_SIP_PACKAGE) {
          return this._LOGO_SIP;
        }
        if (fileName === this._FILENAME_AIP_PACKAGE || fileName === this._FILENAME_DOWNLOAD) {
          return this._LOGO_AIP;
        }
        return this._LOGO_DIP;
      default:
        return "?";
    }
  }

  asDataFile(data: DataFile | Aip | undefined): DataFile {
    return data as DataFile;
  }

  asAip(data: DataFile | Aip | undefined): Aip {
    return data as Aip;
  }

  triggerPreview(): void {
    this.readyForDownload = true;
    this._computePreviewVariables();
  }
}

export interface SharedFileAndAipInformationDialogData<TResource extends BaseResource> {
  parentId: string;
  mode: SharedFileAndAipDetailDialogModeEnum;
  dataFile?: DataFile;
  aip?: Aip;
  buttons: ExtraButtonToolbar<TResource>[];
  statusHistoryState?: Type<StatusHistoryState<StatusHistoryStateModel<TResource>, TResource>>;
  statusHistoryNamespace?: StatusHistoryNamespace;
}

export enum SharedFileAndAipDetailDialogModeEnum {
  file = "FILE",
  aip = "AIP",
}
