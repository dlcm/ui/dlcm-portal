/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-admin-subject-area-label-presentational.component.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {
  Label,
  SubjectArea,
} from "@models";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {isNullOrUndefined} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-admin-subject-area-label",
  templateUrl: "./shared-admin-subject-area-label-presentational.component.html",
  styleUrls: ["./shared-admin-subject-area-label-presentational.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class SharedAdminSubjectAreaLabelPresentational extends SharedAbstractPresentational {
  private _subjectArea: SubjectArea;

  private _language: string;

  @Input()
  set subjectArea(value: SubjectArea) {
    this._subjectArea = value;
    this._computeLabel();
  }

  get subjectArea(): SubjectArea {
    return this._subjectArea;
  }

  @Input()
  set language(value: string) {
    this._language = value;
    this._computeLabel();
  }

  get language(): string {
    return this._language;
  }

  value: Label | undefined;

  private _computeLabel(): void {
    if (isNullOrUndefined(this.language) || isNullOrUndefined(this.subjectArea)) {
      return;
    }
    this.value = this.subjectArea.labels.find(label => label.language.resId === this.language);
  }

  constructor(public readonly _fb: FormBuilder) {
    super();
  }
}
