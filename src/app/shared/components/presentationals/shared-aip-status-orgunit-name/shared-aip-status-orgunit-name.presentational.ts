/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-status-orgunit-name.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {AipCopyList} from "@models";
import {
  isEmptyArray,
  isNullOrUndefined,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-aip-status-orgunit-name",
  templateUrl: "./shared-aip-status-orgunit-name.presentational.html",
  styleUrls: ["./shared-aip-status-orgunit-name.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAipStatusOrgunitNamePresentational extends SharedAbstractPresentational {
  private _aipCopyList: AipCopyList;

  @Input()
  set aipCopyList(value: AipCopyList) {
    this._aipCopyList = value;
    this._computeOrgUnitId();
  }

  get aipCopyList(): AipCopyList {
    return this._aipCopyList;
  }

  orgUnitId: string | undefined;

  private _computeOrgUnitId(): void {
    if (isNullOrUndefined(this.aipCopyList) || isEmptyArray(this.aipCopyList)) {
      this.orgUnitId = undefined;
      return;
    }

    let copyWithDataProvided = undefined;
    this.aipCopyList.copies.some(copy => {
      if (!isNullOrUndefined(copy.aip)) {
        copyWithDataProvided = copy.aip;
        return true;
      }
    });

    if (isNullOrUndefined(copyWithDataProvided)) {
      this.orgUnitId = undefined;
      return;
    }
    this.orgUnitId = copyWithDataProvided.info.organizationalUnitId;
  }
}
