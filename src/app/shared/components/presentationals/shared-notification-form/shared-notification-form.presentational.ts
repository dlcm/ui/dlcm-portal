/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-notification-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {
  DataFile,
  NotificationDlcm,
  Role,
} from "@models";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {Storage} from "@shared/models/storage.model";
import {SecurityService} from "@shared/services/security.service";
import {sharedDepositActionNameSpace} from "@shared/stores/deposit/shared-deposit.action";
import {SharedDepositState} from "@shared/stores/deposit/shared-deposit.state";
import {SharedNotificationAction} from "@shared/stores/notification/shared-notification.action";
import {sharedOrganizationalUnitActionNameSpace} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedOrganizationalUnitState} from "@shared/stores/organizational-unit/shared-organizational-unit.state";
import {sharedPersonActionNameSpace} from "@shared/stores/person/shared-person.action";
import {SharedPersonState} from "@shared/stores/person/shared-person.state";
import {
  AbstractFormPresentational,
  BreakpointService,
  DateUtil,
  EnumUtil,
  KeyValue,
  PropertyName,
  ResourceFileNameSpace,
  ResourceNameSpace,
  SolidifyDataFileModel,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-notification-form",
  templateUrl: "./shared-notification-form.presentational.html",
  styleUrls: ["./shared-notification-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedNotificationFormPresentational extends AbstractFormPresentational<NotificationDlcm> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  listNotificationCategory: KeyValue[] = Enums.Notification.CategoryEnumTranslate;
  listNotificationType: KeyValue[] = Enums.Notification.TypeEnumTranslate;
  listNotificationStatusTranslate: KeyValue[] = Enums.Notification.StatusEnumTranslate;

  get notificationTypeEnum(): typeof Enums.Notification.TypeEnum {
    return Enums.Notification.TypeEnum;
  }

  get notificationCategoryEnum(): typeof Enums.Notification.CategoryEnum {
    return Enums.Notification.CategoryEnum;
  }

  get defaultAipStorage(): Storage {
    return ApiEnum.archivalStorageDefault;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  @Input()
  listRole: Role[];

  sharedPersonActionNameSpace: ResourceNameSpace = sharedPersonActionNameSpace;
  sharedPersonState: typeof SharedPersonState = SharedPersonState;

  sharedOrgUnitActionNameSpace: ResourceNameSpace = sharedOrganizationalUnitActionNameSpace;
  sharedOrganizationalUnitState: typeof SharedOrganizationalUnitState = SharedOrganizationalUnitState;

  sharedDepositActionNameSpace: ResourceNameSpace = sharedDepositActionNameSpace;
  sharedDepositState: typeof SharedDepositState = SharedDepositState;

  sharedNotificationActionNameSpace: ResourceFileNameSpace = SharedNotificationAction;

  dataFileAdapter: (dataFile: DataFile) => SolidifyDataFileModel = DataFileHelper.dataFileAdapter;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly securityService: SecurityService,
              public readonly breakpointService: BreakpointService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _bindFormTo(notificationDlcm: NotificationDlcm): void {
    this.form = this._fb.group({
      [this.formDefinition.notificationCategory]: [notificationDlcm.notificationType?.notificationCategory, [Validators.required, SolidifyValidator]],
      [this.formDefinition.notificationType]: [notificationDlcm.notificationType?.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.notificationStatus]: [notificationDlcm.notificationStatus, [Validators.required, SolidifyValidator]],
      [this.formDefinition.emitterId]: [notificationDlcm.emitter?.person?.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.emitterEmail]: [notificationDlcm.emitter?.email, [SolidifyValidator]],
      [this.formDefinition.message]: [notificationDlcm.message, [Validators.required, SolidifyValidator]],
      [this.formDefinition.responseMessage]: [notificationDlcm.responseMessage, [SolidifyValidator]],
      [this.formDefinition.objectId]: [notificationDlcm.objectId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.creation]: [DateUtil.convertDateToDateTimeString(new Date(notificationDlcm.creation.when)), [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastUpdate]: [DateUtil.convertDateToDateTimeString(new Date(notificationDlcm.lastUpdate.when)), [Validators.required, SolidifyValidator]],
      [this.formDefinition.notifiedOrgUnitId]: [notificationDlcm.notifiedOrgUnit?.resId, [Validators.required, SolidifyValidator]],
    });
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.notificationCategory]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.notificationType]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.notificationStatus]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.emitterId]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.emitterEmail]: ["", [SolidifyValidator]],
      [this.formDefinition.message]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.responseMessage]: ["", [SolidifyValidator]],
      [this.formDefinition.objectId]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.creation]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastUpdate]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.notifiedOrgUnitId]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(notificationDlcm: NotificationDlcm): NotificationDlcm {
    delete notificationDlcm[this.formDefinition.emitterEmail];
    return notificationDlcm;
  }

  protected _disableSpecificField(): void {
    this.form.get(this.formDefinition.emitterEmail).disable();
  }

  navigateToOrgUnit(orgUnitId: string): void {
    this.navigate([RoutesEnum.preservationSpaceOrganizationalUnitDetail, orgUnitId]);
  }

  navigateToDeposit(depositId: string): void {
    this.navigate([RoutesEnum.deposit + urlSeparator + this.model.notifiedOrgUnit.resId + urlSeparator + DepositRoutesEnum.detail + urlSeparator + depositId]);
  }

  navigateToPerson(personId: string): void {
    this.navigate([RoutesEnum.adminPersonDetail, personId]);
  }

  goToAip(): void {
    const aipId: string = this.model.objectId;
    this._navigateBS.next([AppRoutesEnum.preservationPlanning, PreservationPlanningRoutesEnum.aip, String(this.defaultAipStorage.index), SharedAipRoutesEnum.aipDetail, aipId]);
  }

  goToAipDownloaded(): void {
    const aipId: string = this.model.objectId;
    this._navigateBS.next([RoutesEnum.preservationPlanningAipDownloadedDetail, aipId]);
  }

  goToSip(): void {
    const sipId: string = this.model.objectId;
    this._navigateBS.next([RoutesEnum.preservationPlanningSipDetail, sipId]);
  }

  goToDip(): void {
    const dipId: string = this.model.objectId;
    this._navigateBS.next([RoutesEnum.preservationPlanningDipDetail, dipId]);
  }

  goToAipSteward(): void {
    const aipId: string = this.model.objectId;
    this._navigateBS.next([RoutesEnum.preservationSpaceAipStewardDetail, aipId]);
  }

  goToArchive(): void {
    const archiveId: string = this.model.objectId;
    this._navigateBS.next([RoutesEnum.archives, archiveId]);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() notificationCategory: string;
  @PropertyName() notificationType: string;
  @PropertyName() notificationStatus: string;
  @PropertyName() emitterId: string;
  @PropertyName() emitterEmail: string;
  @PropertyName() message: string;
  @PropertyName() responseMessage: string;
  @PropertyName() objectId: string;
  @PropertyName() creation: string;
  @PropertyName() lastUpdate: string;
  @PropertyName() notifiedOrgUnitId: string;
}
