/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-data-sensitivity-input.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {KeyValueOption} from "@shared/models/key-value-option.model";
import {tap} from "rxjs/operators";

@Component({
  selector: "dlcm-shared-data-sensitivity-input",
  templateUrl: "./shared-data-sensitivity-input.presentational.html",
  styleUrls: ["./shared-data-sensitivity-input.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SharedDataSensitivityInputPresentational,
    },
  ],
})
export class SharedDataSensitivityInputPresentational extends SharedAbstractPresentational implements OnInit, ControlValueAccessor {
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;

  @Input()
  formControl: FormControl;

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this.formControl.statusChanges.pipe(
      tap(status => {
        this._changeDetector.detectChanges();
      }),
    ));
  }

  @Input()
  dataSensitivityEnumValues: KeyValueOption[] = Enums.DataSensitivity.DataSensitivityEnumTranslate;

  get dataSensitivityEnum(): typeof Enums.DataSensitivity.DataSensitivityEnum {
    return Enums.DataSensitivity.DataSensitivityEnum;
  }

  constructor(private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  propagateChange: (__: any) => void = (__: any) => {};

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
  }
}
