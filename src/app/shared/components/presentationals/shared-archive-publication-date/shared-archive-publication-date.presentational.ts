/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-archive-publication-date.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {Archive} from "@models";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {MetadataUtil} from "@shared/utils/metadata.util";
import {
  DateUtil,
  isNullOrUndefined,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-archive-publication-date",
  templateUrl: "./shared-archive-publication-date.presentational.html",
  styleUrls: ["./shared-archive-publication-date.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedArchivePublicationDatePresentational extends SharedAbstractPresentational {
  _archive: Archive;
  publicationDate: string;

  @Input()
  set archive(archive: Archive) {
    this._archive = archive;
    this.publicationDate = this._computePublicationDate();
  }

  constructor(private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  private _computePublicationDate(): string {
    if (isNullOrUndefined(this._archive?.archiveMetadata?.metadata)) {
      return StringUtil.stringEmpty;
    }
    return DateUtil.convertDateToDateString(MetadataUtil.getIssuedDate(this._archive.archiveMetadata.metadata));
  }
}
