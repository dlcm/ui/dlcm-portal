/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-aip-status-summary.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from "@angular/core";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Enums} from "@enums";
import {
  AipCopy,
  AipCopyList,
} from "@models";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
  SharedAipRoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {
  EnumUtil,
  isNotNullNorUndefined,
  isNull,
  isNullOrUndefined,
  isUndefined,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-aip-status-summary",
  templateUrl: "./shared-aip-status-summary.presentational.html",
  styleUrls: ["./shared-aip-status-summary.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedAipStatusSummaryPresentational extends SharedAbstractPresentational implements OnInit {
  private _aipCopyList: AipCopyList;

  @Input()
  set aipCopyList(value: AipCopyList) {
    this._aipCopyList = value;
    this.aipCopySorted = [];
    ApiEnum.archivalStorageList.forEach(storagion => {
      this.aipCopyList.copies.forEach(copy => {
        if (copy.storageUrl === storagion.url) {
          this.aipCopySorted.push(copy);
        }
      });
    });
  }

  get aipCopyList(): AipCopyList {
    return this._aipCopyList;
  }

  aipCopySorted: AipCopy[] = [];

  private readonly _PATH_TO_AIP_STORAGION: string = urlSeparator + AppRoutesEnum.preservationPlanning + urlSeparator + PreservationPlanningRoutesEnum.aip + urlSeparator;

  get packageStatusEnum(): typeof Enums.Package.StatusEnum {
    return Enums.Package.StatusEnum;
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  getStatusLabel(status: Enums.Package.StatusEnum): string {
    if (isNull(status)) {
      return MARK_AS_TRANSLATABLE("enum.package.status.copyMissing");
    }
    if (isUndefined(status)) {
      return MARK_AS_TRANSLATABLE("enum.package.status.nonUpdatedCopy");
    }
    return EnumUtil.getLabel(Enums.Package.StatusEnumTranslate, status);
  }

  getStoragionNumber(aipCopy: AipCopy): number {
    const storagion = ApiEnum.archivalStorageList.find(sto => aipCopy.storageUrl === sto.url).index;
    if (isNotNullNorUndefined(storagion)) {
      return storagion;
    }
    return 1;
  }

  getUrl(aipCopy: AipCopy): string | undefined {
    if (isNullOrUndefined(aipCopy.status)) {
      return undefined;
    }

    return this._PATH_TO_AIP_STORAGION + this.getStoragionNumber(aipCopy) + urlSeparator + SharedAipRoutesEnum.aipDetail + urlSeparator + aipCopy.aip.resId;
  }
}
