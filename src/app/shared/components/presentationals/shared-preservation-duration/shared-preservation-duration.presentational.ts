/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-preservation-duration.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  OnInit,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormBuilder,
  FormControl,
  FormGroup,
  NG_VALUE_ACCESSOR,
  Validators,
} from "@angular/forms";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Enums} from "@enums";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  FormValidationHelper,
  isNotNullNorUndefined,
  isNullOrUndefined,
  KeyValue,
  PropertyName,
  SOLIDIFY_CONSTANTS,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-preservation-duration",
  templateUrl: "./shared-preservation-duration.presentational.html",
  styleUrls: ["./shared-preservation-duration.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SharedPreservationDurationPresentational,
    },
  ],
})
export class SharedPreservationDurationPresentational extends SharedAbstractPresentational implements OnInit, ControlValueAccessor {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  listUnits: KeyValue[] = Enums.RetentionPolicy.RetentionPolicyEnumTranslate;

  private readonly _DAYS_BY_YEAR: number = 365;
  private readonly _YEARS_BY_CENTURY: number = 100;

  @HostBinding("class.readonly")
  protected _readonly: boolean;

  @Input()
  set readonly(value: boolean) {
    this._readonly = value;
    this._updateFormReadonly();
  }

  get readonly(): boolean {
    return this._readonly;
  }

  @Input()
  formControl: FormControl;

  form: FormGroup;

  propagateChange: (__: any) => void = (__: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
  }

  constructor(private readonly _changeDetectorRef: ChangeDetectorRef,
              private readonly _fb: FormBuilder) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    const value = this.formControl.value;
    if (isNullOrUndefined(value)) {
      this.form = this._fb.group({
        [this.formDefinition.retention]: [value, [Validators.required, SolidifyValidator]],
        [this.formDefinition.retentionUnits]: [Enums.RetentionPolicy.RetentionPolicyEnum.days, [Validators.required, SolidifyValidator]],
      });
    } else {
      this.form = this._fb.group({
        [this.formDefinition.retention]: [this._adaptRetentionWithUnits(value), [Validators.required, SolidifyValidator]],
        [this.formDefinition.retentionUnits]: [this._getUnitsFromRetention(value), [Validators.required, SolidifyValidator]],
      });
    }
    this._addValidators();
    this._updateFormReadonly();

    this.subscribe(this.form.get(this.formDefinition.retention).valueChanges, v => {
      this.formControl.setValue(this.calculateRetention());
      this.formControl.markAsDirty();
    });

    this.subscribe(this.form.get(this.formDefinition.retention).statusChanges, status => {
      const v = this.form.get(this.formDefinition.retention).value;
      if (status === SOLIDIFY_CONSTANTS.FORM_STATUS_VALID && isNotNullNorUndefined(v)) {
        this.formControl.setErrors(null);
      } else {
        this.formControl.setErrors({invalid: true});
      }
    });
  }

  get retentionPolicyEnum(): typeof Enums.RetentionPolicy.RetentionPolicyEnum {
    return Enums.RetentionPolicy.RetentionPolicyEnum;
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  isRequired(key: string): boolean {
    const errors = this.getFormControl(key).errors;
    return isNullOrUndefined(errors) ? false : errors.required;
  }

  calculateRetention(): number {
    const retention: number = this.form.get(this.formDefinition.retention).value;
    const units: string = this.form.get(this.formDefinition.retentionUnits).value;
    switch (units) {
      case Enums.RetentionPolicy.RetentionPolicyEnum.forever:
        return 0;
      case Enums.RetentionPolicy.RetentionPolicyEnum.days:
        return retention;
      case Enums.RetentionPolicy.RetentionPolicyEnum.years:
        return retention * this._DAYS_BY_YEAR;
      case Enums.RetentionPolicy.RetentionPolicyEnum.centuries:
        return retention * this._DAYS_BY_YEAR * this._YEARS_BY_CENTURY;
      default:
        return;
    }
  }

  private _getUnitsFromRetention(retention: number): string {
    let unit: string;
    if (retention === 0) {
      unit = Enums.RetentionPolicy.RetentionPolicyEnum.forever;
    } else if (retention < this._DAYS_BY_YEAR) {
      unit = Enums.RetentionPolicy.RetentionPolicyEnum.days;
    } else if (retention < (this._DAYS_BY_YEAR * this._YEARS_BY_CENTURY)) {
      unit = Enums.RetentionPolicy.RetentionPolicyEnum.years;
    } else {
      unit = Enums.RetentionPolicy.RetentionPolicyEnum.centuries;
    }
    return unit;
  }

  private _adaptRetentionWithUnits(retention: number): number {
    let newRetention: number;
    if (retention === 0) {
      newRetention = 0;
    } else if (retention < this._DAYS_BY_YEAR) {
      return retention;
    } else if (retention >= this._DAYS_BY_YEAR && retention < (this._DAYS_BY_YEAR * this._YEARS_BY_CENTURY)) {
      newRetention = retention / this._DAYS_BY_YEAR;
    } else {
      newRetention = retention / (this._DAYS_BY_YEAR * this._YEARS_BY_CENTURY);
    }
    return newRetention;
  }

  private _addValidators(): void {
    const retentionUnitsFormControl = this.form.get(this.formDefinition.retentionUnits);
    this.subscribe(retentionUnitsFormControl.valueChanges.pipe(
      distinctUntilChanged(),
      tap(value => {
        this._setValidator(value);
      }),
    ));
    this._setValidator(retentionUnitsFormControl.value);
  }

  private _setValidator(value: Enums.RetentionPolicy.RetentionPolicyEnum): void {
    const retentionFormControl = this.form.get(this.formDefinition.retention);
    if (value === Enums.RetentionPolicy.RetentionPolicyEnum.days) {
      retentionFormControl.setValidators([Validators.min(1), Validators.max(this._DAYS_BY_YEAR), Validators.required]);
    } else if (value === Enums.RetentionPolicy.RetentionPolicyEnum.years) {
      retentionFormControl.setValidators([Validators.min(1), Validators.max(this._YEARS_BY_CENTURY), Validators.required]);
    } else if (value === Enums.RetentionPolicy.RetentionPolicyEnum.centuries) {
      retentionFormControl.setValidators([Validators.min(1), Validators.required]);
    } else {
      retentionFormControl.setValue(0);
      retentionFormControl.setValidators([]);
    }
    retentionFormControl.updateValueAndValidity();
    this._changeDetectorRef.detectChanges();
  }

  private _updateFormReadonly(): void {
    if (isNullOrUndefined(this.form)) {
      return;
    }
    if (this.readonly) {
      this.form.disable();
    } else {
      this.form.enable();
    }
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() retention: string;
  @PropertyName() retentionUnits: string;
}
