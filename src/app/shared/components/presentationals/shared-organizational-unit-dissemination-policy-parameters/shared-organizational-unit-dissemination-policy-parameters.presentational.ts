/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-organizational-unit-dissemination-policy-parameters.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {DLCM_CONSTANTS} from "@app/constants";
import {
  DisseminationPolicy,
  OrganizationalUnitDisseminationPolicyContainer,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  SharedOrganizationalUnitDisseminationPolicyParametersDialog,
  SharedOrganizationalUnitDisseminationPolicyParametersDialogData,
} from "@shared/components/dialogs/shared-organizational-unit-dissemination-policy-parameters/shared-organizational-unit-dissemination-policy-parameters.dialog";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {SharedOrganizationalUnitDisseminationPolicyOverlayPresentational} from "@shared/components/presentationals/shared-organizational-unit-dissemination-policy-overlay/shared-organizational-unit-dissemination-policy-overlay.presentational";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {OrganizationalUnitDisseminationPolicyHelper} from "@shared/helpers/organizational-unit-dissemination-policy.helper";
import {SharedDisseminationPolicyAction} from "@shared/stores/dissemination-policy/shared-dissemination-policy.action";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {tap} from "rxjs/operators";
import {
  DialogUtil,
  isNotNullNorUndefined,
  ObservableUtil,
  StoreUtil,
  Type,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-organizational-unit-dissemination-policy-parameters",
  templateUrl: "./shared-organizational-unit-dissemination-policy-parameters.presentational.html",
  styleUrls: ["./shared-organizational-unit-dissemination-policy-parameters.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SharedOrganizationalUnitDisseminationPolicyParametersPresentational),
      multi: true,
    },
  ],
})
export class SharedOrganizationalUnitDisseminationPolicyParametersPresentational extends SharedAbstractPresentational implements OnInit {
  private _onChange: (value: string) => void;
  private _onTouched: () => void;

  @Input()
  selectedDisseminationPolicies: OrganizationalUnitDisseminationPolicyContainer[];

  @Input()
  formControl: FormControl;

  protected readonly _navigateBS: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(undefined);
  @Output("navigate")
  readonly navigateObs: Observable<string[]> = ObservableUtil.asObservable(this._navigateBS);

  listOrganizationalUnitDisseminationPolicyContainer: OrganizationalUnitDisseminationPolicyContainer[];

  organizationalUnitDisseminationPolicyOverlayComponent: Type<SharedOrganizationalUnitDisseminationPolicyOverlayPresentational> = SharedOrganizationalUnitDisseminationPolicyOverlayPresentational;

  oaisDisseminationPolicy: DisseminationPolicy;
  basicDisseminationPolicy: DisseminationPolicy;

  constructor(protected readonly _dialog: MatDialog,
              protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _translateService: TranslateService,
  ) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new SharedDisseminationPolicyAction.GetById(DLCM_CONSTANTS.DISSEMINATION_POLICY_BASIC_ID),
        SharedDisseminationPolicyAction.GetByIdSuccess,
        result => {
          this.basicDisseminationPolicy = result.model;
          this._changeDetectorRef.detectChanges();
        },
        SharedDisseminationPolicyAction.GetByIdFail,
        result => {
          this._changeDetectorRef.detectChanges();
        },
      ),
    );

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new SharedDisseminationPolicyAction.GetById(DLCM_CONSTANTS.DISSEMINATION_POLICY_OAIS_ID),
        SharedDisseminationPolicyAction.GetByIdSuccess,
        result => {
          this.oaisDisseminationPolicy = result.model;
          this._changeDetectorRef.detectChanges();
        },
        SharedDisseminationPolicyAction.GetByIdFail,
        result => {
          this._changeDetectorRef.detectChanges();
        },
      ),
    );

    this.listOrganizationalUnitDisseminationPolicyContainer = [...OrganizationalUnitDisseminationPolicyHelper.fillComputedNameInList(this._translateService, this.selectedDisseminationPolicies) ?? []];
    this.subscribe(this.formControl.parent.statusChanges.pipe(
      tap(() => this._changeDetectorRef.detectChanges()),
    ));
  }

  editDisseminationPoliciesParameters(index: number): void {
    if (this.formControl.disabled) {
      return;
    }
    const model = this.listOrganizationalUnitDisseminationPolicyContainer[index];
    const listOther = [...this.listOrganizationalUnitDisseminationPolicyContainer];
    listOther.splice(index, 1);
    this.subscribe(DialogUtil.open(this._dialog, SharedOrganizationalUnitDisseminationPolicyParametersDialog, {
      organizationalUnitDisseminationPolicyContainer: model,
      listOtherOrganizationalUnitDisseminationPolicyContainer: listOther,
    } as SharedOrganizationalUnitDisseminationPolicyParametersDialogData, {
      minWidth: "500px",
    }, result => {
      this.listOrganizationalUnitDisseminationPolicyContainer = [...this.listOrganizationalUnitDisseminationPolicyContainer];
      this.listOrganizationalUnitDisseminationPolicyContainer[index] = {
        ...this.listOrganizationalUnitDisseminationPolicyContainer[index],
        computedName: result.computedName,
        joinResource: {
          ...result.joinResource,
          compositeKey: this.listOrganizationalUnitDisseminationPolicyContainer[index].joinResource.compositeKey,
        },
      };
      this._computeDisseminationPoliciesValue();
      this._changeDetectorRef.detectChanges();
    }));
  }

  createDisseminationPoliciesParameters(): void {
    this.subscribe(DialogUtil.open(this._dialog, SharedOrganizationalUnitDisseminationPolicyParametersDialog, {
      listOtherOrganizationalUnitDisseminationPolicyContainer: this.listOrganizationalUnitDisseminationPolicyContainer,
    } as SharedOrganizationalUnitDisseminationPolicyParametersDialogData, {
      minWidth: "500px",
    }, result => {
      this.listOrganizationalUnitDisseminationPolicyContainer = [...this.listOrganizationalUnitDisseminationPolicyContainer, result];
      this._computeDisseminationPoliciesValue();
      this._changeDetectorRef.detectChanges();
    }));
  }

  removeDisseminationPoliciesParameters(index: number): void {
    this.listOrganizationalUnitDisseminationPolicyContainer = [...this.listOrganizationalUnitDisseminationPolicyContainer];
    this.listOrganizationalUnitDisseminationPolicyContainer.splice(index, 1);
    this._computeDisseminationPoliciesValue();
  }

  trackByFn(index: number, organizationalUnitDisseminationPolicyContainer: OrganizationalUnitDisseminationPolicyContainer): string {
    return organizationalUnitDisseminationPolicyContainer.resId + "_" + organizationalUnitDisseminationPolicyContainer.joinResource.parameters;
  }

  private _computeDisseminationPoliciesValue(): void {
    this.formControl.setValue(this.listOrganizationalUnitDisseminationPolicyContainer.map(d => d.joinResource.compositeKey));
    this.formControl.markAsDirty();
  }

  navigateToDisseminationPolicy($event: Event, disseminationPolicy: DisseminationPolicy): void {
    if (isNotNullNorUndefined($event)) {
      $event.stopPropagation();
      $event.preventDefault();
    }
    this._navigateBS.next([RoutesEnum.adminDisseminationPolicyDetail, disseminationPolicy.resId]);
  }

  public writeValue(value: string): void {
  }

  public registerOnChange(fn: (value: string) => void): void {
    this._onChange = fn;
  }

  public registerOnTouched(fn: () => void): void {
    this._onTouched = fn;
  }

  public setDisabledState?(isDisabled: boolean): void {
    // Implement if needed
  }

}
