/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-organizational-unit-overlay.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
} from "@angular/core";
import {OrganizationalUnit} from "@models";
import {
  overlayAnimation,
  SharedResourceLogoOverlayPresentational,
} from "@shared/components/presentationals/shared-resource-logo-overlay/shared-resource-logo-overlay.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {SharedOrgUnitAction} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedOrganizationalUnitState} from "@shared/stores/organizational-unit/shared-organizational-unit.state";
import {
  isNotNullNorUndefined,
  ResourceFileNameSpace,
  ResourceFileState,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-organizational-unit-overlay",
  templateUrl: "./shared-organizational-unit-overlay.presentational.html",
  styleUrls: ["./shared-organizational-unit-overlay.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [overlayAnimation],
})
export class SharedOrganizationalUnitOverlayPresentational extends SharedResourceLogoOverlayPresentational<OrganizationalUnit> {
  logoActionNameSpace: ResourceFileNameSpace = SharedOrgUnitAction;
  logoState: typeof ResourceFileState = SharedOrganizationalUnitState as any;

  get isLogoPresent(): boolean {
    return isNotNullNorUndefined(this.data?.logo);
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }
}
