/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-organizational-unit-dissemination-policy-overlay.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
} from "@angular/core";
import {DLCM_CONSTANTS} from "@app/constants";
import {Enums} from "@enums";
import {OrganizationalUnitDisseminationPolicyContainer} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {overlayAnimation} from "@shared/components/presentationals/shared-resource-logo-overlay/shared-resource-logo-overlay.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {OrganizationalUnitDisseminationPolicyHelper} from "@shared/helpers/organizational-unit-dissemination-policy.helper";
import {
  AbstractOverlayPresentational,
  EnumUtil,
  isNotNullNorUndefinedNorWhiteString,
  MappingObject,
  MappingObjectUtil,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-organizational-unit-dissemination-policy-overlay-overlay",
  templateUrl: "./shared-organizational-unit-dissemination-policy-overlay.presentational.html",
  styleUrls: ["./shared-organizational-unit-dissemination-policy-overlay.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [overlayAnimation],
})
export class SharedOrganizationalUnitDisseminationPolicyOverlayPresentational extends AbstractOverlayPresentational<OrganizationalUnitDisseminationPolicyContainer> {
  parametersMap: MappingObject<string, string>;
  typeToTranslate: string;

  get mappingObjectUtil(): typeof MappingObjectUtil {
    return MappingObjectUtil;
  }

  get stringUtil(): typeof StringUtil {
    return StringUtil;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  constructor(protected readonly _elementRef: ElementRef,
              private readonly _translateService: TranslateService) {
    super(_elementRef);
  }

  protected override _postUpdateData(): void {
    this._data = OrganizationalUnitDisseminationPolicyHelper.fillComputedName(this._translateService, this.data);
    if (isNotNullNorUndefinedNorWhiteString(this.data?.joinResource?.parameters)) {
      this.parametersMap = JSON.parse(this.data.joinResource.parameters);
      MappingObjectUtil.delete(this.parametersMap, DLCM_CONSTANTS.ORGANIZATIONAL_UNIT_DISSEMINATION_POLICY_PARAMETER_NAME);
    }
    this.typeToTranslate = EnumUtil.getLabel(Enums.DisseminationPolicy.TypeEnumTranslate, this.data.type);
  }
}
