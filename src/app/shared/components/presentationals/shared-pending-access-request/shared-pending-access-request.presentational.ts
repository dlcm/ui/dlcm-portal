/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-pending-access-request.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  OnInit,
} from "@angular/core";
import {Enums} from "@enums";
import {NotificationDlcm} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

export const sharedPendingAccessRequestPresentationalSelector = "dlcm-shared-pending-access-request";

@Component({
  selector: sharedPendingAccessRequestPresentationalSelector,
  templateUrl: "./shared-pending-access-request.presentational.html",
  styleUrls: ["./shared-pending-access-request.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedPendingAccessRequestPresentational extends SharedAbstractPresentational implements OnInit {
  private _notification: NotificationDlcm;

  @HostBinding("class.visible")
  @Input()
  set notification(value: NotificationDlcm) {
    this._notification = value;
    if (isNullOrUndefined(this.notification)) {
      return;
    }
    this._computeInfos();
  }

  get notification(): NotificationDlcm {
    return this._notification;
  }

  icon: IconNameEnum;
  tooltipToTranslate: string;
  tooltipClickForMoreInformationToTranslate: string = LabelTranslateEnum.clickForMoreInformation;
  cssClass: "pending" | "approved" | "refused";

  private _computeInfos(): void {
    const notificationStatus = this.notification.notificationStatus;
    switch (notificationStatus) {
      case Enums.Notification.StatusEnum.PENDING:
        this.icon = IconNameEnum.history;
        this.cssClass = "pending";
        this.tooltipToTranslate = MARK_AS_TRANSLATABLE("component.pendingAccessRequest.pending");
        break;
      case Enums.Notification.StatusEnum.APPROVED:
        this.icon = IconNameEnum.success;
        this.cssClass = "approved";
        this.tooltipToTranslate = MARK_AS_TRANSLATABLE("component.pendingAccessRequest.approved");
        break;
      case Enums.Notification.StatusEnum.REFUSED:
        this.icon = IconNameEnum.close;
        this.cssClass = "refused";
        this.tooltipToTranslate = MARK_AS_TRANSLATABLE("component.pendingAccessRequest.refused");
        break;
    }
  }

  constructor(private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  navigate($event: MouseEvent): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationSpaceNotificationSentDetail, this.notification.resId]));
    $event.stopPropagation();
  }

  externalChangeDetector(): void {
    this._changeDetector.detectChanges();
  }
}
