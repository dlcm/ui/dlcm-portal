/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-multi-action-button.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {MatButton} from "@angular/material/button";
import {
  MatMenu,
  MatMenuItem,
  MatMenuTrigger,
} from "@angular/material/menu";
import {TranslateModule} from "@ngx-translate/core";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  BaseResource,
  ExtraButtonToolbar,
  SolidifyFrontendApplicationModule,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-multi-action-button",
  templateUrl: "./shared-multi-action-button.presentational.html",
  styleUrls: ["./shared-multi-action-button.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    MatMenu,
    MatMenuTrigger,
    MatButton,
    MatMenuItem,
    SolidifyFrontendApplicationModule,
    MatButton,
    TranslateModule,
  ],
})
export class SharedMultiActionButtonPresentational<TResource extends BaseResource> extends SharedAbstractPresentational {
  @Input()
  resource: TResource;

  @Input()
  mainButton: ExtraButtonToolbar<TResource>;

  @Input()
  listSubActions: ExtraButtonToolbar<TResource>[];

  preventClick(event: Event): void {
    event.stopPropagation();
    event.preventDefault();
    event.stopImmediatePropagation();
  }
}
