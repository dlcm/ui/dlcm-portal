/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-package-button-navigator.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
} from "@shared/enums/routes.enum";
import {Storage} from "@shared/models/storage.model";
import {isNullOrUndefinedOrWhiteString} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-package-button-navigator",
  templateUrl: "./shared-package-button-navigator.presentational.html",
  styleUrls: ["./shared-package-button-navigator.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedPackageButtonNavigatorPresentational extends SharedAbstractPresentational {
  @Input()
  currentViewMode: PackageButtonNavigatorTypeEnum | undefined;

  @Input()
  depositId: string;

  @Input()
  orgUnitId: string;

  @Input()
  sipId: string;

  @Input()
  dipId: string;

  @Input()
  aipId: string;

  @Input()
  currentStorageIndex: number;

  archivalStorageList: Storage[] = ApiEnum.archivalStorageList;

  get packageButtonNavigatorTypeEnum(): typeof PackageButtonNavigatorTypeEnum {
    return PackageButtonNavigatorTypeEnum;
  }

  constructor(private readonly _store: Store) {
    super();
  }

  get navigateToDeposit(): string {
    if (isNullOrUndefinedOrWhiteString(this.orgUnitId) || isNullOrUndefinedOrWhiteString(this.depositId)) {
      return;
    }
    return ["/" + RoutesEnum.deposit, this.orgUnitId, DepositRoutesEnum.detail, this.depositId].join("/");
  }

  navigateToStoragion(storage: Storage): string {
    if (isNullOrUndefinedOrWhiteString(this.aipId)) {
      return;
    }
    return ["/" + AppRoutesEnum.preservationPlanning, PreservationPlanningRoutesEnum.aip, String(storage.index), SharedAipRoutesEnum.aipDetail, this.aipId].join("/");
  }

  navigateTo(targetPackage: PackageButtonNavigatorTypeEnum, id: string): string {
    if (isNullOrUndefinedOrWhiteString(id)) {
      return;
    }
    const url = this._getPath(targetPackage);
    return `/${url}/${id}`;
  }

  private _getPath(targetPackage: PackageButtonNavigatorTypeEnum): string {
    switch (targetPackage) {
      case PackageButtonNavigatorTypeEnum.sip:
        return RoutesEnum.preservationPlanningSipDetail;
      case PackageButtonNavigatorTypeEnum.dip:
        return RoutesEnum.preservationPlanningDipDetail;
      case PackageButtonNavigatorTypeEnum.archive:
        return RoutesEnum.archives;
    }
  }
}

export type PackageButtonNavigatorTypeEnum = "deposit" | "sip" | "storage" | "dip" | "archive" | "archiveDownloaded";
export const PackageButtonNavigatorTypeEnum = {
  deposit: "deposit" as PackageButtonNavigatorTypeEnum,
  sip: "sip" as PackageButtonNavigatorTypeEnum,
  storage: "storage" as PackageButtonNavigatorTypeEnum,
  dip: "dip" as PackageButtonNavigatorTypeEnum,
  archive: "archive" as PackageButtonNavigatorTypeEnum,
  archiveDownloaded: "archiveDownloaded" as PackageButtonNavigatorTypeEnum,
};
