/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-archive-rating-star.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isNotNullNorUndefined,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-archive-rating-star",
  templateUrl: "./shared-archive-rating-star.presentational.html",
  styleUrls: ["./shared-archive-rating-star.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SharedArchiveRatingStarPresentational,
    },
  ],
})
export class SharedArchiveRatingStarPresentational extends SharedAbstractPresentational implements OnInit, ControlValueAccessor {
  maxStarsNumber: number = 5;

  @Input()
  formControl: FormControl;

  @Input()
  value: number = 0;

  @Input()
  numberReview: number | undefined = undefined;

  @Input()
  label: string;

  @Input()
  center: boolean = false;

  @Input()
  isEditable: boolean = false;

  private readonly _valueBS: BehaviorSubject<number | undefined> = new BehaviorSubject<number | undefined>(undefined);
  @Output("valueChange")
  readonly valueObs: Observable<number | undefined> = ObservableUtil.asObservable(this._valueBS);

  ngOnInit(): void {
    super.ngOnInit();
    if (isNotNullNorUndefined(this.formControl)) {
      this.value = this.formControl.value;
    }
  }

  propagateChange: (__: any) => void = (__: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  hasBeenRated(): boolean {
    return this.value === 0;
  }

  rateChange(rate: number): void {
    this._valueBS.next(rate);
    this.formControl.setValue(rate);
    this.formControl.markAsDirty();
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
  }
}
