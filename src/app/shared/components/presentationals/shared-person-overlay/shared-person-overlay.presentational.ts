/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-person-overlay.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
} from "@angular/core";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  I18nLink,
  Institution,
  Person,
  User,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {overlayAnimation} from "@shared/components/presentationals/shared-resource-logo-overlay/shared-resource-logo-overlay.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {SharedPersonInstitutionAction} from "@shared/stores/person/institution/shared-people-institution.action";
import {SharedPersonAction} from "@shared/stores/person/shared-person.action";
import {SharedPersonState} from "@shared/stores/person/shared-person.state";
import {
  AbstractOverlayPresentational,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isNotNullNorUndefinedNorWhiteString,
  isTrue,
  MappingObjectUtil,
  ObjectUtil,
  QueryParameters,
  ResourceFileNameSpace,
  SsrUtil,
  StoreUtil,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-person-overlay",
  templateUrl: "./shared-person-overlay.presentational.html",
  styleUrls: ["./shared-person-overlay.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [overlayAnimation],
})
export class SharedPersonOverlayPresentational extends AbstractOverlayPresentational<Person, SharedPersonOverlayExtra> implements OnInit {
  protected _data: Person;

  personAvatarActionNameSpace: ResourceFileNameSpace = SharedPersonAction;
  personState: typeof SharedPersonState = SharedPersonState;

  extra: SharedPersonOverlayExtra;

  protected override _postUpdateData(): void {
  }

  get user(): User {
    if (isNullOrUndefined(this.data)) {
      return this.data;
    }
    return this.data as User;
  }

  get isAvatarPresent(): boolean {
    return isNotNullNorUndefined(this.data?.avatar);
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  constructor(protected readonly _elementRef: ElementRef,
              protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef) {
    super(_elementRef);
  }

  navigateToRorId(institution: Institution): void {
    const rodIdUrl = MappingObjectUtil.get(institution.identifiers, Enums.IdentifiersEnum.ROR);
    if (isNullOrUndefinedOrWhiteString(rodIdUrl)) {
      return;
    }
    SsrUtil.window?.open(rodIdUrl, "_blank");
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (isTrue(this.extra?.retrieveInstitution)) {
      const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo);
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new SharedPersonInstitutionAction.GetAll(this.data.resId, queryParameters),
        SharedPersonInstitutionAction.GetAllSuccess,
        result => {
          this.data = ObjectUtil.clone(this.data);
          this.data.institutions = result?.list?._data;
          this._changeDetector.detectChanges();
        },
      ));
    }

    if (isNotNullNorUndefinedNorWhiteString(this.data.orcid)) {
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new SharedPersonAction.GetExternalOrcidWebsites(this.data.orcid),
        SharedPersonAction.GetExternalOrcidWebsitesSuccess,
        result => {
          this.data = ObjectUtil.clone(this.data);
          const currentLanguage = MemoizedUtil.selectSnapshot(this._store, environment.appState, state => state.appLanguage);
          const orcidLinks: I18nLink[] = [];
          result.orcidWebsites.forEach(website => {
            website.links.forEach(link => {
              if (link.languageCode === currentLanguage) {
                orcidLinks.push(link);
              }
            });
          });
          this.data.orcidLinks = orcidLinks;
          this._changeDetector.detectChanges();
        },
      ));
    }
  }
}

export interface SharedPersonOverlayExtra {
  retrieveInstitution?: boolean;
}
