/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-compliance-level.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {Enums} from "@enums";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  EnumUtil,
  isNullOrUndefined,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-compliance-level-rating",
  templateUrl: "./shared-compliance-level.presentational.html",
  styleUrls: ["./shared-compliance-level.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SharedComplianceLevelPresentational,
    },
  ],
})

export class SharedComplianceLevelPresentational extends SharedAbstractPresentational implements OnInit, ControlValueAccessor {
  @Input()
  formControl: FormControl;

  @Input()
  set value(value: Enums.ComplianceLevel.ComplianceLevelEnum) {
    this._value = value;
    this.computeComplianceLevelInfos();
  }

  get value(): Enums.ComplianceLevel.ComplianceLevelEnum {
    return this._value;
  }

  _value: Enums.ComplianceLevel.ComplianceLevelEnum;

  @Input()
  withLabel: boolean = true;

  @Input()
  center: boolean = false;

  rating: number = 0;

  maxStarsNumber: number = 3;
  label: string | undefined = LabelTranslateEnum.complianceLevel;
  tooltipToTranslate: string | undefined;

  hasBeenAssessed: boolean = true;

  ngOnInit(): void {
    if (!isNullOrUndefined(this.formControl)) {
      this.subscribe(this.formControl.valueChanges.pipe(
        distinctUntilChanged(),
        tap(value => {
          this.value = value;
        }),
      ));
      this.value = this.formControl.value;
    }
  }

  computeComplianceLevelInfos(): void {
    const complianceLevel: Enums.ComplianceLevel.ComplianceLevelEnum = this.value;
    this.tooltipToTranslate = EnumUtil.getLabel(Enums.ComplianceLevel.ComplianceLevelEnumTranslate, complianceLevel);
    const complianceLevelNumber = Enums.ComplianceLevel.ConvertComplianceLevel.convertStringToNumber(complianceLevel);
    this.rating = complianceLevelNumber < 0 ? complianceLevelNumber : complianceLevelNumber - 1;
    this.computeHasBeenAssessed();
  }

  computeHasBeenAssessed(): void {
    this.hasBeenAssessed = isNullOrUndefined(this.value) || this.value === Enums.ComplianceLevel.ComplianceLevelEnum.NOT_ASSESSED;
  }

  writeValue(value: Enums.ComplianceLevel.ComplianceLevelEnum): void {
  }

  registerOnChange(fn: any): void {
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState?(isDisabled: boolean): void {
  }
}
