/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-datafile-quick-status.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  HostListener,
  Input,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Enums} from "@enums";
import {SharedInfoExcludedIgnoredFileDialog} from "@shared/components/dialogs/shared-info-excluded-ignored-file/shared-info-excluded-ignored-file.dialog";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {DialogUtil} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-datafile-quick-status",
  templateUrl: "./shared-datafile-quick-status.presentational.html",
  styleUrls: ["./shared-datafile-quick-status.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class SharedDatafileQuickStatusPresentational extends SharedAbstractPresentational {
  private readonly _CLASS_READY: string = "ready";
  private readonly _CLASS_ERROR: string = "error";
  private readonly _CLASS_WARNING: string = "warning";
  private readonly _CLASS_CLEANED: string = "cleaned";
  private readonly _CLASS_CAN_SEE_DETAIL: string = "can-see-detail";

  readonly CLASS_PENDING: string = "pending";

  _value: Enums.DataFile.StatusEnum;

  @Input()
  set value(value: Enums.DataFile.StatusEnum) {
    this._value = value;
    this.classes = this.getColor();
    this.tooltipToTranslate = Enums.DataFile.getStatusExplanation(value);
  }

  get value(): Enums.DataFile.StatusEnum {
    return this._value;
  }

  constructor(private readonly _dialog: MatDialog) {
    super();
  }

  tooltipToTranslate: string | undefined;

  classes: string;

  @HostListener("click", ["$event"]) click(mouseEvent: MouseEvent): void {
    mouseEvent.stopPropagation();
    if (this.value !== Enums.DataFile.StatusEnum.EXCLUDED_FILE && this.value !== Enums.DataFile.StatusEnum.IGNORED_FILE) {
      return;
    }
    DialogUtil.open(this._dialog, SharedInfoExcludedIgnoredFileDialog, {
      dataFileStatusEnum: this.value,
    });
  }

  getColor(): string {
    switch (this.value) {
      case Enums.DataFile.StatusEnum.READY:
        return this._CLASS_READY;
      case Enums.DataFile.StatusEnum.IN_ERROR:
        return this._CLASS_ERROR;
      case Enums.DataFile.StatusEnum.EXCLUDED_FILE:
        return this._CLASS_ERROR + " " + this._CLASS_CAN_SEE_DETAIL;
      case Enums.DataFile.StatusEnum.IGNORED_FILE:
        return this._CLASS_WARNING + " " + this._CLASS_CAN_SEE_DETAIL;
      case Enums.DataFile.StatusEnum.CLEANED:
        return this._CLASS_CLEANED;
      default:
        return this.CLASS_PENDING;
    }
  }
}
