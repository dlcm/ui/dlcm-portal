/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-resource-logo-overlay.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  animate,
  AnimationTriggerMetadata,
  style,
  transition,
  trigger,
} from "@angular/animations";
import {Directive} from "@angular/core";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {
  AbstractOverlayPresentational,
  BaseResourceLogo,
  isNotNullNorUndefined,
  isNullOrUndefinedOrWhiteString,
  ResourceFileNameSpace,
  ResourceFileState,
  SsrUtil,
} from "solidify-frontend";

@Directive()
export abstract class SharedResourceLogoOverlayPresentational<TResource extends BaseResourceLogo, UExtra = any> extends AbstractOverlayPresentational<TResource, UExtra> {
  protected _data: TResource;

  abstract logoActionNameSpace: ResourceFileNameSpace;
  abstract logoState: typeof ResourceFileState;

  get isLogoPresent(): boolean {
    return isNotNullNorUndefined(this.data?.logo);
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }

  navigateToExternalUrl(url: string): void {
    if (isNullOrUndefinedOrWhiteString(url)) {
      return;
    }
    SsrUtil.window?.open(url, "_blank");
  }
}

export const overlayAnimation: AnimationTriggerMetadata = trigger("overlay", [
  transition(":enter", [
    style({opacity: 0}),
    animate(300, style({opacity: 1})),
  ]),
  transition(":leave", [
    animate(300, style({opacity: 0})),
  ]),
]);
