/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-checksum-item.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Enums} from "@enums";
import {
  ClipboardUtil,
  EnumUtil,
  MARK_AS_TRANSLATABLE,
  NotificationService,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-checksum-item",
  templateUrl: "./shared-checksum-item.presentational.html",
  styleUrls: ["./shared-checksum-item.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedChecksumItemPresentational extends SharedAbstractPresentational {
  @Input()
  checksumAlgo: Enums.DataFile.Checksum.AlgoEnum;

  @Input()
  checksumOrigin: Enums.DataFile.Checksum.OriginEnum;

  @Input()
  checksum: string;

  get checksumOriginEnumTranslate(): typeof Enums.DataFile.Checksum.OriginEnumTranslate {
    return Enums.DataFile.Checksum.OriginEnumTranslate;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  constructor(private readonly _notificationService: NotificationService) {
    super();
  }

  copy(checksum: string): void {
    ClipboardUtil.copyStringToClipboard(checksum);
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("app.notification.checksumCopy"));
  }
}
