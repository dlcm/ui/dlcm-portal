/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-dua-type.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from "@angular/core";
import {Enums} from "@enums";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {KeyValueInfo} from "@shared/models/key-value-info.model";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  EnumUtil,
  isNotNullNorUndefined,
  isNullOrUndefined,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-dua-type",
  templateUrl: "./shared-dua-type.presentational.html",
  styleUrls: ["./shared-dua-type.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedDuaTypePresentational extends SharedAbstractPresentational implements OnInit {
  icon: IconNameEnum;
  label: string;

  _duaType: Enums.Deposit.DataUsePolicyEnum;

  @Input()
  set duaType(duaType: Enums.Deposit.DataUsePolicyEnum) {
    this._duaType = duaType;
    this._computeDataUsePolicyLogo(duaType);
    this._computeLabel(duaType);
  }

  get duaType(): Enums.Deposit.DataUsePolicyEnum {
    return this._duaType;
  }

  @Input()
  size: string = "size-24";

  @Input()
  withTooltip: boolean = false;

  constructor(private readonly _changeDetector: ChangeDetectorRef,
              private readonly _translateService: TranslateService,
              private readonly _store: Store,
              private readonly _cd: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this._translateService.onLangChange.asObservable().pipe(
      distinctUntilChanged(),
      tap(language => {
        this._computeLabel(this.duaType);
        this._cd.detectChanges();
      }),
    ));
  }

  private _computeDataUsePolicyLogo(duaType: Enums.Deposit.DataUsePolicyEnum): void {
    this.icon = this._getLogo(duaType);
  }

  private _computeLabel(duaType: Enums.Deposit.DataUsePolicyEnum): void {
    if (isNullOrUndefined(duaType)) {
      this.label = StringUtil.stringEmpty;
      return;
    }
    const keyValueInfos = EnumUtil.getKeyValue(Enums.Deposit.DuaTypeEnumTranslate, duaType) as KeyValueInfo;
    this.label = this._translateService.instant(keyValueInfos.value) + (isNotNullNorUndefined(keyValueInfos.infoToTranslate) ? " : " + this._translateService.instant(keyValueInfos.infoToTranslate) : StringUtil.stringEmpty);
  }

  private _getLogo(duaType: Enums.Deposit.DataUsePolicyEnum): IconNameEnum {
    switch (duaType) {
      case Enums.Deposit.DuaTypeEnum.SIGNED_DUA:
        return IconNameEnum.duaSigned;
      case Enums.Deposit.DuaTypeEnum.CLICK_THROUGH_DUA:
        return IconNameEnum.duaClickThrough;
      case Enums.Deposit.DuaTypeEnum.EXTERNAL_DUA:
        return IconNameEnum.duaExternal;
      default:
        return IconNameEnum.duaNone;
    }
  }
}
