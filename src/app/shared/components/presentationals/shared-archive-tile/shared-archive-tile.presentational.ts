/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-archive-tile.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {environment} from "@environments/environment";
import {ArchiveAccessRightService} from "@home/services/archive-access-right.service";
import {Archive} from "@models";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {MetadataUtil} from "@shared/utils/metadata.util";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  ButtonThemeEnum,
  DateUtil,
  DialogUtil,
  isNullOrUndefined,
  ObservableUtil,
} from "solidify-frontend";
import {Enums} from "@enums";
import {
  HomeArchiveClickThroughDuaDownloadDialog,
} from "@home/components/dialogs/archive-click-through-dua-download/home-archive-click-through-dua-download-dialog.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: "dlcm-shared-archive-tile",
  templateUrl: "./shared-archive-tile.presentational.html",
  styleUrls: ["./shared-archive-tile.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedArchiveTilePresentational extends SharedAbstractPresentational implements OnInit {
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;
  readonly disableAccessRequest: boolean = environment.disableAccessRequest;

  @Input()
  archive: Archive;

  _mode: SharedArchiveTileMode;

  @Input()
  set mode(value: SharedArchiveTileMode) {
    this._mode = value;
  }

  get mode(): SharedArchiveTileMode {
    return this._mode;
  }

  @Input()
  isNotLogged: boolean = false;

  @Input()
  alreadyInCart: boolean = false;

  @HostBinding("class.is-access-controlled")
  @Input()
  isAccessControlled: boolean = false;

  @HostBinding("class.is-line-display-mode")
  @Input()
  isLineDisplayMode: boolean = false;

  private readonly _downloadBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("downloadChange")
  readonly downloadObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._downloadBS);

  private readonly _deleteBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("deleteChange")
  readonly deleteObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._deleteBS);

  private readonly _addToCartBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("addToCartChange")
  readonly addToCartObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._addToCartBS);

  private readonly _requestAccessBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("requestAccessChange")
  readonly requestAccessObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._requestAccessBS);

  private readonly _selectBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("selectChange")
  readonly selectObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._selectBS);

  @HostListener("click", ["$event"]) click(mouseEvent: MouseEvent): void {
    this._selectBS.next(this.archive);
  }

  imageArchive: string | undefined;

  get publicationDate(): string {
    const publicationDate = MetadataUtil.getIssuedDate(this.archive?.archiveMetadata?.metadata);
    if (isNullOrUndefined(publicationDate)) {
      return this.archive.yearPublicationDate;
    }
    return DateUtil.convertDateToDateString(publicationDate);
  }

  get downloadButtonTheme(): ButtonThemeEnum {
    if (this._mode === "home-tiles") {
      return ButtonThemeEnum.flatButton;
    }
    return ButtonThemeEnum.button;
  }

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }

  constructor(readonly archiveAccessRightService: ArchiveAccessRightService,
              private readonly _dialog: MatDialog) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.imageArchive = `${ApiEnum.accessPublicMetadata}/${this.archive.resId}/${ApiActionNameEnum.THUMBNAIL}`;
  }

  download(mouseEvent: MouseEvent): void {
    mouseEvent.stopPropagation();
    mouseEvent.preventDefault();
    if (this.archive.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA) {
      this.openClickThroughDuaDialog();
    } else {
      this._downloadBS.next(this.archive);
    }
  }

  openClickThroughDuaDialog(): void {
    this.subscribe(DialogUtil.open(this._dialog, HomeArchiveClickThroughDuaDownloadDialog, {
        archive: this.archive,
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      }, () => {
        this._downloadBS.next(this.archive);
      }));
  }

  sendRequest(mouseEvent: MouseEvent): void {
    mouseEvent.stopPropagation();
    mouseEvent.preventDefault();
    this._requestAccessBS.next(this.archive);
  }

  delete(mouseEvent: MouseEvent): void {
    mouseEvent.stopPropagation();
    mouseEvent.preventDefault();
    this._deleteBS.next(this.archive);
  }

  addToCart(mouseEvent: MouseEvent): void {
    mouseEvent.stopPropagation();
    mouseEvent.preventDefault();
    this._addToCartBS.next(this.archive);
  }
}

export type SharedArchiveTileMode = "home-tiles" | "order-draft" | "order";
export const SharedArchiveTileMode = {
  HOME_TILES: "home-tiles" as SharedArchiveTileMode,
  ORDER_DRAFT: "order-draft" as SharedArchiveTileMode,
  ORDER: "order" as SharedArchiveTileMode,
};
