/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-notification-input.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {SecurityService} from "@shared/services/security.service";
import {
  FormValidationHelper,
  KeyValue,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-notification-input",
  templateUrl: "./shared-notification-input.presentational.html",
  styleUrls: ["./shared-notification-input.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: SharedNotificationInputPresentational,
    },
  ],
})
export class SharedNotificationInputPresentational extends SharedAbstractPresentational implements ControlValueAccessor {
  listNotificationType: KeyValue[] = Enums.NotificationType.NotificationTypeEnumTranslate;

  get listNotificationTypeFiltered(): KeyValue[] {
    let preFilteredList = this.listNotificationType.filter(n => this.LIST_NOTIFICATION_TYPE_TO_HIDE.indexOf(n.key as Enums.NotificationType.NotificationTypeEnum) === -1);
    if (!this.isApproverOfAnyOrgUnit && !this.isStewardOfAnyOrgUnit) {
      preFilteredList = preFilteredList.filter(n => this.LIST_NOTIFICATION_TYPE_FOR_VALIDATOR_ONLY.indexOf(n.key as Enums.NotificationType.NotificationTypeEnum) === -1);
    }
    if (this._securityService.isUser()) {
      preFilteredList = preFilteredList.filter(n => this.LIST_NOTIFICATION_TYPE_FOR_ADMIN_ONLY.indexOf(n.key as Enums.NotificationType.NotificationTypeEnum) === -1);
    }
    return preFilteredList;
  }

  LIST_NOTIFICATION_TYPE_TO_HIDE: Enums.NotificationType.NotificationTypeEnum[] = environment.listNotificationToHideInProfile;

  LIST_NOTIFICATION_TYPE_FOR_VALIDATOR_ONLY: Enums.NotificationType.NotificationTypeEnum[] = [
    Enums.NotificationType.NotificationTypeEnum.VALIDATE_DEPOSIT_REQUEST,
    Enums.NotificationType.NotificationTypeEnum.IN_ERROR_DEPOSIT_INFO,
    Enums.NotificationType.NotificationTypeEnum.COMPLETED_ARCHIVE_INFO,
  ];

  LIST_NOTIFICATION_TYPE_FOR_ADMIN_ONLY: Enums.NotificationType.NotificationTypeEnum[] = [
    Enums.NotificationType.NotificationTypeEnum.CREATE_ORGUNIT_REQUEST,
  ];

  @Input()
  isStewardOfAnyOrgUnit: boolean;

  @Input()
  isApproverOfAnyOrgUnit: boolean;

  @Input()
  formControl: FormControl;

  @Input()
  readonly: boolean;

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  get listValueChecked(): string[] {
    return this.formControl.value ?? [];
  }

  constructor(private readonly _dialog: MatDialog,
              private readonly _cd: ChangeDetectorRef,
              private readonly _securityService: SecurityService) {
    super();
  }

  propagateChange: (__: any) => void = (__: any) => {};

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(obj: any): void {
  }

  change(notificationId: string, checked: boolean): void {
    const indexOf = this.listValueChecked.indexOf(notificationId);
    if (checked) {
      if (indexOf === -1) {
        this.formControl.setValue([...this.listValueChecked, notificationId]);
        this.formControl.parent.markAsDirty();
      }
    } else {
      if (indexOf !== -1) {
        const listValueCheckedCopy = [...this.listValueChecked];
        listValueCheckedCopy.splice(indexOf, 1);
        this.formControl.setValue(listValueCheckedCopy);
        this.formControl.parent.markAsDirty();
      }
    }
  }

  isEnable(notificationId: string): boolean {
    return this.listValueChecked.indexOf(notificationId) !== -1;
  }
}
