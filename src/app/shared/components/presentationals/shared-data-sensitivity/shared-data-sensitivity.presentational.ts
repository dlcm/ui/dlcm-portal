/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-data-sensitivity.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from "@angular/core";
import {Enums} from "@enums";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {KeyValueInfo} from "@shared/models/key-value-info.model";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  EnumUtil,
  isNotNullNorUndefined,
  isNullOrUndefined,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-data-sensitivity",
  templateUrl: "./shared-data-sensitivity.presentational.html",
  styleUrls: ["./shared-data-sensitivity.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedDataSensitivityPresentational extends SharedAbstractPresentational implements OnInit {
  _dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum;
  dataSensitivityColor: DataSensitivityColor;
  icon: IconNameEnum = IconNameEnum.dataSensitivity;
  label: string;

  @Input()
  set dataSensitivity(dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum) {
    this._dataSensitivity = dataSensitivity;
    this._computeDataSensitivityColor(dataSensitivity);
    this._computeLabel(dataSensitivity);
  }

  get dataSensitivity(): Enums.DataSensitivity.DataSensitivityEnum {
    return this._dataSensitivity;
  }

  @Input()
  size: string = "size-24";

  @Input()
  withTooltip: boolean = false;

  constructor(private readonly _changeDetector: ChangeDetectorRef,
              private readonly _translateService: TranslateService,
              private readonly _store: Store,
              private readonly _cd: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this._translateService.onLangChange.asObservable().pipe(
      distinctUntilChanged(),
      tap(language => {
        this._computeLabel(this.dataSensitivity);
        this._cd.detectChanges();
      }),
    ));
  }

  private _computeDataSensitivityColor(dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum): void {
    this.dataSensitivityColor = this._getColor(dataSensitivity);
  }

  private _computeLabel(dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum): void {
    if (isNullOrUndefined(dataSensitivity)) {
      this.label = StringUtil.stringEmpty;
      return;
    }
    const keyValueInfos = EnumUtil.getKeyValue(Enums.DataSensitivity.DataSensitivityEnumTranslate, dataSensitivity) as KeyValueInfo;
    this.label = this._translateService.instant(keyValueInfos.value) + (isNotNullNorUndefined(keyValueInfos.infoToTranslate) ? " : " + this._translateService.instant(keyValueInfos.infoToTranslate) : StringUtil.stringEmpty);
  }

  private _getColor(dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum): DataSensitivityColor {
    switch (dataSensitivity) {
      case Enums.DataSensitivity.DataSensitivityEnum.BLUE:
        return "blue";
      case Enums.DataSensitivity.DataSensitivityEnum.GREEN:
        return "green";
      case Enums.DataSensitivity.DataSensitivityEnum.YELLOW:
        return "yellow";
      case Enums.DataSensitivity.DataSensitivityEnum.ORANGE:
        return "orange";
      case Enums.DataSensitivity.DataSensitivityEnum.RED:
        return "red";
      case Enums.DataSensitivity.DataSensitivityEnum.CRIMSON:
        return "crimson";
      case Enums.DataSensitivity.DataSensitivityEnum.UNDEFINED:
      default:
        return "undefined";
    }
  }
}

export type DataSensitivityColor = "blue" | "green" | "yellow" | "orange" | "red" | "crimson" | "undefined";
