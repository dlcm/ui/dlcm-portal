/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-info-excluded-ignored-file.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {DepositState} from "@deposit/stores/deposit.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {FileList} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {Observable} from "rxjs";
import {
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-info-excluded-ignored-file-dialog",
  templateUrl: "./shared-info-excluded-ignored-file.dialog.html",
  styleUrls: ["./shared-info-excluded-ignored-file.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedInfoExcludedIgnoredFileDialog extends SharedAbstractDialog<SharedInfoExcludedIgnoredFileDialogData> implements OnInit {
  fileListObs: Observable<FileList>;

  titleToTranslate: string = "";
  messageToTranslate: string = "";

  constructor(private readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<SharedInfoExcludedIgnoredFileDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: SharedInfoExcludedIgnoredFileDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (this.data.dataFileStatusEnum === Enums.DataFile.StatusEnum.IGNORED_FILE) {
      this.fileListObs = MemoizedUtil.select(this._store, DepositState, state => state.ignoredFileList);
      this.titleToTranslate = MARK_AS_TRANSLATABLE("deposit.file.dialog.excludedIgnoredFile.title.ignored");
      this.messageToTranslate = MARK_AS_TRANSLATABLE("deposit.file.dialog.excludedIgnoredFile.message.ignored");
    } else if (this.data.dataFileStatusEnum === Enums.DataFile.StatusEnum.EXCLUDED_FILE) {
      this.fileListObs = MemoizedUtil.select(this._store, DepositState, state => state.excludedFileList);
      this.titleToTranslate = MARK_AS_TRANSLATABLE("deposit.file.dialog.excludedIgnoredFile.title.excluded");
      this.messageToTranslate = MARK_AS_TRANSLATABLE("deposit.file.dialog.excludedIgnoredFile.message.excluded");
    } else {
      this.close();
    }
  }

  getPuidLink(puid: string): string {
    return environment.urlNationalArchivePronom + puid;
  }
}

export interface SharedInfoExcludedIgnoredFileDialogData {
  dataFileStatusEnum: Enums.DataFile.StatusEnum;
}
