/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-organizational-unit-dissemination-policy-parameters.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {DLCM_CONSTANTS} from "@app/constants";
import {Enums} from "@enums";
import {
  DisseminationPolicy,
  OrganizationalUnitDisseminationPolicyContainer,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {OrganizationalUnitDisseminationPolicyHelper} from "@shared/helpers/organizational-unit-dissemination-policy.helper";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SharedDisseminationPolicyAction} from "@shared/stores/dissemination-policy/shared-dissemination-policy.action";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  FormValidationHelper,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MappingObject,
  MappingObjectUtil,
  PropertyName,
  SolidifyForbiddenValuesValidator,
  SolidifyValidator,
  StoreUtil,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-organizational-unit-dissemination-policy-parameters-dialog",
  templateUrl: "./shared-organizational-unit-dissemination-policy-parameters.dialog.html",
  styleUrls: ["./shared-organizational-unit-dissemination-policy-parameters.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedOrganizationalUnitDisseminationPolicyParametersDialog extends SharedAbstractDialog<SharedOrganizationalUnitDisseminationPolicyParametersDialogData, OrganizationalUnitDisseminationPolicyContainer> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  formControlDisseminationPolicy: FormControl;
  formGroup: FormGroup;

  disseminationPolicies: DisseminationPolicy[];
  isLoadingObs: Observable<boolean>;

  get isCreateMode(): boolean {
    return isNullOrUndefined(this.data.organizationalUnitDisseminationPolicyContainer);
  }

  get isEditMode(): boolean {
    return !this.isCreateMode;
  }

  get stringUtil(): typeof StringUtil {
    return StringUtil;
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  listOtherOrgUnitDisseminationPoliciesParametersInJson: string[] = [];
  isAnotherDisseminationPolicyWithSameParams: boolean = false;
  uniqueNameValidator: ValidatorFn;

  constructor(protected readonly _dialogRef: MatDialogRef<SharedOrganizationalUnitDisseminationPolicyParametersDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: SharedOrganizationalUnitDisseminationPolicyParametersDialogData,
              private readonly _fb: FormBuilder,
              private readonly _actions$: Actions,
              private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef,
              protected readonly _translateService: TranslateService,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslate: LabelTranslateInterface) {
    super(_dialogRef, data);

    this._computeListOtherOrgUnitDisseminationPolicies();
    this._computeListOtherOrgUnitDisseminationPoliciesParametersInJson();

    this.formControlDisseminationPolicy = new FormControl(this.data.organizationalUnitDisseminationPolicyContainer?.resId ?? "", [Validators.required, SolidifyValidator]);
    if (this.isEditMode) {
      this.formControlDisseminationPolicy.disable();
    }
    this.formGroup = this._fb.group({});

    if (isNotNullNorUndefined(this.data.organizationalUnitDisseminationPolicyContainer)) {
      const parameters = this.data.organizationalUnitDisseminationPolicyContainer?.joinResource?.parameters;
      this._bindParametersToForm(JSON.parse(parameters));
    }
  }

  ngOnInit(): void {
    this._dispatchGetActionToRetrieveLabel();
  }

  private _dispatchGetActionToRetrieveLabel(): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedDisseminationPolicyAction.GetAll(),
      SharedDisseminationPolicyAction.GetAllSuccess,
      action => {
        this.disseminationPolicies = action.list._data.filter(policy => (policy.type !== Enums.DisseminationPolicy.TypeEnum.BASIC
          && policy.type !== Enums.DisseminationPolicy.TypeEnum.OAIS));
        this._computeCurrentDisseminationPolicy();
        this.subscribe(this._observeDisseminationPolicyValueChange());
        this._changeDetector.detectChanges();
      },
      SharedDisseminationPolicyAction.GetAllFail,
      action => {
        this._changeDetector.detectChanges();
      }));
  }

  private _observeDisseminationPolicyValueChange(): Observable<string> {
    return this.formControlDisseminationPolicy.valueChanges.pipe(
      distinctUntilChanged(),
      tap(resId => this._computeCurrentDisseminationPolicy()),
    );
  }

  private _computeCurrentDisseminationPolicy(): void {
    const disseminationResId = this.formControlDisseminationPolicy.value;
    if (isNullOrUndefinedOrWhiteString(disseminationResId)) {
      return;
    }
    const disseminationPolicy = this.disseminationPolicies.find(policy => policy.resId === disseminationResId);
    this._selectDisseminationPolicy(disseminationPolicy);
  }

  private _bindParametersToForm(parameters: MappingObject<string, string>): void {
    // Clear form group by creating a new empty form group
    this.formGroup = this._fb.group({});
    // Add new controls that correspond to the parameters
    MappingObjectUtil.keys(parameters).forEach(key => {
      const validators = [Validators.required, SolidifyValidator];
      if (key === DLCM_CONSTANTS.ORGANIZATIONAL_UNIT_DISSEMINATION_POLICY_PARAMETER_NAME) {
        validators.push(this.uniqueNameValidator);
      }
      this.formGroup.addControl(key, this._fb.control(parameters[key] ?? "", validators));
    });

    this.subscribe(this.formGroup.valueChanges.pipe(
      tap(form => {
        this._computeIsParametersValid();
        this._changeDetector.detectChanges();
      }),
    ));
  }

  validate(): void {
    const organizationalUnitDisseminationPolicyContainer = {
      ...this.currentDisseminationPolicy,
      joinResource: {
        parameters: JSON.stringify(this.formGroup.value),
      } as any,
    } as OrganizationalUnitDisseminationPolicyContainer;
    this.submit(OrganizationalUnitDisseminationPolicyHelper.fillComputedName(this._translateService, organizationalUnitDisseminationPolicyContainer));
  }

  currentDisseminationPolicy: DisseminationPolicy;

  private _selectDisseminationPolicy(disseminationPolicy: DisseminationPolicy): void {
    this.currentDisseminationPolicy = disseminationPolicy;
    if (this.data.organizationalUnitDisseminationPolicyContainer?.resId === disseminationPolicy?.resId) {
      return;
    }
    const parameters: MappingObject<string, string> = isNotNullNorUndefinedNorWhiteString(disseminationPolicy.parameters) ? JSON.parse(disseminationPolicy.parameters) : {};
    this._bindParametersToForm(parameters);
    this._computeListOtherOrgUnitDisseminationPoliciesParametersInJson();
    this._computeIsParametersValid();
  }

  private _computeListOtherOrgUnitDisseminationPolicies(): void {
    const listForbiddenNames = this.data.listOtherOrganizationalUnitDisseminationPolicyContainer
      .map(o => o.joinResource.parameters)
      .map(o => JSON.parse(o)?.[DLCM_CONSTANTS.ORGANIZATIONAL_UNIT_DISSEMINATION_POLICY_PARAMETER_NAME])
      .filter(o => isNotNullNorUndefinedNorWhiteString(o));
    this.uniqueNameValidator = SolidifyForbiddenValuesValidator(this._translateService, listForbiddenNames, this._labelTranslate.applicationAlreadyUsed, true);
  }

  private _computeListOtherOrgUnitDisseminationPoliciesParametersInJson(): void {
    this.listOtherOrgUnitDisseminationPoliciesParametersInJson = this.data.listOtherOrganizationalUnitDisseminationPolicyContainer
      .filter(o => isNullOrUndefined(this.currentDisseminationPolicy) || o.resId === this.currentDisseminationPolicy?.resId)
      .map(o => {
        const parameters = JSON.parse(o.joinResource.parameters);
        MappingObjectUtil.delete(parameters, DLCM_CONSTANTS.ORGANIZATIONAL_UNIT_DISSEMINATION_POLICY_PARAMETER_NAME);
        return JSON.stringify(parameters) as string;
      });
  }

  private _computeIsParametersValid(): void {
    const formValue = {...this.formGroup.value};
    // Ignore name in comparison
    delete formValue[DLCM_CONSTANTS.ORGANIZATIONAL_UNIT_DISSEMINATION_POLICY_PARAMETER_NAME];
    const formParam = JSON.stringify(formValue);
    this.isAnotherDisseminationPolicyWithSameParams = this.listOtherOrgUnitDisseminationPoliciesParametersInJson.findIndex(o => o === formParam) >= 0;
  }
}

export interface SharedOrganizationalUnitDisseminationPolicyParametersDialogData {
  organizationalUnitDisseminationPolicyContainer?: OrganizationalUnitDisseminationPolicyContainer;
  listOtherOrganizationalUnitDisseminationPolicyContainer?: OrganizationalUnitDisseminationPolicyContainer[];
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() resId: string;
}
