/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-citation.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {SharedCitationAction} from "@shared/stores/citations/shared-citation.action";
import {SharedCitationState} from "@shared/stores/citations/shared-citation.state";
import {Observable} from "rxjs";
import {
  Citation,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-shared-citation-dialog",
  templateUrl: "./shared-citation.dialog.html",
  styleUrls: ["./shared-citation.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SharedCitationDialog extends SharedAbstractDialog<SharedCitationDialog> {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedCitationState);
  listCitationsObs: Observable<Citation[]> = MemoizedUtil.select(this._store, SharedCitationState, state => state.citations);
  listBibliographiesObs: Observable<Citation[]> = MemoizedUtil.select(this._store, SharedCitationState, state => state.bibliographies);

  get localStorage(): typeof LocalStorageEnum {
    return LocalStorageEnum;
  }

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<SharedCitationDialog>,
              private readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: SharedCitationDialog) {
    super(_dialogRef, data);
    this._store.dispatch(new SharedCitationAction.GetBibliographies(this.data.archiveId));
    this._store.dispatch(new SharedCitationAction.GetCitations(this.data.archiveId));
  }
}

export interface SharedCitationDialog {
  archiveId: string;
  archiveTitle: string;
}
