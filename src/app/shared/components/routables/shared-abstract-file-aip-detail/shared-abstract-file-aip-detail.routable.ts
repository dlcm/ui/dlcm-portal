/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared-abstract-file-aip-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectorRef,
  Directive,
  OnInit,
  Type,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {
  Aip,
  DataFile,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  SharedFileAndAipDetailDialogModeEnum,
  SharedFileAndAipInformationContainer,
  SharedFileAndAipInformationDialogData,
} from "@shared/components/containers/shared-file-and-aip-information/shared-file-and-aip-information.container";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {LocalStateModel} from "@shared/models/local-state.model";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {SecurityService} from "@shared/services/security.service";
import {StatusHistoryNamespace} from "@shared/stores/status-history/status-history-namespace.model";
import {
  StatusHistoryState,
  StatusHistoryStateModel,
} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  filter,
  tap,
} from "rxjs/operators";
import {
  AssociationNameSpace,
  AssociationRemoteNameSpace,
  CompositionNameSpace,
  DeleteDialog,
  DialogUtil,
  ExtraButtonToolbar,
  isNotNullNorUndefined,
  isTrue,
  MemoizedUtil,
  MemoizedUtilStateType,
  StoreUtil,
} from "solidify-frontend";

@Directive()
export abstract class SharedAbstractFileAipDetailRoutable<TResource extends DataFile | Aip> extends SharedAbstractRoutable implements OnInit {
  resourceObs: Observable<TResource>;

  mode: SharedFileAndAipDetailDialogModeEnum;
  data: SharedFileAndAipInformationDialogData<TResource>;

  canDelete: boolean;

  @ViewChild("sharedFileAndAipInformationContainer")
  readonly sharedFileAndAipInformationContainer: SharedFileAndAipInformationContainer<TResource>;

  protected _parentId: string | undefined;
  protected _dataFileId: string | undefined;

  protected _extraActions: ExtraButtonToolbar<TResource>[] = [];

  protected _actionsFile: ExtraButtonToolbar<DataFile>[] = [
    {
      labelToTranslate: (current) => LabelTranslateEnum.download,
      color: "primary",
      icon: IconNameEnum.download,
      callback: current => this._downloadDataFile(),
      displayCondition: current => current.status === Enums.DataFile.StatusEnum.PROCESSED || current.status === Enums.DataFile.StatusEnum.READY || current.status === Enums.DataFile.StatusEnum.VIRUS_CHECKED,
      order: 52,
    },
    {
      labelToTranslate: (current) => LabelTranslateEnum.resume,
      color: "primary",
      icon: IconNameEnum.resume,
      callback: current => this._resume(),
      displayCondition: current => current.status === Enums.DataFile.StatusEnum.IN_ERROR,
      order: 54,
    },
    {
      labelToTranslate: (current) => LabelTranslateEnum.putInError,
      color: "primary",
      icon: IconNameEnum.putInError,
      callback: current => this._putInError(),
      displayCondition: current => (this._securityService.isRoot()
        && current.status !== Enums.DataFile.StatusEnum.READY
        && current.status !== Enums.DataFile.StatusEnum.CLEANED),
      order: 56,
    },
  ];

  protected _actionsAip: ExtraButtonToolbar<Aip>[] = [
    {
      labelToTranslate: (current) => LabelTranslateEnum.goToAip,
      icon: IconNameEnum.internalLink,
      color: "primary",
      callback: current => this._goToAip(),
      order: 52,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _state: MemoizedUtilStateType<any, TResource, any>,
              protected readonly _stateAction: CompositionNameSpace | AssociationNameSpace | AssociationRemoteNameSpace,
              protected readonly _statusHistoryState: Type<StatusHistoryState<StatusHistoryStateModel<TResource>, TResource>>,
              protected readonly _statusHistoryNamespace: StatusHistoryNamespace,
              protected readonly _storeDialogService: StoreDialogService,
              protected readonly _securityService: SecurityService) {
    super();
    this.resourceObs = MemoizedUtil.current(this._store, _state);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._retrieveResIdFromUrl();
    this._computeMode();
    if (isNotNullNorUndefined(this.mode)) {
      this.subscribe(this._observableCreateData());
      this._getOrRefreshResource();
    }
  }

  private _computeButtons(): ExtraButtonToolbar<TResource | Aip | DataFile>[] {
    if (this.mode === SharedFileAndAipDetailDialogModeEnum.aip) {
      return [...this._actionsAip, ...this._extraActions];
    } else if (this.mode === SharedFileAndAipDetailDialogModeEnum.file) {
      return [...this._actionsFile, ...this._extraActions];
    }
    return [];
  }

  protected _observableCreateData(): Observable<TResource> {
    return this.resourceObs.pipe(
      filter(resource => isNotNullNorUndefined(resource) && resource.resId === this._dataFileId),
      tap(resource => {
        this.data = {
          mode: this.mode,
          dataFile: this.mode === SharedFileAndAipDetailDialogModeEnum.file ? resource : undefined,
          aip: this.mode === SharedFileAndAipDetailDialogModeEnum.aip ? resource : undefined,
          resId: resource.resId,
          parentId: this._parentId,
          statusHistoryState: this._statusHistoryState,
          statusHistoryNamespace: this._statusHistoryNamespace,
          buttons: this._computeButtons(),
        } as SharedFileAndAipInformationDialogData<TResource>;
        this._computeCanDelete();
        this._changeDetector.detectChanges();
      }),
    );
  }

  protected _computeCanDelete(): void {
    let status;
    if (this.mode === SharedFileAndAipDetailDialogModeEnum.file) {
      status = this.data.dataFile.status;
    } else if (this.mode === SharedFileAndAipDetailDialogModeEnum.aip) {
      status = this.data.aip.info.status;
    }
    this.canDelete = status === Enums.DataFile.StatusEnum.IN_ERROR;
  }

  protected _retrieveResIdFromUrl(): void {
    this._dataFileId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._parentId = this._route.parent.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  protected _computeMode(): void {
    const url = this._store.selectSnapshot((s: LocalStateModel) => s.router.state.url);
    const route = url.substring(0, url.lastIndexOf(urlSeparator));
    if (route.endsWith("files")) {
      this.mode = SharedFileAndAipDetailDialogModeEnum.file;
    } else if (route.endsWith("collections")) {
      this.mode = SharedFileAndAipDetailDialogModeEnum.aip;
    } else {
      // eslint-disable-next-line no-console
      console.error("Unable to define mode file or aip");
    }
  }

  protected _getOrRefreshResource(): void {
    this._store.dispatch(new this._stateAction.GetById(this._parentId, this._dataFileId));
  }

  private _downloadDataFile(): void {
    this._store.dispatch(new this._stateAction["Download"](this._parentId, this.data.dataFile));
  }

  private _resume(): void {
    this._store.dispatch(new this._stateAction["Resume"](this._parentId, this.data.dataFile));
  }

  private _putInError(): void {
    this._store.dispatch(new this._stateAction["PutInError"](this._parentId, this.data.dataFile));
  }

  protected _goToAip(): void {
    this._store.dispatch(new this._stateAction["GoToAip"](this.data.aip));
  }

  delete(): void {
    let objectToSend;
    let name;
    if (this.mode === SharedFileAndAipDetailDialogModeEnum.file) {
      objectToSend = this.data.dataFile;
      name = this.data.dataFile.fileName;
    } else if (this.mode === SharedFileAndAipDetailDialogModeEnum.aip) {
      objectToSend = this.data.aip;
      name = this.data.aip.info.name;
    } else {
      return;
    }
    const stateEnum = StoreUtil.getStateNameFromClass(this._state);
    const deleteData = this._storeDialogService.deleteData(stateEnum);
    deleteData.name = name;
    this.subscribe(DialogUtil.open(this._dialog, DeleteDialog, deleteData,
      {
        width: "400px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {

          this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
            new this._stateAction.Delete(this._parentId, objectToSend.resId),
            this._stateAction.DeleteSuccess,
            resultAction => {
              this._store.dispatch(this.sharedFileAndAipInformationContainer.backToListNavigate());
            }));
        }
      }));
  }
}
