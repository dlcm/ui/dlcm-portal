/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - shared.const.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {
  isNullOrUndefined,
  StringUtil,
} from "solidify-frontend";

export const dataTestAttributeName: string = "data-test";

export const getMostImportantRole: (listRoles: any[]) => Enums.Role.RoleEnum = (listRoles: any[]) => {
  if (listRoles.length === 0) {
    return "";
  }
  if (listRoles.length === 1) {
    return listRoles[0];
  }
  let role = listRoles.find(r => r === Enums.Role.RoleEnum.MANAGER);
  if (!isNullOrUndefined(role)) {
    return Enums.Role.RoleEnum.MANAGER;
  }
  role = listRoles.find(r => r === Enums.Role.RoleEnum.STEWARD);
  if (!isNullOrUndefined(role)) {
    return Enums.Role.RoleEnum.STEWARD;
  }
  role = listRoles.find(r => r === Enums.Role.RoleEnum.APPROVER);
  if (!isNullOrUndefined(role)) {
    return Enums.Role.RoleEnum.APPROVER;
  }
  role = listRoles.find(r => r === Enums.Role.RoleEnum.CREATOR);
  if (!isNullOrUndefined(role)) {
    return Enums.Role.RoleEnum.CREATOR;
  }
  role = listRoles.find(r => r === Enums.Role.RoleEnum.VISITOR);
  if (!isNullOrUndefined(role)) {
    return Enums.Role.RoleEnum.VISITOR;
  }
  return StringUtil.stringEmpty;
};
