/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - cookie-consent-preferences.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {
  CookiePreference,
  CookieType,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

export const cookieConsentPreferences: CookiePreference[] = [
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.notificationInboxPending,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.notificationInboxPending.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.notificationInboxPending.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.cart,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.cart.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.cart.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.orderPending,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.orderPending.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.orderPending.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.newOrderAvailable,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.newOrderAvailable.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.newOrderAvailable.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.orgUnitForDepositMenu,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.orgUnitForDepositMenu.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.orgUnitForDepositMenu.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.singleSelectLastSelectionOrgUnit,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.singleSelectLastSelectionOrgUnit.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.singleSelectLastSelectionOrgUnit.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.multiSelectLastSelectionContributor,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.multiSelectLastSelectionContributor.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.multiSelectLastSelectionContributor.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.preservationSpaceOrganizationalUnitShowOnlyMine,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.preservationSpaceOrganizationalUnitShowOnlyMine.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.preservationSpaceOrganizationalUnitShowOnlyMine.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.preservationSpaceInstitutionShowOnlyMine,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.preservationSpaceInstitutionShowOnlyMine.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.preservationSpaceInstitutionShowOnlyMine.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.depositShowOnlyMine,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.depositShowOnlyMine.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.depositShowOnlyMine.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.orderShowOnlyPublicOrders,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.orderShowOnlyPublicOrders.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.orderShowOnlyPublicOrders.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.adminShowOnlyMyInstitutions,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.adminShowOnlyMyInstitutions.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.adminShowOnlyMyInstitutions.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.adminShowOnlyArchiveMastersTypes,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.adminShowOnlyArchiveMastersTypes.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.adminShowOnlyArchiveMastersTypes.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.lastBibliographyFormatUsed,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.lastBibliographiesFormatUsed.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.lastBibliographiesFormatUsed.description"),
    visible: () => true,
  },
  {
    type: CookieType.localStorage,
    isEnabled: false,
    name: LocalStorageEnum.lastCitationFormatUsed,
    labelToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.lastCitationsFormatUsed.title"),
    descriptionToTranslate: MARK_AS_TRANSLATABLE("cookieConsentSidebar.lastCitationsFormatUsed.description"),
    visible: () => true,
  },
];
