/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - meta-info-list.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Archive} from "@models";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {MetadataUtil} from "@shared/utils/metadata.util";
import {
  DateUtil,
  MARK_AS_TRANSLATABLE,
  MetaInfo,
  MetaUtil,
} from "solidify-frontend";

const TITLE_MAX_CHAR: number = 60;
const ELLIPSIS: string = "...";

const META_NAME_KEYWORDS: string = "keywords";
const META_NAME_ARCHIVE_ABSTRACT: string = "archive_abstract";
const META_NAME_ARCHIVE_TYPE: string = "archive_type";
const META_NAME_ARCHIVE_DOI: string = "archive_doi";
const META_NAME_ARCHIVE_TITLE: string = "archive_title";
const META_NAME_ARCHIVE_AUTHOR: string = "archive_author";
const META_NAME_ARCHIVE_AUTHORS: string = "archive_authors";
const META_NAME_ARCHIVE_DATE: string = "archive_date";
const META_NAME_ARCHIVE_KEYWORDS: string = "archive_keywords";

export const LIST_ARCHIVE_META_NAME: string[] = [
  META_NAME_KEYWORDS,
  META_NAME_ARCHIVE_ABSTRACT,
  META_NAME_ARCHIVE_TYPE,
  META_NAME_ARCHIVE_DOI,
  META_NAME_ARCHIVE_TITLE,
  META_NAME_ARCHIVE_AUTHOR,
  META_NAME_ARCHIVE_DATE,
  META_NAME_ARCHIVE_AUTHORS,
  META_NAME_ARCHIVE_KEYWORDS,
];

export const metaInfoArchive: MetaInfo<Archive> = {
  regex: /^\/home\/detail\/.*/,
  titleToTranslateCallback: (translate, archive) => {
    const suffix = translate.instant(MARK_AS_TRANSLATABLE("meta.archive.suffixTitle"));
    const title: string = archive.title;
    const concatenation: string = title + suffix;
    if (concatenation.length <= TITLE_MAX_CHAR) {
      return concatenation;
    }
    return title.substring(0, TITLE_MAX_CHAR - (suffix.length + ELLIPSIS.length)) + ELLIPSIS + suffix;
  },
  descriptionToTranslateCallback: (translate, archive) => translate.instant(MARK_AS_TRANSLATABLE("meta.archive.description"), archive),
  extraMetaTreatmentCallback: (document, meta, translateService, archive) => {
    const metadata = archive.archiveMetadata.metadata;
    MetaUtil.cleanListMetaByName(meta, LIST_ARCHIVE_META_NAME);

    MetaUtil.setMetaTagNameArrayWithSeparator(meta, META_NAME_KEYWORDS, archive.keywords);
    MetaUtil.setMetaTagNameArrayWithSeparator(meta, META_NAME_ARCHIVE_KEYWORDS, archive.keywords);
    MetaUtil.setMetaTagName(meta, META_NAME_ARCHIVE_ABSTRACT, archive.description);
    MetaUtil.setMetaTagName(meta, META_NAME_ARCHIVE_TYPE, MetadataUtil.getType(metadata) === "DATASET" ? LabelTranslateEnum.dataset : MetadataUtil.getType(metadata));
    MetaUtil.setMetaTagName(meta, META_NAME_ARCHIVE_DOI, MetadataUtil.getDOI(metadata));
    MetaUtil.setMetaTagName(meta, META_NAME_ARCHIVE_TITLE, archive.title);
    const listAuthors = archive.contributors?.map(c => c.creatorName);
    MetaUtil.setMetaTagNameForEachArray(meta, META_NAME_ARCHIVE_AUTHOR, listAuthors);
    MetaUtil.setMetaTagName(meta, META_NAME_ARCHIVE_DATE, DateUtil.convertDateToDateTimeString(MetadataUtil.getSubmittedDate(metadata)));
    MetaUtil.setMetaTagNameArrayWithSeparator(meta, META_NAME_ARCHIVE_AUTHORS, listAuthors, "; ");
    if (archive.withThumbnail) {
      MetaUtil.setMetaTagProperty(meta, "og:image", `${ApiEnum.accessPublicMetadata}/${archive.resId}/${ApiActionNameEnum.THUMBNAIL}`);
    }
  },
  bypassAutomaticMeta: true,
};

export const metaInfoAbout: MetaInfo<void> = {
  regex: /^\/about*/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaAboutTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaAboutDescription),
};

export const metaInfoAdmin: MetaInfo<void> = {
  regex: /^\/admin\/*/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaAdminTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaGeneralDescription),
};

export const metaInfoDefault: MetaInfo<void> = {
  regex: /^.*/,
  titleToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaGeneralTitle),
  descriptionToTranslateCallback: (translate) => translate.instant(LabelTranslateEnum.metaGeneralDescription),
};

export const metaInfoList: MetaInfo<any>[] = [
  metaInfoArchive,
  metaInfoAbout,
  metaInfoAdmin,
  metaInfoDefault,
];
