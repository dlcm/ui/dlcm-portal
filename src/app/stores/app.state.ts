/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {
  Inject,
  Injectable,
  LOCALE_ID,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {AppExtendAction} from "@app/stores/app.action";
import {AppArchiveAclAction} from "@app/stores/archive-acl/app-archive-acl.action";
import {
  AppArchiveAclState,
  AppArchiveAclStateModel,
} from "@app/stores/archive-acl/app-archive-acl.state";
import {AppAuthorizedInstitutionAction} from "@app/stores/authorized-institution/app-authorized-institution.action";
import {
  AppAuthorizedInstitutionState,
  AppAuthorizedInstitutionStateModel,
} from "@app/stores/authorized-institution/app-authorized-institution.state";
import {AppAuthorizedOrganizationalUnitAction} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {
  AppAuthorizedOrganizationalUnitState,
  AppAuthorizedOrganizationalUnitStateModel,
} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.state";
import {AppCartAction} from "@app/stores/cart/app-cart.action";
import {AppCartState} from "@app/stores/cart/app-cart.state";
import {AppCartOrderAction} from "@app/stores/cart/order/app-cart-order.action";
import {AppCartOrderState} from "@app/stores/cart/order/app-cart-order.state";
import {AppMemberOrganizationalUnitAction} from "@app/stores/member-organizational-unit/app-member-organizational-unit.action";
import {
  AppMemberOrganizationalUnitState,
  AppMemberOrganizationalUnitStateModel,
} from "@app/stores/member-organizational-unit/app-member-organizational-unit.state";
import {AppNotificationInboxAction} from "@app/stores/notification-inbox/app-notification-inbox.action";
import {AppNotificationInboxState} from "@app/stores/notification-inbox/app-notification-inbox.state";
import {
  AppPersonState,
  AppPersonStateModel,
} from "@app/stores/person/app-person.state";
import {
  AppSystemPropertyState,
  AppSystemPropertyStateModel,
} from "@app/stores/system-property/app-system-property.state";
import {
  AppTocState,
  AppTocStateModel,
} from "@app/stores/toc/app-toc.state";
import {AppUserAction} from "@app/stores/user/app-user.action";
import {
  AppUserState,
  defaultAppUserStateModel,
} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {ArchiveAccessRightService} from "@home/services/archive-access-right.service";
import {
  ApplicationRole,
  NotificationDlcm,
  Order,
  OrganizationalUnit,
  Person,
  User,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Token} from "@shared/models/token.model";
import {SecurityService} from "@shared/services/security.service";
import {SharedNotificationAction} from "@shared/stores/notification/shared-notification.action";
import {SharedRoleAction} from "@shared/stores/role/shared-role.action";
import {HighlightLoader} from "ngx-highlightjs";
import {Observable} from "rxjs";
import {
  ApiService,
  APP_OPTIONS,
  AppBannerState,
  AppBannerStateModel,
  AppCarouselState,
  AppConfigService,
  AppDoiState,
  CookieConsentUtil,
  CookieType,
  defaultSolidifyApplicationAppStateInitValue,
  isNullOrUndefined,
  LocalStorageHelper,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NativeNotificationService,
  NotificationService,
  OAuth2Service,
  ofSolidifyActionCompleted,
  PollingHelper,
  PrivacyPolicyTermsOfUseApprovalDialog,
  QueryParameters,
  SolidifyAppAction,
  SolidifyApplicationAppOptions,
  SolidifyApplicationAppState,
  SolidifyApplicationAppStateModel,
  SolidifyAppUserStateModel,
  SolidifyStateContext,
  SsrUtil,
  StoreUtil,
  TransferStateService,
} from "solidify-frontend";

export interface AppStateModel extends SolidifyApplicationAppStateModel {
  userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[];
  [StateEnum.application_person]: AppPersonStateModel | undefined;
  [StateEnum.application_authorizedOrganizationalUnit]: AppAuthorizedOrganizationalUnitStateModel | undefined;
  [StateEnum.application_memberOrganizationalUnit]: AppMemberOrganizationalUnitStateModel | undefined;
  [StateEnum.application_authorizedInstitution]: AppAuthorizedInstitutionStateModel | undefined;
  [StateEnum.application_toc]: AppTocStateModel | undefined;
  [StateEnum.application_systemProperty]: AppSystemPropertyStateModel;
  [StateEnum.application_banner]: AppBannerStateModel | undefined;
  [StateEnum.application_archiveAcl]: AppArchiveAclStateModel | undefined;
}

@Injectable()
@State<AppStateModel>({
  name: StateEnum.application,
  defaults: {
    ...defaultSolidifyApplicationAppStateInitValue(),
    theme: environment.theme,
    userRoles: [Enums.UserApplicationRole.UserApplicationRoleEnum.guest],
    [StateEnum.application_person]: undefined,
    [StateEnum.application_authorizedOrganizationalUnit]: undefined,
    [StateEnum.application_memberOrganizationalUnit]: undefined,
    [StateEnum.application_authorizedInstitution]: undefined,
    [StateEnum.application_toc]: undefined,
    [StateEnum.application_systemProperty]: undefined,
    [StateEnum.application_banner]: undefined,
    [StateEnum.application_archiveAcl]: undefined,
  },
  children: [
    AppUserState,
    AppPersonState,
    AppAuthorizedOrganizationalUnitState,
    AppMemberOrganizationalUnitState,
    AppAuthorizedInstitutionState,
    AppCartState,
    AppTocState,
    AppNotificationInboxState,
    AppSystemPropertyState,
    AppBannerState,
    AppDoiState,
    AppCarouselState,
    AppArchiveAclState,
  ],
})
export class AppState extends SolidifyApplicationAppState<AppStateModel> {
  constructor(@Inject(LOCALE_ID) protected readonly _locale: string,
              protected readonly _store: Store,
              protected readonly _translate: TranslateService,
              protected readonly _oauthService: OAuth2Service,
              protected readonly _actions$: Actions,
              protected readonly _apiService: ApiService,
              protected readonly _httpClient: HttpClient,
              protected readonly _notificationService: NotificationService,
              @Inject(APP_OPTIONS) protected readonly _optionsState: SolidifyApplicationAppOptions,
              protected readonly _hljsLoader: HighlightLoader,
              protected readonly _nativeNotificationService: NativeNotificationService,
              protected readonly _securityService: SecurityService,
              protected readonly _appConfigService: AppConfigService,
              protected readonly _dialog: MatDialog,
              protected readonly _transferState: TransferStateService,
              private readonly _archiveAccessRightService: ArchiveAccessRightService,
  ) {
    super(
      _locale,
      _store,
      _translate,
      _oauthService,
      _actions$,
      _apiService,
      _httpClient,
      _notificationService,
      _optionsState,
      environment,
      _hljsLoader,
      _nativeNotificationService,
      _appConfigService,
      _dialog,
      PrivacyPolicyTermsOfUseApprovalDialog,
      _securityService,
      _transferState,
    );
  }

  @Selector()
  static listAuthorizedOrganizationalUnit(state: AppStateModel): OrganizationalUnit[] {
    return state.application_authorizedOrganizationalUnit.list;
  }

  @Selector()
  static listAuthorizedOrganizationalUnitId(state: AppStateModel): string[] {
    const list = this.listAuthorizedOrganizationalUnit(state);
    if (isNullOrUndefined(list)) {
      return [];
    }
    return list.map(o => o.resId);
  }

  @Selector()
  static currentPerson(state: AppStateModel): Person {
    return state.application_person.current;
  }

  @Selector()
  static currentUser(state: AppStateModel): User {
    return state.application_user.current;
  }

  @Selector()
  static currentUserApplicationRole(state: AppStateModel): ApplicationRole {
    const currentPersonRole = this.currentUser(state);
    if (isNullOrUndefined(currentPersonRole)) {
      return undefined;
    }
    return currentPersonRole.applicationRole;
  }

  @Selector()
  static currentUserApplicationRoleResId(state: AppStateModel): string | undefined {
    const applicationRole = this.currentUserApplicationRole(state);
    if (isNullOrUndefined(applicationRole)) {
      return undefined;
    }
    return applicationRole.resId;
  }

  @Action(AppExtendAction.LoadCart)
  loadCart(ctx: SolidifyStateContext<AppStateModel>, action: AppExtendAction.LoadCart): void {
    ctx.dispatch(new AppCartAction.GetAllCart());
  }

  @Action(AppExtendAction.StartPollingOrder)
  startPollingOrder(ctx: SolidifyStateContext<AppStateModel>, action: AppExtendAction.StartPollingOrder): void {
    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: environment.refreshOrderAvailableIntervalInSecond,
      incrementInterval: true,
      resetIntervalWhenUserMouseEvent: true,
      maximumIntervalRefreshInSecond: environment.refreshOrderAvailableIntervalInSecond * 10,
      filter: () => LocalStorageHelper.getListItem(LocalStorageEnum.orderPending).length > 0,
      actionToDo: () => {
        const listOrderPending = LocalStorageHelper.getListItem(LocalStorageEnum.orderPending);
        const orderList = MemoizedUtil.listSnapshot(this._store, AppCartOrderState);
        listOrderPending.forEach(orderId => {
          let order: Order = undefined;
          if (!isNullOrUndefined(orderList)) {
            order = orderList.find(o => o.resId === orderId);
          }
          if (!isNullOrUndefined(order) && (order.status === Enums.Order.StatusEnum.READY || order.status === Enums.Order.StatusEnum.IN_ERROR)) {
            LocalStorageHelper.removeItemInList(LocalStorageEnum.orderPending, orderId);
            if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.newOrderAvailable)) {
              LocalStorageHelper.addItemInList(LocalStorageEnum.newOrderAvailable, orderId);
            }
            const actionNotification = {
              text: LabelTranslateEnum.see,
              callback: () => this._store.dispatch(new Navigate([RoutesEnum.orderMyOrderDetail, orderId])),
            };
            if (order.status === Enums.Order.StatusEnum.READY) {
              this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("app.notification.order.ready"), undefined, actionNotification);
              const notificationTitle = this._translate.instant(MARK_AS_TRANSLATABLE("app.notification.order.ready")) + ": " + order.name;
              this._nativeNotificationService.sendNotification(notificationTitle);
            } else {
              this._notificationService.showError(MARK_AS_TRANSLATABLE("app.notification.order.inError"), undefined, actionNotification);
            }
          } else {
            ctx.dispatch(new AppCartOrderAction.AddInListById(orderId, true, true));
          }
        });
      },
    }));
  }

  @Action(AppExtendAction.StartPollingNotification)
  startPollingNotification(ctx: SolidifyStateContext<AppStateModel>, action: AppExtendAction.StartPollingNotification): void {
    if (SsrUtil.isServer) {
      return;
    }
    if (ctx.getState().isLoggedIn) {
      this._store.dispatch(new AppExtendAction.UpdateNotificationInbox());
    }

    this.subscribe(PollingHelper.startPollingObs({
      waitingTimeBeforeStartInSecond: environment.refreshNotificationInboxAvailableIntervalInSecond,
      initialIntervalRefreshInSecond: environment.refreshNotificationInboxAvailableIntervalInSecond,
      incrementInterval: true,
      resetIntervalWhenUserMouseEvent: true,
      maximumIntervalRefreshInSecond: environment.refreshNotificationInboxAvailableIntervalInSecond * 10,
      filter: () => ctx.getState().isLoggedIn,
      actionToDo: () => {
        this._store.dispatch(new AppExtendAction.UpdateNotificationInbox());
      },
    }));
  }

  @Action(AppExtendAction.UpdateNotificationInbox)
  updateNotificationInbox(ctx: SolidifyStateContext<AppStateModel>, action: AppExtendAction.UpdateNotificationInbox): Observable<AppNotificationInboxAction.GetAllSuccess> {
    const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo);
    const searchItems = queryParameters.search.searchItems;
    const NOTIFICATION_MARK: keyof NotificationDlcm = "notificationMark";
    MappingObjectUtil.set(searchItems, NOTIFICATION_MARK, Enums.Notification.MarkEnum.UNREAD);

    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new AppNotificationInboxAction.GetAll(queryParameters),
      AppNotificationInboxAction.GetAllSuccess,
      resultAction => {
        if (!CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.notificationInboxPending)) {
          return;
        }
        const listNewNotificationInboxPending = resultAction.list._data.map(newNotification => newNotification.resId);
        const listOldNotificationInboxPending = LocalStorageHelper.getListItem(LocalStorageEnum.notificationInboxPending);
        let counterNewNotification = 0;
        listNewNotificationInboxPending.forEach(newNotificationId => {
          if (!listOldNotificationInboxPending.includes(newNotificationId)) {
            counterNewNotification++;
          }
        });
        if (counterNewNotification > 0) {
          const actionNotification = {
            text: LabelTranslateEnum.see,
            callback: () => this._store.dispatch(new Navigate([RoutesEnum.preservationSpaceNotificationInbox])),
          };
          const translateKeyNotificationInbox = MARK_AS_TRANSLATABLE("app.notification.notificationInbox.newIncoming");
          this._notificationService.showSuccess(translateKeyNotificationInbox, {count: counterNewNotification}, actionNotification);
          this._nativeNotificationService.sendNotificationTranslated(translateKeyNotificationInbox, {count: counterNewNotification});
        }
        LocalStorageHelper.initItemInList(LocalStorageEnum.notificationInboxPending, listNewNotificationInboxPending);
      });
  }

  @Action(AppExtendAction.StartPollingCleanPendingRequestNotification)
  startPollingCleanPendingRequestNotification(ctx: SolidifyStateContext<AppStateModel>, action: AppExtendAction.StartPollingCleanPendingRequestNotification): void {
    this.subscribe(PollingHelper.startPollingObs({
      waitingTimeBeforeStartInSecond: environment.refreshCleanPendingRequestNotificationIntervalInSecond,
      initialIntervalRefreshInSecond: environment.refreshCleanPendingRequestNotificationIntervalInSecond,
      incrementInterval: false,
      resetIntervalWhenUserMouseEvent: false,
      maximumIntervalRefreshInSecond: environment.refreshCleanPendingRequestNotificationIntervalInSecond * 10,
      actionToDo: () => {
        ctx.dispatch(new SharedNotificationAction.CleanPendingRequestNotification());
      },
    }));
  }

  override doLoginSuccess(ctx: SolidifyStateContext<AppStateModel>, action: SolidifyAppAction.LoginSuccess): Token {
    const tokenDecoded = super.doLoginSuccess(ctx, action) as Token;

    ctx.patchState({
      isLoggedIn: true,
      userRoles: tokenDecoded.authorities,
      token: tokenDecoded,
    });

    this.subscribe(StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new AppUserAction.GetCurrentUser(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppUserAction.GetCurrentUserSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppUserAction.GetCurrentUserFail)),
        ],
      },
      {
        action: new AppAuthorizedOrganizationalUnitAction.GetAll(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllFail)),
        ],
      },
      {
        action: new AppMemberOrganizationalUnitAction.GetAll(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppMemberOrganizationalUnitAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppMemberOrganizationalUnitAction.GetAllFail)),
        ],
      },
      {
        action: new AppAuthorizedInstitutionAction.GetAll(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedInstitutionAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedInstitutionAction.GetAllFail)),
        ],
      },
      {
        action: new AppArchiveAclAction.GetAll(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppArchiveAclAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppArchiveAclAction.GetAllFail)),
        ],
      },
      {
        action: new SharedRoleAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedRoleAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedRoleAction.GetAllFail)),
        ],
      },
    ]));

    return tokenDecoded;
  }

  override _treatmentAfterRevokeToken(ctx: SolidifyStateContext<AppStateModel>): void {
    super._treatmentAfterRevokeToken(ctx);
    this._archiveAccessRightService?.refreshCache();
    ctx.patchState({
      ignorePreventLeavePopup: true,
      isLoggedIn: false,
      userRoles: [Enums.UserApplicationRole.UserApplicationRoleEnum.guest],
      token: undefined,
      application_user: {...defaultAppUserStateModel()} as SolidifyAppUserStateModel,
    });
  }
}
