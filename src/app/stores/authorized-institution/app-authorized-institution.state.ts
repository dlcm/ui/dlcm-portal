/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app-authorized-institution.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  AppAuthorizedInstitutionAction,
  appAuthorizedInstitutionNameSpace,
} from "@app/stores/authorized-institution/app-authorized-institution.action";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Institution} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceStateInitValue,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface AppAuthorizedInstitutionStateModel extends ResourceStateModel<Institution> {
  listManagedInstitutions: Institution[];
}

@Injectable()
@State<AppAuthorizedInstitutionStateModel>({
  name: StateEnum.application_authorizedInstitution,
  defaults: {
    ...defaultResourceStateInitValue(),
    listManagedInstitutions: [],
    queryParameters: new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo),
  },
})
export class AppAuthorizedInstitutionState extends ResourceState<AppAuthorizedInstitutionStateModel, Institution> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appAuthorizedInstitutionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminAuthorizedInstitutions;
  }

  @OverrideDefaultAction()
  @Action(AppAuthorizedInstitutionAction.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<AppAuthorizedInstitutionStateModel>, action: AppAuthorizedInstitutionAction.GetAllSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    const listManagedInstitutions = action.list?._data?.filter(i => i.role?.resId === Enums.Role.RoleEnum.MANAGER) ?? [];

    ctx.patchState({
      total: action.list._page?.totalItems,
      list: action.list._data,
      listManagedInstitutions: listManagedInstitutions,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters,
    });
  }
}
