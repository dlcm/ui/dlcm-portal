/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app-authorized-organizational-unit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  AppAuthorizedOrganizationalUnitAction,
  appAuthorizedOrganizationalUnitNameSpace,
} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {environment} from "@environments/environment";
import {OrganizationalUnit} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceStateInitValue,
  MappingObjectUtil,
  NotificationService,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface AppAuthorizedOrganizationalUnitStateModel extends ResourceStateModel<OrganizationalUnit> {
}

const getQueryParameter: () => QueryParameters = () => {
  const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo);
  MappingObjectUtil.set(queryParameters.search.searchItems, "openOnly", "false");
  MappingObjectUtil.set(queryParameters.search.searchItems, "roleSuperiorToVisitor", "true");
  return queryParameters;
};

// Store org unit where current user is authorized to do action with a role (CREATOR, APPROVER, STEWARD, MANAGER)
// If current user have the VISITOR role, the org unit is not present in this state (see app-member-organizational-unit.state.ts for that)
@Injectable()
@State<AppAuthorizedOrganizationalUnitStateModel>({
  name: StateEnum.application_authorizedOrganizationalUnit,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: getQueryParameter(),
  },
})
export class AppAuthorizedOrganizationalUnitState extends ResourceState<AppAuthorizedOrganizationalUnitStateModel, OrganizationalUnit> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appAuthorizedOrganizationalUnitNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminAuthorizedOrganizationalUnits;
  }

  @Action(AppAuthorizedOrganizationalUnitAction.AddOrUpdateInList)
  addOrUpdateInList(ctx: SolidifyStateContext<AppAuthorizedOrganizationalUnitStateModel>, action: AppAuthorizedOrganizationalUnitAction.AddOrUpdateInList): void {
    const orgUnit = action.model;
    const indexExistingOrgUnit = ctx.getState().list?.findIndex(o => o.resId === orgUnit.resId);
    if (indexExistingOrgUnit >= 0) {
      ctx.patchState({
        isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      });
      const simulatedParentAction = new AppAuthorizedOrganizationalUnitAction.AddInListById(orgUnit.resId, true, true);
      ctx.dispatch(new AppAuthorizedOrganizationalUnitAction.AddInListByIdSuccess(simulatedParentAction, orgUnit, indexExistingOrgUnit));
    } else {
      ctx.dispatch(new AppAuthorizedOrganizationalUnitAction.AddInList(orgUnit));
    }
  }
}
