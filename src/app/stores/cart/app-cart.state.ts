/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app-cart.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {LocalStorageEnum} from "@app/shared/enums/local-storage.enum";
import {AppCartAction} from "@app/stores/cart/app-cart.action";
import {AppCartArchiveAction} from "@app/stores/cart/archive/app-cart-archive.action";
import {
  AppCartArchiveState,
  AppCartArchiveStateModel,
} from "@app/stores/cart/archive/app-cart-archive.state";
import {
  AppCartDipState,
  AppCartDipStateModel,
} from "@app/stores/cart/dip/app-cart-dip.state";
import {AppCartOrderAction} from "@app/stores/cart/order/app-cart-order.action";
import {
  AppCartOrderState,
  AppCartOrderStateModel,
} from "@app/stores/cart/order/app-cart-order.state";
import {Enums} from "@enums";
import {HomeStateModel} from "@home/stores/home.state";
import {Order} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  BaseStateModel,
  CookieConsentService,
  CookieConsentUtil,
  CookieType,
  isNullOrUndefined,
  LocalStorageHelper,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  SolidifyStateContext,
} from "solidify-frontend";

export interface AppCartStateModel extends BaseStateModel {
  application_cart_archive: AppCartArchiveStateModel | undefined;
  application_cart_order: AppCartOrderStateModel | undefined;
  application_cart_dip: AppCartDipStateModel | undefined;
}

@Injectable()
@State<AppCartStateModel>({
  name: StateEnum.application_cart,
  defaults: {
    application_cart_archive: undefined,
    application_cart_order: undefined,
    application_cart_dip: undefined,
    isLoadingCounter: 0,
  },
  children: [
    AppCartArchiveState,
    AppCartOrderState,
    AppCartDipState,
  ],
})
export class AppCartState {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _cookieConsentService: CookieConsentService) {
  }

  @Action(AppCartAction.AddToCart)
  addToCart(ctx: SolidifyStateContext<HomeStateModel>, action: AppCartAction.AddToCart): void {
    if (!CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.cart)) {
      this._cookieConsentService.notifyFeatureDisabledBecauseCookieDeclined(CookieType.localStorage, LocalStorageEnum.cart);
      return;
    }
    const numberBefore = LocalStorageHelper.getListItem(LocalStorageEnum.cart).length;
    const numberAfter = LocalStorageHelper.addItemInList(LocalStorageEnum.cart, action.archive.resId).length;
    if (numberBefore < numberAfter) {
      ctx.dispatch(new AppCartArchiveAction.AddInList(action.archive));
      this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("home.archive.cart.notification.addWithSuccessInCart"));
    } else {
      this._notificationService.showInformation(MARK_AS_TRANSLATABLE("home.archive.cart.notification.alreadyInCart"));
    }
  }

  @Action(AppCartAction.RemoveToCart)
  removeToCart(ctx: SolidifyStateContext<HomeStateModel>, action: AppCartAction.RemoveToCart): void {
    LocalStorageHelper.removeItemInList(LocalStorageEnum.cart, action.resId);
    ctx.dispatch(new AppCartArchiveAction.RemoveInListById(action.resId));
  }

  @Action(AppCartAction.RemoveAll)
  removeAll(ctx: SolidifyStateContext<HomeStateModel>, action: AppCartAction.RemoveAll): void {
    LocalStorageHelper.initItemInList(LocalStorageEnum.cart, undefined);
    ctx.dispatch(new AppCartArchiveAction.RemoveAllInList());
  }

  @Action(AppCartAction.GetAllCart)
  getAllCart(ctx: SolidifyStateContext<HomeStateModel>, action: AppCartAction.GetAllCart): void {
    const listArchiveId = LocalStorageHelper.getListItem(LocalStorageEnum.cart);
    ctx.dispatch(new AppCartArchiveAction.GetByListId(listArchiveId));
  }

  @Action(AppCartAction.Submit)
  submit(ctx: SolidifyStateContext<HomeStateModel>, action: AppCartAction.Submit): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const order = {
      name: action.order.name,
      disseminationPolicyId: action.order.disseminationPolicyId,
      organizationalUnitDisseminationPolicyId: action.order.organizationalUnitDisseminationPolicyId,
      queryType: Enums.Order.QueryTypeEnum.SIMPLE,
      query: LocalStorageHelper.getItem(LocalStorageEnum.cart),
      publicOrder: action.order.publicOrder,
    } as Order;
    ctx.dispatch(new AppCartOrderAction.Create({
      model: order,
    }));
  }

  @Action(AppCartAction.SubmitSuccess)
  submitSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: AppCartAction.SubmitSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("app.order.notification.cartSubmit.success"));

    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.orderPending)) {
      LocalStorageHelper.addItemInList(LocalStorageEnum.orderPending, action.orderId);
    }

    ctx.dispatch([
      new Navigate([RoutesEnum.orderMyOrderDetail + urlSeparator + action.orderId]),
      new AppCartAction.Clean(),
    ]);
  }

  @Action(AppCartAction.SubmitFail)
  submitFail(ctx: SolidifyStateContext<HomeStateModel>, action: AppCartAction.SubmitFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    this._notificationService.showError(MARK_AS_TRANSLATABLE("app.order.notification.cartSubmit.fail"));
    if (isNullOrUndefined(action.orderId)) {
      return;
    }
    ctx.dispatch(new Navigate([RoutesEnum.orderMyOrderDetail + urlSeparator + action.orderId]));
  }

  @Action(AppCartAction.Clean)
  clean(ctx: SolidifyStateContext<HomeStateModel>, action: AppCartAction.Clean): void {
    LocalStorageHelper.removeItem(LocalStorageEnum.cart);
    ctx.dispatch(new AppCartArchiveAction.Clean(false));
  }
}
