/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app-cart-dip.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  AppCartDipAction,
  appCartDipActionNameSpace,
} from "@app/stores/cart/dip/app-cart-dip.action";
import {AppCartDipDataFileAction} from "@app/stores/cart/dip/data-file/app-cart-dip-data-file.action";
import {AppCartDipDataFileState} from "@app/stores/cart/dip/data-file/app-cart-dip-data-file.state";
import {environment} from "@environments/environment";
import {Dip} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs/index";
import {
  ApiService,
  defaultResourceStateInitValue,
  DownloadService,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface AppCartDipStateModel extends ResourceStateModel<Dip> {
}

@Injectable()
@State<AppCartDipStateModel>({
  name: StateEnum.application_cart_dip,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
  children: [
    AppCartDipDataFileState,
  ],
})
export class AppCartDipState extends ResourceState<AppCartDipStateModel, Dip> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _httpClient: HttpClient,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appCartDipActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessDip;
  }

  @Action(AppCartDipAction.Download)
  download(ctx: SolidifyStateContext<AppCartDipStateModel>, action: AppCartDipAction.Download): Observable<AppCartDipDataFileAction.GetAllSuccess> {
    const fileName = "dip_" + action.id + ".zip";
    this._notificationService.showInformation(LabelTranslateEnum.fileDownloadStart);

    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(ctx, this._actions$,
      new AppCartDipDataFileAction.GetAll(action.id),
      AppCartDipDataFileAction.GetAllSuccess,
      resultAction => {
        const listDipDataFile = resultAction.list;
        let size = 0;
        listDipDataFile._data.forEach(dipDataFile => {
          size += dipDataFile.fileSize;
        });
        this._downloadService.download(`${this._urlResource}/${action.id}/${ApiActionNameEnum.DL}`, fileName, size);
      });
  }
}
