/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app-cart-dip-data-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  AppCartDipDataFileAction,
  appCartDipDataFileActionNameSpace,
} from "@app/stores/cart/dip/data-file/app-cart-dip-data-file.action";
import {environment} from "@environments/environment";
import {DipDataFile} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  DownloadService,
  NotificationService,
  SolidifyStateContext,
} from "solidify-frontend";

export const defaultAppCartDipDataFileValue: () => AppCartDipDataFileStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
  });

export interface AppCartDipDataFileStateModel extends CompositionStateModel<DipDataFile> {
}

@Injectable()
@State<AppCartDipDataFileStateModel>({
  name: StateEnum.application_cart_dip_dataFile,
  defaults: {
    ...defaultAppCartDipDataFileValue(),
  },
  children: [],
})
export class AppCartDipDataFileState extends CompositionState<AppCartDipDataFileStateModel, DipDataFile> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appCartDipDataFileActionNameSpace,
      resourceName: ApiResourceNameEnum.DATAFILE,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessDip;
  }

  @Action(AppCartDipDataFileAction.Download)
  download(ctx: SolidifyStateContext<AppCartDipDataFileStateModel>, action: AppCartDipDataFileAction.Download): void {
    const url = `${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFile.resId}/${ApiActionNameEnum.DL}`;
    this._downloadService.download(url, action.dataFile.fileName, action.dataFile.fileSize);
  }
}
