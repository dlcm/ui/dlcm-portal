/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app-cart-order.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminOrganizationalUnitStateModel} from "@admin/organizational-unit/stores/admin-organizational-unit.state";
import {Injectable} from "@angular/core";
import {AppCartAction} from "@app/stores/cart/app-cart.action";
import {
  AppCartOrderAction,
  appCartOrderActionNameSpace,
} from "@app/stores/cart/order/app-cart-order.action";
import {environment} from "@environments/environment";
import {
  Order,
  OrderArchive,
} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  LocalStorageHelper,
  NotificationService,
  OverrideDefaultAction,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface AppCartOrderStateModel extends ResourceStateModel<Order> {
}

@Injectable()
@State<AppCartOrderStateModel>({
  name: StateEnum.application_cart_order,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
  children: [],
})
export class AppCartOrderState extends ResourceState<AppCartOrderStateModel, Order> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appCartOrderActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessOrders;
  }

  @Selector()
  static isLoading(state: AppCartOrderStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static currentTitle(state: AppCartOrderStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isLoadingWithDependency(state: AppCartOrderStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: AppCartOrderStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AppCartOrderStateModel): boolean {
    return true;
  }

  @OverrideDefaultAction()
  @Action(AppCartOrderAction.CreateSuccess)
  createSuccess(ctx: SolidifyStateContext<AdminOrganizationalUnitStateModel>, action: AppCartOrderAction.CreateSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.model,
    });

    ctx.dispatch(new AppCartOrderAction.Save(action.model.resId));
  }

  @OverrideDefaultAction()
  @Action(AppCartOrderAction.CreateFail)
  createFail(ctx: SolidifyStateContext<AdminOrganizationalUnitStateModel>, action: AppCartOrderAction.CreateFail): void {
    super.createFail(ctx, action);
    ctx.dispatch(new AppCartAction.SubmitFail(action as any, undefined));
  }

  @Action(AppCartOrderAction.Save)
  save(ctx: SolidifyStateContext<AppCartOrderStateModel>, action: AppCartOrderAction.Save): Observable<OrderArchive> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<OrderArchive>(`${this._urlResource}/${action.resId}/${ApiActionNameEnum.SAVE}`)
      .pipe(
        tap(() => {
          ctx.dispatch(new AppCartOrderAction.SaveSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppCartOrderAction.SaveFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AppCartOrderAction.SaveSuccess)
  saveSuccess(ctx: SolidifyStateContext<AppCartOrderStateModel>, action: AppCartOrderAction.SaveSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new AppCartOrderAction.Submit(action.parentAction.resId));
  }

  @Action(AppCartOrderAction.SaveFail)
  saveFail(ctx: SolidifyStateContext<AppCartOrderStateModel>, action: AppCartOrderAction.SaveFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new AppCartAction.SubmitFail(action as any, action.parentAction.resId));
  }

  @Action(AppCartOrderAction.Submit)
  submit(ctx: SolidifyStateContext<AppCartOrderStateModel>, action: AppCartOrderAction.Submit): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post(`${this._urlResource}/${action.resId}/${ApiActionNameEnum.SUBMIT}`, null)
      .pipe(
        tap(() => {
          ctx.dispatch(new AppCartOrderAction.SubmitSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppCartOrderAction.SubmitFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AppCartOrderAction.SubmitSuccess)
  submitSuccess(ctx: SolidifyStateContext<AppCartOrderStateModel>, action: AppCartOrderAction.SubmitSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new AppCartAction.SubmitSuccess(action as any, action.parentAction.resId));
  }

  @Action(AppCartOrderAction.SubmitFail)
  submitFail(ctx: SolidifyStateContext<AppCartOrderStateModel>, action: AppCartOrderAction.SubmitFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new AppCartAction.SubmitFail(action as any, action.parentAction.resId));
  }

  @OverrideDefaultAction()
  @Action(AppCartOrderAction.AddInListByIdFail)
  addInListByIdFail(ctx: SolidifyStateContext<AppCartOrderStateModel>, action: AppCartOrderAction.AddInListByIdFail): void {
    super.addInListByIdFail(ctx, action);
    LocalStorageHelper.removeItemInList(LocalStorageEnum.orderPending, action.parentAction.resId);
  }
}
