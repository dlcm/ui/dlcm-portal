/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app-cart-archive.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpStatusCode} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {LocalStorageEnum} from "@app/shared/enums/local-storage.enum";
import {
  AppCartArchiveAction,
  appCartArchiveNamespace,
} from "@app/stores/cart/archive/app-cart-archive.action";
import {environment} from "@environments/environment";
import {
  Archive,
  ArchiveMetadata,
} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ArchiveHelper} from "@shared/helpers/archive.helper";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  LocalStorageHelper,
  NotificationService,
  OverrideDefaultAction,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
} from "solidify-frontend";

export interface AppCartArchiveStateModel extends ResourceStateModel<Archive> {
}

@Injectable()
@State<AppCartArchiveStateModel>({
  name: StateEnum.application_cart_archive,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class AppCartArchiveState extends ResourceState<AppCartArchiveStateModel, Archive> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appCartArchiveNamespace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessPublicMetadata;
  }

  @OverrideDefaultAction()
  @Action(AppCartArchiveAction.GetById)
  getById(ctx: SolidifyStateContext<AppCartArchiveStateModel>, action: AppCartArchiveAction.GetById): Observable<Archive> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });

    return this._apiService.getById<ArchiveMetadata>(this._urlResource, action.id)
      .pipe(
        tap((model: any) => {
          ctx.dispatch(new AppCartArchiveAction.GetByIdSuccess(action, ArchiveHelper.adaptArchiveMetadataInArchive(model)));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AppCartArchiveAction.GetByIdFail(action, error));
          throw this._environment.errorToSkipInErrorHandler;
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(AppCartArchiveAction.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<AppCartArchiveStateModel>, action: AppCartArchiveAction.GetByIdFail): void {
    if (action?.error?.status === HttpStatusCode.NotFound) {
      LocalStorageHelper.removeItemInList(LocalStorageEnum.cart, action.parentAction.id);
    }
    super.getByIdFail(ctx, action);
  }

  @OverrideDefaultAction()
  @Action(AppCartArchiveAction.GetByListId)
  getByListId(ctx: SolidifyStateContext<AppCartArchiveStateModel>, action: AppCartArchiveAction.GetByListId): void {
    ctx.patchState({
      list: undefined,
    });
    super.getByListId(ctx, action);
  }

  @OverrideDefaultAction()
  @Action(AppCartArchiveAction.GetByListIdSuccess)
  getByListIdSuccess(ctx: SolidifyStateContext<AppCartArchiveStateModel>, action: AppCartArchiveAction.GetByListIdSuccess): void {
    super.getByListIdSuccess(ctx, action);
    const list = ctx.getState().list;
    ctx.patchState({
      total: list ? list.length : 0,
    });
  }

  @OverrideDefaultAction()
  @Action(AppCartArchiveAction.GetByListIdFail)
  getByListIdFail(ctx: SolidifyStateContext<AppCartArchiveStateModel>, action: AppCartArchiveAction.GetByListIdFail): void {
    const list = ctx.getState().listTemp ?? [];
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      list: list,
      listTemp: undefined,
      total: list ? list.length : 0,
    });
  }

  @Action(AppCartArchiveAction.RemoveAllInList)
  deleteAllInList(ctx: SolidifyStateContext<AppCartArchiveStateModel>, action: AppCartArchiveAction.RemoveAllInList): void {
    ctx.patchState({
      list: [],
      total: 0,
    });
  }
}
