/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app-cart.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Archive,
  Order,
} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
} from "solidify-frontend";

const state = StateEnum.application_cart;

export namespace AppCartAction {
  export class AddToCart {
    static readonly type: string = `[${state}] Add To Cart`;

    constructor(public archive: Archive) {
    }
  }

  export class RemoveToCart {
    static readonly type: string = `[${state}] Remove To Cart`;

    constructor(public resId: string) {
    }
  }

  export class RemoveAll {
    static readonly type: string = `[${state}] Remove All`;
  }

  export class Clean {
    static readonly type: string = `[${state}] Clean`;
  }

  export class GetAllCart {
    static readonly type: string = `[${state}] Get All Cart`;
  }

  export class Submit extends BaseAction {
    static readonly type: string = `[${state}] Submit Cart`;

    constructor(public order: Order) {
      super();
    }
  }

  export class SubmitSuccess extends BaseSubActionSuccess<Submit> {
    static readonly type: string = `[${state}] Submit Success`;

    constructor(public parentAction: Submit, public orderId: string) {
      super(parentAction);
    }
  }

  export class SubmitFail extends BaseSubActionFail<Submit> {
    static readonly type: string = `[${state}] Submit Fail`;

    constructor(public parentAction: Submit, public orderId: string) {
      super(parentAction);
    }
  }
}

export const appCartActionNameSpace = AppCartAction;
