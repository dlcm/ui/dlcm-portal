/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app-member-organizational-unit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {appMemberOrganizationalUnitNameSpace} from "@app/stores/member-organizational-unit/app-member-organizational-unit.action";
import {environment} from "@environments/environment";
import {OrganizationalUnit} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceStateInitValue,
  MappingObjectUtil,
  NotificationService,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
} from "solidify-frontend";

export interface AppMemberOrganizationalUnitStateModel extends ResourceStateModel<OrganizationalUnit> {
}

const getQueryParameter: () => QueryParameters = () => {
  const queryParameters = new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo);
  MappingObjectUtil.set(queryParameters.search.searchItems, "openOnly", "false");
  MappingObjectUtil.set(queryParameters.search.searchItems, "roleSuperiorToVisitor", "false");
  return queryParameters;
};

// Store org unit where current user is member with a role (VISITOR, CREATOR, APPROVER, STEWARD, MANAGER)
@Injectable()
@State<AppMemberOrganizationalUnitStateModel>({
  name: StateEnum.application_memberOrganizationalUnit,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: getQueryParameter(),
  },
})
export class AppMemberOrganizationalUnitState extends ResourceState<AppMemberOrganizationalUnitStateModel, OrganizationalUnit> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: appMemberOrganizationalUnitNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminAuthorizedOrganizationalUnits;
  }
}
