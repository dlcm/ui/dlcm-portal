/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - data-table-component-solidify.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Aip} from "@models";
import {SharedArchiveNameContainer} from "@shared/components/containers/shared-archive-name/shared-archive-name.container";
import {SharedOrganizationalUnitNameContainer} from "@shared/components/containers/shared-organizational-unit-name/shared-organizational-unit-name.container";
import {
  SharedResourceRoleMemberContainer,
  SharedResourceRoleMemberContainerMode,
} from "@shared/components/containers/shared-resource-role-member/shared-resource-role-member-container.component";
import {SharedAccessLevelWithEmbargoPresentational} from "@shared/components/presentationals/shared-access-level-with-embargo/shared-access-level-with-embargo.presentational";
import {SharedAccessLevelPresentational} from "@shared/components/presentationals/shared-access-level/shared-access-level.presentational";
import {SharedAdminSubjectAreaLabelPresentational} from "@shared/components/presentationals/shared-admin-subject-area-label/shared-admin-subject-area-label-presentational.component";
import {SharedAipStatusNamePresentational} from "@shared/components/presentationals/shared-aip-status-name/shared-aip-status-name.presentational";
import {SharedAipStatusOrgunitNamePresentational} from "@shared/components/presentationals/shared-aip-status-orgunit-name/shared-aip-status-orgunit-name.presentational";
import {SharedAipStatusSummaryPresentational} from "@shared/components/presentationals/shared-aip-status-summary/shared-aip-status-summary.presentational";
import {SharedArchivePublicationDatePresentational} from "@shared/components/presentationals/shared-archive-publication-date/shared-archive-publication-date.presentational";
import {SharedComplianceLevelPresentational} from "@shared/components/presentationals/shared-compliance-level/shared-compliance-level.presentational";
import {SharedDataSensitivityPresentational} from "@shared/components/presentationals/shared-data-sensitivity/shared-data-sensitivity.presentational";
import {SharedDatafileQuickStatusPresentational} from "@shared/components/presentationals/shared-datafile-quick-status/shared-datafile-quick-status.presentational";
import {SharedProgressBarPresentational} from "@shared/components/presentationals/shared-progress-bar/shared-progress-bar.presentational";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {UserInfoDataTableColumn} from "@shared/models/user-info-data-table-column.model";
import {
  DataTableComponent,
  DataTableComponentInput,
  EnumUtil,
  isNotNullNorUndefined,
  isNullOrUndefined,
  KeyValue,
  LogoWrapperContainer,
  MappingObject,
  StatusPresentational,
  ValueType,
} from "solidify-frontend";

export const dataTableComponentSolidify: MappingObject<DataTableComponentEnum, DataTableComponent> = {
  [DataTableComponentEnum.status]: {
    componentType: StatusPresentational,
    inputs: [
      {
        key: "statusModel",
        valueType: ValueType.isCallback,
        callbackValue: (rowData, col) => EnumUtil.getKeyValue(col.filterEnum as KeyValue[], DataTableComponentHelper.getData(rowData, col)),
      },
      {
        key: "label",
        valueType: ValueType.isStatic,
        staticValue: undefined,
      },
      {
        key: "takeAllWidth",
        valueType: ValueType.isStatic,
        staticValue: true,
      },
    ],
  } as DataTableComponent,
  [DataTableComponentEnum.logo]: {
    componentType: LogoWrapperContainer,
    inputs: [
      {
        key: "isUser",
        valueType: ValueType.isAttributeOnCol,
        attributeOnCol: "isUser",
      },
      {
        key: "logoNamespace",
        valueType: ValueType.isAttributeOnCol,
        attributeOnCol: "resourceNameSpace",
      },
      {
        key: "logoState",
        valueType: ValueType.isAttributeOnCol,
        attributeOnCol: "resourceState",
      },
      {
        key: "userInfo",
        valueType: ValueType.isRowData,
      },
      {
        key: "idResource",
        valueType: ValueType.isCallback,
        callbackValue: (rowData, col: UserInfoDataTableColumn<any>) => col.idResource(rowData),
      },
      {
        key: "isLogoPresent",
        valueType: ValueType.isCallback,
        callbackValue: (rowData, col: UserInfoDataTableColumn<any>) => col.isLogoPresent(rowData),
      },
    ],
  } as DataTableComponent,
  [DataTableComponentEnum.accessLevel]: {
    componentType: SharedAccessLevelPresentational,
    inputs: [
      {
        key: "accessLevel",
        valueType: ValueType.isColData,
      },
      {
        key: "withTooltip",
        valueType: ValueType.isStatic,
        staticValue: true,
      },
    ],
  },
  [DataTableComponentEnum.aipStatusSummaryPresentational]: {
    componentType: SharedAipStatusSummaryPresentational,
    inputs: [
      {
        key: "aipCopyList",
        valueType: ValueType.isRowData,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.aipStatusNamePresentational]: {
    componentType: SharedAipStatusNamePresentational,
    inputs: [
      {
        key: "aipCopyList",
        valueType: ValueType.isRowData,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.aipStatusOrgUnitPresentational]: {
    componentType: SharedAipStatusOrgunitNamePresentational,
    inputs: [
      {
        key: "aipCopyList",
        valueType: ValueType.isRowData,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.conformityLevelStar]: {
    componentType: SharedComplianceLevelPresentational,
    inputs: [
      {
        key: "center",
        valueType: ValueType.isStatic,
        staticValue: true,
      } as DataTableComponentInput,
      {
        key: "value",
        valueType: ValueType.isColData,
      } as DataTableComponentInput,
      {
        key: "withLabel",
        valueType: ValueType.isStatic,
        staticValue: false,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.dataFileQuickStatus]: {
    componentType: SharedDatafileQuickStatusPresentational,
    inputs: [
      {
        key: "value",
        valueType: ValueType.isColData,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.jobExecutionProgression]: {
    componentType: SharedProgressBarPresentational,
    inputs: [
      {
        key: "value",
        valueType: ValueType.isColData,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.organizationalUnitName]: {
    componentType: SharedOrganizationalUnitNameContainer,
    inputs: [
      {
        key: "id",
        valueType: ValueType.isColData,
      } as DataTableComponentInput,
      {
        key: "displayOverlay",
        valueType: ValueType.isStatic,
        staticValue: true,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.archive]: {
    componentType: SharedArchiveNameContainer,
    inputs: [
      {
        key: "id",
        valueType: ValueType.isColData,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.adminSubjectAreaLabel]: {
    componentType: SharedAdminSubjectAreaLabelPresentational,
    inputs: [
      {
        key: "language",
        valueType: ValueType.isField,
      } as DataTableComponentInput,
      {
        key: "subjectArea",
        valueType: ValueType.isRowData,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.organizationalUnitMember]: {
    componentType: SharedResourceRoleMemberContainer,
    inputs: [
      {
        key: "resource",
        valueType: ValueType.isRowData,
      } as DataTableComponentInput,
      {
        key: "mode",
        valueType: ValueType.isStatic,
        staticValue: SharedResourceRoleMemberContainerMode.organizationalUnit,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.institutionMember]: {
    componentType: SharedResourceRoleMemberContainer,
    inputs: [
      {
        key: "resource",
        valueType: ValueType.isRowData,
      } as DataTableComponentInput,
      {
        key: "mode",
        valueType: ValueType.isStatic,
        staticValue: SharedResourceRoleMemberContainerMode.institution,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.sensitivity]: {
    componentType: SharedDataSensitivityPresentational,
    inputs: [
      {
        key: "dataSensitivity",
        valueType: ValueType.isColData,
      } as DataTableComponentInput,
      {
        key: "withTooltip",
        valueType: ValueType.isStatic,
        staticValue: true,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.archivePublicationDate]: {
    componentType: SharedArchivePublicationDatePresentational,
    inputs: [
      {
        key: "archive",
        valueType: ValueType.isRowData,
      } as DataTableComponentInput,
    ],
  },
  [DataTableComponentEnum.accessLevelWithEmbargo]: {
    componentType: SharedAccessLevelWithEmbargoPresentational,
    inputs: [
      {
        key: "accessLevel",
        valueType: ValueType.isColData,
      } as DataTableComponentInput,
      {
        key: "embargoAccessLevel",
        valueType: ValueType.isCallback,
        callbackValue: (rowData: Aip, col) => rowData?.info?.embargo?.access,
      } as DataTableComponentInput,
      {
        key: "embargoEndDate",
        valueType: ValueType.isCallback,
        callbackValue: (rowData: Aip, col) => {
          const aip = rowData;
          const start = aip?.info?.embargo?.startDate;
          const endDate = aip?.info?.embargo?.["endDate"];
          if (isNotNullNorUndefined((endDate))) {
            return new Date(endDate);
          }
          const months = aip?.info?.embargo?.months;
          if (isNullOrUndefined(start) || isNullOrUndefined(months)) {
            return undefined;
          }
          const startDate = new Date(start);
          return new Date(startDate.setMonth(startDate.getMonth() + months));
        },
      } as DataTableComponentInput,
      {
        key: "withTooltip",
        valueType: ValueType.isStatic,
        staticValue: true,
      } as DataTableComponentInput,
    ],
  },
} as MappingObject<DataTableComponentEnum, DataTableComponent>;
