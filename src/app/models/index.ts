/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - index.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

/* eslint-disable no-restricted-imports */
import {Enums} from "@app/enums";
import {PersonAvatar} from "@app/generated-api/model/person-avatar.partial.model";
import {ArchiveMetadataNamespace} from "@app/shared/models/business/archive-metadata.model";
import {BaseResourceRole} from "@shared/models/base-resource-role.model";
import {
  ApplicationRole as SolidifyApplicationRole,
  BaseRelationResource,
  BaseRelationResourceType,
  BaseResource,
  BaseResourceAvatar,
  BaseResourceLogo,
  BaseResourceWithLabels,
  BaseResourceWithRelationExtended,
  FacetProperty,
  Label as SolidifyLabel,
  MappingObject,
  OverrideType,
  SolidifyDataFileModel,
  SolidifyFile,
  SolidifyFileUploadType,
  SolidifyObject,
  SolidifyRole,
  SystemPropertyPartial as SolidifySystemPropertyPartial,
  User as SolidifyUser,
  UserApplicationRoleEnum,
} from "solidify-frontend";
import {AdditionalFieldsForm as AdditionalFieldsFormPartial} from "../generated-api/model/additional-fields-form.partial.model";
import {AipCopyList as AipCopyListPartial} from "../generated-api/model/aip-copy-list.partial.model";
import {AipCopy as AipCopyPartial} from "../generated-api/model/aip-copy.partial.model";
import {AipDataFile as AipDataFilePartial} from "../generated-api/model/aip-data-file.partial.model";
import {AipStatus as AipStatusPartial} from "../generated-api/model/aip-status.partial.model";
import {ApplicationRole as ApplicationRolePartial} from "../generated-api/model/application-role.partial.model";
import {ArchivalInfoPackage as AipPartial} from "../generated-api/model/archival-info-package.partial.model";
import {ArchiveACL as ArchiveACLPartial} from "../generated-api/model/archive-acl.partial.model";
// import {ArchiveDua as ArchiveDuaPartial} from "../generated-api/model/archive-dua.partial.model";
import {ArchiveMetadata as ArchiveMetadataPartial} from "../generated-api/model/archive-metadata.partial.model";
import {ArchivePublicDataFile as ArchivePublicDataFilePartial} from "../generated-api/model/archive-public-data-file.partial.model";
import {ArchivePublicData as ArchivePublicDataPartial} from "../generated-api/model/archive-public-data.partial.model";
import {ArchiveStatisticsDto as ArchiveStatisticsDtoPartial} from "../generated-api/model/archive-statistics-dto.partial.model";
import {ArchiveStatistics as ArchiveStatisticPartial} from "../generated-api/model/archive-statistics.partial.model";
// import {ArchiveThumbnailLogo as ArchiveThumbnailLogoPartial} from "../generated-api/model/archive-thumbnail-logo.partial.model";
// import {ArchiveThumbnail as ArchiveThumbnailPartial} from "../generated-api/model/archive-thumbnail.partial.model";
import {ArchiveType as ArchiveTypePartial} from "../generated-api/model/archive-type.partial.model";
import {ArchiveUserRating as ArchiveUserRatingPartial} from "../generated-api/model/archive-user-rating.partial.model";
import {AuthUserDto as AuthUserDtoPartial} from "../generated-api/model/auth-user-dto.partial.model";
import {AverageRating as AverageRatingPartial} from "../generated-api/model/average-rating.partial.model";
import {ChangeInfo as ChangeInfoPartial} from "../generated-api/model/change-info.partial.model";
import {ChecksumCheck as AipChecksumCheckPartial} from "../generated-api/model/checksum-check.partial.model";
import {Contributor as ContributorPartial} from "../generated-api/model/contributor.partial.model";
import {DataFileChecksum as DataFileChecksumPartial} from "../generated-api/model/data-file-checksum.partial.model";
import {DepositDataFile as DepositDataFilePartial} from "../generated-api/model/deposit-data-file.partial.model";
import {Deposit as DepositPartial} from "../generated-api/model/deposit.partial.model";
import {DipDataFile as DipDataFilePartial} from "../generated-api/model/dip-data-file.partial.model";
import {DisseminationInfoPackage as DipPartial} from "../generated-api/model/dissemination-info-package.partial.model";
import {DisseminationPolicyDto as DisseminationPolicyDtoPartial} from "../generated-api/model/dissemination-policy-dto.partial.model";
import {DisseminationPolicy as DisseminationPolicyPartial} from "../generated-api/model/dissemination-policy.partial.model";
import {DownloadToken as DownloadTokenPartial} from "../generated-api/model/download-token.partial.model";
import {EmbargoInfo as EmbargoInfoPartial} from "../generated-api/model/embargo-info.partial.model";
import {FacetProperties as FacetPropertiesPartial} from "../generated-api/model/facet-properties.partial.model";
import {FacetResultValue as FacetResultValuePartial} from "../generated-api/model/facet-result-value.partial.model";
import {FacetResult as FacetResultPartial} from "../generated-api/model/facet-result.partial.model";
import {FileFormat as FileFormatPartial} from "../generated-api/model/file-format.partial.model";
import {FileList as FileListPartial} from "../generated-api/model/file-list.partial.model";
import {FundingAgency as FundingAgencyPartial} from "../generated-api/model/funding-agency.partial.model";
import {I18nLink as I18nLinkPartial} from "../generated-api/model/i18n-link.partial.model";
import {Institution as InstitutionPartial} from "../generated-api/model/institution.partial.model";
import {JobExecutionReportLine as JobExecutionReportLinePartial} from "../generated-api/model/job-execution-report-line.partial.model";
import {JobExecutionReport as JobExecutionReportPartial} from "../generated-api/model/job-execution-report.partial.model";
import {JobExecution as JobExecutionPartial} from "../generated-api/model/job-execution.partial.model";
import {JobScheduling as JobSchedulingPartial} from "../generated-api/model/job-scheduling.partial.model";
import {Language as LanguagePartial} from "../generated-api/model/language.partial.model";
import {License as LicensePartial} from "../generated-api/model/license.partial.model";
import {Link as LinkPartial} from "../generated-api/model/link.partial.model";
import {LoginInfo as LoginInfoPartial} from "../generated-api/model/login-info.partial.model";
import {MetadataType as MetadataTypePartial} from "../generated-api/model/metadata-type.partial.model";
import {NotificationType as NotificationTypePartial} from "../generated-api/model/notification-type.partial.model";
import {Notification as NotificationPartial} from "../generated-api/model/notification.partial.model";
import {OAIMetadataPrefix as OaiMetadataPrefixPartial} from "../generated-api/model/oai-metadata-prefix.partial.model";
import {OAISet as OaiSetPartial} from "../generated-api/model/oai-set.partial.model";
import {OrcidWebsiteDTO as OrcidWebsiteDTOPartial} from "../generated-api/model/orcid-website-dto.partial.model";
import {OrderArchive as OrderArchivePartial} from "../generated-api/model/order-archive.partial.model";
import {OrderSubsetItem as OrderSubsetItemPartial} from "../generated-api/model/order-subset-item.partial.model";
import {Order as OrderPartial} from "../generated-api/model/order.partial.model";
import {OrganizationalUnitDisseminationPolicy as OrganizationalUnitDisseminationPolicyPartial} from "../generated-api/model/organizational-unit-dissemination-policy.partial.model";
import {OrganizationalUnit as OrganizationalUnitPartial} from "../generated-api/model/organizational-unit.partial.model";
import {Person as PersonPartial} from "../generated-api/model/person.partial.model";
import {PreservationJob as PreservationJobPartial} from "../generated-api/model/preservation-job.partial.model";
import {PreservationPolicy as PreservationPolicyPartial} from "../generated-api/model/preservation-policy.partial.model";
import {RatingType as RatingTypePartial} from "../generated-api/model/rating-type.partial.model";
import {RepresentationInfo as RepresentationInfoPartial} from "../generated-api/model/representation-info.partial.model";
import {RequestType as RequestTypePartial} from "../generated-api/model/request-type.partial.model";
import {ResumptionTokenType as ResumptionTokenTypePartial} from "../generated-api/model/resumption-token-type.partial.model";
import {Role as RolePartial} from "../generated-api/model/role.partial.model";
import {ScheduledTask as ScheduledTaskPartial} from "../generated-api/model/scheduled-task.partial.model";
import {SearchCriteria as SearchCriteriaPartial} from "../generated-api/model/search-criteria.partial.model";
import {SetType as SetTypePartial} from "../generated-api/model/set-type.partial.model";
import {SipDataFile as SipDataFilePartial} from "../generated-api/model/sip-data-file.partial.model";
import {StatisticsInfo as StatisticsInfoPartial} from "../generated-api/model/statistics-info.partial.model";
import {StoredAIP as StoredAIPPartial} from "../generated-api/model/stored-aip.partial.model";
import {SubjectArea as SubjectAreaPartial} from "../generated-api/model/subject-area.partial.model";
import {SubmissionAgreementFile as SubmissionAgreementFilePartial} from "../generated-api/model/submission-agreement-file.partial.model";
import {SubmissionAgreement as SubmissionAgreementPartial} from "../generated-api/model/submission-agreement.partial.model";
import {SubmissionInfoPackage as SipPartial} from "../generated-api/model/submission-info-package.partial.model";
import {SubmissionPolicy as SubmissionPolicyPartial} from "../generated-api/model/submission-policy.partial.model";
import {SystemProperties as SystemPropertyPartial} from "../generated-api/model/system-properties.partial.model";
import {Tool as ToolPartial} from "../generated-api/model/tool.partial.model";
import {User as UserPartial} from "../generated-api/model/user.partial.model";
import {VirusCheck as AipVirusCheckPartial} from "../generated-api/model/virus-check.partial.model";
import RatingTypeEnum = Enums.Archive.RatingTypeEnum;
import IdentifiersEnum = Enums.IdentifiersEnum;

/* eslint-enable no-restricted-imports */

export type BaseResourceExtended = {
  creation?: ChangeInfo;
  lastUpdate?: ChangeInfo;
  links?: Link[];
} & BaseResource;

export type AdditionalFieldsForm = OverrideType<AdditionalFieldsFormPartial, {
  type?: Enums.FormDescription.TypeEnum;
}> & BaseResourceExtended;

export type AipCopy = OverrideType<AipCopyPartial, {
  aip?: Aip;
  status?: Enums.Package.StatusEnum;
}>;

export type AipCopyList = OverrideType<AipCopyListPartial, {
  copies?: AipCopy[];
  links?: Link[];
}>;

export type AipDataFile = OverrideType<OverrideType<AipDataFilePartial>, DataFile<Aip>>;

export type AipStatus = OverrideType<AipStatusPartial, {
  status?: Enums.Package.StatusEnum;
}> & BaseResource;

export type Aip = OverrideType<OverrideType<AipPartial, Package>, {
  archiveContainer?: Enums.Aip.AipContainerEnum;
  checksumCheck?: ChecksumCheck;
  checksums?: DataFileChecksum[];
  fileFormat?: FileFormat;
  virusCheck?: VirusCheck;
  complianceLevel?: Enums.ComplianceLevel.ComplianceLevelEnum;
}> & BaseResourceExtended;

export type ApplicationRole = OverrideType<ApplicationRolePartial, {
  resId?: UserApplicationRoleEnum;
}> & SolidifyApplicationRole & OverrideType<BaseResourceExtended, {
  resId?: UserApplicationRoleEnum;
}>;

export type ArchiveACL = OverrideType<ArchiveACLPartial, {
  organizationalUnit?: OrganizationalUnit;
  user?: User;
  signedDuaFile?: ArchivePublicDataFile;
  duaFileChange?: SolidifyFileUploadType; // For portal usage
}> & BaseResourceExtended;

export type ArchiveDua = OverrideType<ArchivePublicData> & BaseResourceExtended;

export interface Archive {
  resId?: string;
  currentAccessLevel: Enums.Access.AccessEnum;
  organizationalUnitId: string;
  title: string;
  description: string;
  yearPublicationDate: string;
  retentionDate: string;
  retentionDay: number;
  retentionDuration: string;
  sizeDisplay: string;
  size: number;
  files: string;
  dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum;
  dataSensitivityToTranslate: string;
  accessLevel: Enums.Access.AccessEnum;
  accessLevelToTranslate: string;
  contributors: ArchiveContributor[];
  institutions: ArchiveInstitution[];
  fundingAgencies: ArchiveFundingAgency[];
  license: ArchiveLicense;
  archiveMetadata: ArchiveMetadata;
  keywords: string[];
  withThumbnail: boolean;
  withDua: boolean;
  withDuaFile: boolean;
  duaContentType: string;
  readmeContentType: string;
  dataUsePolicy: Enums.Deposit.DataUsePolicyEnum;
  isCollection: boolean;
  withReadme: boolean;
  ark: string;
}

export interface ArchiveContributor {
  creatorName?: string;
  givenName?: string;
  familyName?: string;
  affiliations?: ArchiveAffiliation[];
  orcid?: string;
}

export interface ArchiveAffiliation {
  name?: string;
  rorIdLink?: string;
}

export interface ArchiveInstitution {
  name?: string;
  identifierType?: string;
  identifierUrl?: string;
  identifier?: string;
}

export interface ArchiveFundingAgency {
  name?: string;
  identifierType?: string;
  identifierUrl?: string;
  identifier?: string;
}

export interface ArchiveLicense {
  name?: string;
  url?: string;
  identifierType?: string;
  identifier?: string;
}

export type ArchiveDataFile = DataFile<Archive>;

export type ArchivePublicData = OverrideType<ArchivePublicDataPartial> & BaseResourceExtended;

export type ArchivePublicDataFile = OverrideType<ArchivePublicDataFilePartial> & BaseResourceExtended;

export type ArchiveMetadata = OverrideType<ArchiveMetadataPartial, {
  currentAccess?: Enums.Access.AccessEnum;
  links?: Link;
  metadata?: ArchiveMetadataNamespace.PublicMetadata;
}>;

export type ArchiveStatistic = OverrideType<ArchiveStatisticPartial> & BaseResourceExtended;

export type ArchiveStatisticsDto = OverrideType<ArchiveStatisticsDtoPartial, {
  statistics?: StatisticsInfo;
  averageRatings: AverageRating[];
  averageRating: AverageRating[];
}>;

export type ArchiveThumbnail = OverrideType<ArchivePublicData, {
  datasetThumbnail?: ArchiveThumbnailLogo;
}> & BaseResourceExtended;

export type ArchiveThumbnailLogo = OverrideType<ArchivePublicDataFilePartial> & BaseResourceExtended;

export type ArchiveType = OverrideType<ArchiveTypePartial, {
  masterType?: ArchiveType;
}> & BaseResourceExtended;

export type ArchiveUserRating = OverrideType<ArchiveUserRatingPartial, {
  user?: User;
  ratingType?: RatingType;
}> & BaseResourceExtended;

export type AuthUserDto = OverrideType<AuthUserDtoPartial> & BaseResource;

export type AverageRating = OverrideType<AverageRatingPartial, {
  ratingType?: Enums.Archive.RatingTypeEnum;
}> & BaseResourceExtended;

export type ChangeInfo = OverrideType<ChangeInfoPartial>;

export type ChecksumCheck = OverrideType<AipChecksumCheckPartial>;

export type Contributor = OverrideType<ContributorPartial, {
  avatar?: PersonAvatar;
}> & UserInfo & BaseResourceAvatar & BaseResourceExtended;

export type DataFile<TPackage extends BaseResource = any> = {
  sourceData?: string;
  status?: Enums.DataFile.StatusEnum;
  statusMessage?: string;
  virusCheck?: VirusCheck;
  smartSize?: string;
  relativeLocation?: string;
  finalData?: string;
  fileSize?: number;
  fileName?: string;
  fileFormat?: FileFormat;
  dataCategory?: Enums.DataFile.DataCategoryAndType.DataCategoryEnum;
  dataType?: Enums.DataFile.DataCategoryAndType.DataTypeEnum;
  metadataType?: MetadataType;
  complianceLevel?: Enums.ComplianceLevel.ComplianceLevelEnum;
  checksums?: DataFileChecksum[];
  infoPackage?: TPackage;
  contentType?: string;
} & SolidifyFile & SolidifyDataFileModel & BaseResourceExtended;

export type DataFileChecksum = OverrideType<DataFileChecksumPartial, {
  checksumAlgo?: Enums.DataFile.Checksum.AlgoEnum;
  checksumType?: Enums.DataFile.Checksum.TypeEnum;
  checksumOrigin: Enums.DataFile.Checksum.OriginEnum;
}>;

export type Deposit = OverrideType<DepositPartial, {
  access?: Enums.Access.AccessEnum;
  dataSensitivity?: Enums.DataSensitivity.DataSensitivityEnum;
  dataUsePolicy?: Enums.Deposit.DataUsePolicyEnum;
  complianceLevel?: Enums.ComplianceLevel.ComplianceLevelEnum;
  embargo?: EmbargoInfo;
  language?: Language;
  archiveType?: ArchiveType;
  metadataVersion: Enums.MetadataVersion.MetadataVersionEnum;
  organizationalUnit?: OrganizationalUnit;
  preservationPolicy?: PreservationPolicy;
  status?: Enums.Deposit.StatusEnum;
  submissionPolicy?: SubmissionPolicy;
  subjectAreas?: SubjectArea[];
  duaFileChange?: SolidifyFileUploadType; // For portal usage
}> & BaseResourceExtended;

export type DepositDataFile = OverrideType<OverrideType<DepositDataFilePartial, {
  searchCriterias?: SearchCriteria[];
  fileStatus?: Enums.DataFile.StatusEnum;
}>, DataFile<Deposit>>;

export type DipDataFile = OverrideType<OverrideType<DipDataFilePartial>, DataFile<Dip>>;

export type Dip = OverrideType<DipPartial, Package> & BaseResource;

export type DisseminationPolicy = OverrideType<DisseminationPolicyPartial, {
  type?: Enums.DisseminationPolicy.TypeEnum;
  downloadFileName?: Enums.DisseminationPolicy.DownloadFileNameEnum;
}> & BaseResourceExtended;

export type DisseminationPolicyDto = OverrideType<DisseminationPolicyDtoPartial, {
  subsetItemList?: OrderSubsetItem[];
}>;

export type OrganizationalUnitDisseminationPolicy = OverrideType<OrganizationalUnitDisseminationPolicyPartial, {
  parameters?: string;
  creation?: ChangeInfo;
  lastUpdate?: ChangeInfo;
}> & BaseRelationResourceType;

export type OrganizationalUnitDisseminationPolicyContainer = OverrideType<DisseminationPolicy, {
  joinResource?: OrganizationalUnitDisseminationPolicy;
  computedName?: string; // name that merge join resource name if present and dissemination policy name
}> & BaseResourceWithRelationExtended<OrganizationalUnitDisseminationPolicy>;

export type DownloadToken = OverrideType<DownloadTokenPartial, {
  user?: User;
  archiveId?: string;
}> & BaseResourceExtended;

export type EmbargoInfo = OverrideType<EmbargoInfoPartial, {
  access?: Enums.Access.AccessEnum;
}>;

export type FacetProperties = OverrideType<FacetPropertiesPartial, {
  labels?: Label[];
}>;

export type FacetResult = OverrideType<FacetResultPartial, {
  values?: FacetResultValue[];
}>;

export type FacetResultValue = OverrideType<FacetResultValuePartial>;

export type FileFormat = OverrideType<FileFormatPartial, {
  tool?: Tool;
}>;

export type FileList = OverrideType<FileListPartial>;

export type FundingAgency = OverrideType<FundingAgencyPartial, {
  organizationalUnits?: OrganizationalUnit[];
  identifiers?: MappingObject<IdentifiersEnum, string>;
  logo?: SolidifyFile;
}> & BaseResourceLogo & BaseResourceExtended;

export type I18nLink = OverrideType<I18nLinkPartial>;

export type Institution = OverrideType<InstitutionPartial, {
  organizationalUnits?: OrganizationalUnit[];
  identifiers?: MappingObject<IdentifiersEnum, string>;
  logo?: SolidifyFile;
  role?: Role;
}> & BaseResourceLogo & BaseResourceExtended;

export type JobExecution = OverrideType<JobExecutionPartial, {
  preservationJob?: PreservationJob;
  status?: Enums.PreservationJob.StatusEnum;
}> & BaseResourceExtended;

export type JobExecutionReport = OverrideType<JobExecutionReportPartial> & BaseResourceExtended;

export type JobExecutionReportLine = OverrideType<JobExecutionReportLinePartial, {
  status?: Enums.PreservationJob.JobExecutionReportLineStatusEnum;
}> & BaseResourceExtended;

export type JobScheduling = OverrideType<JobSchedulingPartial>;

export interface JobType {
  label: string;
  isRecurring: boolean;
}

export type Label = OverrideType<SolidifyLabel, {
  languageCode?: Enums.Language.LanguageEnum;
  text?: string;
}>;

export type Language = OverrideType<LanguagePartial> & BaseResourceExtended;

export type License = OverrideType<LicensePartial, {
  odConformance?: Enums.License.OdConformanceEnum;
  osdConformance?: Enums.License.OsdConformanceEnum;
  status?: Enums.License.StatusEnum;
  logo?: SolidifyFile;
}> & BaseResourceExtended & BaseResourceLogo;

export type Link = OverrideType<LinkPartial>;

export type LoginInfo = OverrideType<LoginInfoPartial>;

export type MetadataType = OverrideType<MetadataTypePartial> & BaseResource;

export type NotificationDlcm = OverrideType<NotificationPartial, {
  recipient?: Person;
  emitter?: User;
  notificationStatus?: Enums.Notification.StatusEnum;
  notificationType?: NotificationType;
  notifiedOrgUnit?: OrganizationalUnit;
  specification?: SolidifyObject;
  managedBy?: SolidifyObject;
  signedDuaFile?: ArchivePublicDataFile;
}> & BaseResourceExtended & BaseResourceLogo;

export type NotificationType = OverrideType<NotificationTypePartial, {
  resId?: Enums.Notification.TypeEnum;
  notifiedOrgUnitRole?: Role;
  notifiedApplicationRole?: ApplicationRole;
  notificationCategory?: Enums.Notification.CategoryEnum;
}> & OverrideType<BaseResourceExtended, {
  resId?: Enums.Notification.TypeEnum;
}>;

export type OaiMetadataPrefix = OverrideType<OaiMetadataPrefixPartial> & BaseResourceExtended;

export type OaiSet = OverrideType<OaiSetPartial, {
  organizationalUnits?: OrganizationalUnit[];
}> & BaseResourceExtended;

export type Order = OverrideType<OrderPartial, {
  metadataVersion?: Enums.MetadataVersion.MetadataVersionEnum;
  queryType?: Enums.Order.QueryTypeEnum;
  status?: Enums.Order.StatusEnum;
  orderStatus?: Enums.Order.StatusEnum;
}> & BaseResourceExtended;

export type OrcidWebsiteDTO = OverrideType<OrcidWebsiteDTOPartial>;

export type OrderArchive = OverrideType<OrderArchivePartial, {
  archive?: Aip;
  metadata?: ArchiveMetadata;
}> & BaseResourceExtended;

export type OrderSubsetItem = OverrideType<OrderSubsetItemPartial, {
  order?: Order;
}> & BaseResourceExtended;

export type OrganizationalUnit = OverrideType<OrganizationalUnitPartial, {
  defaultSubmissionPolicy?: SubmissionPolicy;
  defaultPreservationPolicy?: PreservationPolicy;
  defaultDisseminationPolicy?: DisseminationPolicy; // TODO REMOVE
  submissionPolicies?: SubmissionPolicy[];
  preservationPolicies?: PreservationPolicy[];
  disseminationPolicies?: OrganizationalUnitDisseminationPolicyContainer[];
  fundingAgencies?: FundingAgency[];
  institutions?: Institution[];
  subjectAreas?: SubjectArea[];
  defaultLicense?: License;
  roleFromOrganizationalUnit?: Role | null;
  roleFromInstitution?: Role | null;
  logo?: SolidifyFile;
}> & BaseResourceLogo & BaseResourceRole & BaseResourceExtended;

export type Package = {
  info?: RepresentationInfo;
  packageStatus?: Enums.Package.StatusEnum;
} & BaseResourceExtended;

export type Person = OverrideType<PersonPartial, {
  organizationalUnits?: OrganizationalUnit[];
  institutions?: Institution[];
  notificationTypes?: NotificationType[];
  avatar?: PersonAvatar;
  searchCriterias?: SearchCriteria[];
  orcidLinks?: I18nLink[];
}> & BaseResourceAvatar & BaseResourceExtended;

export type PreservationJob = OverrideType<PreservationJobPartial, {
  scheduling?: JobScheduling;
  lastExecutionStatus?: Enums.PreservationJob.StatusEnum;
  jobRecurrence: Enums.PreservationJob.JobRecurrenceEnum;
  jobType: Enums.PreservationJob.JobTypeEnum;
}> & BaseResourceExtended;

export type PreservationPolicy = OverrideType<PreservationPolicyPartial> & BaseResourceExtended;

export type RatingType = OverrideType<RatingTypePartial, {
  resId?: RatingTypeEnum;
}> & OverrideType<BaseResourceExtended, {
  resId?: RatingTypeEnum;
}>;

export type RepresentationInfo = OverrideType<RepresentationInfoPartial, {
  access?: Enums.Access.AccessEnum;
  dataSensitivity?: Enums.DataSensitivity.DataSensitivityEnum;
  complianceLevel?: Enums.ComplianceLevel.ComplianceLevelEnum;
  metadataVersion?: Enums.MetadataVersion.MetadataVersionEnum;
  status?: Enums.Package.StatusEnum;
  currentAccess?: Enums.Access.AccessEnum;
}>;

export type RequestType = OverrideType<RequestTypePartial, {
  verb?: Enums.RequestType.VerbEnum;
}>;

export type SubjectArea = OverrideType<SubjectAreaPartial, {
  searchCriterias?: SearchCriteria[];
  labels?: Label[];
}> & OverrideType<BaseResourceWithLabels, {
  labels?: Label[];
}> & BaseResourceExtended;

export type ResumptionTokenType = OverrideType<ResumptionTokenTypePartial>;

export type Role = OverrideType<RolePartial> & BaseResourceExtended & SolidifyRole;

export type ScheduledTask = OverrideType<ScheduledTaskPartial, {
  taskType?: Enums.ScheduledTask.TaskTypeEnum;
}> & BaseResourceExtended;

export type SearchCriteria = OverrideType<SearchCriteriaPartial, {
  operationType?: Enums.SearchCriteria.OperationTypeEnum;
}>;

export type SetType = OverrideType<SetTypePartial>;

export type SipDataFile = OverrideType<OverrideType<SipDataFilePartial>, DataFile<Sip>>;

export type StatisticsInfo = OverrideType<StatisticsInfoPartial>;

export type StoredAIP = OverrideType<StoredAIPPartial, Aip> & Aip;

export type SubmissionAgreement = OverrideType<SubmissionAgreementPartial, {
  description?: string;
  title?: string;
  version?: string;
  submissionAgreementFile?: SubmissionAgreementFile;
  fileChange?: SolidifyFileUploadType; // For portal usage
  // fileUploaded: boolean;
  // isFileToDelete: boolean;
}> & BaseResourceExtended;

export type SubmissionAgreementFile = OverrideType<SubmissionAgreementFilePartial>;

export type Sip = OverrideType<OverrideType<SipPartial, Package>, {
  organizationalUnit?: OrganizationalUnit;
  submissionPolicy?: SubmissionPolicy;
}> & BaseResourceExtended;

export type SubmissionPolicy = OverrideType<SubmissionPolicyPartial, {
  submissionAgreementType?: Enums.SubmissionAgreementType.SubmissionAgreementTypeEnum;
  submissionAgreement?: SubmissionAgreement;
}> & BaseResourceExtended;

export type SubmissionAgreementUser = OverrideType<BaseRelationResource, {
  resId?: string; // Not provided, to avoid typing error
  approbationTime?: string;
  user?: User;
  submissionAgreement?: SubmissionAgreement;
  creation?: ChangeInfo;
  lastUpdate?: ChangeInfo;
}>;

export type SystemProperty = OverrideType<SystemPropertyPartial, {
  defaultIdentifierType?: Enums.Deposit.DepositIdentifierTypeEnum;
  searchFacets?: FacetProperty[];
  links?: Link[];
}> & SolidifySystemPropertyPartial;

export type Tool = OverrideType<ToolPartial>;

export type User = OverrideType<UserPartial, {
  searchCriterias?: SearchCriteria[];
  applicationRole?: ApplicationRole;
  person?: Person;
  authUserDto?: AuthUserDto;
  accessToken?: string;
  refreshToken?: string;
}> & BaseResourceExtended & SolidifyUser;

export type UserInfo = {
  resId?: string;
  firstName?: string;
  lastName?: string;
  externalUid?: string;
} & BaseResource;

export type VirusCheck = OverrideType<AipVirusCheckPartial, {
  tool?: Tool;
}>;

export interface SearchCondition {
  type?: Enums.SearchCondition.Type;
  booleanClauseType?: Enums.SearchCondition.BooleanClauseType;
  field?: string;
  fields?: string[];
  value?: string;
  terms?: string[];
  upperValue?: string;
  lowerValue?: string;
  nestedConditions?: SearchCondition[];
}
