/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {HomeRoutingModule} from "@app/features/home/home-routing.module";
import {HomeState} from "@app/features/home/stores/home.state";
import {SharedModule} from "@app/shared/shared.module";
import {HomeFundingAgencyContainer} from "@home/components/containers/home-funding-agency-container/home-funding-agency-container.presentational";
import {HomeInstitutionContainer} from "@home/components/containers/home-institution-container/home-institution-container.presentational";
import {HomeLicenseContainer} from "@home/components/containers/home-license-container/home-license-container.presentational";
import {HomeArchiveClickThroughDuaDownloadDialog} from "@home/components/dialogs/archive-click-through-dua-download/home-archive-click-through-dua-download-dialog.component";
import {HomeArchiveRestrictedRequestAccessDialog} from "@home/components/dialogs/archive-restricted-request-access/home-archive-restricted-request-access-dialog.component";
import {HomeSearchHelpDialog} from "@home/components/dialogs/home-search-help/home-search-help.dialog";
import {HomeArchiveDetailPresentational} from "@home/components/presentationals/home-archive-form/home-archive-detail.presentational";
import {HomeArchiveRatingPresentational} from "@home/components/presentationals/home-archive-rating/home-archive-rating.presentational";
import {HomeDownloadArchiveButtonPresentational} from "@home/components/presentationals/home-download-archive-button/home-download-archive-button.presentational";
import {HomeMainButtonPresentational} from "@home/components/presentationals/home-main-button/home-main-button.presentational";
import {HomeMainPageContentPresentational} from "@home/components/presentationals/home-main-page-content/home-main-page-content.presentational";
import {HomePartnerImagePresentational} from "@home/components/presentationals/home-partner-image/home-partner-image.presentational";
import {HomePartnersPresentational} from "@home/components/presentationals/home-partners/home-partners.presentational";
import {HomeSearchBarPresentational} from "@home/components/presentationals/home-search-bar/home-search-bar.presentational";
import {HomeArchiveCollectionListRoutable} from "@home/components/routables/home-archive-collection-list/home-archive-collection-list.routable";
import {HomeArchiveDetailRoutable} from "@home/components/routables/home-archive-detail/home-archive-detail.routable";
import {HomeArchiveFileDetailRoutable} from "@home/components/routables/home-archive-file-detail/home-archive-file-detail.routable";
import {HomeArchiveFileListRoutable} from "@home/components/routables/home-archive-file-list/home-archive-file-list.routable";
import {HomeArchiveNotFoundRoutable} from "@home/components/routables/home-archive-not-found/home-archive-not-found.routable";
import {HomePageRoutable} from "@home/components/routables/home-page/home-page.routable";
import {HomeSearchRoutable} from "@home/components/routables/home-search/home-search.routable";
import {HomeArchiveCollectionState} from "@home/stores/archive/collection/home-archive-collection.state";
import {HomeArchiveDataFileState} from "@home/stores/archive/data-file/home-archive-data-file.state";
import {HomeArchivePackageState} from "@home/stores/archive/package/home-archive-package.state";
import {HomeArchiveRatingState} from "@home/stores/archive/rating/home-archive-rating.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SolidifyFrontendSearchModule} from "solidify-frontend";

const routables = [
  HomePageRoutable,
  HomeSearchRoutable,
  HomeArchiveDetailRoutable,
  HomeArchiveFileListRoutable,
  HomeArchiveFileDetailRoutable,
  HomeArchiveCollectionListRoutable,
  HomeArchiveNotFoundRoutable,
];
const dialogs = [
  HomeArchiveClickThroughDuaDownloadDialog,
  HomeArchiveRestrictedRequestAccessDialog,
  HomeSearchHelpDialog,
];
const containers = [
  HomeLicenseContainer,
  HomeInstitutionContainer,
  HomeFundingAgencyContainer,
];
const presentationals = [
  HomeSearchBarPresentational,
  HomeArchiveDetailPresentational,
  HomeMainPageContentPresentational,
  HomePartnersPresentational,
  HomePartnerImagePresentational,
  HomeMainButtonPresentational,
  HomeArchiveRatingPresentational,
];
const services = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...presentationals,
    ...dialogs,
  ],
  imports: [
    SharedModule,
    HomeRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      HomeState,
      HomeArchiveCollectionState,
      HomeArchiveDataFileState,
      HomeArchivePackageState,
      HomeArchiveRatingState,
    ]),
    SolidifyFrontendSearchModule,
    HomeDownloadArchiveButtonPresentational,
  ],
  exports: [
    ...routables,
  ],
  providers: [
    ...services,
  ],
})
export class HomeModule {
}
