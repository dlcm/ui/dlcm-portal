/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-detail.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  Output,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  ArchiveMetadataNamespace,
  MetadataEnum,
} from "@app/shared/models/business/archive-metadata.model";
import {MetadataUtil} from "@app/shared/utils/metadata.util";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {OrganizationalUnitDisseminationPolicyIds} from "@home/models/organizational-unit-dissemination-policy-ids.model";
import {ArchiveAccessRightService} from "@home/services/archive-access-right.service";
import {
  Archive,
  ArchiveContributor,
  ArchiveStatisticsDto,
  ArchiveUserRating,
  Person,
  User,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {SharedPersonOverlayPresentational} from "@shared/components/presentationals/shared-person-overlay/shared-person-overlay.presentational";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  HomePageRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {
  ArchiveMetadataPackages,
  MetadataPackagesEnum,
} from "@shared/models/business/archive-metadata-packages.model";
import {KeyValueInfo} from "@shared/models/key-value-info.model";
import {SecurityService} from "@shared/services/security.service";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  ClipboardUtil,
  DateUtil,
  DialogUtil,
  DownloadService,
  EnumUtil,
  FilePreviewDialog,
  FileStatusEnum,
  FileVisualizerHelper,
  FileVisualizerService,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObject,
  NotificationService,
  ObservableUtil,
  RegexUtil,
  SOLIDIFY_CONSTANTS,
  SolidifyDataFileModel,
  Type,
} from "solidify-frontend";

@Component({
  selector: "dlcm-home-archive-detail",
  templateUrl: "./home-archive-detail.presentational.html",
  styleUrls: ["./home-archive-detail.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeArchiveDetailPresentational extends SharedAbstractPresentational implements OnInit, OnChanges {
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;
  readonly disableAccessRequest: boolean = environment.disableAccessRequest;

  private readonly _downloadBS: BehaviorSubject<{ archive: Archive, organizationalUnitDisseminationPolicyIds?: OrganizationalUnitDisseminationPolicyIds } | undefined>
    = new BehaviorSubject<{ archive: Archive, organizationalUnitDisseminationPolicyIds?: OrganizationalUnitDisseminationPolicyIds } | undefined>(undefined);
  @Output("downloadChange")
  readonly downloadObs: Observable<{ archive: Archive, organizationalUnitDisseminationPolicyIds?: OrganizationalUnitDisseminationPolicyIds } | undefined> = ObservableUtil.asObservable(this._downloadBS);

  private readonly _showArchiveBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("showArchiveChange")
  readonly showArchiveObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._showArchiveBS);

  private readonly _addToCartBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("addToCartChange")
  readonly addToCartObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._addToCartBS);

  private readonly _askAccessBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("askAccessChange")
  readonly askAccessObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._askAccessBS);

  private readonly _sendToOrcidBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("sendToOrcidChange")
  readonly sendToOrcidObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._sendToOrcidBS);

  private readonly _navigateBS: BehaviorSubject<Navigate> = new BehaviorSubject<Navigate>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<Navigate> = ObservableUtil.asObservable(this._navigateBS);

  private readonly _rateBS: BehaviorSubject<ArchiveUserRating[]> = new BehaviorSubject<ArchiveUserRating[]>(undefined);
  @Output("rateChange")
  readonly rateObs: Observable<ArchiveUserRating[]> = ObservableUtil.asObservable(this._rateBS);

  private _archive: Archive;

  @Input()
  set archive(value: Archive) {
    this._archive = value;

    this._computeCanPreviewDua();
    this._computeCanPreviewReadme();
    this._computeIsObsolete();

    this.imageArchive = undefined;
    if (isNotNullNorUndefined(this.archive)) {
      this.imageArchive = `${ApiEnum.accessPublicMetadata}/${this.archive.resId}/${ApiActionNameEnum.THUMBNAIL}`;
    }
  }

  get archive(): Archive {
    return this._archive;
  }

  private _computeCanPreviewDua(): void {
    this.duaFileUrl = undefined;
    this.duaDataFile = undefined;
    this.canPreviewDua = false;

    if (this.archive?.withDua && this.archive?.withDuaFile) {
      this.duaFileUrl = `${ApiEnum.accessPublicMetadata}/${this.archive.resId}/${ApiActionNameEnum.DUA}`;
      this.duaDataFile = {
        fileName: environment.duaFileName,
        status: FileStatusEnum.READY,
        mimetype: this.archive.duaContentType,
      };
      this.canPreviewDua = FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
        dataFile: this.duaDataFile,
      });
    }
  }

  private _computeCanPreviewReadme(): void {
    this.readmeFileUrl = undefined;
    this.readmeDataFile = undefined;
    this.canPreviewReadme = false;

    if (this.archive?.withReadme) {
      this.readmeFileUrl = `${ApiEnum.accessPublicMetadata}/${this.archive.resId}/${ApiActionNameEnum.README}`;
      this.readmeDataFile = {
        fileName: environment.readmeFileName,
        status: FileStatusEnum.READY,
        mimetype: this.archive.readmeContentType,
      };
      this.canPreviewReadme = FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
        dataFile: this.readmeDataFile,
      });
    }
  }

  private _computeIsObsolete(): void {
    const listDoi = MetadataUtil.getDoiIsObsoletedBy(this.metadata);
    this.doiOIfNewerVersion = isNotNullNorUndefinedNorEmptyArray(listDoi) ? listDoi[0] : undefined;
  }

  @Input()
  archiveStatistics: ArchiveStatisticsDto;

  @Input()
  archivePackages: ArchiveMetadataPackages;

  @Input()
  listArchiveUserRating: ArchiveUserRating[];

  @Input()
  relativeArchive: Archive[];

  @Input()
  isLoadingPrepareDownload: boolean;

  @Input()
  isLoggedIn: boolean;

  @Input()
  currentUser: User;

  @Input()
  isLoadingArchiveRating: boolean;

  @Input()
  defaultIdentifierType: Enums.Deposit.DepositIdentifierTypeEnum;

  get isContentStructurePublic(): boolean {
    return this.metadata?.[MetadataEnum.aipContentStructurePublic];
  }

  get verifiedOrcid(): boolean {
    return this._securityService.verifiedOrcid();
  }

  get metadata(): ArchiveMetadataNamespace.PublicMetadata | undefined {
    if (isNullOrUndefined(this.archive) || isNullOrUndefined(this.archive.archiveMetadata)) {
      return undefined;
    }
    return this.archive.archiveMetadata.metadata;
  }

  listInfo: Info[];

  imageArchive: string | undefined;
  viewModeList: ViewModeTableEnum = ViewModeTableEnum.tiles;

  duaFileUrl: string | undefined;
  canPreviewDua: boolean;
  duaDataFile: SolidifyDataFileModel;

  readmeFileUrl: string | undefined;
  canPreviewReadme: boolean;
  readmeDataFile: SolidifyDataFileModel;

  doiOIfNewerVersion: string;

  personOverlayComponent: Type<SharedPersonOverlayPresentational> = SharedPersonOverlayPresentational;

  get isContributor(): boolean {
    return this.archive.contributors.findIndex(c => c.orcid === this.currentUser?.person?.orcid) >= 0;
  }

  get displaySendToOrcid(): boolean {
    return this.isLoggedIn
      && this.isContributor
      && this.verifiedOrcid;
  }

  get viewModeListEnum(): typeof ViewModeTableEnum {
    return ViewModeTableEnum;
  }

  get typeInfoEnum(): typeof TypeInfoEnum {
    return TypeInfoEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }

  get dataUsePolicyEnumTranslate(): typeof Enums.Deposit.DataUsePolicyEnumTranslate {
    return Enums.Deposit.DataUsePolicyEnumTranslate;
  }

  get doiLink(): string {
    return environment.doiLink;
  }

  constructor(protected readonly _securityService: SecurityService,
              protected readonly _notificationService: NotificationService,
              readonly archiveAccessRightService: ArchiveAccessRightService,
              private readonly _dialog: MatDialog,
              private readonly _downloadService: DownloadService,
              private readonly _fileVisualizerService: FileVisualizerService) {
    super();
  }

  ngOnInit(): void {
    this._defineListInfo();
  }

  trackByFn(index: number, info: Info): string {
    return index + "";
  }

  private _defineListInfo(): void {
    const ark = {
      labelToTranslate: LabelTranslateEnum.ark,
      value: this.metadata.ark,
      type: TypeInfoEnum.ark,
    };
    const doi = {
      labelToTranslate: LabelTranslateEnum.doi,
      value: MetadataUtil.getDOI(this.metadata),
      type: TypeInfoEnum.doiWithShortDoi,
    };
    this.listInfo = [
      {
        labelToTranslate: LabelTranslateEnum.organizationalUnit,
        value: this.archive.organizationalUnitId,
        type: TypeInfoEnum.orgUnit,
      },
      {
        labelToTranslate: LabelTranslateEnum.type,
        value: MetadataUtil.getType(this.metadata) === "DATASET" ? LabelTranslateEnum.dataset : MetadataUtil.getType(this.metadata),
        type: TypeInfoEnum.translate,
      },
      ...(this.defaultIdentifierType === Enums.Deposit.DepositIdentifierTypeEnum.ARK ? [ark, doi] : [doi, ark]),
      {
        labelToTranslate: LabelTranslateEnum.isIdenticalTo,
        values: MetadataUtil.getDoiIsIdenticalTo(this.metadata),
        type: TypeInfoEnum.doi,
      },
      {
        labelToTranslate: LabelTranslateEnum.isReferencedBy,
        values: MetadataUtil.getDoiIsReferencedBy(this.metadata),
        type: TypeInfoEnum.doi,
      },
      {
        labelToTranslate: LabelTranslateEnum.isObsoletedBy,
        values: MetadataUtil.getDoiIsObsoletedBy(this.metadata),
        type: TypeInfoEnum.doi,
      },
      {
        labelToTranslate: LabelTranslateEnum.embargoAccessLevel,
        value: EnumUtil.getLabel(Enums.Access.AccessEnumTranslate, MetadataUtil.getEmbargoAccessLevel(this.metadata)),
        type: TypeInfoEnum.translate,
      },
      {
        labelToTranslate: LabelTranslateEnum.embargoEndDate,
        value: DateUtil.convertDateToDateString(MetadataUtil.getEmbargoEndDate(this.metadata)),
      },
      {
        labelToTranslate: LabelTranslateEnum.submittedDate,
        value: DateUtil.convertDateToDateTimeString(MetadataUtil.getSubmittedDate(this.metadata)),
      },
      {
        labelToTranslate: LabelTranslateEnum.descriptionOther,
        value: MetadataUtil.getDescriptionOther(this.metadata),
      },
      {
        labelToTranslate: LabelTranslateEnum.descriptionTechnicalInfo,
        value: MetadataUtil.getDescriptionTechnicalInfo(this.metadata),
      },
      {
        labelToTranslate: LabelTranslateEnum.descriptionSeriesInformation,
        value: MetadataUtil.getDescriptionSeriesInformation(this.metadata),
      },
      {
        labelToTranslate: LabelTranslateEnum.descriptionTableOfContents,
        value: MetadataUtil.getDescriptionTableOfContents(this.metadata),
      },
      {
        labelToTranslate: LabelTranslateEnum.descriptionMethods,
        value: MetadataUtil.getDescriptionMethods(this.metadata),
      },
    ];
    this.listInfo = this.listInfo.filter(i => isNonEmptyString(i.value) || isNonEmptyArray(i.values));
  }

  download(archive: Archive, organizationalUnitDisseminationPolicyIds?: OrganizationalUnitDisseminationPolicyIds): void {
    this._downloadBS.next({archive, organizationalUnitDisseminationPolicyIds});
  }

  getMetadataDatacite(): string {
    return MetadataUtil.getDatacite(this.metadata);
  }

  addToCart(archive: Archive): void {
    this._addToCartBS.next(archive);
  }

  askAccess(archive: Archive): void {
    this._askAccessBS.next(archive);
  }

  sendToOrcid(archive: Archive): void {
    this._sendToOrcidBS.next(archive);
  }

  showDetail(archive: Archive): void {
    this._showArchiveBS.next(archive);
  }

  showDetailNavigate(archive: Archive): string {
    return `${RoutesEnum.archives}/${archive.resId}`;
  }

  navigateToFilesList(): void {
    if (this.archive.isCollection) {
      this._navigateBS.next(new Navigate([RoutesEnum.homeDetail, this.archive.resId, HomePageRoutesEnum.collections]));
    } else {
      this._navigateBS.next(new Navigate([RoutesEnum.homeDetail, this.archive.resId, HomePageRoutesEnum.files]));
    }
  }

  getMetadataVersion(archivePackages: ArchiveMetadataPackages): string {
    return archivePackages.metadata[MetadataPackagesEnum.metadataVersion];
  }

  getRetentionExpiration(archivePackages: ArchiveMetadataPackages): Date {
    const retentionEnd = archivePackages.metadata[MetadataPackagesEnum.aipRetentionEnd];
    return DateUtil.convertOffsetDateTimeIso8601ToDate(retentionEnd);
  }

  getDisposalApproval(archivePackages: ArchiveMetadataPackages): string {
    return archivePackages.metadata[MetadataPackagesEnum.aipDispositionApproval] ? LabelTranslateEnum.yes : LabelTranslateEnum.no;
  }

  getCreationDate(archivePackages: ArchiveMetadataPackages): Date {
    const creation = archivePackages.metadata[MetadataPackagesEnum.creation];
    return DateUtil.convertOffsetDateTimeIso8601ToDate(creation);
  }

  copyToClipboard(id: string): void {
    ClipboardUtil.copyStringToClipboard(id);
    this._notificationService.showInformation(LabelTranslateEnum.idCopyToClipboard);
  }

  getDepositCreationDate(archivePackages: ArchiveMetadataPackages): Date {
    const creation = archivePackages.metadata.packages.deposits.events.creation?.date;
    return DateUtil.convertOffsetDateTimeIso8601ToDate(creation);
  }

  getDepositApprobationDate(archivePackages: ArchiveMetadataPackages): Date {
    const approval = archivePackages.metadata.packages.deposits.events?.appraisal?.date;
    return DateUtil.convertOffsetDateTimeIso8601ToDate(approval);
  }

  getSensitivityTooltip(dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum): string {
    return (EnumUtil.getKeyValue(Enums.DataSensitivity.DataSensitivityEnumTranslate, dataSensitivity) as KeyValueInfo)?.infoToTranslate;
  }

  computeLink(text: string): string {
    if (isNullOrUndefined(text)) {
      return text;
    }
    return text.replace(RegexUtil.urlInComplexText, match => `<a href="${match}" target="_blank" class="no-hover-animation">${match}</a>`);
  }

  updateRate($event: ArchiveUserRating[]): void {
    this._rateBS.next($event);
  }

  readonly CONTRIBUTOR_SEPARATOR: string = ", ";

  getContributorOverlayData(contributor: ArchiveContributor): Person {
    const fullName = contributor.creatorName;
    const indexOfSpace = fullName.indexOf(this.CONTRIBUTOR_SEPARATOR);
    const lastName = indexOfSpace === -1 ? fullName : fullName.substring(0, indexOfSpace);
    const firstName = indexOfSpace === -1 ? "" : fullName.substring(indexOfSpace + this.CONTRIBUTOR_SEPARATOR.length);
    return {
      firstName: firstName,
      lastName: lastName,
      orcid: contributor.orcid,
      institutions: contributor.affiliations.map(a => ({
        name: a.name,
        rorId: isNotNullNorUndefinedNorWhiteString(a.rorIdLink) ? a.rorIdLink.substring(a.rorIdLink.lastIndexOf(SOLIDIFY_CONSTANTS.URL_SEPARATOR) + 1) : undefined,
        identifiers: {
          [Enums.IdentifiersEnum.ROR]: a.rorIdLink,
        } as MappingObject<Enums.IdentifiersEnum, string>,
      })),
    };
  }

  previewDuaFile(): void {
    DialogUtil.open(this._dialog, FilePreviewDialog, {
        fileInput: {
          dataFile: this.duaDataFile,
        },
        fileDownloadUrl: this.duaFileUrl,
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      });
  }

  downloadDuaFile(): void {
    this.subscribe(this._downloadService.downloadInMemory(this.duaFileUrl, this.duaDataFile.fileName, true, this.duaDataFile.mimetype));
  }

  previewReadmeFile(): void {
    DialogUtil.open(this._dialog, FilePreviewDialog, {
        fileInput: {
          dataFile: this.readmeDataFile,
        },
        fileDownloadUrl: this.readmeFileUrl,
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      });
  }

  downloadReadmeFile(): void {
    this.subscribe(this._downloadService.downloadInMemory(this.readmeFileUrl, this.readmeDataFile.fileName, true, this.readmeDataFile.mimetype));
  }

  get depositIdentifierTypeEnum(): typeof Enums.Deposit.DepositIdentifierTypeEnum {
    return Enums.Deposit.DepositIdentifierTypeEnum;
  }
}

class Info {
  labelToTranslate: string;
  value?: string;
  values?: string[];
  code?: boolean = false;
  type?: TypeInfoEnum;
}

enum TypeInfoEnum {
  doi = 1,
  doiWithShortDoi,
  ark,
  orgUnit,
  translate,
}
