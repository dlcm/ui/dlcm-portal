/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-rating.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
} from "@angular/forms";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Enums} from "@enums";
import {
  ArchiveStatisticsDto,
  ArchiveUserRating,
  AverageRating,
} from "@models";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  isNullOrUndefined,
  ObservableUtil,
  PropertyName,
} from "solidify-frontend";

@Component({
  selector: "dlcm-home-archive-rating",
  templateUrl: "./home-archive-rating.presentational.html",
  styleUrls: ["./home-archive-rating.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeArchiveRatingPresentational extends SharedAbstractPresentational implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  @Input()
  archiveStatistics: ArchiveStatisticsDto;

  private _listArchiveUserRating: ArchiveUserRating[];

  @Input()
  set listArchiveUserRating(value: ArchiveUserRating[]) {
    this._listArchiveUserRating = value;

    this.listArchiveUserRating?.forEach(archiveUserRating => {
      switch (archiveUserRating.ratingType?.resId) {
        case Enums.Archive.RatingTypeEnum.QUALITY:
          this.formRatingQuality = this._createFormGroupRating(Enums.Archive.RatingTypeEnum.QUALITY, archiveUserRating);
          break;
        case Enums.Archive.RatingTypeEnum.USEFULNESS:
          this.formRatingUserfulness = this._createFormGroupRating(Enums.Archive.RatingTypeEnum.USEFULNESS, archiveUserRating);
          break;
        default:
          break;
      }
    });
  }

  get listArchiveUserRating(): ArchiveUserRating[] {
    return this._listArchiveUserRating;
  }

  @Input()
  archiveId: string;

  @Input()
  isLoggedIn: boolean;

  @Input()
  isRatingEditableMode: boolean = false;

  @Input()
  isLoading: boolean;

  private readonly _rateBS: BehaviorSubject<ArchiveUserRating[]> = new BehaviorSubject<ArchiveUserRating[]>(undefined);
  @Output("rateChange")
  readonly rateObs: Observable<ArchiveUserRating[]> = ObservableUtil.asObservable(this._rateBS);

  formRatingQuality: FormGroup;
  formRatingUserfulness: FormGroup;

  constructor(private readonly _fb: FormBuilder,
              private readonly _cd: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._initRatingIfNotDefined();
  }

  private _initRatingIfNotDefined(): void {
    if (isNullOrUndefined(this.formRatingQuality)) {
      this.formRatingQuality = this._createFormWithValues(Enums.Archive.RatingTypeEnum.QUALITY);
    }
    if (isNullOrUndefined(this.formRatingUserfulness)) {
      this.formRatingUserfulness = this._createFormWithValues(Enums.Archive.RatingTypeEnum.USEFULNESS);
    }
  }

  private _createFormWithValues(ratingType: Enums.Archive.RatingTypeEnum, grade: number = undefined, resId: string = undefined): FormGroup {
    return this._fb.group({
      [this.formDefinition.ratingType]: [ratingType, []],
      [this.formDefinition.grade]: [isNullOrUndefined(grade) ? 0 : grade, []],
      [this.formDefinition.resId]: [resId],
    });
  }

  private _createFormGroupRating(ratingType: Enums.Archive.RatingTypeEnum, archiveUserRating: ArchiveUserRating = undefined): FormGroup {
    return this._createFormWithValues(ratingType, isNullOrUndefined(archiveUserRating) ? null : Number(archiveUserRating.grade), archiveUserRating?.resId);
  }

  labelToTranslate(ratingType: string): string {
    return this.enumUtil.getLabel(Enums.Archive.RatingTypeEnumTranslate, ratingType);
  }

  submitRating(): void {
    const list: ArchiveUserRating[] = [];
    if (!this.formRatingQuality.pristine) {
      list.push(this._createRating(this.formRatingQuality));
    }
    if (!this.formRatingUserfulness.pristine) {
      list.push(this._createRating(this.formRatingUserfulness));
    }
    if (list.length > 0) {
      this._rateBS.next(list);
    }
    this.leaveRatingMode();
  }

  private _createRating(form: FormGroup): ArchiveUserRating {
    return {
      grade: form.get(this.formDefinition.grade).value,
      ratingType: {
        resId: form.get(this.formDefinition.ratingType).value,
      },
      resId: form.get(this.formDefinition.resId).value,
      archiveId: this.archiveId,
    };
  }

  enterInRatingMode(): void {
    this.isRatingEditableMode = true;
  }

  leaveRatingMode(): void {
    this.isRatingEditableMode = false;
  }

  getFormGroupRating(averageRating: AverageRating): FormGroup | undefined {
    switch (averageRating.ratingType) {
      case Enums.Archive.RatingTypeEnum.QUALITY:
        return this.formRatingQuality;
      case Enums.Archive.RatingTypeEnum.USEFULNESS:
        return this.formRatingUserfulness;
      default:
        return undefined;
    }
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() ratingType: string;
  @PropertyName() grade: string;
  @PropertyName() resId: string;
}
