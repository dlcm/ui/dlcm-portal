/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-download-archive-button.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  Output,
} from "@angular/core";
import {DLCM_CONSTANTS} from "@app/constants";
import {OrganizationalUnitDisseminationPolicyIds} from "@home/models/organizational-unit-dissemination-policy-ids.model";
import {Archive} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {SharedMultiActionButtonPresentational} from "@shared/components/presentationals/shared-multi-action-button/shared-multi-action-button.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {KeyValueIconPlaceholder} from "@shared/models/key-value-placeholder.model";
import {ArchiveDisseminationPolicyService} from "@shared/services/archive-dissemination-policy.service";
import {SecurityService} from "@shared/services/security.service";
import {KeyValueOrgUnitDisseminationPolicyType} from "@shared/types/key-value-org-unit-dissemination-policy.type";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {tap} from "rxjs/operators";
import {
  ExtraButtonToolbar,
  isNullOrUndefinedOrWhiteString,
  ObservableUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-home-download-archive-button",
  templateUrl: "./home-download-archive-button.presentational.html",
  styleUrls: ["./home-download-archive-button.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [
    SharedMultiActionButtonPresentational,
  ],
})
export class HomeDownloadArchiveButtonPresentational extends SharedAbstractPresentational {
  private _numberSelection: number;

  @Input()
  set numberSelection(value: number) {
    this._numberSelection = value;
    this._computeMainButton();
  }

  get numberSelection(): number {
    return this._numberSelection;
  }

  private _disabled: boolean;

  @Input()
  set disabled(value: boolean) {
    this._disabled = value;
    this._computeMainButton();
  }

  get disabled(): boolean {
    return this._disabled;
  }

  private _archive: Archive;

  @Input()
  set archive(value: Archive) {
    this._archive = value;
    this._computeSubActions();
  }

  get archive(): Archive {
    return this._archive;
  }

  private readonly _downloadBS: BehaviorSubject<OrganizationalUnitDisseminationPolicyIds> = new BehaviorSubject<OrganizationalUnitDisseminationPolicyIds>(undefined);
  @Output("downloadChange")
  readonly downloadObs: Observable<OrganizationalUnitDisseminationPolicyIds> = ObservableUtil.asObservable(this._downloadBS);

  mainButton: ExtraButtonToolbar<undefined>;
  listSubActions: ExtraButtonToolbar<Archive | undefined>[] = [];

  constructor(private readonly _changeDetector: ChangeDetectorRef,
              private readonly _translateService: TranslateService,
              private readonly _archiveDisseminationPolicyService: ArchiveDisseminationPolicyService,
              private readonly _securityService: SecurityService) {
    super();
    this._computeMainButton();
    this._computeSubActions();
  }

  private _computeMainButton(): void {
    this.mainButton = {
      callback: current => this._downloadBS.next({disseminationPolicyId: DLCM_CONSTANTS.DISSEMINATION_POLICY_BASIC_ID}),
      labelToTranslate: () => this.numberSelection > 0 ? LabelTranslateEnum.downloadXFiles : LabelTranslateEnum.downloadArchive,
      labelParams: {
        files: this.numberSelection,
      },
      disableCondition: () => this.disabled,
      color: "primary",
      icon: IconNameEnum.download,
      typeButton: "flat-button",
    };
  }

  private _computeSubActions(): void {
    this.listSubActions = this._convertKeyValueListToButtonList(this._archiveDisseminationPolicyService.LIST_DEFAULT_DISSEMINATION_POLICIES_OPTIONS);
    if (isNullOrUndefinedOrWhiteString(this.archive?.organizationalUnitId) || !this._securityService.isLoggedIn()) {
      return;
    }
    this.subscribe(this._archiveDisseminationPolicyService.getListDisseminationPolicyOptionsForOrganizationalUnitId(this.archive.organizationalUnitId)
      .pipe(
        tap(listOptionsDisseminationPolicy => {
          this.listSubActions = this._convertKeyValueListToButtonList(listOptionsDisseminationPolicy);
          this._changeDetector.detectChanges();
        }),
      ));
  }

  private _convertKeyValueListToButtonList(listKeyValue: KeyValueIconPlaceholder[]): ExtraButtonToolbar<Archive | undefined>[] {
    return listKeyValue
      .map(keyValue => this._convertKeyValueToButton(keyValue));
  }

  private _convertKeyValueToButton(keyValue: KeyValueOrgUnitDisseminationPolicyType): ExtraButtonToolbar<Archive | undefined> {
    return {
      callback: current => this._downloadBS.next({
        disseminationPolicyId: keyValue.key,
        organizationalUnitDisseminationPolicyId: keyValue.object?.joinResource?.compositeKey,
      }),
      labelToTranslate: () => LabelTranslateEnum.formatX,
      labelParams: {
        format: this._translateService.instant(keyValue.value),
      },
      icon: keyValue.icon,
      tooltipToTranslate: keyValue.placeholderToTranslate,
      color: undefined,
      typeButton: undefined,
    };
  }
}

