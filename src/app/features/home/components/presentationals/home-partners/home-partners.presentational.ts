/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-partners.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {SharedAbstractPresentational} from "@shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {ThemeEnum} from "@shared/enums/theme.enum";
import {isNotNullNorUndefined} from "solidify-frontend";

@Component({
  selector: "dlcm-home-partners",
  templateUrl: "./home-partners.presentational.html",
  styleUrls: ["./home-partners.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomePartnersPresentational extends SharedAbstractPresentational {
  @Input()
  currentTheme: ThemeEnum;

  private readonly _unigeImage: ImageInfos = {
    imageName: "UNIGE.png",
    altText: "UNIGE",
    url: "https://www.unige.ch/",
  };

  listFundingOrg: ImageInfos[] = [
    {
      imageName: "canton-GE.png",
      altText: "Canton Genève",
      url: "https://www.ge.ch/",
    },
    {
      imageName: "swiss-universities.png",
      altText: "swiss-universities",
      url: "https://www.swissuniversities.ch/",
    },
  ];

  listDeveloppedBy: ImageInfos[] = [
    this._unigeImage,
  ];

  listProjectMembers: ImageInfos[] = [
    this._unigeImage,
    {
      imageName: "hes-so.png",
      altText: "HES-SO",
      url: "https://www.hes-so.ch/",
    },
    {
      imageName: "heg.png",
      altText: "HEG",
      url: "https://www.hesge.ch/heg/",
    },
    {
      imageName: "zhaw.png",
      altText: "ZHAW",
      url: "https://www.zhaw.ch/",
    },
  ];

  listPartners: ImageInfos[] = [
    {
      imageName: "ethz.png",
      altText: "ETHZ",
      url: "https://ethz.ch/",
    },
    {
      imageName: "EPFL.png",
      altText: "EPFL",
      url: "https://www.epfl.ch/",
    },
    {
      imageName: "cern.png",
      altText: "CERN",
      url: "",
    },
    {
      imageName: "berner_fachhorhschule.png",
      altText: "Berner Fachhorhschule",
      url: "https://home.cern/",
    },
    {
      imageName: "cnrs.png",
      altText: "CNRS",
      url: "http://www.cnrs.fr/",
    },
    {
      imageName: "confederation_suisse.png",
      altText: "Confederation suisse",
      url: "https://www.admin.ch/",
    },
    {
      imageName: "cscs.png",
      altText: "CSCS",
      url: "https://www.cscs.ch/",
    },
    {
      imageName: "dcc.png",
      altText: "DCC",
      url: undefined,
    },
    {
      imageName: "enhancer.png",
      altText: "Enhancer",
      url: "https://www.enhancer.ch/",
    },
    {
      imageName: "fnsnf.png",
      altText: "FNSNF",
      url: "http://www.snf.ch/",
    },
    {
      imageName: "genohm.png",
      altText: "Genohm",
      url: "https://www.genohm.com/",
    },
    {
      imageName: "sdsc.png",
      altText: "SDSC",
      url: "https://datascience.ch/",
    },
    {
      imageName: "standforduniversity.png",
      altText: "Stanford University",
      url: "https://www.stanford.edu/",
    },
    {
      imageName: "swissbib.png",
      altText: "swissbib",
      url: "https://www.swissbib.ch/",
    },
    {
      imageName: "train2dacar.png",
      altText: "Train2Dacar",
      url: undefined,
    },
    {
      imageName: "ulb.png",
      altText: "ULB",
      url: "https://www.ulb.be/",
    },
    {
      imageName: "university_bern.png",
      altText: "University Bern",
      url: "https://www.unibe.ch/",
    },
    {
      imageName: "universityofcambridge.png",
      altText: "University of Canbridge",
      url: "https://www.cam.ac.uk/",
    },
  ];

  get themeEnum(): typeof ThemeEnum {
    return ThemeEnum;
  }

  preventDefault($event: MouseEvent, url: string): void {
    if (isNotNullNorUndefined(url)) {
      return;
    }
    $event.preventDefault();
  }
}

interface ImageInfos {
  imageName: string;
  altText: string;
  url: string;
}
