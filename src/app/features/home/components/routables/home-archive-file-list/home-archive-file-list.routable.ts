/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-file-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {DepositDataFileHelper} from "@deposit/helpers/deposit-data-file.helper";
import {Enums} from "@enums";
import {ArchiveHelper} from "@home/helpers/archive.helper";
import {OrganizationalUnitDisseminationPolicyIds} from "@home/models/organizational-unit-dissemination-policy-ids.model";
import {ArchiveAccessRightService} from "@home/services/archive-access-right.service";
import {HomeArchiveDataFileAction} from "@home/stores/archive/data-file/home-archive-data-file.action";
import {HomeArchiveDataFileState} from "@home/stores/archive/data-file/home-archive-data-file.state";
import {HomeState} from "@home/stores/home.state";
import {
  Archive,
  ArchiveDataFile,
  Deposit,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedFileAndAipInformationContainer} from "@shared/components/containers/shared-file-and-aip-information/shared-file-and-aip-information.container";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {FileViewModeEnum} from "@shared/enums/file-view-mode.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  HomePageRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {LocalStateModel} from "@shared/models/local-state.model";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  ArrayUtil,
  DataTableActions,
  DataTableBulkActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DataTablePresentational,
  isNullOrUndefinedOrEmptyArray,
  isUndefined,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
} from "solidify-frontend";

@Component({
  selector: "dlcm-home-archive-file-list-routable",
  templateUrl: "./home-archive-file-list.routable.html",
  styleUrls: ["./home-archive-file-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeArchiveFileListRoutable extends SharedAbstractRoutable implements OnInit, OnDestroy {
  private _archiveId: string;
  private readonly _KEY_QUERY_PARAMETERS: keyof ArchiveDataFile = "status";
  isLoadingDataFileObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeArchiveDataFileState);
  listDataFileObs: Observable<ArchiveDataFile[]> = MemoizedUtil.select(this._store, HomeArchiveDataFileState, state => state.list);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, HomeArchiveDataFileState, state => state.queryParameters);
  intermediateFoldersObs: Observable<string[]> = MemoizedUtil.select(this._store, HomeArchiveDataFileState, state => state.intermediateFolders);
  foldersWithIntermediateFoldersObs: Observable<string[]> = MemoizedUtil.select(this._store, HomeArchiveDataFileState, state => state.foldersWithIntermediateFolders);
  currentFolderObs: Observable<string> = MemoizedUtil.select(this._store, HomeArchiveDataFileState, state => state.currentFolder);

  get currentFolder(): string {
    return MemoizedUtil.selectSnapshot(this._store, HomeArchiveDataFileState, state => state.currentFolder);
  }

  listSelection: Selection[] = [];

  @ViewChild("dataTablePresentational")
  readonly dataTablePresentational: DataTablePresentational<ArchiveDataFile>;

  currentFileViewMode: FileViewModeEnum = FileViewModeEnum.FolderView;

  columns: DataTableColumns<ArchiveDataFile>[];

  actions: DataTableActions<ArchiveDataFile>[] = [
    {
      logo: IconNameEnum.download,
      callback: (archiveDataFile: ArchiveDataFile) => this._downloadDataFile(archiveDataFile),
      placeholder: current => LabelTranslateEnum.download,
      displayOnCondition: current => this.haveRightToDownload,
    },
  ];

  bulkActions: DataTableBulkActions<ArchiveDataFile>[] = [];

  columnsToSkippedFilter: keyof ArchiveDataFile[] | string[] = [
    DepositDataFileHelper.RELATIVE_LOCATION,
    this._KEY_QUERY_PARAMETERS,
  ];

  dialogDataFileRef: MatDialogRef<SharedFileAndAipInformationContainer<ArchiveDataFile>>;

  get fileViewModeEnum(): typeof FileViewModeEnum {
    return FileViewModeEnum;
  }

  get archive(): Archive {
    return MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.current);
  }

  get haveRightToDownloadObs(): Observable<boolean> {
    return this._archiveAccessRightService.isDownloadAuthorizedByIdObs(this._archiveId);
  }

  get haveRightToDownload(): boolean {
    return this._archiveAccessRightService.isDownloadAuthorizedByIdSnapshot(this._archiveId);
  }

  readonly KEY_PARAM_NAME: keyof Deposit & string = undefined;
  isInDisplayArchiveDataFileListMode: boolean = false;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _securityService: SecurityService,
              protected readonly _archiveAccessRightService: ArchiveAccessRightService) {
    super();

    this.columns = [
      {
        field: "fileName",
        header: LabelTranslateEnum.fileName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.ascending,
        filterableField: "file" as any,
        isSortable: false,
        isFilterable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
      },
      {
        field: "complianceLevel",
        header: LabelTranslateEnum.complianceLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        sortableField: "dataFile.complianceLevel" as any,
        filterableField: "dataFile.complianceLevel" as any,
        isSortable: false,
        isFilterable: false,
        translate: true,
        width: "185px",
        component: DataTableComponentHelper.get(DataTableComponentEnum.conformityLevelStar),
        filterEnum: Enums.ComplianceLevel.ComplianceLevelEnumTranslate,
        alignment: "center",
      },
      {
        field: "smartSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        sortableField: "dataFile.fileSize" as any,
        filterableField: "dataFile.fileSize" as any,
        isSortable: false,
        isFilterable: false,
        translate: true,
        alignment: "right",
        width: "100px",
      },
    ];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._getCurrentModelOnParent();

    this.subscribe(this._store.select((s: LocalStateModel) => s.router.state.url)
      .pipe(
        distinctUntilChanged(),
        tap(url => {
          this.isInDisplayArchiveDataFileListMode = url.endsWith(HomePageRoutesEnum.files);
          this._changeDetector.detectChanges();
        }),
      ));
  }

  private _getCurrentModelOnParent(): void {
    this._archiveId = this._route.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._getArchiveById(this._archiveId);
  }

  private _getArchiveById(id: string): void {
    this.getSubResourceWithParentId(id);
  }

  getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new HomeArchiveDataFileAction.GetAll(id, undefined, true));
    this._store.dispatch(new HomeArchiveDataFileAction.GetListFolder(id));
  }

  onQueryParametersEvent(queryParameters: QueryParameters, withRefresh: boolean = true): void {
    this._store.dispatch(new HomeArchiveDataFileAction.ChangeQueryParameters(this._archiveId, queryParameters, true, withRefresh));
  }

  selectFolder(folderFullName: string | undefined): void {
    if (isUndefined(folderFullName)) {
      this.currentFileViewMode = FileViewModeEnum.FlatView;
    } else {
      this.currentFileViewMode = FileViewModeEnum.FolderView;
    }
    this._store.dispatch(new HomeArchiveDataFileAction.ChangeCurrentFolder(folderFullName, true));
    this.isDataTableFirstDisplay = true;
    this.computeListSelectionOnCurrentFolder();
  }

  showDetail(archiveDataFile: ArchiveDataFile): void {
    this._store.dispatch(new Navigate([RoutesEnum.homeDetail, this._archiveId, HomePageRoutesEnum.files, archiveDataFile.resId]));
  }

  back(): void {
    this._store.dispatch(new Navigate([RoutesEnum.archives, this._archiveId]));
  }

  downloadArchive(organizationalUnitDisseminationPolicyIds?: OrganizationalUnitDisseminationPolicyIds): void {
    this._download(organizationalUnitDisseminationPolicyIds);
  }

  downloadSelection(organizationalUnitDisseminationPolicyIds?: OrganizationalUnitDisseminationPolicyIds): void {
    const listPath = this.listSelection.map(s => ArchiveHelper.generateSubItemPath(s.relativeLocation, s.fileName));
    this._download(organizationalUnitDisseminationPolicyIds, listPath);
  }

  downloadFolder(folderName: string): void {
    this._download(undefined, [ArchiveHelper.generateSubItemPath(folderName)]);
  }

  private _downloadDataFile($event: ArchiveDataFile): void {
    this._download(undefined, [ArchiveHelper.generateSubItemPath($event.relativeLocation, $event.fileName)]);
  }

  private _download(organizationalUnitDisseminationPolicyIds?: OrganizationalUnitDisseminationPolicyIds, listPath?: string[]): void {
    ArchiveHelper.download(this._store, this._dialog, this.archive, organizationalUnitDisseminationPolicyIds, listPath);
  }

  isDataTableFirstDisplay: boolean = true;

  multiSelectionChangeFiles(list: ArchiveDataFile[]): void {
    // Ignore first event
    if (isNullOrUndefinedOrEmptyArray(list) && this.isDataTableFirstDisplay) {
      return;
    }
    if (this.currentFileViewMode === FileViewModeEnum.FolderView) {
      this._multiSelectionChangeFilesFolderView(list);
    } else if (this.currentFileViewMode === FileViewModeEnum.FlatView) {
      this._multiSelectionChangeFilesFlatView(list);
    }
  }

  private _multiSelectionChangeFilesFolderView(list: ArchiveDataFile[]): void {
    this.isDataTableFirstDisplay = false;
    const existingSelection = [...this.listSelection];
    ArrayUtil.extract(existingSelection, s => s.relativeLocation === this.currentFolder);
    const folderSelection = list.map(s => this._convertArchiveDataFileToSelection(s));
    this.listSelection = [...existingSelection, ...folderSelection];
  }

  private _multiSelectionChangeFilesFlatView(list: ArchiveDataFile[]): void {
    this.listSelection = [...list].map(s => this._convertArchiveDataFileToSelection(s));
  }

  private _convertArchiveDataFileToSelection(archiveDataFile: ArchiveDataFile): Selection {
    return {
      fileName: archiveDataFile.fileName,
      relativeLocation: archiveDataFile.relativeLocation,
      resource: archiveDataFile,
    };
  }

  listSelectionOnCurrentFolder: ArchiveDataFile[] = [];

  computeListSelectionOnCurrentFolder(): void {
    if (this.currentFileViewMode === FileViewModeEnum.FolderView) {
      this.listSelectionOnCurrentFolder = this.listSelection.filter(s => s.relativeLocation === this.currentFolder).map(s => s.resource);
    } else if (this.currentFileViewMode === FileViewModeEnum.FlatView) {
      this.listSelectionOnCurrentFolder = this.listSelection.map(s => s.resource);
    }
  }
}

interface Selection {
  fileName?: string;
  relativeLocation?: string;
  resource?: ArchiveDataFile;
}
