/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-search.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {
  ActivatedRoute,
  Params,
} from "@angular/router";
import {HomeAction} from "@app/features/home/stores/home.action";
import {HomeState} from "@app/features/home/stores/home.state";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@app/shared/enums/routes.enum";
import {SharedArchiveAction} from "@app/shared/stores/archive/shared-archive.action";
import {AppState} from "@app/stores/app.state";
import {AppCartAction} from "@app/stores/cart/app-cart.action";
import {Enums} from "@enums";
import {HomeArchiveClickThroughDuaDownloadDialog} from "@home/components/dialogs/archive-click-through-dua-download/home-archive-click-through-dua-download-dialog.component";
import {HomeSearchHelpDialog} from "@home/components/dialogs/home-search-help/home-search-help.dialog";
import {HomeHelper} from "@home/helpers/home.helper";
import {PortalSearch} from "@home/models/portal-search.model";
import {ArchiveAccessRightService} from "@home/services/archive-access-right.service";
import {
  Archive,
  SystemProperty,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {ArchiveDataTableHelper} from "@shared/helpers/archive-data-table.helper";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {Observable} from "rxjs";
import {
  map,
  tap,
} from "rxjs/operators";
import {
  AppSystemPropertyState,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  ExtendEnum,
  Facet,
  FacetNamePartialEnum,
  isTrue,
  LoginMode,
  MappingObject,
  MemoizedUtil,
  OAuth2Service,
  OrderEnum,
  Paging,
  QueryParameters,
  QueryParametersUtil,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

@Component({
  selector: "dlcm-home-search-routable",
  templateUrl: "./home-search.routable.html",
  styleUrls: ["./home-search.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeSearchRoutable extends SharedAbstractPresentational implements OnInit, OnDestroy {
  searchObs: Observable<string> = MemoizedUtil.select(this._store, HomeState, state => state.search);
  searchModeIsQueryObs: Observable<boolean> = MemoizedUtil.select(this._store, HomeState, state => state.searchModeIsQuery);
  facetsSelectedObs: Observable<MappingObject<Enums.Facet.Name, string[]>> = MemoizedUtil.select(this._store, HomeState, state => state.facetsSelected);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, HomeState, state => state.queryParameters);
  viewModeTableEnumObs: Observable<ViewModeTableEnum> = MemoizedUtil.select(this._store, HomeState, state => state.viewModeTableEnum);
  totalObs: Observable<number> = MemoizedUtil.select(this._store, HomeState, state => state.total);
  listObs: Observable<Archive[]> = MemoizedUtil.select(this._store, HomeState, state => state.list);
  facetsObs: Observable<Facet[]> = MemoizedUtil.select(this._store, HomeState, state => state.facets);
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeState);
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);
  isInTourModeObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.isInTourMode);
  isLoggedInObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.isLoggedIn);
  isFacetClosed: boolean = true;
  detailRouteToInterpolate: string;

  get viewModeTableEnum(): typeof ViewModeTableEnum {
    return ViewModeTableEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  columns: DataTableColumns[] = [
    {
      field: "organizationalUnitId",
      header: LabelTranslateEnum.organizationalUnit,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      component: DataTableComponentHelper.get(DataTableComponentEnum.organizationalUnitName),
      isSortable: true,
      isFilterable: false,
    },
    ...ArchiveDataTableHelper.searchColumns,
  ];

  actions: DataTableActions<Archive>[] = [
    {
      logo: IconNameEnum.download,
      callback: current => this.download(current),
      placeholder: current => LabelTranslateEnum.download,
      disableCondition: current => this.archiveAccessRightService.isDownloadAuthorizedObs(current).pipe(map(isAuthorized => !isAuthorized)),
      displayOnCondition: current => MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn) || current.accessLevel === Enums.Access.AccessEnum.PUBLIC,
    },
    {
      logo: IconNameEnum.addToCart,
      callback: current => this.addToCart(current),
      placeholder: current => LabelTranslateEnum.addToDownloadOrder,
      displayOnCondition: current => MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn),
    },
    {
      logo: IconNameEnum.sendRequest,
      callback: current => this._requestAccessDataset(current),
      placeholder: current => LabelTranslateEnum.requestAccess,
      displayOnCondition: current => !this.archiveAccessRightService.isDownloadAuthorizedSnapshot(current),
    },
  ];

  constructor(private readonly _store: Store,
              private readonly _translate: TranslateService,
              private readonly _route: ActivatedRoute,
              private readonly _actions$: Actions,
              private readonly _oauthService: OAuth2Service,
              protected readonly _dialog: MatDialog,
              readonly archiveAccessRightService: ArchiveAccessRightService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.detailRouteToInterpolate = `${RoutesEnum.archives}/\{${SOLIDIFY_CONSTANTS.RES_ID}\}`;
    this.subscribe(this._observeSearchQueryParam());
  }

  private _observeSearchQueryParam(): Observable<Params> {
    return this._route.queryParams.pipe(tap(params => {
      const searchInfos = HomeHelper.extractSearchInfosFromUrl(params);
      this._store.dispatch(new HomeAction.Search(true, searchInfos.search, searchInfos.searchModeIsQuery, searchInfos.facetsSelected, searchInfos.viewTabMode));
    }));
  }

  private _requestAccessDataset(archive: Archive): void {
    this.archiveAccessRightService.requestAccessDataset(archive);
  }

  searchText(search: string): void {
    const facetsSelected = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.facetsSelected);
    const viewMode = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.viewModeTableEnum);
    const searchModeIsQuery: boolean = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.searchModeIsQuery);
    this._search(search, searchModeIsQuery, facetsSelected, viewMode);
  }

  searchFacet(facetsSelected: MappingObject<ExtendEnum<FacetNamePartialEnum>, string[]>): void {
    const search = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.search);
    const viewMode = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.viewModeTableEnum);
    const searchModeIsQuery: boolean = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.searchModeIsQuery);
    this._search(search, searchModeIsQuery, facetsSelected, viewMode);
  }

  searchQueryToggle(searchModeIsQuery: boolean): void {
    const search = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.search);
    const facetsSelected = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.facetsSelected);
    const viewMode = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.viewModeTableEnum);
    this._search(search, searchModeIsQuery, facetsSelected, viewMode);
  }

  switchViewMode(viewMode: ViewModeTableEnum): void {
    const search = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.search);
    const facetsSelected = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.facetsSelected);
    const searchModeIsQuery: boolean = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.searchModeIsQuery);
    this._search(search, searchModeIsQuery, facetsSelected, viewMode);
  }

  searchModeChange($event: MatSlideToggleChange): void {
    const searchModeIsQuery: boolean = isTrue($event.checked);
    this.searchQueryToggle(searchModeIsQuery);
  }

  private _search(searchTerm: string, searchModeIsQuery: boolean, facetsSelected: MappingObject<ExtendEnum<FacetNamePartialEnum>, string[]>, viewModeTableEnum: ViewModeTableEnum = ViewModeTableEnum.list): void {
    const portalSearch = {
      searchText: searchTerm,
      searchModeIsQuery: searchModeIsQuery,
      facetsSelected: facetsSelected,
      viewModeTable: viewModeTableEnum,
    } as PortalSearch;

    HomeHelper.navigateToSearch(this._store, portalSearch);
  }

  onQueryParametersEvent($event: QueryParameters): void {
    this._store.dispatch(new HomeAction.ChangeQueryParameters($event));
  }

  showDetail(archive: Archive): void {
    this._store.dispatch(new Navigate([this.showDetailNavigate(archive)]));
  }

  showDetailNavigate(archive: Archive): string {
    return `${RoutesEnum.archives}/${archive.resId}`;
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  download(archive: Archive): void {
    if (archive.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA) {
      this.openClickThroughDuaDialog(archive);
    } else {
      this._store.dispatch(new SharedArchiveAction.Download(archive));
    }
  }

  openClickThroughDuaDialog(archive: Archive): void {
    this.subscribe(DialogUtil.open(this._dialog, HomeArchiveClickThroughDuaDownloadDialog, {
        archive: archive,
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      }, () => {
        this._store.dispatch(new SharedArchiveAction.Download(archive));
      }));
  }

  addToCart(archive: Archive): void {
    this._store.dispatch(new AppCartAction.AddToCart(archive));
  }

  navigate($event: RoutesEnum): void {
    this._store.dispatch(new Navigate([$event]));
  }

  goToDeposit(): void {
    const isLogged = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn);
    if (isLogged) {
      this._store.dispatch(new Navigate([RoutesEnum.deposit]));
    } else {
      this._oauthService.initAuthorizationCodeFlow(LoginMode.STANDARD, AppRoutesEnum.deposit);
    }
  }

  pageChange(paging: Paging): void {
    const queryParameters = QueryParametersUtil.clone(MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.queryParameters));
    queryParameters.paging = paging;
    this._store.dispatch(new HomeAction.ChangeQueryParameters(queryParameters));
  }

  openHelp(): void {
    DialogUtil.open(this._dialog, HomeSearchHelpDialog);
  }

  requestAccessDataset(archive: Archive): void {
    this.archiveAccessRightService.requestAccessDataset(archive);
  }
}
