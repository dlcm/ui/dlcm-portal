/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-file-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {ArchiveHelper} from "@home/helpers/archive.helper";
import {ArchiveAccessRightService} from "@home/services/archive-access-right.service";
import {HomeArchiveDataFileAction} from "@home/stores/archive/data-file/home-archive-data-file.action";
import {HomeArchiveDataFileState} from "@home/stores/archive/data-file/home-archive-data-file.state";
import {HomeState} from "@home/stores/home.state";
import {ArchiveDataFile} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  SharedFileAndAipDetailDialogModeEnum,
  SharedFileAndAipInformationDialogData,
} from "@shared/components/containers/shared-file-and-aip-information/shared-file-and-aip-information.container";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {Observable} from "rxjs";
import {
  filter,
  tap,
} from "rxjs/operators";
import {
  DownloadService,
  isNotNullNorUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-home-archive-file-detail-routable",
  templateUrl: "./home-archive-file-detail.routable.html",
  styleUrls: ["../../../../../shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeArchiveFileDetailRoutable extends SharedAbstractRoutable implements OnInit {
  data: SharedFileAndAipInformationDialogData<ArchiveDataFile>;
  archiveDataFileObs: Observable<ArchiveDataFile> = MemoizedUtil.select(this._store, HomeArchiveDataFileState, state => state.current);
  readyForDownloadObs: Observable<boolean> = MemoizedUtil.select(this._store, HomeArchiveDataFileState, state => state.readyForDownload);
  mode: SharedFileAndAipDetailDialogModeEnum = SharedFileAndAipDetailDialogModeEnum.file;
  canDownload: Observable<boolean>;

  protected _parentId: string | undefined;
  protected _dataFileId: string | undefined;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _archiveAccessRightService: ArchiveAccessRightService,
              protected readonly _downloadService: DownloadService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._retrieveResIdFromUrl();
    this.canDownload = this._archiveAccessRightService.isDownloadAuthorizedByIdObs(this._parentId);
    this._store.dispatch(new HomeArchiveDataFileAction.GetById(this._parentId, this._dataFileId));
    this._store.dispatch(new HomeArchiveDataFileAction.CheckDataFileIfIsReadyDownload(this._parentId, this._dataFileId));
    this.subscribe(this._observableCreateData());
  }

  protected _retrieveResIdFromUrl(): void {
    this._dataFileId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdDataFileWithoutPrefixParam);
    this._parentId = this._route.parent.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  protected _observableCreateData(): Observable<ArchiveDataFile> {
    return this.archiveDataFileObs.pipe(
      filter(resource => isNotNullNorUndefined(resource) && resource.resId === this._dataFileId),
      tap(resource => {
        this.data = {
          mode: this.mode,
          dataFile: {
            ...resource,
            status: Enums.DataFile.StatusEnum.READY,
          },
          aip: undefined,
          resId: resource.resId,
          parentId: this._parentId,
          buttons: [
            {
              labelToTranslate: (current) => LabelTranslateEnum.download,
              color: "primary",
              icon: IconNameEnum.download,
              callback: current => this._download(current),
              displayCondition: current => this._archiveAccessRightService.isDownloadAuthorizedByIdObs(this._parentId),
              order: 52,
            },
            {
              labelToTranslate: (current) => LabelTranslateEnum.downloadFileOnly,
              color: "primary",
              icon: IconNameEnum.download,
              callback: current => this._downloadFileOnly(current),
              displayCondition: current => this._archiveAccessRightService.isDownloadAuthorizedByIdObs(this._parentId),
              order: 52,
            }],
        } as SharedFileAndAipInformationDialogData<ArchiveDataFile>;
        this._changeDetector.detectChanges();
      }),
    );
  }

  private _download(archiveDataFile: ArchiveDataFile): void {
    const archive = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.current);
    ArchiveHelper.download(this._store, this._dialog, archive, undefined, [ArchiveHelper.generateSubItemPath(archiveDataFile.relativeLocation, archiveDataFile.fileName)]);
  }

  private _downloadFileOnly(archiveDataFile: ArchiveDataFile): void {
    const archive = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.current);
    ArchiveHelper.downloadFileOnly(this._downloadService, this._dialog, archive, archiveDataFile);
  }

  triggerPreview(): void {
    this._store.dispatch(new HomeArchiveDataFileAction.GetById(this._parentId, this._dataFileId));
  }
}
