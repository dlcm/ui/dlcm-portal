/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-collection-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {HomeArchiveCollectionAction} from "@home/stores/archive/collection/home-archive-collection.action";
import {HomeArchiveCollectionState} from "@home/stores/archive/collection/home-archive-collection.state";
import {
  Archive,
  ArchiveDataFile,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedFileAndAipInformationContainer} from "@shared/components/containers/shared-file-and-aip-information/shared-file-and-aip-information.container";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  HomePageRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {LocalStateModel} from "@shared/models/local-state.model";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  DataTableActions,
  DataTableBulkActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DataTablePresentational,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
} from "solidify-frontend";

@Component({
  selector: "dlcm-home-archive-collection-list-routable",
  templateUrl: "./home-archive-collection-list.routable.html",
  styleUrls: ["./home-archive-collection-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeArchiveCollectionListRoutable extends SharedAbstractRoutable implements OnInit, OnDestroy {
  private _archiveId: string;
  private readonly _KEY_QUERY_PARAMETERS: keyof ArchiveDataFile = "status";

  isLoadingDataFileObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeArchiveCollectionState);
  listCollectionObs: Observable<ArchiveDataFile[]> = MemoizedUtil.select(this._store, HomeArchiveCollectionState, state => state.list);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, HomeArchiveCollectionState, state => state.queryParameters);

  @ViewChild("dataTablePresentational")
  readonly dataTablePresentational: DataTablePresentational<ArchiveDataFile>;

  columns: DataTableColumns<ArchiveDataFile>[];

  actions: DataTableActions<ArchiveDataFile>[] = [
    // {
    //   logo: IconNameEnum.download,
    //   callback: (archiveDataFile: ArchiveDataFile) => this.downloadDataFile(this._resId, archiveDataFile),
    //   placeholder: current => LabelTranslateEnum.download,
    //   displayOnCondition: current => current.status === Enums.DataFile.StatusEnum.PROCESSED || current.status === Enums.DataFile.StatusEnum.READY || current.status === Enums.DataFile.StatusEnum.VIRUS_CHECKED,
    // },
  ];

  bulkActions: DataTableBulkActions<ArchiveDataFile>[] = [];

  columnsToSkippedFilter: keyof ArchiveDataFile[] | string[] = [
    this._KEY_QUERY_PARAMETERS,
  ];

  dialogDataFileRef: MatDialogRef<SharedFileAndAipInformationContainer<ArchiveDataFile>>;

  readonly KEY_PARAM_NAME: keyof Archive & string = undefined;
  isInDisplayArchiveDataFileListMode: boolean = false;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _securityService: SecurityService) {
    super();

    this.columns = [
      {
        field: "infoPackage.title" as any,
        header: LabelTranslateEnum.title,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.ascending,
        isSortable: false,
        isFilterable: false,
      },
      {
        field: "infoPackage.accessLevel" as any,
        header: LabelTranslateEnum.accessLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        filterEnum: Enums.Access.AccessEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.accessLevelWithEmbargo),
        width: "180px",
      },
      {
        field: "infoPackage.dataSensitivity" as any,
        header: LabelTranslateEnum.sensitivity,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        filterEnum: Enums.DataSensitivity.DataSensitivityEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.sensitivity),
        width: "60px",
      },
      {
        field: "complianceLevel",
        header: LabelTranslateEnum.complianceLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        sortableField: "dataFile.complianceLevel" as any,
        filterableField: "dataFile.complianceLevel" as any,
        isSortable: false,
        isFilterable: false,
        translate: true,
        width: "185px",
        component: DataTableComponentHelper.get(DataTableComponentEnum.conformityLevelStar),
        filterEnum: Enums.ComplianceLevel.ComplianceLevelEnumTranslate,
        alignment: "center",
      },
      {
        field: "smartSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        sortableField: "dataFile.fileSize" as any,
        filterableField: "dataFile.fileSize" as any,
        isSortable: false,
        isFilterable: false,
        translate: true,
        alignment: "right",
        width: "100px",
      },
    ];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._getCurrentModelOnParent();

    this.subscribe(this._store.select((s: LocalStateModel) => s.router.state.url)
      .pipe(
        distinctUntilChanged(),
        tap(url => {
          this.isInDisplayArchiveDataFileListMode = url.endsWith(HomePageRoutesEnum.collections);
          this._changeDetector.detectChanges();
        }),
      ));
  }

  private _getCurrentModelOnParent(): void {
    this._archiveId = this._route.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._getArchiveById(this._archiveId);
  }

  private _getArchiveById(id: string): void {
    this.getSubResourceWithParentId(id);
  }

  getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new HomeArchiveCollectionAction.GetAll(id, undefined, true));
  }

  onQueryParametersEvent(queryParameters: QueryParameters, withRefresh: boolean = true): void {
    this._store.dispatch(new HomeArchiveCollectionAction.ChangeQueryParameters(this._archiveId, queryParameters, true, withRefresh));
  }

  download($event: ArchiveDataFile): void {
    this._store.dispatch(new HomeArchiveCollectionAction.Download(this._archiveId, $event));
  }

  showDetail(archiveDataFile: ArchiveDataFile): void {
    this._store.dispatch(new Navigate([RoutesEnum.archives, archiveDataFile.resId]));
  }

  back(): void {
    this._store.dispatch(new Navigate([RoutesEnum.archives, this._archiveId]));
  }
}
