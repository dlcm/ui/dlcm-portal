/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {Meta} from "@angular/platform-browser";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {HomeAction} from "@app/features/home/stores/home.action";
import {HomeState} from "@app/features/home/stores/home.state";
import {
  LIST_ARCHIVE_META_NAME,
  metaInfoArchive,
} from "@app/meta-info-list";
import {SecurityService} from "@app/shared/services/security.service";
import {AppState} from "@app/stores/app.state";
import {AppCartAction} from "@app/stores/cart/app-cart.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {ArchiveHelper} from "@home/helpers/archive.helper";
import {HomeHelper} from "@home/helpers/home.helper";
import {OrganizationalUnitDisseminationPolicyIds} from "@home/models/organizational-unit-dissemination-policy-ids.model";
import {PortalSearch} from "@home/models/portal-search.model";
import {ArchiveAccessRightService} from "@home/services/archive-access-right.service";
import {HomeArchivePackageAction} from "@home/stores/archive/package/home-archive-package.action";
import {HomeArchivePackageState} from "@home/stores/archive/package/home-archive-package.state";
import {HomeArchiveRatingAction} from "@home/stores/archive/rating/home-archive-rating.action";
import {HomeArchiveRatingState} from "@home/stores/archive/rating/home-archive-rating.state";
import {
  Archive,
  ArchiveStatisticsDto,
  ArchiveUserRating,
  SystemProperty,
  User,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedCitationDialog} from "@shared/components/dialogs/shared-citation/shared-citation.dialog";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  HomePageRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {
  ArchiveMetadataPackages,
  MetadataPackagesEnum,
} from "@shared/models/business/archive-metadata-packages.model";
import {LocalStateModel} from "@shared/models/local-state.model";
import {SharedArchiveState} from "@shared/stores/archive/shared-archive.state";
import {SharedCitationAction} from "@shared/stores/citations/shared-citation.action";
import {SharedLicenseAction} from "@shared/stores/license/shared-license.action";
import {SharedNotificationAction} from "@shared/stores/notification/shared-notification.action";
import {
  Observable,
  pipe,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  switchMap,
  take,
  tap,
} from "rxjs/operators";
import {
  AppSystemPropertyState,
  DialogUtil,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  MetaService,
  MetaUtil,
  SsrUtil,
  StoreUtil,
  UrlQueryParamHelper,
} from "solidify-frontend";

@Component({
  selector: "dlcm-home-archive-detail-routable",
  templateUrl: "./home-archive-detail.routable.html",
  styleUrls: ["./home-archive-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeArchiveDetailRoutable extends SharedAbstractRoutable implements OnInit, OnDestroy {
  isLoadingPrepareDownloadObs: Observable<boolean>;
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeState);
  currentUserObs: Observable<User> = MemoizedUtil.current(this._store, AppUserState);
  currentObs: Observable<Archive> = MemoizedUtil.select(this._store, HomeState, state => state.current);
  archiveStatisticDtoObs: Observable<ArchiveStatisticsDto> = MemoizedUtil.select(this._store, HomeState, state => state.archiveStatisticDto);
  isLoggedInObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.isLoggedIn);
  isLoadingPackageObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeArchivePackageState);
  packageObs: Observable<ArchiveMetadataPackages> = MemoizedUtil.select(this._store, HomeArchivePackageState, state => state.package);

  listArchiveFromOrgUnitObs: Observable<Archive[]> = MemoizedUtil.select(this._store, HomeState, state => state.listRelativeArchive);
  isInDisplayArchiveDetailMode: boolean = true;

  isLoadingArchiveRatingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, HomeArchiveRatingState);
  listArchiveRatingObs: Observable<ArchiveUserRating[]> = MemoizedUtil.list(this._store, HomeArchiveRatingState);

  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);

  canSeeDeposit: boolean = false;

  get archive(): Archive {
    return MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.current);
  }

  get package(): ArchiveMetadataPackages {
    return MemoizedUtil.selectSnapshot(this._store, HomeArchivePackageState, state => state.package);
  }

  get depositId(): string | undefined {
    return this.package?.metadata?.packages?.deposits?.id;
  }

  get depositPath(): string | undefined {
    const organizationalUnitId = this.package?.metadata?.[MetadataPackagesEnum.aipOrganizationalUnit];
    if (isNullOrUndefined(organizationalUnitId) || isNullOrUndefined(this.depositId)) {
      return;
    }
    return `/${RoutesEnum.deposit}/${organizationalUnitId}/${DepositRoutesEnum.detail}/${this.depositId}`;
  }

  private _resId: string;
  private _title: string;

  constructor(private readonly _store: Store,
              private readonly _dialog: MatDialog,
              private readonly _actions$: Actions,
              private readonly _archiveAccessRightService: ArchiveAccessRightService,
              private readonly _router: Router,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _route: ActivatedRoute,
              private readonly _metaService: MetaService,
              private readonly _meta: Meta,
              private readonly _translateService: TranslateService,
              readonly securityService: SecurityService,
  ) {
    super();
  }

  private _rewriteUrl(): void {
    if (SsrUtil.window?.history.replaceState && isNotNullNorUndefined(this._resId)) {
      SsrUtil.window?.history.replaceState({}, null, `${environment.baseHref}${AppRoutesEnum.archives}/${this._resId}${SsrUtil.window.location.search}`);
    }
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (SsrUtil.isBrowser) {
      this.subscribe(this._store.select((s: LocalStateModel) => s.router.state.url).pipe(
          distinctUntilChanged(),
          tap(url => {
            this.isInDisplayArchiveDetailMode = !url.includes(HomePageRoutesEnum.files) && !url.includes(HomePageRoutesEnum.collections);
            this._rewriteUrl();
            this._changeDetector.detectChanges();
          }),
        ),
      );

      this.subscribe(this._translateService.onLangChange.pipe(
        tap(() => {
          this._metaService.setMetaFromInfo(metaInfoArchive, this.archive);
        }),
      ));
    }

    this.subscribe(this.packageObs.pipe(
      tap(() => {
        this.canSeeDeposit = false;
        this._changeDetector.detectChanges();
      }),
      filter(archivePackage => this.securityService.isLoggedIn() && archivePackage?.resId === this._resId),
      switchMap(archivePackage => this.securityService.canSeeDetailDepositByIdObs(this.depositId).pipe(
        tap(canSeeDeposit => {
          this.canSeeDeposit = canSeeDeposit;
          this._changeDetector.detectChanges();
        }),
      )),
    ));

    this.subscribe(this.currentObs.pipe(
      filter(archive => isNotNullNorUndefined(archive) && archive.resId !== this._resId),
      distinctUntilChanged((previous, current) => previous?.resId === current?.resId),
      SsrUtil.isServer ? take(1) : pipe(),
      tap(archive => {
        this._resId = archive.resId;
        this._title = archive.title;
        setTimeout(() => {
          this._rewriteUrl();
        }, 0);
        this._getById(this._resId);
        this._initIsLoadingPrepareDownloadObs();
        this._metaService.setMetaFromInfo(metaInfoArchive, archive);
      }),
    ));

    if (SsrUtil.isBrowser) {
      this._promptRequestAccessDialogIfLoggedAndParamInUrl();
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._store.dispatch(new HomeAction.CleanCurrent());
    this._store.dispatch(new SharedLicenseAction.Clean());
    MetaUtil.cleanListMetaByName(this._meta, LIST_ARCHIVE_META_NAME);
  }

  private _promptRequestAccessDialogIfLoggedAndParamInUrl(): void {
    this.subscribe(this._route.paramMap.pipe(
      take(1),
      tap(p => {
        const currentUser = MemoizedUtil.currentSnapshot(this._store, AppUserState);
        if (isNullOrUndefined(currentUser)) {
          return;
        }
        this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
          new SharedNotificationAction.GetPendingNotification(Enums.Notification.TypeEnum.ACCESS_DATASET_REQUEST, this.archive.resId),
          SharedNotificationAction.GetPendingNotificationSuccess,
          successAction => {
            if (successAction.notification?.notificationStatus === Enums.Notification.StatusEnum.PENDING) {
              return;
            }
            this._checkArchiveQueryParams();
          },
        ));

      }),
    ));
  }

  private _checkArchiveQueryParams(): void {
    const hasAccessRequest = UrlQueryParamHelper.currentUrlContainsQueryParam(environment.archiveQueryParamAccessRequest);

    if (hasAccessRequest) {
      this.subscribe(this._archiveAccessRightService.isDownloadAuthorizedObs(this.archive).pipe(
        take(1),
        filter(isAuthorized => !isAuthorized),
        tap(isAuthorized => {
          const queryParams: MappingObject<string, string | undefined> = UrlQueryParamHelper.getQueryParamMappingObjectFromCurrentUrl();
          this._archiveAccessRightService.openRequestArchiveDialog(this.archive);
          // clean url
          if (MappingObjectUtil.has(queryParams, environment.archiveQueryParamAccessRequest)) {
            this._router.navigate([RoutesEnum.archives, this._resId], {queryParams: null});
          }
        }),
      ));
    }
  }

  private _getById(id: string): void {
    this._store.dispatch(new HomeAction.GetStatistics(id));
    this._store.dispatch(new SharedCitationAction.GetBibliographies(id));
    this._store.dispatch(new SharedCitationAction.GetCitations(id));
    this._store.dispatch(new HomeArchivePackageAction.GetById(this._resId));
    const currentUser = MemoizedUtil.currentSnapshot(this._store, AppUserState);
    if (isNotNullNorUndefined(currentUser)) {
      this._store.dispatch(new SharedNotificationAction.GetPendingNotification(Enums.Notification.TypeEnum.ACCESS_DATASET_REQUEST, this._resId));
      this._store.dispatch(new HomeArchiveRatingAction.GetAllByCurrentUser(this._resId));
    }
  }

  private _initIsLoadingPrepareDownloadObs(): void {
    this.isLoadingPrepareDownloadObs = SharedArchiveState.isDownloadPreparation(this._store, this._resId);
  }

  download(event: { archive: Archive, organizationalUnitDisseminationPolicyIds?: OrganizationalUnitDisseminationPolicyIds }): void {
    ArchiveHelper.download(this._store, this._dialog, event.archive, event.organizationalUnitDisseminationPolicyIds);
  }

  addToCart(archive: Archive): void {
    this._store.dispatch(new AppCartAction.AddToCart(archive));
  }

  askAccess(archive: Archive): void {
    this._archiveAccessRightService.requestAccessDataset(archive);
  }

  back(): void {
    const search = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.search);
    const viewMode = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.viewModeTableEnum);
    const facetsSelected = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.facetsSelected);
    const searchModeIsQuery = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.searchModeIsQuery);

    const portalSearch = {
      searchText: search,
      searchModeIsQuery: searchModeIsQuery,
      facetsSelected: facetsSelected,
      viewModeTable: viewMode,
    } as PortalSearch;

    HomeHelper.navigateToSearch(this._store, portalSearch);
  }

  showCitationDialog(): void {
    DialogUtil.open(this._dialog, SharedCitationDialog, {
      archiveId: this._resId,
      archiveTitle: this._title,
    }, {
      width: "800px",
    });
  }

  showArchive(archive: Archive): void {
    this._store.dispatch([
      new HomeAction.CleanCurrent(),
      new Navigate([RoutesEnum.archives, archive.resId]),
    ]);
  }

  sendToOrcid(publishedDeposit: Archive): void {
    if (this.securityService.isLoggedIn()) {
      this._store.dispatch(new HomeAction.ExportToOrcid(publishedDeposit));
    }
  }

  navigate(navigate: Navigate): void {
    this._store.dispatch(navigate);
  }

  updateRate(listNewArchiveUserRating: ArchiveUserRating[]): void {
    listNewArchiveUserRating.forEach(userRating => {
      if (isNotNullNorUndefinedNorWhiteString(userRating.resId)) {
        this._store.dispatch(new HomeArchiveRatingAction.Update(this._resId, userRating));
      } else {
        this._store.dispatch(new HomeArchiveRatingAction.Create(this._resId, userRating));
      }
    });
  }
}
