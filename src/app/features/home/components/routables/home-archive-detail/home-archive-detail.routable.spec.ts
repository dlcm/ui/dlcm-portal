/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-detail.routable.spec.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClientTestingModule} from "@angular/common/http/testing";
import {NO_ERRORS_SCHEMA} from "@angular/core";
import {
  ComponentFixture,
  TestBed,
  waitForAsync,
} from "@angular/core/testing";
import {ReactiveFormsModule} from "@angular/forms";
import {
  ActivatedRoute,
  Params,
} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {
  ActivatedRouteStub,
  MockTranslatePipe,
  MockTranslateService,
} from "solidify-frontend";

import {HomeArchiveDetailRoutable} from "./home-archive-detail.routable";

xdescribe("HomeArchiveDetailRoutable", () => {
  let component: HomeArchiveDetailRoutable;
  let fixture: ComponentFixture<HomeArchiveDetailRoutable>;
  const activatedRoute: ActivatedRouteStub = new ActivatedRouteStub({id: "test"} as Params);
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, HttpClientTestingModule, NgxsModule.forRoot([])],
      declarations: [HomeArchiveDetailRoutable, MockTranslatePipe],
      providers: [
        {
          provide: TranslateService,
          useClass: MockTranslateService,
        },
        {
          provide: ActivatedRoute,
          useValue: activatedRoute,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeArchiveDetailRoutable);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
