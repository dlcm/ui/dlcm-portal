/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-click-through-dua-download-dialog.component.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from "@angular/material/dialog";
import {environment} from "@environments/environment";
import {Archive} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SharedNotificationState} from "@shared/stores/notification/shared-notification.state";
import {Observable} from "rxjs";
import {
  DialogUtil,
  FilePreviewDialog,
  FileStatusEnum,
  FileVisualizerHelper,
  FileVisualizerService,
  FormValidationHelper,
  isTrue,
  MemoizedUtil,
  PropertyName,
  SolidifyDataFileModel,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-home-archive-click-through-dua-download-dialog",
  templateUrl: "./home-archive-click-through-dua-download-dialog.component.html",
  styleUrls: ["./home-archive-click-through-dua-download-dialog.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeArchiveClickThroughDuaDownloadDialog extends SharedAbstractDialog<HomeArchiveClickThroughDuaDownloadDialogData, boolean> implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedNotificationState);

  duaFileUrl: string | undefined;
  canPreviewDua: boolean;
  duaDataFile: SolidifyDataFileModel;

  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  constructor(private readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<HomeArchiveClickThroughDuaDownloadDialog>,
              private readonly _fb: FormBuilder,
              private readonly _dialog: MatDialog,
              private readonly _fileVisualizerService: FileVisualizerService,
              @Inject(MAT_DIALOG_DATA) public readonly data: HomeArchiveClickThroughDuaDownloadDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.form = this._fb.group({
      [this.formDefinition.acceptDua]: [false, [SolidifyValidator, Validators.requiredTrue]],
    });

    this._computeDuaInfo();
  }

  private _computeDuaInfo(): void {
    this.duaFileUrl = undefined;
    this.duaDataFile = undefined;
    this.canPreviewDua = false;
    if (isTrue(this.data.archive.withDuaFile)) {
      this.duaFileUrl = `${ApiEnum.accessPublicMetadata}/${this.data.archive.resId}/${ApiActionNameEnum.DUA}`;
      this.duaDataFile = {
        fileName: environment.duaFileName,
        mimetype: this.data.archive.duaContentType,
        status: FileStatusEnum.READY,
      };
      this.canPreviewDua = FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
        dataFile: this.duaDataFile,
      });
    }
  }

  previewDuaFile(): void {
    DialogUtil.open(this._dialog, FilePreviewDialog, {
        fileInput: {
          dataFile: this.duaDataFile,
        },
        fileDownloadUrl: this.duaFileUrl,
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      });
  }

  onSubmit(): void {
    this.submit(true);
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }
}

export interface HomeArchiveClickThroughDuaDownloadDialogData {
  archive: Archive;
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() acceptDua: string;
}
