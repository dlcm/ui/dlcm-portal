/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-restricted-request-access-dialog.component.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from "@angular/material/dialog";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Archive,
  DataFile,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SharedNotificationState} from "@shared/stores/notification/shared-notification.state";
import {Observable} from "rxjs";
import {
  DialogUtil,
  DownloadService,
  EnumUtil,
  FilePreviewDialog,
  FileStatusEnum,
  FileVisualizerHelper,
  FileVisualizerService,
  FormValidationHelper,
  isTrue,
  MemoizedUtil,
  PropertyName,
  SolidifyDataFileModel,
  SolidifyFileUploadInputRequiredValidator,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-home-preservation-space-organizational-unit-request-access-dialog",
  templateUrl: "./home-archive-restricted-request-access-dialog.component.html",
  styleUrls: ["./home-archive-restricted-request-access-dialog.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeArchiveRestrictedRequestAccessDialog extends SharedAbstractDialog<HomeArchiveRestrictedRequestAccessDialogData, HomeArchiveRestrictedRequestAccessDialogResult> implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedNotificationState);

  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  get dataUsePolicyEnum(): typeof Enums.Deposit.DataUsePolicyEnum {
    return Enums.Deposit.DataUsePolicyEnum;
  }

  duaFileUrl: string | undefined;
  canPreviewDua: boolean;
  duaDataFile: SolidifyDataFileModel;

  dataFileAdapter: (dataFile: DataFile) => SolidifyDataFileModel = DataFileHelper.dataFileAdapter;

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<HomeArchiveRestrictedRequestAccessDialog>,
              private readonly _fb: FormBuilder,
              private readonly _dialog: MatDialog,
              private readonly _fileVisualizerService: FileVisualizerService,
              private readonly _downloadService: DownloadService,
              @Inject(MAT_DIALOG_DATA) public readonly data: HomeArchiveRestrictedRequestAccessDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.message]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.acceptDua]: [false, [SolidifyValidator]],
      [this.formDefinition.duaFileSigned]: [undefined, [SolidifyValidator]],
    });

    if (this.data.archive.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA) {
      this.form.get(this.formDefinition.acceptDua).addValidators(Validators.requiredTrue);
    }

    if (this.data.archive.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.SIGNED_DUA) {
      this.form.get(this.formDefinition.duaFileSigned).addValidators(SolidifyFileUploadInputRequiredValidator);
    }

    this._computeDuaInfo();
  }

  private _computeDuaInfo(): void {
    this.duaFileUrl = undefined;
    this.duaDataFile = undefined;
    this.canPreviewDua = false;
    if (isTrue(this.data.archive.withDuaFile)) {
      this.duaFileUrl = `${ApiEnum.accessPublicMetadata}/${this.data.archive.resId}/${ApiActionNameEnum.DUA}`;
      this.duaDataFile = {
        fileName: environment.duaFileName,
        mimetype: this.data.archive.duaContentType,
        status: FileStatusEnum.READY,
      };
      this.canPreviewDua = FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
        dataFile: this.duaDataFile,
      });
    }
  }

  onSubmit(): void {
    this.submit({
      message: this.form.get(this.formDefinition.message).value,
      duaFileSigned: this.form.get(this.formDefinition.duaFileSigned).value,
    } as HomeArchiveRestrictedRequestAccessDialogResult);
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  previewDuaFile(): void {
    DialogUtil.open(this._dialog, FilePreviewDialog, {
        fileInput: {
          dataFile: this.duaDataFile,
        },
        fileDownloadUrl: this.duaFileUrl,
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      });
  }

  downloadDuaFile(): void {
    this.subscribe(this._downloadService.downloadInMemory(this.duaFileUrl, this.duaDataFile.fileName, true, this.duaDataFile.mimetype));
  }

  dataUsePolicyEnumTranslate(value: string): string {
    return EnumUtil.getLabel(Enums.Deposit.DataUsePolicyEnumTranslate, value);
  }

  showDuaDialog(dataUsePolicy: Enums.Deposit.DataUsePolicyEnum): boolean {
    return dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA || dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.SIGNED_DUA
      || dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.EXTERNAL_DUA;
  }

  isDuaExternal(dataUsePolicy: Enums.Deposit.DataUsePolicyEnum): boolean {
    return dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.EXTERNAL_DUA;
  }
}

export interface HomeArchiveRestrictedRequestAccessDialogData {
  archive: Archive;
}

export interface HomeArchiveRestrictedRequestAccessDialogResult {
  message: string;
  duaFileSigned: File | undefined;
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() message: string;
  @PropertyName() acceptDua: string;
  @PropertyName() duaFileSigned: string;
}
