/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - archive-access-right.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
  HttpStatusCode,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {AppState} from "@app/stores/app.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  HomeArchiveRestrictedRequestAccessDialog,
  HomeArchiveRestrictedRequestAccessDialogData,
} from "@home/components/dialogs/archive-restricted-request-access/home-archive-restricted-request-access-dialog.component";
import {Archive} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedNotificationAction} from "@shared/stores/notification/shared-notification.action";
import {
  BehaviorSubject,
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  take,
} from "rxjs/operators";
import {
  AbstractBaseService,
  DialogUtil,
  HeaderEnum,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isObservable,
  LoginMode,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  OAuth2Service,
  ObservableUtil,
  PollingHelper,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SsrUtil,
  StoreUtil,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class ArchiveAccessRightService extends AbstractBaseService {
  archiveRightMapping: MappingObject<string, boolean | Observable<boolean>> = {} as MappingObject<string, boolean | Observable<boolean>>;

  constructor(private readonly _dialog: MatDialog,
              private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _securityService: SecurityService,
              private readonly _httpClient: HttpClient,
              private readonly _oauthService: OAuth2Service) {
    super();

    if (SsrUtil.isBrowser) {
      setTimeout(() => {
        this.subscribe(PollingHelper.startPollingObs({
          initialIntervalRefreshInSecond: environment.frequencyCleanCacheArchiveAccessRightInSecond,
          actionToDo: () => this.refreshCache(),
        }));
      }, environment.frequencyCleanCacheArchiveAccessRightInSecond * 1000);
    }
  }

  isDownloadAuthorizedObs(archive: Archive): Observable<boolean> {
    return this.isDownloadAuthorizedByIdObs(archive?.resId);
  }

  isDownloadAuthorizedByIdObs(archiveId: string): Observable<boolean> {
    const isAuthorized = MappingObjectUtil.get(this.archiveRightMapping, archiveId);
    if (isNotNullNorUndefined(isAuthorized)) {
      if (isObservable(isAuthorized)) {
        return isAuthorized;
      }
      return of(isAuthorized);
    }
    const behaviorSubject = new BehaviorSubject<boolean>(undefined);
    const observable = ObservableUtil.asObservable(behaviorSubject);
    MappingObjectUtil.set(this.archiveRightMapping, archiveId, observable);
    let headers = new HttpHeaders();
    headers = headers.set(HeaderEnum.avoidTokenRequestWhenNeeded, SOLIDIFY_CONSTANTS.STRING_TRUE);
    this.subscribe(this._httpClient.head(ApiEnum.accessPublicMetadata + "/" + archiveId + "/" + ApiActionNameEnum.DOWNLOAD_TOKEN, {
      headers,
    })
      .pipe(
        take(1),
        map(() => this._isAuthorized(behaviorSubject, archiveId)),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          if (error.status === HttpStatusCode.Unauthorized) {
            return of(this._isAuthorized(behaviorSubject, archiveId));
          }
          return of(this._isNotAuthorized(behaviorSubject, archiveId));
        }),
      ));
    return observable;
  }

  private _isAuthorized(behaviorSubject: BehaviorSubject<boolean>, archiveId: string): boolean {
    return this._setPermission(behaviorSubject, archiveId, true);
  }

  private _isNotAuthorized(behaviorSubject: BehaviorSubject<boolean>, archiveId: string): boolean {
    return this._setPermission(behaviorSubject, archiveId, false);
  }

  private _setPermission(behaviorSubject: BehaviorSubject<boolean>, archiveId: string, isAuthorized: boolean): boolean {
    MappingObjectUtil.set(this.archiveRightMapping, archiveId, isAuthorized);
    behaviorSubject.next(isAuthorized);
    behaviorSubject.complete();
    return isAuthorized;
  }

  refreshCache(): void {
    this.archiveRightMapping = {} as MappingObject<string, boolean | Observable<boolean>>;
  }

  forceRefreshIsDownloadAuthorizedByIdObs(archiveId: string): Observable<boolean> {
    this._cleanCacheEntry(archiveId);
    return this.isDownloadAuthorizedByIdObs(archiveId);
  }

  private _cleanCacheEntry(archiveId: string): boolean {
    return MappingObjectUtil.delete(this.archiveRightMapping, archiveId);
  }

  isDownloadAuthorizedSnapshot(archive: Archive): boolean {
    return this.isDownloadAuthorizedByIdSnapshot(archive?.resId);
  }

  isDownloadAuthorizedByIdSnapshot(archiveId: string): boolean {
    const isAuthorized = MappingObjectUtil.get(this.archiveRightMapping, archiveId);
    if (isNotNullNorUndefined(isAuthorized)) {
      if (isObservable(isAuthorized)) {
        return false;
      }
      return isAuthorized;
    }
    return false;
  }

  requestAccessDataset(archive: Archive): void {
    const isLogged = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isLoggedIn);

    if (isLogged) {
      this.openRequestArchiveDialog(archive);
    } else {
      const queryParam = "?" + environment.archiveQueryParamAccessRequest + "=" + SOLIDIFY_CONSTANTS.STRING_TRUE;
      this._oauthService.initAuthorizationCodeFlow(LoginMode.STANDARD, RoutesEnum.archives + urlSeparator + archive.resId + queryParam);
    }
  }

  private _sendRequestAccessDataset(archiveId: string, archiveOrgUnitId: string, message: string, duaFileSigned: File): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new SharedNotificationAction.Create({
        model: {
          emitter: MemoizedUtil.currentSnapshot(this._store, AppUserState),
          notifiedOrgUnit: {
            resId: archiveOrgUnitId,
          },
          objectId: archiveId,
          message: message,
          notificationType: {
            resId: Enums.Notification.TypeEnum.ACCESS_DATASET_REQUEST,
          },
        },
      }),
      SharedNotificationAction.CreateSuccess,
      result => {
        this._store.dispatch(new SharedNotificationAction.UpdatePendingNotification(archiveId, result.model));
        if (isNotNullNorUndefined(duaFileSigned)) {
          this._store.dispatch(new SharedNotificationAction.UploadFile(result.model.resId, {
            file: duaFileSigned,
          }));
        }
      },
    ));

  }

  openRequestArchiveDialog(archive: Archive): void {
    const dialogData: HomeArchiveRestrictedRequestAccessDialogData = {
      archive: archive,
    };
    this._openRequestArchiveDialog(dialogData);
  }

  private _openRequestArchiveDialog(data: HomeArchiveRestrictedRequestAccessDialogData): void {
    this.subscribe(DialogUtil.open(this._dialog, HomeArchiveRestrictedRequestAccessDialog, data, {
      minWidth: "500px",
      takeOne: true,
    }, result => {
      this._sendRequestAccessDataset(data.archive.resId, data.archive.organizationalUnitId, result.message, result.duaFileSigned);
    }));
  }

  isPublicMetadata(archive: Archive): boolean {
    return this._checkCurrentAccessLevelIs(archive, Enums.Access.AccessEnum.PUBLIC);
  }

  isRestrictedMetadata(archive: Archive): boolean {
    return this._checkCurrentAccessLevelIs(archive, Enums.Access.AccessEnum.RESTRICTED);
  }

  isClosedMetadata(archive: Archive): boolean {
    return this._checkCurrentAccessLevelIs(archive, Enums.Access.AccessEnum.CLOSED);
  }

  private _checkCurrentAccessLevelIs(archive: Archive, accessLevel: Enums.Access.AccessEnum): boolean {
    if (isNullOrUndefined(archive)) {
      return false;
    }
    return archive.accessLevel === accessLevel;
  }

  getTooltipDownload(archive: Archive, isDisabled: boolean): undefined | string {
    if (this.isPublicMetadata(archive) || !isDisabled) {
      return undefined;
    }
    if (this.isRestrictedMetadata(archive)) {
      return MARK_AS_TRANSLATABLE("homePage.archive.detail.buttonDisabledReason.needToBeMember");
    }
    if (this.isClosedMetadata(archive)) {
      return MARK_AS_TRANSLATABLE("homePage.archive.detail.buttonDisabledReason.needToHaveAcl");
    }
  }
}
