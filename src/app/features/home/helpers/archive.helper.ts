/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - archive.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {MatDialog} from "@angular/material/dialog";
import {Enums} from "@enums";
import {HomeArchiveClickThroughDuaDownloadDialog} from "@home/components/dialogs/archive-click-through-dua-download/home-archive-click-through-dua-download-dialog.component";
import {OrganizationalUnitDisseminationPolicyIds} from "@home/models/organizational-unit-dissemination-policy-ids.model";
import {
  Archive,
  ArchiveDataFile,
} from "@models";
import {Store} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {SharedArchiveAction} from "@shared/stores/archive/shared-archive.action";
import {Observable} from "rxjs";
import {
  DialogUtil,
  DownloadService,
  isNotNullNorUndefinedNorWhiteString,
  isTrue,
  ObservableUtil,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

export class ArchiveHelper {
  static generateSubItemPath(relativeLocation: string, fileName?: string): string {
    let path = relativeLocation;
    if (!path.endsWith(SOLIDIFY_CONSTANTS.FILE_SEPARATOR)) {
      path = path + SOLIDIFY_CONSTANTS.FILE_SEPARATOR;
    }
    if (path.startsWith(SOLIDIFY_CONSTANTS.FILE_SEPARATOR)) {
      path = path.substring(1);
    }
    return path + (isNotNullNorUndefinedNorWhiteString(fileName) ? fileName : "");
  }

  static download(store: Store, dialog: MatDialog, archive: Archive, organizationalUnitDisseminationPolicyIds?: OrganizationalUnitDisseminationPolicyIds, listPath?: string[]): void {
    if (archive.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA) {
      ObservableUtil.takeOneThenUnsubscribe(
        this._openClickThroughDuaDialog(dialog, archive),
        accept => {
          if (isTrue(accept)) {
            this._download(store, archive, organizationalUnitDisseminationPolicyIds, listPath);
          }
        },
      );
    } else {
      this._download(store, archive, organizationalUnitDisseminationPolicyIds, listPath);
    }
  }

  static downloadFileOnly(downloadService: DownloadService, dialog: MatDialog, archive: Archive, archiveDataFile: ArchiveDataFile): void {
    if (archive.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA) {
      ObservableUtil.takeOneThenUnsubscribe(
        this._openClickThroughDuaDialog(dialog, archive),
        accept => {
          if (isTrue(accept)) {
            this._downloadFileOnly(downloadService, archiveDataFile);
          }
        },
      );
    } else {
      this._downloadFileOnly(downloadService, archiveDataFile);
    }
  }

  private static _openClickThroughDuaDialog(dialog: MatDialog, archive: Archive): Observable<boolean> {
    return DialogUtil.open(dialog, HomeArchiveClickThroughDuaDownloadDialog, {
        archive: archive,
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      }, () => {
      });
  }

  private static _download(store: Store, archive: Archive, organizationalUnitDisseminationPolicyIds?: OrganizationalUnitDisseminationPolicyIds, listPath?: string[]): void {
    store.dispatch(new SharedArchiveAction.Download(archive,
      {
        disseminationPolicyId: organizationalUnitDisseminationPolicyIds?.disseminationPolicyId,
        organizationalUnitDisseminationPolicyId: organizationalUnitDisseminationPolicyIds?.organizationalUnitDisseminationPolicyId,
        subsetItemList: listPath?.map(p => ({itemPath: p})),
      }));
  }

  private static _downloadFileOnly(downloadService: DownloadService, archiveDataFile: ArchiveDataFile): void {
    const url = `${ApiEnum.accessPublicMetadata}/${archiveDataFile.infoPackage.resId}/${ApiResourceNameEnum.DATAFILE}/${archiveDataFile.resId}/${ApiActionNameEnum.DL}`;
    downloadService.download(url, archiveDataFile.fileName, archiveDataFile.fileSize, false, archiveDataFile.mimetype);
  }
}
