/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {
  AppRoutesEnum,
  HomePageRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {DlcmRoutes} from "@app/shared/models/dlcm-route.model";
import {HomeArchiveCollectionListRoutable} from "@home/components/routables/home-archive-collection-list/home-archive-collection-list.routable";
import {HomeArchiveDetailRoutable} from "@home/components/routables/home-archive-detail/home-archive-detail.routable";
import {HomeArchiveFileDetailRoutable} from "@home/components/routables/home-archive-file-detail/home-archive-file-detail.routable";
import {HomeArchiveFileListRoutable} from "@home/components/routables/home-archive-file-list/home-archive-file-list.routable";
import {HomeArchiveNotFoundRoutable} from "@home/components/routables/home-archive-not-found/home-archive-not-found.routable";
import {HomePageRoutable} from "@home/components/routables/home-page/home-page.routable";
import {HomeSearchRoutable} from "@home/components/routables/home-search/home-search.routable";
import {HomeState} from "@home/stores/home.state";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {ArchiveGuardService} from "@shared/guards/archive-guard.service";
import {CombinedGuardService} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: "",
    component: HomePageRoutable,
    data: {},
  },
  {
    path: HomePageRoutesEnum.search,
    component: HomeSearchRoutable,
  },
  {
    path: HomePageRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: HomeArchiveDetailRoutable,
    data: {
      breadcrumbMemoizedSelector: HomeState.currentTitle,
      guards: [ArchiveGuardService],
    },
    canActivate: [CombinedGuardService],
    children: [
      {
        path: HomePageRoutesEnum.files,
        component: HomeArchiveFileListRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.files,
        },
        children: [
          {
            path: AppRoutesEnum.paramIdDataFile,
            component: HomeArchiveFileDetailRoutable,
            data: {
              breadcrumb: LabelTranslateEnum.detail,
            },
          },
        ],
      },
      {
        path: HomePageRoutesEnum.collections,
        component: HomeArchiveCollectionListRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.collections,
        },
      },
    ],
  },
  {
    path: HomePageRoutesEnum.notFound + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: HomeArchiveNotFoundRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.archiveNotFound,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {
}
