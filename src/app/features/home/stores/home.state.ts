/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {
  Injectable,
  makeStateKey,
} from "@angular/core";
import {HomeAction} from "@app/features/home/stores/home.action";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {HomeHelper} from "@home/helpers/home.helper";
import {ArchiveAccessRightService} from "@home/services/archive-access-right.service";
import {HomeArchiveCollectionAction} from "@home/stores/archive/collection/home-archive-collection.action";
import {HomeArchiveDataFileAction} from "@home/stores/archive/data-file/home-archive-data-file.action";
import {HomeArchiveDataFileState} from "@home/stores/archive/data-file/home-archive-data-file.state";
import {HomeArchivePackageAction} from "@home/stores/archive/package/home-archive-package.action";
import {HomeArchivePackageState} from "@home/stores/archive/package/home-archive-package.state";
import {HomeArchiveRatingAction} from "@home/stores/archive/rating/home-archive-rating.action";
import {HomeArchiveRatingState} from "@home/stores/archive/rating/home-archive-rating.state";
import {
  Archive,
  ArchiveMetadata,
  ArchiveStatisticsDto,
  SearchCondition,
} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {ArchiveHelper} from "@shared/helpers/archive.helper";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  take,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BasicState,
  Citation,
  CollectionTyped,
  defaultResourceStateInitValue,
  Facet,
  isEmptyString,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  NotificationService,
  QueryParameters,
  QueryParametersUtil,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
  StringUtil,
  TransferStateService,
} from "solidify-frontend";

export const HOME_SEARCH_ALL: string = "*";
const DEFAULT_VIEW_MODE_TABLE: ViewModeTableEnum = environment.defaultHomeViewModeTableEnum;

export interface HomeStateModel extends ResourceStateModel<Archive> {
  search: string;
  searchModeIsQuery: boolean;
  facets: Facet[];
  viewModeTableEnum: ViewModeTableEnum;
  listRelativeArchive: Archive[];
  facetsSelected: MappingObject<Enums.Facet.Name, string[]>;
  archiveStatisticDto: ArchiveStatisticsDto;
  bibliographies: Citation[];
  citations: Citation[];
}

@Injectable()
@State<HomeStateModel>({
  name: StateEnum.home,
  defaults: {
    ...defaultResourceStateInitValue(),
    search: StringUtil.stringEmpty,
    searchModeIsQuery: false,
    viewModeTableEnum: DEFAULT_VIEW_MODE_TABLE,
    queryParameters: new QueryParameters(environment.defaultPageSizeHomePage),
    listRelativeArchive: undefined,
    facets: undefined,
    facetsSelected: {} as MappingObject<Enums.Facet.Name, string[]>,
    archiveStatisticDto: undefined,
    bibliographies: [],
    citations: [],
  },
  children: [
    HomeArchiveDataFileState,
    HomeArchivePackageState,
    HomeArchiveRatingState,
    // HomeArchiveCollectionState,
  ],
})
export class HomeState extends BasicState<HomeStateModel> {
  protected get _urlResource(): string {
    return ApiEnum.accessPublicMetadata;
  }

  constructor(private readonly _apiService: ApiService,
              private readonly _store: Store,
              private readonly _httpClient: HttpClient,
              private readonly _notificationService: NotificationService,
              private readonly _actions$: Actions,
              private readonly _transferState: TransferStateService,
              private readonly _archiveAccessRightService: ArchiveAccessRightService) {
    super();
  }

  private readonly _PATH_FILTER: string = "query";
  private readonly _DEFAULT_SEARCH: string = HOME_SEARCH_ALL;
  private readonly _SEARCH_GLOBAL_PARAM: string = "globalSearch";

  @Selector()
  static current(state: HomeStateModel): Archive {
    return state.current;
  }

  @Selector()
  static search(state: HomeStateModel): string {
    return state.search;
  }

  @Selector()
  static isLoading(state: HomeStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static currentTitle(state: HomeStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.title;
  }

  @Action(HomeAction.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });
    ctx.dispatch(new HomeAction.Search(false, ctx.getState().search, ctx.getState().searchModeIsQuery, ctx.getState().facetsSelected));
  }

  @Action(HomeAction.Search)
  search(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.Search): Observable<CollectionTyped<ArchiveMetadata>> {
    const searchString = this._getSearchStringToApply(ctx, action);
    const viewMode = this._getViewModeToApply(ctx, action);
    let queryParameters = StoreUtil.getQueryParametersToApply(action.queryParameters, ctx);
    if (action.resetPagination) {
      queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      search: searchString,
      searchModeIsQuery: action.searchModeIsQuery,
      facetsSelected: action.facetsSelected,
      viewModeTableEnum: viewMode,
      queryParameters: queryParameters,
    });

    const facetsSelectedForBackend = HomeHelper.getFacetsSelectedForBackend(action.facetsSelected);

    if (action.searchModeIsQuery) {
      queryParameters = QueryParametersUtil.clone(queryParameters);
      MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), this._PATH_FILTER, isEmptyString(searchString) ? HOME_SEARCH_ALL : searchString);
    } else if (!isNullOrUndefined(searchString) && !isEmptyString(searchString)) {
      facetsSelectedForBackend.push({
        field: this._SEARCH_GLOBAL_PARAM,
        value: searchString,
        type: Enums.SearchCondition.Type.MATCH,
        booleanClauseType: Enums.SearchCondition.BooleanClauseType.MUST,
      } as SearchCondition);
    }
    return this._apiService.postQueryParameters<SearchCondition[], CollectionTyped<ArchiveMetadata>>(this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH, facetsSelectedForBackend, queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [HomeAction.Search]),
        tap((collection: CollectionTyped<ArchiveMetadata>) => {
          const collectionArchive = {
            _links: collection._links,
            _page: collection._page,
            _facets: collection._facets,
          } as CollectionTyped<Archive>;
          collectionArchive._data = ArchiveHelper.adaptListArchivesMetadataInArchive(collection._data);
          ctx.dispatch(new HomeAction.SearchSuccess(action, collectionArchive));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.SearchFail(action));
          throw error;
        }),
      );
  }

  private _getSearchStringToApply(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.Search): string {
    if (isNullOrUndefined(action.search)) {
      return this._getDefaultSearchStringIfNotDefined(ctx);
    }
    return action.search;
  }

  private _getDefaultSearchStringIfNotDefined(ctx: SolidifyStateContext<HomeStateModel>): string {
    if (ctx.getState().search === null || ctx.getState().search === undefined) {
      return this._DEFAULT_SEARCH;
    }
    return ctx.getState().search;
  }

  @Action(HomeAction.SearchSuccess)
  searchSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.collection);

    ctx.patchState({
      list: isNullOrUndefined(action.collection) ? [] : action.collection._data,
      total: isNullOrUndefined(action.collection) ? 0 : action.collection._page.totalItems,
      facets: isNullOrUndefined(action.collection) ? [] : action.collection._facets,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters,
    });
  }

  @Action(HomeAction.SearchFail)
  searchFail(ctx: SolidifyStateContext<HomeStateModel>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      list: [],
      total: 0,
    });
  }

  @Action(HomeAction.SearchRelativeArchive)
  searchRelativeArchive(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchRelativeArchive): Observable<CollectionTyped<Archive>> {
    const searchString = `organizationalunitid=${action.archive.organizationalUnitId}`;

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const queryParameters = new QueryParameters(5);
    MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), this._PATH_FILTER, searchString);

    const STATE_KEY = makeStateKey<CollectionTyped<Archive>>(`HomeState-SearchRelativeArchive-${action.archive.organizationalUnitId}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const collectionTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new HomeAction.SearchRelativeArchiveSuccess(action, collectionTransferState));
      return of(collectionTransferState);
    }

    return this._apiService.getCollection<ArchiveMetadata>(this._urlResource + urlSeparator + "search", queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [HomeAction.Search]),
        map((collection: CollectionTyped<ArchiveMetadata>) => {
          const listArchiveMetadata = collection._data;
          const listArchive = ArchiveHelper.adaptListArchivesMetadataInArchive(listArchiveMetadata);
          const collectionArchive = {
            _links: collection._links,
            _page: collection._page,
            _facets: collection._facets,
            _data: listArchive,
          } as CollectionTyped<Archive>;
          this._transferState.set(STATE_KEY, collectionArchive);
          ctx.dispatch(new HomeAction.SearchRelativeArchiveSuccess(action, collectionArchive));
          return collectionArchive;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.SearchRelativeArchiveFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.SearchRelativeArchiveSuccess)
  searchRelativeArchiveSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchRelativeArchiveSuccess): void {
    ctx.patchState({
      listRelativeArchive: isNullOrUndefined(action.list) ? [] : action.list._data.filter(archive => archive.resId !== action.parentAction.archive.resId),
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.SearchRelativeArchiveFail)
  searchRelativeArchiveFail(ctx: SolidifyStateContext<HomeStateModel>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.SearchDetail)
  detail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchDetail): Observable<ArchiveMetadata> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      current: undefined,
      listRelativeArchive: undefined,
    });

    const STATE_KEY = makeStateKey<Archive>(`HomeState-GetById-${action.resId}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const archiveTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new HomeAction.SearchDetailSuccess(action, archiveTransferState));
      return of(archiveTransferState);
    }

    return this._apiService.getById<ArchiveMetadata>(this._urlResource, action.resId)
      .pipe(
        tap(model => {
          const archive = ArchiveHelper.adaptArchiveMetadataInArchive(model);
          this._transferState.set(STATE_KEY, archive);
          ctx.dispatch(new HomeAction.SearchDetailSuccess(action, archive));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.SearchDetailFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.SearchDetailSuccess)
  detailSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchDetailSuccess): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new HomeAction.SearchRelativeArchive(action.model));
  }

  @Action(HomeAction.SearchDetailFail)
  detailFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.SearchDetailFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.CleanCurrent)
  cleanCurrent(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.CleanCurrent): void {
    ctx.patchState({
      current: undefined,
      listRelativeArchive: undefined,
      archiveStatisticDto: undefined,
      citations: [],
      bibliographies: [],
    });
    ctx.dispatch(new HomeArchivePackageAction.CleanCurrent());
    ctx.dispatch(new HomeArchiveRatingAction.CleanCurrent());
    ctx.dispatch(new HomeArchiveDataFileAction.CleanCurrent());
    ctx.dispatch(new HomeArchiveCollectionAction.CleanCurrent());
  }

  private _getViewModeToApply(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.Search): ViewModeTableEnum {
    const currentViewModeInState = ctx.getState().viewModeTableEnum;
    const currentViewToUse = isNullOrUndefined(currentViewModeInState) ? DEFAULT_VIEW_MODE_TABLE : currentViewModeInState;
    return isNullOrUndefined(action.viewMode) ? currentViewToUse : action.viewMode;
  }

  @Action(HomeAction.GetStatistics)
  getStatistics(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetStatistics): Observable<ArchiveStatisticsDto> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      archiveStatisticDto: undefined,
    });

    const STATE_KEY = makeStateKey<ArchiveStatisticsDto>(`HomeState-Statistics-${action.archiveId}`);
    if (this._transferState.hasKey(STATE_KEY)) {
      const statisticsTransferState = this._transferState.get(STATE_KEY, undefined);
      this._transferState.remove(STATE_KEY);
      ctx.dispatch(new HomeAction.GetStatisticsSuccess(action, statisticsTransferState));
      return of(statisticsTransferState);
    }

    return this._apiService.get<ArchiveStatisticsDto>(`${this._urlResource}/${action.archiveId}/${ApiActionNameEnum.STATISTICS}`)
      .pipe(
        tap((model: ArchiveStatisticsDto) => {
          this._transferState.set(STATE_KEY, model);
          ctx.dispatch(new HomeAction.GetStatisticsSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.GetStatisticsFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeAction.GetStatisticsSuccess)
  getStatisticsSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetStatisticsSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      archiveStatisticDto: action.archiveStatisticDto,
    });
  }

  @Action(HomeAction.GetStatisticsFail)
  getStatisticsFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.GetStatisticsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeAction.ForceRefreshIsDownloadAuthorized)
  forceRefreshIsDownloadAuthorized(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ForceRefreshIsDownloadAuthorized): Observable<boolean> {
    return this._archiveAccessRightService.forceRefreshIsDownloadAuthorizedByIdObs(action.archiveId).pipe(take(1));
  }

  @Action(HomeAction.ExportToOrcid)
  exportToOrcid(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ExportToOrcid): Observable<void> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, void>(`${ApiEnum.adminOrcidSynchronizations}/${ApiActionNameEnum.SYNCHRONIZE}/${action.archive.resId}`)
      .pipe(
        tap(() => {
          ctx.dispatch(new HomeAction.ExportToOrcidSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeAction.ExportToOrcidFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeAction.ExportToOrcidSuccess)
  exportToOrcidSuccess(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ExportToOrcidSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(LabelTranslateEnum.notificationArchiveExportedToOrcidProfileSuccess);
  }

  @Action(HomeAction.ExportToOrcidFail)
  exportToOrcidFail(ctx: SolidifyStateContext<HomeStateModel>, action: HomeAction.ExportToOrcidFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(LabelTranslateEnum.notificationArchiveExportedToOrcidProfileFail);
  }
}

