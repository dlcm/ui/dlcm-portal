/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-rating.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {AppUserState} from "@app/stores/user/app-user.state";
import {environment} from "@environments/environment";
import {
  HomeArchiveRatingAction,
  homeArchiveRatingActionNameSpace,
} from "@home/stores/archive/rating/home-archive-rating.action";
import {HomeAction} from "@home/stores/home.action";
import {ArchiveUserRating} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  MemoizedUtil,
  NotificationService,
  OverrideDefaultAction,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export const defaultHomeArchiveRatingValue: () => HomeArchiveRatingStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
  });

export interface HomeArchiveRatingStateModel extends CompositionStateModel<ArchiveUserRating> {
}

@Injectable()
@State<HomeArchiveRatingStateModel>({
  name: StateEnum.home_archive_rating,
  defaults: {
    ...defaultHomeArchiveRatingValue(),
  },
  children: [],
})
export class HomeArchiveRatingState extends CompositionState<HomeArchiveRatingStateModel, ArchiveUserRating> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: homeArchiveRatingActionNameSpace,
      resourceName: ApiActionNameEnum.RATING,
      keepCurrentStateAfterCreate: true,
      keepCurrentStateAfterUpdate: true,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessPublicMetadata;
  }

  protected override _evaluateSubResourceUrl(parentId: string): string {
    return this._urlResource + urlSeparator + parentId + urlSeparator + this._resourceName;
  }

  @Action(HomeArchiveRatingAction.GetAllByCurrentUser)
  getAllByCurrentUser(ctx: SolidifyStateContext<HomeArchiveRatingStateModel>, action: HomeArchiveRatingAction.GetAllByCurrentUser): Observable<CollectionTyped<ArchiveUserRating>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        list: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    const url = this._evaluateSubResourceUrl(action.parentId) + urlSeparator + ApiActionNameEnum.RATING_BY_USER;
    return this._apiService.getCollection<ArchiveUserRating>(url, ctx.getState().queryParameters)
      .pipe(
        tap((collection: CollectionTyped<ArchiveUserRating>) => {
          ctx.dispatch(new HomeArchiveRatingAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeArchiveRatingAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(HomeArchiveRatingAction.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<HomeArchiveRatingStateModel>, action: HomeArchiveRatingAction.GetAllSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    ctx.patchState({
      list: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: queryParameters,
    });
  }

  @OverrideDefaultAction()
  @Action(HomeArchiveRatingAction.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<HomeArchiveRatingStateModel>, action: HomeArchiveRatingAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @OverrideDefaultAction()
  @Action(HomeArchiveRatingAction.Create)
  create(ctx: SolidifyStateContext<HomeArchiveRatingStateModel>, action: HomeArchiveRatingAction.Create): Observable<ArchiveUserRating> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.post<ArchiveUserRating>(url, undefined, {
      ratingType: action.model.ratingType.resId,
      grade: action.model.grade,
    } as any)
      .pipe(
        tap(model => {
          ctx.dispatch(new HomeArchiveRatingAction.CreateSuccess(action, action.parentId, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeArchiveRatingAction.CreateFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(HomeArchiveRatingAction.Update)
  update(ctx: SolidifyStateContext<HomeArchiveRatingStateModel>, action: HomeArchiveRatingAction.Update): Observable<ArchiveUserRating> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const rating = action.model;
    rating.user = MemoizedUtil.selectSnapshot(this._store, AppUserState, state => state.current);
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.patch<ArchiveUserRating>(url, action.model)
      .pipe(
        tap(model => {
          ctx.dispatch(new HomeArchiveRatingAction.UpdateSuccess(action, action.parentId, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeArchiveRatingAction.UpdateFail(action, action.parentId));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(HomeArchiveRatingAction.CreateSuccess)
  createSuccess(ctx: SolidifyStateContext<HomeArchiveRatingStateModel>, action: HomeArchiveRatingAction.CreateSuccess): void {
    super.createSuccess(ctx, action);
    this._notificationService.showSuccess(LabelTranslateEnum.evaluationTakenIntoAccount);
    ctx.dispatch([
      new HomeAction.GetStatistics(action.parentId),
      new HomeArchiveRatingAction.GetAllByCurrentUser(action.parentId),
    ]);
  }

  @OverrideDefaultAction()
  @Action(HomeArchiveRatingAction.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<HomeArchiveRatingStateModel>, action: HomeArchiveRatingAction.UpdateSuccess): void {
    super.updateSuccess(ctx, action);
    this._notificationService.showSuccess(LabelTranslateEnum.evaluationTakenIntoAccount);
    ctx.dispatch([
      new HomeAction.GetStatistics(action.parentId),
      new HomeArchiveRatingAction.GetAllByCurrentUser(action.parentId),
    ]);
  }

  @Action(HomeArchiveRatingAction.CleanCurrent)
  cleanCurrent(ctx: SolidifyStateContext<HomeArchiveRatingStateModel>, action: HomeArchiveRatingAction.CleanCurrent): void {
    ctx.patchState({
      ...defaultHomeArchiveRatingValue(),
    });
  }
}
