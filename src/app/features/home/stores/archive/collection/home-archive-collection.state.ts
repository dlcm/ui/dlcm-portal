/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-collection.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {HomeArchiveCollectionAction} from "@home/stores/archive/collection/home-archive-collection.action";
import {ArchiveDataFile} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ArchiveDataFileHelper} from "@shared/helpers/archive-data-file.helper";
import {ArchiveMetadataDataFile} from "@shared/models/business/archive-metadata-data-file.model";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseResourceStateModel,
  BasicState,
  CollectionTyped,
  isTrue,
  NotificationService,
  ofSolidifyActionCompleted,
  QueryParameters,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export const defaultHomeArchiveCollectionValue: () => HomeArchiveCollectionStateModel = () =>
  ({
    isLoadingCounter: 0,
    numberFiles: undefined,
    queryParameters: new QueryParameters(),
    total: 0,
    list: [],
    current: undefined,
  });

export interface HomeArchiveCollectionStateModel extends BaseResourceStateModel {
  numberFiles: number | undefined;
  list: ArchiveDataFile[];
  current: ArchiveDataFile;
}

@Injectable()
@State<HomeArchiveCollectionStateModel>({
  name: StateEnum.home_archive_collection,
  defaults: {
    ...defaultHomeArchiveCollectionValue(),
  },
  children: [],
})
export class HomeArchiveCollectionState extends BasicState<HomeArchiveCollectionStateModel> {
  private readonly _resourceName: ApiResourceNameEnum = ApiResourceNameEnum.AIP;

  protected get _urlResource(): string {
    return ApiEnum.accessPublicMetadata;
  }

  private _evaluateSubResourceUrl(parentId: string): string {
    return this._urlResource + urlSeparator + parentId + urlSeparator + this._resourceName;
  }

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _httpClient: HttpClient,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super();
  }

  @Action(HomeArchiveCollectionAction.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<HomeArchiveCollectionStateModel>, action: HomeArchiveCollectionAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });
    if (isTrue(action.getAllAfterChange)) {
      ctx.dispatch(new HomeArchiveCollectionAction.GetAll(action.parentId, undefined, action.keepCurrentContext));
    }
  }

  @Action(HomeArchiveCollectionAction.GetAll)
  getAll(ctx: SolidifyStateContext<HomeArchiveCollectionStateModel>, action: HomeArchiveCollectionAction.GetAll): Observable<CollectionTyped<ArchiveMetadataDataFile>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        list: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getCollection<ArchiveMetadataDataFile>(url, ctx.getState().queryParameters)
      .pipe(
        tap((collection: CollectionTyped<ArchiveMetadataDataFile>) => {
          collection._data = ArchiveDataFileHelper.adaptListArchivesMetadataInArchive(collection._data) as ArchiveDataFile[] | any;
          ctx.dispatch(new HomeArchiveCollectionAction.GetAllSuccess(action, collection as CollectionTyped<ArchiveDataFile>));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeArchiveCollectionAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeArchiveCollectionAction.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<HomeArchiveCollectionStateModel>, action: HomeArchiveCollectionAction.GetAllSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx as any, action.list);

    ctx.patchState({
      list: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: queryParameters,
    });
  }

  @Action(HomeArchiveCollectionAction.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<HomeArchiveCollectionStateModel>, action: HomeArchiveCollectionAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchiveCollectionAction.GetById)
  getById(ctx: SolidifyStateContext<HomeArchiveCollectionStateModel>, action: HomeArchiveCollectionAction.GetById): Observable<ArchiveDataFile> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      current: undefined,
    });

    return this._apiService.getById<ArchiveMetadataDataFile>(this._evaluateSubResourceUrl(action.parentId), action.resId)
      .pipe(
        tap(model => ctx.dispatch(new HomeArchiveCollectionAction.GetByIdSuccess(action, ArchiveDataFileHelper.adaptArchiveMetadataInArchiveDataFile(model)))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeArchiveCollectionAction.GetByIdFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeArchiveCollectionAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<HomeArchiveCollectionStateModel>, action: HomeArchiveCollectionAction.GetByIdSuccess): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchiveCollectionAction.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<HomeArchiveCollectionStateModel>, action: HomeArchiveCollectionAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchiveCollectionAction.Refresh)
  refresh(ctx: SolidifyStateContext<HomeArchiveCollectionStateModel>, action: HomeArchiveCollectionAction.Refresh): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new HomeArchiveCollectionAction.GetAll(action.parentId, undefined, true),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(HomeArchiveCollectionAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(HomeArchiveCollectionAction.GetAllFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new HomeArchiveCollectionAction.RefreshSuccess(action));
        } else {
          ctx.dispatch(new HomeArchiveCollectionAction.RefreshFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(HomeArchiveCollectionAction.RefreshSuccess)
  refreshSuccess(ctx: SolidifyStateContext<HomeArchiveCollectionStateModel>, action: HomeArchiveCollectionAction.RefreshSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchiveCollectionAction.RefreshFail)
  refreshFail(ctx: SolidifyStateContext<HomeArchiveCollectionStateModel>, action: HomeArchiveCollectionAction.RefreshFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchiveCollectionAction.CleanCurrent)
  cleanCurrent(ctx: SolidifyStateContext<HomeArchiveCollectionStateModel>, action: HomeArchiveCollectionAction.CleanCurrent): void {
    ctx.patchState({
      ...defaultHomeArchiveCollectionValue(),
    });
  }
}
