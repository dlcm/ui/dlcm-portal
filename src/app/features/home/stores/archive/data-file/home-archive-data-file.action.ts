/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-data-file.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ArchiveDataFile} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  CollectionTyped,
  QueryParameters,
} from "solidify-frontend";

const state = StateEnum.home_archive_dataFile;

export namespace HomeArchiveDataFileAction {
  export class ChangeQueryParameters extends BaseAction {
    static readonly type: string = `[${state}] Change Query Parameters`;

    constructor(public parentId: string, public queryParameters: QueryParameters, public keepCurrentContext: boolean = false, public getAllAfterChange: boolean = true) {
      super();
    }
  }

  export class GetAll extends BaseAction {
    static readonly type: string = `[${state}] Get All`;

    constructor(public parentId: string, public queryParameters?: QueryParameters, public keepCurrentContext: boolean = false) {
      super();
    }
  }

  export class GetAllSuccess extends BaseSubActionSuccess<GetAll> {
    static readonly type: string = `[${state}] Get All Success`;

    constructor(public parentAction: GetAll, public list: CollectionTyped<ArchiveDataFile>) {
      super(parentAction);
    }
  }

  export class GetAllFail extends BaseSubActionFail<GetAll> {
    static readonly type: string = `[${state}] Get All Fail`;
  }

  export class GetById extends BaseAction {
    static readonly type: string = `[${state}] Get By Id`;

    constructor(public parentId: string, public resId: string, public keepCurrentContext: boolean = false) {
      super();
    }
  }

  export class GetByIdSuccess extends BaseSubActionSuccess<GetById> {
    static readonly type: string = `[${state}] Get By Id Success`;

    constructor(public parentAction: GetById, public model: ArchiveDataFile) {
      super(parentAction);
    }
  }

  export class GetByIdFail extends BaseSubActionFail<GetById> {
    static readonly type: string = `[${state}] Get By Id Fail`;
  }

  export class ChangeCurrentFolder {
    static readonly type: string = `[${state}] Change Current Folder`;

    constructor(public newCurrentFolder: string, public refreshNewFolderContent: boolean = false) {
    }
  }

  export class CheckDataFileIfIsReadyDownload extends BaseAction {
    static readonly type: string = `[${state}] Check Data File If Is Ready Download`;

    constructor(public parentId: string, public id: string) {
      super();
    }
  }

  export class CheckDataFileIfIsReadyDownloadSuccess extends BaseSubActionSuccess<CheckDataFileIfIsReadyDownload> {
    static readonly type: string = `[${state}] Check Data File If Is Ready Download Success`;
  }

  export class CheckDataFileIfIsReadyDownloadFail extends BaseSubActionFail<CheckDataFileIfIsReadyDownload> {
    static readonly type: string = `[${state}] Check Data File If Is Ready Download Fail`;
  }

  export class Refresh extends BaseAction {
    static readonly type: string = `[${state}] Refresh`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class RefreshSuccess extends BaseSubActionSuccess<Refresh> {
    static readonly type: string = `[${state}] Refresh Success`;
  }

  export class RefreshFail extends BaseSubActionFail<Refresh> {
    static readonly type: string = `[${state}] Refresh Fail`;
  }

  export class GetListFolder extends BaseAction {
    static readonly type: string = `[${state}] Get List Folder`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class GetListFolderSuccess extends BaseSubActionSuccess<GetListFolder> {
    static readonly type: string = `[${state}] Get List Folder Success`;

    constructor(public parentAction: GetListFolder, public folder: string[]) {
      super(parentAction);
    }
  }

  export class GetListFolderFail extends BaseSubActionFail<GetListFolder> {
    static readonly type: string = `[${state}] Get List Folder Fail`;

    constructor(public parentAction: GetListFolder) {
      super(parentAction);
    }
  }

  export class CleanCurrent {
    static readonly type: string = `[${state}] Clean Current`;
  }
}
