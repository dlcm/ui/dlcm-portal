/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-data-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {DepositDataFileHelper} from "@deposit/helpers/deposit-data-file.helper";
import {HomeArchiveDataFileAction} from "@home/stores/archive/data-file/home-archive-data-file.action";
import {HomeState} from "@home/stores/home.state";
import {ArchiveDataFile} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ArchiveDataFileHelper} from "@shared/helpers/archive-data-file.helper";
import {ArchiveMetadataDataFile} from "@shared/models/business/archive-metadata-data-file.model";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseResourceStateModel,
  BasicState,
  CollectionTyped,
  DownloadService,
  isNullOrUndefined,
  isTrue,
  MappingObjectUtil,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  QueryParameters,
  QueryParametersUtil,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export const defaultHomeArchiveDataFileValue: () => HomeArchiveDataFileStateModel = () =>
  ({
    isLoadingCounter: 0,
    currentFolder: "" + DepositDataFileHelper.ROOT,
    folders: [],
    foldersWithIntermediateFolders: [],
    intermediateFolders: [],
    numberFiles: undefined,
    queryParameters: new QueryParameters(),
    total: 0,
    list: [],
    current: undefined,
    readyForDownload: undefined,
  });

export interface HomeArchiveDataFileStateModel extends BaseResourceStateModel {
  currentFolder: string;
  folders: string[];
  foldersWithIntermediateFolders: string[];
  intermediateFolders: string[];
  numberFiles: number | undefined;
  list: ArchiveDataFile[];
  current: ArchiveDataFile;
  readyForDownload: boolean | undefined;
}

@Injectable()
@State<HomeArchiveDataFileStateModel>({
  name: StateEnum.home_archive_dataFile,
  defaults: {
    ...defaultHomeArchiveDataFileValue(),
  },
  children: [],
})
export class HomeArchiveDataFileState extends BasicState<HomeArchiveDataFileStateModel> {
  private readonly _resourceName: ApiResourceNameEnum = ApiResourceNameEnum.DATAFILE;
  private readonly _KEY_PATH: string = "path";

  protected get _urlResource(): string {
    return ApiEnum.accessPublicMetadata;
  }

  private _evaluateSubResourceUrl(parentId: string): string {
    return this._urlResource + urlSeparator + parentId + urlSeparator + this._resourceName;
  }

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _httpClient: HttpClient,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
  ) {
    super();
  }

  @Action(HomeArchiveDataFileAction.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });
    if (isTrue(action.getAllAfterChange)) {
      ctx.dispatch(new HomeArchiveDataFileAction.GetAll(action.parentId, undefined, action.keepCurrentContext));
    }
  }

  @Action(HomeArchiveDataFileAction.GetAll)
  getAll(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.GetAll): Observable<CollectionTyped<ArchiveMetadataDataFile>> {
    let queryParameters = action.queryParameters;
    if (isNullOrUndefined(queryParameters)) {
      queryParameters = QueryParametersUtil.clone(ctx.getState().queryParameters);
    }
    const currentFolder = ctx.getState().currentFolder;
    if (isNullOrUndefined(currentFolder)) {
      MappingObjectUtil.delete(QueryParametersUtil.getSearchItems(queryParameters), this._KEY_PATH);

    } else {
      MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), this._KEY_PATH, ctx.getState().currentFolder);
    }
    ctx.patchState({
      queryParameters: queryParameters,
    });

    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        total: 0,
        list: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    const url = this._evaluateSubResourceUrl(action.parentId);
    return this._apiService.getCollection<ArchiveMetadataDataFile>(url, ctx.getState().queryParameters)
      .pipe(
        tap((collection: CollectionTyped<ArchiveMetadataDataFile>) => {
          collection._data = ArchiveDataFileHelper.adaptListArchivesMetadataInArchive(collection._data) as ArchiveDataFile[] | any;
          ctx.dispatch(new HomeArchiveDataFileAction.GetAllSuccess(action, collection as CollectionTyped<ArchiveDataFile>));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeArchiveDataFileAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeArchiveDataFileAction.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.GetAllSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx as any, action.list);

    ctx.patchState({
      list: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: queryParameters,
    });
  }

  @Action(HomeArchiveDataFileAction.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchiveDataFileAction.GetById)
  getById(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.GetById): Observable<ArchiveDataFile> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      current: undefined,
    });

    return this._apiService.getById<ArchiveMetadataDataFile>(this._evaluateSubResourceUrl(action.parentId), action.resId)
      .pipe(
        tap(model => ctx.dispatch(new HomeArchiveDataFileAction.GetByIdSuccess(action, ArchiveDataFileHelper.adaptArchiveMetadataInArchiveDataFile(model)))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeArchiveDataFileAction.GetByIdFail(action));
          throw error;
        }),
      );
  }

  @Action(HomeArchiveDataFileAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.GetByIdSuccess): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchiveDataFileAction.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchiveDataFileAction.Refresh)
  refresh(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.Refresh): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new HomeArchiveDataFileAction.GetAll(action.parentId, undefined, true),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(HomeArchiveDataFileAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(HomeArchiveDataFileAction.GetAllFail)),
        ],
      },
      {
        action: new HomeArchiveDataFileAction.GetListFolder(action.parentId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(HomeArchiveDataFileAction.GetListFolderSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(HomeArchiveDataFileAction.GetListFolderFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new HomeArchiveDataFileAction.RefreshSuccess(action));
        } else {
          ctx.dispatch(new HomeArchiveDataFileAction.RefreshFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(HomeArchiveDataFileAction.RefreshSuccess)
  refreshSuccess(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.RefreshSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchiveDataFileAction.RefreshFail)
  refreshFail(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.RefreshFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchiveDataFileAction.GetListFolder)
  getListFolder(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.GetListFolder): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._httpClient.get<string[]>(`${this._urlResource}/${action.parentId}/${ApiActionNameEnum.LIST_FOLDERS}`)
      .pipe(
        tap((listFolder: string[]) => {
          ctx.dispatch(new HomeArchiveDataFileAction.GetListFolderSuccess(action, listFolder));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeArchiveDataFileAction.GetListFolderFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(HomeArchiveDataFileAction.GetListFolderSuccess)
  getListFolderSuccess(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.GetListFolderSuccess): void {
    const folders = [...action.folder];
    const foldersWithIntermediateFolders = action.folder;
    const intermediateFolders = DepositDataFileHelper.createIntermediateFolders(foldersWithIntermediateFolders);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      folders,
      foldersWithIntermediateFolders,
      intermediateFolders,
    });
  }

  @Action(HomeArchiveDataFileAction.GetListFolderFail)
  getListFolderFail(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.GetListFolderFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchiveDataFileAction.ChangeCurrentFolder)
  changeCurrentFolder(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.ChangeCurrentFolder): void {
    ctx.patchState({
      currentFolder: action.newCurrentFolder,
      list: undefined,
    });
    const archiveId = MemoizedUtil.selectSnapshot(this._store, HomeState, state => state.current?.resId);
    if (!isNullOrUndefined(action.newCurrentFolder)) {
      ctx.dispatch(new HomeArchiveDataFileAction.GetListFolder(archiveId));
    }
    if (action.refreshNewFolderContent) {
      let queryParameters = QueryParametersUtil.clone(ctx.getState().queryParameters);
      queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
      ctx.dispatch(new HomeArchiveDataFileAction.GetAll(archiveId, queryParameters));
    }
  }

  @Action(HomeArchiveDataFileAction.CheckDataFileIfIsReadyDownload)
  checkDataFileIfIsReadyDownload(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.CheckDataFileIfIsReadyDownload): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.get<void>(`${this._evaluateSubResourceUrl(action.parentId)}/${action.id}/${ApiActionNameEnum.READY_FOR_DOWNLOAD}`)
      .pipe(
        map(() => {
          ctx.dispatch(new HomeArchiveDataFileAction.CheckDataFileIfIsReadyDownloadSuccess(action));
          return true;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeArchiveDataFileAction.CheckDataFileIfIsReadyDownloadFail(action));
          return of(false);
        }),
      );
  }

  @Action(HomeArchiveDataFileAction.CheckDataFileIfIsReadyDownloadSuccess)
  checkDataFileIfIsReadyDownloadSuccess(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.CheckDataFileIfIsReadyDownloadSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      readyForDownload: true,
    });
  }

  @Action(HomeArchiveDataFileAction.CheckDataFileIfIsReadyDownloadFail)
  checkDataFileIfIsReadyDownloadFail(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.CheckDataFileIfIsReadyDownloadFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      readyForDownload: false,
    });
  }

  @Action(HomeArchiveDataFileAction.CleanCurrent)
  cleanCurrent(ctx: SolidifyStateContext<HomeArchiveDataFileStateModel>, action: HomeArchiveDataFileAction.CleanCurrent): void {
    ctx.patchState({
      ...defaultHomeArchiveDataFileValue(),
    });
  }
}
