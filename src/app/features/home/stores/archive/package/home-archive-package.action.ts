/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-package.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {StateEnum} from "@shared/enums/state.enum";
import {ArchiveMetadataPackages} from "@shared/models/business/archive-metadata-packages.model";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
} from "solidify-frontend";

const state = StateEnum.home_archive_package;

export namespace HomeArchivePackageAction {
  export class GetById extends BaseAction {
    static readonly type: string = `[${state}] Get By Id`;

    constructor(public resId: string, public keepCurrentContext: boolean = false) {
      super();
    }
  }

  export class GetByIdSuccess extends BaseSubActionSuccess<GetById> {
    static readonly type: string = `[${state}] Get By Id Success`;

    constructor(public parentAction: GetById, public model: ArchiveMetadataPackages) {
      super(parentAction);
    }
  }

  export class GetByIdFail extends BaseSubActionFail<GetById> {
    static readonly type: string = `[${state}] Get By Id Fail`;
  }

  export class CleanCurrent {
    static readonly type: string = `[${state}] Clean Current`;
  }
}
