/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home-archive-package.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {HomeArchivePackageAction} from "@home/stores/archive/package/home-archive-package.action";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ArchiveMetadataPackages} from "@shared/models/business/archive-metadata-packages.model";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseStateModel,
  BasicState,
  isFalse,
  NotificationService,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
} from "solidify-frontend";

export const defaultHomeArchivePackageValue: () => HomeArchivePackageStateModel = () =>
  ({
    isLoadingCounter: 0,
    package: undefined,
  });

export interface HomeArchivePackageStateModel extends BaseStateModel {
  package: ArchiveMetadataPackages;
}

@Injectable()
@State<HomeArchivePackageStateModel>({
  name: StateEnum.home_archive_package,
  defaults: {
    ...defaultHomeArchivePackageValue(),
  },
  children: [],
})
export class HomeArchivePackageState extends BasicState<HomeArchivePackageStateModel> {
  private readonly _resourceName: ApiResourceNameEnum = ApiResourceNameEnum.PACKAGES;

  protected get _urlResource(): string {
    return ApiEnum.accessPublicMetadata;
  }

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _httpClient: HttpClient,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super();
  }

  @Action(HomeArchivePackageAction.GetById)
  getById(ctx: SolidifyStateContext<HomeArchivePackageStateModel>, action: HomeArchivePackageAction.GetById): Observable<ArchiveMetadataPackages> {
    const objectToPatch: HomeArchivePackageStateModel | any = {
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    };

    if (isFalse(action.keepCurrentContext)) {
      objectToPatch.package = undefined;
    }
    ctx.patchState(objectToPatch);

    return this._apiService.get<ArchiveMetadataPackages>(this._urlResource + urlSeparator + action.resId + urlSeparator + this._resourceName)
      .pipe(
        tap(model => ctx.dispatch(new HomeArchivePackageAction.GetByIdSuccess(action, model))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new HomeArchivePackageAction.GetByIdFail(action));
          throw environment.errorToSkipInErrorHandler;
        }),
      );
  }

  @Action(HomeArchivePackageAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<HomeArchivePackageStateModel>, action: HomeArchivePackageAction.GetByIdSuccess): void {
    ctx.patchState({
      package: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchivePackageAction.GetByIdFail)
  getByIdFail(ctx: SolidifyStateContext<HomeArchivePackageStateModel>, action: HomeArchivePackageAction.GetByIdFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(HomeArchivePackageAction.CleanCurrent)
  cleanCurrent(ctx: SolidifyStateContext<HomeArchivePackageStateModel>, action: HomeArchivePackageAction.CleanCurrent): void {
    ctx.patchState({
      ...defaultHomeArchivePackageValue(),
    });
  }
}
