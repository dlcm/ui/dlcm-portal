/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - home.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {
  Archive,
  ArchiveStatisticsDto,
} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeTableEnum} from "@shared/enums/view-mode-table.enum";
import {
  BaseAction,
  BaseSubAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  CollectionTyped,
  MappingObject,
  QueryParameters,
} from "solidify-frontend";

const state = StateEnum.home;

export namespace HomeAction {
  export class ChangeQueryParameters {
    static readonly type: string = `[${state}] Change Query Parameters`;

    constructor(public queryParameters?: QueryParameters) {
    }
  }

  export class Search extends BaseAction {
    static readonly type: string = `[${state}] Search`;

    constructor(public resetPagination: boolean, public search?: string, public searchModeIsQuery?: boolean, public facetsSelected?: MappingObject<Enums.Facet.Name, string[]>, public viewMode?: ViewModeTableEnum, public queryParameters?: QueryParameters) {
      super();
    }
  }

  export class SearchSuccess extends BaseSubAction<Search> {
    static readonly type: string = `[${state}] Search Success`;

    constructor(public parentAction: Search, public collection?: CollectionTyped<Archive> | null | undefined) {
      super(parentAction);
    }
  }

  export class SearchFail extends BaseSubAction<Search> {
    static readonly type: string = `[${state}] Search Fail`;
  }

  export class SearchRelativeArchive extends BaseAction {
    static readonly type: string = `[${state}] Search Relative Archive`;

    constructor(public archive: Archive) {
      super();
    }
  }

  export class SearchRelativeArchiveSuccess extends BaseSubActionSuccess<SearchRelativeArchive> {
    static readonly type: string = `[${state}] Search Relative Archive Success`;

    constructor(public parentAction: SearchRelativeArchive, public list: CollectionTyped<Archive> | null | undefined) {
      super(parentAction);
    }
  }

  export class SearchRelativeArchiveFail extends BaseSubActionFail<SearchRelativeArchive> {
    static readonly type: string = `[${state}] Search Relative Archive Fail`;
  }

  export class SearchDetail extends BaseAction {
    static readonly type: string = `[${state}] Search Detail`;

    constructor(public resId: string) {
      super();
    }
  }

  export class SearchDetailSuccess extends BaseSubActionSuccess<SearchDetail> {
    static readonly type: string = `[${state}] Search Detail Success`;

    constructor(public parentAction: SearchDetail, public model: Archive) {
      super(parentAction);
    }
  }

  export class SearchDetailFail extends BaseSubActionFail<SearchDetail> {
    static readonly type: string = `[${state}] Search Detail Fail`;
  }

  export class CleanCurrent {
    static readonly type: string = `[${state}] Clean Current`;
  }

  export class GetStatistics extends BaseAction {
    static readonly type: string = `[${state}] Get Statistics`;

    constructor(public archiveId: string) {
      super();
    }
  }

  export class GetStatisticsSuccess extends BaseSubActionSuccess<GetStatistics> {
    static readonly type: string = `[${state}] Get Statistics Success`;

    constructor(public parentAction: GetStatistics, public archiveStatisticDto: ArchiveStatisticsDto) {
      super(parentAction);
    }
  }

  export class GetStatisticsFail extends BaseSubActionFail<GetStatistics> {
    static readonly type: string = `[${state}] Get Statistics Fail`;
  }

  export class ForceRefreshIsDownloadAuthorized extends BaseAction {
    static readonly type: string = `[${state}] Force Refresh Is Download Authorized`;

    constructor(public archiveId: string) {
      super();
    }
  }

  export class ExportToOrcid extends BaseAction {
    static readonly type: string = `[${state}] Export To Orcid`;

    constructor(public archive: Archive) {
      super();
    }
  }

  export class ExportToOrcidSuccess extends BaseSubActionFail<ExportToOrcid> {
    static readonly type: string = `[${state}] Export To Orcid Success`;

    constructor(public parentAction: ExportToOrcid) {
      super(parentAction);
    }
  }

  export class ExportToOrcidFail extends BaseSubActionFail<ExportToOrcid> {
    static readonly type: string = `[${state}] Export To Orcid Fail`;

    constructor(public parentAction: ExportToOrcid) {
      super(parentAction);
    }
  }

}
