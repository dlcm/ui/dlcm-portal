/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  OrderAllOrderState,
  OrderAllOrderStateModel,
} from "@app/features/order/features/all-order/stores/order-all-order.state";
import {
  State,
  Store,
} from "@ngxs/store";
import {
  OrderMyOrderState,
  OrderMyOrderStateModel,
} from "@order/features/my-order/stores/order-my-order.state";
import {StateEnum} from "@shared/enums/state.enum";
import {BaseStateModel} from "solidify-frontend";

export interface OrderStateModel extends BaseStateModel {
  order_allOrder: OrderAllOrderStateModel | undefined;
  order_myOrder: OrderMyOrderStateModel | undefined;
}

@Injectable()
@State<OrderStateModel>({
  name: StateEnum.order,
  defaults: {
    isLoadingCounter: 0,
    order_allOrder: undefined,
    order_myOrder: undefined,
  },
  children: [
    OrderMyOrderState,
    OrderAllOrderState,
  ],
})
export class OrderState {
  constructor(protected readonly _store: Store) {
  }
}
