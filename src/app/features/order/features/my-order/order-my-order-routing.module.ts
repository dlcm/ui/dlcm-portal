/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-my-order-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {OrderMyOrderDetailEditRoutable} from "@order/features/my-order/components/routables/my-order-detail-edit/order-my-order-detail-edit.routable";
import {OrderMyOrderListRoutable} from "@order/features/my-order/components/routables/my-order-list/order-my-order-list.routable";
import {OrderMyOrderPendingRoutable} from "@order/features/my-order/components/routables/my-order-pending/order-my-order-pending.routable";
import {OrderMyOrderState} from "@order/features/my-order/stores/order-my-order.state";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  OrderRoutesEnum,
} from "@shared/enums/routes.enum";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";
import {
  CanDeactivateGuard,
  EmptyContainer,
} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: OrderMyOrderListRoutable,
    data: {},
  },
  {
    path: OrderRoutesEnum.myOrderDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: OrderMyOrderDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: OrderMyOrderState.currentTitle,
    },
    children: [
      {
        path: OrderRoutesEnum.myOrderEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
  {
    path: OrderRoutesEnum.myOrderDraft,
    component: OrderMyOrderPendingRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.pendingArchivesOrder,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderMyOrderRoutingModule {
}
