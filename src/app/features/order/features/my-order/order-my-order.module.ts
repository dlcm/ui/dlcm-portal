/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-my-order.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {OrderMyOrderPresentational} from "@order/features/my-order/components/presentationals/my-order/order-my-order.presentational";
import {OrderMyOrderListRoutable} from "@order/features/my-order/components/routables/my-order-list/order-my-order-list.routable";
import {OrderMyOrderPendingRoutable} from "@order/features/my-order/components/routables/my-order-pending/order-my-order-pending.routable";
import {OrderMyOrderRoutingModule} from "@order/features/my-order/order-my-order-routing.module";
import {OrderMyOrderAipState} from "@order/features/my-order/stores/aip/order-my-order-aip.state";
import {OrderMyOrderDipState} from "@order/features/my-order/stores/dip/order-my-order-dip.state";
import {OrderMyOrderState} from "@order/features/my-order/stores/order-my-order.state";
import {OrderMyOrderStatusHistoryState} from "@order/features/my-order/stores/status-history/order-my-order-status-history.state";
import {SharedModule} from "@shared/shared.module";
import {OrderMyOrderDetailEditRoutable} from "./components/routables/my-order-detail-edit/order-my-order-detail-edit.routable";

const routables = [
  OrderMyOrderListRoutable,
  OrderMyOrderDetailEditRoutable,
  OrderMyOrderPendingRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  OrderMyOrderPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    OrderMyOrderRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      OrderMyOrderState,
      OrderMyOrderStatusHistoryState,
      OrderMyOrderDipState,
      OrderMyOrderAipState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class OrderMyOrderModule {
}
