/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-my-order-aip.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {
  Aip,
  Archive,
  ArchiveMetadata,
} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  OrderMyOrderAipAction,
  orderMyOrderAipNamespace,
} from "@order/features/my-order/stores/aip/order-my-order-aip.action";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ArchiveHelper} from "@shared/helpers/archive.helper";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  AssociationState,
  AssociationStateModel,
  CollectionTyped,
  defaultAssociationStateInitValue,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  QueryParameters,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export const defaultOrderMyOrderAipStateInitValue: () => OrderMyOrderAipStateModel = () =>
  ({
    ...defaultAssociationStateInitValue(),
    listArchives: [],
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption),
  });

export interface OrderMyOrderAipStateModel extends AssociationStateModel<Aip> {
  listArchives: Archive[];
}

@Injectable()
@State<OrderMyOrderAipStateModel>({
  name: StateEnum.order_myOrder_aip,
  defaults: {
    ...defaultOrderMyOrderAipStateInitValue(),
  },
})
export class OrderMyOrderAipState extends AssociationState<OrderMyOrderAipStateModel, Aip> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: orderMyOrderAipNamespace,
      resourceName: ApiResourceNameEnum.AIP,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessOrders;
  }

  @OverrideDefaultAction()
  @Action(OrderMyOrderAipAction.GetAll)
  override getAll(ctx: SolidifyStateContext<OrderMyOrderAipStateModel>, action: OrderMyOrderAipAction.GetAll): Observable<CollectionTyped<any>> {
    ctx.patchState({
      listArchives: [],
    });
    return super.getAll(ctx, action);
  }

  @OverrideDefaultAction()
  @Action(OrderMyOrderAipAction.GetAllSuccess)
  override getAllSuccess(ctx: SolidifyStateContext<OrderMyOrderAipStateModel>, action: OrderMyOrderAipAction.GetAllSuccess): void {
    super.getAllSuccess(ctx, action);

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    this.subscribe(StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, action.list._data.map(aip => ({
        action: new OrderMyOrderAipAction.SearchDetail(aip.resId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(OrderMyOrderAipAction.SearchDetailSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(OrderMyOrderAipAction.SearchDetailFail)),
        ],
      })),
    ).pipe(
      map(result => {
        ctx.patchState({
          listArchives: result.listActionSuccess.map((actionSuccess: OrderMyOrderAipAction.SearchDetailSuccess) => actionSuccess.model),
          isLoadingCounter: ctx.getState().isLoadingCounter - 1,
        });
        return result.success;
      }),
    ));
  }

  @Action(OrderMyOrderAipAction.SearchDetail)
  searchDetail(ctx: SolidifyStateContext<OrderMyOrderAipStateModel>, action: OrderMyOrderAipAction.SearchDetail): Observable<ArchiveMetadata> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      current: undefined,
    });

    return this._apiService.getById<ArchiveMetadata>(ApiEnum.accessPublicMetadata, action.resId)
      .pipe(
        tap(model => {
          const archive = ArchiveHelper.adaptArchiveMetadataInArchive(model);
          ctx.dispatch(new OrderMyOrderAipAction.SearchDetailSuccess(action, archive));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new OrderMyOrderAipAction.SearchDetailFail(action));
          throw error;
        }),
      );
  }

  @Action(OrderMyOrderAipAction.SearchDetailSuccess)
  searchDetailSuccess(ctx: SolidifyStateContext<OrderMyOrderAipStateModel>, action: OrderMyOrderAipAction.SearchDetailSuccess): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(OrderMyOrderAipAction.SearchDetailFail)
  searchDetailFail(ctx: SolidifyStateContext<OrderMyOrderAipStateModel>, action: OrderMyOrderAipAction.SearchDetailFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
