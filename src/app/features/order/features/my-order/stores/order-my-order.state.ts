/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-my-order.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {
  Order,
  OrderArchive,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {
  defaultOrderMyOrderAipStateInitValue,
  OrderMyOrderAipState,
  OrderMyOrderAipStateModel,
} from "@order/features/my-order/stores/aip/order-my-order-aip.state";
import {
  OrderMyOrderDipState,
  OrderMyOrderDipStateModel,
} from "@order/features/my-order/stores/dip/order-my-order-dip.state";
import {
  OrderMyOrderAction,
  orderMyOrderActionNameSpace,
} from "@order/features/my-order/stores/order-my-order.action";
import {
  OrderMyOrderStatusHistoryState,
  OrderMyOrderStatusHistoryStateModel,
} from "@order/features/my-order/stores/status-history/order-my-order-status-history.state";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {
  Observable,
  pipe,
} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultAssociationStateInitValue,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  OverrideDefaultAction,
  QueryParametersUtil,
  ResourceActionHelper,
  ResourceState,
  ResourceStateModel,
  Result,
  ResultActionStatusEnum,
  SOLIDIFY_CONSTANTS,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface OrderMyOrderStateModel extends ResourceStateModel<Order> {
  [StateEnum.order_myOrder_statusHistory]: OrderMyOrderStatusHistoryStateModel;
  [StateEnum.order_myOrder_dip]: OrderMyOrderDipStateModel | undefined;
  [StateEnum.order_myOrder_aip]: OrderMyOrderAipStateModel | undefined;
}

@Injectable()
@State<OrderMyOrderStateModel>({
  name: StateEnum.order_myOrder,
  defaults: {
    ...defaultResourceStateInitValue(),
    [StateEnum.order_myOrder_statusHistory]: {...defaultStatusHistoryInitValue()},
    [StateEnum.order_myOrder_dip]: {...defaultAssociationStateInitValue()},
    [StateEnum.order_myOrder_aip]: {...defaultOrderMyOrderAipStateInitValue()},
  },
  children: [
    OrderMyOrderStatusHistoryState,
    OrderMyOrderDipState,
    OrderMyOrderAipState,
  ],
})
export class OrderMyOrderState extends ResourceState<OrderMyOrderStateModel, Order> {
  static readonly KEY_DISPLAY_PUBLIC: string = "onlyPublic";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: orderMyOrderActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.orderMyOrderDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.orderMyOrderDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.orderMyOrder,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("order.myOrder.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("order.myOrder.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("order.myOrder.notification.resource.update"),
      keepCurrentStateAfterUpdate: true,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessOrders;
  }

  @Selector()
  static isLoading(state: OrderMyOrderStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static currentTitle(state: OrderMyOrderStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isLoadingWithDependency(state: OrderMyOrderStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: OrderMyOrderStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: OrderMyOrderStateModel): boolean {
    return true;
  }

  @OverrideDefaultAction()
  @Action(OrderMyOrderAction.GetAll)
  getAll(ctx: SolidifyStateContext<OrderMyOrderStateModel>, action: OrderMyOrderAction.GetAll): Observable<CollectionTyped<Order>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        list: undefined,
        total: 0,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });

    const searchItems = QueryParametersUtil.getSearchItems(ctx.getState().queryParameters);
    const isPublicOrder = MappingObjectUtil.get(searchItems, OrderMyOrderState.KEY_DISPLAY_PUBLIC);
    const baseUrl = isPublicOrder === SOLIDIFY_CONSTANTS.STRING_TRUE ? this._urlResource + "/" + ApiActionNameEnum.ORDER_LIST_PUBLIC : this._urlResource + "/" + ApiActionNameEnum.ORDER_LIST_CREATED_BY_USER;

    return this._apiService.getCollection<Order>(baseUrl, ctx.getState().queryParameters)
      .pipe(
        action.cancelIncomplete ? StoreUtil.cancelUncompleted(action, ctx, this._actions$, [this._nameSpace.GetAll, Navigate]) : pipe(),
        tap((collection: CollectionTyped<Order>) => {
          ctx.dispatch(ResourceActionHelper.getAllSuccess<Order>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(OrderMyOrderAction.Submit)
  submit(ctx: SolidifyStateContext<OrderMyOrderStateModel>, action: OrderMyOrderAction.Submit): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(`${this._urlResource}/${action.resId}/${ApiActionNameEnum.SUBMIT}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new OrderMyOrderAction.SubmitSuccess(action));
          } else {
            ctx.dispatch(new OrderMyOrderAction.SubmitFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new OrderMyOrderAction.SubmitFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(OrderMyOrderAction.SubmitSuccess)
  submitSuccess(ctx: SolidifyStateContext<OrderMyOrderStateModel>, action: OrderMyOrderAction.SubmitSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new OrderMyOrderAction.GetById(action.parentAction.resId));
  }

  @Action(OrderMyOrderAction.SubmitFail)
  submitFail(ctx: SolidifyStateContext<OrderMyOrderStateModel>, action: OrderMyOrderAction.SubmitFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(OrderMyOrderAction.Resume)
  resume(ctx: SolidifyStateContext<OrderMyOrderStateModel>, action: OrderMyOrderAction.Resume): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(`${this._urlResource}/${action.resId}/${ApiActionNameEnum.RESUME}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new OrderMyOrderAction.ResumeSuccess(action));
          } else {
            ctx.dispatch(new OrderMyOrderAction.ResumeFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new OrderMyOrderAction.ResumeFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(OrderMyOrderAction.ResumeSuccess)
  resumeSuccess(ctx: SolidifyStateContext<OrderMyOrderStateModel>, action: OrderMyOrderAction.ResumeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new OrderMyOrderAction.GetById(action.parentAction.resId));
  }

  @Action(OrderMyOrderAction.ResumeFail)
  resumeFail(ctx: SolidifyStateContext<OrderMyOrderStateModel>, action: OrderMyOrderAction.ResumeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(OrderMyOrderAction.Save)
  save(ctx: SolidifyStateContext<OrderMyOrderStateModel>, action: OrderMyOrderAction.Save): Observable<OrderArchive> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<OrderArchive>(`${this._urlResource}/${action.resId}/${ApiActionNameEnum.SAVE}`)
      .pipe(
        tap(() => {
          ctx.dispatch(new OrderMyOrderAction.SaveSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new OrderMyOrderAction.SaveFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(OrderMyOrderAction.SaveSuccess)
  saveSuccess(ctx: SolidifyStateContext<OrderMyOrderStateModel>, action: OrderMyOrderAction.SaveSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(OrderMyOrderAction.SaveFail)
  saveFail(ctx: SolidifyStateContext<OrderMyOrderStateModel>, action: OrderMyOrderAction.SaveFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
