/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-my-order-pending.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {AppExtendAction} from "@app/stores/app.action";
import {AppCartAction} from "@app/stores/cart/app-cart.action";
import {AppCartState} from "@app/stores/cart/app-cart.state";
import {AppCartArchiveState} from "@app/stores/cart/archive/app-cart-archive.state";
import {ArchiveAccessRightService} from "@home/services/archive-access-right.service";
import {
  Archive,
  Order,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {ArchiveDisseminationPolicyService} from "@shared/services/archive-dissemination-policy.service";
import {KeyValueOrgUnitDisseminationPolicyType} from "@shared/types/key-value-org-unit-dissemination-policy.type";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {
  MemoizedUtil,
  ModelFormControlEvent,
} from "solidify-frontend";

@Component({
  selector: "dlcm-my-order-pending-routable",
  templateUrl: "./order-my-order-pending.routable.html",
  styleUrls: ["./order-my-order-pending.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderMyOrderPendingRoutable extends SharedAbstractRoutable implements OnInit {
  isLoadingCartObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AppCartState as any);
  isLoadingArchiveObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AppCartArchiveState);
  listObs: Observable<Archive[]> = MemoizedUtil.list(this._store, AppCartArchiveState);

  listOptionsDisseminationPolicy: KeyValueOrgUnitDisseminationPolicyType[];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _router: Router,
              private readonly _archiveAccessRightService: ArchiveAccessRightService,
              private readonly _archiveDisseminationPolicyService: ArchiveDisseminationPolicyService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this._archiveDisseminationPolicyService.getListDisseminationPolicyOptionsFromListArchive(this.listObs).pipe(
      tap(listOptionsDisseminationPolicy => {
        this.listOptionsDisseminationPolicy = listOptionsDisseminationPolicy;
        this._changeDetector.detectChanges();
      }),
    ));

    this._store.dispatch(new AppExtendAction.LoadCart());
  }

  submit(event: ModelFormControlEvent<Order>): void {
    this._store.dispatch(new AppCartAction.Submit(event.model));
  }

  deleteAllArchive(): void {
    this._store.dispatch(new AppCartAction.RemoveAll());
  }

  deleteArchive(archive: Archive): void {
    this._store.dispatch(new AppCartAction.RemoveToCart(archive.resId));
  }

  navigateToArchive(archive: Archive): void {
    this._store.dispatch(new Navigate([RoutesEnum.archives, archive.resId]));
  }

  requestAccess(archive: Archive): void {
    this._archiveAccessRightService.requestAccessDataset(archive);
  }

  backToHomePageNavigate(): Navigate {
    return new Navigate([RoutesEnum.orderMyOrder]);
  }
}
