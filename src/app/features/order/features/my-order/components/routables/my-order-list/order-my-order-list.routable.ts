/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-my-order-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {AppUserState} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {Order} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  OrderMyOrderAction,
  orderMyOrderActionNameSpace,
} from "@order/features/my-order/stores/order-my-order.action";
import {
  OrderMyOrderState,
  OrderMyOrderStateModel,
} from "@order/features/my-order/stores/order-my-order.state";
import {orderMyOrderStatusHistoryNamespace} from "@order/features/my-order/stores/status-history/order-my-order-status-history.action";
import {OrderMyOrderStatusHistoryState} from "@order/features/my-order/stores/status-history/order-my-order-status-history.state";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  CookieConsentUtil,
  CookieType,
  DataTableFieldTypeEnum,
  isNotNullNorUndefined,
  isNullOrUndefined,
  LocalStorageHelper,
  MappingObjectUtil,
  MemoizedUtil,
  ObjectUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

const onlyPublicOrdersStorageKey: LocalStorageEnum = LocalStorageEnum.orderShowOnlyPublicOrders;
const onlyPublicOrdersFn: () => boolean = () => {
  const storageValue = LocalStorageHelper.getItem(onlyPublicOrdersStorageKey);
  if (isNullOrUndefined(storageValue)) {
    return false;
  }
  return storageValue === SOLIDIFY_CONSTANTS.STRING_TRUE;
};

@Component({
  selector: "dlcm-order-my-order-list-routable",
  templateUrl: "../../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./order-my-order-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderMyOrderListRoutable extends AbstractListRoutable<Order, OrderMyOrderStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string = LabelTranslateEnum.backToArchiveOrders;
  readonly KEY_PARAM_NAME: keyof Order & string = "name";
  private readonly _KEY_CREATOR: string = "creation.who";

  columnsSkippedToClear: string[] = [this._KEY_CREATOR, OrderMyOrderState.KEY_DISPLAY_PUBLIC];

  private _onlyPublicOrders: boolean = undefined;

  protected get onlyPublicOrders(): boolean {
    if (isNotNullNorUndefined(this._onlyPublicOrders)) {
      return this._onlyPublicOrders;
    }
    return onlyPublicOrdersFn();
  }

  protected set onlyPublicOrders(value: boolean) {
    this._onlyPublicOrders = value;
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, onlyPublicOrdersStorageKey)) {
      LocalStorageHelper.setItem(onlyPublicOrdersStorageKey, this.onlyPublicOrders + "");
    }
  }

  override listNewId: string[] = LocalStorageHelper.getListItem(LocalStorageEnum.newOrderAvailable);

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.order_myOrder, orderMyOrderActionNameSpace, _injector, {
      canCreate: false,
      canGoBack: _securityService.isRootOrAdmin(),
      listExtraButtons: [
        {
          color: "primary",
          icon: null,
          labelToTranslate: (current) => LabelTranslateEnum.showOnlyPublicOrders,
          callback: (model, buttonElementRef, checked) => this.showOnlyPublicOrder(checked),
          order: 40,
          isToggleButton: true,
          isToggleChecked: onlyPublicOrdersFn(),
        },
      ],
      historyState: OrderMyOrderStatusHistoryState,
      historyStateAction: orderMyOrderStatusHistoryNamespace,
      historyStatusEnumTranslate: Enums.Order.StatusEnumTranslate,
    }, StateEnum.order);
  }

  showOnlyPublicOrder(checked: boolean): void {
    let queryParameter = MemoizedUtil.queryParametersSnapshot(this._store, OrderMyOrderState);
    queryParameter = QueryParametersUtil.clone(queryParameter);
    queryParameter.paging.pageIndex = 0;
    if (checked) {
      this._updateQueryParameterWithCurrentUser(queryParameter, true);
      this.onlyPublicOrders = true;
    } else {
      this._updateQueryParameterWithCurrentUser(queryParameter, false);
      this.onlyPublicOrders = false;
    }
    this._store.dispatch(new OrderMyOrderAction.ChangeQueryParameters(queryParameter, true));
    this._changeDetector.detectChanges();
  }

  override onQueryParametersEvent(queryParameters: QueryParameters): void {
    if (this.onlyPublicOrders) {
      this._updateQueryParameterWithCurrentUser(queryParameters, true);
      this._store.dispatch(new OrderMyOrderAction.ChangeQueryParameters(queryParameters, true));
    } else {
      this._updateQueryParameterWithCurrentUser(queryParameters, false);
      this._store.dispatch(new OrderMyOrderAction.ChangeQueryParameters(queryParameters, true));
    }
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  conditionDisplayEditButton(model: Order | undefined): boolean {
    return true;
  }

  conditionDisplayDeleteButton(model: Order | undefined): boolean {
    return this._securityService.isRootOrAdmin() || MemoizedUtil.currentSnapshot(this._store, AppUserState)?.externalUid === model?.creation?.who;
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "queryType",
        header: LabelTranslateEnum.queryType,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Order.QueryTypeEnumTranslate,
      },
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Order.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  private _updateQueryParameterWithCurrentUser(queryParameters: QueryParameters, onlyPublicOrder: boolean): QueryParameters | undefined {
    queryParameters = ObjectUtil.clone(queryParameters);
    queryParameters.search = ObjectUtil.clone(queryParameters.search);

    if (onlyPublicOrder) {
      MappingObjectUtil.set(queryParameters.search.searchItems, OrderMyOrderState.KEY_DISPLAY_PUBLIC, "true");
    } else {
      MappingObjectUtil.delete(queryParameters.search.searchItems, OrderMyOrderState.KEY_DISPLAY_PUBLIC);
    }
    return queryParameters;
  }

}
