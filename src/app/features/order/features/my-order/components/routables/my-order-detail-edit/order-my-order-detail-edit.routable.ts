/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-my-order-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {AppCartDipAction} from "@app/stores/cart/dip/app-cart-dip.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {HomeArchiveClickThroughDuaDownloadDialog} from "@home/components/dialogs/archive-click-through-dua-download/home-archive-click-through-dua-download-dialog.component";
import {
  Archive,
  Dip,
  Order,
  User,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {OrderMyOrderAipAction} from "@order/features/my-order/stores/aip/order-my-order-aip.action";
import {OrderMyOrderAipState} from "@order/features/my-order/stores/aip/order-my-order-aip.state";
import {OrderMyOrderDipAction} from "@order/features/my-order/stores/dip/order-my-order-dip.action";
import {OrderMyOrderDipState} from "@order/features/my-order/stores/dip/order-my-order-dip.state";
import {
  OrderMyOrderAction,
  orderMyOrderActionNameSpace,
} from "@order/features/my-order/stores/order-my-order.action";
import {
  OrderMyOrderState,
  OrderMyOrderStateModel,
} from "@order/features/my-order/stores/order-my-order.state";
import {OrderMyOrderStatusHistoryAction} from "@order/features/my-order/stores/status-history/order-my-order-status-history.action";
import {OrderMyOrderStatusHistoryState} from "@order/features/my-order/stores/status-history/order-my-order-status-history.state";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ArchiveDisseminationPolicyService} from "@shared/services/archive-dissemination-policy.service";
import {SecurityService} from "@shared/services/security.service";
import {SharedArchiveAction} from "@shared/stores/archive/shared-archive.action";
import {sharedOrderActionNameSpace} from "@shared/stores/order/shared-order.action";
import {KeyValueOrgUnitDisseminationPolicyType} from "@shared/types/key-value-org-unit-dissemination-policy.type";
import {
  combineLatest,
  Observable,
} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  ButtonColorEnum,
  ConfirmDialog,
  DialogUtil,
  EnumUtil,
  ExtraButtonToolbar,
  isEmptyArray,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  LocalStorageHelper,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  PollingHelper,
  QueryParameters,
  ResourceActionHelper,
  ResourceNameSpace,
  StatusHistory,
  StatusHistoryDialog,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-order-my-order-detail-edit-routable",
  templateUrl: "./order-my-order-detail-edit.routable.html",
  styleUrls: ["./order-my-order-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderMyOrderDetailEditRoutable extends AbstractDetailEditCommonRoutable<Order, OrderMyOrderStateModel> implements OnInit, OnDestroy {
  @Select(OrderMyOrderState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(OrderMyOrderState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, OrderMyOrderStatusHistoryState, state => state.history);
  isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, OrderMyOrderStatusHistoryState);
  queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, OrderMyOrderStatusHistoryState, state => state.queryParameters);

  listDipObs: Observable<Dip[]> = MemoizedUtil.selected(this._store, OrderMyOrderDipState);
  listArchiveObs: Observable<Archive[]> = MemoizedUtil.select(this._store, OrderMyOrderAipState, state => state.listArchives);

  isLoadingDipObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, OrderMyOrderDipState);
  isLoadingAipObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, OrderMyOrderAipState);

  currentUserObs: Observable<User> = MemoizedUtil.current(this._store, AppUserState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedOrderActionNameSpace;

  readonly KEY_PARAM_NAME: keyof Order & string = "name";

  override deleteAvailable: boolean = false;

  listOptionsDisseminationPolicy: KeyValueOrgUnitDisseminationPolicyType[];

  listExtraButtons: ExtraButtonToolbar<Order>[] = [
    {
      color: "primary",
      icon: IconNameEnum.resume,
      displayCondition: current => !this.isEdit && !isNullOrUndefined(current) && current.status === Enums.Order.StatusEnum.IN_ERROR,
      callback: () => this._resume(),
      labelToTranslate: (current) => LabelTranslateEnum.resume,
      order: 40,
    },
  ];

  private readonly _INTERVAL_REFRESH_INITIAL_IN_SECOND: number = 5;
  private readonly _INTERVAL_REFRESH_MAX_IN_SECOND: number = 60;

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this._archiveDisseminationPolicyService.getListDisseminationPolicyOptionsFromListArchive(this.listArchiveObs).pipe(
      tap(listOptionsDisseminationPolicy => {
        this.listOptionsDisseminationPolicy = listOptionsDisseminationPolicy;
        this._changeDetector.detectChanges();
      }),
    ));

    this.subscribe(PollingHelper.startPollingObs({
      waitingTimeBeforeStartInSecond: this._INTERVAL_REFRESH_INITIAL_IN_SECOND,
      initialIntervalRefreshInSecond: this._INTERVAL_REFRESH_INITIAL_IN_SECOND,
      incrementInterval: true,
      resetIntervalWhenUserMouseEvent: true,
      maximumIntervalRefreshInSecond: this._INTERVAL_REFRESH_MAX_IN_SECOND,
      continueUntil: () => isNullOrUndefined(this.current) || (this.current.status !== Enums.Order.StatusEnum.READY && this.current.status !== Enums.Order.StatusEnum.IN_ERROR),
      actionToDo: () => this._store.dispatch(ResourceActionHelper.getById(this._resourceNameSpace, this._resId, true)),
    }));

    this.subscribe(combineLatest([this.currentUserObs, this.currentObs]).pipe(
      distinctUntilChanged(),
      filter(([currentUser, currentOrder]) => isNotNullNorUndefined(currentUser) && isNotNullNorUndefined(currentOrder)),
      take(1),
      tap(([currentUser, currentOrder]) => {
        this.deleteAvailable = this._securityService.isRootOrAdmin() || currentOrder?.creation?.who === currentUser?.externalUid;
      }),
    ));

    this.subscribe(this.currentObs.pipe(
        distinctUntilChanged(),
        filter(current => !isNullOrUndefined(current) && (this.current.status === Enums.Order.StatusEnum.READY || this.current.status === Enums.Order.StatusEnum.IN_ERROR)),
        take(1),
      ),
      (current) => {
        this._store.dispatch(new OrderMyOrderDipAction.GetAll(this._resId));
        this._store.dispatch(new OrderMyOrderAipAction.GetAll(this._resId)); // ALLOW TO UPDATE SIZE
        LocalStorageHelper.removeItemInList(LocalStorageEnum.newOrderAvailable, this._resId);

        const currentUser = MemoizedUtil.currentSnapshot(this._store, AppUserState);
        this.deleteAvailable = current?.creation?.who === currentUser?.externalUid;
      });

    this.subscribe(this.currentObs.pipe(
        distinctUntilChanged(),
        filter(current => !isNullOrUndefined(current) && this.current.status !== Enums.Order.StatusEnum.READY && this.current.status !== Enums.Order.StatusEnum.IN_ERROR),
        take(1),
      ),
      (current) => {
        this._store.dispatch(new OrderMyOrderAipAction.GetAll(this._resId));
      });

    this.subscribe(PollingHelper.startPollingObs({
      // Manage case when we are on the page that become ready
      initialIntervalRefreshInSecond: 1,
      incrementInterval: true,
      resetIntervalWhenUserMouseEvent: true,
      filter: () => (isNotNullNorUndefined(this.current) && (this.current.status === Enums.Order.StatusEnum.READY || this.current.status === Enums.Order.StatusEnum.IN_ERROR)) && LocalStorageHelper.getListItem(LocalStorageEnum.newOrderAvailable)
        .includes(this._resId),
      actionToDo: () => {
        LocalStorageHelper.removeItemInList(LocalStorageEnum.newOrderAvailable, this._resId);
      },
    }));
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _router: Router,
              private readonly _securityService: SecurityService,
              private readonly _archiveDisseminationPolicyService: ArchiveDisseminationPolicyService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.order_myOrder, _injector, orderMyOrderActionNameSpace, StateEnum.order);
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._cleanState();
  }

  _getSubResourceWithParentId(id: string): void {
  }

  showHistory(): void {
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: this._resId,
      name: StringUtil.convertToPascalCase(this._state),
      statusHistory: this.historyObs,
      isLoading: this.isLoadingHistoryObs,
      queryParametersObs: this.queryParametersHistoryObs,
      state: OrderMyOrderStatusHistoryAction,
      statusEnums: Enums.Order.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }

  private _resume(): void {
    this._store.dispatch(new OrderMyOrderAction.Resume(this._resId));
  }

  downloadDip(dip: Dip): void {
    this._store.dispatch(new AppCartDipAction.Download(dip.resId));
  }

  navigateToArchive(archive: Archive): void {
    this._store.dispatch(new Navigate([RoutesEnum.archives, archive.resId]));
  }

  downloadArchive(archive: Archive): void {
    this._store.dispatch(new SharedArchiveAction.Download(archive));
  }

  downloadAll(listArchive: Archive[]): void {
    const listArchiveWithClickThrough = listArchive.filter(archive => archive.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA);
    if (isNonEmptyArray(listArchiveWithClickThrough)) {
      if (listArchiveWithClickThrough.length === 1) {
        this._openClickThroughDuaDialogRecursively(listArchiveWithClickThrough);
        return;
      }

      this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
        titleToTranslate: MARK_AS_TRANSLATABLE("order.myOrder.dialog.downloadAllWithClickThrough.title"),
        messageToTranslate: MARK_AS_TRANSLATABLE("order.myOrder.dialog.downloadAllWithClickThrough.message"),
        paramMessage: {count: listArchiveWithClickThrough.length},
        confirmButtonToTranslate: LabelTranslateEnum.downloadAll,
        cancelButtonToTranslate: LabelTranslateEnum.cancel,
        colorConfirm: ButtonColorEnum.primary,
      }, undefined, (isConfirmed: boolean) => {
        this._openClickThroughDuaDialogRecursively(listArchiveWithClickThrough);
      }));

      return;
    }

    this._downloadOrder();
  }

  private _openClickThroughDuaDialogRecursively(listArchive: Archive[]): void {
    if (isEmptyArray(listArchive)) {
      this._downloadOrder();
      return;
    }
    const archive = listArchive.shift();
    this.subscribe(DialogUtil.open(this._dialog, HomeArchiveClickThroughDuaDownloadDialog, {
        archive: archive,
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      }, () => {
        this._openClickThroughDuaDialogRecursively(listArchive);
      }));
  }

  private _downloadOrder(): void {
    this.subscribe(this.listDipObs.pipe(
      distinctUntilChanged(),
      filter(listDip => isNonEmptyArray(listDip)),
      take(1),
    ), listDip => {
      listDip.forEach(dip => this.downloadDip(dip));
    });
  }

  save(order: Order): void {
    this.update({
      model: order,
    });
  }
}
