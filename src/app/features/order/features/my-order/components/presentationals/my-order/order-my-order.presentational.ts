/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-my-order.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  Output,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";
import {DLCM_CONSTANTS} from "@app/constants";
import {Enums} from "@enums";
import {ArchiveAccessRightService} from "@home/services/archive-access-right.service";
import {
  Archive,
  Order,
} from "@models";
import {SharedArchiveTileMode} from "@shared/components/presentationals/shared-archive-tile/shared-archive-tile.presentational";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {KeyValueOrgUnitDisseminationPolicyType} from "@shared/types/key-value-org-unit-dissemination-policy.type";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {tap} from "rxjs/operators";
import {
  AbstractFormPresentational,
  BreakpointService,
  FormValidationHelper,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  ObservableUtil,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-order-my-order",
  templateUrl: "./order-my-order.presentational.html",
  styleUrls: ["./order-my-order.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderMyOrderPresentational extends AbstractFormPresentational<Order> {
  private readonly _CONCATENATION_SEPARATOR: string = ";";

  _listArchives: Archive[];

  trackByFn(index: number, archive: Archive): string {
    return archive.resId;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  private readonly _deleteOrderBS: BehaviorSubject<Order | undefined> = new BehaviorSubject<Order | undefined>(undefined);
  @Output("deleteOrder")
  readonly deleteOrderObs: Observable<Order | undefined> = ObservableUtil.asObservable(this._deleteOrderBS);

  private readonly _deleteAllArchiveBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("deleteAllArchive")
  readonly deleteAllArchiveObs: Observable<void | undefined> = ObservableUtil.asObservable(this._deleteAllArchiveBS);

  private readonly _deleteArchiveBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("deleteArchive")
  readonly deleteArchiveObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._deleteArchiveBS);

  private readonly _navigateToArchiveBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("navigateToArchive")
  readonly navigateToArchiveObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._navigateToArchiveBS);

  private readonly _requestAccessBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("requestAccess")
  readonly requestAccessObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._requestAccessBS);

  private readonly _downloadArchiveBS: BehaviorSubject<Archive | undefined> = new BehaviorSubject<Archive | undefined>(undefined);
  @Output("downloadArchive")
  readonly downloadArchiveObs: Observable<Archive | undefined> = ObservableUtil.asObservable(this._downloadArchiveBS);

  private readonly _downloadAllBS: BehaviorSubject<Archive[] | undefined> = new BehaviorSubject<Archive[] | undefined>(undefined);
  @Output("downloadAll")
  readonly downloadAllObs: Observable<Archive[] | undefined> = ObservableUtil.asObservable(this._downloadAllBS);

  @Input()
  set listArchives(value: Archive[]) {
    this._listArchives = value;
    this.totalSize = 0;
    if (isNullOrUndefined(this._listArchives)) {
      return;
    }
    this._listArchives.forEach(archive => {
      this.totalSize += archive.size;
      this.subscribe(this.archiveAccessRightService.isDownloadAuthorizedObs(archive).pipe(
        tap(isAuthorized => {
          if (!isAuthorized) {
            this.isGeneralAccessControlled = true;
          }
        }),
      ));
    });
  }

  get listArchives(): Archive[] {
    return this._listArchives;
  }

  get orderStatusEnum(): typeof Enums.Order.StatusEnum {
    return Enums.Order.StatusEnum;
  }

  isGeneralAccessControlled: boolean = false;

  @Input()
  deleteAvailable: boolean;
  _order: Order;

  @Input()
  set order(value: Order) {
    this._order = value;
    if (isNullOrUndefined(this.order)) {
      return;
    }
    if (this.mode === "order") {
      if (this.order.status === Enums.Order.StatusEnum.READY) {
        this.iconOrderStatus = IconNameEnum.orderReady;
        this.messageOrderStatus = LabelTranslateEnum.orderReady;
        this.classOrderStatus = "is-ready";
      } else if (this.order.status === Enums.Order.StatusEnum.IN_ERROR) {
        this.iconOrderStatus = IconNameEnum.orderInError;
        this.messageOrderStatus = LabelTranslateEnum.orderInError;
        this.classOrderStatus = "is-in-error";
      } else {
        this.iconOrderStatus = IconNameEnum.orderInProgress;
        this.messageOrderStatus = LabelTranslateEnum.orderInProgress;
        this.classOrderStatus = "is-in-progress";
      }
    }
  }

  get order(): Order {
    return this._order;
  }

  get model(): Order {
    return this.order;
  }

  @Input()
  mode: SharedArchiveTileMode;

  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  totalSize: number = 0;
  iconOrderStatus: IconNameEnum | undefined = undefined;
  messageOrderStatus: string | undefined = undefined;
  classOrderStatus: "is-in-error" | "is-in-progress" | "is-ready" | undefined = undefined;

  @Input()
  listOptionsDisseminationPolicy: KeyValueOrgUnitDisseminationPolicyType[];

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              private readonly _fb: FormBuilder,
              public readonly archiveAccessRightService: ArchiveAccessRightService,
              public readonly breakpointService: BreakpointService,
              protected readonly _injector: Injector) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.publicOrder]: [false, [SolidifyValidator]],
      [this.formDefinition.disseminationPolicyIdAndAssociationId]: [DLCM_CONSTANTS.DISSEMINATION_POLICY_BASIC_ID, [Validators.required, SolidifyValidator]],
    });
  }

  protected _bindFormTo(order: Order): void {
    let concatenateDisseminationPolicy = order.disseminationPolicyId;
    if (isNotNullNorUndefined(order.organizationalUnitDisseminationPolicyId)) {
      concatenateDisseminationPolicy += this._CONCATENATION_SEPARATOR + order.organizationalUnitDisseminationPolicyId;
    }
    this.form = this._fb.group({
      [this.formDefinition.name]: [order.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.publicOrder]: [order.publicOrder, [SolidifyValidator]],
      [this.formDefinition.disseminationPolicyIdAndAssociationId]: [concatenateDisseminationPolicy, [Validators.required, SolidifyValidator]],
    });
  }

  protected override _disableSpecificField(): void {
    super._disableSpecificField();
    if (isNotNullNorUndefined(this.model)) {
      this.form.get(this.formDefinition.disseminationPolicyIdAndAssociationId).disable();
    }
  }

  protected _treatmentBeforeSubmit(order: Order): Order {
    delete order[this.formDefinition.disseminationPolicyIdAndAssociationId];
    order.resId = this.order?.resId;
    const concatenateDisseminationPolicy = this.form.get(this.formDefinition.disseminationPolicyIdAndAssociationId).value;
    const disseminationPolicyArray = concatenateDisseminationPolicy.split(this._CONCATENATION_SEPARATOR);
    order.disseminationPolicyId = disseminationPolicyArray[0];
    order.organizationalUnitDisseminationPolicyId = disseminationPolicyArray.length > 1 ? disseminationPolicyArray[1] : undefined;
    return order;
  }

  deleteAll(): void {
    this._deleteAllArchiveBS.next();
  }

  navigateToArchive(archive: Archive): void {
    this._navigateToArchiveBS.next(archive);
  }

  deleteArchive(archive: Archive): void {
    this._deleteArchiveBS.next(archive);
  }

  downloadArchive(archive: Archive): void {
    this._downloadArchiveBS.next(archive);
  }

  requestAccess(archive: Archive): void {
    this._requestAccessBS.next(archive);
  }

  deleteOrder(): void {
    this._deleteOrderBS.next(this.order);
  }

  downloadOrder(): void {
    this._downloadAllBS.next(this.listArchives);
  }

  concatenateIds(keyValueOrgUnitDisseminationPolicy: KeyValueOrgUnitDisseminationPolicyType): string {
    const compositeKey = keyValueOrgUnitDisseminationPolicy.object?.joinResource?.compositeKey;
    if (isNotNullNorUndefinedNorWhiteString(compositeKey)) {
      return `${keyValueOrgUnitDisseminationPolicy.key}${this._CONCATENATION_SEPARATOR}${compositeKey}`;
    }
    return keyValueOrgUnitDisseminationPolicy.key;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() publicOrder: string;
  @PropertyName() disseminationPolicyIdAndAssociationId: string;
}
