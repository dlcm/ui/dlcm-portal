/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-all-order.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {OrderAllOrderFormPresentational} from "@order/features/all-order/components/presentationals/all-order-form/order-all-order-form.presentational";
import {OrderAllOrderCreateRoutable} from "@order/features/all-order/components/routables/all-order-create/order-all-order-create.routable";
import {OrderAllOrderListRoutable} from "@order/features/all-order/components/routables/all-order-list/order-all-order-list.routable";
import {OrderAllOrderRoutingModule} from "@order/features/all-order/order-all-order-routing.module";
import {OrderAllOrderAipState} from "@order/features/all-order/stores/aip/order-all-order-aip.state";
import {OrderAllOrderDipState} from "@order/features/all-order/stores/dip/order-all-order-dip.state";
import {OrderAllOrderState} from "@order/features/all-order/stores/order-all-order.state";
import {OrderAllOrderOrderArchiveState} from "@order/features/all-order/stores/order-archive/order-all-order-order-archive.state";
import {OrderAllOrderStatusHistoryState} from "@order/features/all-order/stores/status-history/order-all-order-status-history.state";
import {SharedModule} from "@shared/shared.module";
import {OrderAllOrderDetailEditRoutable} from "./components/routables/all-order-detail-edit/order-all-order-detail-edit.routable";

const routables = [
  OrderAllOrderListRoutable,
  OrderAllOrderDetailEditRoutable,
  OrderAllOrderCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  OrderAllOrderFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    OrderAllOrderRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      OrderAllOrderState,
      OrderAllOrderStatusHistoryState,
      OrderAllOrderAipState,
      OrderAllOrderDipState,
      OrderAllOrderOrderArchiveState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class OrderAllOrderModule {
}
