/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-all-order-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {OrderAllOrderCreateRoutable} from "@order/features/all-order/components/routables/all-order-create/order-all-order-create.routable";
import {OrderAllOrderDetailEditRoutable} from "@order/features/all-order/components/routables/all-order-detail-edit/order-all-order-detail-edit.routable";
import {OrderAllOrderListRoutable} from "@order/features/all-order/components/routables/all-order-list/order-all-order-list.routable";
import {OrderAllOrderState} from "@order/features/all-order/stores/order-all-order.state";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  OrderRoutesEnum,
} from "@shared/enums/routes.enum";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";
import {
  CanDeactivateGuard,
  EmptyContainer,
} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: OrderAllOrderListRoutable,
    data: {},
  },
  {
    path: OrderRoutesEnum.allOrderDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: OrderAllOrderDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: OrderAllOrderState.currentTitle,
    },
    children: [
      {
        path: OrderRoutesEnum.allOrderEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
  {
    path: OrderRoutesEnum.allOrderCreate,
    component: OrderAllOrderCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderAllOrderRoutingModule {
}
