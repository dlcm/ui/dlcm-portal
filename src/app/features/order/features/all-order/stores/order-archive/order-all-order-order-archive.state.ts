/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-all-order-order-archive.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {OrderArchive} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  OrderAllOrderOrderArchiveAction,
  orderAllOrderOrderArchiveActionNameSpace,
} from "@order/features/all-order/stores/order-archive/order-all-order-order-archive.action";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  ResourceActionHelper,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface OrderAllOrderOrderArchiveStateModel extends ResourceStateModel<OrderArchive> {
}

@Injectable()
@State<OrderAllOrderOrderArchiveStateModel>({
  name: StateEnum.order_allOrder_orderArchive,
  defaults: {
    ...defaultResourceStateInitValue(),
    queryParameters: new QueryParameters(),
  },
  children: [],
})
export class OrderAllOrderOrderArchiveState extends ResourceState<OrderAllOrderOrderArchiveStateModel, OrderArchive> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: orderAllOrderOrderArchiveActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessOrders;
  }

  @OverrideDefaultAction()
  @Action(OrderAllOrderOrderArchiveAction.GetAll)
  getAll(ctx: SolidifyStateContext<OrderAllOrderOrderArchiveStateModel>, action: OrderAllOrderOrderArchiveAction.GetAll): Observable<CollectionTyped<OrderArchive>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        list: undefined,
        total: 0,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
      ...reset,
    });
    return this._apiService.getCollection<OrderArchive>(this._urlResource + urlSeparator + action.resId + urlSeparator + ApiActionNameEnum.VIEW, ctx.getState().queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [this._nameSpace.GetAll, Navigate]),
        tap((collection: CollectionTyped<OrderArchive>) => {
          ctx.dispatch(ResourceActionHelper.getAllSuccess<OrderArchive>(this._nameSpace, action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(ResourceActionHelper.getAllFail(this._nameSpace, action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }
}
