/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-all-order.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {
  Order,
  OrderArchive,
} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {
  OrderAllOrderAipState,
  OrderAllOrderAipStateModel,
} from "@order/features/all-order/stores/aip/order-all-order-aip.state";
import {
  OrderAllOrderDipState,
  OrderAllOrderDipStateModel,
} from "@order/features/all-order/stores/dip/order-all-order-dip.state";
import {
  OrderAllOrderAction,
  orderAllOrderActionNameSpace,
} from "@order/features/all-order/stores/order-all-order.action";
import {
  OrderAllOrderOrderArchiveState,
  OrderAllOrderOrderArchiveStateModel,
} from "@order/features/all-order/stores/order-archive/order-all-order-order-archive.state";
import {
  OrderAllOrderStatusHistoryState,
  OrderAllOrderStatusHistoryStateModel,
} from "@order/features/all-order/stores/status-history/order-all-order-status-history.state";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CookieConsentUtil,
  CookieType,
  defaultAssociationStateInitValue,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  LocalStorageHelper,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface OrderAllOrderStateModel extends ResourceStateModel<Order> {
  order_allOrder_statusHistory: OrderAllOrderStatusHistoryStateModel;
  order_allOrder_aip: OrderAllOrderAipStateModel | undefined;
  order_allOrder_dip: OrderAllOrderDipStateModel | undefined;
  order_allOrder_orderArchive: OrderAllOrderOrderArchiveStateModel | undefined;
}

@Injectable()
@State<OrderAllOrderStateModel>({
  name: StateEnum.order_allOrder,
  defaults: {
    ...defaultResourceStateInitValue(),
    order_allOrder_statusHistory: {...defaultStatusHistoryInitValue()},
    order_allOrder_aip: {...defaultAssociationStateInitValue()},
    order_allOrder_dip: {...defaultAssociationStateInitValue()},
    order_allOrder_orderArchive: {...defaultResourceStateInitValue()},
  },
  children: [
    OrderAllOrderStatusHistoryState,
    OrderAllOrderOrderArchiveState,
    OrderAllOrderAipState,
    OrderAllOrderDipState,
  ],
})
export class OrderAllOrderState extends ResourceState<OrderAllOrderStateModel, Order> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: orderAllOrderActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.orderAllOrderDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.orderAllOrderDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.orderAllOrder,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("order.allOrder.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("order.allOrder.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("order.allOrder.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessOrders;
  }

  @Selector()
  static isLoading(state: OrderAllOrderStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static currentTitle(state: OrderAllOrderStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isLoadingWithDependency(state: OrderAllOrderStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: OrderAllOrderStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: OrderAllOrderStateModel): boolean {
    return true;
  }

  @Action(OrderAllOrderAction.Submit)
  submit(ctx: SolidifyStateContext<OrderAllOrderStateModel>, action: OrderAllOrderAction.Submit): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(`${this._urlResource}/${action.resId}/${ApiActionNameEnum.SUBMIT}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new OrderAllOrderAction.SubmitSuccess(action));
          } else {
            ctx.dispatch(new OrderAllOrderAction.SubmitFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new OrderAllOrderAction.SubmitFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(OrderAllOrderAction.SubmitSuccess)
  submitSuccess(ctx: SolidifyStateContext<OrderAllOrderStateModel>, action: OrderAllOrderAction.SubmitSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new OrderAllOrderAction.GetById(action.parentAction.resId));

    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.orderPending)) {
      LocalStorageHelper.addItemInList(LocalStorageEnum.orderPending, action.parentAction.resId);
    }
  }

  @Action(OrderAllOrderAction.SubmitFail)
  submitFail(ctx: SolidifyStateContext<OrderAllOrderStateModel>, action: OrderAllOrderAction.SubmitFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(OrderAllOrderAction.Resume)
  resume(ctx: SolidifyStateContext<OrderAllOrderStateModel>, action: OrderAllOrderAction.Resume): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(`${this._urlResource}/${action.resId}/${ApiActionNameEnum.RESUME}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new OrderAllOrderAction.ResumeSuccess(action));
          } else {
            ctx.dispatch(new OrderAllOrderAction.ResumeFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new OrderAllOrderAction.ResumeFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(OrderAllOrderAction.ResumeSuccess)
  resumeSuccess(ctx: SolidifyStateContext<OrderAllOrderStateModel>, action: OrderAllOrderAction.ResumeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new OrderAllOrderAction.GetById(action.parentAction.resId));
  }

  @Action(OrderAllOrderAction.ResumeFail)
  resumeFail(ctx: SolidifyStateContext<OrderAllOrderStateModel>, action: OrderAllOrderAction.ResumeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(OrderAllOrderAction.Save)
  save(ctx: SolidifyStateContext<OrderAllOrderStateModel>, action: OrderAllOrderAction.Save): Observable<OrderArchive> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<OrderArchive>(`${this._urlResource}/${action.resId}/${ApiActionNameEnum.SAVE}`)
      .pipe(
        tap(() => {
          ctx.dispatch(new OrderAllOrderAction.SaveSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new OrderAllOrderAction.SaveFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(OrderAllOrderAction.SaveSuccess)
  saveSuccess(ctx: SolidifyStateContext<OrderAllOrderStateModel>, action: OrderAllOrderAction.SaveSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(OrderAllOrderAction.SaveFail)
  saveFail(ctx: SolidifyStateContext<OrderAllOrderStateModel>, action: OrderAllOrderAction.SaveFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

}
