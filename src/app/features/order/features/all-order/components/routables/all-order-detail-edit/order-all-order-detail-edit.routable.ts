/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-all-order-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Aip,
  Dip,
  Order,
  OrderArchive,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {OrderAllOrderFormPresentational} from "@order/features/all-order/components/presentationals/all-order-form/order-all-order-form.presentational";
import {OrderAllOrderAipAction} from "@order/features/all-order/stores/aip/order-all-order-aip.action";
import {OrderAllOrderAipState} from "@order/features/all-order/stores/aip/order-all-order-aip.state";
import {OrderAllOrderDipAction} from "@order/features/all-order/stores/dip/order-all-order-dip.action";
import {OrderAllOrderDipState} from "@order/features/all-order/stores/dip/order-all-order-dip.state";
import {
  OrderAllOrderAction,
  orderAllOrderActionNameSpace,
} from "@order/features/all-order/stores/order-all-order.action";
import {
  OrderAllOrderState,
  OrderAllOrderStateModel,
} from "@order/features/all-order/stores/order-all-order.state";
import {OrderAllOrderOrderArchiveAction} from "@order/features/all-order/stores/order-archive/order-all-order-order-archive.action";
import {OrderAllOrderOrderArchiveState} from "@order/features/all-order/stores/order-archive/order-all-order-order-archive.state";
import {OrderAllOrderStatusHistoryAction} from "@order/features/all-order/stores/status-history/order-all-order-status-history.action";
import {OrderAllOrderStatusHistoryState} from "@order/features/all-order/stores/status-history/order-all-order-status-history.state";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ArchiveDisseminationPolicyService} from "@shared/services/archive-dissemination-policy.service";
import {SecurityService} from "@shared/services/security.service";
import {sharedOrderActionNameSpace} from "@shared/stores/order/shared-order.action";
import {KeyValueOrgUnitDisseminationPolicyType} from "@shared/types/key-value-org-unit-dissemination-policy.type";
import {MetadataUtil} from "@shared/utils/metadata.util";
import {Observable} from "rxjs";
import {
  filter,
  map,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  DialogUtil,
  EnumUtil,
  ExtraButtonToolbar,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
  Paging,
  QueryParameters,
  ResourceNameSpace,
  StatusHistory,
  StatusHistoryDialog,
  StoreUtil,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-order-all-order-detail-edit-routable",
  templateUrl: "./order-all-order-detail-edit.routable.html",
  styleUrls: ["./order-all-order-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderAllOrderDetailEditRoutable extends AbstractDetailEditCommonRoutable<Order, OrderAllOrderStateModel> implements OnInit, OnDestroy {
  @Select(OrderAllOrderState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(OrderAllOrderState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, OrderAllOrderStatusHistoryState, state => state.history);
  queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, OrderAllOrderStatusHistoryState, state => state.queryParameters);
  isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, OrderAllOrderStatusHistoryState);

  listAipObs: Observable<Aip[]> = MemoizedUtil.selected(this._store, OrderAllOrderAipState);
  isLoadingAipObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, OrderAllOrderAipState);
  listDipObs: Observable<Dip[]> = MemoizedUtil.selected(this._store, OrderAllOrderDipState);
  isLoadingDipObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, OrderAllOrderDipState);

  listOrderArchivesObs: Observable<OrderArchive[]> = MemoizedUtil.list(this._store, OrderAllOrderOrderArchiveState);
  isLoadingOrderArchivesObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, OrderAllOrderOrderArchiveState);

  queryParametersAipObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, OrderAllOrderAipState);
  queryParametersOrderArchivesObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, OrderAllOrderOrderArchiveState);
  queryParametersDipObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, OrderAllOrderDipState);

  listOptionsDisseminationPolicy: KeyValueOrgUnitDisseminationPolicyType[];

  @ViewChild("formPresentational")
  readonly formPresentational: OrderAllOrderFormPresentational;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedOrderActionNameSpace;

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  readonly KEY_PARAM_NAME: keyof Order & string = "name";

  listExtraButtons: ExtraButtonToolbar<Order>[] = [
    {
      color: "primary",
      icon: IconNameEnum.done,
      displayCondition: current => !this.isEdit && !isNullOrUndefined(current) && current.status === Enums.Order.StatusEnum.IN_PROGRESS,
      callback: () => this._save(),
      labelToTranslate: (current) => LabelTranslateEnum.save,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.submit,
      displayCondition: current => !this.isEdit && !isNullOrUndefined(current) && current.status === Enums.Order.StatusEnum.IN_PROGRESS && current.aipNumber > 0,
      callback: () => this._submit(),
      labelToTranslate: (current) => LabelTranslateEnum.submit,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.resume,
      displayCondition: current => !this.isEdit && !isNullOrUndefined(current) && current.status === Enums.Order.StatusEnum.IN_ERROR,
      callback: () => this._resume(),
      labelToTranslate: (current) => LabelTranslateEnum.resume,
      order: 40,
    },
  ];

  get orderStatusEnum(): typeof Enums.Order.StatusEnum {
    return Enums.Order.StatusEnum;
  }

  get metadataUtil(): typeof MetadataUtil {
    return MetadataUtil;
  }

  get accessLevelEnumTranslate(): typeof Enums.Access.AccessEnumTranslate {
    return Enums.Access.AccessEnumTranslate;
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _router: Router,
              private readonly _securityService: SecurityService,
              private readonly _archiveDisseminationPolicyService: ArchiveDisseminationPolicyService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.order_allOrder, _injector, orderAllOrderActionNameSpace, StateEnum.order);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this.subscribe(this._archiveDisseminationPolicyService.getListDisseminationPolicyOptionsFromListArchive(this.listOrderArchivesObs.pipe(
      filter(listOrderArchives => isNotNullNorUndefined(listOrderArchives)),
      map(listOrderArchives => listOrderArchives.map(oa => ({
        organizationalUnitId: oa.archive.info.organizationalUnitId,
      } as any))),
    )).pipe(
      tap(listOptionsDisseminationPolicy => {
        this.listOptionsDisseminationPolicy = listOptionsDisseminationPolicy;
        this._changeDetector.detectChanges();
      }),
    ));
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._cleanState();
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new OrderAllOrderAipAction.GetAll(id));
    this._store.dispatch(new OrderAllOrderDipAction.GetAll(id));
    this._view();
  }

  showHistory(): void {
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: this._resId,
      name: StringUtil.convertToPascalCase(this._state),
      statusHistory: this.historyObs,
      isLoading: this.isLoadingHistoryObs,
      queryParametersObs: this.queryParametersHistoryObs,
      state: OrderAllOrderStatusHistoryAction,
      statusEnums: Enums.Order.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }

  private _submit(): void {
    this._store.dispatch(new OrderAllOrderAction.Submit(this._resId));
  }

  private _resume(): void {
    this._store.dispatch(new OrderAllOrderAction.Resume(this._resId));
  }

  private _view(): void {
    this._store.dispatch(new OrderAllOrderOrderArchiveAction.GetAll(this._resId));
  }

  private _save(): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new OrderAllOrderAction.Save(this._resId),
      OrderAllOrderAction.SaveSuccess,
      resultAction => {
        this._store.dispatch(new OrderAllOrderAipAction.GetAll(this._resId));
      }));
  }

  goToDip(dip: Dip): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationPlanningDipDetail, dip.resId]));
  }

  goToAipDownloaded(aip: Aip): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationPlanningAipDownloadedDetail, aip.resId]));
  }

  goToAip(aipId: string): void {
    const storagionNode = 1;
    this._store.dispatch(new Navigate([AppRoutesEnum.preservationPlanning, PreservationPlanningRoutesEnum.aip, storagionNode, SharedAipRoutesEnum.aipDetail, aipId]));
  }

  pageChangeMatchingArchives($event: Paging): void {
    const queryParameters = new QueryParameters();
    queryParameters.paging = $event;
    this._store.dispatch(new OrderAllOrderOrderArchiveAction.GetAll(this._resId, queryParameters));
  }

  pageChangeAipsSelected($event: Paging): void {
    const queryParameters = new QueryParameters();
    queryParameters.paging = $event;
    this._store.dispatch(new OrderAllOrderAipAction.GetAll(this._resId, queryParameters));
  }

  pageChangeDipsAvailable($event: Paging): void {
    const queryParameters = new QueryParameters();
    queryParameters.paging = $event;
    this._store.dispatch(new OrderAllOrderDipAction.GetAll(this._resId, queryParameters));
  }
}
