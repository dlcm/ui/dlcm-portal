/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-all-order-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {Order} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {orderAllOrderActionNameSpace} from "@order/features/all-order/stores/order-all-order.action";
import {OrderAllOrderStateModel} from "@order/features/all-order/stores/order-all-order.state";
import {orderAllOrderStatusHistoryNamespace} from "@order/features/all-order/stores/status-history/order-all-order-status-history.action";
import {OrderAllOrderStatusHistoryState} from "@order/features/all-order/stores/status-history/order-all-order-status-history.state";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "dlcm-order-all-order-list-routable",
  templateUrl: "../../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./order-all-order-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderAllOrderListRoutable extends AbstractListRoutable<Order, OrderAllOrderStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string = LabelTranslateEnum.backToArchiveOrders;
  readonly KEY_PARAM_NAME: keyof Order & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.order_allOrder, orderAllOrderActionNameSpace, _injector, {
      canCreate: true,
      historyState: OrderAllOrderStatusHistoryState,
      historyStateAction: orderAllOrderStatusHistoryNamespace,
      historyStatusEnumTranslate: Enums.Order.StatusEnumTranslate,
    }, StateEnum.order);
  }

  conditionDisplayEditButton(model: Order | undefined): boolean {
    return model.status !== Enums.Order.StatusEnum.READY;
  }

  conditionDisplayDeleteButton(model: Order | undefined): boolean {
    return true;
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "queryType",
        header: LabelTranslateEnum.queryType,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Order.QueryTypeEnumTranslate,
      },
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Order.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }
}
