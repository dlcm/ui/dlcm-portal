/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-all-order-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  ViewChild,
} from "@angular/core";
import {Order} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {orderAllOrderActionNameSpace} from "@order/features/all-order/stores/order-all-order.action";
import {
  OrderAllOrderState,
  OrderAllOrderStateModel,
} from "@order/features/all-order/stores/order-all-order.state";
import {StateEnum} from "@shared/enums/state.enum";
import {KeyValueIconPlaceholder} from "@shared/models/key-value-placeholder.model";
import {ArchiveDisseminationPolicyService} from "@shared/services/archive-dissemination-policy.service";
import {sharedOrderActionNameSpace} from "@shared/stores/order/shared-order.action";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  AbstractFormPresentational,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "dlcm-order-all-order-create-routable",
  templateUrl: "./order-all-order-create.routable.html",
  styleUrls: ["./order-all-order-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderAllOrderCreateRoutable extends AbstractCreateRoutable<Order, OrderAllOrderStateModel> {
  @Select(OrderAllOrderState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(OrderAllOrderState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedOrderActionNameSpace;

  @ViewChild("formPresentational")
  readonly formPresentational: AbstractFormPresentational<Order>;

  listOptionsDisseminationPolicy: KeyValueIconPlaceholder[] = this._archiveDisseminationPolicyService.LIST_DEFAULT_DISSEMINATION_POLICIES_OPTIONS;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector,
              private readonly _archiveDisseminationPolicyService: ArchiveDisseminationPolicyService) {
    super(_store, _actions$, _changeDetector, StateEnum.order_allOrder, _injector, orderAllOrderActionNameSpace, StateEnum.order);
  }
}
