/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-all-order-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {DLCM_CONSTANTS} from "@app/constants";
import {Enums} from "@enums";
import {Order} from "@models";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {KeyValueOrgUnitDisseminationPolicyType} from "@shared/types/key-value-org-unit-dissemination-policy.type";
import {
  AbstractFormPresentational,
  EnumUtil,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  KeyValue,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-order-all-order-form",
  templateUrl: "./order-all-order-form.presentational.html",
  styleUrls: ["./order-all-order-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderAllOrderFormPresentational extends AbstractFormPresentational<Order> {
  private readonly _CONCATENATION_SEPARATOR: string = ";";

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  orderStatusEnumValuesTranslate: KeyValue[] = Enums.Order.StatusEnumTranslate;
  queryTypeEnumValues: KeyValue[] = Enums.Order.QueryTypeEnumTranslate;

  @Input()
  listOptionsDisseminationPolicy: KeyValueOrgUnitDisseminationPolicyType[];

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get isDisseminationReadonly(): boolean {
    return isNotNullNorUndefined(this.model) && this.model.status !== Enums.Order.StatusEnum.IN_PROGRESS;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.resId]: [undefined, []],
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.disseminationPolicyIdAndAssociationId]: [DLCM_CONSTANTS.DISSEMINATION_POLICY_BASIC_ID, [Validators.required, SolidifyValidator]],
      [this.formDefinition.queryType]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.query]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.status]: [undefined, []],
    });
  }

  protected _bindFormTo(order: Order): void {
    let concatenateDisseminationPolicy = order.disseminationPolicyId;
    if (isNotNullNorUndefined(order.organizationalUnitDisseminationPolicyId)) {
      concatenateDisseminationPolicy += this._CONCATENATION_SEPARATOR + order.organizationalUnitDisseminationPolicyId;
    }
    this.form = this._fb.group({
      [this.formDefinition.resId]: [order.resId, []],
      [this.formDefinition.name]: [order.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.disseminationPolicyIdAndAssociationId]: [concatenateDisseminationPolicy, [Validators.required, SolidifyValidator]],
      [this.formDefinition.queryType]: [order.queryType, [Validators.required, SolidifyValidator]],
      [this.formDefinition.query]: [order.query, [Validators.required, SolidifyValidator]],
      [this.formDefinition.status]: [order.status, []],
    });
    this.isValidWhenDisable = this.form.valid;
  }

  protected override _disableSpecificField(): void {
    super._disableSpecificField();
    if (this.isDisseminationReadonly) {
      this.form.get(this.formDefinition.disseminationPolicyIdAndAssociationId).disable();
    }
  }

  protected _treatmentBeforeSubmit(order: Order): Order {
    delete order.resId;
    delete order.status;
    delete order[this.formDefinition.disseminationPolicyIdAndAssociationId];
    const concatenateDisseminationPolicy = this.form.get(this.formDefinition.disseminationPolicyIdAndAssociationId).value;
    const disseminationPolicyArray = concatenateDisseminationPolicy.split(this._CONCATENATION_SEPARATOR);
    order.disseminationPolicyId = disseminationPolicyArray[0];
    order.organizationalUnitDisseminationPolicyId = disseminationPolicyArray.length > 1 ? disseminationPolicyArray[1] : undefined;
    return order;
  }

  concatenateIds(keyValueOrgUnitDisseminationPolicy: KeyValueOrgUnitDisseminationPolicyType): string {
    const compositeKey = keyValueOrgUnitDisseminationPolicy.object?.joinResource?.compositeKey;
    if (isNotNullNorUndefinedNorWhiteString(compositeKey)) {
      return `${keyValueOrgUnitDisseminationPolicy.key}${this._CONCATENATION_SEPARATOR}${compositeKey}`;
    }
    return keyValueOrgUnitDisseminationPolicy.key;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() resId: string;
  @PropertyName() name: string;
  @PropertyName() disseminationPolicyIdAndAssociationId: string;
  @PropertyName() queryType: string;
  @PropertyName() query: string;
  @PropertyName() status: string;
}
