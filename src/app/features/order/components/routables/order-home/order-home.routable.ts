/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
} from "@angular/core";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {LocalStateModel} from "@app/shared/models/local-state.model";
import {Enums} from "@enums";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {PermissionUtil} from "@shared/utils/permission.util";
import {
  LocalStorageHelper,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

@Component({
  selector: "dlcm-order-home-routable",
  templateUrl: "./order-home.routable.html",
  styleUrls: ["./order-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderHomeRoutable extends SharedAbstractPresentational {

  userRolesObs: Enums.UserApplicationRole.UserApplicationRoleEnum[];

  orderResources: OrderResource[] = [
    {
      avatarIcon: IconNameEnum.myOrder,
      titleToTranslate: LabelTranslateEnum.myOrders,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("order.myOrder.home.subtitle"),
      path: RoutesEnum.orderMyOrder,
      isVisible: () => true,
      numberNew: () => LocalStorageHelper.getListItem(LocalStorageEnum.newOrderAvailable).length,
    },
    {
      avatarIcon: IconNameEnum.allOrder,
      titleToTranslate: LabelTranslateEnum.allOrders,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("order.allOrder.home.subtitle"),
      path: RoutesEnum.orderAllOrder,
      isVisible: () => PermissionUtil.isUserHavePermission(true, ApplicationRolePermissionEnum.adminPermission, this.userRolesObs),
    },
  ];

  constructor(private readonly _store: Store) {
    super();
    this.userRolesObs = this._store.selectSnapshot((state: LocalStateModel) => state.application.userRoles);
  }

  navigate(path: RoutesEnum): void {
    this._store.dispatch(new Navigate([path]));
  }

  getOrderResources(): OrderResource[] {
    return this.orderResources.filter((resource) => resource.isVisible() === true);
  }
}

interface OrderResource {
  avatarIcon: IconNameEnum;
  titleToTranslate: string;
  subtitleToTranslate: string;
  path: RoutesEnum;
  isVisible: () => boolean;
  numberNew?: () => number;
}
