/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {OrderHomeRoutable} from "@app/features/order/components/routables/order-home/order-home.routable";
import {
  AppRoutesEnum,
  OrderRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {ApplicationRoleGuardService} from "@shared/guards/application-role-guard.service";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";
import {CombinedGuardService} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: OrderHomeRoutable,
    data: {},
  },
  {
    path: OrderRoutesEnum.myOrder,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/my-order/order-my-order.module").then(m => m.OrderMyOrderModule),
    data: {
      breadcrumb: LabelTranslateEnum.myOrders,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: OrderRoutesEnum.allOrder,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/all-order/order-all-order.module").then(m => m.OrderAllOrderModule),
    data: {
      breadcrumb: LabelTranslateEnum.allOrders,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderRoutingModule {

}
