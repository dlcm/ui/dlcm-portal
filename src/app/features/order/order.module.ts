/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - order.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {OrderHomeRoutable} from "@app/features/order/components/routables/order-home/order-home.routable";
import {OrderAllOrderAipState} from "@app/features/order/features/all-order/stores/aip/order-all-order-aip.state";
import {OrderAllOrderDipState} from "@app/features/order/features/all-order/stores/dip/order-all-order-dip.state";
import {OrderAllOrderState} from "@app/features/order/features/all-order/stores/order-all-order.state";
import {OrderAllOrderOrderArchiveState} from "@app/features/order/features/all-order/stores/order-archive/order-all-order-order-archive.state";
import {OrderAllOrderStatusHistoryState} from "@app/features/order/features/all-order/stores/status-history/order-all-order-status-history.state";
import {OrderRoutingModule} from "@app/features/order/order-routing.module";
import {OrderState} from "@app/features/order/stores/order.state";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {OrderMyOrderAipState} from "@order/features/my-order/stores/aip/order-my-order-aip.state";
import {OrderMyOrderDipState} from "@order/features/my-order/stores/dip/order-my-order-dip.state";
import {OrderMyOrderState} from "@order/features/my-order/stores/order-my-order.state";
import {OrderMyOrderStatusHistoryState} from "@order/features/my-order/stores/status-history/order-my-order-status-history.state";

const routables = [
  OrderHomeRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    OrderRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      OrderState,
      OrderAllOrderState,
      OrderAllOrderStatusHistoryState,
      OrderAllOrderAipState,
      OrderAllOrderDipState,
      OrderAllOrderOrderArchiveState,
      OrderMyOrderState,
      OrderMyOrderStatusHistoryState,
      OrderMyOrderDipState,
      OrderMyOrderAipState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class OrderModule {
}
