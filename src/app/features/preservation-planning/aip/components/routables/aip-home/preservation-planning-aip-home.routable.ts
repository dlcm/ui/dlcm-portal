/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-aip-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
} from "@angular/core";
import {Enums} from "@enums";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {ApiEnum} from "@shared/enums/api.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {LocalStateModel} from "@shared/models/local-state.model";
import {Storage} from "@shared/models/storage.model";

@Component({
  selector: "dlcm-preservation-planning-aip-home-routable",
  templateUrl: "./preservation-planning-aip-home.routable.html",
  styleUrls: ["./preservation-planning-aip-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningAipHomeRoutable extends SharedAbstractRoutable {

  userRolesObs: Enums.UserApplicationRole.UserApplicationRoleEnum[];

  aipResources: AipResource[] = [];

  constructor(private readonly _store: Store) {
    super();
    this.userRolesObs = this._store.selectSnapshot((state: LocalStateModel) => state.application.userRoles);
  }

  navigate(path: RoutesEnum): void {
    this._store.dispatch(new Navigate([path]));
  }

  getAipResources(): AipResource[] {
    const storagionUrls: Storage[] = ApiEnum.archivalStorageList;
    let i = 1;
    storagionUrls.forEach(storagion => {
      this.aipResources.push(
        {
          index: storagion.index,
          avatarIcon: IconNameEnum.storagion,
          titleToTranslate: LabelTranslateEnum.storagionInjectNumber,
          name: storagion.name,
          path: RoutesEnum.preservationPlanningAip + AppRoutesEnum.separator + storagion.index,
          isVisible: () => true,
          url: storagion.url,
        });
      i++;
    });
    return this.aipResources;
  }

  back(): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationPlanning]));
  }
}

interface AipResource {
  index: number;
  avatarIcon: IconNameEnum;
  titleToTranslate: string;
  path: RoutesEnum;
  isVisible: () => boolean;
  url: string;
  name: string;
}
