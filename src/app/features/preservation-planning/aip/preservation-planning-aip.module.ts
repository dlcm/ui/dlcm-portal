/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-aip.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {PreservationPlanningAipHomeRoutable} from "@preservation-planning/aip/components/routables/aip-home/preservation-planning-aip-home.routable";
import {PreservationPlanningAipRoutingModule} from "@preservation-planning/aip/preservation-planning-aip-routing.module";
import {SharedAipApproveDisposalDialog} from "@shared/features/aip/components/dialogs/aip-approve-disposal/shared-aip-approve-disposal.dialog";
import {SharedAipExtendRetentionDialog} from "@shared/features/aip/components/dialogs/aip-extend-retention/shared-aip-extend-retention.dialog";

const routables = [
  PreservationPlanningAipHomeRoutable,
];
const containers = [];
const dialogs = [
  SharedAipExtendRetentionDialog,
  SharedAipApproveDisposalDialog,
];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    PreservationPlanningAipRoutingModule,
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationPlanningAipModule {
}
