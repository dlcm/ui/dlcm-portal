/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-aip-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {PreservationPlanningAipHomeRoutable} from "@preservation-planning/aip/components/routables/aip-home/preservation-planning-aip-home.routable";
import {PreservationPlanningAipDetailGuardService} from "@preservation-planning/aip/preservation-planning-aip-detail-guard.service";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
  SharedAipRoutesEnum,
} from "@shared/enums/routes.enum";
import {SharedAipCollectionDetailRoutable} from "@shared/features/aip/components/routables/aip-collection-detail/shared-aip-collection-detail.routable";
import {SharedAipCollectionRoutable} from "@shared/features/aip/components/routables/aip-collection/shared-aip-collection.routable";
import {SharedAipDetailEditRoutable} from "@shared/features/aip/components/routables/aip-detail-edit/shared-aip-detail-edit.routable";
import {SharedAipFileDetailRoutable} from "@shared/features/aip/components/routables/aip-file-detail/shared-aip-file-detail.routable";
import {SharedAipFileRoutable} from "@shared/features/aip/components/routables/aip-file/shared-aip-file.routable";
import {SharedAipListRoutable} from "@shared/features/aip/components/routables/aip-list/shared-aip-list.routable";
import {SharedAipMetadataRoutable} from "@shared/features/aip/components/routables/aip-metadata/shared-aip-metadata.routable";
import {SharedAipNotFoundRoutable} from "@shared/features/aip/components/routables/aip-not-found/shared-aip-not-found.routable";
import {SharedAipStoragionRootRoutable} from "@shared/features/aip/components/routables/aip-storagion-root/shared-aip-storagion-root.routable";
import {SharedAipTabsRoutable} from "@shared/features/aip/components/routables/aip-tabs/shared-aip-tabs.routable";
import {AipHelper} from "@shared/features/aip/helpers/aip.helper";
import {SharedAipSharedModule} from "@shared/features/aip/shared-aip-shared.module";
import {SharedAipState} from "@shared/features/aip/stores/shared-aip.state";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";
import {
  CombinedGuardService,
  MappingObject,
  MappingObjectUtil,
  StringUtil,
} from "solidify-frontend";

export const callbackAipStoragionBreadcrumb: (params: MappingObject<string, string>) => string = (params: MappingObject<string, string>) => AipHelper.getStoragionNameByIndex(+MappingObjectUtil.get(params, PreservationPlanningRoutesEnum.storagionNumberWithoutPrefixParam as string));
export const callbackAipTabBreadcrumb: (params: MappingObject<string, string>) => string = (params: MappingObject<string, string>) => {
  const tab = MappingObjectUtil.get(params, SharedAipRoutesEnum.tabWithoutPrefixParam as string);
  let name = StringUtil.stringEmpty;
  if (tab === SharedAipRoutesEnum.aipTabAll) {
    name = LabelTranslateEnum.all;
  } else if (tab === SharedAipRoutesEnum.aipTabUnit) {
    name = LabelTranslateEnum.units;
  } else if (tab === SharedAipRoutesEnum.aipTabCollections) {
    name = LabelTranslateEnum.collection;
  }
  return name;
};

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: PreservationPlanningAipHomeRoutable,
    data: {},
  },
  {
    path: PreservationPlanningRoutesEnum.storagionNumber,
    redirectTo: PreservationPlanningRoutesEnum.storagionNumber + AppRoutesEnum.separator + SharedAipRoutesEnum.aipList + AppRoutesEnum.separator + SharedAipRoutesEnum.aipTabAll,
    pathMatch: "full",
  },
  {
    path: PreservationPlanningRoutesEnum.storagionNumber + AppRoutesEnum.separator + SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    redirectTo: PreservationPlanningRoutesEnum.storagionNumber + AppRoutesEnum.separator + SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId + AppRoutesEnum.separator + SharedAipRoutesEnum.aipMetadata,
    pathMatch: "full",
  },
  {
    path: PreservationPlanningRoutesEnum.storagionNumber,
    component: SharedAipStoragionRootRoutable,
    data: {
      breadcrumb: callbackAipStoragionBreadcrumb,
    },
    children: [
      {
        path: SharedAipRoutesEnum.aipNotFound + AppRoutesEnum.separator + AppRoutesEnum.paramId,
        component: SharedAipNotFoundRoutable,
      },
      {
        path: SharedAipRoutesEnum.aipList,
        component: SharedAipTabsRoutable,
        data: {},
        children: [
          {
            path: SharedAipRoutesEnum.tab,
            component: SharedAipListRoutable,
            data: {
              breadcrumb: callbackAipTabBreadcrumb,
              noBreadcrumbLink: true,
            },
          },
        ],
      },
      {
        path: SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
        component: SharedAipDetailEditRoutable,
        data: {
          breadcrumbMemoizedSelector: SharedAipState.currentAipName,
          guards: [PreservationPlanningAipDetailGuardService],
        },
        canActivate: [CombinedGuardService],
        children: [
          {
            path: SharedAipRoutesEnum.aipMetadata,
            component: SharedAipMetadataRoutable,
            data: {
              breadcrumb: LabelTranslateEnum.metadata,
              noBreadcrumbLink: true,
            },
          },
          {
            path: SharedAipRoutesEnum.aipCollections,
            component: SharedAipCollectionRoutable,
            data: {
              breadcrumb: LabelTranslateEnum.collection,
              noBreadcrumbLink: true,
            },
            children: [
              {
                path: AppRoutesEnum.paramId,
                component: SharedAipCollectionDetailRoutable,
                data: {
                  breadcrumb: LabelTranslateEnum.detail,
                },
              },
            ],
          },
          {
            path: SharedAipRoutesEnum.aipFiles,
            component: SharedAipFileRoutable,
            data: {
              breadcrumb: LabelTranslateEnum.files,
              noBreadcrumbLink: true,
            },
            children: [
              {
                path: AppRoutesEnum.paramId,
                component: SharedAipFileDetailRoutable,
                data: {
                  breadcrumb: LabelTranslateEnum.detail,
                },
              },
            ],
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [SharedAipSharedModule.forChild(routes)],
  exports: [SharedAipSharedModule],
})
export class PreservationPlanningAipRoutingModule {
}
