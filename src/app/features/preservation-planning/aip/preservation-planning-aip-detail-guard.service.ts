/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-aip-detail-guard.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
} from "@angular/router";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
} from "@shared/enums/routes.enum";
import {SharedAipAction} from "@shared/features/aip/stores/shared-aip.action";
import {RouteUtil} from "@shared/utils/route.util";
import {
  Observable,
  of,
} from "rxjs";
import {
  map,
  take,
} from "rxjs/operators";
import {
  isNullOrUndefined,
  isTrue,
  ofSolidifyActionCompleted,
} from "solidify-frontend";

@Injectable({
  providedIn: "root",
})
export class PreservationPlanningAipDetailGuardService implements CanActivate {
  constructor(private readonly _router: Router,
              private readonly _store: Store,
              private readonly _actions$: Actions) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this._isAuthorized(route);
  }

  private _isAuthorized(route: ActivatedRouteSnapshot): Observable<boolean> {
    const idAip = route.params[AppRoutesEnum.paramIdWithoutPrefixParam];
    const storagionNodeId = +route.parent.params[PreservationPlanningRoutesEnum.storagionNumberWithoutPrefixParam];

    const storage = ApiEnum.archivalStorageList.find(storagion => storagion.index === storagionNodeId);
    if (isNullOrUndefined(storage)) {
      this._router.navigate([RoutesEnum.preservationPlanningAip]);
      return of(false);
    }

    const url = RouteUtil.generateFullUrlStringFromActivatedRouteSnapshot(route);
    this._store.dispatch(new SharedAipAction.GetById(idAip, false, false, storagionNodeId, url));
    return this._actions$.pipe(
      ofSolidifyActionCompleted(SharedAipAction.GetById),
      take(1),
      map((result) => {
        if (isTrue(result.result.successful)) {
          return true;
        } else {
          this._router.navigate([RoutesEnum.preservationPlanningAip, storagionNodeId, SharedAipRoutesEnum.aipNotFound, idAip]);
          return false;
        }
      }),
    );
  }
}
