/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  JobExecution,
  JobType,
  PreservationJob,
} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {
  defaultPreservationPlanningJobExecutionInitValue,
  PreservationPlanningJobExecutionState,
  PreservationPlanningJobExecutionStateModel,
} from "@preservation-planning/job/stores/job-execution/preservation-planning-job-execution.state";
import {
  PreservationPlanningJobAction,
  preservationPlanningJobActionNameSpace,
} from "@preservation-planning/job/stores/preservation-planning-job.action";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  isEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface PreservationPlanningJobStateModel extends ResourceStateModel<PreservationJob> {
  listJobTypes: JobType[] | undefined;
  listJobRecurrences: Enums.PreservationJob.JobRecurrenceEnum[] | undefined;
  [StateEnum.preservationPlanning_job_execution]: PreservationPlanningJobExecutionStateModel | undefined;
}

@Injectable()
@State<PreservationPlanningJobStateModel>({
  name: StateEnum.preservationPlanning_job,
  defaults: {
    ...defaultResourceStateInitValue(),
    listJobTypes: undefined,
    listJobRecurrences: undefined,
    [StateEnum.preservationPlanning_job_execution]: {...defaultPreservationPlanningJobExecutionInitValue()},
  },
  children: [
    PreservationPlanningJobExecutionState,
  ],
})
export class PreservationPlanningJobState extends ResourceState<PreservationPlanningJobStateModel, PreservationJob> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationPlanningJobActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.preservationPlanningJobDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.preservationPlanningJobDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.preservationPlanningJob,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("preservation.job.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("preservation.job.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("preservation.job.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.preservationPlanningPreservationJobs;
  }

  @Selector()
  static isLoading(state: PreservationPlanningJobStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: PreservationPlanningJobStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: PreservationPlanningJobStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: PreservationPlanningJobStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: PreservationPlanningJobStateModel): boolean {
    return true;
  }

  @Action(PreservationPlanningJobAction.Init)
  init(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.Init): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + ApiActionNameEnum.INIT).pipe(
      tap(result => {
        if (result?.status === ResultActionStatusEnum.EXECUTED) {
          ctx.dispatch(new PreservationPlanningJobAction.InitSuccess(action));
        } else {
          ctx.dispatch(new PreservationPlanningJobAction.InitFail(action));
        }
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new PreservationPlanningJobAction.InitFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(PreservationPlanningJobAction.InitSuccess)
  initSuccess(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.InitSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new PreservationPlanningJobAction.GetAll());
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("preservation.job.notification.init.success"));
  }

  @Action(PreservationPlanningJobAction.InitFail)
  initFail(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.InitFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("preservation.job.notification.init.fail"));
  }

  @Action(PreservationPlanningJobAction.VerifyBeforeStart)
  verifyBeforeStart(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.VerifyBeforeStart): Observable<CollectionTyped<JobExecution>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const JOB_EXEC_STATUS: keyof JobExecution = "status";
    const queryParameters = new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo);
    MappingObjectUtil.set(queryParameters.search.searchItems, JOB_EXEC_STATUS, Enums.PreservationJob.StatusEnum.IN_PROGRESS);

    return this._apiService.getCollection<JobExecution>(this._urlResource + urlSeparator + action.resId + urlSeparator + ApiResourceNameEnum.PRES_JOB_EXECUTION, queryParameters)
      .pipe(
        tap(result => {
          if (isEmptyArray(result._data)) {
            ctx.dispatch(new PreservationPlanningJobAction.VerifyBeforeStartSuccess(action));
          } else {
            ctx.dispatch(new PreservationPlanningJobAction.VerifyBeforeStartFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationPlanningJobAction.StartFail(action, undefined));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(PreservationPlanningJobAction.VerifyBeforeStartSuccess)
  verifyBeforeStartSuccess(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.VerifyBeforeStartSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new PreservationPlanningJobAction.Start(action.parentAction.resId));
  }

  @Action(PreservationPlanningJobAction.VerifyBeforeStartFail)
  verifyBeforeStartFail(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.VerifyBeforeStartFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("preservation.job.notification.verify.fail"));
  }

  @Action(PreservationPlanningJobAction.Start)
  start(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.Start): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.resId + urlSeparator + ApiActionNameEnum.START).pipe(
      tap(result => {
        if (result?.status === ResultActionStatusEnum.EXECUTED) {
          ctx.dispatch(new PreservationPlanningJobAction.StartSuccess(action));
        } else {
          ctx.dispatch(new PreservationPlanningJobAction.StartFail(action, result.message));
        }
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new PreservationPlanningJobAction.StartFail(action, undefined));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(PreservationPlanningJobAction.StartSuccess)
  startSuccess(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.StartSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("preservation.job.notification.start.success"));
  }

  @Action(PreservationPlanningJobAction.StartFail)
  startFail(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.StartFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(isNotNullNorUndefinedNorWhiteString(action.message) ? action.message : MARK_AS_TRANSLATABLE("preservation.job.notification.start.fail"));
  }

  @Action(PreservationPlanningJobAction.Resume)
  resume(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.Resume): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.resId + urlSeparator + ApiActionNameEnum.RESUME).pipe(
      tap(result => {
        if (result?.status === ResultActionStatusEnum.EXECUTED) {
          ctx.dispatch(new PreservationPlanningJobAction.ResumeSuccess(action));
        } else {
          ctx.dispatch(new PreservationPlanningJobAction.ResumeFail(action));
        }
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new PreservationPlanningJobAction.ResumeFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(PreservationPlanningJobAction.ResumeSuccess)
  resumeSuccess(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.ResumeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("preservation.job.notification.resume.success"));
  }

  @Action(PreservationPlanningJobAction.ResumeFail)
  resumeFail(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.ResumeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("preservation.job.notification.resume.fail"));
  }

  @Action(PreservationPlanningJobAction.ListJobTypes)
  listJobTypes(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.ListJobTypes): Observable<JobType[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.getList<JobType>(this._urlResource + urlSeparator + ApiActionNameEnum.LIST_JOB_TYPE, null).pipe(
      tap(result => {
        ctx.dispatch(new PreservationPlanningJobAction.ListJobTypesSuccess(action, result));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new PreservationPlanningJobAction.ListJobTypesFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(PreservationPlanningJobAction.ListJobTypesSuccess)
  listJobTypesSuccess(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.ListJobTypesSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      listJobTypes: action.listJobTypes,
    });
  }

  @Action(PreservationPlanningJobAction.ListJobTypesFail)
  listJobTypesFail(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.ListJobTypesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(PreservationPlanningJobAction.ListJobRecurrences)
  listJobRecurrences(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.ListJobRecurrences): Observable<Enums.PreservationJob.JobRecurrenceEnum[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.getList<Enums.PreservationJob.JobRecurrenceEnum>(this._urlResource + urlSeparator + ApiActionNameEnum.LIST_JOB_RECURRENCE, null)
      .pipe(
        tap(result => {
          ctx.dispatch(new PreservationPlanningJobAction.ListJobRecurrencesSuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationPlanningJobAction.ListJobRecurrencesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(PreservationPlanningJobAction.ListJobRecurrencesSuccess)
  listJobRecurrencesSuccess(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.ListJobRecurrencesSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      listJobRecurrences: action.listJobRecurrences,
    });
  }

  @Action(PreservationPlanningJobAction.ListJobRecurrencesFail)
  listJobRecurrencesFail(ctx: SolidifyStateContext<PreservationPlanningJobStateModel>, action: PreservationPlanningJobAction.ListJobRecurrencesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
