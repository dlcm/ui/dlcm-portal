/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-execution-report.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {
  JobExecutionReport,
  JobExecutionReportLine,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {PreservationPlanningDepositAction} from "@preservation-planning/deposit/stores/preservation-planning-deposit.action";
import {ReportNamespace} from "@preservation-planning/job/stores/job-execution/job-execution-report/preservation-planning-job-execution-report-namespace.model";
import {
  PreservationPlanningJobExecutionReportAction,
  preservationPlanningJobExecutionReportNamespace,
} from "@preservation-planning/job/stores/job-execution/job-execution-report/preservation-planning-job-execution-report.action";
import {PreservationPlanningJobExecutionStateModel} from "@preservation-planning/job/stores/job-execution/preservation-planning-job-execution.state";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseState,
  BaseStateModel,
  CollectionTyped,
  defaultBaseStateInitValue,
  NotificationService,
  ObjectUtil,
  QueryParameters,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export const defaultPreservationPlanningJobExecutionReportInitValue: () => PreservationPlanningJobExecutionReportStateModel = () =>
  ({
    ...defaultBaseStateInitValue(),
    reports: [],
    queryParameters: new QueryParameters(environment.defaultPageSize),
    lineReport: [],
    queryParametersLines: new QueryParameters(environment.defaultPageSize),
  });

export interface PreservationPlanningJobExecutionReportStateModel extends BaseStateModel {
  reports: JobExecutionReport[];
  queryParameters: QueryParameters;
  lineReport: JobExecutionReportLine[];
  queryParametersLines: QueryParameters;
}

@Injectable()
@State<PreservationPlanningJobExecutionReportStateModel>({
  name: StateEnum.preservationPlanning_job_execution_report,
  defaults: {
    ...defaultPreservationPlanningJobExecutionReportInitValue(),
  },
  children: [],
})
export class PreservationPlanningJobExecutionReportState extends BaseState<PreservationPlanningJobExecutionReportStateModel> {

  protected readonly _nameSpace: ReportNamespace;

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: preservationPlanningJobExecutionReportNamespace,
    }, PreservationPlanningJobExecutionReportState);
  }

  protected get _urlResource(): string {
    return ApiEnum.preservationPlanningPreservationJobs;
  }

  getQueryParametersToApply(queryParameters: QueryParameters, queryParameterState: QueryParameters): QueryParameters {
    return queryParameters == null ? queryParameterState : queryParameters;
  }

  updateQueryParameters<T>(ctx: SolidifyStateContext<PreservationPlanningJobExecutionReportStateModel>, list: CollectionTyped<T> | null | undefined, queryParametersLine: boolean): QueryParameters {
    let queryParameters = null;
    if (queryParametersLine) {
      queryParameters = ObjectUtil.clone(ctx.getState().queryParametersLines);
    } else {
      queryParameters = ObjectUtil.clone(ctx.getState().queryParameters);
    }
    const paging = ObjectUtil.clone(queryParameters.paging);
    paging.length = list === null || list === undefined ? 0 : list._page.totalItems;
    queryParameters.paging = paging;
    return queryParameters;
  }

  @Action(PreservationPlanningJobExecutionReportAction.Report)
  report(ctx: SolidifyStateContext<PreservationPlanningJobExecutionReportStateModel>, action: PreservationPlanningJobExecutionReportAction.Report): Observable<CollectionTyped<JobExecutionReport>> {
    ctx.patchState({
      reports: [],
      lineReport: [],
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: this.getQueryParametersToApply(action.queryParameters, ctx.getState().queryParameters),
    });
    return this._apiService.getCollection<JobExecutionReport>(this._urlResource + urlSeparator + action.parentId + urlSeparator + ApiResourceNameEnum.PRES_JOB_EXECUTION + urlSeparator + action.id + urlSeparator + ApiActionNameEnum.REPORT, ctx.getState().queryParameters)
      .pipe(
        tap((list: CollectionTyped<JobExecutionReport>) => {
          ctx.dispatch(new this._nameSpace.ReportSuccess(action, list));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new this._nameSpace.ReportFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(PreservationPlanningJobExecutionReportAction.ReportSuccess)
  reportSuccess(ctx: SolidifyStateContext<PreservationPlanningJobExecutionReportStateModel>, action: PreservationPlanningJobExecutionReportAction.ReportSuccess): void {
    const queryParameters = this.updateQueryParameters(ctx, action.list, false);
    ctx.patchState({
      reports: action.list._data,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: queryParameters,
    });
  }

  @Action(PreservationPlanningJobExecutionReportAction.ReportFail)
  reportFail(ctx: SolidifyStateContext<PreservationPlanningJobExecutionReportStateModel>, action: PreservationPlanningJobExecutionReportAction.ReportFail): void {
    ctx.patchState({
      reports: [],
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(PreservationPlanningJobExecutionReportAction.ReportLines)
  reportLines(ctx: SolidifyStateContext<PreservationPlanningJobExecutionReportStateModel>, action: PreservationPlanningJobExecutionReportAction.ReportLines): Observable<CollectionTyped<JobExecutionReportLine>> {
    ctx.patchState({
      lineReport: [],
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParametersLines: this.getQueryParametersToApply(action.queryParameters, ctx.getState().queryParametersLines),
    });

    return this._apiService.getCollection<JobExecutionReportLine>(this._urlResource + urlSeparator + action.parentId + urlSeparator + ApiResourceNameEnum.PRES_JOB_EXECUTION + urlSeparator + action.id + urlSeparator + ApiActionNameEnum.REPORT + urlSeparator + action.reportId + urlSeparator + "detail", action.queryParameters)
      .pipe(
        tap((list: CollectionTyped<JobExecutionReportLine>) => {
          ctx.dispatch(new this._nameSpace.ReportLinesSuccess(action, list));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new this._nameSpace.ReportLinesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(PreservationPlanningJobExecutionReportAction.ReportLinesSuccess)
  reportLinesSuccess(ctx: SolidifyStateContext<PreservationPlanningJobExecutionReportStateModel>, action: PreservationPlanningJobExecutionReportAction.ReportLinesSuccess): void {
    const queryParameters = this.updateQueryParameters(ctx, action.list, true);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      lineReport: action.list._data,
      queryParametersLines: queryParameters,
    });
  }

  @Action(PreservationPlanningJobExecutionReportAction.ReportLinesFail)
  reportLinesFail(ctx: SolidifyStateContext<PreservationPlanningJobExecutionReportStateModel>, action: PreservationPlanningJobExecutionReportAction.ReportLinesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(PreservationPlanningJobExecutionReportAction.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<PreservationPlanningJobExecutionReportStateModel>, action: PreservationPlanningJobExecutionReportAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });

    ctx.dispatch(new this._nameSpace.Report(action.id, action.parentId, action.queryParameters));
  }

  @Action(PreservationPlanningJobExecutionReportAction.ChangeQueryParametersLines)
  changeQueryParametersLines(ctx: SolidifyStateContext<PreservationPlanningJobExecutionReportStateModel>, action: PreservationPlanningJobExecutionReportAction.ChangeQueryParametersLines): void {
    ctx.patchState({
      queryParametersLines: action.queryParameters,
    });

    ctx.dispatch(new this._nameSpace.ReportLines(action.id, action.parentId, action.reportId, action.queryParameters));
  }

  @Action(PreservationPlanningJobExecutionReportAction.CleanQueryParameters)
  cleanQueryParameters(ctx: SolidifyStateContext<PreservationPlanningJobExecutionReportStateModel>, action: PreservationPlanningJobExecutionReportAction.CleanQueryParameters): void {
    ctx.patchState({
      queryParameters: new QueryParameters(),
    });
  }

  @Action(PreservationPlanningJobExecutionReportAction.CleanQueryParametersLines)
  cleanQueryParametersLines(ctx: SolidifyStateContext<PreservationPlanningJobExecutionReportStateModel>, action: PreservationPlanningJobExecutionReportAction.CleanQueryParametersLines): void {
    ctx.patchState({
      queryParametersLines: new QueryParameters(),
    });
  }

  @Action(PreservationPlanningJobExecutionReportAction.GoToAip)
  goToAip(ctx: SolidifyStateContext<PreservationPlanningJobExecutionStateModel>, action: PreservationPlanningJobExecutionReportAction.GoToAip): void {
    const pathAipDetail = RoutesEnum.preservationPlanningAip + urlSeparator + 1 + urlSeparator + SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator;
    const path = [pathAipDetail, action.aipId];
    ctx.dispatch(new Navigate(path));
  }

  @Action(PreservationPlanningJobExecutionReportAction.GoToSip)
  goToSip(ctx: SolidifyStateContext<PreservationPlanningJobExecutionStateModel>, action: PreservationPlanningJobExecutionReportAction.GoToSip): void {
    const pathSipDetail = RoutesEnum.preservationPlanningSip + urlSeparator + PreservationPlanningRoutesEnum.sipDetail + AppRoutesEnum.separator;
    const path = [pathSipDetail, action.sipId];
    ctx.dispatch(new Navigate(path));
  }

  @Action(PreservationPlanningJobExecutionReportAction.GoToDeposit)
  goToDeposit(ctx: SolidifyStateContext<PreservationPlanningJobExecutionStateModel>, action: PreservationPlanningJobExecutionReportAction.GoToDeposit): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(ctx as SolidifyStateContext<any>, this._actions$,
      new PreservationPlanningDepositAction.GetById(action.depositId),
      PreservationPlanningDepositAction.GetByIdSuccess,
      resultAction => {
        ctx.dispatch(new Navigate([RoutesEnum.deposit, resultAction.model.organizationalUnitId, DepositRoutesEnum.detail, action.depositId]));
      }));
  }
}

