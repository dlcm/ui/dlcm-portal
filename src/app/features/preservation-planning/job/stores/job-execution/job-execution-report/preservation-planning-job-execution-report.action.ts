/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-execution-report.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  JobExecutionReport,
  JobExecutionReportLine,
} from "@models";
import {ReportNamespace} from "@preservation-planning/job/stores/job-execution/job-execution-report/preservation-planning-job-execution-report-namespace.model";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  CollectionTyped,
  QueryParameters,
} from "solidify-frontend";

const state = StateEnum.preservationPlanning_job_execution_report;

export namespace PreservationPlanningJobExecutionReportAction {

  export class GoToAip {
    static readonly type: string = `[${state}] Go To Aip`;

    constructor(public aipId: string) {
    }
  }

  export class GoToSip {
    static readonly type: string = `[${state}] Go To Sip`;

    constructor(public sipId: string) {
    }
  }

  export class GoToDeposit {
    static readonly type: string = `[${state}] Go To Deposit`;

    constructor(public depositId: string) {
    }
  }

  export class Report extends BaseAction {
    static readonly type: string = `[${state}] Report`;

    constructor(public id: string, public parentId: string, public queryParameters?: QueryParameters) {
      super();
    }
  }

  export class ReportSuccess extends BaseSubActionSuccess<Report> {
    static readonly type: string = `[${state}] Report Success`;

    constructor(public parentAction: Report, public list: CollectionTyped<JobExecutionReport>) {
      super(parentAction);
    }
  }

  export class ReportFail extends BaseSubActionFail<Report> {
    static readonly type: string = `[${state}] Report Fail`;
  }

  export class ChangeQueryParameters extends BaseAction {
    static readonly type: string = `[${state}] Change Query Parameter`;

    constructor(public queryParameters: QueryParameters, public id: string, public parentId?: string) {
      super();
    }
  }

  export class ReportLines extends BaseAction {
    static readonly type: string = `[${state}] Report Lines`;

    constructor(public id: string, public parentId: string, public reportId: string, public queryParameters?: QueryParameters) {
      super();
    }
  }

  export class ReportLinesSuccess extends BaseSubActionSuccess<ReportLines> {
    static readonly type: string = `[${state}] Report Lines Success`;

    constructor(public parentAction: ReportLines, public list: CollectionTyped<JobExecutionReportLine>) {
      super(parentAction);
    }
  }

  export class ReportLinesFail extends BaseSubActionFail<ReportLines> {
    static readonly type: string = `[${state}]] Report Lines Fail`;
  }

  export class ChangeQueryParametersLines extends BaseAction {
    static readonly type: string = `[${state}] Change Query Parameters Lines`;

    constructor(public queryParameters: QueryParameters, public id: string, public parentId: string, public reportId: string) {
      super();
    }
  }

  export class CleanQueryParametersLines extends BaseAction {
    static readonly type: string = `[${state}] Clean Query Parameters Lines`;

    constructor() {
      super();
    }
  }

  export class CleanQueryParameters extends BaseAction {
    static readonly type: string = `[${state}] Clean Query Parameters`;

    constructor() {
      super();
    }
  }
}

export const preservationPlanningJobExecutionReportNamespace: ReportNamespace = PreservationPlanningJobExecutionReportAction;
