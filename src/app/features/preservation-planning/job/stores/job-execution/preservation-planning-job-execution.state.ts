/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-execution.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {
  JobExecution,
  JobExecutionReport,
} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {
  defaultPreservationPlanningJobExecutionReportInitValue,
  PreservationPlanningJobExecutionReportState,
  PreservationPlanningJobExecutionReportStateModel,
} from "@preservation-planning/job/stores/job-execution/job-execution-report/preservation-planning-job-execution-report.state";
import {
  preservationJobExecutionActionNameSpace,
  PreservationPlanningJobExecutionAction,
} from "@preservation-planning/job/stores/job-execution/preservation-planning-job-execution.action";
import {PreservationPlanningJobStateModel} from "@preservation-planning/job/stores/preservation-planning-job.state";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export const defaultPreservationPlanningJobExecutionInitValue: () => PreservationPlanningJobExecutionStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    [StateEnum.preservationPlanning_job_execution_report]: {...defaultPreservationPlanningJobExecutionReportInitValue()},
  });

export interface PreservationPlanningJobExecutionStateModel extends CompositionStateModel<JobExecution> {
  [StateEnum.preservationPlanning_job_execution_report]: PreservationPlanningJobExecutionReportStateModel;
}

@Injectable()
@State<PreservationPlanningJobExecutionStateModel>({
  name: StateEnum.preservationPlanning_job_execution,
  defaults: {
    ...defaultCompositionStateInitValue(),
    [StateEnum.preservationPlanning_job_execution_report]: {...defaultPreservationPlanningJobExecutionReportInitValue()},
  },
  children: [
    PreservationPlanningJobExecutionReportState,
  ],
})
export class PreservationPlanningJobExecutionState extends CompositionState<PreservationPlanningJobExecutionStateModel, JobExecution> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationJobExecutionActionNameSpace,
      resourceName: ApiResourceNameEnum.PRES_JOB_EXECUTION,
    });
  }

  @Selector()
  static currentTitle(state: PreservationPlanningJobExecutionStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return "Run " + state.current.runNumber;
  }

  @Selector()
  static isReadyToBeDisplayed(state: PreservationPlanningJobStateModel): boolean {
    return !isNullOrUndefined(state.current);
  }

  @Selector()
  static isLoading(state: PreservationPlanningJobStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static listReport(state: PreservationPlanningJobExecutionStateModel): JobExecutionReport[] {
    return state[StateEnum.preservationPlanning_job_execution_report].reports;
  }

  protected get _urlResource(): string {
    return ApiEnum.preservationPlanningPreservationJobs;
  }

  @Action(PreservationPlanningJobExecutionAction.Resume)
  resume(ctx: SolidifyStateContext<PreservationPlanningJobExecutionStateModel>, action: PreservationPlanningJobExecutionAction.Resume): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.parentId + urlSeparator + ApiResourceNameEnum.PRES_JOB_EXECUTION + urlSeparator + action.jobExecutionId + urlSeparator + ApiActionNameEnum.RESUME)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new PreservationPlanningJobExecutionAction.ResumeSuccess(action));
          } else {
            ctx.dispatch(new PreservationPlanningJobExecutionAction.ResumeFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationPlanningJobExecutionAction.ResumeFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(PreservationPlanningJobExecutionAction.ResumeSuccess)
  resumeSuccess(ctx: SolidifyStateContext<PreservationPlanningJobExecutionStateModel>, action: PreservationPlanningJobExecutionAction.ResumeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("preservation.jobExecution.notification.resume.success"));
  }

  @Action(PreservationPlanningJobExecutionAction.ResumeFail)
  resumeFail(ctx: SolidifyStateContext<PreservationPlanningJobExecutionStateModel>, action: PreservationPlanningJobExecutionAction.ResumeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("preservation.jobExecution.notification.resume.fail"));
  }
}
