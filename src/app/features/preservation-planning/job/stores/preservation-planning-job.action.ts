/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {
  JobType,
  PreservationJob,
} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  ResourceAction,
  ResourceNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.preservationPlanning_job;

export namespace PreservationPlanningJobAction {
  @TypeDefaultAction(state)
  export class LoadResource extends ResourceAction.LoadResource {
  }

  @TypeDefaultAction(state)
  export class LoadResourceSuccess extends ResourceAction.LoadResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class LoadResourceFail extends ResourceAction.LoadResourceFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends ResourceAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends ResourceAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends ResourceAction.GetAllSuccess<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends ResourceAction.GetAllFail<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class GetByListId extends ResourceAction.GetByListId {
  }

  @TypeDefaultAction(state)
  export class GetByListIdSuccess extends ResourceAction.GetByListIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetByListIdFail extends ResourceAction.GetByListIdFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends ResourceAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends ResourceAction.GetByIdSuccess<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends ResourceAction.GetByIdFail<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class Create extends ResourceAction.Create<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends ResourceAction.CreateSuccess<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends ResourceAction.CreateFail<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class Update extends ResourceAction.Update<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends ResourceAction.UpdateSuccess<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends ResourceAction.UpdateFail<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class Delete extends ResourceAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends ResourceAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends ResourceAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends ResourceAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends ResourceAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends ResourceAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class AddInList extends ResourceAction.AddInList<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class AddInListById extends ResourceAction.AddInListById {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdSuccess extends ResourceAction.AddInListByIdSuccess<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdFail extends ResourceAction.AddInListByIdFail<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class RemoveInListById extends ResourceAction.RemoveInListById {
  }

  @TypeDefaultAction(state)
  export class RemoveInListByListId extends ResourceAction.RemoveInListByListId {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkList extends ResourceAction.LoadNextChunkList {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListSuccess extends ResourceAction.LoadNextChunkListSuccess<PreservationJob> {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListFail extends ResourceAction.LoadNextChunkListFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends ResourceAction.Clean {
  }

  export class Resume extends BaseAction {
    static readonly type: string = `[${state}] Resume`;

    constructor(public resId: string) {
      super();
    }
  }

  export class ResumeSuccess extends BaseSubActionSuccess<Resume> {
    static readonly type: string = `[${state}] Resume Success`;
  }

  export class ResumeFail extends BaseSubActionFail<Resume> {
    static readonly type: string = `[${state}] Resume Fail`;
  }

  export class Start extends BaseAction {
    static readonly type: string = `[${state}] Start`;

    constructor(public resId: string) {
      super();
    }
  }

  export class StartSuccess extends BaseSubActionSuccess<Start> {
    static readonly type: string = `[${state}] Start Success`;
  }

  export class StartFail extends BaseSubActionFail<Start> {
    static readonly type: string = `[${state}] Start Fail`;

    constructor(public parentAction: Start, public message: string) {
      super(parentAction);
    }
  }

  export class VerifyBeforeStart extends BaseAction {
    static readonly type: string = `[${state}] Verify Before Start`;

    constructor(public resId: string) {
      super();
    }
  }

  export class VerifyBeforeStartSuccess extends BaseSubActionSuccess<VerifyBeforeStart> {
    static readonly type: string = `[${state}] Verify Before Start Success`;
  }

  export class VerifyBeforeStartFail extends BaseSubActionFail<VerifyBeforeStart> {
    static readonly type: string = `[${state}] Verify Start Fail`;
  }

  export class Init extends BaseAction {
    static readonly type: string = `[${state}] Init`;
  }

  export class InitSuccess extends BaseSubActionSuccess<Init> {
    static readonly type: string = `[${state}] Init Success`;
  }

  export class InitFail extends BaseSubActionFail<Init> {
    static readonly type: string = `[${state}] Init Fail`;
  }

  export class ListJobRecurrences extends BaseAction {
    static readonly type: string = `[${state}] List Job Recurrences`;
  }

  export class ListJobRecurrencesSuccess extends BaseSubActionSuccess<ListJobRecurrences> {
    static readonly type: string = `[${state}] List Job Recurrences Success`;

    constructor(public parent: ListJobRecurrences, public listJobRecurrences: Enums.PreservationJob.JobRecurrenceEnum[]) {
      super(parent);
    }
  }

  export class ListJobRecurrencesFail extends BaseSubActionFail<ListJobRecurrences> {
    static readonly type: string = `[${state}] List Job Recurrences Fail`;
  }

  export class ListJobTypes extends BaseAction {
    static readonly type: string = `[${state}] List Job Types`;
  }

  export class ListJobTypesSuccess extends BaseSubActionSuccess<ListJobTypes> {
    static readonly type: string = `[${state}] List Job Types Success`;

    constructor(public parent: ListJobTypes, public listJobTypes: JobType[]) {
      super(parent);
    }
  }

  export class ListJobTypesFail extends BaseSubActionFail<ListJobTypes> {
    static readonly type: string = `[${state}] List Job Types Fail`;
  }
}

export const preservationPlanningJobActionNameSpace: ResourceNameSpace = PreservationPlanningJobAction;
