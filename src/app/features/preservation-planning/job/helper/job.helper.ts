/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - job.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  KeyValue,
  MARK_AS_TRANSLATABLE,
} from "solidify-frontend";

export class JobHelper {
  static getWeekDays(): KeyValue[] {
    return [
      {
        key: "1",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.weekday.monday"),
      },
      {
        key: "2",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.weekday.tuesday"),
      },
      {
        key: "3",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.weekday.wednesday"),
      },
      {
        key: "4",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.weekday.thursday"),
      },
      {
        key: "5",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.weekday.friday"),
      },
      {
        key: "6",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.weekday.saturday"),
      },
      {
        key: "7",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.weekday.sunday"),
      },
    ];
  }

  static getMonths(): KeyValue[] {
    return [
      {
        key: "1",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.january"),
      },
      {
        key: "2",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.february"),
      },
      {
        key: "3",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.march"),
      },
      {
        key: "4",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.april"),
      },
      {
        key: "5",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.may"),
      },
      {
        key: "6",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.june"),
      },
      {
        key: "7",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.july"),
      },
      {
        key: "8",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.august"),
      },
      {
        key: "9",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.september"),
      },
      {
        key: "10",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.october"),
      },
      {
        key: "11",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.november"),
      },
      {
        key: "12",
        value: MARK_AS_TRANSLATABLE("preservation.job.scheduling.month.december"),
      },
    ];
  }
}
