/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {DlcmRoutes} from "@app/shared/models/dlcm-route.model";
import {PreservationPlanningJobCreateRoutable} from "@preservation-planning/job/components/routables/job-create/preservation-planning-job-create.routable";
import {PreservationPlanningJobDetailEditRoutable} from "@preservation-planning/job/components/routables/job-detail-edit/preservation-planning-job-detail-edit.routable";
import {PreservationPlanningJobExecutionDetailRoutable} from "@preservation-planning/job/components/routables/job-execution-detail/preservation-planning-job-execution-detail.routable";
import {PreservationPlanningJobListRoutable} from "@preservation-planning/job/components/routables/job-list/preservation-planning-job-list.routable";
import {PreservationPlanningJobState} from "@preservation-planning/job/stores/preservation-planning-job.state";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
} from "@shared/enums/routes.enum";
import {
  CanDeactivateGuard,
  EmptyContainer,
} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: PreservationPlanningJobListRoutable,
    data: {},
    children: [],
  },
  {
    path: PreservationPlanningRoutesEnum.jobCreate,
    component: PreservationPlanningJobCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: PreservationPlanningRoutesEnum.jobDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: PreservationPlanningJobDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: PreservationPlanningJobState.currentTitle,
    },
    children: [
      {
        path: PreservationPlanningRoutesEnum.execution + AppRoutesEnum.separator + AppRoutesEnum.paramIdExecution,
        component: PreservationPlanningJobExecutionDetailRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.executions,
        },
      },
      {
        path: PreservationPlanningRoutesEnum.jobEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreservationPlanningJobRoutingModule {
}
