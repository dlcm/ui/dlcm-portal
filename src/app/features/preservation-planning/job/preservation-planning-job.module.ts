/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {PreservationPlanningJobExecutionListContainer} from "@preservation-planning/job/components/containers/job-execution-list/preservation-planning-job-execution-list.container";
import {PreservationPlanningJobReportDetailErrorMessageDialog} from "@preservation-planning/job/components/dialogs/job-report-detail-error-message/preservation-planning-job-report-detail-error-message.dialog";
import {PreservationPlanningJobReportDetailDialog} from "@preservation-planning/job/components/dialogs/job-report-detail/preservation-planning-job-report-detail.dialog";
import {PreservationPlanningJobExecutionFormPresentational} from "@preservation-planning/job/components/presentationals/job-execution-form/preservation-planning-job-execution-form.presentational";
import {PreservationPlanningJobFormPresentational} from "@preservation-planning/job/components/presentationals/job-form/preservation-planning-job-form.presentational";
import {PreservationPlanningJobCreateRoutable} from "@preservation-planning/job/components/routables/job-create/preservation-planning-job-create.routable";
import {PreservationPlanningJobDetailEditRoutable} from "@preservation-planning/job/components/routables/job-detail-edit/preservation-planning-job-detail-edit.routable";
import {PreservationPlanningJobExecutionDetailRoutable} from "@preservation-planning/job/components/routables/job-execution-detail/preservation-planning-job-execution-detail.routable";
import {PreservationPlanningJobListRoutable} from "@preservation-planning/job/components/routables/job-list/preservation-planning-job-list.routable";
import {PreservationPlanningJobRoutingModule} from "@preservation-planning/job/preservation-planning-job-routing.module";
import {PreservationPlanningJobExecutionReportState} from "@preservation-planning/job/stores/job-execution/job-execution-report/preservation-planning-job-execution-report.state";
import {PreservationPlanningJobExecutionState} from "@preservation-planning/job/stores/job-execution/preservation-planning-job-execution.state";
import {PreservationPlanningJobState} from "@preservation-planning/job/stores/preservation-planning-job.state";

const routables = [
  PreservationPlanningJobListRoutable,
  PreservationPlanningJobDetailEditRoutable,
  PreservationPlanningJobCreateRoutable,
  PreservationPlanningJobExecutionDetailRoutable,
];
const containers = [
  PreservationPlanningJobExecutionListContainer,
];
const dialogs = [
  PreservationPlanningJobReportDetailDialog,
  PreservationPlanningJobReportDetailErrorMessageDialog,
];
const presentationals = [
  PreservationPlanningJobFormPresentational,
  PreservationPlanningJobExecutionFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    PreservationPlanningJobRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      PreservationPlanningJobState,
      PreservationPlanningJobExecutionState,
      PreservationPlanningJobExecutionReportState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationPlanningJobModule {
}
