/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-report-detail-error-message.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {
  MARK_AS_TRANSLATABLE,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-job-report-detail-error-message-dialog",
  templateUrl: "./preservation-planning-job-report-detail-error-message.dialog.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningJobReportDetailErrorMessageDialog extends SharedAbstractDialog<PreservationPlanningJobReportDetailErrorMessageDialogData> implements OnInit {
  public readonly KEY_TITLE: string = MARK_AS_TRANSLATABLE("preservation.jobExecution.report.detail.errorMessage.title");

  paramMessage: { resId: string } = {resId: StringUtil.stringEmpty};

  constructor(protected readonly _dialogRef: MatDialogRef<PreservationPlanningJobReportDetailErrorMessageDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: PreservationPlanningJobReportDetailErrorMessageDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.paramMessage = {resId: this.data.resId};
  }
}

export interface PreservationPlanningJobReportDetailErrorMessageDialogData {
  errorMessage: string;
  resId: string;
}
