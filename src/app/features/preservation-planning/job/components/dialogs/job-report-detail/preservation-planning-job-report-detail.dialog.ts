/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-report-detail.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from "@angular/material/dialog";
import {PreservationPlanningDepositStatusHistoryState} from "@app/features/preservation-planning/deposit/stores/status-history/preservation-planning-deposit-status-history.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {JobExecutionReportLine} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {PreservationPlanningDepositStatusHistoryAction} from "@preservation-planning/deposit/stores/status-history/preservation-planning-deposit-status-history.action";
import {PreservationPlanningDipAction} from "@preservation-planning/dip/stores/preservation-planning-dip.action";
import {PreservationPlanningDipStatusHistoryAction} from "@preservation-planning/dip/stores/status-history/preservation-planning-dip-status-history.action";
import {PreservationPlanningDipStatusHistoryState} from "@preservation-planning/dip/stores/status-history/preservation-planning-dip-status-history.state";
import {PreservationPlanningJobReportDetailErrorMessageDialog} from "@preservation-planning/job/components/dialogs/job-report-detail-error-message/preservation-planning-job-report-detail-error-message.dialog";
import {PreservationPlanningJobExecutionReportAction} from "@preservation-planning/job/stores/job-execution/job-execution-report/preservation-planning-job-execution-report.action";
import {PreservationPlanningJobExecutionReportState} from "@preservation-planning/job/stores/job-execution/job-execution-report/preservation-planning-job-execution-report.state";
import {PreservationPlanningSipStatusHistoryAction} from "@preservation-planning/sip/stores/status-history/preservation-planning-sip-status-history.action";
import {PreservationPlanningSipStatusHistoryState} from "@preservation-planning/sip/stores/status-history/preservation-planning-sip-status-history.state";
import {PreservationPlanningOrderAction} from "@preservation-planning/stores/order/preservation-planning-order.action";
import {PreservationPlanningOrderStatusHistoryAction} from "@preservation-planning/stores/order/status-history/preservation-planning-order-status-history.action";
import {PreservationPlanningOrderStatusHistoryState} from "@preservation-planning/stores/order/status-history/preservation-planning-order-status-history.state";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedAipStatusHistoryAction} from "@shared/features/aip/stores/status-history/shared-aip-status-history.action";
import {SharedAipStatusHistoryState} from "@shared/features/aip/stores/status-history/shared-aip-status-history.state";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {Observable} from "rxjs";
import {
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  isNotNullNorUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  StatusHistory,
  StatusHistoryDialog,
  StatusHistoryDialogData,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-job-report-detail-dialog",
  templateUrl: "./preservation-planning-job-report-detail.dialog.html",
  styleUrls: ["./preservation-planning-job-report-detail.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningJobReportDetailDialog extends SharedAbstractDialog<PreservationPlanningJobReportDetailDialogData> implements OnInit, OnDestroy {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningJobExecutionReportState);
  listObs: Observable<JobExecutionReportLine[]> = MemoizedUtil.select(this._store, PreservationPlanningJobExecutionReportState, state => state.lineReport);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, PreservationPlanningJobExecutionReportState, state => state.queryParametersLines);

  historyAipObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, SharedAipStatusHistoryState, state => state.history);
  isLoadingHistoryAipObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedAipStatusHistoryState);
  queryParametersHistoryAipObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, SharedAipStatusHistoryState, state => state.queryParameters);

  historySipObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, PreservationPlanningSipStatusHistoryState, state => state.history);
  isLoadingHistorySipObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningSipStatusHistoryState);
  queryParametersHistorySipObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, PreservationPlanningSipStatusHistoryState, state => state.queryParameters);

  historyDepositObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, PreservationPlanningDepositStatusHistoryState, state => state.history);
  isLoadingHistoryDepositObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningDepositStatusHistoryState);
  queryParametersHistoryDepositObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, PreservationPlanningDepositStatusHistoryState, state => state.queryParameters);

  historyOrderObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, PreservationPlanningOrderStatusHistoryState, state => state.history);
  isLoadingHistoryOrderObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningOrderStatusHistoryState);
  queryParametersHistoryOrderObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, PreservationPlanningOrderStatusHistoryState, state => state.queryParameters);

  historyDipObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, PreservationPlanningDipStatusHistoryState, state => state.history);
  isLoadingHistoryDipObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningDipStatusHistoryState);
  queryParametersHistoryDipObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, PreservationPlanningDipStatusHistoryState, state => state.queryParameters);

  paramMessage: { resId: string } = {resId: StringUtil.stringEmpty};

  columns: DataTableColumns<JobExecutionReportLine>[];
  actions: DataTableActions<JobExecutionReportLine>[] = [
    {
      logo: IconNameEnum.error,
      callback: (jobExecutionReportLine: JobExecutionReportLine) => this.showMessageError(jobExecutionReportLine),
      placeholder: current => MARK_AS_TRANSLATABLE("crud.list.action.errorMessage"),
      displayOnCondition: (jobExecutionReportLine: JobExecutionReportLine) => jobExecutionReportLine.status !== Enums.PreservationJob.JobExecutionReportLineStatusEnum.PROCESSED,
    },
    {
      logo: IconNameEnum.history,
      callback: (jobExecutionReportLine: JobExecutionReportLine) => this.historyAip(jobExecutionReportLine),
      placeholder: current => LabelTranslateEnum.showHistoryStatus,
      displayOnCondition: (jobExecutionReportLine: JobExecutionReportLine) => jobExecutionReportLine.status !== Enums.PreservationJob.JobExecutionReportLineStatusEnum.ERROR
        && isNotNullNorUndefined(jobExecutionReportLine.url),
    },
  ];

  constructor(private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialogRef: MatDialogRef<PreservationPlanningJobReportDetailDialog>,
              protected readonly _dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public readonly data: PreservationPlanningJobReportDetailDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.paramMessage = {resId: this.data.reportId};
    this.columns = [
      {
        field: "resId",
        header: LabelTranslateEnum.package,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.descending,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
        translate: true,
        filterEnum: Enums.PreservationJob.JobExecutionReportLineStatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "changeTime",
        header: LabelTranslateEnum.startDate,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
    ];
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._store.dispatch(new PreservationPlanningJobExecutionReportAction.CleanQueryParametersLines());
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new PreservationPlanningJobExecutionReportAction.ChangeQueryParametersLines(queryParameters, this.data.executionId, this.data.preservationJobId, this.data.reportId));
    this._changeDetector.detectChanges();
  }

  showExecutionDetail(jobExecutionReportLine: JobExecutionReportLine): void {
    if (isNotNullNorUndefined(jobExecutionReportLine.url)) {
      if (jobExecutionReportLine.url.indexOf(ApiEnum.ingestSip) !== -1) {
        this._store.dispatch(new PreservationPlanningJobExecutionReportAction.GoToSip(jobExecutionReportLine.resId));
        this.close();
      } else if (jobExecutionReportLine.url.indexOf(ApiEnum.preIngestDeposits) !== -1) {
        this._store.dispatch(new PreservationPlanningJobExecutionReportAction.GoToDeposit(jobExecutionReportLine.resId));
        this.close();
      } else if (jobExecutionReportLine.url.indexOf(ApiEnum.accessOrders) !== -1) {
        this._store.dispatch(new PreservationPlanningOrderAction.GoToOrder(jobExecutionReportLine.resId));
        this.close();
      } else if (jobExecutionReportLine.url.indexOf(ApiEnum.accessDip) !== -1) {
        this._store.dispatch(new PreservationPlanningDipAction.GoToDip(jobExecutionReportLine.resId));
        this.close();
      } else if (jobExecutionReportLine.url.indexOf(ApiEnum.accessAipDownloaded) !== -1) {
        this._store.dispatch(new Navigate([RoutesEnum.preservationPlanningAipDownloadedDetail, jobExecutionReportLine.resId]));
        this.close();
      } else {
        for (const storage of ApiEnum.archivalStorageAipStorages) {
          if (jobExecutionReportLine.url.indexOf(storage.url) !== -1 || jobExecutionReportLine.url === ApiEnum.archivalStorage) {
            this._store.dispatch(new PreservationPlanningJobExecutionReportAction.GoToAip(jobExecutionReportLine.resId));
            this.close();
            break;
          }
        }
      }
    }
  }

  showMessageError(jobExecutionReportLine: JobExecutionReportLine): void {
    DialogUtil.open(this._dialog, PreservationPlanningJobReportDetailErrorMessageDialog, {
      errorMessage: jobExecutionReportLine.errorMessage,
      resId: jobExecutionReportLine.resId,
    });
  }

  historyAip(jobExecutionReportLine: JobExecutionReportLine): void {
    //depending on the reportType, it will show the related aip or sip
    let dataForDialog: StatusHistoryDialogData;
    if (this.data.jobType === Enums.PreservationJob.JobTypeEnum.PURGE_SUBMISSION_TEMP_FILES || this.data.jobType === Enums.PreservationJob.JobTypeEnum.CLEAN_SUBMISSION) {
      dataForDialog = {
        parentId: null,
        resourceResId: jobExecutionReportLine.resId,
        name: StateEnum.preservationPlanning_sip,
        statusHistory: this.historySipObs,
        isLoading: this.isLoadingHistorySipObs,
        queryParametersObs: this.queryParametersHistorySipObs,
        state: PreservationPlanningSipStatusHistoryAction,
        statusEnums: Enums.Package.StatusEnumTranslate,
      };
    } else if (this.data.jobType === Enums.PreservationJob.JobTypeEnum.CHECK_COMPLIANCE_LEVEL) {
      dataForDialog = {
        parentId: null,
        resourceResId: jobExecutionReportLine.resId,
        name: StateEnum.preservationPlanning_deposit,
        statusHistory: this.historyDepositObs,
        isLoading: this.isLoadingHistoryDepositObs,
        queryParametersObs: this.queryParametersHistoryDepositObs,
        state: PreservationPlanningDepositStatusHistoryAction,
        statusEnums: Enums.Package.StatusEnumTranslate,
      };
    } else if (this.data.jobType === Enums.PreservationJob.JobTypeEnum.PURGE_ORDER) {
      dataForDialog = this._getDataHistoryForPurgeOrder(jobExecutionReportLine);
    } else {
      dataForDialog = {
        parentId: null,
        resourceResId: jobExecutionReportLine.resId,
        name: StateEnum.shared_aip,
        statusHistory: this.historyAipObs,
        isLoading: this.isLoadingHistoryAipObs,
        queryParametersObs: this.queryParametersHistoryAipObs,
        state: SharedAipStatusHistoryAction,
        statusEnums: Enums.Package.StatusEnumTranslate,
      };
    }
    DialogUtil.open(this._dialog, StatusHistoryDialog, dataForDialog, {
      width: environment.modalWidth,
    });
  }

  private _getDataHistoryForPurgeOrder(jobExecutionReportLine: JobExecutionReportLine): StatusHistoryDialogData {
    let dataForDialog: StatusHistoryDialogData;
    if (jobExecutionReportLine.url.indexOf(ApiEnum.accessOrders) !== -1) {
      dataForDialog = {
        parentId: null,
        resourceResId: jobExecutionReportLine.resId,
        name: StateEnum.preservationPlanning_order,
        statusHistory: this.historyOrderObs,
        isLoading: this.isLoadingHistoryOrderObs,
        queryParametersObs: this.queryParametersHistoryOrderObs,
        state: PreservationPlanningOrderStatusHistoryAction,
        statusEnums: Enums.Package.StatusEnumTranslate,
      };
    } else if (jobExecutionReportLine.url.indexOf(ApiEnum.accessAipDownloaded) !== -1) {
      dataForDialog = {
        parentId: null,
        resourceResId: jobExecutionReportLine.resId,
        name: StateEnum.shared_aip,
        statusHistory: this.historyAipObs,
        isLoading: this.isLoadingHistoryAipObs,
        queryParametersObs: this.queryParametersHistoryAipObs,
        state: SharedAipStatusHistoryAction,
        statusEnums: Enums.Package.StatusEnumTranslate,
      };
    } else if (jobExecutionReportLine.url.indexOf(ApiEnum.accessDip) !== -1) {
      dataForDialog = {
        parentId: null,
        resourceResId: jobExecutionReportLine.resId,
        name: StateEnum.preservationPlanning_dip,
        statusHistory: this.historyAipObs,
        isLoading: this.isLoadingHistoryDipObs,
        queryParametersObs: this.queryParametersHistoryDipObs,
        state: PreservationPlanningDipStatusHistoryAction,
        statusEnums: Enums.Package.StatusEnumTranslate,
      };
    }
    return dataForDialog;
  }
}

export interface PreservationPlanningJobReportDetailDialogData {
  linesReport: JobExecutionReportLine[];
  preservationJobId: string;
  executionId: string;
  reportId: string;
  jobType: Enums.PreservationJob.JobTypeEnum;
}
