/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-execution-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {JobExecution} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  AbstractFormPresentational,
  DateUtil,
  EnumUtil,
  KeyValue,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-job-execution-form",
  templateUrl: "./preservation-planning-job-execution-form.presentational.html",
  styleUrls: ["./preservation-planning-job-execution-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningJobExecutionFormPresentational extends AbstractFormPresentational<JobExecution> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  listExecutionStatusToTranslate: KeyValue[] = Enums.PreservationJob.StatusEnumTranslate;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _bindFormTo(jobExecution: JobExecution): void {
    this.form = this._fb.group({
      [this.formDefinition.status]: [jobExecution.status, [Validators.required, SolidifyValidator]],
      [this.formDefinition.statusMessage]: [jobExecution.statusMessage, [SolidifyValidator]],
      [this.formDefinition.startDate]: [DateUtil.convertDateToDateTimeString(jobExecution.startDate as any), [Validators.required, SolidifyValidator]],
      [this.formDefinition.endDate]: [DateUtil.convertDateToDateTimeString(jobExecution.endDate as any), [Validators.required, SolidifyValidator]],
      [this.formDefinition.totalItems]: [jobExecution.ignoredItems + jobExecution.processedItems + jobExecution.inErrorItems, [Validators.required, SolidifyValidator]],
      [this.formDefinition.ignoredItems]: [jobExecution.ignoredItems, [Validators.required, SolidifyValidator]],
      [this.formDefinition.processedItems]: [jobExecution.processedItems, [Validators.required, SolidifyValidator]],
      [this.formDefinition.inErrorItems]: [jobExecution.inErrorItems, [Validators.required, SolidifyValidator]],
    });
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.status]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.statusMessage]: ["", [SolidifyValidator]],
      [this.formDefinition.startDate]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.endDate]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.totalItems]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.ignoredItems]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.processedItems]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.inErrorItems]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(model: JobExecution): JobExecution {
    return undefined;
  }

}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() status: string;
  @PropertyName() statusMessage: string;
  @PropertyName() startDate: string;
  @PropertyName() endDate: string;
  @PropertyName() totalItems: string;
  @PropertyName() ignoredItems: string;
  @PropertyName() processedItems: string;
  @PropertyName() inErrorItems: string;
}
