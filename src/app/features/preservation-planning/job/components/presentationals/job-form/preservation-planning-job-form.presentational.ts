/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {JobHelper} from "@app/features/preservation-planning/job/helper/job.helper";
import {Enums} from "@enums";
import {
  JobScheduling,
  PreservationJob,
} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {tap} from "rxjs/operators";
import {
  AbstractFormPresentational,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNumberReal,
  KeyValue,
  ObjectUtil,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-job-form",
  templateUrl: "./preservation-planning-job-form.presentational.html",
  styleUrls: ["./preservation-planning-job-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningJobFormPresentational extends AbstractFormPresentational<PreservationJob> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  listJobType: KeyValue[] = Enums.PreservationJob.JobTypeEnumTranslate;
  listJobRecurrence: KeyValue[] = Enums.PreservationJob.JobRecurrenceEnumTranslate;

  listWeekDay: KeyValue[] = JobHelper.getWeekDays();
  listMonth: KeyValue[] = JobHelper.getMonths();

  editValue: boolean = false;

  @ViewChild("hourFormatEditable") hourFormatEditableRef: ElementRef;

  get jobRecurrenceEnum(): typeof Enums.PreservationJob.JobRecurrenceEnum {
    return Enums.PreservationJob.JobRecurrenceEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._computeRecurenceValidator();
    this.subscribe(this.form.get(this.formDefinition.jobRecurrence).valueChanges.pipe(
      tap(() => {
        this._computeRecurenceValidator();
        this._changeDetectorRef.detectChanges();
      }),
    ));
  }

  private readonly _DEFAULT_HOUR: number = 16;
  private readonly _DEFAULT_WEEK_DAY: string = "1";
  private readonly _DEFAULT_MONTH_DAY: number = 1;
  private readonly _DEFAULT_MONTH: string = "1";

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.enable]: [true, [SolidifyValidator]],
      [this.formDefinition.jobRecurrence]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.jobType]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.hour]: [this._DEFAULT_HOUR, [Validators.min(0), Validators.max(23), SolidifyValidator]],
      [this.formDefinition.weekDay]: [this._DEFAULT_WEEK_DAY, [SolidifyValidator]],
      [this.formDefinition.monthDay]: [this._DEFAULT_MONTH_DAY, [Validators.min(1), Validators.max(31), SolidifyValidator]],
      [this.formDefinition.month]: [this._DEFAULT_MONTH, [SolidifyValidator]],
      [this.formDefinition.maxItems]: ["0", [SolidifyValidator]],
      [this.formDefinition.parameters]: ["", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(preservationJob: PreservationJob): void {
    // Work around since this property has a validator with minimal value 1 but default value 0
    const scheduling: JobScheduling = ObjectUtil.clone(preservationJob.scheduling);
    if (scheduling.monthDay === 0) {
      scheduling.monthDay = undefined;
    }
    this.form = this._fb.group({
      [this.formDefinition.name]: [preservationJob.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.enable]: [preservationJob.enable, [SolidifyValidator]],
      [this.formDefinition.jobRecurrence]: [preservationJob.jobRecurrence, [Validators.required, SolidifyValidator]],
      [this.formDefinition.jobType]: [preservationJob.jobType, [Validators.required, SolidifyValidator]],
      [this.formDefinition.hour]: [this._getHours(preservationJob) ?? this._DEFAULT_HOUR, [Validators.min(0), Validators.max(23), SolidifyValidator]],
      [this.formDefinition.weekDay]: [this._getWeekDay(preservationJob) ?? this._DEFAULT_WEEK_DAY, [SolidifyValidator]],
      [this.formDefinition.monthDay]: [scheduling.monthDay ?? this._DEFAULT_MONTH_DAY, [Validators.min(1), Validators.max(31), SolidifyValidator]],
      [this.formDefinition.month]: [this._getMonth(preservationJob) ?? this._DEFAULT_MONTH, [SolidifyValidator]],
      [this.formDefinition.maxItems]: [preservationJob.maxItems, [SolidifyValidator]],
      [this.formDefinition.parameters]: [preservationJob.parameters, [SolidifyValidator]],
    });
  }

  private _computeRecurenceValidator(): void {
    const fcHour = this.form.get(this.formDefinition.hour);
    const fcWeekDay = this.form.get(this.formDefinition.weekDay);
    const fcMonthDay = this.form.get(this.formDefinition.monthDay);
    const fcMonth = this.form.get(this.formDefinition.month);

    fcHour.removeValidators(Validators.required);
    fcWeekDay.removeValidators(Validators.required);
    fcMonthDay.removeValidators(Validators.required);
    fcMonth.removeValidators(Validators.required);

    const jobRecurrence: Enums.PreservationJob.JobRecurrenceEnum = this.form.get(this.formDefinition.jobRecurrence).value;
    switch (jobRecurrence) {
      case Enums.PreservationJob.JobRecurrenceEnum.ONCE:
        break;
      case Enums.PreservationJob.JobRecurrenceEnum.DAILY:
        fcHour.addValidators(Validators.required);
        break;
      case Enums.PreservationJob.JobRecurrenceEnum.WEEKLY:
        fcHour.addValidators(Validators.required);
        fcWeekDay.addValidators(Validators.required);
        break;
      case Enums.PreservationJob.JobRecurrenceEnum.MONTHLY:
        fcHour.addValidators(Validators.required);
        fcMonthDay.addValidators(Validators.required);
        break;
      case Enums.PreservationJob.JobRecurrenceEnum.YEARLY:
        fcHour.addValidators(Validators.required);
        fcMonthDay.addValidators(Validators.required);
        fcMonth.addValidators(Validators.required);
        break;
    }
  }

  protected _treatmentBeforeSubmit(preservationJob: PreservationJob): PreservationJob {
    delete preservationJob[this.formDefinition.hour];
    delete preservationJob[this.formDefinition.month];
    delete preservationJob[this.formDefinition.monthDay];
    delete preservationJob[this.formDefinition.weekDay];

    // Convert all in string
    const hourString: string = this.form.get(this.formDefinition.hour).value ? this.form.get(this.formDefinition.hour).value + "" : null;
    const monthString: string = this.form.get(this.formDefinition.month).value ? this.form.get(this.formDefinition.month).value + "" : null;
    const monthDayString: string = this.form.get(this.formDefinition.monthDay).value ? this.form.get(this.formDefinition.monthDay).value + "" : null;
    const weekDayString: string = this.form.get(this.formDefinition.weekDay).value ? this.form.get(this.formDefinition.weekDay).value + "" : null;

    // Cast into number
    const hour = isNotNullNorUndefinedNorWhiteString(hourString) && isNumberReal(+hourString) ? +hourString : null;
    const month = isNotNullNorUndefinedNorWhiteString(monthString) && isNumberReal(+monthString) ? +monthString : null;
    const monthDay = isNotNullNorUndefinedNorWhiteString(monthDayString) && isNumberReal(+monthDayString) ? +monthDayString : null;
    const weekDay = isNotNullNorUndefinedNorWhiteString(weekDayString) && isNumberReal(+weekDayString) ? +weekDayString : null;

    const scheduling: JobScheduling = {};

    switch (preservationJob.jobRecurrence) {
      case Enums.PreservationJob.JobRecurrenceEnum.ONCE:
        break;
      case Enums.PreservationJob.JobRecurrenceEnum.DAILY:
        scheduling.hour = hour;
        break;
      case Enums.PreservationJob.JobRecurrenceEnum.WEEKLY:
        scheduling.hour = hour;
        scheduling.weekDay = weekDay;
        break;
      case Enums.PreservationJob.JobRecurrenceEnum.MONTHLY:
        scheduling.hour = hour;
        scheduling.monthDay = monthDay;
        break;
      case Enums.PreservationJob.JobRecurrenceEnum.YEARLY:
        scheduling.hour = hour;
        scheduling.monthDay = monthDay;
        scheduling.month = month;
        break;
    }

    preservationJob.scheduling = scheduling;
    return preservationJob;
  }

  focusFieldEditable(): void {
    this.editValue = true;
    setTimeout(() => {
      this.hourFormatEditableRef.nativeElement.focus();
    }, 0);
  }

  checkHour(): void {
    this.editValue = false;
    const formControl = this.form.get(this.formDefinition.hour);
    if (formControl.invalid) {
      formControl.setValue("");
    }
  }

  private _getHours(preservationJob: PreservationJob): number | undefined {
    return isNullOrUndefined(preservationJob.scheduling) ? undefined : preservationJob.scheduling.hour;
  }

  private _getMonth(preservationJob: PreservationJob): string {
    if (isNullOrUndefined(preservationJob.scheduling) || isNullOrUndefined(preservationJob.scheduling.month)) {
      return undefined;
    }
    const month = JobHelper.getMonths().find(item => item.key === "" + preservationJob.scheduling.month);
    return month === undefined ? null : month.key;
  }

  private _getWeekDay(preservationJob: PreservationJob): string {
    if (isNullOrUndefined(preservationJob.scheduling) || isNullOrUndefined(preservationJob.scheduling.weekDay)) {
      return undefined;
    }
    const weekDay = JobHelper.getWeekDays().find(item => item.key === "" + preservationJob.scheduling.weekDay);
    return weekDay === undefined ? null : weekDay.key;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() enable: string;
  @PropertyName() jobRecurrence: string;
  @PropertyName() jobType: string;
  @PropertyName() maxItems: string;
  @PropertyName() parameters: string;
  @PropertyName() hour: string;
  @PropertyName() weekDay: string;
  @PropertyName() monthDay: string;
  @PropertyName() month: string;
}
