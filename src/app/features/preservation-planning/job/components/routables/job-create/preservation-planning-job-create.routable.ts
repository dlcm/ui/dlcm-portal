/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  ViewChild,
} from "@angular/core";
import {PreservationJob} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {preservationPlanningJobActionNameSpace} from "@preservation-planning/job/stores/preservation-planning-job.action";
import {
  PreservationPlanningJobState,
  PreservationPlanningJobStateModel,
} from "@preservation-planning/job/stores/preservation-planning-job.state";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  AbstractFormPresentational,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-job-create-routable",
  templateUrl: "./preservation-planning-job-create.routable.html",
  styleUrls: ["./preservation-planning-job-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningJobCreateRoutable extends AbstractCreateRoutable<PreservationJob, PreservationPlanningJobStateModel> {
  @Select(PreservationPlanningJobState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(PreservationPlanningJobState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;

  @ViewChild("formPresentational")
  readonly formPresentational: AbstractFormPresentational<PreservationJob>;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.preservationPlanning_job, _injector, preservationPlanningJobActionNameSpace, StateEnum.preservationPlanning);
  }
}
