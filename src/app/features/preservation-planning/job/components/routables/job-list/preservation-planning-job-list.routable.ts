/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {PreservationJob} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  PreservationPlanningJobAction,
  preservationPlanningJobActionNameSpace,
} from "@preservation-planning/job/stores/preservation-planning-job.action";
import {PreservationPlanningJobStateModel} from "@preservation-planning/job/stores/preservation-planning-job.state";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  AbstractListRoutable,
  DataTableActions,
  DataTableFieldTypeEnum,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-job-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./preservation-planning-job-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningJobListRoutable extends AbstractListRoutable<PreservationJob, PreservationPlanningJobStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToPreservationPlanning;
  readonly KEY_PARAM_NAME: keyof PreservationJob & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.preservationPlanning_job, preservationPlanningJobActionNameSpace, _injector, {
      listExtraButtons: [
        {
          color: "primary",
          icon: IconNameEnum.init,
          labelToTranslate: (current) => MARK_AS_TRANSLATABLE("preservation.job.button.init"),
          callback: () => this.init(),
          order: 40,
        },
      ],
    }, StateEnum.preservationPlanning);
  }

  conditionDisplayEditButton(model: PreservationJob | undefined): boolean {
    return true;
  }

  conditionDisplayDeleteButton(model: PreservationJob | undefined): boolean {
    return true;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.defineColumns();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "jobRecurrence" as any,
        header: LabelTranslateEnum.recurrence,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.PreservationJob.JobRecurrenceEnumTranslate,
      },
      {
        field: "jobType",
        header: LabelTranslateEnum.type,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.PreservationJob.JobTypeEnumTranslate,
      },
      {
        field: "enable" as any,
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  protected override _defineActions(): DataTableActions<PreservationJob>[] {
    return [
      ...super._defineActions(),
      {
        logo: IconNameEnum.run,
        callback: (model: PreservationJob) => this._start(model),
        placeholder: current => LabelTranslateEnum.run,
        displayOnCondition: (model: PreservationJob) => isNullOrUndefined(model) || (model.jobRecurrence !== Enums.PreservationJob.JobRecurrenceEnum.ONCE),
      },
    ];
  }

  back(): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationPlanning]));
  }

  init(): void {
    this._store.dispatch(new PreservationPlanningJobAction.Init());
  }

  private _start(job: PreservationJob): void {
    this._store.dispatch(new PreservationPlanningJobAction.VerifyBeforeStart(job.resId));
  }
}
