/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {Enums} from "@enums";
import {
  JobExecution,
  PreservationJob,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {PreservationPlanningJobExecutionListContainer} from "@preservation-planning/job/components/containers/job-execution-list/preservation-planning-job-execution-list.container";
import {PreservationPlanningJobExecutionState} from "@preservation-planning/job/stores/job-execution/preservation-planning-job-execution.state";
import {preservationPlanningJobActionNameSpace} from "@preservation-planning/job/stores/preservation-planning-job.action";
import {
  PreservationPlanningJobState,
  PreservationPlanningJobStateModel,
} from "@preservation-planning/job/stores/preservation-planning-job.state";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {PreservationPlanningRoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  ExtraButtonToolbar,
  isFalse,
  isNullOrUndefined,
  MemoizedUtil,
  ScrollService,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-job-detail-edit-routable",
  templateUrl: "./preservation-planning-job-detail-edit.routable.html",
  styleUrls: ["./preservation-planning-job-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningJobDetailEditRoutable extends AbstractDetailEditCommonRoutable<PreservationJob, PreservationPlanningJobStateModel> implements OnInit {
  @Select(PreservationPlanningJobState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(PreservationPlanningJobState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  listObs: Observable<JobExecution[]> = MemoizedUtil.list(this._store, PreservationPlanningJobExecutionState);

  @ViewChild("jobExecutionList")
  readonly jobExecutionList: PreservationPlanningJobExecutionListContainer;

  readonly KEY_PARAM_NAME: keyof PreservationJob & string = "name";
  preservationJob: boolean | undefined = undefined;

  listExtraButtons: ExtraButtonToolbar<PreservationJob>[] = [
    {
      color: "primary",
      icon: IconNameEnum.run,
      labelToTranslate: (current) => LabelTranslateEnum.run,
      callback: () => this.jobExecutionList.start(),
      disableCondition: resource => isFalse(resource.enable),
      displayCondition: (resource) => !this.isEdit && resource && (resource.jobRecurrence !== Enums.PreservationJob.JobRecurrenceEnum.ONCE || MemoizedUtil.select(this._store, PreservationPlanningJobExecutionState, state => !isNullOrUndefined(state.list) && state.list.length === 0)),
      order: 41,
    },
    {
      color: "primary",
      icon: IconNameEnum.resume,
      labelToTranslate: (current) => LabelTranslateEnum.resume,
      callback: () => this.jobExecutionList.resume(),
      displayCondition: (resource) => !this.isEdit && resource && (resource.lastExecutionStatus === Enums.PreservationJob.StatusEnum.IN_ERROR),
      order: 41,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _router: Router,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _scrollService: ScrollService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.preservationPlanning_job, _injector, preservationPlanningJobActionNameSpace, StateEnum.preservationPlanning);
  }

  _getSubResourceWithParentId(id: string): void {
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this.urlStateObs
      .pipe(
        tap(urlState => {
          if (urlState) {
            const url = urlState.url;
            this.preservationJob = !url.includes(PreservationPlanningRoutesEnum.execution);
            this._changeDetector.detectChanges();
          }
        }),
      ));
  }
}
