/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-execution-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {
  JobExecution,
  JobExecutionReport,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {
  PreservationPlanningJobReportDetailDialog,
  PreservationPlanningJobReportDetailDialogData,
} from "@preservation-planning/job/components/dialogs/job-report-detail/preservation-planning-job-report-detail.dialog";
import {PreservationPlanningJobExecutionReportAction} from "@preservation-planning/job/stores/job-execution/job-execution-report/preservation-planning-job-execution-report.action";
import {PreservationPlanningJobExecutionReportState} from "@preservation-planning/job/stores/job-execution/job-execution-report/preservation-planning-job-execution-report.state";
import {PreservationPlanningJobExecutionAction} from "@preservation-planning/job/stores/job-execution/preservation-planning-job-execution.action";
import {PreservationPlanningJobExecutionState} from "@preservation-planning/job/stores/job-execution/preservation-planning-job-execution.state";
import {PreservationPlanningJobState} from "@preservation-planning/job/stores/preservation-planning-job.state";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {Observable} from "rxjs";
import {
  BaseResource,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  ExtraButtonToolbar,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  OrderEnum,
  PollingHelper,
  QueryParameters,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-job-execution-detail-routable",
  templateUrl: "./preservation-planning-job-execution-detail.routable.html",
  styleUrls: ["./preservation-planning-job-execution-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningJobExecutionDetailRoutable extends SharedAbstractRoutable implements OnInit, OnDestroy {
  currentObs: Observable<JobExecution> = MemoizedUtil.current(this._store, PreservationPlanningJobExecutionState);
  @Select(PreservationPlanningJobExecutionState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningJobExecutionState);
  isLoadingReportObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningJobExecutionReportState);
  listReportObs: Observable<JobExecutionReport[]> = MemoizedUtil.select(this._store, PreservationPlanningJobExecutionReportState, state => state.reports);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, PreservationPlanningJobExecutionReportState, state => state.queryParameters);

  readonly editAvailable: boolean = false;
  readonly deleteAvailable: boolean = false;

  columns: DataTableColumns<BaseResource & JobExecutionReport>[] = [
    {
      field: "executionNumber",
      header: MARK_AS_TRANSLATABLE("preservation.jobExecution.report.table.header.executionNumber"),
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.descending,
      isFilterable: false,
      isSortable: true,
    },
    {
      field: "processedItems",
      header: MARK_AS_TRANSLATABLE("preservation.jobExecution.report.table.header.processedItems"),
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      isFilterable: false,
      isSortable: false,
    },
    {
      field: "ignoredItems",
      header: MARK_AS_TRANSLATABLE("preservation.jobExecution.report.table.header.ignoredItems"),
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      isFilterable: false,
      isSortable: false,
    },
    {
      field: "inErrorItems",
      header: MARK_AS_TRANSLATABLE("preservation.jobExecution.report.table.header.inErrorItems"),
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      isFilterable: false,
      isSortable: false,
    },
  ];

  private _preservationJobId: string;
  private _resId: string;

  constructor(private readonly _store: Store,
              private readonly _route: ActivatedRoute,
              protected readonly _changeDetector: ChangeDetectorRef,
              private readonly _actions$: Actions,
              private readonly _dialog: MatDialog) {
    super();
    this._retrieveCurrentModelWithUrl();
  }

  protected _retrieveCurrentModelWithUrl(): void {
    this._retrieveResIdFromUrl();
    this._store.dispatch(new PreservationPlanningJobExecutionAction.GetById(this._preservationJobId, this._resId));
  }

  protected _retrieveResIdFromUrl(): void {
    this._resId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdExecutionWithoutPrefixParam);
    this._preservationJobId = this._route.snapshot.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._store.dispatch(new PreservationPlanningJobExecutionReportAction.CleanQueryParameters());
  }

  backToListNavigate(): Navigate {
    return new Navigate([`${RoutesEnum.preservationPlanning}/${PreservationPlanningRoutesEnum.job}/${PreservationPlanningRoutesEnum.jobDetail}/${this._preservationJobId}`]);
  }

  listExtraButtons: ExtraButtonToolbar<JobExecution>[] = [
    {
      color: "primary",
      icon: IconNameEnum.resume,
      labelToTranslate: (current) => LabelTranslateEnum.resume,
      callback: () => this._resume(),
      displayCondition: () => !isNullOrUndefined(MemoizedUtil.currentSnapshot(this._store, PreservationPlanningJobExecutionState)) && MemoizedUtil.currentSnapshot(this._store, PreservationPlanningJobExecutionState).status === Enums.PreservationJob.StatusEnum.IN_ERROR,
      order: 40,
    },
  ];

  private _resume(): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new PreservationPlanningJobExecutionAction.Resume(this._preservationJobId, this._resId),
      PreservationPlanningJobExecutionAction.ResumeSuccess,
      resultAction => {
        this.refresh();
      }));

    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: 2,
      incrementInterval: true,
      resetIntervalWhenUserMouseEvent: true,
      continueUntil: () => isNullOrUndefined(MemoizedUtil.currentSnapshot(this._store, PreservationPlanningJobExecutionState)) || (MemoizedUtil.currentSnapshot(this._store, PreservationPlanningJobExecutionState).status !== Enums.PreservationJob.StatusEnum.IN_ERROR &&
        MemoizedUtil.currentSnapshot(this._store, PreservationPlanningJobExecutionState).status !== Enums.PreservationJob.StatusEnum.COMPLETED),
      actionToDo: () => this.refresh(),
    }));
  }

  showReportDetail(report: JobExecutionReport): void {
    const jobType = MemoizedUtil.currentSnapshot(this._store, PreservationPlanningJobState).jobType;

    const action = new PreservationPlanningJobExecutionReportAction.ReportLines(this._resId, this._preservationJobId, report.resId, null);
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, action,
      PreservationPlanningJobExecutionReportAction.ReportLinesSuccess,
      result => {
        const reportLines = result.list._data;
        DialogUtil.open(this._dialog, PreservationPlanningJobReportDetailDialog, {
          linesReport: reportLines,
          preservationJobId: this._preservationJobId,
          executionId: this._resId,
          reportId: report.resId,
          jobType: jobType,
        } as PreservationPlanningJobReportDetailDialogData);
      }));
  }

  refresh(): void {
    this._store.dispatch(new PreservationPlanningJobExecutionAction.GetById(this._preservationJobId, this._resId));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new PreservationPlanningJobExecutionReportAction.ChangeQueryParameters(queryParameters, this._resId, this._preservationJobId));
    this._changeDetector.detectChanges();
  }
}
