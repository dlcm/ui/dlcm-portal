/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-job-execution-list.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Enums} from "@enums";
import {
  JobExecution,
  PreservationJob,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {PreservationPlanningJobExecutionAction} from "@preservation-planning/job/stores/job-execution/preservation-planning-job-execution.action";
import {PreservationPlanningJobExecutionState} from "@preservation-planning/job/stores/job-execution/preservation-planning-job-execution.state";
import {PreservationPlanningJobAction} from "@preservation-planning/job/stores/preservation-planning-job.action";
import {PreservationPlanningJobState} from "@preservation-planning/job/stores/preservation-planning-job.state";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  DataTableColumns,
  DataTableFieldTypeEnum,
  isNullOrUndefined,
  isTrue,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  OrderEnum,
  PollingHelper,
  QueryParameters,
  StoreUtil,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-job-execution-container",
  templateUrl: "./preservation-planning-job-execution-list.container.html",
  styleUrls: ["./preservation-planning-job-execution-list.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningJobExecutionListContainer extends SharedAbstractPresentational implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningJobExecutionState);
  listObs: Observable<JobExecution[]> = MemoizedUtil.list(this._store, PreservationPlanningJobExecutionState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, PreservationPlanningJobExecutionState);
  jobObs: Observable<PreservationJob> = MemoizedUtil.current(this._store, PreservationPlanningJobState);
  columns: DataTableColumns<JobExecution>[];
  paramMessage: { name: string } = {name: StringUtil.stringEmpty};
  private readonly _INTERVAL_REFRESH_IN_SECOND: number = 0.2;
  private readonly _MAX_INTERVAL_REFRESH_IN_SECOND: number = 2;

  pollingToUpdateProgressBar: boolean = false;
  private _resId: string;

  constructor(private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _route: ActivatedRoute,
              private readonly _actions$: Actions,
              private readonly _dialog: MatDialog) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.defineColumns();
    this._retrieveCurrentModelWithUrl();

    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: this._INTERVAL_REFRESH_IN_SECOND,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: this._MAX_INTERVAL_REFRESH_IN_SECOND,
      resetIntervalWhenUserMouseEvent: true,
      filter: () => isTrue(this.pollingToUpdateProgressBar),
      actionToDo: () => this.getAll(),
    }));

    this.subscribe(this.jobObs.pipe(filter(job => !isNullOrUndefined(job))), job => this.paramMessage = {name: job.name});
    this.subscribe(this.listObs.pipe(
      distinctUntilChanged(),
      filter(list => !isNullOrUndefined(list))), list => {
      this.pollingToUpdateProgressBar = list.findIndex(jobExec => jobExec.status === Enums.PreservationJob.StatusEnum.IN_PROGRESS || jobExec.status === Enums.PreservationJob.StatusEnum.READY) !== -1;
    });
    this._changeDetector.detectChanges();
  }

  protected _retrieveResIdFromUrl(): void {
    this._resId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    if (isNullOrUndefined(this._resId)) {
      this._resId = this._route.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    }
  }

  protected _retrieveCurrentModelWithUrl(): void {
    this._retrieveResIdFromUrl();
    this._getById(this._resId);
  }

  protected _getById(id: string): void {
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "runNumber",
        header: LabelTranslateEnum.runNumber,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.descending,
        isFilterable: false,
        isSortable: true,
        width: "80px",
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
        translate: true,
        filterEnum: Enums.PreservationJob.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        width: "160px",
        alignment: "center",
      },
      {
        field: "completionStatus",
        header: LabelTranslateEnum.completionStatus,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
        component: DataTableComponentHelper.get(DataTableComponentEnum.jobExecutionProgression),
      },
      {
        field: "startDate",
        header: LabelTranslateEnum.startDate,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "endDate",
        header: LabelTranslateEnum.endDate,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
    ];
  }

  getAll(queryParameters?: QueryParameters): void {
    this._store.dispatch(new PreservationPlanningJobExecutionAction.GetAll(this._resId, queryParameters, true));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this.getAll(queryParameters);
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  start(): void {
    const action = new PreservationPlanningJobAction.VerifyBeforeStart(this._resId);
    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(PreservationPlanningJobAction.StartSuccess),
      take(1),
      tap(resultAction => {
        if (resultAction.result.successful) {
          this.getAll();
          this._changeDetector.detectChanges();
        }
      }),
    ));
    this._store.dispatch(action);
  }

  resume(): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new PreservationPlanningJobAction.Resume(this._resId),
      PreservationPlanningJobAction.ResumeSuccess,
      resultAction => {
        this.getAll();
        this._changeDetector.detectChanges();
      }));
  }

  showExecutionDetail(jobExecution: JobExecution): void {
    const pathExecution = RoutesEnum.preservationPlanning + AppRoutesEnum.separator + PreservationPlanningRoutesEnum.job + AppRoutesEnum.separator + PreservationPlanningRoutesEnum.jobDetail + AppRoutesEnum.separator + this._resId + AppRoutesEnum.separator + PreservationPlanningRoutesEnum.execution;
    const path = [pathExecution, jobExecution.resId];
    this._store.dispatch(new Navigate(path));
  }
}
