/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {DlcmRoutes} from "@app/shared/models/dlcm-route.model";
import {PreservationPlanningSipCollectionDetailRoutable} from "@preservation-planning/sip/components/routables/sip-collection-detail/preservation-planning-sip-collection-detail.routable";
import {PreservationPlanningSipCollectionRoutable} from "@preservation-planning/sip/components/routables/sip-collection/preservation-planning-sip-collection.routable";
import {PreservationPlanningSipDetailEditRoutable} from "@preservation-planning/sip/components/routables/sip-detail-edit/preservation-planning-sip-detail-edit.routable";
import {PreservationPlanningSipFileDetailRoutable} from "@preservation-planning/sip/components/routables/sip-file-detail/preservation-planning-sip-file-detail.routable";
import {PreservationPlanningSipFileRoutable} from "@preservation-planning/sip/components/routables/sip-file/preservation-planning-sip-file.routable";
import {PreservationPlanningSipListRoutable} from "@preservation-planning/sip/components/routables/sip-list/preservation-planning-sip-list.routable";
import {PreservationPlanningSipMetadataRoutable} from "@preservation-planning/sip/components/routables/sip-metadata/preservation-planning-sip-metadata.routable";
import {PreservationPlanningSipState} from "@preservation-planning/sip/stores/preservation-planning-sip.state";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: PreservationPlanningSipListRoutable,
    data: {},
  },
  {
    path: PreservationPlanningRoutesEnum.sipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    redirectTo: PreservationPlanningRoutesEnum.sipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId + AppRoutesEnum.separator + PreservationPlanningRoutesEnum.sipMetadata,
    pathMatch: "full",
  },
  {
    path: PreservationPlanningRoutesEnum.sipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: PreservationPlanningSipDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: PreservationPlanningSipState.currentTitle,
    },
    children: [
      {
        path: PreservationPlanningRoutesEnum.sipMetadata,
        component: PreservationPlanningSipMetadataRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.metadata,
          noBreadcrumbLink: true,
        },
      },
      {
        path: PreservationPlanningRoutesEnum.sipFiles,
        component: PreservationPlanningSipFileRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.files,
          noBreadcrumbLink: true,
        },
        children: [
          {
            path: AppRoutesEnum.paramId,
            component: PreservationPlanningSipFileDetailRoutable,
            data: {
              breadcrumb: LabelTranslateEnum.detail,
            },
          },
        ],
      },
      {
        path: PreservationPlanningRoutesEnum.sipCollections,
        component: PreservationPlanningSipCollectionRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.collection,
          noBreadcrumbLink: true,
        },
        children: [
          {
            path: AppRoutesEnum.paramId,
            component: PreservationPlanningSipCollectionDetailRoutable,
            data: {
              breadcrumb: LabelTranslateEnum.detail,
            },
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreservationPlanningSipRoutingModule {
}
