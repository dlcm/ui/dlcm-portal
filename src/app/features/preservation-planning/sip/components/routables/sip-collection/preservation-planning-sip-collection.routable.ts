/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip-collection.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Aip,
  Sip,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {PreservationPlanningSipFormPresentational} from "@preservation-planning/sip/components/presentationals/sip-form/preservation-planning-sip-form.presentational";
import {PreservationPlanningSipCollectionAction} from "@preservation-planning/sip/stores/collection/preservation-planning-sip-collection.action";
import {PreservationPlanningSipCollectionState} from "@preservation-planning/sip/stores/collection/preservation-planning-sip-collection.state";
import {preservationPlanningSipCollectionStatusHistoryNamespace} from "@preservation-planning/sip/stores/collection/status-history/preservation-planning-sip-collection-status-history.action";
import {PreservationPlanningSipCollectionStatusHistoryState} from "@preservation-planning/sip/stores/collection/status-history/preservation-planning-sip-collection-status-history.state";
import {preservationPlanningSipActionNameSpace} from "@preservation-planning/sip/stores/preservation-planning-sip.action";
import {PreservationPlanningSipStateModel} from "@preservation-planning/sip/stores/preservation-planning-sip.state";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedAipCollectionAction} from "@shared/features/aip/stores/aip-collection/shared-aip-collection.action";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {LocalStateModel} from "@shared/models/local-state.model";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditRoutable,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  StatusHistory,
  StatusHistoryDialog,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-sip-collection-routable",
  templateUrl: "./preservation-planning-sip-collection.routable.html",
  styleUrls: ["./preservation-planning-sip-collection.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningSipCollectionRoutable extends AbstractDetailEditRoutable<Sip, PreservationPlanningSipStateModel> implements OnInit {
  isLoadingCollectionObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningSipCollectionState);
  listCollectionObs: Observable<Aip[]> = MemoizedUtil.list(this._store, PreservationPlanningSipCollectionState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, PreservationPlanningSipCollectionState);

  isInDetailMode: boolean = false;

  @ViewChild("formPresentational")
  readonly formPresentational: PreservationPlanningSipFormPresentational;

  columns: DataTableColumns<Aip>[];
  actions: DataTableActions<Aip>[];

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  readonly KEY_PARAM_NAME: keyof Sip & string = undefined;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.preservationPlanning_sip, _injector, preservationPlanningSipActionNameSpace, StateEnum.preservationPlanning);

    this.columns = [
      {
        field: "info.name" as any,
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
      },
      {
        field: "info.status" as any,
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        filterEnum: Enums.Package.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "smartSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        sortableField: "dataFile.fileSize" as any,
        filterableField: "dataFile.fileSize" as any,
        isSortable: false,
        isFilterable: false,
        translate: true,
        alignment: "right",
        width: "100px",
      },
      {
        field: "info.complianceLevel" as any,
        header: LabelTranslateEnum.complianceLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        component: DataTableComponentHelper.get(DataTableComponentEnum.conformityLevelStar),
        filterEnum: Enums.ComplianceLevel.ComplianceLevelEnumTranslate,
        alignment: "center",
      },
    ];

    this.actions = [
      {
        logo: IconNameEnum.download,
        callback: (aip: Aip) => this._downloadSipAip(this._resId, aip),
        placeholder: current => LabelTranslateEnum.download,
        displayOnCondition: current => false,
      },
      {
        logo: IconNameEnum.internalLink,
        callback: (aip: Aip) => this._goToAip(aip),
        placeholder: current => LabelTranslateEnum.goToAip,
      },
    ];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._getCurrentModelOnParent();

    this.subscribe(this._store.select((s: LocalStateModel) => s.router.state.url)
      .pipe(
        distinctUntilChanged(),
        tap(url => {
          this.isInDetailMode = !url.endsWith(PreservationPlanningRoutesEnum.sipCollections);
          this._changeDetector.detectChanges();
        }),
      ),
    );
  }

  private _getCurrentModelOnParent(): void {
    this._resId = this._route.snapshot.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._getDepositById(this._resId);
  }

  private _getDepositById(id: string): void {
    this._getSubResourceWithParentId(id);
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new PreservationPlanningSipCollectionAction.GetAll(id, null, true));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new PreservationPlanningSipCollectionAction.ChangeQueryParameters(this._resId, queryParameters, true));
  }

  refresh(): void {
    this._store.dispatch(new PreservationPlanningSipCollectionAction.Refresh(this._resId));
  }

  showDetail(aip: Aip): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationPlanningSipDetail, this._resId, PreservationPlanningRoutesEnum.sipCollections, aip.resId]));
  }

  private _downloadSipAip(parentId: string, aip: Aip): void {
    this._store.dispatch(new SharedAipCollectionAction.DownloadAip(parentId, aip));
  }

  private _goToAip(aip: Aip): void {
    this._store.dispatch(new PreservationPlanningSipCollectionAction.GoToAip(aip));
  }

  showHistory(aip: Aip): void {
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningSipCollectionStatusHistoryState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, PreservationPlanningSipCollectionStatusHistoryState, state => state.queryParameters);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, PreservationPlanningSipCollectionStatusHistoryState, state => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: aip.resId,
      name: StringUtil.convertToPascalCase(StateEnum.preservationPlanning_sip_collection_statusHistory),
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: preservationPlanningSipCollectionStatusHistoryNamespace,
      statusEnums: Enums.Package.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }
}
