/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip-file.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Sip,
  SipDataFile,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {PreservationPlanningSipFormPresentational} from "@preservation-planning/sip/components/presentationals/sip-form/preservation-planning-sip-form.presentational";
import {PreservationPlanningSipDataFileAction} from "@preservation-planning/sip/stores/data-file/preservation-planning-sip-data-file.action";
import {PreservationPlanningSipDataFileState} from "@preservation-planning/sip/stores/data-file/preservation-planning-sip-data-file.state";
import {preservationPlanningSipDataFileStatusHistoryNamespace} from "@preservation-planning/sip/stores/data-file/status-history/preservation-planning-sip-data-file-status-history.action";
import {PreservationPlanningSipDataFileStatusHistoryState} from "@preservation-planning/sip/stores/data-file/status-history/preservation-planning-sip-data-file-status-history.state";
import {preservationPlanningSipActionNameSpace} from "@preservation-planning/sip/stores/preservation-planning-sip.action";
import {PreservationPlanningSipStateModel} from "@preservation-planning/sip/stores/preservation-planning-sip.state";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {LocalStateModel} from "@shared/models/local-state.model";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditRoutable,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DeleteDialog,
  DialogUtil,
  isNullOrUndefined,
  isTrue,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  StatusHistory,
  StatusHistoryDialog,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-sip-file-routable",
  templateUrl: "./preservation-planning-sip-file.routable.html",
  styleUrls: ["./preservation-planning-sip-file.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningSipFileRoutable extends AbstractDetailEditRoutable<Sip, PreservationPlanningSipStateModel> implements OnInit {
  isLoadingDataFileObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningSipDataFileState);
  listDataFileObs: Observable<SipDataFile[]> = MemoizedUtil.list(this._store, PreservationPlanningSipDataFileState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, PreservationPlanningSipDataFileState);

  isInDetailMode: boolean = false;

  @ViewChild("formPresentational")
  readonly formPresentational: PreservationPlanningSipFormPresentational;

  columns: DataTableColumns<SipDataFile>[];
  actions: DataTableActions<SipDataFile>[];

  readonly KEY_PARAM_NAME: keyof Sip & string = undefined;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.preservationPlanning_sip, _injector, preservationPlanningSipActionNameSpace, StateEnum.preservationPlanning);

    this.columns = [
      {
        field: "fileName",
        header: LabelTranslateEnum.fileName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        filterEnum: Enums.DataFile.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "smartSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        sortableField: "dataFile.fileSize" as any,
        filterableField: "dataFile.fileSize" as any,
        isSortable: false,
        isFilterable: false,
        translate: true,
        alignment: "right",
        width: "100px",
      },
      {
        field: "complianceLevel",
        header: LabelTranslateEnum.complianceLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        component: DataTableComponentHelper.get(DataTableComponentEnum.conformityLevelStar),
        filterEnum: Enums.ComplianceLevel.ComplianceLevelEnumTranslate,
        alignment: "center",
      },
    ];

    this.actions = [
      {
        logo: IconNameEnum.resume,
        callback: (sipDataFile: SipDataFile) => this._resumeDataFile(this._resId, sipDataFile),
        placeholder: current => LabelTranslateEnum.resume,
        displayOnCondition: current => isNullOrUndefined(current) || current.status === Enums.DataFile.StatusEnum.IN_ERROR,
      },
      {
        logo: IconNameEnum.putInError,
        callback: (sipDataFile: SipDataFile) => this._putInErrorDataFile(this._resId, sipDataFile),
        placeholder: current => LabelTranslateEnum.putInError,
        displayOnCondition: current => this._securityService.isRoot()
          && (isNullOrUndefined(current)
            || (current.status !== Enums.DataFile.StatusEnum.READY && current.status !== Enums.DataFile.StatusEnum.CLEANED)),
      },
      {
        logo: IconNameEnum.download,
        callback: (sipDataFile: SipDataFile) => this._downloadDataFile(this._resId, sipDataFile),
        placeholder: current => LabelTranslateEnum.download,
        displayOnCondition: (current: SipDataFile) => current.status === Enums.DataFile.StatusEnum.PROCESSED || current.status === Enums.DataFile.StatusEnum.READY || current.status === Enums.DataFile.StatusEnum.VIRUS_CHECKED,
      },
      {
        logo: IconNameEnum.delete,
        callback: (sipDataFile: SipDataFile) => this._deleteDatafile(sipDataFile),
        placeholder: current => LabelTranslateEnum.delete,
        displayOnCondition: (current: SipDataFile) => this._securityService.isRoot() && (isNullOrUndefined(current) || current.status === Enums.DataFile.StatusEnum.IN_ERROR),
      },
    ];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._getCurrentModelOnParent();

    this.subscribe(this._store.select((s: LocalStateModel) => s.router.state.url)
      .pipe(
        distinctUntilChanged(),
        tap(url => {
          this.isInDetailMode = !url.endsWith(PreservationPlanningRoutesEnum.sipFiles);
          this._changeDetector.detectChanges();
        }),
      ),
    );
  }

  private _getCurrentModelOnParent(): void {
    this._resId = this._route.snapshot.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._getDepositById(this._resId);
  }

  private _getDepositById(id: string): void {
    this._getSubResourceWithParentId(id);
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new PreservationPlanningSipDataFileAction.GetAll(id, null, true));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new PreservationPlanningSipDataFileAction.ChangeQueryParameters(this._resId, queryParameters, true));
  }

  refresh(): void {
    this._store.dispatch(new PreservationPlanningSipDataFileAction.Refresh(this._resId));
  }

  download($event: SipDataFile): void {
    this._store.dispatch(new PreservationPlanningSipDataFileAction.Download(this._resId, $event));
  }

  showDetail(sipDataFile: SipDataFile): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationPlanningSipDetail, this._resId, PreservationPlanningRoutesEnum.sipFiles, sipDataFile.resId]));
  }

  private _downloadDataFile(parentId: string, dataFile: SipDataFile): void {
    this._store.dispatch(new PreservationPlanningSipDataFileAction.Download(parentId, dataFile));
  }

  private _resumeDataFile(parentId: string, dataFile: SipDataFile): void {
    this._store.dispatch(new PreservationPlanningSipDataFileAction.Resume(parentId, dataFile));
  }

  private _putInErrorDataFile(parentId: string, dataFile: SipDataFile): void {
    this._store.dispatch(new PreservationPlanningSipDataFileAction.PutInError(parentId, dataFile));
  }

  private _deleteDatafile(sipDataFile: SipDataFile): void {
    const deleteData = this._storeDialogService.deleteData(StateEnum.preservationPlanning_sip_dataFile);
    deleteData.name = sipDataFile.fileName;
    this.subscribe(DialogUtil.open(this._dialog, DeleteDialog, deleteData,
      {
        width: "400px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this._store.dispatch(new PreservationPlanningSipDataFileAction.Delete(this._resId, sipDataFile.resId));
        }
      }));
  }

  showHistory(sipDataFile: SipDataFile): void {
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningSipDataFileStatusHistoryState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, PreservationPlanningSipDataFileStatusHistoryState, state => state.queryParameters);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, PreservationPlanningSipDataFileStatusHistoryState, state => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: sipDataFile.infoPackage.resId,
      resourceResId: sipDataFile.resId,
      name: StringUtil.convertToPascalCase(StateEnum.preservationPlanning_sip_dataFile_statusHistory),
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: preservationPlanningSipDataFileStatusHistoryNamespace,
      statusEnums: Enums.DataFile.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }
}
