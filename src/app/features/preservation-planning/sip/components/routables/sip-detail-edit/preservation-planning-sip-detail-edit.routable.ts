/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {MatTabGroup} from "@angular/material/tabs";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Sip} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {PreservationPlanningSipFormPresentational} from "@preservation-planning/sip/components/presentationals/sip-form/preservation-planning-sip-form.presentational";
import {
  PreservationPlanningSipAction,
  preservationPlanningSipActionNameSpace,
} from "@preservation-planning/sip/stores/preservation-planning-sip.action";
import {
  PreservationPlanningSipState,
  PreservationPlanningSipStateModel,
} from "@preservation-planning/sip/stores/preservation-planning-sip.state";
import {PreservationPlanningSipStatusHistoryAction} from "@preservation-planning/sip/stores/status-history/preservation-planning-sip-status-history.action";
import {PreservationPlanningSipStatusHistoryState} from "@preservation-planning/sip/stores/status-history/preservation-planning-sip-status-history.state";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  PreservationPlanningRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {LocalStateModel} from "@shared/models/local-state.model";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {
  AbstractDetailEditRoutable,
  ButtonColorEnum,
  DialogUtil,
  ExtraButtonToolbar,
  isNotNullNorUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  PollingHelper,
  QueryParameters,
  StatusHistory,
  StatusHistoryDialog,
  StringUtil,
  Tab,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-sip-detail-edit-routable",
  templateUrl: "./preservation-planning-sip-detail-edit.routable.html",
  styleUrls: ["./preservation-planning-sip-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningSipDetailEditRoutable extends AbstractDetailEditRoutable<Sip, PreservationPlanningSipStateModel> implements OnInit, OnDestroy {
  private readonly _STATUS_TO_STOP_POLLING_REFRESH_STATUS: Enums.Package.StatusEnum[] = [
    Enums.Package.StatusEnum.IN_ERROR,
    Enums.Package.StatusEnum.COMPLETED,
    Enums.Package.StatusEnum.PRESERVATION_ERROR,
    Enums.Package.StatusEnum.CLEANED,
    Enums.Package.StatusEnum.DISPOSED,
  ];

  historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, PreservationPlanningSipStatusHistoryState, state => state.history);
  isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningSipStatusHistoryState);
  queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, PreservationPlanningSipStatusHistoryState, state => state.queryParameters);

  userRolesObs: Enums.UserApplicationRole.UserApplicationRoleEnum[];

  @ViewChild("formPresentational")
  readonly formPresentational: PreservationPlanningSipFormPresentational;

  @ViewChild("matTabGroup")
  readonly matTabGroup: MatTabGroup;

  message: string;

  override readonly editAvailable: boolean = false;

  override readonly deleteAvailable: boolean = false;

  readonly KEY_PARAM_NAME: keyof Sip & string = "resId";
  readonly KEY_DELETE_BUTTON: string = undefined;
  readonly KEY_EDIT_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string = MARK_AS_TRANSLATABLE("preservation.sip.button.goBackToList");
  readonly KEY_BACK_TO_DETAIL_BUTTON: string = undefined;

  private _currentTab: Tab;

  private get _rootUrl(): string[] {
    return [RoutesEnum.preservationPlanningSipDetail, this._resId];
  }

  listExtraButtons: ExtraButtonToolbar<Sip>[] = [
    {
      color: ButtonColorEnum.primary,
      icon: IconNameEnum.download,
      displayCondition: current => isNotNullNorUndefined(current?.info)
        && current.info.status === Enums.Package.StatusEnum.COMPLETED,
      callback: () => this._download(),
      labelToTranslate: (current) => LabelTranslateEnum.download,
      order: 40,
    },
    {
      color: ButtonColorEnum.primary,
      icon: IconNameEnum.resume,
      displayCondition: current => isNotNullNorUndefined(current?.info)
        && current.info.status === Enums.Package.StatusEnum.IN_ERROR,
      callback: () => this._resume(),
      labelToTranslate: (current) => LabelTranslateEnum.resume,
      order: 40,
    },
    {
      color: ButtonColorEnum.primary,
      icon: IconNameEnum.submit,
      displayCondition: current => isNotNullNorUndefined(current?.info)
        && this._securityService.isRoot()
        && current.info.status === Enums.Package.StatusEnum.COMPLETED,
      callback: () => this._resubmit(),
      labelToTranslate: (current) => LabelTranslateEnum.resubmit,
      order: 40,
    },
    {
      color: ButtonColorEnum.primary,
      icon: IconNameEnum.putInError,
      displayCondition: current => isNotNullNorUndefined(current?.info)
        && this._securityService.isRoot()
        && current.info.status !== Enums.Package.StatusEnum.IN_ERROR
        && current.info.status !== Enums.Package.StatusEnum.COMPLETED,
      callback: () => this._putInError(),
      labelToTranslate: (current) => LabelTranslateEnum.putInError,
      order: 40,
    },
    {
      color: ButtonColorEnum.primary,
      icon: IconNameEnum.delete,
      displayCondition: current => isNotNullNorUndefined(current?.info)
        && this._securityService.isRoot()
        && current.info.status === Enums.Package.StatusEnum.IN_ERROR,
      callback: () => this.delete(),
      labelToTranslate: (current) => LabelTranslateEnum.delete,
      order: 40,
    },
  ];

  listTabs: Tab[] = [
    {
      id: TabEnum.METADATA,
      suffixUrl: PreservationPlanningRoutesEnum.sipMetadata,
      icon: IconNameEnum.metadata,
      titleToTranslate: LabelTranslateEnum.metadata,
      route: () => [...this._rootUrl, PreservationPlanningRoutesEnum.sipMetadata],
    },
    {
      id: TabEnum.FILES,
      suffixUrl: PreservationPlanningRoutesEnum.sipFiles,
      icon: IconNameEnum.files,
      titleToTranslate: LabelTranslateEnum.files,
      route: () => [...this._rootUrl, PreservationPlanningRoutesEnum.sipFiles],
      conditionDisplay: () => this.currentObs.pipe(map(current => current?.dataFileNumber > 0)),
    },
    {
      id: TabEnum.COLLECTION,
      suffixUrl: PreservationPlanningRoutesEnum.sipCollections,
      icon: IconNameEnum.collection,
      titleToTranslate: LabelTranslateEnum.collection,
      route: () => [...this._rootUrl, PreservationPlanningRoutesEnum.sipCollections],
      conditionDisplay: () => this.currentObs.pipe(map(current => current?.collectionSize > 0)),
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _router: Router,
              protected readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.preservationPlanning_sip, _injector, preservationPlanningSipActionNameSpace, StateEnum.preservationPlanning);
    this.userRolesObs = this._store.selectSnapshot((state: LocalStateModel) => state.application.userRoles);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.retrieveCurrentModelWithUrl();
    this._createPollingListenerCompletedStatus();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._cleanState();
  }

  _getSubResourceWithParentId(id: string): void {
  }

  showHistory(): void {
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: this._resId,
      name: StringUtil.convertToPascalCase(this._state),
      statusHistory: this.historyObs,
      isLoading: this.isLoadingHistoryObs,
      queryParametersObs: this.queryParametersHistoryObs,
      state: PreservationPlanningSipStatusHistoryAction,
      statusEnums: Enums.Package.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }

  setCurrentTab($event: Tab): void {
    this._currentTab = $event;
  }

  private _download(): void {
    this._store.dispatch(new PreservationPlanningSipAction.Download(this._resId));
  }

  private _resume(): void {
    this._store.dispatch(new PreservationPlanningSipAction.Resume(this._resId));
  }

  private _resubmit(): void {
    this._store.dispatch(new PreservationPlanningSipAction.Resubmit(this._resId));
  }

  private _putInError(): void {
    this._store.dispatch(new PreservationPlanningSipAction.PutInError(this._resId));
  }

  private _createPollingListenerCompletedStatus(): void {
    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: environment.refreshDepositSubmittedIntervalInSecond,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: 60,
      stopRefreshAfterMaximumIntervalReached: true,
      resetIntervalWhenUserMouseEvent: true,
      filter: () => this._shouldContinuePollingWaitCompletedStatus(),
      actionToDo: () => {
        this._store.dispatch(new PreservationPlanningSipAction.GetById(this._resId, true));
      },
    }));
  }

  private _shouldContinuePollingWaitCompletedStatus(): boolean {
    const status = MemoizedUtil.selectSnapshot(this._store, PreservationPlanningSipState, state => state.current?.info?.status) as Enums.Package.StatusEnum;
    return isNotNullNorUndefined(status) && this._STATUS_TO_STOP_POLLING_REFRESH_STATUS.indexOf(status) === -1;
  }
}

enum TabEnum {
  METADATA = "METADATA",
  FILES = "FILES",
  COLLECTION = "COLLECTIONS",
}
