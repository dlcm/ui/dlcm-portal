/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip-metadata.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {DepositFormPresentational} from "@deposit/components/presentationals/deposit-form/deposit-form.presentational";
import {Sip} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {preservationPlanningSipActionNameSpace} from "@preservation-planning/sip/stores/preservation-planning-sip.action";
import {
  PreservationPlanningSipState,
  PreservationPlanningSipStateModel,
} from "@preservation-planning/sip/stores/preservation-planning-sip.state";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {AbstractDetailEditRoutable} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-sip-metadata-routable",
  templateUrl: "./preservation-planning-sip-metadata.routable.html",
  styleUrls: ["./preservation-planning-sip-metadata.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class PreservationPlanningSipMetadataRoutable extends AbstractDetailEditRoutable<Sip, PreservationPlanningSipStateModel> implements OnInit, OnDestroy {
  @Select(PreservationPlanningSipState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(PreservationPlanningSipState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  @ViewChild("formPresentational")
  readonly formPresentational: DepositFormPresentational;

  readonly KEY_PARAM_NAME: keyof Sip & string = undefined;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.preservationPlanning_sip, _injector, preservationPlanningSipActionNameSpace, StateEnum.preservationPlanning);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  protected _getSubResourceWithParentId(id: string): void {
  }
}
