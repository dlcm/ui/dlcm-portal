/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip-file-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {SipDataFile} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {preservationPlanningSipDataFileActionNameSpace} from "@preservation-planning/sip/stores/data-file/preservation-planning-sip-data-file.action";
import {PreservationPlanningSipDataFileState} from "@preservation-planning/sip/stores/data-file/preservation-planning-sip-data-file.state";
import {preservationPlanningSipDataFileStatusHistoryNamespace} from "@preservation-planning/sip/stores/data-file/status-history/preservation-planning-sip-data-file-status-history.action";
import {PreservationPlanningSipDataFileStatusHistoryState} from "@preservation-planning/sip/stores/data-file/status-history/preservation-planning-sip-data-file-status-history.state";
import {SharedAbstractFileAipDetailRoutable} from "@shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable";
import {SecurityService} from "@shared/services/security.service";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {isTrue} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-sip-file-detail",
  templateUrl: "../../../../../../shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable.html",
  styleUrls: ["../../../../../../shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningSipFileDetailRoutable extends SharedAbstractFileAipDetailRoutable<SipDataFile> implements OnInit {
  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _storeDialogService: StoreDialogService,
              protected readonly _securityService: SecurityService) {
    super(_store,
      _route,
      _actions$,
      _dialog,
      _changeDetector,
      PreservationPlanningSipDataFileState,
      preservationPlanningSipDataFileActionNameSpace,
      PreservationPlanningSipDataFileStatusHistoryState,
      preservationPlanningSipDataFileStatusHistoryNamespace,
      _storeDialogService,
      _securityService);
  }

  protected override _computeCanDelete(): void {
    super._computeCanDelete();
    if (isTrue(this.canDelete)) {
      this.canDelete = this._securityService.isRoot();
    }
  }
}
