/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {BaseFormDefinition} from "@app/shared/models/base-form-definition.model";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  License,
  Sip,
} from "@models";
import {ApiEnum} from "@shared/enums/api.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {KeyValueInfo} from "@shared/models/key-value-info.model";
import {Storage} from "@shared/models/storage.model";
import {sharedLicenseActionNameSpace} from "@shared/stores/license/shared-license.action";
import {SharedLicenseState} from "@shared/stores/license/shared-license.state";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  AbstractFormPresentational,
  BreakpointService,
  DateUtil,
  EnumUtil,
  isNotNullNorUndefined,
  isNullOrUndefined,
  KeyValue,
  ObservableUtil,
  PropertyName,
  ResourceNameSpace,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-sip-form",
  templateUrl: "./preservation-planning-sip-form.presentational.html",
  styleUrls: ["./preservation-planning-sip-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningSipFormPresentational extends AbstractFormPresentational<Sip> implements OnInit {
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  accessEnumValues: KeyValue[] = Enums.Access.AccessEnumTranslate;
  packageStatusEnumValuesTranslate: KeyValue[] = Enums.Package.StatusEnumTranslate;

  sharedLicenseActionNameSpace: ResourceNameSpace = sharedLicenseActionNameSpace;
  sharedLicenseState: typeof SharedLicenseState = SharedLicenseState;

  @Input()
  isReady: boolean = false;

  listStoragions: Storage[] = ApiEnum.archivalStorageList;

  get packageStatusEnum(): typeof Enums.Package.StatusEnum {
    return Enums.Package.StatusEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  private readonly _orgUnitValueBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("orgUnitChange")
  readonly orgUnitValueObs: Observable<string | undefined> = ObservableUtil.asObservable(this._orgUnitValueBS);

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly breakpointService: BreakpointService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  protected _initNewForm(): void {
  }

  protected _bindFormTo(sip: Sip): void {
    this.form = this._fb.group({
      [this.formDefinition.organizationalUnitName]: [sip.organizationalUnit.name, []],
      [this.formDefinition.resId]: [sip.resId, []],
      [this.formDefinition.name]: [sip.info.name, []],
      [this.formDefinition.year]: ["", []],
      [this.formDefinition.status]: [sip.info.status, []],
      [this.formDefinition.access]: [sip.info.access, []],
      [this.formDefinition.dataSensitivity]: [sip.info.dataSensitivity, []],
      [this.formDefinition.licenseId]: [sip.info.licenseId, []],
      [this.formDefinition.complianceLevel]: [sip.info.complianceLevel, []],
      [this.formDefinition.embargoAccess]: [sip.info.embargo?.access, []],
      [this.formDefinition.embargoNumberMonths]: [sip.info.embargo?.months, []],
      [this.formDefinition.embargoStartDate]: [isNullOrUndefined(sip.info.embargo?.startDate) ? "" : DateUtil.convertDateToDateTimeString(new Date(sip.info.embargo?.startDate))],
      [this.formDefinition.contentStructurePublic]: [sip.info.contentStructurePublic, [SolidifyValidator]],
    });
    this.isValidWhenDisable = this.form.valid;
  }

  protected override _modelUpdated(oldValue: Sip | undefined, newValue: Sip): void {
    super._modelUpdated(oldValue, newValue);
    if (isNotNullNorUndefined(newValue) && isNotNullNorUndefined(this.form) && isNotNullNorUndefined(this.form.get(this.formDefinition.status))) {
      this.form.get(this.formDefinition.status).setValue(newValue.info.status);
    }
  }

  protected _treatmentBeforeSubmit(sip: Sip): Sip {
    return sip;
  }

  getSensitivityTooltip(dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum): string {
    return (EnumUtil.getKeyValue(Enums.DataSensitivity.DataSensitivityEnumTranslate, dataSensitivity) as KeyValueInfo)?.infoToTranslate;
  }

  licenceCallback: (value: License) => string = (value: License) => value.title;
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() organizationalUnitName: string;
  @PropertyName() resId: string;
  @PropertyName() name: string;
  @PropertyName() year: string;
  @PropertyName() status: string;
  @PropertyName() access: string;
  @PropertyName() dataSensitivity: string;
  @PropertyName() licenseId: string;
  @PropertyName() complianceLevel: string;
  @PropertyName() deposit: string;
  @PropertyName() aip: string;
  @PropertyName() embargoAccess: string;
  @PropertyName() embargoNumberMonths: string;
  @PropertyName() embargoStartDate: string;
  @PropertyName() contentStructurePublic: string;
}
