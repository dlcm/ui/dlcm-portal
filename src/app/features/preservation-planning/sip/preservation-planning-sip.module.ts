/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {PreservationPlanningSipFormPresentational} from "@preservation-planning/sip/components/presentationals/sip-form/preservation-planning-sip-form.presentational";
import {PreservationPlanningSipCollectionDetailRoutable} from "@preservation-planning/sip/components/routables/sip-collection-detail/preservation-planning-sip-collection-detail.routable";
import {PreservationPlanningSipCollectionRoutable} from "@preservation-planning/sip/components/routables/sip-collection/preservation-planning-sip-collection.routable";
import {PreservationPlanningSipFileDetailRoutable} from "@preservation-planning/sip/components/routables/sip-file-detail/preservation-planning-sip-file-detail.routable";
import {PreservationPlanningSipFileRoutable} from "@preservation-planning/sip/components/routables/sip-file/preservation-planning-sip-file.routable";
import {PreservationPlanningSipListRoutable} from "@preservation-planning/sip/components/routables/sip-list/preservation-planning-sip-list.routable";
import {PreservationPlanningSipMetadataRoutable} from "@preservation-planning/sip/components/routables/sip-metadata/preservation-planning-sip-metadata.routable";
import {PreservationPlanningSipRoutingModule} from "@preservation-planning/sip/preservation-planning-sip-routing.module";
import {PreservationPlanningSipCollectionState} from "@preservation-planning/sip/stores/collection/preservation-planning-sip-collection.state";
import {PreservationPlanningSipCollectionStatusHistoryState} from "@preservation-planning/sip/stores/collection/status-history/preservation-planning-sip-collection-status-history.state";
import {PreservationPlanningSipDataFileState} from "@preservation-planning/sip/stores/data-file/preservation-planning-sip-data-file.state";
import {PreservationPlanningSipDataFileStatusHistoryState} from "@preservation-planning/sip/stores/data-file/status-history/preservation-planning-sip-data-file-status-history.state";
import {PreservationPlanningSipState} from "@preservation-planning/sip/stores/preservation-planning-sip.state";
import {PreservationPlanningSipStatusHistoryState} from "@preservation-planning/sip/stores/status-history/preservation-planning-sip-status-history.state";
import {PreservationPlanningSipDetailEditRoutable} from "./components/routables/sip-detail-edit/preservation-planning-sip-detail-edit.routable";

const routables = [
  PreservationPlanningSipListRoutable,
  PreservationPlanningSipDetailEditRoutable,
  PreservationPlanningSipFileRoutable,
  PreservationPlanningSipFileDetailRoutable,
  PreservationPlanningSipCollectionRoutable,
  PreservationPlanningSipCollectionDetailRoutable,
  PreservationPlanningSipMetadataRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  PreservationPlanningSipFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    PreservationPlanningSipRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      PreservationPlanningSipState,
      PreservationPlanningSipStatusHistoryState,
      PreservationPlanningSipDataFileState,
      PreservationPlanningSipDataFileStatusHistoryState,
      PreservationPlanningSipCollectionState,
      PreservationPlanningSipCollectionStatusHistoryState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationPlanningSipModule {
}
