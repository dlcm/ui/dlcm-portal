/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip-data-file.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SipDataFile} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  CompositionAction,
  CompositionNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.preservationPlanning_sip_dataFile;

export namespace PreservationPlanningSipDataFileAction {
  @TypeDefaultAction(state)
  export class GetAll extends CompositionAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends CompositionAction.GetAllSuccess<SipDataFile> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends CompositionAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends CompositionAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends CompositionAction.GetByIdSuccess<SipDataFile> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends CompositionAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends CompositionAction.Update<SipDataFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends CompositionAction.UpdateSuccess<SipDataFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends CompositionAction.UpdateFail<SipDataFile> {
  }

  @TypeDefaultAction(state)
  export class Create extends CompositionAction.Create<SipDataFile> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends CompositionAction.CreateSuccess<SipDataFile> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends CompositionAction.CreateFail<SipDataFile> {
  }

  @TypeDefaultAction(state)
  export class Delete extends CompositionAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends CompositionAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends CompositionAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends CompositionAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class Clean extends CompositionAction.Clean {
  }

  export class Refresh {
    static readonly type: string = `[${state}] Refresh`;

    constructor(public parentId: string) {
    }
  }

  export class Download {
    static readonly type: string = `[${state}] Download`;

    constructor(public parentId: string, public dataFile: SipDataFile) {
    }
  }

  export class Resume {
    static readonly type: string = `[${state}] Resume`;

    constructor(public parentId: string, public dataFile: SipDataFile) {
    }
  }

  export class PutInError {
    static readonly type: string = `[${state}] PutInError`;

    constructor(public parentId: string, public dataFile: SipDataFile) {
    }
  }
}

export const preservationPlanningSipDataFileActionNameSpace: CompositionNameSpace = PreservationPlanningSipDataFileAction;
