/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip-data-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {SipDataFile} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  PreservationPlanningSipDataFileAction,
  preservationPlanningSipDataFileActionNameSpace,
} from "@preservation-planning/sip/stores/data-file/preservation-planning-sip-data-file.action";
import {
  PreservationPlanningSipDataFileStatusHistoryState,
  SharedAipDataFileStatusHistoryStateModel,
} from "@preservation-planning/sip/stores/data-file/status-history/preservation-planning-sip-data-file-status-history.state";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  DownloadService,
  NotificationService,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";

export const defaultPreservationPlanningSipDataFileValue: () => PreservationPlanningSipDataFileStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    [StateEnum.preservationPlanning_sip_dataFile_statusHistory]: defaultStatusHistoryInitValue(),
  });

export interface PreservationPlanningSipDataFileStateModel extends CompositionStateModel<SipDataFile> {
  [StateEnum.preservationPlanning_sip_dataFile_statusHistory]: SharedAipDataFileStatusHistoryStateModel;
}

@Injectable()
@State<PreservationPlanningSipDataFileStateModel>({
  name: StateEnum.preservationPlanning_sip_dataFile,
  defaults: {
    ...defaultPreservationPlanningSipDataFileValue(),
  },
  children: [
    PreservationPlanningSipDataFileStatusHistoryState,
  ],
})
export class PreservationPlanningSipDataFileState extends CompositionState<PreservationPlanningSipDataFileStateModel, SipDataFile> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationPlanningSipDataFileActionNameSpace,
      resourceName: ApiResourceNameEnum.DATAFILE,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.ingestSip;
  }

  @Action(PreservationPlanningSipDataFileAction.Refresh)
  refresh(ctx: SolidifyStateContext<PreservationPlanningSipDataFileStateModel>, action: PreservationPlanningSipDataFileAction.Refresh): void {
    ctx.dispatch(new PreservationPlanningSipDataFileAction.GetAll(action.parentId, undefined, true));
  }

  @Action(PreservationPlanningSipDataFileAction.Download)
  download(ctx: SolidifyStateContext<PreservationPlanningSipDataFileStateModel>, action: PreservationPlanningSipDataFileAction.Download): void {
    const url = `${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFile.resId}/${ApiActionNameEnum.DL}`;
    this._downloadService.download(url, action.dataFile.fileName, action.dataFile.fileSize);
  }

  @Action(PreservationPlanningSipDataFileAction.Resume)
  resume(ctx: SolidifyStateContext<PreservationPlanningSipDataFileStateModel>, action: PreservationPlanningSipDataFileAction.Resume): Observable<Result> {
    return this._apiService.post<any, Result>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFile.resId}/${ApiActionNameEnum.RESUME}`)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            this._notificationService.showInformation(LabelTranslateEnum.resourceResumed);
            ctx.dispatch(new PreservationPlanningSipDataFileAction.GetAll(action.parentId, undefined, true));
          } else {
            this._notificationService.showError(LabelTranslateEnum.unableResumedResource);
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          this._notificationService.showError(LabelTranslateEnum.unableResumedResource);
          throw new SolidifyStateError(this, error);
        }),
      );
  }
}
