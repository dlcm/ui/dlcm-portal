/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip-collection.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {Aip} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  PreservationPlanningSipCollectionAction,
  preservationPlanningSipCollectionActionNameSpace,
} from "@preservation-planning/sip/stores/collection/preservation-planning-sip-collection.action";
import {
  PreservationPlanningSipCollectionStatusHistoryState,
  PreservationPlanningSipCollectionStatusHistoryStateModel,
} from "@preservation-planning/sip/stores/collection/status-history/preservation-planning-sip-collection-status-history.state";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {
  AppRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  DownloadService,
  NotificationService,
  SolidifyStateContext,
} from "solidify-frontend";

export const defaultPreservationPlanningSipDataFileValue: () => PreservationPlanningSipCollectionStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    [StateEnum.preservationPlanning_sip_collection_statusHistory]: defaultStatusHistoryInitValue(),
  });

export interface PreservationPlanningSipCollectionStateModel extends CompositionStateModel<Aip> {
  [StateEnum.preservationPlanning_sip_collection_statusHistory]: PreservationPlanningSipCollectionStatusHistoryStateModel;
}

@Injectable()
@State<PreservationPlanningSipCollectionStateModel>({
  name: StateEnum.preservationPlanning_sip_collection,
  defaults: {
    ...defaultPreservationPlanningSipDataFileValue(),
  },
  children: [
    PreservationPlanningSipCollectionStatusHistoryState,
  ],
})
export class PreservationPlanningSipCollectionState extends CompositionState<PreservationPlanningSipCollectionStateModel, Aip> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationPlanningSipCollectionActionNameSpace,
      resourceName: ApiResourceNameEnum.AIP,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.ingestSip;
  }

  @Action(PreservationPlanningSipCollectionAction.Refresh)
  refresh(ctx: SolidifyStateContext<PreservationPlanningSipCollectionStateModel>, action: PreservationPlanningSipCollectionAction.Refresh): void {
    ctx.dispatch(new PreservationPlanningSipCollectionAction.GetAll(action.parentId, undefined, true));
  }

  @Action(PreservationPlanningSipCollectionAction.GoToAip)
  goToAip(ctx: SolidifyStateContext<PreservationPlanningSipCollectionStateModel>, action: PreservationPlanningSipCollectionAction.GoToAip): void {
    const pathAipDetail = RoutesEnum.preservationPlanningAip + urlSeparator + 1 + urlSeparator + SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator;
    const path = [pathAipDetail, action.aip.resId];
    ctx.dispatch(new Navigate(path));
  }

  @Action(PreservationPlanningSipCollectionAction.DownloadAip)
  downloadAip(ctx: SolidifyStateContext<PreservationPlanningSipCollectionStateModel>, action: PreservationPlanningSipCollectionAction.DownloadAip): void {
    // NOT WORK
    const url = `${this._urlResource}/${action.parentId}/${this._resourceName}/${action.aip.resId}/${ApiActionNameEnum.DL}`;
    this._downloadService.download(url, action.aip.info.name, action.aip.archiveSize);
  }
}
