/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-sip.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {Sip} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {PreservationPlanningSipCollectionState} from "@preservation-planning/sip/stores/collection/preservation-planning-sip-collection.state";
import {
  defaultPreservationPlanningSipDataFileValue,
  PreservationPlanningSipDataFileState,
  PreservationPlanningSipDataFileStateModel,
} from "@preservation-planning/sip/stores/data-file/preservation-planning-sip-data-file.state";
import {
  PreservationPlanningSipAction,
  preservationPlanningSipActionNameSpace,
} from "@preservation-planning/sip/stores/preservation-planning-sip.action";
import {
  PreservationPlanningSipStatusHistoryState,
  PreservationPlanningSipStatusHistoryStateModel,
} from "@preservation-planning/sip/stores/status-history/preservation-planning-sip-status-history.state";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  DownloadService,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface PreservationPlanningSipStateModel extends ResourceStateModel<Sip> {
  [StateEnum.preservationPlanning_sip_dataFile]: PreservationPlanningSipDataFileStateModel;
  [StateEnum.preservationPlanning_sip_statusHistory]: PreservationPlanningSipStatusHistoryStateModel;
  isLoadingDataFile: boolean;
}

@Injectable()
@State<PreservationPlanningSipStateModel>({
  name: StateEnum.preservationPlanning_sip,
  defaults: {
    ...defaultResourceStateInitValue(),
    [StateEnum.preservationPlanning_sip_dataFile]: defaultPreservationPlanningSipDataFileValue(),
    [StateEnum.preservationPlanning_sip_statusHistory]: {...defaultStatusHistoryInitValue()},
    isLoadingDataFile: false,
  },
  children: [
    PreservationPlanningSipDataFileState,
    PreservationPlanningSipStatusHistoryState,
    PreservationPlanningSipCollectionState,
  ],
})
export class PreservationPlanningSipState extends ResourceState<PreservationPlanningSipStateModel, Sip> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _httpClient: HttpClient,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationPlanningSipActionNameSpace,
      routeRedirectUrlAfterSuccessDeleteAction: () => RoutesEnum.preservationPlanningSip,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.ingestSip;
  }

  @Selector()
  static isLoading(state: PreservationPlanningSipStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static currentTitle(state: PreservationPlanningSipStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.info.name;
  }

  @Selector()
  static isLoadingWithDependency(state: PreservationPlanningSipStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isReadyToBeDisplayed(state: PreservationPlanningSipStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: PreservationPlanningSipStateModel): boolean {
    return true;
  }

  @Action(PreservationPlanningSipAction.Download)
  download(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: PreservationPlanningSipAction.Download): void {
    const fileName = "sip_" + action.id + ".zip";
    this._downloadService.download(`${this._urlResource}/${action.id}/${ApiActionNameEnum.DL}`, fileName);
  }

  @Action(PreservationPlanningSipAction.Resume)
  resume(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: PreservationPlanningSipAction.Resume): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._httpClient.post<Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.RESUME}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new PreservationPlanningSipAction.ResumeSuccess(action));
          } else {
            ctx.dispatch(new PreservationPlanningSipAction.ResumeFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationPlanningSipAction.ResumeFail(action));
          throw error;
        }),
      );
  }

  @Action(PreservationPlanningSipAction.ResumeSuccess)
  resumeSuccess(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: PreservationPlanningSipAction.ResumeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.sip.action.resume.success"));
    ctx.dispatch(new PreservationPlanningSipAction.GetById(action.parentAction.id, true));
  }

  @Action(PreservationPlanningSipAction.ResumeFail)
  resumeFail(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: PreservationPlanningSipAction.ResumeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.sip.action.resume.fail"));
  }

  @Action(PreservationPlanningSipAction.Resubmit)
  resubmit(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: PreservationPlanningSipAction.Resubmit): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._httpClient.post<Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.RESUBMIT}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new PreservationPlanningSipAction.ResubmitSuccess(action));
          } else {
            ctx.dispatch(new PreservationPlanningSipAction.ResubmitFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationPlanningSipAction.ResubmitFail(action));
          throw error;
        }),
      );
  }

  @Action(PreservationPlanningSipAction.ResubmitSuccess)
  resubmitSuccess(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: PreservationPlanningSipAction.ResubmitSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.sip.action.resubmit.success"));
    ctx.dispatch(new PreservationPlanningSipAction.GetById(action.parentAction.id, true));
  }

  @Action(PreservationPlanningSipAction.ResubmitFail)
  resubmitFail(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: PreservationPlanningSipAction.ResubmitFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.sip.action.resubmit.fail"));
  }

  @Action(PreservationPlanningSipAction.PutInError)
  putInError(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: PreservationPlanningSipAction.PutInError): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._httpClient.post<Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.PUT_IN_ERROR}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new PreservationPlanningSipAction.PutInErrorSuccess(action));
          } else {
            ctx.dispatch(new PreservationPlanningSipAction.PutInErrorFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationPlanningSipAction.PutInErrorFail(action));
          throw error;
        }),
      );
  }

  @Action(PreservationPlanningSipAction.PutInErrorSuccess)
  putInErrorSuccess(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: PreservationPlanningSipAction.PutInErrorSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.sip.action.putInError.success"));
    ctx.dispatch(new PreservationPlanningSipAction.GetById(action.parentAction.id, true));
  }

  @Action(PreservationPlanningSipAction.PutInErrorFail)
  putInErrorFail(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: PreservationPlanningSipAction.PutInErrorFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.sip.action.putInError.fail"));
  }
}
