/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-dip.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {PreservationPlanningDipFormPresentational} from "@preservation-planning/dip/components/presentationals/dip-form/preservation-planning-dip-form.presentational";
import {PreservationPlanningDipFileDetailRoutable} from "@preservation-planning/dip/components/routables/dip-file-detail/preservation-planning-dip-file-detail.routable";
import {PreservationPlanningDipFileRoutable} from "@preservation-planning/dip/components/routables/dip-file/preservation-planning-dip-file.routable";
import {PreservationPlanningDipListRoutable} from "@preservation-planning/dip/components/routables/dip-list/preservation-planning-dip-list.routable";
import {PreservationPlanningDipMetadataRoutable} from "@preservation-planning/dip/components/routables/dip-metadata/preservation-planning-dip-metadata.routable";
import {PreservationPlanningDipRoutingModule} from "@preservation-planning/dip/preservation-planning-dip-routing.module";
import {PreservationPlanningDipAipState} from "@preservation-planning/dip/stores/aip/preservation-planning-dip-aip.state";
import {PreservationPlanningDipDataFileState} from "@preservation-planning/dip/stores/data-file/preservation-planning-dip-data-file.state";
import {PreservationPlanningDipDataFileStatusHistoryState} from "@preservation-planning/dip/stores/data-file/status-history/preservation-planning-dip-data-file-status-history.state";
import {PreservationPlanningDipState} from "@preservation-planning/dip/stores/preservation-planning-dip.state";
import {PreservationPlanningDipStatusHistoryState} from "@preservation-planning/dip/stores/status-history/preservation-planning-dip-status-history.state";
import {PreservationPlanningDipDetailEditRoutable} from "./components/routables/dip-detail-edit/preservation-planning-dip-detail-edit.routable";

const routables = [
  PreservationPlanningDipListRoutable,
  PreservationPlanningDipDetailEditRoutable,
  PreservationPlanningDipFileRoutable,
  PreservationPlanningDipFileDetailRoutable,
  PreservationPlanningDipMetadataRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  PreservationPlanningDipFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    PreservationPlanningDipRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      PreservationPlanningDipState,
      PreservationPlanningDipDataFileState,
      PreservationPlanningDipStatusHistoryState,
      PreservationPlanningDipDataFileStatusHistoryState,
      PreservationPlanningDipAipState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationPlanningDipModule {
}
