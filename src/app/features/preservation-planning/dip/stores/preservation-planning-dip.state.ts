/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-dip.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {Dip} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {OrderAllOrderStateModel} from "@order/features/all-order/stores/order-all-order.state";
import {
  PreservationPlanningDipAipState,
  PreservationPlanningDipAipStateModel,
} from "@preservation-planning/dip/stores/aip/preservation-planning-dip-aip.state";
import {
  defaultPreservationPlanningDipDataFileValue,
  PreservationPlanningDipDataFileState,
  PreservationPlanningDipDataFileStateModel,
} from "@preservation-planning/dip/stores/data-file/preservation-planning-dip-data-file.state";
import {
  PreservationPlanningDipAction,
  preservationPlanningDipActionNameSpace,
} from "@preservation-planning/dip/stores/preservation-planning-dip.action";
import {
  PreservationPlanningDipStatusHistoryState,
  PreservationPlanningDipStatusHistoryStateModel,
} from "@preservation-planning/dip/stores/status-history/preservation-planning-dip-status-history.state";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultAssociationStateInitValue,
  defaultResourceStateInitValue,
  DownloadService,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  OverrideDefaultAction,
  ResourceState,
  ResourceStateModel,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface PreservationPlanningDipStateModel extends ResourceStateModel<Dip> {
  [StateEnum.preservationPlanning_dip_dataFile]: PreservationPlanningDipDataFileStateModel;
  isLoadingDataFile: boolean;
  [StateEnum.preservationPlanning_dip_statusHistory]: PreservationPlanningDipStatusHistoryStateModel;
  [StateEnum.preservationPlanning_dip_aip]: PreservationPlanningDipAipStateModel;
}

@Injectable()
@State<PreservationPlanningDipStateModel>({
  name: StateEnum.preservationPlanning_dip,
  defaults: {
    ...defaultResourceStateInitValue(),
    [StateEnum.preservationPlanning_dip_dataFile]: defaultPreservationPlanningDipDataFileValue(),
    isLoadingDataFile: false,
    [StateEnum.preservationPlanning_dip_statusHistory]: {...defaultStatusHistoryInitValue()},
    [StateEnum.preservationPlanning_dip_aip]: {...defaultAssociationStateInitValue()},
  },
  children: [
    PreservationPlanningDipDataFileState,
    PreservationPlanningDipStatusHistoryState,
    PreservationPlanningDipAipState,
  ],
})
export class PreservationPlanningDipState extends ResourceState<PreservationPlanningDipStateModel, Dip> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _httpClient: HttpClient,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationPlanningDipActionNameSpace,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.preservationPlanningDip,
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("dip.notification.resource.delete.success"),
      notificationResourceDeleteFailTextToTranslate: MARK_AS_TRANSLATABLE("dip.notification.resource.delete.fail"),
      notificationResourceUpdateFailTextToTranslate: MARK_AS_TRANSLATABLE("dip.notification.resource.update.fail"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessDip;
  }

  @Selector()
  static isLoading(state: PreservationPlanningDipStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static currentTitle(state: PreservationPlanningDipStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.info.name;
  }

  @Selector()
  static isLoadingWithDependency(state: PreservationPlanningDipStateModel): boolean {
    return this.isLoading(state)
      || StoreUtil.isLoadingState(state[StateEnum.preservationPlanning_dip_aip]);
  }

  @Selector()
  static isReadyToBeDisplayed(state: PreservationPlanningDipStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current)
      && !isNullOrUndefined(state[StateEnum.preservationPlanning_dip_aip].selected);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: PreservationPlanningDipStateModel): boolean {
    return true;
  }

  @OverrideDefaultAction()
  @Action(PreservationPlanningDipAction.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<PreservationPlanningDipStateModel>, action: PreservationPlanningDipAction.UpdateSuccess): void {
    super.updateSuccess(ctx, action);

    ctx.dispatch([
      new Navigate([RoutesEnum.preservationPlanningDipDetail, action.model.resId, DepositRoutesEnum.data]),
    ]);
  }

  @Action(PreservationPlanningDipAction.Download)
  download(ctx: SolidifyStateContext<PreservationPlanningDipStateModel>, action: PreservationPlanningDipAction.Download): void {
    const fileName = "dip_" + action.id + ".zip";
    this._downloadService.download(`${this._urlResource}/${action.id}/${ApiActionNameEnum.DL}`, fileName);
  }

  @Action(PreservationPlanningDipAction.Resume)
  resume(ctx: SolidifyStateContext<PreservationPlanningDipStateModel>, action: PreservationPlanningDipAction.Resume): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._httpClient.post<Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.RESUME}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new PreservationPlanningDipAction.ResumeSuccess(action));
          } else {
            ctx.dispatch(new PreservationPlanningDipAction.ResumeFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationPlanningDipAction.ResumeFail(action));
          throw error;
        }),
      );
  }

  @Action(PreservationPlanningDipAction.ResumeSuccess)
  resumeSuccess(ctx: SolidifyStateContext<PreservationPlanningDipStateModel>, action: PreservationPlanningDipAction.ResumeSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.dip.action.resume.success"));
    ctx.dispatch(new PreservationPlanningDipAction.GetById(action.parentAction.id, true));
  }

  @Action(PreservationPlanningDipAction.ResumeFail)
  resumeFail(ctx: SolidifyStateContext<PreservationPlanningDipStateModel>, action: PreservationPlanningDipAction.ResumeFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.dip.action.resume.fail"));
  }

  @Action(PreservationPlanningDipAction.PutInError)
  putInError(ctx: SolidifyStateContext<PreservationPlanningDipStateModel>, action: PreservationPlanningDipAction.PutInError): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._httpClient.post<Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.PUT_IN_ERROR}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new PreservationPlanningDipAction.PutInErrorSuccess(action));
          } else {
            ctx.dispatch(new PreservationPlanningDipAction.PutInErrorFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationPlanningDipAction.PutInErrorFail(action));
          throw error;
        }),
      );
  }

  @Action(PreservationPlanningDipAction.PutInErrorSuccess)
  putInErrorSuccess(ctx: SolidifyStateContext<PreservationPlanningDipStateModel>, action: PreservationPlanningDipAction.PutInErrorSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.dip.action.putInError.success"));
    ctx.dispatch(new PreservationPlanningDipAction.GetById(action.parentAction.id, true));
  }

  @Action(PreservationPlanningDipAction.PutInErrorFail)
  putInErrorFail(ctx: SolidifyStateContext<PreservationPlanningDipStateModel>, action: PreservationPlanningDipAction.PutInErrorFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.dip.action.putInError.fail"));
  }

  @Action(PreservationPlanningDipAction.GoToDip)
  goToDip(ctx: SolidifyStateContext<OrderAllOrderStateModel>, action: PreservationPlanningDipAction.GoToDip): void {
    const pathOrderDetail = RoutesEnum.preservationPlanningDip + urlSeparator + PreservationPlanningRoutesEnum.dipDetail + AppRoutesEnum.separator;
    const path = [pathOrderDetail, action.dipId];
    ctx.dispatch(new Navigate(path));
  }
}
