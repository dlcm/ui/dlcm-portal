/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-dip-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {DlcmRoutes} from "@app/shared/models/dlcm-route.model";
import {PreservationPlanningDipDetailEditRoutable} from "@preservation-planning/dip/components/routables/dip-detail-edit/preservation-planning-dip-detail-edit.routable";
import {PreservationPlanningDipFileDetailRoutable} from "@preservation-planning/dip/components/routables/dip-file-detail/preservation-planning-dip-file-detail.routable";
import {PreservationPlanningDipFileRoutable} from "@preservation-planning/dip/components/routables/dip-file/preservation-planning-dip-file.routable";
import {PreservationPlanningDipListRoutable} from "@preservation-planning/dip/components/routables/dip-list/preservation-planning-dip-list.routable";
import {PreservationPlanningDipMetadataRoutable} from "@preservation-planning/dip/components/routables/dip-metadata/preservation-planning-dip-metadata.routable";
import {PreservationPlanningDipState} from "@preservation-planning/dip/stores/preservation-planning-dip.state";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  CanDeactivateGuard,
  EmptyContainer,
} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: PreservationPlanningDipListRoutable,
    data: {},
  },
  {
    path: PreservationPlanningRoutesEnum.dipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    redirectTo: PreservationPlanningRoutesEnum.dipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId + AppRoutesEnum.separator + PreservationPlanningRoutesEnum.dipMetadata,
    pathMatch: "full",
  },
  {
    path: PreservationPlanningRoutesEnum.dipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: PreservationPlanningDipDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: PreservationPlanningDipState.currentTitle,
    },
    children: [
      {
        path: PreservationPlanningRoutesEnum.dipMetadata,
        component: PreservationPlanningDipMetadataRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.metadata,
          noBreadcrumbLink: true,
        },
        children: [
          {
            path: PreservationPlanningRoutesEnum.dipEdit,
            component: EmptyContainer,
            data: {
              breadcrumb: LabelTranslateEnum.edit,
            },
            canDeactivate: [CanDeactivateGuard],
          },
        ],
      },
      {
        path: PreservationPlanningRoutesEnum.dipFiles,
        component: PreservationPlanningDipFileRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.files,
          noBreadcrumbLink: true,
        },
        children: [
          {
            path: AppRoutesEnum.paramId,
            component: PreservationPlanningDipFileDetailRoutable,
            data: {
              breadcrumb: LabelTranslateEnum.detail,
            },
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreservationPlanningDipRoutingModule {
}
