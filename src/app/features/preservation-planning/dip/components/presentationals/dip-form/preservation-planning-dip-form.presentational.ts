/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-dip-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {BaseFormDefinition} from "@app/shared/models/base-form-definition.model";
import {Enums} from "@enums";
import {Dip} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {KeyValueInfo} from "@shared/models/key-value-info.model";
import {sharedOrganizationalUnitActionNameSpace} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedOrganizationalUnitState} from "@shared/stores/organizational-unit/shared-organizational-unit.state";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  AbstractFormPresentational,
  BreakpointService,
  DateUtil,
  EnumUtil,
  isNotNullNorUndefined,
  isNullOrUndefined,
  KeyValue,
  ObservableUtil,
  PropertyName,
  ResourceNameSpace,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-dip-form",
  templateUrl: "./preservation-planning-dip-form.presentational.html",
  styleUrls: ["./preservation-planning-dip-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningDipFormPresentational extends AbstractFormPresentational<Dip> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  accessEnumValues: KeyValue[] = Enums.Access.AccessEnumTranslate;
  packageStatusEnumValuesTranslate: KeyValue[] = Enums.Package.StatusEnumTranslate;

  sharedOrgUnitActionNameSpace: ResourceNameSpace = sharedOrganizationalUnitActionNameSpace;
  sharedOrgUnitState: typeof SharedOrganizationalUnitState = SharedOrganizationalUnitState;

  @Input()
  isReady: boolean = false;

  getSensitivityTooltip(dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum): string {
    return (EnumUtil.getKeyValue(Enums.DataSensitivity.DataSensitivityEnumTranslate, dataSensitivity) as KeyValueInfo)?.infoToTranslate;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  private readonly _orgUnitValueBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("orgUnitChange")
  readonly orgUnitValueObs: Observable<string | undefined> = ObservableUtil.asObservable(this._orgUnitValueBS);

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly breakpointService: BreakpointService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  protected _initNewForm(): void {
  }

  protected _bindFormTo(dip: Dip): void {
    this.form = this._fb.group({
      [this.formDefinition.organizationalUnitId]: [dip.info.organizationalUnitId, [SolidifyValidator]],
      [this.formDefinition.resId]: [dip.resId, [SolidifyValidator]],
      [this.formDefinition.name]: [dip.info.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [dip.info.description, [SolidifyValidator]],
      [this.formDefinition.status]: [dip.info.status, [SolidifyValidator]],
      [this.formDefinition.access]: [dip.info.access, [SolidifyValidator]],
      [this.formDefinition.dataSensitivity]: [dip.info.dataSensitivity, [SolidifyValidator]],
      [this.formDefinition.complianceLevel]: [dip.info.complianceLevel, [SolidifyValidator]],
      [this.formDefinition.embargoAccess]: [dip.info.embargo?.access, []],
      [this.formDefinition.embargoNumberMonths]: [dip.info.embargo?.months, []],
      [this.formDefinition.embargoStartDate]: [isNullOrUndefined(dip.info.embargo?.startDate) ? "" : DateUtil.convertDateToDateTimeString(new Date(dip.info.embargo?.startDate))],
    });
    this.isValidWhenDisable = this.form.valid;
  }

  protected _treatmentBeforeSubmit(dip: Dip): Dip {
    const dipToSubmit: Dip = {
      info: {
        name: this.form.get(this.formDefinition.name).value,
        description: this.form.get(this.formDefinition.description).value,
      },
    };
    return dipToSubmit;
  }

  protected override _disableSpecificField(): void {
    this.form.get(this.formDefinition.resId).disable();
    this.form.get(this.formDefinition.status).disable();
    this.form.get(this.formDefinition.access).disable();
    this.form.get(this.formDefinition.complianceLevel).disable();
  }

  protected override _modelUpdated(oldValue: Dip | undefined, newValue: Dip): void {
    super._modelUpdated(oldValue, newValue);
    if (isNotNullNorUndefined(newValue) && isNotNullNorUndefined(this.form) && isNotNullNorUndefined(this.form.get(this.formDefinition.status))) {
      this.form.get(this.formDefinition.status).setValue(newValue.info.status);
    }
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() organizationalUnitId: string;
  @PropertyName() resId: string;
  @PropertyName() name: string;
  @PropertyName() description: string;
  @PropertyName() status: string;
  @PropertyName() access: string;
  @PropertyName() dataSensitivity: string;
  @PropertyName() complianceLevel: string;
  @PropertyName() aip: string;
  @PropertyName() embargoAccess: string;
  @PropertyName() embargoNumberMonths: string;
  @PropertyName() embargoStartDate: string;
}
