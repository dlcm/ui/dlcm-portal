/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-dip-file-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {DipDataFile} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {preservationPlanningDipDataFileActionNameSpace} from "@preservation-planning/dip/stores/data-file/preservation-planning-dip-data-file.action";
import {PreservationPlanningDipDataFileState} from "@preservation-planning/dip/stores/data-file/preservation-planning-dip-data-file.state";
import {preservationPlanningDipDataFileStatusHistoryNamespace} from "@preservation-planning/dip/stores/data-file/status-history/preservation-planning-dip-data-file-status-history.action";
import {PreservationPlanningDipDataFileStatusHistoryState} from "@preservation-planning/dip/stores/data-file/status-history/preservation-planning-dip-data-file-status-history.state";
import {SharedAbstractFileAipDetailRoutable} from "@shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable";
import {SecurityService} from "@shared/services/security.service";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {isTrue} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-dip-file-detail",
  templateUrl: "../../../../../../shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable.html",
  styleUrls: ["../../../../../../shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningDipFileDetailRoutable extends SharedAbstractFileAipDetailRoutable<DipDataFile> implements OnInit {
  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _storeDialogService: StoreDialogService,
              protected readonly _securityService: SecurityService) {
    super(_store,
      _route,
      _actions$,
      _dialog,
      _changeDetector,
      PreservationPlanningDipDataFileState,
      preservationPlanningDipDataFileActionNameSpace,
      PreservationPlanningDipDataFileStatusHistoryState,
      preservationPlanningDipDataFileStatusHistoryNamespace,
      _storeDialogService,
      _securityService);
  }

  protected override _computeCanDelete(): void {
    super._computeCanDelete();
    if (isTrue(this.canDelete)) {
      this.canDelete = this._securityService.isRoot();
    }
  }
}
