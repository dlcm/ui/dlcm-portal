/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-dip-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {appAuthorizedOrganizationalUnitNameSpace} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {AppAuthorizedOrganizationalUnitState} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.state";
import {Enums} from "@enums";
import {
  Dip,
  OrganizationalUnit,
  PreservationJob,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  PreservationPlanningDipAction,
  preservationPlanningDipActionNameSpace,
} from "@preservation-planning/dip/stores/preservation-planning-dip.action";
import {PreservationPlanningDipStateModel} from "@preservation-planning/dip/stores/preservation-planning-dip.state";
import {preservationPlanningDipStatusHistoryNamespace} from "@preservation-planning/dip/stores/status-history/preservation-planning-dip-status-history.action";
import {PreservationPlanningDipStatusHistoryState} from "@preservation-planning/dip/stores/status-history/preservation-planning-dip-status-history.state";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  DataTableActions,
  DataTableFieldTypeEnum,
  OrderEnum,
  ResourceNameSpace,
  RouterExtensionService,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-dip-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./preservation-planning-dip-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningDipListRoutable extends AbstractListRoutable<Dip, PreservationPlanningDipStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string = LabelTranslateEnum.backToPreservationPlanning;
  readonly KEY_PARAM_NAME: keyof Dip & string = "info.name" as any;

  appAuthorizedOrganizationalUnitSort: Sort<OrganizationalUnit> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  appAuthorizedOrganizationalUnitNameSpace: ResourceNameSpace = appAuthorizedOrganizationalUnitNameSpace;
  appAuthorizedOrganizationalUnitState: typeof AppAuthorizedOrganizationalUnitState = AppAuthorizedOrganizationalUnitState;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.preservationPlanning_dip, preservationPlanningDipActionNameSpace, _injector, {
      canCreate: false,
      historyState: PreservationPlanningDipStatusHistoryState,
      historyStateAction: preservationPlanningDipStatusHistoryNamespace,
      historyStatusEnumTranslate: Enums.Package.StatusEnumTranslate,
    }, StateEnum.preservationPlanning);
  }

  conditionDisplayEditButton(model: Dip | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: Dip | undefined): boolean {
    return this._securityService.isRoot();
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "info.name" as any,
        header: LabelTranslateEnum.title,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "info.organizationalUnitId" as any,
        header: LabelTranslateEnum.organizationalUnit,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        component: DataTableComponentHelper.get(DataTableComponentEnum.organizationalUnitName),
        isFilterable: true,
        isSortable: true,
        resourceNameSpace: this.appAuthorizedOrganizationalUnitNameSpace,
        resourceState: this.appAuthorizedOrganizationalUnitState as any,
        searchableSingleSelectSort: this.appAuthorizedOrganizationalUnitSort,
        filterableField: "info.organizationalUnitId" as any,
        sortableField: "info.organizationalUnitId" as any,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "info.status" as any,
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Package.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
    ];
  }

  protected override _defineActions(): DataTableActions<PreservationJob>[] {
    return [
      ...super._defineActions(),
      {
        logo: IconNameEnum.download,
        callback: (model: Dip) => this._download(model),
        placeholder: current => LabelTranslateEnum.download,
        displayOnCondition: (model: Dip) => true,
      },
    ];
  }

  private _download(dip: Dip): void {
    this._store.dispatch(new PreservationPlanningDipAction.Download(dip.resId));
  }
}
