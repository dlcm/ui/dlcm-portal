/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-dip-file.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Dip,
  DipDataFile,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {PreservationPlanningDipFormPresentational} from "@preservation-planning/dip/components/presentationals/dip-form/preservation-planning-dip-form.presentational";
import {PreservationPlanningDipDataFileAction} from "@preservation-planning/dip/stores/data-file/preservation-planning-dip-data-file.action";
import {PreservationPlanningDipDataFileState} from "@preservation-planning/dip/stores/data-file/preservation-planning-dip-data-file.state";
import {preservationPlanningDipDataFileStatusHistoryNamespace} from "@preservation-planning/dip/stores/data-file/status-history/preservation-planning-dip-data-file-status-history.action";
import {PreservationPlanningDipDataFileStatusHistoryState} from "@preservation-planning/dip/stores/data-file/status-history/preservation-planning-dip-data-file-status-history.state";
import {preservationPlanningDipActionNameSpace} from "@preservation-planning/dip/stores/preservation-planning-dip.action";
import {PreservationPlanningDipStateModel} from "@preservation-planning/dip/stores/preservation-planning-dip.state";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  AbstractDetailEditRoutable,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DeleteDialog,
  DialogUtil,
  isNullOrUndefined,
  isTrue,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  StatusHistory,
  StatusHistoryDialog,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-dip-file-routable",
  templateUrl: "./preservation-planning-dip-file.routable.html",
  styleUrls: ["./preservation-planning-dip-file.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningDipFileRoutable extends AbstractDetailEditRoutable<Dip, PreservationPlanningDipStateModel> implements OnInit {
  isLoadingDataFileObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningDipDataFileState);
  listDataFileObs: Observable<DipDataFile[]> = MemoizedUtil.list(this._store, PreservationPlanningDipDataFileState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, PreservationPlanningDipDataFileState);

  isInDetailMode: boolean = false;

  @ViewChild("formPresentational")
  readonly formPresentational: PreservationPlanningDipFormPresentational;

  columns: DataTableColumns<DipDataFile>[];
  actions: DataTableActions<DipDataFile>[];

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  readonly KEY_PARAM_NAME: keyof Dip & string = undefined;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.preservationPlanning_dip, _injector, preservationPlanningDipActionNameSpace, StateEnum.preservationPlanning);

    this.columns = [
      {
        field: "fileName",
        header: LabelTranslateEnum.fileName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: true,
        isFilterable: false,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        filterEnum: Enums.DataFile.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "smartSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        sortableField: "dataFile.fileSize" as any,
        filterableField: "dataFile.fileSize" as any,
        isSortable: false,
        isFilterable: false,
        translate: true,
        alignment: "right",
        width: "100px",
      },
      {
        field: "complianceLevel",
        header: LabelTranslateEnum.complianceLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        component: DataTableComponentHelper.get(DataTableComponentEnum.conformityLevelStar),
        filterEnum: Enums.ComplianceLevel.ComplianceLevelEnumTranslate,
        alignment: "center",
      },
    ];

    this.actions = [
      {
        logo: IconNameEnum.resume,
        callback: (dipDataFile: DipDataFile) => this._resumeDataFile(this._resId, dipDataFile),
        placeholder: current => LabelTranslateEnum.resume,
        displayOnCondition: current => isNullOrUndefined(current) || current.status === Enums.DataFile.StatusEnum.IN_ERROR,
      },
      {
        logo: IconNameEnum.download,
        callback: (dipDataFile: DipDataFile) => this._downloadDataFile(this._resId, dipDataFile),
        placeholder: current => LabelTranslateEnum.download,
        displayOnCondition: (current: DipDataFile) => current.status === Enums.DataFile.StatusEnum.PROCESSED || current.status === Enums.DataFile.StatusEnum.READY || current.status === Enums.DataFile.StatusEnum.VIRUS_CHECKED,
      },
      {
        logo: IconNameEnum.delete,
        callback: (dipDataFile: DipDataFile) => this._deleteDatafile(dipDataFile),
        placeholder: current => LabelTranslateEnum.delete,
        displayOnCondition: (current: DipDataFile) => this._securityService.isRoot() && (isNullOrUndefined(current) || current.status === Enums.DataFile.StatusEnum.IN_ERROR),
      },
    ];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._getCurrentModelOnParent();
  }

  private _getCurrentModelOnParent(): void {
    this._resId = this._route.snapshot.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._getDepositById(this._resId);
  }

  private _getDepositById(id: string): void {
    this._getSubResourceWithParentId(id);
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new PreservationPlanningDipDataFileAction.GetAll(id, null, true));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new PreservationPlanningDipDataFileAction.ChangeQueryParameters(this._resId, queryParameters, true));
  }

  refresh(): void {
    this._store.dispatch(new PreservationPlanningDipDataFileAction.Refresh(this._resId));
  }

  download($event: DipDataFile): void {
    this._store.dispatch(new PreservationPlanningDipDataFileAction.Download(this._resId, $event));
  }

  showDetail(dipDataFile: DipDataFile): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationPlanningDipDetail, this._resId, PreservationPlanningRoutesEnum.dipFiles, dipDataFile.resId]));
  }

  private _downloadDataFile(parentId: string, dataFile: DipDataFile): void {
    this._store.dispatch(new PreservationPlanningDipDataFileAction.Download(parentId, dataFile));
  }

  private _resumeDataFile(parentId: string, dataFile: DipDataFile): void {
    this._store.dispatch(new PreservationPlanningDipDataFileAction.Resume(parentId, dataFile));
  }

  private _deleteDatafile(dipDataFile: DipDataFile): void {
    const deleteData = this._storeDialogService.deleteData(StateEnum.preservationPlanning_dip_dataFile);
    deleteData.name = dipDataFile.fileName;
    this.subscribe(DialogUtil.open(this._dialog, DeleteDialog, deleteData,
      {
        width: "400px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this._store.dispatch(new PreservationPlanningDipDataFileAction.Delete(this._resId, dipDataFile.resId));
        }
      }));
  }

  showHistory(dipDataFile: DipDataFile): void {
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningDipDataFileStatusHistoryState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, PreservationPlanningDipDataFileStatusHistoryState, state => state.queryParameters);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, PreservationPlanningDipDataFileStatusHistoryState, state => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: dipDataFile.infoPackage.resId,
      resourceResId: dipDataFile.resId,
      name: StringUtil.convertToPascalCase(StateEnum.preservationPlanning_dip_dataFile_statusHistory),
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: preservationPlanningDipDataFileStatusHistoryNamespace,
      statusEnums: Enums.DataFile.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }
}
