/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-monitoring-web-service.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {PreservationPlanningMonitoringWebServiceAction} from "@preservation-planning/monitoring/stores/web-service/preservation-planning-monitoring-web-service.action";
import {PreservationPlanningMonitoringWebServiceState} from "@preservation-planning/monitoring/stores/web-service/preservation-planning-monitoring-web-service.state";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {Observable} from "rxjs";
import {
  isNullOrUndefined,
  MemoizedUtil,
  SolidifyObject,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-monitoring-web-service-dialog",
  templateUrl: "./preservation-planning-monitoring-web-service.dialog.html",
  styleUrls: ["./preservation-planning-monitoring-web-service.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningMonitoringWebServiceDialog extends SharedAbstractDialog<PreservationPlanningMonitoringWebServiceDialogData> implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningMonitoringWebServiceState);
  resultObs: Observable<SolidifyObject> = MemoizedUtil.select(this._store, PreservationPlanningMonitoringWebServiceState, state => state.result);

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<PreservationPlanningMonitoringWebServiceDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: PreservationPlanningMonitoringWebServiceDialogData,
              private readonly _fb: FormBuilder) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._store.dispatch(new PreservationPlanningMonitoringWebServiceAction.Get(this.data.apiUrl));
  }

  getTitle(): string {
    return StringUtil.capitalize(this.data.serviceName) + (isNullOrUndefined(this.data.resourceName) ? "" : " > " + StringUtil.capitalize(this.data.resourceName));
  }
}

export interface PreservationPlanningMonitoringWebServiceDialogData {
  apiUrl: string;
  serviceName: string;
  resourceName: string | undefined;
}
