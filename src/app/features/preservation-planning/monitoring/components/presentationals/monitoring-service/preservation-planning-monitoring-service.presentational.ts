/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-monitoring-service.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {PreservationPlanningMonitoringWebServiceDialog} from "@preservation-planning/monitoring/components/dialogs/monitoring-web-service/preservation-planning-monitoring-web-service.dialog";
import {urlSeparator} from "@shared/enums/routes.enum";
import {PreservationPlanningMonitorService} from "@shared/models/business/preservation-planning-monitor.model";
import {
  DialogUtil,
  HateOASLink,
  isArray,
  isNullOrUndefined,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-monitoring-service",
  templateUrl: "./preservation-planning-monitoring-service.presentational.html",
  styleUrls: ["./preservation-planning-monitoring-service.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningMonitoringServicePresentational extends SharedAbstractPresentational {
  @Input()
  service: PreservationPlanningMonitorService;

  @Input()
  index: number | undefined;

  constructor(private readonly _dialog: MatDialog) {
    super();
  }

  getResourceName(url: string): string {
    return url.substring(url.lastIndexOf(urlSeparator) + 1, url.length);
  }

  getResources(): HateOASLink[] {
    let resources = undefined;
    if (isNullOrUndefined(this.service._links)) {
      return resources;
    }
    resources = this.service._links.resources;
    if (isArray(resources)) {
      return resources;
    }
    return [resources];
  }

  open(apiUrl: string, serviceName: string, resourceName: string = undefined): void {
    this.subscribe(DialogUtil.open(this._dialog, PreservationPlanningMonitoringWebServiceDialog, {
      apiUrl: apiUrl,
      serviceName: serviceName,
      resourceName: resourceName,
    }, {
      takeOne: true,
    }));
  }
}
