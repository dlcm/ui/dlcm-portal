/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-monitoring-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from "@angular/core";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {PreservationPlanningMonitoringAction} from "@preservation-planning/monitoring/stores/preservation-planning-monitoring.action";
import {PreservationPlanningMonitoringState} from "@preservation-planning/monitoring/stores/preservation-planning-monitoring.state";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {
  PreservationPlanningMonitor,
  PreservationPlanningMonitorService,
} from "@shared/models/business/preservation-planning-monitor.model";
import {Observable} from "rxjs";
import {
  isArray,
  isNullOrUndefined,
  MemoizedUtil,
  ObjectUtil,
  PollingHelper,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-monitoring-home-routable",
  templateUrl: "./preservation-planning-monitoring-home.routable.html",
  styleUrls: ["./preservation-planning-monitoring-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningMonitoringHomeRoutable extends SharedAbstractPresentational implements OnInit {
  preservationPlanningMonitor: Observable<PreservationPlanningMonitor> = MemoizedUtil.select(this._store, PreservationPlanningMonitoringState, state => state.preservationPlanningMonitor, true);
  isLoading: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningMonitoringState);

  private readonly _INTERVAL_REFRESH_IN_SECOND: number = 10;

  constructor(private readonly _store: Store) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.getAll();
    this.subscribe(PollingHelper.startPollingObs({
      waitingTimeBeforeStartInSecond: this._INTERVAL_REFRESH_IN_SECOND,
      initialIntervalRefreshInSecond: this._INTERVAL_REFRESH_IN_SECOND,
      incrementInterval: true,
      resetIntervalWhenUserMouseEvent: true,
      actionToDo: () => this.getAll(),
    }));
  }

  getAll(): void {
    this._store.dispatch(new PreservationPlanningMonitoringAction.Get(true));
  }

  getListOfServices(preservationPlanningMonitor: PreservationPlanningMonitor): PreservationPlanningMonitorService[] | PreservationPlanningMonitor[] {
    const keys = ObjectUtil.keys(preservationPlanningMonitor);
    const result = ObjectUtil.values(preservationPlanningMonitor);
    const newList = [];
    result.forEach((res, i) => {
      if (isArray(res)) {
        const subList = [];
        res = [...res];
        res.forEach((r, index) => {
          r = ObjectUtil.clone(r);
          r.name = keys[i] + " " + (index + 1);
          subList.push(r);
        });
        newList.push(subList);
      } else {
        res = ObjectUtil.clone(res);
        res.name = keys[i];
        newList.push(res);
      }
    });
    return newList;
  }

  trackByFn(index: number, item: PreservationPlanningMonitorService | PreservationPlanningMonitor): string {
    if (isNullOrUndefined(item.name)) {
      return StringUtil.stringEmpty;
    }
    item = item as PreservationPlanningMonitorService;
    return item.name + " " + item.status;
  }

  back(): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationPlanning]));
  }

  asPreservationPlanningMonitorService(value: any): PreservationPlanningMonitorService {
    return value as PreservationPlanningMonitorService;
  }

  asPreservationPlanningMonitorServiceArray(value: any): PreservationPlanningMonitorService[] {
    return value as PreservationPlanningMonitorService[];
  }
}
