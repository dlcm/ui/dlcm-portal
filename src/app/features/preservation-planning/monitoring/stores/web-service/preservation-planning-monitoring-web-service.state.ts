/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-monitoring-web-service.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  PreservationPlanningMonitoringWebServiceAction,
  preservationPlanningMonitorWebServiceActionNameSpace,
} from "@preservation-planning/monitoring/stores/web-service/preservation-planning-monitoring-web-service.action";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseState,
  BaseStateModel,
  defaultBaseStateInitValue,
  NotificationService,
  SolidifyHttpErrorResponseModel,
  SolidifyObject,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface PreservationPlanningMonitoringWebServiceStateModel extends BaseStateModel {
  result: SolidifyObject | undefined;
}

@Injectable()
@State<PreservationPlanningMonitoringWebServiceStateModel>({
  name: StateEnum.preservationPlanning_monitoring_webService,
  defaults: {
    ...defaultBaseStateInitValue(),
    result: undefined,
  },
  children: [],
})
export class PreservationPlanningMonitoringWebServiceState extends BaseState<PreservationPlanningMonitoringWebServiceStateModel> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              private readonly _httpClient: HttpClient) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: preservationPlanningMonitorWebServiceActionNameSpace,
    }, PreservationPlanningMonitoringWebServiceState);
  }

  protected get _urlResource(): string {
    return undefined;
  }

  @Action(PreservationPlanningMonitoringWebServiceAction.Get)
  get(ctx: SolidifyStateContext<PreservationPlanningMonitoringWebServiceStateModel>, action: PreservationPlanningMonitoringWebServiceAction.Get): Observable<SolidifyObject> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        result: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });

    return this._apiService.get(action.apiUrl).pipe(
      StoreUtil.cancelUncompleted(action, ctx, this._actions$, [PreservationPlanningMonitoringWebServiceAction.Get, Navigate]),
      tap((result: SolidifyObject) => {
        ctx.dispatch(new PreservationPlanningMonitoringWebServiceAction.GetSuccess(action, result));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new PreservationPlanningMonitoringWebServiceAction.GetFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(PreservationPlanningMonitoringWebServiceAction.GetSuccess)
  getAllSuccess(ctx: SolidifyStateContext<PreservationPlanningMonitoringWebServiceStateModel>, action: PreservationPlanningMonitoringWebServiceAction.GetSuccess): void {
    ctx.patchState({
      result: action.result,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(PreservationPlanningMonitoringWebServiceAction.GetFail)
  getAllFail(ctx: SolidifyStateContext<PreservationPlanningMonitoringWebServiceStateModel>, action: PreservationPlanningMonitoringWebServiceAction.GetFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
