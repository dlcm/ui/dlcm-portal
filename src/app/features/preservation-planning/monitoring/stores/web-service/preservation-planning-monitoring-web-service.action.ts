/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-monitoring-web-service.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  SolidifyObject,
} from "solidify-frontend";

const state = StateEnum.preservationPlanning_monitoring_webService;

export namespace PreservationPlanningMonitoringWebServiceAction {
  export class Get extends BaseAction {
    static readonly type: string = `[${state}] Get`;

    constructor(public apiUrl: string, public keepCurrentContext: boolean = false) {
      super();
    }
  }

  export class GetSuccess extends BaseSubActionSuccess<Get> {
    static readonly type: string = `[${state}] Get Success`;

    constructor(public parent: Get, public result: SolidifyObject) {
      super(parent);
    }
  }

  export class GetFail extends BaseSubActionFail<Get> {
    static readonly type: string = `[${state}] Get Fail`;

    constructor(public parent: Get) {
      super(parent);
    }
  }
}

export const preservationPlanningMonitorWebServiceActionNameSpace = PreservationPlanningMonitoringWebServiceAction;
