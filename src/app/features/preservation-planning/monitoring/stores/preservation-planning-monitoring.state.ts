/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-monitoring.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  preservationPlanningMonitorActionNameSpace,
  PreservationPlanningMonitoringAction,
} from "@preservation-planning/monitoring/stores/preservation-planning-monitoring.action";
import {
  PreservationPlanningMonitoringWebServiceState,
  PreservationPlanningMonitoringWebServiceStateModel,
} from "@preservation-planning/monitoring/stores/web-service/preservation-planning-monitoring-web-service.state";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {PreservationPlanningMonitor} from "@shared/models/business/preservation-planning-monitor.model";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BaseState,
  BaseStateModel,
  defaultBaseStateInitValue,
  NotificationService,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface PreservationPlanningMonitoringStateModel extends BaseStateModel {
  preservationPlanningMonitor: PreservationPlanningMonitor | undefined;
  [StateEnum.preservationPlanning_monitoring_webService]: PreservationPlanningMonitoringWebServiceStateModel | undefined;
}

@Injectable()
@State<PreservationPlanningMonitoringStateModel>({
  name: StateEnum.preservationPlanning_monitoring,
  defaults: {
    ...defaultBaseStateInitValue(),
    preservationPlanningMonitor: undefined,
    [StateEnum.preservationPlanning_monitoring_webService]: undefined,
  },
  children: [
    PreservationPlanningMonitoringWebServiceState,
  ],
})
export class PreservationPlanningMonitoringState extends BaseState<PreservationPlanningMonitoringStateModel> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: preservationPlanningMonitorActionNameSpace,
    }, PreservationPlanningMonitoringState);
  }

  protected get _urlResource(): string {
    return ApiEnum.preservationPlanningMonitor;
  }

  @Action(PreservationPlanningMonitoringAction.Get)
  get(ctx: SolidifyStateContext<PreservationPlanningMonitoringStateModel>, action: PreservationPlanningMonitoringAction.Get): Observable<PreservationPlanningMonitor> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        preservationPlanningMonitor: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });
    return this._apiService.getCollection<PreservationPlanningMonitor>(this._urlResource, null)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [Navigate, PreservationPlanningMonitoringAction.Get]),
        tap((preservationPlanningMonitor: PreservationPlanningMonitor) => {
          ctx.dispatch(new PreservationPlanningMonitoringAction.GetSuccess(action, preservationPlanningMonitor));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationPlanningMonitoringAction.GetFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(PreservationPlanningMonitoringAction.GetSuccess)
  getAllSuccess(ctx: SolidifyStateContext<PreservationPlanningMonitoringStateModel>, action: PreservationPlanningMonitoringAction.GetSuccess): void {
    ctx.patchState({
      preservationPlanningMonitor: action.preservationPlanningMonitor,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(PreservationPlanningMonitoringAction.GetFail)
  getAllFail(ctx: SolidifyStateContext<PreservationPlanningMonitoringStateModel>, action: PreservationPlanningMonitoringAction.GetFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
