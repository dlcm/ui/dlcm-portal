/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-monitoring.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {PreservationPlanningMonitoringWebServiceDialog} from "@preservation-planning/monitoring/components/dialogs/monitoring-web-service/preservation-planning-monitoring-web-service.dialog";
import {PreservationPlanningMonitoringServicePresentational} from "@preservation-planning/monitoring/components/presentationals/monitoring-service/preservation-planning-monitoring-service.presentational";
import {PreservationPlanningMonitoringHomeRoutable} from "@preservation-planning/monitoring/components/routables/monitoring-home/preservation-planning-monitoring-home.routable";
import {PreservationPlanningMonitoringRoutingModule} from "@preservation-planning/monitoring/preservation-planning-monitoring-routing.module";
import {PreservationPlanningMonitoringState} from "@preservation-planning/monitoring/stores/preservation-planning-monitoring.state";
import {PreservationPlanningMonitoringWebServiceState} from "@preservation-planning/monitoring/stores/web-service/preservation-planning-monitoring-web-service.state";

const routables = [
  PreservationPlanningMonitoringHomeRoutable,
];
const containers = [];
const dialogs = [
  PreservationPlanningMonitoringWebServiceDialog,
];
const presentationals = [
  PreservationPlanningMonitoringServicePresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    PreservationPlanningMonitoringRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      PreservationPlanningMonitoringState,
      PreservationPlanningMonitoringWebServiceState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationPlanningMonitoringModule {
}
