/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-aip-status-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Enums} from "@enums";
import {AipCopyList} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {PreservationAipStatusAction} from "@preservation-planning/aip-status/stores/preservation-planning-aip-status.action";
import {PreservationPlanningAipStatusState} from "@preservation-planning/aip-status/stores/preservation-planning-aip-status.state";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {Observable} from "rxjs";
import {
  DataTableColumns,
  DataTableFieldTypeEnum,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-aip-status-home-routable",
  templateUrl: "./preservation-planning-aip-status-home.routable.html",
  styleUrls: ["./preservation-planning-aip-status-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningAipStatusHomeRoutable extends SharedAbstractPresentational implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationPlanningAipStatusState);
  listObs: Observable<AipCopyList[]> = MemoizedUtil.list(this._store, PreservationPlanningAipStatusState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, PreservationPlanningAipStatusState);
  columns: DataTableColumns<AipCopyList>[];

  constructor(private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.defineColumns();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "resId",
        header: LabelTranslateEnum.identifier,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
      },
      {
        field: "copies",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.aipStatusNamePresentational),
      },
      {
        field: "copies",
        header: LabelTranslateEnum.organizationalUnit,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        component: DataTableComponentHelper.get(DataTableComponentEnum.aipStatusOrgUnitPresentational),
        isFilterable: false,
        isSortable: false,
      },
      {
        field: "status" as any,
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: false,
        filterableField: "searchType" as any,
        filterEnum: Enums.Aip.AipCopySearchTypeEnumTranslate,
        translate: true,
        component: DataTableComponentHelper.get(DataTableComponentEnum.aipStatusSummaryPresentational),
      },
      {
        field: "creationDate",
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        filterableField: "creation.when" as any,
        sortableField: "creation.when" as any,
      },
    ];
  }

  getAll(queryParameters?: QueryParameters): void {
    this._store.dispatch(new PreservationAipStatusAction.GetAll(queryParameters, true));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new PreservationAipStatusAction.ChangeQueryParameters(queryParameters, true));
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  back(): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationPlanning]));
  }

  deleteCache(): void {
    this._store.dispatch(new PreservationAipStatusAction.DeleteCache());
  }
}
