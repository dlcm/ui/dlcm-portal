/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-aip-status.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {DepositStateModel} from "@deposit/stores/deposit.state";
import {environment} from "@environments/environment";
import {AipCopyList} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  PreservationAipStatusAction,
  preservationAipStatusActionNameSpace,
} from "@preservation-planning/aip-status/stores/preservation-planning-aip-status.action";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";

export interface PreservationPlanningAipStatusStateModel extends ResourceStateModel<AipCopyList> {
}

@Injectable()
@State<PreservationPlanningAipStatusStateModel>({
  name: StateEnum.preservationPlanning_aipStatus,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class PreservationPlanningAipStatusState extends ResourceState<PreservationPlanningAipStatusStateModel, AipCopyList> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _httpClient: HttpClient) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationAipStatusActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.preservationPlanningPreservationAipStatuses;
  }

  @Action(PreservationAipStatusAction.DeleteCache)
  deleteCache(ctx: SolidifyStateContext<DepositStateModel>, action: PreservationAipStatusAction.DeleteCache): Observable<string> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._httpClient.post(this._urlResource + urlSeparator + ApiActionNameEnum.DELETE_CACHE, null, {
      responseType: "text",
    }).pipe(
      tap(result => {
        ctx.dispatch(new PreservationAipStatusAction.DeleteCacheSuccess(action));
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new PreservationAipStatusAction.DeleteCacheFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @Action(PreservationAipStatusAction.DeleteCacheSuccess)
  deleteCacheSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: PreservationAipStatusAction.DeleteCacheSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("admin.preservation.aipStatuses.notification.deleteCache.success"));
  }

  @Action(PreservationAipStatusAction.DeleteCacheFail)
  deleteCacheFail(ctx: SolidifyStateContext<DepositStateModel>, action: PreservationAipStatusAction.DeleteCacheFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("admin.preservation.aipStatuses.notification.deleteCache.fail"));
  }
}
