/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {PreservationPlanningAipStatusState} from "@preservation-planning/aip-status/stores/preservation-planning-aip-status.state";
import {PreservationPlanningHomeRoutable} from "@preservation-planning/components/routables/preservation-planning-home/preservation-planning-home.routable";
import {PreservationPlanningDepositState} from "@preservation-planning/deposit/stores/preservation-planning-deposit.state";
import {PreservationPlanningDepositStatusHistoryState} from "@preservation-planning/deposit/stores/status-history/preservation-planning-deposit-status-history.state";
import {PreservationPlanningDipAipState} from "@preservation-planning/dip/stores/aip/preservation-planning-dip-aip.state";
import {PreservationPlanningDipDataFileState} from "@preservation-planning/dip/stores/data-file/preservation-planning-dip-data-file.state";
import {PreservationPlanningDipDataFileStatusHistoryState} from "@preservation-planning/dip/stores/data-file/status-history/preservation-planning-dip-data-file-status-history.state";
import {PreservationPlanningDipState} from "@preservation-planning/dip/stores/preservation-planning-dip.state";
import {PreservationPlanningDipStatusHistoryState} from "@preservation-planning/dip/stores/status-history/preservation-planning-dip-status-history.state";
import {PreservationPlanningJobExecutionReportState} from "@preservation-planning/job/stores/job-execution/job-execution-report/preservation-planning-job-execution-report.state";
import {PreservationPlanningJobExecutionState} from "@preservation-planning/job/stores/job-execution/preservation-planning-job-execution.state";
import {PreservationPlanningJobState} from "@preservation-planning/job/stores/preservation-planning-job.state";
import {PreservationPlanningMonitoringState} from "@preservation-planning/monitoring/stores/preservation-planning-monitoring.state";
import {PreservationPlanningMonitoringWebServiceState} from "@preservation-planning/monitoring/stores/web-service/preservation-planning-monitoring-web-service.state";
import {PreservationPlanningRoutingModule} from "@preservation-planning/preservation-planning-routing.module";
import {PreservationPlanningSipCollectionState} from "@preservation-planning/sip/stores/collection/preservation-planning-sip-collection.state";
import {PreservationPlanningSipCollectionStatusHistoryState} from "@preservation-planning/sip/stores/collection/status-history/preservation-planning-sip-collection-status-history.state";
import {PreservationPlanningSipDataFileState} from "@preservation-planning/sip/stores/data-file/preservation-planning-sip-data-file.state";
import {PreservationPlanningSipDataFileStatusHistoryState} from "@preservation-planning/sip/stores/data-file/status-history/preservation-planning-sip-data-file-status-history.state";
import {PreservationPlanningSipState} from "@preservation-planning/sip/stores/preservation-planning-sip.state";
import {PreservationPlanningSipStatusHistoryState} from "@preservation-planning/sip/stores/status-history/preservation-planning-sip-status-history.state";
import {PreservationPlanningOrderState} from "@preservation-planning/stores/order/preservation-planning-order.state";
import {PreservationPlanningOrderStatusHistoryState} from "@preservation-planning/stores/order/status-history/preservation-planning-order-status-history.state";
import {PreservationPlanningState} from "@preservation-planning/stores/preservation-planning.state";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SsrUtil} from "solidify-frontend";

const routables = [
  PreservationPlanningHomeRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    PreservationPlanningRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      PreservationPlanningState,
      PreservationPlanningMonitoringState,
      PreservationPlanningMonitoringWebServiceState,
      PreservationPlanningAipStatusState,
      PreservationPlanningJobState,
      PreservationPlanningJobExecutionState,
      PreservationPlanningJobExecutionReportState,
      PreservationPlanningSipState,
      PreservationPlanningSipStatusHistoryState,
      PreservationPlanningSipDataFileState,
      PreservationPlanningSipDataFileStatusHistoryState,
      PreservationPlanningSipCollectionState,
      PreservationPlanningSipCollectionStatusHistoryState,
      PreservationPlanningDipState,
      PreservationPlanningDipDataFileState,
      PreservationPlanningDipStatusHistoryState,
      PreservationPlanningDipDataFileStatusHistoryState,
      PreservationPlanningDipAipState,
      PreservationPlanningDepositState,
      PreservationPlanningDepositStatusHistoryState,
      PreservationPlanningOrderState,
      PreservationPlanningOrderStatusHistoryState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationPlanningModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.preservationPlanningModuleLoaded] = true;
    }
  }
}
