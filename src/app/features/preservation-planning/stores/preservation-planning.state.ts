/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  State,
  Store,
} from "@ngxs/store";
import {
  PreservationPlanningAipStatusState,
  PreservationPlanningAipStatusStateModel,
} from "@preservation-planning/aip-status/stores/preservation-planning-aip-status.state";
import {
  PreservationPlanningDepositState,
  PreservationPlanningDepositStateModel,
} from "@preservation-planning/deposit/stores/preservation-planning-deposit.state";
import {
  PreservationPlanningDipState,
  PreservationPlanningDipStateModel,
} from "@preservation-planning/dip/stores/preservation-planning-dip.state";
import {
  PreservationPlanningJobState,
  PreservationPlanningJobStateModel,
} from "@preservation-planning/job/stores/preservation-planning-job.state";
import {
  PreservationPlanningMonitoringState,
  PreservationPlanningMonitoringStateModel,
} from "@preservation-planning/monitoring/stores/preservation-planning-monitoring.state";
import {
  PreservationPlanningSipState,
  PreservationPlanningSipStateModel,
} from "@preservation-planning/sip/stores/preservation-planning-sip.state";
import {
  PreservationPlanningOrderState,
  PreservationPlanningOrderStateModel,
} from "@preservation-planning/stores/order/preservation-planning-order.state";
import {StateEnum} from "@shared/enums/state.enum";
import {BaseStateModel} from "solidify-frontend";

export interface PreservationPlanningStateModel extends BaseStateModel {
  [StateEnum.preservationPlanning_deposit]: PreservationPlanningDepositStateModel | undefined;
  [StateEnum.preservationPlanning_monitoring]: PreservationPlanningMonitoringStateModel | undefined;
  [StateEnum.preservationPlanning_job]: PreservationPlanningJobStateModel | undefined;
  [StateEnum.preservationPlanning_aipStatus]: PreservationPlanningAipStatusStateModel | undefined;
  [StateEnum.preservationPlanning_sip]: PreservationPlanningSipStateModel | undefined;
  [StateEnum.preservationPlanning_dip]: PreservationPlanningDipStateModel | undefined;
  [StateEnum.preservationPlanning_order]: PreservationPlanningOrderStateModel | undefined;
}

@Injectable()
@State<PreservationPlanningStateModel>({
  name: StateEnum.preservationPlanning,
  defaults: {
    isLoadingCounter: 0,
    [StateEnum.preservationPlanning_deposit]: undefined,
    [StateEnum.preservationPlanning_monitoring]: undefined,
    [StateEnum.preservationPlanning_job]: undefined,
    [StateEnum.preservationPlanning_aipStatus]: undefined,
    [StateEnum.preservationPlanning_sip]: undefined,
    [StateEnum.preservationPlanning_dip]: undefined,
    [StateEnum.preservationPlanning_order]: undefined,
  },
  children: [
    PreservationPlanningDepositState,
    PreservationPlanningMonitoringState,
    PreservationPlanningJobState,
    PreservationPlanningAipStatusState,
    PreservationPlanningSipState,
    PreservationPlanningDipState,
    PreservationPlanningOrderState,
  ],
})
export class PreservationPlanningState {
  constructor(protected readonly _store: Store) {
  }
}
