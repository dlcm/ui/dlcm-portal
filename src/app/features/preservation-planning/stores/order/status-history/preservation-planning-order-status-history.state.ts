/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-order-status-history.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {Order} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {PreservationPlanningOrderStateModel} from "@preservation-planning/stores/order/preservation-planning-order.state";
import {preservationPlanningOrderStatusHistoryNamespace} from "@preservation-planning/stores/order/status-history/preservation-planning-order-status-history.action";
import {StateEnum} from "@shared/enums/state.enum";
import {
  defaultStatusHistoryInitValue,
  StatusHistoryState,
  StatusHistoryStateModel,
} from "@shared/stores/status-history/status-history.state";
import {
  ApiService,
  NotificationService,
  StoreUtil,
} from "solidify-frontend";

export interface PreservationPlanningOrderStatusHistoryStateModel extends StatusHistoryStateModel<Order> {
}

@Injectable()
@State<PreservationPlanningOrderStatusHistoryStateModel>({
  name: StateEnum.preservationPlanning_order_statusHistory,
  defaults: {
    ...defaultStatusHistoryInitValue(),
  },
})
export class PreservationPlanningOrderStatusHistoryState extends StatusHistoryState<PreservationPlanningOrderStatusHistoryStateModel, Order> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: preservationPlanningOrderStatusHistoryNamespace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessOrders;
  }

  @Selector()
  static isLoading(state: PreservationPlanningOrderStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }
}
