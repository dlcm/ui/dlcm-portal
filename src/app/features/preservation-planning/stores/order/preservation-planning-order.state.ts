/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-order.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {Order} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  PreservationPlanningOrderAction,
  preservationPlanningOrderNamespace,
} from "@preservation-planning/stores/order/preservation-planning-order.action";
import {
  PreservationPlanningOrderStatusHistoryState,
  PreservationPlanningOrderStatusHistoryStateModel,
} from "@preservation-planning/stores/order/status-history/preservation-planning-order-status-history.state";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  AppRoutesEnum,
  OrderRoutesEnum,
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {
  ApiService,
  defaultResourceStateInitValue,
  NotificationService,
  OrderEnum,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
  Sort,
} from "solidify-frontend";

export interface PreservationPlanningOrderStateModel extends ResourceStateModel<Order> {
  [StateEnum.preservationPlanning_order_statusHistory]: PreservationPlanningOrderStatusHistoryStateModel;
}

@Injectable()
@State<PreservationPlanningOrderStateModel>({
  name: StateEnum.preservationPlanning_order,
  defaults: {
    ...defaultResourceStateInitValue(),
    [StateEnum.preservationPlanning_order_statusHistory]: {...defaultStatusHistoryInitValue()},
    queryParameters: new QueryParameters(environment.defaultEnumValuePageSizeOption, {field: "name", order: OrderEnum.ascending} as Sort),
  },
  children: [
    PreservationPlanningOrderStatusHistoryState,
  ],
})
export class PreservationPlanningOrderState extends ResourceState<PreservationPlanningOrderStateModel, Order> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationPlanningOrderNamespace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.accessOrders;
  }

  @Action(PreservationPlanningOrderAction.GoToOrder)
  goToOrder(ctx: SolidifyStateContext<PreservationPlanningOrderStateModel>, action: PreservationPlanningOrderAction.GoToOrder): void {
    ctx.dispatch(new Navigate([RoutesEnum.orderAllOrder + urlSeparator + OrderRoutesEnum.allOrderDetail + AppRoutesEnum.separator, action.orderId]));
  }

}
