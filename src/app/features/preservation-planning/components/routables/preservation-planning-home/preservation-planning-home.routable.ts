/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Injector,
} from "@angular/core";
import {RoutesEnum} from "@app/shared/enums/routes.enum";
import {AppState} from "@app/stores/app.state";
import {Enums} from "@enums";
import {TranslateService} from "@ngx-translate/core";
import {Store} from "@ngxs/store";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {PermissionUtil} from "@shared/utils/permission.util";
import {
  AbstractHomeRoutable,
  HomeTileModel,
  isEmptyString,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-home-routable",
  templateUrl: "./preservation-planning-home.routable.html",
  styleUrls: ["./preservation-planning-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningHomeRoutable extends AbstractHomeRoutable {
  protected _userRolesObs: Enums.UserApplicationRole.UserApplicationRoleEnum[] = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.userRoles);

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  _tiles: HomeTileModel[] = [
    {
      avatarIcon: IconNameEnum.deposit,
      titleToTranslate: LabelTranslateEnum.deposit,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("preservation.deposit.home.subtitle"),
      path: RoutesEnum.preservationPlanningDeposit,
      permission: ApplicationRolePermissionEnum.noPermission,
    },
    {
      avatarIcon: IconNameEnum.sip,
      titleToTranslate: LabelTranslateEnum.sip,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("preservation.sip.home.subtitle"),
      path: RoutesEnum.preservationPlanningSip,
      permission: ApplicationRolePermissionEnum.noPermission,
    },
    {
      avatarIcon: IconNameEnum.aip,
      titleToTranslate: LabelTranslateEnum.aip,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("preservation.aip.home.subtitle"),
      path: RoutesEnum.preservationPlanningAip,
      permission: ApplicationRolePermissionEnum.noPermission,
    },
    {
      avatarIcon: IconNameEnum.archivingStatus,
      titleToTranslate: LabelTranslateEnum.archivingStatus,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("preservation.aipStats.home.subtitle"),
      path: RoutesEnum.preservationPlanningAipStatuses,
      permission: ApplicationRolePermissionEnum.noPermission,
    },
    {
      avatarIcon: IconNameEnum.dip,
      titleToTranslate: LabelTranslateEnum.dip,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("preservation.dip.home.subtitle"),
      path: RoutesEnum.preservationPlanningDip,
      permission: ApplicationRolePermissionEnum.noPermission,
    },
    {
      avatarIcon: IconNameEnum.aipDownloaded,
      titleToTranslate: LabelTranslateEnum.aipDownloaded,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("preservation.aipDownloaded.home.subtitle"),
      path: RoutesEnum.preservationPlanningAipDownloaded,
      permission: ApplicationRolePermissionEnum.noPermission,
    },
    {
      avatarIcon: IconNameEnum.jobs,
      titleToTranslate: LabelTranslateEnum.jobs,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("preservation.jobs.home.subtitle"),
      path: RoutesEnum.preservationPlanningJob,
      permission: ApplicationRolePermissionEnum.noPermission,
    },
    {
      avatarIcon: IconNameEnum.monitoring,
      titleToTranslate: LabelTranslateEnum.monitoring,
      subtitleToTranslate: MARK_AS_TRANSLATABLE("preservation.monitoring.home.subtitle"),
      path: RoutesEnum.preservationPlanningMonitoring,
      permission: ApplicationRolePermissionEnum.rootPermission,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _translate: TranslateService,
              protected readonly _injector: Injector) {
    super(_store, _translate, _injector);
  }

  protected _displayTile(tile: HomeTileModel): boolean {
    return PermissionUtil.isUserHavePermission(true, tile.permission as ApplicationRolePermissionEnum, this._userRolesObs)
      && (isEmptyString(this.searchValueInUrl) || this._translate.instant(tile.titleToTranslate).toLowerCase().startsWith(this.searchValueInUrl));
  }
}
