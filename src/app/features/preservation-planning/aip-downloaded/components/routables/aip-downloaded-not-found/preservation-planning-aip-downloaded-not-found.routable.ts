/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-aip-downloaded-not-found.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
} from "@angular/core";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {RoutesEnum} from "@shared/enums/routes.enum";

@Component({
  selector: "dlcm-preservation-planning-aip-downloaded-not-found-routable",
  templateUrl: "./preservation-planning-aip-downloaded-not-found.routable.html",
  styleUrls: ["./preservation-planning-aip-downloaded-not-found.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningAipDownloadedNotFoundRoutable extends SharedAbstractRoutable {
  constructor(private readonly _store: Store,
              private readonly _route: ActivatedRoute,
              private readonly _router: Router) {
    super();
  }

  back(): void {
    this._navigate([RoutesEnum.preservationPlanningAipDownloaded]);
  }

  private _navigate(path: (string | number)[]): void {
    this._store.dispatch(new Navigate(path));
  }
}
