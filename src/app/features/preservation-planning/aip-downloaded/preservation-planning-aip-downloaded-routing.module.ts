/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-aip-downloaded-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {PreservationPlanningAipDownloadedNotFoundRoutable} from "@preservation-planning/aip-downloaded/components/routables/aip-downloaded-not-found/preservation-planning-aip-downloaded-not-found.routable";
import {callbackAipTabBreadcrumb} from "@preservation-planning/aip/preservation-planning-aip-routing.module";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  SharedAipRoutesEnum,
} from "@shared/enums/routes.enum";
import {SharedAipCollectionDetailRoutable} from "@shared/features/aip/components/routables/aip-collection-detail/shared-aip-collection-detail.routable";
import {SharedAipCollectionRoutable} from "@shared/features/aip/components/routables/aip-collection/shared-aip-collection.routable";
import {SharedAipDetailEditRoutable} from "@shared/features/aip/components/routables/aip-detail-edit/shared-aip-detail-edit.routable";
import {SharedAipFileDetailRoutable} from "@shared/features/aip/components/routables/aip-file-detail/shared-aip-file-detail.routable";
import {SharedAipFileRoutable} from "@shared/features/aip/components/routables/aip-file/shared-aip-file.routable";
import {SharedAipListRoutable} from "@shared/features/aip/components/routables/aip-list/shared-aip-list.routable";
import {SharedAipMetadataRoutable} from "@shared/features/aip/components/routables/aip-metadata/shared-aip-metadata.routable";
import {SharedAipTabsRoutable} from "@shared/features/aip/components/routables/aip-tabs/shared-aip-tabs.routable";
import {SharedAipSharedModule} from "@shared/features/aip/shared-aip-shared.module";
import {SharedAipState} from "@shared/features/aip/stores/shared-aip.state";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";

const routes: DlcmRoutes = [
  {
    path: SharedAipRoutesEnum.aipNotFound + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: PreservationPlanningAipDownloadedNotFoundRoutable,
  },
  // {
  // path: PreservationPlanningRoutesEnum.preservationPlanning + AppRoutesEnum.separator + PreservationPlanningRoutesEnum.aipDownloaded,
  // redirectTo: PreservationPlanningRoutesEnum.aipDownloaded + AppRoutesEnum.separator + "all",
  // pathMatch: "full",
  // },
  // {
  //   path: PreservationPlanningRoutesEnum.storagionNumber + AppRoutesEnum.separator + SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
  //   redirectTo: PreservationPlanningRoutesEnum.storagionNumber + AppRoutesEnum.separator + SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId + AppRoutesEnum.separator + PreservationPlanningRoutesEnum.aipMetadata,
  //   pathMatch: "full",
  {
    path: AppRoutesEnum.root,
    redirectTo: SharedAipRoutesEnum.aipTabAll,
    pathMatch: "full",
  },
  {
    path: SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    redirectTo: SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId + AppRoutesEnum.separator + SharedAipRoutesEnum.aipMetadata,
    pathMatch: "full",
  },
  // },
  {
    path: AppRoutesEnum.root,
    component: SharedAipTabsRoutable,
    data: {},
    children: [
      {
        path: SharedAipRoutesEnum.tab,
        component: SharedAipListRoutable,
        data: {
          breadcrumb: callbackAipTabBreadcrumb,
          noBreadcrumbLink: true,
        },
      },
    ],
  },
  {
    path: SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: SharedAipDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: SharedAipState.currentAipName,
    },
    children: [
      {
        path: SharedAipRoutesEnum.aipMetadata,
        component: SharedAipMetadataRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.metadata,
          noBreadcrumbLink: true,
        },
      },
      {
        path: SharedAipRoutesEnum.aipCollections,
        component: SharedAipCollectionRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.collection,
          noBreadcrumbLink: true,
        },
        children: [
          {
            path: AppRoutesEnum.paramId,
            component: SharedAipCollectionDetailRoutable,
            data: {
              breadcrumb: LabelTranslateEnum.detail,
            },
          },
        ],
      },
      {
        path: SharedAipRoutesEnum.aipFiles,
        component: SharedAipFileRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.files,
          noBreadcrumbLink: true,
        },
        children: [
          {
            path: AppRoutesEnum.paramId,
            component: SharedAipFileDetailRoutable,
            data: {
              breadcrumb: LabelTranslateEnum.detail,
            },
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [SharedAipSharedModule.forChild(routes)],
  exports: [SharedAipSharedModule],
})
export class PreservationPlanningAipDownloadedRoutingModule {
}
