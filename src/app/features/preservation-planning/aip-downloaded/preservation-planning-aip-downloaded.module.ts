/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-aip-downloaded.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {PreservationPlanningAipDownloadedNotFoundRoutable} from "@preservation-planning/aip-downloaded/components/routables/aip-downloaded-not-found/preservation-planning-aip-downloaded-not-found.routable";
import {PreservationPlanningAipDownloadedRoutingModule} from "@preservation-planning/aip-downloaded/preservation-planning-aip-downloaded-routing.module";

const routables = [
  PreservationPlanningAipDownloadedNotFoundRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    PreservationPlanningAipDownloadedRoutingModule,
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationPlanningAipDownloadedModule {
}
