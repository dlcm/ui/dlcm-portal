/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {
  AppRoutesEnum,
  PreservationPlanningRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {PreservationPlanningHomeRoutable} from "@preservation-planning/components/routables/preservation-planning-home/preservation-planning-home.routable";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {ApplicationRoleGuardService} from "@shared/guards/application-role-guard.service";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";
import {CombinedGuardService} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: PreservationPlanningHomeRoutable,
    data: {},
  },
  {
    path: PreservationPlanningRoutesEnum.deposit,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./deposit/preservation-planning-deposit.module").then(m => m.PreservationPlanningDepositModule),
    data: {
      breadcrumb: LabelTranslateEnum.deposit,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: PreservationPlanningRoutesEnum.monitoring,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./monitoring/preservation-planning-monitoring.module").then(m => m.PreservationPlanningMonitoringModule),
    data: {
      breadcrumb: LabelTranslateEnum.monitoring,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: PreservationPlanningRoutesEnum.aipStatuses,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./aip-status/preservation-planning-aip-status.module").then(m => m.PreservationPlanningAipStatusModule),
    data: {
      breadcrumb: LabelTranslateEnum.aipStatuses,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: PreservationPlanningRoutesEnum.job,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./job/preservation-planning-job.module").then(m => m.PreservationPlanningJobModule),
    data: {
      breadcrumb: LabelTranslateEnum.jobs,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: PreservationPlanningRoutesEnum.aip,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./aip/preservation-planning-aip.module").then(m => m.PreservationPlanningAipModule),
    data: {
      breadcrumb: LabelTranslateEnum.aip,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: PreservationPlanningRoutesEnum.sip,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./sip/preservation-planning-sip.module").then(m => m.PreservationPlanningSipModule),
    data: {
      breadcrumb: LabelTranslateEnum.sip,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: PreservationPlanningRoutesEnum.dip,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./dip/preservation-planning-dip.module").then(m => m.PreservationPlanningDipModule),
    data: {
      breadcrumb: LabelTranslateEnum.dip,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: PreservationPlanningRoutesEnum.aipDownloaded,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./aip-downloaded/preservation-planning-aip-downloaded.module").then(m => m.PreservationPlanningAipDownloadedModule),
    data: {
      breadcrumb: LabelTranslateEnum.aipDownloaded,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreservationPlanningRoutingModule {

}
