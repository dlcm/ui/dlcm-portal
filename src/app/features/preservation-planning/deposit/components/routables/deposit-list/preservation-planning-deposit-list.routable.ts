/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-deposit-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {appMemberOrganizationalUnitNameSpace} from "@app/stores/member-organizational-unit/app-member-organizational-unit.action";
import {AppMemberOrganizationalUnitState} from "@app/stores/member-organizational-unit/app-member-organizational-unit.state";
import {Enums} from "@enums";
import {
  Deposit,
  OrganizationalUnit,
  PreservationJob,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  PreservationPlanningDepositAction,
  preservationPlanningDepositActionNameSpace,
} from "@preservation-planning/deposit/stores/preservation-planning-deposit.action";
import {PreservationPlanningDepositStateModel} from "@preservation-planning/deposit/stores/preservation-planning-deposit.state";
import {preservationPlanningDepositStatusHistoryNamespace} from "@preservation-planning/deposit/stores/status-history/preservation-planning-deposit-status-history.action";
import {PreservationPlanningDepositStatusHistoryState} from "@preservation-planning/deposit/stores/status-history/preservation-planning-deposit-status-history.state";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {
  AbstractListRoutable,
  DataTableActions,
  DataTableFieldTypeEnum,
  OrderEnum,
  ResourceNameSpace,
  RouterExtensionService,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-planning-deposit-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./preservation-planning-deposit-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationPlanningDepositListRoutable extends AbstractListRoutable<Deposit, PreservationPlanningDepositStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string = LabelTranslateEnum.backToPreservationPlanning;
  readonly KEY_PARAM_NAME: keyof Deposit & string = undefined;

  appMemberOrganizationalUnitSort: Sort<OrganizationalUnit> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  appMemberOrganizationalUnitNameSpace: ResourceNameSpace = appMemberOrganizationalUnitNameSpace;
  appMemberOrganizationalUnitState: typeof AppMemberOrganizationalUnitState = AppMemberOrganizationalUnitState;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.preservationPlanning_deposit, preservationPlanningDepositActionNameSpace, _injector, {
      canCreate: false,
      historyState: PreservationPlanningDepositStatusHistoryState,
      historyStateAction: preservationPlanningDepositStatusHistoryNamespace,
      historyStatusEnumTranslate: Enums.Deposit.StatusEnumTranslate,
    }, StateEnum.preservationPlanning);
  }

  conditionDisplayEditButton(model: Deposit | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: Deposit | undefined): boolean {
    return false;
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "title" as any,
        header: LabelTranslateEnum.title,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "organizationalUnit.name" as any,
        header: LabelTranslateEnum.organizationalUnit,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        resourceNameSpace: this.appMemberOrganizationalUnitNameSpace,
        resourceState: this.appMemberOrganizationalUnitState as any,
        searchableSingleSelectSort: this.appMemberOrganizationalUnitSort,
        filterableField: "organizationalUnitId" as any,
        sortableField: "organizationalUnitId" as any,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "status" as any,
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Deposit.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
    ];
  }

  protected override _defineActions(): DataTableActions<PreservationJob>[] {
    return [
      {
        logo: IconNameEnum.download,
        callback: (model: Deposit) => this._download(model),
        placeholder: current => LabelTranslateEnum.download,
        displayOnCondition: (model: Deposit) => true,
      },
    ];
  }

  private _download(sip: Deposit): void {
    this._store.dispatch(new PreservationPlanningDepositAction.Download(sip.resId));
  }

  showDetail(deposit: Deposit): void {
    this._store.dispatch(new Navigate([AppRoutesEnum.deposit, deposit.organizationalUnitId, DepositRoutesEnum.detail, deposit.resId]));
  }
}
