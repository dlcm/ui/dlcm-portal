/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-planning-deposit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {Deposit} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {
  PreservationPlanningDepositAction,
  preservationPlanningDepositActionNameSpace,
} from "@preservation-planning/deposit/stores/preservation-planning-deposit.action";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {
  ApiService,
  defaultResourceStateInitValue,
  DownloadService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";
import {
  PreservationPlanningDepositStatusHistoryState,
  PreservationPlanningDepositStatusHistoryStateModel,
} from "./status-history/preservation-planning-deposit-status-history.state";

export interface PreservationPlanningDepositStateModel extends ResourceStateModel<Deposit> {
  [StateEnum.preservationPlanning_deposit_statusHistory]: PreservationPlanningDepositStatusHistoryStateModel;
  isLoadingDataFile: boolean;
}

@Injectable()
@State<PreservationPlanningDepositStateModel>({
  name: StateEnum.preservationPlanning_deposit,
  defaults: {
    ...defaultResourceStateInitValue(),
    [StateEnum.preservationPlanning_deposit_statusHistory]: {...defaultStatusHistoryInitValue()},
    isLoadingDataFile: false,
  },
  children: [
    PreservationPlanningDepositStatusHistoryState,
  ],
})
export class PreservationPlanningDepositState extends ResourceState<PreservationPlanningDepositStateModel, Deposit> {
  private readonly _DEPOSIT_FILE_DOWNLOAD_PREFIX: string = "deposit_";
  private readonly _ZIP_EXTENSION: string = ".zip";
  private readonly _FOLDER_QUERY_PARAM: string = "folder";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _httpClient: HttpClient,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationPlanningDepositActionNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.preIngestDeposits;
  }

  @Selector()
  static isLoading(state: PreservationPlanningDepositStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static currentTitle(state: PreservationPlanningDepositStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.title;
  }

  @Selector()
  static isLoadingWithDependency(state: PreservationPlanningDepositStateModel): boolean {
    return this.isLoading(state);
  }

  @Action(PreservationPlanningDepositAction.Download)
  download(ctx: SolidifyStateContext<PreservationPlanningDepositStateModel>, action: PreservationPlanningDepositAction.Download): void {
    let fileName = this._DEPOSIT_FILE_DOWNLOAD_PREFIX + action.id;
    let url = `${this._urlResource}/${action.id}/${ApiActionNameEnum.DL}`;
    if (isNotNullNorUndefined(action.fullFolderName)) {
      url = url + `?${this._FOLDER_QUERY_PARAM}=${action.fullFolderName}`;
      fileName = fileName + action.fullFolderName;
    }
    this._downloadService.download(url, fileName + this._ZIP_EXTENSION);
  }

}
