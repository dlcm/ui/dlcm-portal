/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-archive-acl.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminArchiveAclAction,
  adminArchiveAclActionNameSpace,
} from "@admin/archive-acl/stores/admin-archive-acl.action";
import {Injectable} from "@angular/core";
import {ApiActionNameEnum} from "@app/shared/enums/api-action-name.enum";
import {environment} from "@environments/environment";
import {ArchiveACL} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ArchiveAclDuaSignedFileUploadWrapperModel} from "@shared/models/archive-acl-dua-signed-file-upload-wrapper.model";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  DownloadService,
  ErrorHelper,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceStateModel,
  SolidifyFileUploadStatus,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

const ATTRIBUTE_SIGNED_DUA_FILE: keyof ArchiveACL = "signedDuaFile";

export interface AdminArchiveAclStateModel extends ResourceStateModel<ArchiveACL> {
}

@Injectable()
@State<AdminArchiveAclStateModel>({
  name: StateEnum.admin_archiveAcl,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
  children: [],
})
export class AdminArchiveAclState extends ResourceFileState<AdminArchiveAclStateModel, ArchiveACL> {
  private readonly _ARCHIVE_ID_KEY: string = "archiveId";
  private readonly _USER_ID_KEY: string = "userId";
  private readonly _ORGANIZATIONAL_UNIT_ID_KEY: string = "organizationalUnitId";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminArchiveAclActionNameSpace,
      apiPathGetAll: () => ApiEnum.adminArchiveAcl + "/" + ApiActionNameEnum.LIST_ALL_ARCHIVE_ACL,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminArchiveAclDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminArchiveAclDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminArchiveAcl,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.archive-acl.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.archive-acl.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.archive-acl.notification.resource.update"),
      downloadInMemory: false,
      resourceFileApiActionNameDownloadCustom: ApiActionNameEnum.DOWNLOAD_DUA,
      resourceFileApiActionNameDeleteCustom: ApiActionNameEnum.DELETE_DUA,
      customFileAttribute: ATTRIBUTE_SIGNED_DUA_FILE,
      customUploadUrlSuffix: action => ApiActionNameEnum.UPLOAD_DUA,
      addExtraFormDataDuringUpload: (formData, action) => {
        const fileUploadWrapper = action.fileUploadWrapper as ArchiveAclDuaSignedFileUploadWrapperModel;
        formData.append(this._ARCHIVE_ID_KEY, fileUploadWrapper.archiveId);
        formData.append(this._USER_ID_KEY, fileUploadWrapper.userId);
        formData.append(this._ORGANIZATIONAL_UNIT_ID_KEY, fileUploadWrapper.organizationalUnitId);
      },
    }, _downloadService, ResourceFileStateModeEnum.custom, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminArchiveAcl;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminArchiveAclStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminArchiveAclStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminArchiveAclStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.user.firstName + " " + state.current.user.lastName;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminArchiveAclStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminArchiveAclStateModel): boolean {
    return true;
  }

  protected override _internalCreate(ctx: SolidifyStateContext<AdminArchiveAclStateModel>, action: AdminArchiveAclAction.Create): any {
    const newDeposit = action.modelFormControlEvent?.model;
    const duaFileChange = newDeposit.duaFileChange;
    if (isNullOrUndefined(duaFileChange) || duaFileChange === SolidifyFileUploadStatus.DELETED || duaFileChange === SolidifyFileUploadStatus.UNCHANGED) {
      return super._internalCreate(ctx, action);
    }

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const actionUpload = new AdminArchiveAclAction.UploadFile(undefined, {
      archiveId: newDeposit.aipId,
      userId: newDeposit.user.resId,
      organizationalUnitId: newDeposit.organizationalUnit.resId,
      file: duaFileChange,
    } as ArchiveAclDuaSignedFileUploadWrapperModel);

    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(ctx, this._actions$, actionUpload,
      AdminArchiveAclAction.UploadFileSuccess,
      resultSuccess => {},
      AdminArchiveAclAction.UploadFileFail,
      resultFail => {
        ctx.dispatch(new AdminArchiveAclAction.CreateFail(action));
        throw new SolidifyStateError(this, resultFail.error);
      },
    ).pipe(
      map(result => (result as AdminArchiveAclAction.UploadFileSuccess)?.uploadBody),
      StoreUtil.catchValidationErrors(ctx, action.modelFormControlEvent, this._notificationService, this._optionsState.autoScrollToFirstValidationError) as any,
    );
  }

  @Action(AdminArchiveAclAction.CreateFromNotification)
  createFromNotification(ctx: SolidifyStateContext<AdminArchiveAclStateModel>, action: AdminArchiveAclAction.CreateFromNotification): Observable<ArchiveACL> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<undefined, ArchiveACL>(this._urlResource + urlSeparator + ApiActionNameEnum.CREATE_FROM_NOTIFICATION + urlSeparator + action.notification.resId)
      .pipe(
        tap(result => {
          ctx.dispatch(new AdminArchiveAclAction.CreateFromNotificationSuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminArchiveAclAction.CreateFromNotificationFail(action, error));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminArchiveAclAction.CreateFromNotificationSuccess)
  createFromNotificationSuccess(ctx: SolidifyStateContext<AdminArchiveAclStateModel>, action: AdminArchiveAclAction.CreateFromNotificationSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.archiveAcl,
    });
    ctx.dispatch(new Navigate([RoutesEnum.adminArchiveAclDetail, action.archiveAcl?.resId]));
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("archiveAcl.notification.createFromNotification.success"));
  }

  @Action(AdminArchiveAclAction.CreateFromNotificationFail)
  createFromNotificationFail(ctx: SolidifyStateContext<AdminArchiveAclStateModel>, action: AdminArchiveAclAction.CreateFromNotificationFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("archiveAcl.notification.createFromNotification.fail"), {errors: ErrorHelper.extractUserErrors(action.error)});
  }
}
