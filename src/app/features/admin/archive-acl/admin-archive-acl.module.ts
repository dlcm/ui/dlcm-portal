/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-archive-acl.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminArchiveAclRoutingModule} from "@admin/archive-acl/admin-archive-acl-routing.module";
import {AdminArchiveAclFormPresentational} from "@admin/archive-acl/components/presentationals/admin-archive-acl-form/admin-archive-acl-form.presentational";
import {AdminArchiveAclCreateRoutable} from "@admin/archive-acl/components/routables/admin-archive-acl-create/admin-archive-acl-create.routable";
import {AdminArchiveAclDetailEditRoutable} from "@admin/archive-acl/components/routables/admin-archive-acl-detail-edit/admin-archive-acl-detail-edit.routable";
import {AdminArchiveAclListRoutable} from "@admin/archive-acl/components/routables/admin-archive-acl-list/admin-archive-acl-list.routable";
import {AdminArchiveAclState} from "@admin/archive-acl/stores/admin-archive-acl.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminArchiveAclListRoutable,
  AdminArchiveAclDetailEditRoutable,
  AdminArchiveAclCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminArchiveAclFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminArchiveAclRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminArchiveAclState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminArchiveAclModule {
}
