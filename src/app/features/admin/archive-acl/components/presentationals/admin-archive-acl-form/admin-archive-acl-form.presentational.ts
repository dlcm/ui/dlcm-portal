/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-archive-acl-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminArchiveAclAction} from "@admin/archive-acl/stores/admin-archive-acl.action";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  OnInit,
} from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Archive,
  ArchiveACL,
  DataFile,
  User,
} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SecurityService} from "@shared/services/security.service";
import {sharedArchiveActionNameSpace} from "@shared/stores/archive/shared-archive.action";
import {
  PARAM_QUERY_SEARCH,
  SharedArchiveState,
} from "@shared/stores/archive/shared-archive.state";
import {sharedUserActionNameSpace} from "@shared/stores/user/shared-user.action";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {Observable} from "rxjs";
import {
  filter,
  tap,
} from "rxjs/operators";
import {
  AbstractFormPresentational,
  BreakpointService,
  DateUtil,
  isNullOrUndefined,
  isTruthyObject,
  OrderEnum,
  PropertyName,
  ResourceFileNameSpace,
  ResourceNameSpace,
  SolidifyDataFileModel,
  SolidifyFileUploadInputRequiredValidator,
  SolidifyValidator,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-archive-acl-form",
  templateUrl: "./admin-archive-acl-form.presentational.html",
  styleUrls: ["./admin-archive-acl-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminArchiveAclFormPresentational extends AbstractFormPresentational<ArchiveACL> implements OnInit {
  readonly DEFAULT_DURATION_IN_MONTH: number = 3;
  readonly classInputIgnored: string = environment.classInputIgnored;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  searchKey: string = PARAM_QUERY_SEARCH;

  sharedArchiveNameSpace: ResourceNameSpace = sharedArchiveActionNameSpace;
  sharedArchiveState: typeof SharedArchiveState = SharedArchiveState;

  sharedUserSort: Sort<User> = {
    field: "lastName",
    order: OrderEnum.ascending,
  };
  sharedUserNameSpace: ResourceNameSpace = sharedUserActionNameSpace;
  sharedUserState: typeof SharedUserState = SharedUserState;
  labelUserCallback: (user: User) => string = user => user.lastName + ", " + user.firstName;

  aipFormControl: AbstractControl;
  userFormControl: AbstractControl;

  adminArchiveAclActionNameSpace: ResourceFileNameSpace = AdminArchiveAclAction;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get isInCreationMode(): boolean {
    return isNullOrUndefined(this.model);
  }

  dataFileAdapter: (dataFile: DataFile) => SolidifyDataFileModel = DataFileHelper.dataFileAdapter;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly breakpointService: BreakpointService,
              public readonly securityService: SecurityService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.aipId]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.organizationalUnitId]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.user]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.expiration]: ["", [SolidifyValidator]],
      [this.formDefinition.fileChange]: [undefined, [SolidifyValidator]],
      [this.formDefinition.deleted]: [false, [SolidifyValidator]],
      [this.formDefinition.expired]: [false, [SolidifyValidator]],
    });
  }

  protected _bindFormTo(archiveAcl: ArchiveACL): void {
    this.form = this._fb.group({
      [this.formDefinition.aipId]: [archiveAcl.aipId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.organizationalUnitId]: [archiveAcl.organizationalUnit.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.user]: [archiveAcl.user.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.expiration]: [DateUtil.convertOffsetDateTimeIso8601ToDate(archiveAcl.expiration), [SolidifyValidator]],
      [this.formDefinition.fileChange]: [archiveAcl.signedDuaFile, [SolidifyValidator]],
      [this.formDefinition.deleted]: [archiveAcl.deleted, [SolidifyValidator]],
      [this.formDefinition.expired]: [archiveAcl.expired, [SolidifyValidator]],
    });
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.aipFormControl = this.form.get(this.formDefinition.aipId);
    this.userFormControl = this.form.get(this.formDefinition.user);

    this.subscribe(this._observableRemoveErrorField(this.aipFormControl));
    this.subscribe(this._observableRemoveErrorField(this.userFormControl));
  }

  private _observableRemoveErrorField(formControl: AbstractControl): Observable<string> {
    return formControl.valueChanges.pipe(
      filter(() => isTruthyObject(formControl.errors)),
      tap(() => {
        this._removeErrorOnAipAndUserField();
      }),
    );
  }

  private _removeErrorOnAipAndUserField(): void {
    this._removeErrorOnFormControl(this.aipFormControl);
    this._removeErrorOnFormControl(this.userFormControl);

    this._changeDetectorRef.detectChanges();
  }

  override _disableSpecificField(): void {
    this.form.get(this.formDefinition.deleted).disable();
    this.form.get(this.formDefinition.expired).disable();

    if (!this.isInCreationMode) {
      this.form.get(this.formDefinition.fileChange).disable();
      this.form.get(this.formDefinition.aipId).disable();
      this.form.get(this.formDefinition.user).disable();
      if (!this.securityService.isRootOrAdmin()) {
        this.form.get(this.formDefinition.expiration).disable();
      }
    }
  }

  private _removeErrorOnFormControl(formControl: AbstractControl): void {
    formControl.setErrors(null);
    formControl.markAsTouched();
    formControl.updateValueAndValidity();
  }

  protected _treatmentBeforeSubmit(archiveAcl: ArchiveACL): ArchiveACL {
    const result = {
      resId: archiveAcl.resId,
      aipId: this.form.get(this.formDefinition.aipId).value,
      organizationalUnit: {
        resId: this.form.get(this.formDefinition.organizationalUnitId).value,
      },
      user: {
        resId: this.form.get(this.formDefinition.user).value,
      },
      expiration: DateUtil.convertToOffsetDateTimeIso8601(this.form.get(this.formDefinition.expiration).value),
    } as ArchiveACL;
    if (this.isInCreationMode && this.isDuaRequired) {
      result.duaFileChange = this.form.get(this.formDefinition.fileChange).value;
    }
    return result;
  }

  archiveSelected(archive: Archive): void {
    if (isNullOrUndefined(archive)) {
      this.form.get(this.formDefinition.organizationalUnitId).setValue("");
    } else {
      this.form.get(this.formDefinition.organizationalUnitId).setValue(archive.organizationalUnitId);
    }
    this._computeIsDuaRequired(archive);
  }

  private _computeIsDuaRequired(archive: Archive | undefined): void {
    const isDataUsePolicyWithDUARequired = archive?.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.SIGNED_DUA;
    const formControlDuaFile = this.form.get(this.formDefinition.fileChange);
    if (isNullOrUndefined(formControlDuaFile)) {
      return;
    }
    if (isDataUsePolicyWithDUARequired) {
      formControlDuaFile.setValidators([SolidifyFileUploadInputRequiredValidator, SolidifyValidator]);
    } else {
      formControlDuaFile.setValidators([SolidifyValidator]);
    }
    formControlDuaFile.updateValueAndValidity();
  }

  get isDuaRequired(): boolean {
    return this.form.get(this.formDefinition.fileChange)?.hasValidator(SolidifyFileUploadInputRequiredValidator);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() aipId: string;
  @PropertyName() organizationalUnitId: string;
  @PropertyName() user: string;
  @PropertyName() expiration: string;
  @PropertyName() fileChange: string;
  @PropertyName() deleted: string;
  @PropertyName() expired: string;
}
