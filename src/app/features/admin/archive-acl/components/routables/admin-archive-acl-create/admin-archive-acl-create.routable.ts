/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-archive-acl-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminArchiveAclActionNameSpace} from "@admin/archive-acl/stores/admin-archive-acl.action";
import {
  AdminArchiveAclState,
  AdminArchiveAclStateModel,
} from "@admin/archive-acl/stores/admin-archive-acl.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {
  ArchiveACL,
  OrganizationalUnit,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedOrganizationalUnitState} from "@shared/stores/organizational-unit/shared-organizational-unit.state";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-archive-acl-create-routable",
  templateUrl: "./admin-archive-acl-create.routable.html",
  styleUrls: ["./admin-archive-acl-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminArchiveAclCreateRoutable extends AbstractCreateRoutable<ArchiveACL, AdminArchiveAclStateModel> {
  @Select(AdminArchiveAclState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminArchiveAclState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;
  listOrgUnitsObs: Observable<OrganizationalUnit[]> = MemoizedUtil.list(this._store, SharedOrganizationalUnitState);

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_archiveAcl, _injector, adminArchiveAclActionNameSpace, StateEnum.admin);
  }

}
