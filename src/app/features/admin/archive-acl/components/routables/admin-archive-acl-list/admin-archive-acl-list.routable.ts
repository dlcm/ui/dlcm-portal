/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-archive-acl-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminArchiveAclActionNameSpace} from "@admin/archive-acl/stores/admin-archive-acl.action";
import {AdminArchiveAclStateModel} from "@admin/archive-acl/stores/admin-archive-acl.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  Archive,
  ArchiveACL,
  OrganizationalUnit,
  User,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {sharedArchiveActionNameSpace} from "@shared/stores/archive/shared-archive.action";
import {
  PARAM_QUERY_SEARCH,
  SharedArchiveState,
} from "@shared/stores/archive/shared-archive.state";
import {sharedOrganizationalUnitActionNameSpace} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedOrganizationalUnitState} from "@shared/stores/organizational-unit/shared-organizational-unit.state";
import {sharedUserActionNameSpace} from "@shared/stores/user/shared-user.action";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  isFalse,
  OrderEnum,
  ResourceNameSpace,
  RouterExtensionService,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-archive-acl-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-archive-acl-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminArchiveAclListRoutable extends AbstractListRoutable<ArchiveACL, AdminArchiveAclStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof ArchiveACL & string = "aipId";

  sharedArchiveNameSpace: ResourceNameSpace = sharedArchiveActionNameSpace;
  sharedArchiveState: typeof SharedArchiveState = SharedArchiveState;

  sharedUserSort: Sort<User> = {
    field: "lastName",
    order: OrderEnum.ascending,
  };
  sharedUserNameSpace: ResourceNameSpace = sharedUserActionNameSpace;
  sharedUserState: typeof SharedUserState = SharedUserState;

  sharedOrganizationalUnitSort: Sort<OrganizationalUnit> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedOrganizationalUnitNameSpace: ResourceNameSpace = sharedOrganizationalUnitActionNameSpace;
  sharedOrganizationalUnitState: typeof SharedOrganizationalUnitState = SharedOrganizationalUnitState;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_archiveAcl, adminArchiveAclActionNameSpace, _injector, {
      canCreate: _securityService.isRootOrAdmin(),
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: ArchiveACL | undefined): boolean {
    return this._securityService.isStewardOfOrgUnit(model.organizationalUnit.resId, true) && isFalse(model.deleted);
  }

  conditionDisplayDeleteButton(model: ArchiveACL | undefined): boolean {
    return this._securityService.isStewardOfOrgUnit(model.organizationalUnit.resId, true) && isFalse(model.deleted);
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "aipId",
        header: LabelTranslateEnum.archive,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        resourceNameSpace: this.sharedArchiveNameSpace,
        resourceState: this.sharedArchiveState as any,
        resourceLabelCallback: (value: Archive) => value.title,
        resourceLabelKey: PARAM_QUERY_SEARCH,
        component: DataTableComponentHelper.get(DataTableComponentEnum.archive),
      },
      {
        field: "user.person.fullName" as any,
        header: LabelTranslateEnum.user,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        resourceNameSpace: this.sharedUserNameSpace,
        resourceState: this.sharedUserState as any,
        resourceLabelKey: "lastName",
        resourceLabelCallback: (value: User) => value.lastName + ", " + value.firstName,
        searchableSingleSelectSort: this.sharedUserSort,
        filterableField: "user.resId" as any,
        sortableField: "user.resId" as any,
      },
      {
        field: "organizationalUnit.name" as any,
        header: LabelTranslateEnum.organizationalUnit,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        resourceNameSpace: this.sharedOrganizationalUnitNameSpace,
        resourceState: this.sharedOrganizationalUnitState as any,
        resourceLabelKey: "name",
        searchableSingleSelectSort: this.sharedOrganizationalUnitSort,
        filterableField: "organizationalUnit.resId" as any,
        sortableField: "organizationalUnit.resId" as any,
      },
      {
        field: "expiration",
        header: LabelTranslateEnum.expiration,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "deleted" as any,
        header: LabelTranslateEnum.enabled,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
        invertedBooleanValue: true,
      },
    ];
  }
}
