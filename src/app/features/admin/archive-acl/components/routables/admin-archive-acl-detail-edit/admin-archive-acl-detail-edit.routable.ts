/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-archive-acl-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminArchiveAclActionNameSpace} from "@admin/archive-acl/stores/admin-archive-acl.action";
import {
  AdminArchiveAclState,
  AdminArchiveAclStateModel,
} from "@admin/archive-acl/stores/admin-archive-acl.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {ArchiveACL} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  filter,
  Observable,
} from "rxjs";
import {tap} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  FileVisualizerService,
  isFalse,
  isNotNullNorUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-archive-acl-detail-edit-routable",
  templateUrl: "./admin-archive-acl-detail-edit.routable.html",
  styleUrls: ["./admin-archive-acl-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminArchiveAclDetailEditRoutable extends AbstractDetailEditCommonRoutable<ArchiveACL, AdminArchiveAclStateModel> implements OnInit {
  @Select(AdminArchiveAclState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminArchiveAclState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  readonly KEY_PARAM_NAME: keyof ArchiveACL & string = "aipId";

  override deleteAvailable: boolean = false;
  override editAvailable: boolean = false;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _fileVisualizerService: FileVisualizerService,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_archiveAcl, _injector, adminArchiveAclActionNameSpace, StateEnum.admin);
  }

  _getSubResourceWithParentId(id: string): void {
  }

  protected get isDeletable(): boolean {
    const archive: ArchiveACL = MemoizedUtil.selectSnapshot(this._store, AdminArchiveAclState, state => state.current);
    if (isNotNullNorUndefined(archive)) {
      return isFalse(archive.deleted);
    }
    return false;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this.currentObs.pipe(
      filter(current => isNotNullNorUndefined(current) && current.resId === this._resId),
      tap(current => {
        const orgUnitId = current.organizationalUnit.resId;
        const isStewardOfOrgUnit = this._securityService.isStewardOfOrgUnit(orgUnitId, true);
        this.editAvailable = isStewardOfOrgUnit;
        this.deleteAvailable = isStewardOfOrgUnit;
        this._changeDetector.detectChanges();
      }),
    ));
  }
}
