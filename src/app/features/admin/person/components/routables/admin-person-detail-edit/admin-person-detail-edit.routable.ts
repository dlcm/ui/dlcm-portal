/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-person-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {InstitutionRole} from "@admin/models/institution-role.model";
import {OrganizationalUnitRole} from "@admin/models/organizational-unit-role.model";
import {
  AdminPersonAction,
  adminPersonActionNameSpace,
} from "@admin/person/stores/admin-person.action";
import {
  AdminPersonState,
  AdminPersonStateModel,
} from "@admin/person/stores/admin-person.state";
import {AdminPersonInstitutionsAction} from "@admin/person/stores/institutions/admin-people-institutions.action";
import {AdminPersonInstitutionsState} from "@admin/person/stores/institutions/admin-people-institutions.state";
import {AdminPersonInstitutionRoleAction} from "@admin/person/stores/person-institution-role/admin-person-institution-role.action";
import {AdminPersonInstitutionRoleState} from "@admin/person/stores/person-institution-role/admin-person-institution-role.state";
import {AdminPersonOrgUnitRoleAction} from "@admin/person/stores/person-orgunit-role/admin-person-orgunit-role.action";
import {AdminPersonOrgUnitRoleState} from "@admin/person/stores/person-orgunit-role/admin-person-orgunit-role.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {AppUserAction} from "@app/stores/user/app-user.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {
  Institution,
  Person,
  Role,
  User,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedPersonActionNameSpace} from "@shared/stores/person/shared-person.action";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {Observable} from "rxjs";
import {
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  isNotNullNorUndefined,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-person-detail-edit-routable",
  templateUrl: "./admin-person-detail-edit.routable.html",
  styleUrls: ["./admin-person-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminPersonDetailEditRoutable extends AbstractDetailEditCommonRoutable<Person, AdminPersonStateModel> implements OnInit {
  @Select(AdminPersonState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminPersonState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);
  selectedInstitutionsObs: Observable<Institution[]> = MemoizedUtil.selected(this._store, AdminPersonInstitutionsState);
  selectedOrgUnitRoleObs: Observable<OrganizationalUnitRole[]> = MemoizedUtil.selected(this._store, AdminPersonOrgUnitRoleState);
  selectedInstitutionRoleObs: Observable<InstitutionRole[]> = MemoizedUtil.selected(this._store, AdminPersonInstitutionRoleState);
  currentUserObs: Observable<User> = MemoizedUtil.current(this._store, AppUserState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedPersonActionNameSpace;

  readonly KEY_PARAM_NAME: keyof Person & string = "fullName";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_person, _injector, adminPersonActionNameSpace, StateEnum.admin);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this.currentUserObs.pipe(
      filter(user => isNotNullNorUndefined(user)),
      take(1),
      tap(currentUser => {
        this._refreshCurrentUserWhenUpdated(currentUser);
      }),
    ));
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new AdminPersonOrgUnitRoleAction.GetAll(id));
    this._store.dispatch(new AdminPersonInstitutionRoleAction.GetAll(id));
    this._store.dispatch(new AdminPersonInstitutionsAction.GetAll(id));
  }

  private _refreshCurrentUserWhenUpdated(currentUser: User): void {
    if (currentUser?.person?.resId === this._resId) {
      this.subscribe(this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonAction.UploadFileSuccess))
        .pipe(
          tap(result => {
            this._store.dispatch(new AppUserAction.GetCurrentUser());
          }),
        ));
      this.subscribe(this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonAction.DeleteFileSuccess))
        .pipe(
          tap(result => {
            this._store.dispatch(new AppUserAction.GetCurrentUser());
          }),
        ));
    }
  }
}
