/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-person-institution-role.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {InstitutionPersonRole} from "@admin/models/institution-person-role.model";
import {adminPersonInstitutionRoleActionNameSpace} from "@admin/person/stores/person-institution-role/admin-person-institution-role.action";
import {Injectable} from "@angular/core";
import {InstitutionRole} from "@app/features/admin/models/institution-role.model";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";

import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultRelation3TiersStateInitValue,
  NotificationService,
  QueryParameters,
  Relation3TiersForm,
  Relation3TiersState,
  Relation3TiersStateModel,
} from "solidify-frontend";

export const defaultAdminPersonInstitutionRoleStateModel: () => AdminPersonInstitutionRoleStateModel = () =>
  ({
    ...defaultRelation3TiersStateInitValue(),
    queryParameters: new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo),
  });

export interface AdminPersonInstitutionRoleStateModel extends Relation3TiersStateModel<InstitutionRole> {
}

@Injectable()
@State<AdminPersonInstitutionRoleStateModel>({
  name: StateEnum.admin_person_institutionRole,
  defaults: {
    ...defaultAdminPersonInstitutionRoleStateModel(),
  },
})
// PersonInstitutionController
export class AdminPersonInstitutionRoleState extends Relation3TiersState<AdminPersonInstitutionRoleStateModel, InstitutionRole, InstitutionPersonRole> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminPersonInstitutionRoleActionNameSpace,
      resourceName: ApiResourceNameEnum.INSTITUTION,
      updateGrandChildListActionName: ApiActionNameEnum.SET_ROLE,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPeople;
  }

  protected _convertResourceInForm(resource: InstitutionRole): Relation3TiersForm {
    return {id: resource.resId, listId: resource.roles.map(r => r.resId)};
  }
}
