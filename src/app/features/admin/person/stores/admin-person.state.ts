/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-person.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminPersonAction,
  adminPersonActionNameSpace,
} from "@admin/person/stores/admin-person.action";
import {AdminPersonInstitutionsAction} from "@admin/person/stores/institutions/admin-people-institutions.action";
import {
  AdminPersonInstitutionsState,
  AdminPersonInstitutionsStateModel,
} from "@admin/person/stores/institutions/admin-people-institutions.state";
import {
  AdminPersonInstitutionRoleState,
  AdminPersonInstitutionRoleStateModel,
  defaultAdminPersonInstitutionRoleStateModel,
} from "@admin/person/stores/person-institution-role/admin-person-institution-role.state";
import {AdminPersonOrgUnitRoleAction} from "@admin/person/stores/person-orgunit-role/admin-person-orgunit-role.action";
import {
  AdminPersonOrgUnitRoleState,
  AdminPersonOrgUnitRoleStateModel,
  defaultAdminPersonOrgUnitRoleStateModel,
} from "@admin/person/stores/person-orgunit-role/admin-person-orgunit-role.state";
import {Injectable} from "@angular/core";
import {AppAuthorizedInstitutionAction} from "@app/stores/authorized-institution/app-authorized-institution.action";
import {AppAuthorizedOrganizationalUnitAction} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {environment} from "@environments/environment";
import {Person} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  defaultAssociationStateInitValue,
  defaultResourceFileStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isArray,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  Relation3TiersForm,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";
import {AdminPersonInstitutionRoleAction} from "./person-institution-role/admin-person-institution-role.action";

export interface AdminPersonStateModel extends ResourceFileStateModel<Person> {
  admin_person_organizationalUnitRole?: AdminPersonOrgUnitRoleStateModel;
  admin_person_institutionRole?: AdminPersonInstitutionRoleStateModel;
  admin_person_institutions?: AdminPersonInstitutionsStateModel;
}

@Injectable()
@State<AdminPersonStateModel>({
  name: StateEnum.admin_person,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    admin_person_organizationalUnitRole: {...defaultAdminPersonOrgUnitRoleStateModel()},
    admin_person_institutionRole: {...defaultAdminPersonInstitutionRoleStateModel()},
    admin_person_institutions: {...defaultAssociationStateInitValue()},
  },
  children: [
    AdminPersonOrgUnitRoleState,
    AdminPersonInstitutionRoleState,
    AdminPersonInstitutionsState,
  ],
})
export class AdminPersonState extends ResourceFileState<AdminPersonStateModel, Person> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminPersonActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminPersonDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminPersonDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminPerson,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.person.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.person.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.person.notification.resource.update"),
      updateSubResourceDispatchMethod: DispatchMethodEnum.PARALLEL,
    }, _downloadService, ResourceFileStateModeEnum.avatar, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminPeople;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminPersonStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminPersonStateModel): boolean {
    return this.isLoading(state)
      || StoreUtil.isLoadingState(state.admin_person_organizationalUnitRole)
      || StoreUtil.isLoadingState(state.admin_person_institutionRole)
      || StoreUtil.isLoadingState(state.admin_person_institutions);
  }

  @Selector()
  static currentTitle(state: AdminPersonStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.fullName;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminPersonStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current)
      && !isNullOrUndefined(state.admin_person_organizationalUnitRole.selected)
      && !isNullOrUndefined(state.admin_person_institutionRole.selected)
      && !isNullOrUndefined(state.admin_person_institutions.selected);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminPersonStateModel): boolean {
    return true;
  }

  protected override _getListActionsUpdateSubResource(model: Person, action: AdminPersonAction.Create | AdminPersonAction.Update, ctx: SolidifyStateContext<AdminPersonStateModel>): ActionSubActionCompletionsWrapper[] {
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);

    const currentPersonId = MemoizedUtil.currentSnapshot(this._store, AppUserState)?.person?.resId;
    const personId = model.resId;
    const concernCurrentUser = currentPersonId === personId;

    const newOrgUnitRole = action.modelFormControlEvent.formControl.get("orgUnitRole").value as Relation3TiersForm[];
    newOrgUnitRole.forEach((elem) => {
      if (!isNullOrUndefined(elem.listId) && !isArray(elem.listId)) {
        elem.listId = [elem.listId];
      }
    });

    const newInstitutionRole = action.modelFormControlEvent.formControl.get("institutionRole").value as Relation3TiersForm[];
    newInstitutionRole.forEach((elem) => {
      if (!isNullOrUndefined(elem.listId) && !isArray(elem.listId)) {
        elem.listId = [elem.listId];
      }
    });

    const newInstitutions = action.modelFormControlEvent.model.institutions.map(o => o.resId);

    actions.push(...[
      {
        action: new AdminPersonOrgUnitRoleAction.Update(personId, newOrgUnitRole),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonOrgUnitRoleAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonOrgUnitRoleAction.UpdateFail)),
        ],
      },
      {
        action: new AdminPersonInstitutionRoleAction.Update(personId, newInstitutionRole),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonInstitutionRoleAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonInstitutionRoleAction.UpdateFail)),
        ],
      },
      {
        action: new AdminPersonInstitutionsAction.Update(personId, newInstitutions),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonInstitutionsAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminPersonInstitutionsAction.UpdateFail)),
        ],
      },
    ]);

    if (concernCurrentUser) {
      actions.push(...[
        {
          action: new AppAuthorizedOrganizationalUnitAction.GetAll(undefined, false, false),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllFail)),
          ],
        },
        {
          action: new AppAuthorizedInstitutionAction.GetAll(),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedInstitutionAction.GetAllSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedInstitutionAction.GetAllFail)),
          ],
        },
      ]);
    }

    return actions;
  }
}
