/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-dissemination-policy-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminDisseminationPolicyActionNameSpace} from "@admin/dissemination-policy/stores/admin-dissemination-policy.action";
import {
  AdminDisseminationPolicyState,
  AdminDisseminationPolicyStateModel,
} from "@admin/dissemination-policy/stores/admin-dissemination-policy.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  ViewChild,
} from "@angular/core";
import {DisseminationPolicy} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  AbstractFormPresentational,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-dissemination-policy-create-routable",
  templateUrl: "./admin-dissemination-policy-create.routable.html",
  styleUrls: ["./admin-dissemination-policy-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminDisseminationPolicyCreateRoutable extends AbstractCreateRoutable<DisseminationPolicy, AdminDisseminationPolicyStateModel> {
  @Select(AdminDisseminationPolicyState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminDisseminationPolicyState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;

  @ViewChild("formPresentational")
  readonly formPresentational: AbstractFormPresentational<DisseminationPolicy>;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_disseminationPolicy, _injector, adminDisseminationPolicyActionNameSpace, StateEnum.admin);
  }
}
