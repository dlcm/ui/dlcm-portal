/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-dissemination-policy-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminDisseminationPolicyActionNameSpace} from "@admin/dissemination-policy/stores/admin-dissemination-policy.action";
import {AdminDisseminationPolicyStateModel} from "@admin/dissemination-policy/stores/admin-dissemination-policy.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {DisseminationPolicy} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-dissemination-policy-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-dissemination-policy-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminDisseminationPolicyListRoutable extends AbstractListRoutable<DisseminationPolicy, AdminDisseminationPolicyStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof DisseminationPolicy & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_disseminationPolicy, adminDisseminationPolicyActionNameSpace, _injector, {
      canCreate: _securityService.isRoot(),
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: DisseminationPolicy | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  conditionDisplayDeleteButton(model: DisseminationPolicy | undefined): boolean {
    return this._securityService.isRoot();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "type",
        header: LabelTranslateEnum.type,
        type: DataTableFieldTypeEnum.singleSelect,
        filterEnum: Enums.DisseminationPolicy.TypeEnumTranslate,
        order: OrderEnum.none,
        translate: true,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }
}
