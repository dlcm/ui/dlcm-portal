/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-dissemination-policy-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {DisseminationPolicy} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  AbstractFormPresentational,
  FormValidationHelper,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorEmptyArray,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  PropertyName,
  SolidifyValidator,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-dissemination-policy-form",
  templateUrl: "./admin-dissemination-policy-form.presentational.html",
  styleUrls: ["./admin-dissemination-policy-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminDisseminationPolicyFormPresentational extends AbstractFormPresentational<DisseminationPolicy> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  fieldsParameters: string[] = [];
  formParameters: FormGroup;
  hasTypeChanged: boolean = false;

  get disseminationPolicyTypeEnumToTranslate(): typeof Enums.DisseminationPolicy.TypeEnumTranslate {
    return Enums.DisseminationPolicy.TypeEnumTranslate;
  }

  get disseminationPolicyDownloadFileNameEnumToTranslate(): typeof Enums.DisseminationPolicy.DownloadFileNameEnumTranslate {
    return Enums.DisseminationPolicy.DownloadFileNameEnumTranslate;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get stringUtil(): typeof StringUtil {
    return StringUtil;
  }

  getFormParameterControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.formParameters, key);
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    if (isNotNullNorUndefined(this.model)) {
      this.subscribe(this.form.get(this.formDefinition.type).valueChanges, (newValue) => {
        if (newValue !== this.model.type) {
          this.hasTypeChanged = true;
        } else {
          this.hasTypeChanged = false;
        }
        this._changeDetectorRef.detectChanges();
      });
    }
    this.subscribe(this.formParameters.valueChanges, () => {
      this.form.markAsDirty();
      this._changeDetectorRef.detectChanges();
    });
  }

  protected _bindFormTo(disseminationPolicies: DisseminationPolicy): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [disseminationPolicies.name, [SolidifyValidator]],
      [this.formDefinition.type]: [disseminationPolicies.type, [Validators.required, SolidifyValidator]],
      [this.formDefinition.prefix]: [disseminationPolicies.prefix, [SolidifyValidator]],
      [this.formDefinition.downloadFileName]: [disseminationPolicies.downloadFileName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.suffix]: [disseminationPolicies.suffix, [SolidifyValidator]],
    });
    this.formParameters = this._fb.group({});
    const parameters: MappingObject<string, string> = isNotNullNorUndefinedNorWhiteString(disseminationPolicies.parameters) ? JSON.parse(disseminationPolicies.parameters) : {};
    this.fieldsParameters = MappingObjectUtil.keys(parameters);
    this.fieldsParameters.forEach((param) => {
      this.formParameters.addControl(param, this._fb.control(MappingObjectUtil.get(parameters, param), [SolidifyValidator]));
    });
    this._updateFormParametersReadonly();
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.type]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.prefix]: ["", [SolidifyValidator]],
      [this.formDefinition.downloadFileName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.suffix]: ["", [SolidifyValidator]],
    });
    this.formParameters = this._fb.group({});
  }

  protected _treatmentBeforeSubmit(disseminationPolicies: DisseminationPolicy): DisseminationPolicy {
    if (isNotNullNorUndefined(this.model) && isNotNullNorUndefinedNorEmptyArray(this.fieldsParameters)) {
      const parameters = {} as MappingObject<string, string>;
      this.fieldsParameters.forEach((param) => {
        const paramValue = this.formParameters.get(param).value;
        MappingObjectUtil.set(parameters, param, paramValue);
      });
      disseminationPolicies.parameters = JSON.stringify(parameters);
    }
    return disseminationPolicies;
  }

  private _updateFormParametersReadonly(): void {
    if (isNullOrUndefined(this.formParameters)) {
      return;
    }
    if (this.readonly) {
      this.formParameters.disable({emitEvent: false});
    } else {
      this.formParameters.enable({emitEvent: false});
    }
  }

  override enterInEditMode(): void {
    super.enterInEditMode();
    this.formParameters?.enable({emitEvent: false});
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() type: string;
  @PropertyName() prefix: string;
  @PropertyName() downloadFileName: string;
  @PropertyName() suffix: string;
}
