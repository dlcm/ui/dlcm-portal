/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-dissemination-policy.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminDisseminationPolicyRoutingModule} from "@admin/dissemination-policy/admin-dissemination-policy-routing.module";
import {AdminDisseminationPolicyFormPresentational} from "@admin/dissemination-policy/components/presentationals/admin-dissemination-policy-form/admin-dissemination-policy-form.presentational";
import {AdminDisseminationPolicyCreateRoutable} from "@admin/dissemination-policy/components/routables/admin-dissemination-policy-create/admin-dissemination-policy-create.routable";
import {AdminDisseminationPolicyDetailEditRoutable} from "@admin/dissemination-policy/components/routables/admin-dissemination-policy-detail-edit/admin-dissemination-policy-detail-edit.routable";
import {AdminDisseminationPolicyListRoutable} from "@admin/dissemination-policy/components/routables/admin-dissemination-policy-list/admin-dissemination-policy-list.routable";
import {AdminDisseminationPolicyState} from "@admin/dissemination-policy/stores/admin-dissemination-policy.state";
import {NgModule} from "@angular/core";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {SharedModule} from "@shared/shared.module";

const routables = [
  AdminDisseminationPolicyListRoutable,
  AdminDisseminationPolicyDetailEditRoutable,
  AdminDisseminationPolicyCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminDisseminationPolicyFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminDisseminationPolicyRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminDisseminationPolicyState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminDisseminationPolicyModule {
}
