/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {AdminHomeRoutable} from "@app/features/admin/components/routables/admin-home/admin-home.routable";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {ApplicationRoleGuardService} from "@app/shared/guards/application-role-guard.service";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";
import {
  CombinedGuardService,
  labelSolidifyGlobalBanner,
  labelSolidifyOaiPmh,
  labelSolidifySearch,
} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminHomeRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.license,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./license/admin-license.module").then(m => m.AdminLicenseModule),
    data: {
      breadcrumb: LabelTranslateEnum.licenses,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.globalBanner,
    // @ts-ignore Dynamic import
    loadChildren: () => import("solidify-frontend").then(m => m.AdminGlobalBannerModule),
    data: {
      breadcrumb: labelSolidifyGlobalBanner.globalBannerAdminBreadcrumb,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.institution,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./institution/admin-institution.module").then(m => m.AdminInstitutionModule),
    data: {
      breadcrumb: LabelTranslateEnum.institutions,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.subjectArea,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./subject-area/admin-subject-area.module").then(m => m.AdminSubjectAreaModule),
    data: {
      breadcrumb: LabelTranslateEnum.subjectAreas,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.fundingAgencies,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./funding-agencies/admin-funding-agencies.module").then(m => m.AdminFundingAgenciesModule),
    data: {
      breadcrumb: LabelTranslateEnum.fundingAgencies,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.indexFieldAlias,
    // @ts-ignore Dynamic import
    loadChildren: () => import("solidify-frontend").then(m => m.AdminIndexFieldAliasModule),
    data: {
      breadcrumb: labelSolidifySearch.searchIndexFieldAliasBreadcrumb,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.archiveAcl,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./archive-acl/admin-archive-acl.module").then(m => m.AdminArchiveAclModule),
    data: {
      breadcrumb: LabelTranslateEnum.archiveAcl,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.archiveType,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./archive-type/admin-archive-type.module").then(m => m.AdminArchiveTypeModule),
    data: {
      breadcrumb: LabelTranslateEnum.archiveType,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.oaiMetadataPrefix,
    // @ts-ignore Dynamic import
    loadChildren: () => import("solidify-frontend").then(m => m.AdminOaiMetadataPrefixModule),
    data: {
      breadcrumb: labelSolidifyOaiPmh.oaiPmhOaiMetadataPrefixBreadcrumb,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.oaiSet,
    // @ts-ignore Dynamic import
    loadChildren: () => import("solidify-frontend").then(m => m.AdminOaiSetModule),
    data: {
      breadcrumb: labelSolidifyOaiPmh.oaiPmhOaiSetBreadcrumb,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.organizationalUnit,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./organizational-unit/admin-organizational-unit.module").then(m => m.AdminOrganizationalUnitModule),
    data: {
      breadcrumb: LabelTranslateEnum.organizationalUnits,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.submissionAgreement,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./submission-agreement/admin-submission-agreement.module").then(m => m.AdminSubmissionAgreementModule),
    data: {
      breadcrumb: LabelTranslateEnum.submissionAgreements,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.submissionAgreementUser,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./submission-agreement-user/admin-submission-agreement-user.module").then(m => m.AdminSubmissionAgreementUserModule),
    data: {
      breadcrumb: LabelTranslateEnum.submissionAgreementsUser,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.submissionPolicy,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./submission-policy/admin-submission-policy.module").then(m => m.AdminSubmissionPolicyModule),
    data: {
      breadcrumb: LabelTranslateEnum.submissionPolicies,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.person,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./person/admin-person.module").then(m => m.AdminPersonModule),
    data: {
      breadcrumb: LabelTranslateEnum.people,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.preservationPolicy,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./preservation-policy/admin-preservation-policy.module").then(m => m.AdminPreservationPolicyModule),
    data: {
      breadcrumb: LabelTranslateEnum.preservationPolicies,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.disseminationPolicy,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./dissemination-policy/admin-dissemination-policy.module").then(m => m.AdminDisseminationPolicyModule),
    data: {
      breadcrumb: LabelTranslateEnum.disseminationPolicies,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.role,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./role/admin-role.module").then(m => m.AdminRoleModule),
    data: {
      breadcrumb: LabelTranslateEnum.roles,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.notification,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./notification/admin-notification.module").then(m => m.AdminNotificationModule),
    data: {
      breadcrumb: LabelTranslateEnum.notifications,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.user,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./user/admin-user.module").then(m => m.AdminUserModule),
    data: {
      breadcrumb: LabelTranslateEnum.users,
      permission: ApplicationRolePermissionEnum.rootPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.language,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./language/admin-language.module").then(m => m.AdminLanguageModule),
    data: {
      breadcrumb: LabelTranslateEnum.languages,
      permission: ApplicationRolePermissionEnum.rootPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.metadataType,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./metadata-type/admin-metadata-type.module").then(m => m.AdminMetadataTypeModule),
    data: {
      breadcrumb: LabelTranslateEnum.metadataTypes,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AdminRoutesEnum.scheduledTask,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./scheduled-task/admin-scheduled-task.module").then(m => m.AdminScheduledTaskModule),
    data: {
      breadcrumb: LabelTranslateEnum.scheduledTask,
      permission: ApplicationRolePermissionEnum.rootPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {
}
