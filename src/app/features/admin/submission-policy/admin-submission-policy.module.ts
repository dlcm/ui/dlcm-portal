/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-policy.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminSubmissionPolicyRoutingModule} from "@admin/submission-policy/admin-submission-policy-routing.module";
import {AdminSubmissionPolicyFormPresentational} from "@admin/submission-policy/components/presentationals/admin-submission-policy-form/admin-submission-policy-form.presentational";
import {AdminSubmissionPolicyCreateRoutable} from "@admin/submission-policy/components/routables/admin-submission-policy-create/admin-submission-policy-create.routable";
import {AdminSubmissionPolicyDetailEditRoutable} from "@admin/submission-policy/components/routables/admin-submission-policy-detail-edit/admin-submission-policy-detail-edit.routable";
import {AdminSubmissionPolicyListRoutable} from "@admin/submission-policy/components/routables/admin-submission-policy-list/admin-submission-policy-list.routable";
import {AdminSubmissionPolicyState} from "@admin/submission-policy/stores/admin-submission-policy.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminSubmissionPolicyListRoutable,
  AdminSubmissionPolicyDetailEditRoutable,
  AdminSubmissionPolicyCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminSubmissionPolicyFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminSubmissionPolicyRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminSubmissionPolicyState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminSubmissionPolicyModule {
}
