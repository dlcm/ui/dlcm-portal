/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-policy.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {SubmissionPolicy} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedSubmissionAgreementAction} from "@shared/stores/submission-agreement/shared-submission-agreement.action";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";
import {
  AdminSubmissionPolicyAction,
  adminSubmissionPolicyActionNameSpace,
} from "./admin-submission-policy.action";

export interface AdminSubmissionPolicyStateModel extends ResourceStateModel<SubmissionPolicy> {
}

@Injectable()
@State<AdminSubmissionPolicyStateModel>({
  name: StateEnum.admin_submissionPolicy,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class AdminSubmissionPolicyState extends ResourceState<AdminSubmissionPolicyStateModel, SubmissionPolicy> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminSubmissionPolicyActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminSubmissionPolicyDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminSubmissionPolicyDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminSubmissionPolicy,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.submissionPolicy.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.submissionPolicy.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.submissionPolicy.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminSubmissionPolicies;
  }

  @Selector()
  static isLoading(state: AdminSubmissionPolicyStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminSubmissionPolicyStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminSubmissionPolicyStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminSubmissionPolicyStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminSubmissionPolicyStateModel): boolean {
    return true;
  }

  @Action(AdminSubmissionPolicyAction.LoadResource)
  loadResource(ctx: SolidifyStateContext<AdminSubmissionPolicyStateModel>, action: AdminSubmissionPolicyAction.LoadResource): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new SharedSubmissionAgreementAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedSubmissionAgreementAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedSubmissionAgreementAction.GetAllFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new AdminSubmissionPolicyAction.LoadResourceSuccess(action));
        } else {
          ctx.dispatch(new AdminSubmissionPolicyAction.LoadResourceFail(action));
        }
        return result.success;
      }));
  }
}
