/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-policy-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminSubmissionPolicyActionNameSpace} from "@admin/submission-policy/stores/admin-submission-policy.action";
import {
  AdminSubmissionPolicyState,
  AdminSubmissionPolicyStateModel,
} from "@admin/submission-policy/stores/admin-submission-policy.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  SubmissionAgreement,
  SubmissionPolicy,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedSubmissionAgreementState} from "@shared/stores/submission-agreement/shared-submission-agreement.state";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-submission-policy-detail-edit-routable",
  templateUrl: "./admin-submission-policy-detail-edit.routable.html",
  styleUrls: ["./admin-submission-policy-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminSubmissionPolicyDetailEditRoutable extends AbstractDetailEditCommonRoutable<SubmissionPolicy, AdminSubmissionPolicyStateModel> {
  @Select(AdminSubmissionPolicyState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminSubmissionPolicyState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  listSubmissionAgreementsObs: Observable<SubmissionAgreement[]> = MemoizedUtil.list(this._store, SharedSubmissionAgreementState);

  readonly KEY_PARAM_NAME: keyof SubmissionPolicy & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_submissionPolicy, _injector, adminSubmissionPolicyActionNameSpace, StateEnum.admin);
    this.editAvailable = this._securityService.isRootOrAdmin();
    this.deleteAvailable = this._securityService.isRootOrAdmin();
  }

  _getSubResourceWithParentId(id: string): void {
  }
}
