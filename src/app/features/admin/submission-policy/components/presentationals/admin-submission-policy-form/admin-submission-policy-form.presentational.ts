/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-policy-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  SubmissionAgreement,
  SubmissionPolicy,
} from "@models";
import {Store} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  AbstractFormPresentational,
  BaseResource,
  isNullOrUndefinedOrWhiteString,
  KeyValue,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-submission-policy-form",
  templateUrl: "./admin-submission-policy-form.presentational.html",
  styleUrls: ["./admin-submission-policy-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminSubmissionPolicyFormPresentational extends AbstractFormPresentational<SubmissionPolicy> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  submissionAgreementTypes: KeyValue[] = Enums.SubmissionAgreementType.SubmissionAgreementTypeEnumTranslate;

  @Input()
  listSubmissionAgreements: SubmissionAgreement[];

  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this.editObs.pipe(
      tap(isEdit => {
        this._disableSubmissionAgreementWhenTypeIsWithout();
      }),
    ));

    this.subscribe(this._observeDisableSubmissionAgreementWhenTypeIsWithout());
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.submissionApproval]: [false, [SolidifyValidator]],
      [this.formDefinition.timeToKeep]: [0, [SolidifyValidator]],
      [this.formDefinition.submissionAgreementType]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.submissionAgreement]: [[], [Validators.required, SolidifyValidator]],
    });
  }

  protected _bindFormTo(submissionPolicy: SubmissionPolicy): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [submissionPolicy.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.submissionApproval]: [submissionPolicy.submissionApproval, [SolidifyValidator]],
      [this.formDefinition.timeToKeep]: [submissionPolicy.timeToKeep, [SolidifyValidator]],
      [this.formDefinition.submissionAgreementType]: [submissionPolicy.submissionAgreementType, [Validators.required, SolidifyValidator]],
      [this.formDefinition.submissionAgreement]: [submissionPolicy?.submissionAgreement?.resId, [Validators.required, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(submissionPolicy: SubmissionPolicy): SubmissionPolicy {
    const submissionAgreementResId: string = submissionPolicy.submissionAgreement as any;
    if (isNullOrUndefinedOrWhiteString(submissionAgreementResId)) {
      submissionPolicy.submissionAgreement = null;
    } else {
      submissionPolicy.submissionAgreement = {
        resId: submissionAgreementResId,
      } as BaseResource & any;
    }
    return submissionPolicy;
  }

  protected override _disableSpecificField(): void {
    super._disableSpecificField();
    this._disableSubmissionAgreementWhenTypeIsWithout();
  }

  private _observeDisableSubmissionAgreementWhenTypeIsWithout(): Observable<string> {
    this._disableSubmissionAgreementWhenTypeIsWithout();

    return this.form.get(this.formDefinition.submissionAgreementType).valueChanges.pipe(
      distinctUntilChanged(),
      tap(() => this._disableSubmissionAgreementWhenTypeIsWithout()),
    );
  }

  private _disableSubmissionAgreementWhenTypeIsWithout(): void {
    const fcSubmissionAgreement = this.form.get(this.formDefinition.submissionAgreement) as FormControl<string>;
    const submissionType: Enums.SubmissionAgreementType.SubmissionAgreementTypeEnum = this.form.get(this.formDefinition.submissionAgreementType).value;

    if (this.readonly) {
      if (fcSubmissionAgreement.enabled) {
        fcSubmissionAgreement.disable();
      }
      return;
    }
    if (submissionType === Enums.SubmissionAgreementType.SubmissionAgreementTypeEnum.WITHOUT) {
      if (fcSubmissionAgreement.enabled) {
        fcSubmissionAgreement.disable();
        fcSubmissionAgreement.setValue(undefined);
      }
      fcSubmissionAgreement.setValidators([SolidifyValidator]);
    } else {
      if (fcSubmissionAgreement.disabled) {
        fcSubmissionAgreement.enable();
      }
      fcSubmissionAgreement.setValidators([Validators.required, SolidifyValidator]);
    }
  }

  navigateToAgreementDetail($event: MouseEvent, agreementId: string): void {
    $event.stopPropagation();
    this.navigate([RoutesEnum.adminSubmissionAgreementDetail, agreementId]);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() submissionApproval: string;
  @PropertyName() timeToKeep: string;
  @PropertyName() submissionAgreementType: string;
  @PropertyName() submissionAgreement: string;
}
