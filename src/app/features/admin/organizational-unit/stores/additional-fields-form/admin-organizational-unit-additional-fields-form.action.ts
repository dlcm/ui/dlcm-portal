/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-additional-fields-form.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdditionalFieldsForm} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  CompositionAction,
  CompositionNameSpace,
  QueryParameters,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.admin_organizationalUnit_additionalFieldsForm;

export namespace AdminOrganizationalUnitAdditionalFieldsFormAction {
  @TypeDefaultAction(state)
  export class GetAll extends CompositionAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends CompositionAction.GetAllSuccess<AdditionalFieldsForm> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends CompositionAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends CompositionAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends CompositionAction.GetByIdSuccess<AdditionalFieldsForm> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends CompositionAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends CompositionAction.Update<AdditionalFieldsForm> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends CompositionAction.UpdateSuccess<AdditionalFieldsForm> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends CompositionAction.UpdateFail<AdditionalFieldsForm> {
  }

  @TypeDefaultAction(state)
  export class Create extends CompositionAction.Create<AdditionalFieldsForm> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends CompositionAction.CreateSuccess<AdditionalFieldsForm> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends CompositionAction.CreateFail<AdditionalFieldsForm> {
  }

  @TypeDefaultAction(state)
  export class Delete extends CompositionAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends CompositionAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends CompositionAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends CompositionAction.Clean {
  }

  export class ChangeQueryParameters extends CompositionAction.ChangeQueryParameters {
    static readonly type: string = `[${state}] Change Query Parameters`;

    constructor(public parentId: string, public queryParameters: QueryParameters, public keepCurrentContext: boolean = false, public getAllAfterChange: boolean = true) {
      super(parentId, queryParameters, keepCurrentContext, getAllAfterChange);
    }
  }

  export class GetCurrentMetadataForm extends BaseAction {
    static readonly type: string = `[${state}] Get Current Metadata Form`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class GetCurrentMetadataFormSuccess extends BaseSubActionSuccess<GetCurrentMetadataForm> {
    static readonly type: string = `[${state}] Get Current Metadata Form Success`;

    constructor(public parentAction: GetCurrentMetadataForm, public currentForm: AdditionalFieldsForm) {
      super(parentAction);
    }
  }

  export class GetCurrentMetadataFormFail extends BaseSubActionFail<GetCurrentMetadataForm> {
    static readonly type: string = `[${state}] Get Current Metadata Form Fail`;
  }
}

export const adminOrganizationalUnitAdditionalFieldsFormActionNameSpace: CompositionNameSpace = AdminOrganizationalUnitAdditionalFieldsFormAction;
