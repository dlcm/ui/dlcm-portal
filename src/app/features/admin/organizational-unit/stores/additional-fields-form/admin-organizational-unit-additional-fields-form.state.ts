/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-additional-fields-form.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminOrganizationalUnitAdditionalFieldsFormAction,
  adminOrganizationalUnitAdditionalFieldsFormActionNameSpace,
} from "@admin/organizational-unit/stores/additional-fields-form/admin-organizational-unit-additional-fields-form.action";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {AdditionalFieldsForm} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  OverrideDefaultAction,
  QueryParameters,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";
import GetCurrentMetadataFormFail = AdminOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormFail;
import GetCurrentMetadataFormSuccess = AdminOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormSuccess;

export const defaultAdminOrganizationalUnitAdditionalFieldsFormStateModel: () => AdminOrganizationalUnitAdditionalFieldsFormStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    queryParameters: new QueryParameters(20),
    loaded: false,
  });

export interface AdminOrganizationalUnitAdditionalFieldsFormStateModel extends CompositionStateModel<AdditionalFieldsForm> {
  loaded: boolean;
}

@Injectable()
@State<AdminOrganizationalUnitAdditionalFieldsFormStateModel>({
  name: StateEnum.admin_organizationalUnit_additionalFieldsForm,
  defaults: {
    ...defaultAdminOrganizationalUnitAdditionalFieldsFormStateModel(),
  },
})
export class AdminOrganizationalUnitAdditionalFieldsFormState extends CompositionState<AdminOrganizationalUnitAdditionalFieldsFormStateModel, AdditionalFieldsForm> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminOrganizationalUnitAdditionalFieldsFormActionNameSpace,
      resourceName: ApiResourceNameEnum.ADDITIONAL_FIELDS_FORM,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOrganizationalUnits;
  }

  @Action(AdminOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataForm)
  getCurrentMetadataForm(ctx: SolidifyStateContext<AdminOrganizationalUnitAdditionalFieldsFormStateModel>, action: AdminOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataForm): Observable<AdditionalFieldsForm> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.get<AdditionalFieldsForm>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${ApiResourceNameEnum.CURRENT_VERSION}`)
      .pipe(
        tap(result => ctx.dispatch(new GetCurrentMetadataFormSuccess(action, result))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new GetCurrentMetadataFormFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormSuccess)
  getCurrentMetadataFormSuccess(ctx: SolidifyStateContext<AdminOrganizationalUnitAdditionalFieldsFormStateModel>, action: AdminOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.currentForm,
      loaded: true,
    });
  }

  @Action(AdminOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormFail)
  getCurrentMetadataFormFail(ctx: SolidifyStateContext<AdminOrganizationalUnitAdditionalFieldsFormStateModel>, action: AdminOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @OverrideDefaultAction()
  @Action(AdminOrganizationalUnitAdditionalFieldsFormAction.CreateFail)
  createFail(ctx: SolidifyStateContext<AdminOrganizationalUnitAdditionalFieldsFormStateModel>, action: AdminOrganizationalUnitAdditionalFieldsFormAction.CreateFail): void {
    super.createFail(ctx, action);
    this._notificationService.showError(MARK_AS_TRANSLATABLE("admin.organizationalUnit.additionalFieldsForm.notification.unableToCreate"));
  }
}
