/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminOrganizationalUnitAdditionalFieldsFormState,
  AdminOrganizationalUnitAdditionalFieldsFormStateModel,
  defaultAdminOrganizationalUnitAdditionalFieldsFormStateModel,
} from "@admin/organizational-unit/stores/additional-fields-form/admin-organizational-unit-additional-fields-form.state";
import {
  AdminOrganizationalUnitAction,
  adminOrganizationalUnitActionNameSpace,
} from "@admin/organizational-unit/stores/admin-organizational-unit.action";
import {AdminOrganizationalUnitDisseminationPolicyAction} from "@admin/organizational-unit/stores/dissemination-policy/admin-organizational-unit-dissemination-policy.action";
import {
  AdminOrganizationalUnitDisseminationPolicyState,
  AdminOrganizationalUnitDisseminationPolicyStateModel,
} from "@admin/organizational-unit/stores/dissemination-policy/admin-organizational-unit-dissemination-policy.state";
import {AdminOrganizationalUnitFundingAgencyAction} from "@admin/organizational-unit/stores/funding-agency/admin-organizational-unit-funding-agency.action";
import {
  AdminOrganizationalUnitFundingAgencyState,
  AdminOrganizationalUnitFundingAgencyStateModel,
} from "@admin/organizational-unit/stores/funding-agency/admin-organizational-unit-funding-agency.state";
import {AdminOrganizationalUnitInstitutionAction} from "@admin/organizational-unit/stores/institution/admin-organizational-unit-institution.action";
import {
  AdminOrganizationalUnitInstitutionState,
  AdminOrganizationalUnitInstitutionStateModel,
} from "@admin/organizational-unit/stores/institution/admin-organizational-unit-institution.state";
import {
  AdminOrganizationalUnitPersonInheritedRoleState,
  AdminOrganizationalUnitPersonInheritedRoleStateModel,
  defaultAdminOrganizationalUnitPersonInheritedRoleStateModel,
} from "@admin/organizational-unit/stores/organizational-unit-person-inherited-role/admin-organizational-unit-person-inherited-role.state";
import {AdminOrganizationalUnitPersonRoleAction} from "@admin/organizational-unit/stores/organizational-unit-person-role/admin-organizational-unit-person-role.action";
import {
  AdminOrganizationalUnitPersonRoleState,
  AdminOrganizationalUnitPersonRoleStateModel,
  defaultAdminOrganizationalUnitPersonRoleStateModel,
} from "@admin/organizational-unit/stores/organizational-unit-person-role/admin-organizational-unit-person-role.state";
import {AdminOrganizationalUnitPreservationPolicyAction} from "@admin/organizational-unit/stores/preservation-policy/admin-organizational-unit-preservation-policy.action";
import {
  AdminOrganizationalUnitPreservationPolicyState,
  AdminOrganizationalUnitPreservationPolicyStateModel,
} from "@admin/organizational-unit/stores/preservation-policy/admin-organizational-unit-preservation-policy.state";
import {
  AdminOrganizationalUnitSubjectAreaState,
  AdminOrganizationalUnitSubjectAreaStateModel,
} from "@admin/organizational-unit/stores/subject-area/admin-organizational-unit-subject-area-state.service";
import {AdminOrganizationalUnitSubjectAreaAction} from "@admin/organizational-unit/stores/subject-area/admin-organizational-unit-subject-area.action";
import {AdminOrganizationalUnitSubmissionPolicyAction} from "@admin/organizational-unit/stores/submission-policy/admin-organizational-unit-submission-policy.action";
import {
  AdminOrganizationalUnitSubmissionPolicyState,
  AdminOrganizationalUnitSubmissionPolicyStateModel,
} from "@admin/organizational-unit/stores/submission-policy/admin-organizational-unit-submission-policy.state";
import {
  HttpResponse,
  HttpStatusCode,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {AppAuthorizedOrganizationalUnitAction} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {AppAuthorizedOrganizationalUnitState} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.state";
import {AppMemberOrganizationalUnitAction} from "@app/stores/member-organizational-unit/app-member-organizational-unit.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {environment} from "@environments/environment";
import {OrganizationalUnit} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {PreservationSpaceOrganizationalUnitStateModel} from "@preservation-space/organizational-unit/stores/preservation-space-organizational-unit.state";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  AdminRoutesEnum,
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedDisseminationPolicyAction} from "@shared/stores/dissemination-policy/shared-dissemination-policy.action";
import {SharedPreservationPolicyAction} from "@shared/stores/preservation-policy/shared-preservation-policy.action";
import {SharedSubjectAreaAction} from "@shared/stores/subject-area/shared-subject-area.action";
import {SharedSubmissionPolicyAction} from "@shared/stores/submission-policy/shared-submission-policy.action";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  ArrayUtil,
  CollectionTyped,
  defaultAssociationStateInitValue,
  defaultMultiRelation2TiersStateInitValue,
  defaultRelation2TiersStateInitValue,
  defaultResourceFileStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isArray,
  isEmptyArray,
  isFalse,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  Relation3TiersForm,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface AdminOrganizationalUnitStateModel extends ResourceFileStateModel<OrganizationalUnit> {
  admin_organizationalUnit_submissionPolicy: AdminOrganizationalUnitSubmissionPolicyStateModel;
  admin_organizationalUnit_preservationPolicy: AdminOrganizationalUnitPreservationPolicyStateModel;
  admin_organizationalUnit_disseminationPolicy: AdminOrganizationalUnitDisseminationPolicyStateModel;
  admin_organizationalUnit_personRole: AdminOrganizationalUnitPersonRoleStateModel;
  admin_organizationalUnit_personInheritedRole: AdminOrganizationalUnitPersonInheritedRoleStateModel;
  admin_organizationalUnit_fundingAgency: AdminOrganizationalUnitFundingAgencyStateModel;
  admin_organizationalUnit_institution: AdminOrganizationalUnitInstitutionStateModel;
  admin_organizationalUnit_additionalFieldsForm: AdminOrganizationalUnitAdditionalFieldsFormStateModel;
  admin_organizationalUnit_subjectArea: AdminOrganizationalUnitSubjectAreaStateModel;
}

@Injectable()
@State<AdminOrganizationalUnitStateModel>({
  name: StateEnum.admin_organizationalUnit,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    admin_organizationalUnit_submissionPolicy: {...defaultRelation2TiersStateInitValue()},
    admin_organizationalUnit_preservationPolicy: {...defaultRelation2TiersStateInitValue()},
    admin_organizationalUnit_disseminationPolicy: {...defaultMultiRelation2TiersStateInitValue()},
    admin_organizationalUnit_personRole: {...defaultAdminOrganizationalUnitPersonRoleStateModel()},
    admin_organizationalUnit_personInheritedRole: {...defaultAdminOrganizationalUnitPersonInheritedRoleStateModel()},
    admin_organizationalUnit_fundingAgency: {...defaultAssociationStateInitValue()},
    admin_organizationalUnit_institution: {...defaultAssociationStateInitValue()},
    admin_organizationalUnit_subjectArea: {...defaultAssociationStateInitValue()},
    admin_organizationalUnit_additionalFieldsForm: {...defaultAdminOrganizationalUnitAdditionalFieldsFormStateModel()},
  },
  children: [
    AdminOrganizationalUnitSubmissionPolicyState,
    AdminOrganizationalUnitPreservationPolicyState,
    AdminOrganizationalUnitDisseminationPolicyState,
    AdminOrganizationalUnitPersonRoleState,
    AdminOrganizationalUnitPersonInheritedRoleState,
    AdminOrganizationalUnitFundingAgencyState,
    AdminOrganizationalUnitInstitutionState,
    AdminOrganizationalUnitSubjectAreaState,
    AdminOrganizationalUnitAdditionalFieldsFormState,
  ],
})
export class AdminOrganizationalUnitState extends ResourceFileState<AdminOrganizationalUnitStateModel, OrganizationalUnit> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              private readonly _securityService: SecurityService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminOrganizationalUnitActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminOrganizationalUnitDetail + urlSeparator + resId + urlSeparator + AdminRoutesEnum.organizationalUnitData,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => {
        const routeDetail = RoutesEnum.adminOrganizationalUnitDetail + urlSeparator + resId + urlSeparator + AdminRoutesEnum.organizationalUnitData;
        if (this._securityService.isRootOrAdmin()) {
          return routeDetail;
        }
        const listAuthorizedOrganizationalUnit = MemoizedUtil.listSnapshot(this._store, AppAuthorizedOrganizationalUnitState);
        if (isEmptyArray(listAuthorizedOrganizationalUnit)) {
          return RoutesEnum.adminOrganizationalUnit;
        }
        const isCurrentOrganizationalUnitManaged = listAuthorizedOrganizationalUnit.findIndex(i => i.resId === resId) >= 0;
        if (isCurrentOrganizationalUnitManaged) {
          return routeDetail;
        }
        return RoutesEnum.adminOrganizationalUnit;
      },
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminOrganizationalUnit,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.organizationalUnit.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.organizationalUnit.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.organizationalUnit.notification.resource.update"),
      updateSubResourceDispatchMethod: DispatchMethodEnum.SEQUENCIAL,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOrganizationalUnits;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminOrganizationalUnitStateModel): boolean {
    return StoreUtil.isLoadingState(state) || state.isLoadingFile;
  }

  @Selector()
  static isLoadingWithDependency(state: AdminOrganizationalUnitStateModel): boolean {
    return this.isLoading(state)
      || StoreUtil.isLoadingState(state.admin_organizationalUnit_submissionPolicy)
      || StoreUtil.isLoadingState(state.admin_organizationalUnit_preservationPolicy)
      || StoreUtil.isLoadingState(state.admin_organizationalUnit_disseminationPolicy)
      || StoreUtil.isLoadingState(state.admin_organizationalUnit_personRole)
      || StoreUtil.isLoadingState(state.admin_organizationalUnit_fundingAgency)
      || StoreUtil.isLoadingState(state.admin_organizationalUnit_subjectArea)
      || StoreUtil.isLoadingState(state.admin_organizationalUnit_institution);
  }

  @Selector()
  static currentTitle(state: AdminOrganizationalUnitStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminOrganizationalUnitStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current)
      && !isNullOrUndefined(state.admin_organizationalUnit_personRole.selected)
      && !isNullOrUndefined(state.admin_organizationalUnit_preservationPolicy.selected)
      && !isNullOrUndefined(state.admin_organizationalUnit_disseminationPolicy.selected)
      && !isNullOrUndefined(state.admin_organizationalUnit_submissionPolicy.selected)
      && !isNullOrUndefined(state.admin_organizationalUnit_fundingAgency.selected)
      && !isNullOrUndefined(state.admin_organizationalUnit_subjectArea.selected)
      && !isNullOrUndefined(state.admin_organizationalUnit_institution.selected);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminOrganizationalUnitStateModel): boolean {
    return true;
  }

  @Action(AdminOrganizationalUnitAction.LoadResource)
  loadResource(ctx: SolidifyStateContext<AdminOrganizationalUnitStateModel>, action: AdminOrganizationalUnitAction.LoadResource): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new SharedPreservationPolicyAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPreservationPolicyAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPreservationPolicyAction.GetAllFail)),
        ],
      },
      {
        action: new SharedSubmissionPolicyAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedSubmissionPolicyAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedSubmissionPolicyAction.GetAllFail)),
        ],
      },
      {
        action: new SharedDisseminationPolicyAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedDisseminationPolicyAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedDisseminationPolicyAction.GetAllFail)),
        ],
      },
      {
        action: new SharedSubjectAreaAction.GetSource(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedSubjectAreaAction.GetSourceSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedSubjectAreaAction.GetSourceFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new AdminOrganizationalUnitAction.LoadResourceSuccess(action));
        } else {
          ctx.dispatch(new AdminOrganizationalUnitAction.LoadResourceFail(action));
        }
        return result.success;
      }));
  }

  @OverrideDefaultAction()
  @Action(AdminOrganizationalUnitAction.CreateSuccess)
  createSuccess(ctx: SolidifyStateContext<AdminOrganizationalUnitStateModel>, action: AdminOrganizationalUnitAction.CreateSuccess): void {
    super.createSuccess(ctx, action);
    ctx.dispatch(new AppAuthorizedOrganizationalUnitAction.GetAll());
  }

  protected override _getListActionsUpdateSubResource(model: OrganizationalUnit, action: AdminOrganizationalUnitAction.Create | AdminOrganizationalUnitAction.Update, ctx: SolidifyStateContext<AdminOrganizationalUnitStateModel>): ActionSubActionCompletionsWrapper[] {
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);

    const orgUnitId = model.resId;

    let oldDefaultPreservationPolicyId = undefined;
    const newDefaultPreservationPolicyId = action.modelFormControlEvent.model.defaultPreservationPolicy?.resId;
    const newPreservationPoliciesIds = action.modelFormControlEvent.model.preservationPolicies.map(p => p.resId);

    let oldDefaultSubmissionPolicyId = undefined;
    const newDefaultSubmissionPolicyId = action.modelFormControlEvent.model.defaultSubmissionPolicy?.resId;
    const newSubmissionPoliciesIds = action.modelFormControlEvent.model.submissionPolicies.map(p => p.resId);

    const newFundingAgencyIds = action.modelFormControlEvent.model.fundingAgencies.map(p => p.resId);

    let oldInstitutionsIds = [];
    const newInstitutionsIds = action.modelFormControlEvent.model.institutions.map(p => p.resId) ?? [];

    const newSubjectAreaIds = action.modelFormControlEvent.model.subjectAreas.map(p => p.resId);

    const oldOrgUnit = ctx.getState().current;
    if (isNotNullNorUndefined(oldOrgUnit)) {
      oldDefaultPreservationPolicyId = oldOrgUnit.defaultPreservationPolicy?.resId;
      oldDefaultSubmissionPolicyId = oldOrgUnit.defaultSubmissionPolicy?.resId;
      const oldInstitutions = MemoizedUtil.selectedSnapshot(this._store, AdminOrganizationalUnitInstitutionState);
      oldInstitutionsIds = oldInstitutions?.map(i => i.resId) ?? [];
    }

    let concernCurrentUser = false;
    const currentPersonId = MemoizedUtil.currentSnapshot(this._store, AppUserState)?.person?.resId;

    const oldListPersonsRoles = MemoizedUtil.selectedSnapshot(this._store, AdminOrganizationalUnitPersonRoleState);
    if (isNotNullNorUndefined(oldListPersonsRoles) && oldListPersonsRoles.findIndex(p => p.resId === currentPersonId) >= 0) {
      concernCurrentUser = true;
    }

    const newPersonRole = action.modelFormControlEvent.formControl.get("personRole").value as Relation3TiersForm[];
    newPersonRole.forEach((elem) => {
      if (elem.id === currentPersonId) {
        concernCurrentUser = true;
      }
      if (!isNullOrUndefined(elem.listId) && !isArray(elem.listId)) {
        elem.listId = [elem.listId];
      }
    });

    actions.push(...[
      {
        action: new AdminOrganizationalUnitPreservationPolicyAction.Update(orgUnitId, newPreservationPoliciesIds, oldDefaultPreservationPolicyId, newDefaultPreservationPolicyId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitPreservationPolicyAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitPreservationPolicyAction.UpdateFail)),
        ],
      },
      {
        action: new AdminOrganizationalUnitSubmissionPolicyAction.Update(orgUnitId, newSubmissionPoliciesIds, oldDefaultSubmissionPolicyId, newDefaultSubmissionPolicyId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitSubmissionPolicyAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitSubmissionPolicyAction.UpdateFail)),
        ],
      },
      {
        action: new AdminOrganizationalUnitDisseminationPolicyAction.Update(orgUnitId, action.modelFormControlEvent.model.disseminationPolicies),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitDisseminationPolicyAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitDisseminationPolicyAction.UpdateFail)),
        ],
      },
      {
        action: new AdminOrganizationalUnitFundingAgencyAction.Update(orgUnitId, newFundingAgencyIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitFundingAgencyAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitFundingAgencyAction.UpdateFail)),
        ],
      },
      {
        action: new AdminOrganizationalUnitSubjectAreaAction.Update(orgUnitId, newSubjectAreaIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitSubjectAreaAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitSubjectAreaAction.UpdateFail)),
        ],
      },
      {
        action: new AdminOrganizationalUnitPersonRoleAction.Update(orgUnitId, newPersonRole),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitPersonRoleAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitPersonRoleAction.UpdateFail)),
        ],
      },
    ]);

    if (action instanceof AdminOrganizationalUnitAction.Update) {
      actions.push({
        action: new AdminOrganizationalUnitInstitutionAction.Update(orgUnitId, newInstitutionsIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitInstitutionAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitInstitutionAction.UpdateFail)),
        ],
      });
    }

    const diffInstitution = ArrayUtil.diff(oldInstitutionsIds, newInstitutionsIds);
    if (concernCurrentUser || isNonEmptyArray(diffInstitution.diff)) {
      actions.push(...[
        {
          action: new AppAuthorizedOrganizationalUnitAction.GetAll(undefined, false, false),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllFail)),
          ],
        },
        {
          action: new AppMemberOrganizationalUnitAction.GetAll(undefined, false, false),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AppMemberOrganizationalUnitAction.GetAllSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AppMemberOrganizationalUnitAction.GetAllFail)),
          ],
        },
      ]);
    }

    return actions;
  }

  @OverrideDefaultAction()
  @Action(AdminOrganizationalUnitAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<AdminOrganizationalUnitStateModel>, action: AdminOrganizationalUnitAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    if (isNotNullNorUndefined(action.model.logo)) {
      ctx.dispatch(new AdminOrganizationalUnitAction.GetFile(action.model.resId));
    }
  }

  @OverrideDefaultAction()
  @Action(AdminOrganizationalUnitAction.Delete)
  delete(ctx: SolidifyStateContext<AdminOrganizationalUnitStateModel>, action: AdminOrganizationalUnitAction.Delete): Observable<HttpResponse<any>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.deleteById<HttpResponse<any>>(this._urlResource, action.resId, null, "response")
      .pipe(
        tap(httpResponse => {
          ctx.dispatch(new AdminOrganizationalUnitAction.DeleteSuccess(action, httpResponse.status));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminOrganizationalUnitAction.DeleteFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(AdminOrganizationalUnitAction.DeleteSuccess)
  deleteSuccess(ctx: SolidifyStateContext<AdminOrganizationalUnitStateModel>, action: AdminOrganizationalUnitAction.DeleteSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    StoreUtil.navigateIfDefined(ctx, this._optionsState.routeRedirectUrlAfterSuccessDeleteAction, undefined, this._optionsState.routeReplaceAfterSuccessDeleteAction);
    if (action.httpStatusCode === HttpStatusCode.Ok) {
      this._notificationService.showSuccess(this._optionsState.notificationResourceDeleteSuccessTextToTranslate);
    } else {
      this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("admin.organizationalUnit.notification.resource.closed"));
    }
    if (isNullOrUndefined(this._optionsState.keepCurrentStateAfterDelete) || isFalse(this._optionsState.keepCurrentStateAfterDelete)) {
      ctx.dispatch(new AdminOrganizationalUnitAction.Clean(true));
    }
    ctx.dispatch(new AppAuthorizedOrganizationalUnitAction.GetAll());
  }

  @Action(AdminOrganizationalUnitAction.LoadOnlyMyOrgUnits)
  loadOnlyMyOrgUnits(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>, action: AdminOrganizationalUnitAction.LoadOnlyMyOrgUnits): Observable<CollectionTyped<OrganizationalUnit>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });

    return this._apiService.getCollection<OrganizationalUnit>(ApiEnum.adminAuthorizedOrganizationalUnits, ctx.getState().queryParameters)
      .pipe(
        tap((collection: CollectionTyped<OrganizationalUnit>) => {
          ctx.dispatch(new AdminOrganizationalUnitAction.LoadOnlyMyOrgUnitsSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminOrganizationalUnitAction.LoadOnlyMyOrgUnitsFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminOrganizationalUnitAction.LoadOnlyMyOrgUnitsSuccess)
  loadOnlyMyOrgUnitsSuccess(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>, action: AdminOrganizationalUnitAction.LoadOnlyMyOrgUnitsSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    ctx.patchState({
      list: action.list._data,
      total: action.list._page.totalItems,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters,
    });
  }

  @Action(AdminOrganizationalUnitAction.LoadOnlyMyOrgUnitsFail)
  loadOnlyMyOrgUnitsFail(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>, action: AdminOrganizationalUnitAction.LoadOnlyMyOrgUnitsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
