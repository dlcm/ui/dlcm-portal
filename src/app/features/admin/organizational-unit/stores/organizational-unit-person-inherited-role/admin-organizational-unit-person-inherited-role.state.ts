/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-person-inherited-role.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {Injectable} from "@angular/core";
import {ApiActionNameEnum} from "@app/shared/enums/api-action-name.enum";
import {ApiEnum} from "@app/shared/enums/api.enum";

import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BasicState,
  defaultResourceStateInitValue,
  NotificationService,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";
import {AdminOrganizationalUnitPersonInheritedRoleAction} from "./admin-organizational-unit-person-inherited-role.action";

export const defaultAdminOrganizationalUnitPersonInheritedRoleStateModel: () => AdminOrganizationalUnitPersonInheritedRoleStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
  });

export interface AdminOrganizationalUnitPersonInheritedRoleStateModel extends ResourceStateModel<PersonRole> {
}

@Injectable()
@State<AdminOrganizationalUnitPersonInheritedRoleStateModel>({
  name: StateEnum.admin_organizationalUnit_personInheritedRole,
  defaults: {
    ...defaultAdminOrganizationalUnitPersonInheritedRoleStateModel(),
  },
})
// OrganizationalUnitPersonInheritedRoleController
export class AdminOrganizationalUnitPersonInheritedRoleState extends BasicState<AdminOrganizationalUnitPersonInheritedRoleStateModel> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super();
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOrganizationalUnits;
  }

  @Action(AdminOrganizationalUnitPersonInheritedRoleAction.GetAll)
  getAll(ctx: SolidifyStateContext<AdminOrganizationalUnitPersonInheritedRoleStateModel>, action: AdminOrganizationalUnitPersonInheritedRoleAction.GetAll): Observable<PersonRole[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      list: [],
    });
    return this._apiService.post<void, PersonRole[]>(`${this._urlResource}/${action.orgUnitId}/${ApiActionNameEnum.LIST_INHERITED_PERSON_ROLES}`)
      .pipe(
        tap((model: PersonRole[]) => {
          ctx.dispatch(new AdminOrganizationalUnitPersonInheritedRoleAction.GetAllSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminOrganizationalUnitPersonInheritedRoleAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminOrganizationalUnitPersonInheritedRoleAction.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<AdminOrganizationalUnitPersonInheritedRoleStateModel>, action: AdminOrganizationalUnitPersonInheritedRoleAction.GetAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      list: action.list,
    });
  }

  @Action(AdminOrganizationalUnitPersonInheritedRoleAction.GetAllFail)
  getAllFail(ctx: SolidifyStateContext<AdminOrganizationalUnitPersonInheritedRoleStateModel>, action: AdminOrganizationalUnitPersonInheritedRoleAction.GetAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
