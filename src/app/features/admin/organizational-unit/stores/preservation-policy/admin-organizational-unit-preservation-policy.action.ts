/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-preservation-policy.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {OrganizationalUnitPreservationPolicy} from "@admin/models/organizational-unit-preservation-policy.model";
import {PreservationPolicy} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  Relation2TiersAction,
  Relation2TiersNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.admin_organizationalUnit_preservationPolicy;

export namespace AdminOrganizationalUnitPreservationPolicyAction {

  @TypeDefaultAction(state)
  export class GetAll extends Relation2TiersAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends Relation2TiersAction.GetAllSuccess<PreservationPolicy> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends Relation2TiersAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends Relation2TiersAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends Relation2TiersAction.GetByIdSuccess<PreservationPolicy> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends Relation2TiersAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends Relation2TiersAction.Update {
    constructor(public parentId: string, public newResId: string[], public oldDefaultResId: string, public newDefaultResId: string) {
      super(parentId, newResId);
    }
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends Relation2TiersAction.UpdateSuccess {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends Relation2TiersAction.UpdateFail {
  }

  @TypeDefaultAction(state)
  export class UpdateRelation extends Relation2TiersAction.UpdateRelation<OrganizationalUnitPreservationPolicy> {
  }

  @TypeDefaultAction(state)
  export class UpdateRelationSuccess extends Relation2TiersAction.UpdateRelationSuccess<OrganizationalUnitPreservationPolicy> {
  }

  @TypeDefaultAction(state)
  export class UpdateRelationFail extends Relation2TiersAction.UpdateRelationFail<OrganizationalUnitPreservationPolicy> {
  }

  @TypeDefaultAction(state)
  export class Create extends Relation2TiersAction.Create {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends Relation2TiersAction.CreateSuccess {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends Relation2TiersAction.CreateFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends Relation2TiersAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends Relation2TiersAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends Relation2TiersAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class Delete extends Relation2TiersAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends Relation2TiersAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends Relation2TiersAction.DeleteFail {
  }
}

export const adminOrganizationalUnitPreservationPolicyActionNameSpace: Relation2TiersNameSpace = AdminOrganizationalUnitPreservationPolicyAction;
