/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-preservation-policy.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {OrganizationalUnitPreservationPolicy} from "@admin/models/organizational-unit-preservation-policy.model";
import {
  AdminOrganizationalUnitPreservationPolicyAction,
  adminOrganizationalUnitPreservationPolicyActionNameSpace,
} from "@admin/organizational-unit/stores/preservation-policy/admin-organizational-unit-preservation-policy.action";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {PreservationPolicy} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  Observable,
  of,
} from "rxjs";
import {
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultRelation2TiersStateInitValue,
  isNotNullNorUndefined,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  Relation2TiersState,
  Relation2TiersStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface AdminOrganizationalUnitPreservationPolicyStateModel extends Relation2TiersStateModel<PreservationPolicy> {
}

@Injectable()
@State<AdminOrganizationalUnitPreservationPolicyStateModel>({
  name: StateEnum.admin_organizationalUnit_preservationPolicy,
  defaults: {
    ...defaultRelation2TiersStateInitValue(),
  },
})
export class AdminOrganizationalUnitPreservationPolicyState extends Relation2TiersState<AdminOrganizationalUnitPreservationPolicyStateModel, PreservationPolicy, OrganizationalUnitPreservationPolicy> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminOrganizationalUnitPreservationPolicyActionNameSpace,
      resourceName: ApiResourceNameEnum.PRES_POLICY,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOrganizationalUnits;
  }

  @OverrideDefaultAction()
  @Action(AdminOrganizationalUnitPreservationPolicyAction.Update)
  update(ctx: SolidifyStateContext<AdminOrganizationalUnitPreservationPolicyStateModel>, action: AdminOrganizationalUnitPreservationPolicyAction.Update): Observable<boolean> {
    return super._internalUpdate(ctx, action)
      .pipe(
        tap((isSuccess: boolean) => {
          if (!isSuccess) {
            ctx.dispatch(new AdminOrganizationalUnitPreservationPolicyAction.UpdateFail(action, action.parentId));
            return;
          }
          this._updateSubResource(action, ctx)
            .subscribe(success => {
              if (success) {
                ctx.dispatch(new AdminOrganizationalUnitPreservationPolicyAction.UpdateSuccess(action, action.parentId));
              } else {
                ctx.dispatch(new AdminOrganizationalUnitPreservationPolicyAction.UpdateFail(action, action.parentId));
              }
            });
        }),
      );
  }

  protected _updateSubResource(action: AdminOrganizationalUnitPreservationPolicyAction.Update, ctx: SolidifyStateContext<AdminOrganizationalUnitPreservationPolicyStateModel>): Observable<boolean> {
    if (action.oldDefaultResId !== action.newDefaultResId && isNotNullNorUndefined(action.newDefaultResId)) {
      return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(ctx, [{
        action: new AdminOrganizationalUnitPreservationPolicyAction.UpdateRelation(action.parentId, action.newDefaultResId, {defaultPolicy: true} as OrganizationalUnitPreservationPolicy),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitPreservationPolicyAction.UpdateRelationSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminOrganizationalUnitPreservationPolicyAction.UpdateRelationFail)),
        ],
      }]).pipe(
        map(result => result.success),
      );
    }
    return of(true);
  }
}
