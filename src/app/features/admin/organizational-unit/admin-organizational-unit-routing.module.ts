/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminOrganizationalUnitAdditionalFieldsListRoutable} from "@admin/organizational-unit/components/routables/admin-organizational-unit-additional-fields-list/admin-organizational-unit-additional-fields-list.routable";
import {AdminOrganizationalUnitCreateRoutable} from "@admin/organizational-unit/components/routables/admin-organizational-unit-create/admin-organizational-unit-create.routable";
import {AdminOrganizationalUnitDetailEditRoutable} from "@admin/organizational-unit/components/routables/admin-organizational-unit-detail-edit/admin-organizational-unit-detail-edit.routable";
import {AdminOrganizationalUnitListRoutable} from "@admin/organizational-unit/components/routables/admin-organizational-unit-list/admin-organizational-unit-list.routable";
import {AdminOrganizationalUnitMetadataRoutable} from "@admin/organizational-unit/components/routables/admin-organizational-unit-metadata/admin-organizational-unit-metadata.routable";
import {AdminOrganizationalUnitState} from "@admin/organizational-unit/stores/admin-organizational-unit.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";
import {
  CanDeactivateGuard,
  EmptyContainer,
  SsrUtil,
} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminOrganizationalUnitListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.organizationalUnitDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    redirectTo: AdminRoutesEnum.organizationalUnitDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId + AppRoutesEnum.separator + AdminRoutesEnum.organizationalUnitData,
    pathMatch: "full",
  },
  {
    path: AdminRoutesEnum.organizationalUnitDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminOrganizationalUnitDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminOrganizationalUnitState.currentTitle,
    },
    children: [
      {
        path: AdminRoutesEnum.organizationalUnitData,
        component: AdminOrganizationalUnitMetadataRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.detail,
        },
        children: [
          {
            path: AdminRoutesEnum.organizationalUnitEdit,
            component: EmptyContainer,
            data: {
              breadcrumb: LabelTranslateEnum.edit,
            },
            canDeactivate: [CanDeactivateGuard],
          },
        ],
      },
      {
        path: AdminRoutesEnum.organizationalUnitAdditionalFieldsForm,
        component: AdminOrganizationalUnitAdditionalFieldsListRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.additionalMetadata,
        },
      },
    ],
  },
  {
    path: AdminRoutesEnum.organizationalUnitCreate,
    component: AdminOrganizationalUnitCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminOrganizationalUnitRoutingModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.adminOrganizationalUnitModuleLoaded] = true;
    }
  }
}
