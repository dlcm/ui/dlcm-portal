/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {AdminOrganizationalUnitService} from "@admin/organizational-unit/services/admin-organizational-unit.service";
import {AdminOrganizationalUnitAction} from "@admin/organizational-unit/stores/admin-organizational-unit.action";
import {AdminOrganizationalUnitState} from "@admin/organizational-unit/stores/admin-organizational-unit.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {NotificationHelper} from "@app/features/preservation-space/notification/helper/notification.helper";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  DisseminationPolicy,
  FundingAgency,
  Institution,
  Label,
  License,
  OrganizationalUnit,
  OrganizationalUnitDisseminationPolicyContainer,
  PreservationPolicy,
  Role,
  SubjectArea,
  SubmissionPolicy,
} from "@models";
import {SharedFundingAgencyOverlayPresentational} from "@shared/components/presentationals/shared-funding-agency-overlay/shared-funding-agency-overlay.presentational";
import {SharedInstitutionOverlayPresentational} from "@shared/components/presentationals/shared-institution-overlay/shared-institution-overlay.presentational";
import {SharedLicenseOverlayPresentational} from "@shared/components/presentationals/shared-license-overlay/shared-license-overlay.presentational";
import {SharedOrganizationalUnitDisseminationPolicyParametersPresentational} from "@shared/components/presentationals/shared-organizational-unit-dissemination-policy-parameters/shared-organizational-unit-dissemination-policy-parameters.presentational";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {sharedFundingAgencyActionNameSpace} from "@shared/stores/funding-agency/shared-funding-agency.action";
import {SharedFundingAgencyState} from "@shared/stores/funding-agency/shared-funding-agency.state";
import {sharedInstitutionActionNameSpace} from "@shared/stores/institution/shared-institution.action";
import {SharedInstitutionState} from "@shared/stores/institution/shared-institution.state";
import {sharedLicenseActionNameSpace} from "@shared/stores/license/shared-license.action";
import {
  SharedLicenseState,
  SharedLicenseStateModel,
} from "@shared/stores/license/shared-license.state";
import {sharedSubjectAreaActionNameSpace} from "@shared/stores/subject-area/shared-subject-area.action";
import {SharedSubjectAreaState} from "@shared/stores/subject-area/shared-subject-area.state";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  AbstractApplicationFormPresentational,
  BreakpointService,
  DateUtil,
  isEmptyString,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  ObservableUtil,
  OrderEnum,
  PropertyName,
  RegexUtil,
  ResourceFileNameSpace,
  ResourceNameSpace,
  SearchableSingleSelectPresentational,
  SolidifyValidator,
  Sort,
  SsrUtil,
  Type,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-organizational-unit-form",
  templateUrl: "./admin-organizational-unit-form.presentational.html",
  styleUrls: ["./admin-organizational-unit-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOrganizationalUnitFormPresentational extends AbstractApplicationFormPresentational<OrganizationalUnit> implements OnInit {
  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  private readonly _KEY_SOURCE: keyof SubjectArea = "source";
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;
  readonly classBypassEdit: string = environment.classBypassEdit;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  organizationalUnitActionNameSpace: ResourceFileNameSpace = AdminOrganizationalUnitAction;
  organizationalUnitState: typeof AdminOrganizationalUnitState = AdminOrganizationalUnitState;

  institutionOverlayComponent: Type<SharedInstitutionOverlayPresentational> = SharedInstitutionOverlayPresentational;
  fundingAgencyOverlayComponent: Type<SharedFundingAgencyOverlayPresentational> = SharedFundingAgencyOverlayPresentational;
  licenseOverlayComponent: Type<SharedLicenseOverlayPresentational> = SharedLicenseOverlayPresentational;

  @Input()
  selectedSubmissionPolicies: SubmissionPolicy[];

  @Input()
  selectedPreservationPolicies: PreservationPolicy[];

  @Input()
  selectedFundingAgencies: FundingAgency[];

  @Input()
  selectedDisseminationPolicies: OrganizationalUnitDisseminationPolicyContainer[];

  @Input()
  selectedPersonRole: PersonRole[];

  @Input()
  selectedInstitutions: Institution[];

  @Input()
  selectedSubjectAreas: SubjectArea[];

  @Input()
  listSubmissionPolicies: SubmissionPolicy[];

  @Input()
  listPreservationPolicies: PreservationPolicy[];

  @Input()
  listDisseminationPolicies: DisseminationPolicy[];

  @Input()
  listManagedInstitution: Institution[];

  @Input()
  subjectAreaSources: string[];

  @Input()
  listRole: Role[];

  @Input()
  isAdminOrRoot: boolean;

  @Input()
  defaultPlatformLicenseId: string;

  @Input()
  urlQueryParameters: MappingObject<string | undefined, string>;

  private _currentLanguageUpperCase: string;

  @Input()
  set currentLanguage(value: Enums.Language.LanguageEnum) {
    this._currentLanguageUpperCase = value?.toUpperCase();
    this.subjectAreaExtraSearchParameterSource = {languageId: this._currentLanguageUpperCase};
  }

  @Input()
  inheritedSelectedPersonRole: PersonRole[];

  protected readonly _seeArchiveBS: BehaviorSubject<OrganizationalUnit> = new BehaviorSubject<OrganizationalUnit>(undefined);
  @Output("seeArchiveChange")
  readonly seeArchiveObs: Observable<OrganizationalUnit> = ObservableUtil.asObservable(this._seeArchiveBS);

  @ViewChild("licenseSingleSearchableSelect")
  licenseSingleSearchableSelect: SearchableSingleSelectPresentational<SharedLicenseStateModel, License>;

  @ViewChild("organizationalUnitDisseminationPolicyParametersPresentational")
  organizationalUnitDisseminationPolicyParametersPresentational: SharedOrganizationalUnitDisseminationPolicyParametersPresentational;

  sharedInstitutionSort: Sort<Institution> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedInstitutionActionNameSpace: ResourceNameSpace = sharedInstitutionActionNameSpace;
  sharedInstitutionState: typeof SharedInstitutionState = SharedInstitutionState;

  sharedFundingAgencySort: Sort<FundingAgency> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedFundingAgencyActionNameSpace: ResourceNameSpace = sharedFundingAgencyActionNameSpace;
  sharedFundingAgencyState: typeof SharedFundingAgencyState = SharedFundingAgencyState;

  sharedSubjectAreaSort: Sort<SubjectArea> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedSubjectAreaActionNameSpace: ResourceNameSpace = sharedSubjectAreaActionNameSpace;
  sharedSubjectAreaState: typeof SharedSubjectAreaState = SharedSubjectAreaState;

  sharedLicenseSort: Sort<License> = {
    field: "title",
    order: OrderEnum.ascending,
  };
  sharedLicenseActionNameSpace: ResourceNameSpace = sharedLicenseActionNameSpace;
  sharedLicenseState: typeof SharedLicenseState = SharedLicenseState;

  subjectAreaExtraSearchParameterSource: MappingObject<string, string>;

  licenseCallback: (value: License) => string = (value: License) => value.openLicenseId + " (" + value.title + ")";
  subjectAreaLabelCallback: (value: SubjectArea) => string = (value: SubjectArea) => {
    let name = value.name;
    const label: Label = value?.labels?.find((l: Label) => l.language?.resId === this._currentLanguageUpperCase);
    if (isNotNullNorUndefined(label) && isNonEmptyString(label.text)) {
      name = label.text;
    }
    return `[${value.source}] ${name}`;
  };

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly breakpointService: BreakpointService,
              private readonly _adminOrganizationalUnitService: AdminOrganizationalUnitService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [SolidifyValidator]],
      [this.formDefinition.openingDate]: [""],
      [this.formDefinition.closingDate]: [""],
      [this.formDefinition.submissionPolicies]: [[], [Validators.required, SolidifyValidator]],
      [this.formDefinition.preservationPolicies]: [[], [Validators.required, SolidifyValidator]],
      [this.formDefinition.disseminationPolicies]: [[], [SolidifyValidator]],
      [this.formDefinition.defaultSubmissionPolicy]: [undefined, [Validators.required, SolidifyValidator]],
      [this.formDefinition.defaultPreservationPolicy]: [undefined, [Validators.required, SolidifyValidator]],
      [this.formDefinition.url]: ["", [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.personRole]: ["", [SolidifyValidator]],
      [this.formDefinition.inheritedPersonRole]: ["", [SolidifyValidator]],
      [this.formDefinition.fundingAgencies]: ["", [SolidifyValidator]],
      [this.formDefinition.institutions]: [isNonEmptyArray(this.listManagedInstitution) ? this.listManagedInstitution.map(i => i.resId) : [], [SolidifyValidator, ...(this.isAdminOrRoot ? [] : [Validators.required])]],
      [this.formDefinition.filterSubjectAreas]: ["", [SolidifyValidator]],
      [this.formDefinition.subjectAreas]: ["", [SolidifyValidator]],
      [this.formDefinition.keywords]: [[], [SolidifyValidator]],
      [this.formDefinition.defaultLicense]: ["", [SolidifyValidator]],
    });

    this._creationForUserAskingInNotificationRequest();
  }

  protected _bindFormTo(organizationalUnit: OrganizationalUnit): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [organizationalUnit.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [organizationalUnit.description, [SolidifyValidator]],
      [this.formDefinition.openingDate]: [organizationalUnit.openingDate],
      [this.formDefinition.closingDate]: [organizationalUnit.closingDate],
      [this.formDefinition.submissionPolicies]: [this.selectedSubmissionPolicies.map(s => s.resId), [Validators.required, SolidifyValidator]],
      [this.formDefinition.preservationPolicies]: [this.selectedPreservationPolicies.map(p => p.resId), [Validators.required, SolidifyValidator]],
      [this.formDefinition.disseminationPolicies]: [this.selectedDisseminationPolicies.map(d => d.joinResource.compositeKey), [SolidifyValidator]],
      [this.formDefinition.defaultSubmissionPolicy]: [(organizationalUnit.defaultSubmissionPolicy ? organizationalUnit.defaultSubmissionPolicy.resId : undefined), [Validators.required, SolidifyValidator]],
      [this.formDefinition.defaultPreservationPolicy]: [(organizationalUnit.defaultPreservationPolicy ? organizationalUnit.defaultPreservationPolicy.resId : undefined), [Validators.required, SolidifyValidator]],
      [this.formDefinition.url]: [organizationalUnit.url, [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.personRole]: ["", [SolidifyValidator]],
      [this.formDefinition.inheritedPersonRole]: ["", [SolidifyValidator]],
      [this.formDefinition.fundingAgencies]: [this.selectedFundingAgencies.map(f => f.resId), [SolidifyValidator]],
      [this.formDefinition.institutions]: [this.selectedInstitutions.map(i => i.resId), [SolidifyValidator, ...(this.isAdminOrRoot ? [] : [Validators.required])]],
      [this.formDefinition.filterSubjectAreas]: ["", [SolidifyValidator]],
      [this.formDefinition.subjectAreas]: [this.selectedSubjectAreas.map(i => i.resId), [SolidifyValidator]],
      [this.formDefinition.keywords]: [[...organizationalUnit.keywords], [SolidifyValidator]],
      [this.formDefinition.defaultLicense]: [organizationalUnit?.defaultLicense?.resId, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(organizationalUnit: OrganizationalUnit): OrganizationalUnit {
    delete organizationalUnit[this.formDefinition.filterSubjectAreas];
    organizationalUnit.openingDate = DateUtil.convertToLocalDateDateSimple(organizationalUnit.openingDate);
    organizationalUnit.closingDate = DateUtil.convertToLocalDateDateSimple(organizationalUnit.closingDate);

    const defaultSubmissionPolicyId = this.form.get(this.formDefinition.defaultSubmissionPolicy).value;
    if (isNotNullNorUndefinedNorWhiteString(defaultSubmissionPolicyId)) {
      organizationalUnit.defaultSubmissionPolicy = {resId: defaultSubmissionPolicyId};
    }

    const defaultPreservationPolicyId = this.form.get(this.formDefinition.defaultPreservationPolicy).value;
    if (isNotNullNorUndefinedNorWhiteString(defaultPreservationPolicyId)) {
      organizationalUnit.defaultPreservationPolicy = {resId: defaultPreservationPolicyId};
    }

    organizationalUnit.preservationPolicies = [];
    this.form.get(this.formDefinition.preservationPolicies).value.forEach(resId => {
      organizationalUnit.preservationPolicies.push({resId: resId});
    });
    organizationalUnit.submissionPolicies = [];
    this.form.get(this.formDefinition.submissionPolicies).value.forEach(resId => {
      organizationalUnit.submissionPolicies.push({resId: resId});
    });
    organizationalUnit.disseminationPolicies = this.organizationalUnitDisseminationPolicyParametersPresentational.listOrganizationalUnitDisseminationPolicyContainer;

    organizationalUnit.fundingAgencies = [];
    if (this.form.get(this.formDefinition.fundingAgencies).value !== "") {
      this.form.get(this.formDefinition.fundingAgencies).value.forEach(resId => {
        organizationalUnit.fundingAgencies.push({resId: resId});
      });
    }
    organizationalUnit.institutions = [];
    if (this.form.get(this.formDefinition.institutions).value !== "") {
      this.form.get(this.formDefinition.institutions).value.forEach(resId => {
        organizationalUnit.institutions.push({resId: resId});
      });
    }
    organizationalUnit.subjectAreas = [];
    if (this.form.get(this.formDefinition.subjectAreas).value !== "") {
      this.form.get(this.formDefinition.subjectAreas).value.forEach(resId => {
        organizationalUnit.subjectAreas.push({resId: resId});
      });
    }

    const defaultLicenseResId = this.form.get(this.formDefinition.defaultLicense).value;
    if (this.licenseSingleSearchableSelect.isDefaultValueSelected || isNullOrUndefined(defaultLicenseResId) || isEmptyString(defaultLicenseResId)) {
      organizationalUnit.defaultLicense = null;
    } else {
      organizationalUnit.defaultLicense = {resId: defaultLicenseResId};
    }
    return organizationalUnit;
  }

  navigateToDeposit(orgUnitId: string): void {
    this.navigate([RoutesEnum.deposit + urlSeparator + orgUnitId]);
  }

  navigateToArchive(): void {
    this._seeArchiveBS.next(this.model);
  }

  navigateToSubmissionPolicy(submissionPolicy: SubmissionPolicy): void {
    this.navigate([RoutesEnum.adminSubmissionPolicyDetail, submissionPolicy.resId]);
  }

  navigateToPreservationPolicy(preservationPolicy: PreservationPolicy): void {
    this.navigate([RoutesEnum.adminPreservationPolicyDetail, preservationPolicy.resId]);
  }

  navigateToFundingAgency(fundingAgency: FundingAgency): void {
    this.navigate([RoutesEnum.adminFundingAgenciesDetail, fundingAgency.resId]);
  }

  navigateToInstitution(institution: Institution): void {
    this.navigate([RoutesEnum.adminInstitutionDetail, institution.resId]);
  }

  navigateToSubjectArea(): void {
    this.navigate([RoutesEnum.adminSubjectArea]);
  }

  navigateToLicense(licenseId: string): void {
    this.navigate([RoutesEnum.adminLicenseDetail, licenseId]);
  }

  filterSubjectArea(source: string): void {
    this._computeExtraSearchParameterCategory(source);
  }

  private _creationForUserAskingInNotificationRequest(): void {
    if (isNullOrUndefined(this.urlQueryParameters) || MappingObjectUtil.size(this.urlQueryParameters) === 0) {
      return;
    }
    const roleId = MappingObjectUtil.get(this.urlQueryParameters, NotificationHelper.KEY_ROLE_ID);
    const personId = MappingObjectUtil.get(this.urlQueryParameters, NotificationHelper.KEY_PERSON_ID);
    const orgUnitName = SsrUtil.window?.decodeURIComponent(MappingObjectUtil.get(this.urlQueryParameters, NotificationHelper.KEY_ORGUNIT_NAME));
    if (isNullOrUndefined(roleId) || isNullOrUndefined(personId) || isNullOrUndefined(orgUnitName)) {
      return;
    }
    const existingPerson = this.selectedPersonRole.find(p => p.resId === personId);
    if (isNullOrUndefined(existingPerson)) {
      this.selectedPersonRole = [{
        resId: personId,
        roles: [{
          resId: roleId,
        } as any],
      }];
    }
    this.form.get(this.formDefinition.name).setValue(orgUnitName);
  }

  private _computeExtraSearchParameterCategory(value: string): void {
    if (isNullOrUndefined(value) || isEmptyString(value)) {
      MappingObjectUtil.delete(this.subjectAreaExtraSearchParameterSource, this._KEY_SOURCE as string);
      return;
    }
    MappingObjectUtil.set(this.subjectAreaExtraSearchParameterSource, this._KEY_SOURCE as string, value);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() description: string;
  @PropertyName() closingDate: string;
  @PropertyName() openingDate: string;
  @PropertyName() submissionPolicies: string;
  @PropertyName() preservationPolicies: string;
  @PropertyName() disseminationPolicies: string;
  @PropertyName() defaultSubmissionPolicy: string;
  @PropertyName() defaultPreservationPolicy: string;
  @PropertyName() url: string;
  @PropertyName() personRole: string;
  @PropertyName() inheritedPersonRole: string;
  @PropertyName() fundingAgencies: string;
  @PropertyName() institutions: string;
  @PropertyName() filterSubjectAreas: string;
  @PropertyName() subjectAreas: string;
  @PropertyName() keywords: string;
  @PropertyName() defaultLicense: string;
}
