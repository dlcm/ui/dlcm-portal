/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-additional-fields-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {AdditionalFieldsForm} from "@models";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  AbstractFormPresentational,
  BreakpointService,
  KeyValue,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-organizational-unit-additional-fields-form",
  templateUrl: "./admin-organizational-unit-additional-fields-form.presentational.html",
  styleUrls: ["./admin-organizational-unit-additional-fields-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOrganizationalUnitAdditionalFieldsFormPresentational extends AbstractFormPresentational<AdditionalFieldsForm> {
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  listFormType: KeyValue[] = Enums.FormDescription.TypeEnumTranslate;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly breakpointService: BreakpointService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.type]: [Enums.FormDescription.TypeEnum.FORMLY, [Validators.required, SolidifyValidator]],
      [this.formDefinition.name]: ["", [SolidifyValidator]],
      [this.formDefinition.description]: [""],
    });

  }

  protected _bindFormTo(additionalFieldsForm: AdditionalFieldsForm): void {
    this.form = this._fb.group({
      [this.formDefinition.type]: [additionalFieldsForm.type, [Validators.required, SolidifyValidator]],
      [this.formDefinition.name]: [additionalFieldsForm.name, [SolidifyValidator]],
      [this.formDefinition.description]: [additionalFieldsForm.description],
    });
  }

  protected _treatmentBeforeSubmit(additionalFieldsForm: AdditionalFieldsForm | any): AdditionalFieldsForm {
    return additionalFieldsForm;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() type: string;
  @PropertyName() name: string;
  @PropertyName() description: string;
  @PropertyName() creation: string;
  @PropertyName() lastUpdate: string;
}
