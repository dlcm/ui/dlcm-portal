/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminOrganizationalUnitAdditionalFieldsDetailCreateUpdateDialog} from "@admin/organizational-unit/components/dialogs/admin-organizational-unit-additional-fields-detail-create-update/admin-organizational-unit-additional-fields-detail-create-update.dialog";
import {AdminOrganizationalUnitService} from "@admin/organizational-unit/services/admin-organizational-unit.service";
import {adminOrganizationalUnitActionNameSpace} from "@admin/organizational-unit/stores/admin-organizational-unit.action";
import {
  AdminOrganizationalUnitState,
  AdminOrganizationalUnitStateModel,
} from "@admin/organizational-unit/stores/admin-organizational-unit.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  OrganizationalUnit,
  PreservationPolicy,
  Role,
  SubmissionPolicy,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedOrganizationalUnitActionNameSpace} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedPreservationPolicyState} from "@shared/stores/preservation-policy/shared-preservation-policy.state";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {SharedSubmissionPolicyState} from "@shared/stores/submission-policy/shared-submission-policy.state";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  DialogUtil,
  ExtraButtonToolbar,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  ResourceNameSpace,
  Tab,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-organizational-unit-detail-edit-routable",
  templateUrl: "./admin-organizational-unit-detail-edit.routable.html",
  styleUrls: ["./admin-organizational-unit-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOrganizationalUnitDetailEditRoutable extends AbstractDetailEditCommonRoutable<OrganizationalUnit, AdminOrganizationalUnitStateModel> implements OnInit {
  @Select(AdminOrganizationalUnitState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminOrganizationalUnitState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  listSubmissionPoliciesObs: Observable<SubmissionPolicy[]> = MemoizedUtil.list(this._store, SharedSubmissionPolicyState);
  listPreservationPoliciesObs: Observable<PreservationPolicy[]> = MemoizedUtil.list(this._store, SharedPreservationPolicyState);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);
  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedOrganizationalUnitActionNameSpace;

  readonly KEY_PARAM_NAME: keyof OrganizationalUnit & string = "name";

  private _currentTab: Tab;

  listTabs: Tab[] = [
    {
      id: TabEnum.DATA,
      suffixUrl: AdminRoutesEnum.organizationalUnitData,
      icon: IconNameEnum.metadata,
      titleToTranslate: LabelTranslateEnum.detail,
      route: () => [...this._rootUrl, AdminRoutesEnum.organizationalUnitData],
    },
    {
      id: TabEnum.ADDITIONAL_FIELDS_FORM,
      suffixUrl: AdminRoutesEnum.organizationalUnitAdditionalFieldsForm,
      icon: IconNameEnum.files,
      titleToTranslate: MARK_AS_TRANSLATABLE("admin.organizationalUnit.tab.additionalFieldsForm"),
      route: () => [...this._rootUrl, AdminRoutesEnum.organizationalUnitAdditionalFieldsForm],
    },
  ];

  private get _rootUrl(): string[] {
    return [AppRoutesEnum.admin, AdminRoutesEnum.organizationalUnit, AdminRoutesEnum.organizationalUnitDetail, this._resId];
  }

  listExtraButtons: ExtraButtonToolbar<OrganizationalUnit>[] = [
    {
      color: "primary",
      typeButton: "flat-button",
      icon: IconNameEnum.save,
      labelToTranslate: (current) => LabelTranslateEnum.save,
      order: 40,
      callback: () => this._adminOrganizationalUnitService?.formPresentational?.onSubmit(),
      displayCondition: () => this.isEdit,
      disableCondition: () => isNullOrUndefined(this._adminOrganizationalUnitService?.formPresentational) || this._adminOrganizationalUnitService?.formPresentational?.form?.pristine || this._adminOrganizationalUnitService?.formPresentational?.form?.invalid,
    },
    {
      color: "primary",
      typeButton: "button",
      icon: IconNameEnum.create,
      labelToTranslate: (current) => MARK_AS_TRANSLATABLE("admin.organizationalUnit.button.createAdditionalFieldsForm"),
      order: 40,
      callback: () => this._createAdditionalFieldsFromScratch(this._resId),
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _adminOrganizationalUnitService: AdminOrganizationalUnitService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_organizationalUnit, _injector, adminOrganizationalUnitActionNameSpace, StateEnum.admin);

    // this.subscribe(this._adminOrganizationalUnitService.detectChangesObs.pipe(tap(() => {
    //   this._changeDetector.detectChanges();
    // })));
  }

  ngOnInit(): void {
    // DON'T CALL SUPER TO PREVENT TO LOAD RESOURCE TWICE, ALREADY DONE ON METADATA COMPONENT
    this.retrieveCurrentModelWithUrl();
    this.subscribe(this.getRetrieveEditModeObs());
  }

  setCurrentTab($event: Tab): void {
    this._currentTab = $event;
  }

  protected _getSubResourceWithParentId(id: string): void {
  }

  private _createAdditionalFieldsFromScratch(_resId: string): void {
    DialogUtil.open(this._dialog, AdminOrganizationalUnitAdditionalFieldsDetailCreateUpdateDialog, {
      parentResId: this._resId,
      additionalFieldsForm: undefined,
      mode: "create",
    });
  }

  override backToDetail(): void {
    super.backToDetail([...this._rootUrl, AdminRoutesEnum.organizationalUnitData]);
  }
}

enum TabEnum {
  DATA = "DATA",
  ADDITIONAL_FIELDS_FORM = "ADDITIONAL_FIELDS_FORM",
}
