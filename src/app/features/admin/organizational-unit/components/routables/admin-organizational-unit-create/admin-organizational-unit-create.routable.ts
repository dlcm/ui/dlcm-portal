/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {adminOrganizationalUnitActionNameSpace} from "@admin/organizational-unit/stores/admin-organizational-unit.action";
import {
  AdminOrganizationalUnitState,
  AdminOrganizationalUnitStateModel,
} from "@admin/organizational-unit/stores/admin-organizational-unit.state";
import {AdminOrganizationalUnitFundingAgencyState} from "@admin/organizational-unit/stores/funding-agency/admin-organizational-unit-funding-agency.state";
import {AdminOrganizationalUnitInstitutionState} from "@admin/organizational-unit/stores/institution/admin-organizational-unit-institution.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {AppState} from "@app/stores/app.state";
import {AppAuthorizedInstitutionState} from "@app/stores/authorized-institution/app-authorized-institution.state";
import {Enums} from "@enums";
import {
  DisseminationPolicy,
  FundingAgency,
  Institution,
  OrganizationalUnit,
  PreservationPolicy,
  Role,
  SubmissionPolicy,
  SystemProperty,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedDisseminationPolicyState} from "@shared/stores/dissemination-policy/shared-dissemination-policy.state";
import {sharedOrganizationalUnitActionNameSpace} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedPreservationPolicyState} from "@shared/stores/preservation-policy/shared-preservation-policy.state";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {SharedSubjectAreaAction} from "@shared/stores/subject-area/shared-subject-area.action";
import {SharedSubjectAreaState} from "@shared/stores/subject-area/shared-subject-area.state";
import {SharedSubmissionPolicyState} from "@shared/stores/submission-policy/shared-submission-policy.state";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  AppSystemPropertyState,
  MemoizedUtil,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-organizational-unit-create-routable",
  templateUrl: "./admin-organizational-unit-create.routable.html",
  styleUrls: ["./admin-organizational-unit-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOrganizationalUnitCreateRoutable extends AbstractCreateRoutable<OrganizationalUnit, AdminOrganizationalUnitStateModel> implements OnInit, OnDestroy {
  listSubmissionPoliciesObs: Observable<SubmissionPolicy[]> = MemoizedUtil.list(this._store, SharedSubmissionPolicyState);
  @Select(AdminOrganizationalUnitState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminOrganizationalUnitState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;
  listPreservationPoliciesObs: Observable<PreservationPolicy[]> = MemoizedUtil.list(this._store, SharedPreservationPolicyState);
  listDisseminationPoliciesObs: Observable<DisseminationPolicy[]> = MemoizedUtil.list(this._store, SharedDisseminationPolicyState);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);
  listManagedInstitutionObs: Observable<Institution[]> = MemoizedUtil.select(this._store, AppAuthorizedInstitutionState, state => state.listManagedInstitutions);
  selectedFundingAgenciesObs: Observable<FundingAgency[]> = MemoizedUtil.selected(this._store, AdminOrganizationalUnitFundingAgencyState);
  selectedInstitutionsObs: Observable<Institution[]> = MemoizedUtil.selected(this._store, AdminOrganizationalUnitInstitutionState);
  subjectAreaSourcesObs: Observable<string[]> = MemoizedUtil.select(this._store, SharedSubjectAreaState, state => state.sources);
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);
  currentLanguageObs: Observable<Enums.Language.LanguageEnum> = MemoizedUtil.select(this._store, AppState, state => state.appLanguage);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedOrganizationalUnitActionNameSpace;

  selectedPersonRole: PersonRole[] = [];

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector,
              readonly securityService: SecurityService) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_organizationalUnit, _injector, adminOrganizationalUnitActionNameSpace, StateEnum.admin);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._store.dispatch(new SharedSubjectAreaAction.GetSource());
  }
}
