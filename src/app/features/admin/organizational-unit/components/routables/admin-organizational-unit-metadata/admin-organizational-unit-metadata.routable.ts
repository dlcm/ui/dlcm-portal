/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-metadata.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {AdminOrganizationalUnitFormPresentational} from "@admin/organizational-unit/components/presentationals/admin-organizational-unit-form/admin-organizational-unit-form.presentational";
import {AdminOrganizationalUnitService} from "@admin/organizational-unit/services/admin-organizational-unit.service";
import {adminOrganizationalUnitActionNameSpace} from "@admin/organizational-unit/stores/admin-organizational-unit.action";
import {
  AdminOrganizationalUnitState,
  AdminOrganizationalUnitStateModel,
} from "@admin/organizational-unit/stores/admin-organizational-unit.state";
import {AdminOrganizationalUnitDisseminationPolicyAction} from "@admin/organizational-unit/stores/dissemination-policy/admin-organizational-unit-dissemination-policy.action";
import {AdminOrganizationalUnitDisseminationPolicyState} from "@admin/organizational-unit/stores/dissemination-policy/admin-organizational-unit-dissemination-policy.state";
import {AdminOrganizationalUnitFundingAgencyAction} from "@admin/organizational-unit/stores/funding-agency/admin-organizational-unit-funding-agency.action";
import {AdminOrganizationalUnitFundingAgencyState} from "@admin/organizational-unit/stores/funding-agency/admin-organizational-unit-funding-agency.state";
import {AdminOrganizationalUnitInstitutionAction} from "@admin/organizational-unit/stores/institution/admin-organizational-unit-institution.action";
import {AdminOrganizationalUnitInstitutionState} from "@admin/organizational-unit/stores/institution/admin-organizational-unit-institution.state";
import {AdminOrganizationalUnitPersonRoleAction} from "@admin/organizational-unit/stores/organizational-unit-person-role/admin-organizational-unit-person-role.action";
import {AdminOrganizationalUnitPersonRoleState} from "@admin/organizational-unit/stores/organizational-unit-person-role/admin-organizational-unit-person-role.state";
import {AdminOrganizationalUnitPreservationPolicyAction} from "@admin/organizational-unit/stores/preservation-policy/admin-organizational-unit-preservation-policy.action";
import {AdminOrganizationalUnitPreservationPolicyState} from "@admin/organizational-unit/stores/preservation-policy/admin-organizational-unit-preservation-policy.state";
import {AdminOrganizationalUnitSubjectAreaState} from "@admin/organizational-unit/stores/subject-area/admin-organizational-unit-subject-area-state.service";
import {AdminOrganizationalUnitSubjectAreaAction} from "@admin/organizational-unit/stores/subject-area/admin-organizational-unit-subject-area.action";
import {AdminOrganizationalUnitSubmissionPolicyAction} from "@admin/organizational-unit/stores/submission-policy/admin-organizational-unit-submission-policy.action";
import {AdminOrganizationalUnitSubmissionPolicyState} from "@admin/organizational-unit/stores/submission-policy/admin-organizational-unit-submission-policy.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {AppState} from "@app/stores/app.state";
import {AppAuthorizedInstitutionState} from "@app/stores/authorized-institution/app-authorized-institution.state";
import {Enums} from "@enums";
import {HomeHelper} from "@home/helpers/home.helper";
import {PortalSearch} from "@home/models/portal-search.model";
import {
  DisseminationPolicy,
  FundingAgency,
  Institution,
  OrganizationalUnit,
  OrganizationalUnitDisseminationPolicyContainer,
  PreservationPolicy,
  Role,
  SubjectArea,
  SubmissionPolicy,
  SystemProperty,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {
  AdminRoutesEnum,
  DepositRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedDisseminationPolicyState} from "@shared/stores/dissemination-policy/shared-dissemination-policy.state";
import {sharedOrganizationalUnitActionNameSpace} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedPreservationPolicyState} from "@shared/stores/preservation-policy/shared-preservation-policy.state";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {SharedSubjectAreaState} from "@shared/stores/subject-area/shared-subject-area.state";
import {SharedSubmissionPolicyState} from "@shared/stores/submission-policy/shared-submission-policy.state";
import {Observable} from "rxjs";
import {
  filter,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  AppSystemPropertyState,
  MappingObject,
  MemoizedUtil,
  ResourceNameSpace,
} from "solidify-frontend";
import {AdminOrganizationalUnitPersonInheritedRoleAction} from "../../../stores/organizational-unit-person-inherited-role/admin-organizational-unit-person-inherited-role.action";
import {AdminOrganizationalUnitPersonInheritedRoleState} from "../../../stores/organizational-unit-person-inherited-role/admin-organizational-unit-person-inherited-role.state";

@Component({
  selector: "dlcm-admin-organizational-unit-detail-edit-routable",
  templateUrl: "./admin-organizational-unit-metadata.routable.html",
  styleUrls: ["./admin-organizational-unit-metadata.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOrganizationalUnitMetadataRoutable extends AbstractDetailEditCommonRoutable<OrganizationalUnit, AdminOrganizationalUnitStateModel> implements OnInit, OnDestroy {
  @Select(AdminOrganizationalUnitState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminOrganizationalUnitState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  selectedSubmissionPoliciesObs: Observable<SubmissionPolicy[]> = MemoizedUtil.selected(this._store, AdminOrganizationalUnitSubmissionPolicyState);
  selectedPreservationPoliciesObs: Observable<PreservationPolicy[]> = MemoizedUtil.selected(this._store, AdminOrganizationalUnitPreservationPolicyState);
  selectedDisseminationPoliciesObs: Observable<OrganizationalUnitDisseminationPolicyContainer[]> = MemoizedUtil.selected(this._store, AdminOrganizationalUnitDisseminationPolicyState);
  selectedFundingAgenciesObs: Observable<FundingAgency[]> = MemoizedUtil.selected(this._store, AdminOrganizationalUnitFundingAgencyState);
  selectedSubjectAreasObs: Observable<SubjectArea[]> = MemoizedUtil.selected(this._store, AdminOrganizationalUnitSubjectAreaState);
  listSubmissionPoliciesObs: Observable<SubmissionPolicy[]> = MemoizedUtil.list(this._store, SharedSubmissionPolicyState);
  listPreservationPoliciesObs: Observable<PreservationPolicy[]> = MemoizedUtil.list(this._store, SharedPreservationPolicyState);
  listDisseminationPoliciesObs: Observable<DisseminationPolicy[]> = MemoizedUtil.list(this._store, SharedDisseminationPolicyState);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);
  listManagedInstitutionObs: Observable<Institution[]> = MemoizedUtil.select(this._store, AppAuthorizedInstitutionState, state => state.listManagedInstitutions);
  selectedPersonRoleObs: Observable<PersonRole[]> = MemoizedUtil.selected(this._store, AdminOrganizationalUnitPersonRoleState);
  listInheritedPersonRolesObs: Observable<PersonRole[]> = MemoizedUtil.select(this._store, AdminOrganizationalUnitPersonInheritedRoleState, state => state.list);
  selectedInstitutionsObs: Observable<Institution[]> = MemoizedUtil.selected(this._store, AdminOrganizationalUnitInstitutionState);
  subjectAreaSourcesObs: Observable<string[]> = MemoizedUtil.select(this._store, SharedSubjectAreaState, state => state.sources);
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);
  currentLanguageObs: Observable<Enums.Language.LanguageEnum> = MemoizedUtil.select(this._store, AppState, state => state.appLanguage);
  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedOrganizationalUnitActionNameSpace;

  @ViewChild("formPresentational")
  readonly formPresentational: AdminOrganizationalUnitFormPresentational;

  readonly KEY_PARAM_NAME: keyof OrganizationalUnit & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _adminOrganizationalUnitService: AdminOrganizationalUnitService,
              readonly securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_organizationalUnit, _injector, adminOrganizationalUnitActionNameSpace, StateEnum.admin);
  }

  ngOnInit(): void {
    // DON'T CALL SUPER TO PREVENT TO GET ID TWICE, ALREADY DONE ON PARENT COMPONENT DETAIL EDIT
    this._retrieveResIdFromUrl();
    this._getSubResourceWithParentId(this._resId);
    this.retrieveResource();
    this.subscribe(this.getRetrieveEditModeObs());

    this.subscribe(this.isReadyToBeDisplayedObs.pipe(
      filter(ready => ready),
      tap(ready => {
        setTimeout(() => {
          this._adminOrganizationalUnitService.formPresentational = this.formPresentational;
        }, 0);
      })),
    );
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new AdminOrganizationalUnitSubmissionPolicyAction.GetAll(id));
    this._store.dispatch(new AdminOrganizationalUnitPreservationPolicyAction.GetAll(id));
    this._store.dispatch(new AdminOrganizationalUnitDisseminationPolicyAction.GetAll(id));
    this._store.dispatch(new AdminOrganizationalUnitPersonRoleAction.GetAll(id));
    this._store.dispatch(new AdminOrganizationalUnitPersonInheritedRoleAction.GetAll(id));
    this._store.dispatch(new AdminOrganizationalUnitFundingAgencyAction.GetAll(id));
    this._store.dispatch(new AdminOrganizationalUnitInstitutionAction.GetAll(id));
    this._store.dispatch(new AdminOrganizationalUnitSubjectAreaAction.GetAll(id));
  }

  override edit(): void {
    if (this.isEdit) {
      return;
    }
    this._store.dispatch(new Navigate([RoutesEnum.adminOrganizationalUnitDetail, this._resId, AdminRoutesEnum.organizationalUnitData, DepositRoutesEnum.edit], {}, {skipLocationChange: true}));
  }

  protected override _cleanState(): void {
  }

  seeArchive(orgUnit: OrganizationalUnit): void {
    const portalSearch = {
      facetsSelected: {[Enums.Facet.Name.ORG_UNIT_FACET]: [orgUnit.name]} as MappingObject<Enums.Facet.Name, string[]>,
    } as PortalSearch;

    HomeHelper.navigateToSearch(this._store, portalSearch);
  }
}
