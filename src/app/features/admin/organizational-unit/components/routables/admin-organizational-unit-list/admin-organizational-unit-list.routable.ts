/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminOrganizationalUnitAction,
  adminOrganizationalUnitActionNameSpace,
} from "@admin/organizational-unit/stores/admin-organizational-unit.action";
import {
  AdminOrganizationalUnitState,
  AdminOrganizationalUnitStateModel,
} from "@admin/organizational-unit/stores/admin-organizational-unit.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {OrganizationalUnit} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {
  AdminRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  CookieConsentUtil,
  CookieType,
  DataTableColumns,
  DataTableFieldTypeEnum,
  isNotNullNorUndefined,
  isNullOrUndefined,
  LocalStorageHelper,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

const onlyMyOrgUnitsStorageKey: LocalStorageEnum = LocalStorageEnum.adminShowOnlyMyInstitutions;
const onlyMyOrgUnitsFn: () => boolean = () => {
  const storageValue = LocalStorageHelper.getItem(onlyMyOrgUnitsStorageKey);
  if (isNullOrUndefined(storageValue)) {
    return true;
  }
  return storageValue === SOLIDIFY_CONSTANTS.STRING_TRUE;
};

@Component({
  selector: "dlcm-admin-organizational-unit-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-organizational-unit-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOrganizationalUnitListRoutable extends AbstractListRoutable<OrganizationalUnit, AdminOrganizationalUnitStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof OrganizationalUnit & string = "name";

  adminOrganizationalUnitState: typeof AdminOrganizationalUnitState = AdminOrganizationalUnitState;

  private _onlyMyOrgUnits: boolean = undefined;

  protected get onlyMyOrgUnits(): boolean {
    if (this._securityService.isRootOrAdmin()) {
      return false;
    }
    if (isNotNullNorUndefined(this._onlyMyOrgUnits)) {
      return this._onlyMyOrgUnits;
    }
    return onlyMyOrgUnitsFn();
  }

  protected set onlyMyOrgUnits(value: boolean) {
    this._onlyMyOrgUnits = value;
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, onlyMyOrgUnitsStorageKey)) {
      LocalStorageHelper.setItem(onlyMyOrgUnitsStorageKey, this.onlyMyOrgUnits + "");
    }
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_organizationalUnit, adminOrganizationalUnitActionNameSpace, _injector, {
      listExtraButtons: [
        {
          color: "primary",
          icon: null,
          labelToTranslate: (current) => LabelTranslateEnum.showOnlyMyOrganisationalUnits,
          callback: (model, buttonElementRef, checked) => this._showOnlyMyOrgUnit(checked),
          order: 40,
          isToggleButton: true,
          isToggleChecked: !_securityService.isRootOrAdmin() && LocalStorageHelper.getItem(LocalStorageEnum.preservationSpaceOrganizationalUnitShowOnlyMine) === SOLIDIFY_CONSTANTS.STRING_TRUE,
          displayCondition: current => this._securityService.isRootOrAdmin(),
        },
      ],
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: OrganizationalUnit | undefined): boolean {
    return this._securityService.isManagerOfOrgUnit(model.resId);
  }

  conditionDisplayDeleteButton(model: OrganizationalUnit | undefined): boolean {
    return this._securityService.isManagerOfOrgUnit(model.resId);
  }

  private _showOnlyMyOrgUnit(checked: boolean): void {
    if (checked) {
      //show only authorized org units
      const currentQueryParam = MemoizedUtil.queryParametersSnapshot(this._store, AdminOrganizationalUnitState);
      const newQueryParam = new QueryParameters(currentQueryParam?.paging?.pageSize);
      this._store.dispatch(new AdminOrganizationalUnitAction.LoadOnlyMyOrgUnits(newQueryParam));
      this.onlyMyOrgUnits = true;
    } else {
      // all org units
      this._store.dispatch(new AdminOrganizationalUnitAction.GetAll());
      this.onlyMyOrgUnits = false;
    }

    this.defineColumns();
    this._changeDetector.detectChanges();
  }

  override onQueryParametersEvent(queryParameters: QueryParameters): void {
    if (this.onlyMyOrgUnits) {
      this._store.dispatch(new AdminOrganizationalUnitAction.LoadOnlyMyOrgUnits(queryParameters));
    } else {
      this._store.dispatch(new AdminOrganizationalUnitAction.ChangeQueryParameters(queryParameters, true));
    }
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  override goToEdit(model: OrganizationalUnit): void {
    this._store.dispatch(new Navigate([RoutesEnum.adminOrganizationalUnitDetail, model.resId, AdminRoutesEnum.organizationalUnitData, AdminRoutesEnum.organizationalUnitEdit], {}, {skipLocationChange: true}));
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "avatar" as any,
        header: LabelTranslateEnum.logo,
        order: OrderEnum.none,
        resourceNameSpace: adminOrganizationalUnitActionNameSpace,
        resourceState: this.adminOrganizationalUnitState as any,
        isFilterable: false,
        isSortable: !this.onlyMyOrgUnits,
        component: DataTableComponentHelper.get(DataTableComponentEnum.logo),
        isUser: false,
        skipIfAvatarMissing: true,
        idResource: (organizationalUnit: OrganizationalUnit) => organizationalUnit.resId,
        isLogoPresent: (organizationalUnit: OrganizationalUnit) => isNotNullNorUndefined(organizationalUnit.logo),
        width: "45px",
      } as DataTableColumns<OrganizationalUnit>,
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: !this.onlyMyOrgUnits,
        dataTest: DataTestEnum.adminOrgUnitListSearchName,
      },
      {
        field: "openingDate",
        header: LabelTranslateEnum.opening,
        type: DataTableFieldTypeEnum.date,
        order: OrderEnum.descending,
        isFilterable: false,
        isSortable: !this.onlyMyOrgUnits,
      },
      {
        field: "closingDate" as any,
        header: LabelTranslateEnum.closing,
        type: DataTableFieldTypeEnum.date,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: !this.onlyMyOrgUnits,
      },
      {
        field: "resId",
        header: "",
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        component: DataTableComponentHelper.get(DataTableComponentEnum.organizationalUnitMember),
        isFilterable: false,
        isSortable: false,
        width: "40px",
      },
    ];
  }
}
