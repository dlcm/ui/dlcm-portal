/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-additional-fields-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminOrganizationalUnitAdditionalFieldsDetailCreateUpdateDialog} from "@admin/organizational-unit/components/dialogs/admin-organizational-unit-additional-fields-detail-create-update/admin-organizational-unit-additional-fields-detail-create-update.dialog";
import {AdminOrganizationalUnitAdditionalFieldsRenameDialog} from "@admin/organizational-unit/components/dialogs/admin-organizational-unit-additional-fields-rename/admin-organizational-unit-additional-fields-rename.dialog";
import {AdminOrganizationalUnitAdditionalFieldsFormAction} from "@admin/organizational-unit/stores/additional-fields-form/admin-organizational-unit-additional-fields-form.action";
import {AdminOrganizationalUnitAdditionalFieldsFormState} from "@admin/organizational-unit/stores/additional-fields-form/admin-organizational-unit-additional-fields-form.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  AdditionalFieldsForm,
  DepositDataFile,
} from "@models";
import {Store} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {Observable} from "rxjs";
import {
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-organizational-unit-additional-fields-list-routable",
  templateUrl: "./admin-organizational-unit-additional-fields-list.routable.html",
  styleUrls: ["./admin-organizational-unit-additional-fields-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOrganizationalUnitAdditionalFieldsListRoutable {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AdminOrganizationalUnitAdditionalFieldsFormState);

  _orgUnitResId: string;
  _resId: string;

  additionalFieldsFormObs: Observable<AdditionalFieldsForm[]> = MemoizedUtil.list(this._store, AdminOrganizationalUnitAdditionalFieldsFormState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, AdminOrganizationalUnitAdditionalFieldsFormState, (state) => state.queryParameters);

  columns: DataTableColumns<AdditionalFieldsForm>[];

  actions: DataTableActions<DepositDataFile>[] = [
    {
      logo: IconNameEnum.edit,
      callback: (additionalFieldsForm: AdditionalFieldsForm) => this._renameVersion(this._resId, additionalFieldsForm),
      placeholder: current => LabelTranslateEnum.rename,
      displayOnCondition: () => true,
    },
    {
      logo: IconNameEnum.create,
      callback: (additionalFieldsForm: AdditionalFieldsForm) => this._createNewVersionFromExistingOne(this._resId, additionalFieldsForm),
      placeholder: current => MARK_AS_TRANSLATABLE("crud.list.action.createNewVersionFromExistingOne"),
      displayOnCondition: () => true,
    },
    {
      logo: IconNameEnum.delete,
      callback: (additionalFieldsForm: AdditionalFieldsForm) => this._delete(additionalFieldsForm.resId),
      placeholder: current => LabelTranslateEnum.delete,
      displayOnCondition: () => true,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              private readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog) {
    this._retrieveResIdFromUrl();
    this.defineColumns();
  }

  protected _retrieveResIdFromUrl(): void {
    this._orgUnitResId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    if (isNullOrUndefined(this._orgUnitResId)) {
      this._orgUnitResId = this._route.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    }
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new AdminOrganizationalUnitAdditionalFieldsFormAction.ChangeQueryParameters(this._orgUnitResId, queryParameters));
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.ascending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  select(additionalFieldsForm: AdditionalFieldsForm): void {
    DialogUtil.open(this._dialog, AdminOrganizationalUnitAdditionalFieldsDetailCreateUpdateDialog, {
      parentResId: this._orgUnitResId,
      additionalFieldsForm: additionalFieldsForm,
      mode: "detail",
    });
  }

  private _renameVersion(_resId: string, additionalFieldsForm: AdditionalFieldsForm): void {
    DialogUtil.open(this._dialog, AdminOrganizationalUnitAdditionalFieldsRenameDialog, {
      parentResId: this._orgUnitResId,
      additionalFieldsForm: additionalFieldsForm,
    });
  }

  private _createNewVersionFromExistingOne(_resId: string, additionalFieldsForm: AdditionalFieldsForm): void {
    DialogUtil.open(this._dialog, AdminOrganizationalUnitAdditionalFieldsDetailCreateUpdateDialog, {
      parentResId: this._orgUnitResId,
      additionalFieldsForm: additionalFieldsForm,
      mode: "update",
    });
  }

  private _delete(resId: string): void {
    this._store.dispatch(new AdminOrganizationalUnitAdditionalFieldsFormAction.Delete(this._orgUnitResId, resId));
  }
}
