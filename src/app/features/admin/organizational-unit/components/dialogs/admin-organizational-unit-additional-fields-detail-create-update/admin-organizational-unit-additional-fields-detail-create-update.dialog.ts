/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-additional-fields-detail-create-update.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminOrganizationalUnitAdditionalFieldsFormAction} from "@admin/organizational-unit/stores/additional-fields-form/admin-organizational-unit-additional-fields-form.action";
import {AdminOrganizationalUnitAdditionalFieldsFormState} from "@admin/organizational-unit/stores/additional-fields-form/admin-organizational-unit-additional-fields-form.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  AdditionalFieldsForm,
  OaiSet,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {Observable} from "rxjs";
import {
  AbstractFormPresentational,
  FormControlKey,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  ModelFormControlEvent,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-organizational-unit-additional-fields-detail-create-update-dialog",
  templateUrl: "./admin-organizational-unit-additional-fields-detail-create-update.dialog.html",
  styleUrls: ["./admin-organizational-unit-additional-fields-detail-create-update.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOrganizationalUnitAdditionalFieldsDetailCreateUpdateDialog extends SharedAbstractDialog<AdminOrganizationalUnitAdditionalFieldsDetailCreateEditDialogData, AdditionalFieldsForm> implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AdminOrganizationalUnitAdditionalFieldsFormState);
  confirmToTranslate: string;

  @ViewChild("formPresentational")
  readonly formPresentational: AbstractFormPresentational<OaiSet>;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _dialogRef: MatDialogRef<AdminOrganizationalUnitAdditionalFieldsDetailCreateUpdateDialog>,
              @Inject(MAT_DIALOG_DATA) readonly data: AdminOrganizationalUnitAdditionalFieldsDetailCreateEditDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (this.data.mode === "update") {
      this.titleToTranslate = MARK_AS_TRANSLATABLE("admin.organizationalUnit.additionalFields.modal.detailCreateUpdate.title.update");
      this.confirmToTranslate = MARK_AS_TRANSLATABLE("admin.organizationalUnit.additionalFields.modal.detailCreateUpdate.button.confirm");
    } else if (this.data.mode === "detail") {
      this.titleToTranslate = MARK_AS_TRANSLATABLE("admin.organizationalUnit.additionalFields.modal.detailCreateUpdate.title.detail");
      this.confirmToTranslate = MARK_AS_TRANSLATABLE("admin.organizationalUnit.additionalFields.modal.detailCreateUpdate.button.confirm");
    } else if (this.data.mode === "create") {
      this.titleToTranslate = MARK_AS_TRANSLATABLE("admin.organizationalUnit.additionalFields.modal.detailCreateUpdate.title.create");
      this.confirmToTranslate = MARK_AS_TRANSLATABLE("admin.organizationalUnit.additionalFields.modal.detailCreateUpdate.button.confirm");
    }
  }

  update($event: ModelFormControlEvent<AdditionalFieldsForm>): void {
  }

  checkAvailable($event: FormControlKey): void {
  }

  updateCanDeactivate($event: boolean): void {
  }

  confirm(additionalFieldsForm: AdditionalFieldsForm): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new AdminOrganizationalUnitAdditionalFieldsFormAction.Create(this.data.parentResId, additionalFieldsForm),
      AdminOrganizationalUnitAdditionalFieldsFormAction.CreateSuccess,
      resultAction => {
        this._dialogRef.close();
      }));
  }
}

export interface AdminOrganizationalUnitAdditionalFieldsDetailCreateEditDialogData {
  parentResId: string;
  additionalFieldsForm: AdditionalFieldsForm;
  mode: "update" | "create" | "detail";
}
