/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit-additional-fields-rename.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminOrganizationalUnitAdditionalFieldsFormAction} from "@admin/organizational-unit/stores/additional-fields-form/admin-organizational-unit-additional-fields-form.action";
import {AdminOrganizationalUnitAdditionalFieldsFormState} from "@admin/organizational-unit/stores/additional-fields-form/admin-organizational-unit-additional-fields-form.state";
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {AdditionalFieldsForm} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {Observable} from "rxjs";
import {
  FormValidationHelper,
  MemoizedUtil,
  PropertyName,
  SolidifyValidator,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-organizational-unit-additional-fields-rename-dialog",
  templateUrl: "./admin-organizational-unit-additional-fields-rename.dialog.html",
  styleUrls: ["./admin-organizational-unit-additional-fields-rename.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminOrganizationalUnitAdditionalFieldsRenameDialog extends SharedAbstractDialog<AdminOrganizationalUnitAdditionalFieldsRenameDialogData> implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AdminOrganizationalUnitAdditionalFieldsFormState);

  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<AdminOrganizationalUnitAdditionalFieldsRenameDialog>,
              @Inject(MAT_DIALOG_DATA) protected readonly _data: AdminOrganizationalUnitAdditionalFieldsRenameDialogData,
              private readonly _fb: FormBuilder) {
    super(_dialogRef, _data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.name]: [this.data.additionalFieldsForm.name, SolidifyValidator],
    });
  }

  close(): void {
    this._dialogRef.close();
  }

  onSubmit(): void {
    const action = new AdminOrganizationalUnitAdditionalFieldsFormAction.Update(this.data.parentResId, {
      resId: this.data.additionalFieldsForm.resId,
      name: this.form.get(this.formDefinition.name).value,
    });

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      action,
      AdminOrganizationalUnitAdditionalFieldsFormAction.UpdateSuccess,
      resultAction => {
        this._dialogRef.close();
        this._store.dispatch(new AdminOrganizationalUnitAdditionalFieldsFormAction.GetAll(this.data.parentResId));
      }));
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
}

export interface AdminOrganizationalUnitAdditionalFieldsRenameDialogData {
  parentResId: string;
  additionalFieldsForm: AdditionalFieldsForm;
}
