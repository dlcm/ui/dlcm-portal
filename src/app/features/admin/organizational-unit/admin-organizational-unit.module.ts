/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-organizational-unit.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminOrganizationalUnitRoutingModule} from "@admin/organizational-unit/admin-organizational-unit-routing.module";
import {AdminOrganizationalUnitAdditionalFieldsDetailCreateUpdateDialog} from "@admin/organizational-unit/components/dialogs/admin-organizational-unit-additional-fields-detail-create-update/admin-organizational-unit-additional-fields-detail-create-update.dialog";
import {AdminOrganizationalUnitAdditionalFieldsRenameDialog} from "@admin/organizational-unit/components/dialogs/admin-organizational-unit-additional-fields-rename/admin-organizational-unit-additional-fields-rename.dialog";
import {AdminOrganizationalUnitAdditionalFieldsFormPresentational} from "@admin/organizational-unit/components/presentationals/admin-organizational-unit-additional-fields-form/admin-organizational-unit-additional-fields-form.presentational";
import {AdminOrganizationalUnitFormPresentational} from "@admin/organizational-unit/components/presentationals/admin-organizational-unit-form/admin-organizational-unit-form.presentational";
import {AdminOrganizationalUnitAdditionalFieldsListRoutable} from "@admin/organizational-unit/components/routables/admin-organizational-unit-additional-fields-list/admin-organizational-unit-additional-fields-list.routable";
import {AdminOrganizationalUnitCreateRoutable} from "@admin/organizational-unit/components/routables/admin-organizational-unit-create/admin-organizational-unit-create.routable";
import {AdminOrganizationalUnitDetailEditRoutable} from "@admin/organizational-unit/components/routables/admin-organizational-unit-detail-edit/admin-organizational-unit-detail-edit.routable";
import {AdminOrganizationalUnitListRoutable} from "@admin/organizational-unit/components/routables/admin-organizational-unit-list/admin-organizational-unit-list.routable";
import {AdminOrganizationalUnitMetadataRoutable} from "@admin/organizational-unit/components/routables/admin-organizational-unit-metadata/admin-organizational-unit-metadata.routable";
import {AdminOrganizationalUnitService} from "@admin/organizational-unit/services/admin-organizational-unit.service";
import {AdminOrganizationalUnitState} from "@admin/organizational-unit/stores/admin-organizational-unit.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminOrganizationalUnitCreateRoutable,
  AdminOrganizationalUnitDetailEditRoutable,
  AdminOrganizationalUnitListRoutable,
  AdminOrganizationalUnitMetadataRoutable,
  AdminOrganizationalUnitAdditionalFieldsListRoutable,
];
const containers = [];
const dialogs = [
  AdminOrganizationalUnitAdditionalFieldsDetailCreateUpdateDialog,
  AdminOrganizationalUnitAdditionalFieldsRenameDialog,
];
const presentationals = [
  AdminOrganizationalUnitFormPresentational,
  AdminOrganizationalUnitAdditionalFieldsFormPresentational,
];
const services = [
  AdminOrganizationalUnitService,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminOrganizationalUnitRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminOrganizationalUnitState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [
    ...services,
  ],
})
export class AdminOrganizationalUnitModule {
}
