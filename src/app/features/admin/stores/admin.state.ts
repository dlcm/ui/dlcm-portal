/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminArchiveAclState} from "@admin/archive-acl/stores/admin-archive-acl.state";
import {
  AdminArchiveTypeState,
  AdminArchiveTypeStateModel,
} from "@admin/archive-type/stores/admin-archive-type.state";
import {
  AdminDisseminationPolicyState,
  AdminDisseminationPolicyStateModel,
} from "@admin/dissemination-policy/stores/admin-dissemination-policy.state";
import {
  AdminFundingAgenciesState,
  AdminFundingAgenciesStateModel,
} from "@admin/funding-agencies/stores/admin-funding-agencies.state";
import {
  AdminInstitutionState,
  AdminInstitutionStateModel,
} from "@admin/institution/stores/admin-institution.state";
import {
  AdminLanguageState,
  AdminLanguageStateModel,
} from "@admin/language/stores/admin-language.state";
import {
  AdminLicenseState,
  AdminLicenseStateModel,
} from "@admin/license/stores/admin-license.state";
import {
  AdminMetadataTypeState,
  AdminMetadataTypeStateModel,
} from "@admin/metadata-type/stores/admin-metadata-type.state";
import {
  AdminNotificationState,
  AdminNotificationStateModel,
} from "@admin/notification/stores/admin-notification.state";
import {
  AdminOrganizationalUnitState,
  AdminOrganizationalUnitStateModel,
} from "@admin/organizational-unit/stores/admin-organizational-unit.state";
import {
  AdminPersonState,
  AdminPersonStateModel,
} from "@admin/person/stores/admin-person.state";
import {
  AdminPreservationPolicyState,
  AdminPreservationPolicyStateModel,
} from "@admin/preservation-policy/stores/admin-preservation-policy.state";
import {
  AdminRoleState,
  AdminRoleStateModel,
} from "@admin/role/stores/admin-role.state";
import {
  AdminScheduledTaskState,
  AdminScheduledTaskStateModel,
} from "@admin/scheduled-task/stores/admin-scheduled-task.state";
import {
  AdminSubjectAreaState,
  AdminSubjectAreaStateModel,
} from "@admin/subject-area/stores/admin-subject-area-state.service";
import {
  AdminSubmissionAgreementUserState,
  AdminSubmissionAgreementUserStateModel,
} from "@admin/submission-agreement-user/stores/admin-submission-agreement-user.state";
import {
  AdminSubmissionAgreementState,
  AdminSubmissionAgreementStateModel,
} from "@admin/submission-agreement/stores/admin-submission-agreement.state";
import {
  AdminSubmissionPolicyState,
  AdminSubmissionPolicyStateModel,
} from "@admin/submission-policy/stores/admin-submission-policy.state";
import {
  AdminUserState,
  AdminUserStateModel,
} from "@admin/user/stores/admin-user.state";
import {Injectable} from "@angular/core";
import {
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  AdminGlobalBannerState,
  AdminGlobalBannerStateModel,
  AdminIndexFieldAliasState,
  AdminIndexFieldAliasStateModel,
  AdminOaiMetadataPrefixState,
  AdminOaiMetadataPrefixStateModel,
  AdminOaiSetState,
  AdminOaiSetStateModel,
  BaseStateModel,
} from "solidify-frontend";

export interface AdminStateModel extends BaseStateModel {
  [StateEnum.admin_submissionAgreement]: AdminSubmissionAgreementStateModel;
  [StateEnum.admin_submissionAgreementUser]: AdminSubmissionAgreementUserStateModel;
  [StateEnum.admin_submissionPolicy]: AdminSubmissionPolicyStateModel;
  [StateEnum.admin_preservationPolicy]: AdminPreservationPolicyStateModel;
  [StateEnum.admin_archiveType]: AdminArchiveTypeStateModel;
  [StateEnum.admin_license]: AdminLicenseStateModel;
  [StateEnum.admin_globalBanner]: AdminGlobalBannerStateModel;
  [StateEnum.admin_organizationalUnit]: AdminOrganizationalUnitStateModel;
  [StateEnum.admin_institution]: AdminInstitutionStateModel;
  [StateEnum.admin_subjectArea]: AdminSubjectAreaStateModel;
  [StateEnum.admin_user]: AdminUserStateModel;
  [StateEnum.admin_oaiMetadataPrefix]: AdminOaiMetadataPrefixStateModel;
  [StateEnum.admin_oaiSet]: AdminOaiSetStateModel;
  [StateEnum.admin_person]: AdminPersonStateModel;
  [StateEnum.admin_role]: AdminRoleStateModel;
  [StateEnum.admin_fundingAgencies]: AdminFundingAgenciesStateModel;
  [StateEnum.admin_indexFieldAlias]: AdminIndexFieldAliasStateModel;
  [StateEnum.admin_archiveAcl]: AdminIndexFieldAliasStateModel;
  [StateEnum.admin_disseminationPolicy]: AdminDisseminationPolicyStateModel;
  [StateEnum.admin_language]: AdminLanguageStateModel;
  [StateEnum.admin_metadataType]: AdminMetadataTypeStateModel;
  [StateEnum.admin_notification]: AdminNotificationStateModel;
  [StateEnum.admin_scheduledTask]: AdminScheduledTaskStateModel;
}

@Injectable()
@State<AdminStateModel>({
  name: StateEnum.admin,
  defaults: {
    isLoadingCounter: 0,
    [StateEnum.admin_submissionAgreement]: null,
    [StateEnum.admin_submissionAgreementUser]: null,
    [StateEnum.admin_submissionPolicy]: null,
    [StateEnum.admin_preservationPolicy]: null,
    [StateEnum.admin_archiveType]: null,
    [StateEnum.admin_license]: null,
    [StateEnum.admin_globalBanner]: null,
    [StateEnum.admin_organizationalUnit]: null,
    [StateEnum.admin_institution]: null,
    [StateEnum.admin_subjectArea]: null,
    [StateEnum.admin_user]: null,
    [StateEnum.admin_oaiMetadataPrefix]: null,
    [StateEnum.admin_oaiSet]: null,
    [StateEnum.admin_person]: null,
    [StateEnum.admin_role]: null,
    [StateEnum.admin_fundingAgencies]: null,
    [StateEnum.admin_indexFieldAlias]: null,
    [StateEnum.admin_archiveAcl]: null,
    [StateEnum.admin_disseminationPolicy]: null,
    [StateEnum.admin_language]: null,
    [StateEnum.admin_metadataType]: null,
    [StateEnum.admin_notification]: null,
    [StateEnum.admin_scheduledTask]: null,
  },
  children: [
    AdminSubmissionAgreementState,
    AdminSubmissionAgreementUserState,
    AdminSubmissionPolicyState,
    AdminPreservationPolicyState,
    AdminArchiveTypeState,
    AdminLicenseState,
    AdminGlobalBannerState,
    AdminOrganizationalUnitState,
    AdminInstitutionState,
    AdminSubjectAreaState,
    AdminUserState,
    AdminOaiMetadataPrefixState,
    AdminOaiSetState,
    AdminPersonState,
    AdminRoleState,
    AdminFundingAgenciesState,
    AdminIndexFieldAliasState,
    AdminArchiveAclState,
    AdminDisseminationPolicyState,
    AdminLanguageState,
    AdminMetadataTypeState,
    AdminNotificationState,
    AdminScheduledTaskState,
  ],
})
export class AdminState {
  constructor(protected readonly _store: Store) {
  }
}
