/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-preservation-policy.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminPreservationPolicyRoutingModule} from "@admin/preservation-policy/admin-preservation-policy-routing.module";
import {AdminPreservationPolicyFormPresentational} from "@admin/preservation-policy/components/presentationals/admin-preservation-policy-form/admin-preservation-policy-form.presentational";
import {AdminPreservationPolicyCreateRoutable} from "@admin/preservation-policy/components/routables/admin-preservation-policy-create/admin-preservation-policy-create.routable";
import {AdminPreservationPolicyDetailEditRoutable} from "@admin/preservation-policy/components/routables/admin-preservation-policy-detail-edit/admin-preservation-policy-detail-edit.routable";
import {AdminPreservationPolicyListRoutable} from "@admin/preservation-policy/components/routables/admin-preservation-policy-list/admin-preservation-policy-list.routable";
import {AdminPreservationPolicyState} from "@admin/preservation-policy/stores/admin-preservation-policy.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminPreservationPolicyListRoutable,
  AdminPreservationPolicyDetailEditRoutable,
  AdminPreservationPolicyCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminPreservationPolicyFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminPreservationPolicyRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminPreservationPolicyState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminPreservationPolicyModule {
}
