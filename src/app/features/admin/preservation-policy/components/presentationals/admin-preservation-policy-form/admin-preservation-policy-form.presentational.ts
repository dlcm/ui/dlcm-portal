/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-preservation-policy-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {environment} from "@environments/environment";
import {PreservationPolicy} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  AbstractFormPresentational,
  KeyValue,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-preservation-policy-form",
  templateUrl: "./admin-preservation-policy-form.presentational.html",
  styleUrls: ["./admin-preservation-policy-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminPreservationPolicyFormPresentational extends AbstractFormPresentational<PreservationPolicy> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  listStoragions: KeyValue[];

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
    this.listStoragions = environment.archivalStorageName.map((value, index) => new KeyValue(index + "", value));
  }

  protected _bindFormTo(preservationPolicies: PreservationPolicy): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [preservationPolicies.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.dispositionApproval]: [preservationPolicies.dispositionApproval, [SolidifyValidator]],
      [this.formDefinition.retention]: [preservationPolicies.retention, [Validators.required, SolidifyValidator]],
      [this.formDefinition.mainStorage]: [preservationPolicies.mainStorage, [Validators.required, SolidifyValidator]],
    });
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.dispositionApproval]: [false, [SolidifyValidator]],
      [this.formDefinition.retention]: [undefined, [Validators.required, SolidifyValidator]],
      [this.formDefinition.mainStorage]: [environment.defaultStorageIndex, [Validators.required, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(preservationPolicies: PreservationPolicy): PreservationPolicy {
    return preservationPolicies;
  }

}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() dispositionApproval: string;
  @PropertyName() retention: string;
  @PropertyName() mainStorage: string;
}
