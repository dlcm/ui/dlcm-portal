/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-agreement-user.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminSubmissionAgreementUserRoutingModule} from "@admin/submission-agreement-user/admin-submission-agreement-user-routing.module";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {AdminSubmissionAgreementUserListRoutable} from "./components/routables/submission-agreement-user-list/admin-submission-agreement-user-list.routable";

const routables = [
  AdminSubmissionAgreementUserListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminSubmissionAgreementUserRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminSubmissionAgreementUserModule {
}
