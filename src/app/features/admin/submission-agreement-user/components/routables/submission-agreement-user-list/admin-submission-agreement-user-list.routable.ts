/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-agreement-user-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminSubmissionAgreementUserStateModel} from "@admin/submission-agreement-user/stores/admin-submission-agreement-user.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  SubmissionAgreement,
  SubmissionAgreementUser,
  User,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {UserHelper} from "@shared/helpers/user.helper";
import {SecurityService} from "@shared/services/security.service";
import {sharedSubmissionAgreementActionNameSpace} from "@shared/stores/submission-agreement/shared-submission-agreement.action";
import {SharedSubmissionAgreementState} from "@shared/stores/submission-agreement/shared-submission-agreement.state";
import {sharedUserActionNameSpace} from "@shared/stores/user/shared-user.action";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {
  AbstractListRoutable,
  DataTableActions,
  DataTableFieldTypeEnum,
  OrderEnum,
  ResourceNameSpace,
  RouterExtensionService,
  Sort,
} from "solidify-frontend";
import {adminSubmissionAgreementUserActionNameSpace} from "../../../stores/admin-submission-agreement-user.action";

@Component({
  selector: "dlcm-admin-submission-agreement-user-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-submission-agreement-user-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminSubmissionAgreementUserListRoutable extends AbstractListRoutable<SubmissionAgreementUser, AdminSubmissionAgreementUserStateModel> {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof SubmissionAgreementUser & string = undefined;

  override stickyTopPosition: number = 0;

  sharedUserSort: Sort<User> = {
    field: "lastName",
    order: OrderEnum.ascending,
  };
  sharedUserNameSpace: ResourceNameSpace = sharedUserActionNameSpace;
  sharedUserState: typeof SharedUserState = SharedUserState;
  labelUserCallback: (user: User) => string = user => user.lastName + ", " + user.firstName;

  sharedSubmissionAgreementSort: Sort<SubmissionAgreement> = {
    field: "title",
    order: OrderEnum.ascending,
  };
  sharedSubmissionAgreementNameSpace: ResourceNameSpace = sharedSubmissionAgreementActionNameSpace;
  sharedSubmissionAgreementState: typeof SharedSubmissionAgreementState = SharedSubmissionAgreementState;
  labelSubmissionAgreementCallback: (submissionAgreement: SubmissionAgreement) => string = submissionAgreement => submissionAgreement.title + " (" + submissionAgreement.version + ")";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_submissionAgreementUser, adminSubmissionAgreementUserActionNameSpace, _injector,
      {
        canCreate: false,
        canRefresh: true,
        canGoBack: true,
      }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: SubmissionAgreementUser | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: SubmissionAgreementUser | undefined): boolean {
    return false;
  }

  protected override _defineActions(): DataTableActions<SubmissionAgreementUser>[] {
    return [
      ...super._defineActions(),
      {
        logo: IconNameEnum.users,
        callback: (model: SubmissionAgreementUser) => this._store.dispatch(new Navigate([RoutesEnum.adminUserDetail, model.user.resId])),
        placeholder: current => LabelTranslateEnum.seeUserDetail,
      },
      {
        logo: IconNameEnum.submissionAgreements,
        callback: (model: SubmissionAgreementUser) => this._store.dispatch(new Navigate([RoutesEnum.adminSubmissionAgreementDetail, model.submissionAgreement.resId])),
        placeholder: current => LabelTranslateEnum.seeAgreementDetail,
      },
    ];
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "user.fullName" as any,
        header: LabelTranslateEnum.user,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        resourceNameSpace: this.sharedUserNameSpace,
        resourceState: this.sharedUserState as any,
        resourceLabelKey: "lastName",
        resourceLabelCallback: this.labelUserCallback,
        resourceExtraLabelSecondLineCallback: UserHelper.extraInfoSecondLineLabelCallback,
        searchableSingleSelectSort: this.sharedUserSort,
        filterableField: "userId" as any,
        sortableField: "user.firstName" as any,
      },
      {
        field: "submissionAgreement.title" as any,
        header: LabelTranslateEnum.submissionAgreement,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: false,
        resourceNameSpace: this.sharedSubmissionAgreementNameSpace,
        resourceState: this.sharedSubmissionAgreementState as any,
        resourceLabelKey: "title",
        resourceLabelCallback: this.labelSubmissionAgreementCallback,
        resourceExtraLabelSecondLineCallback: UserHelper.extraInfoSecondLineLabelCallback,
        searchableSingleSelectSort: this.sharedSubmissionAgreementSort,
        filterableField: "submissionAgreementId" as any,
        sortableField: "submissionAgreementId" as any,
      },
      {
        field: "approbationTime",
        header: LabelTranslateEnum.approvedThe,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
      },
    ];
  }

  override showDetail(model: SubmissionAgreementUser): void {
  }
}
