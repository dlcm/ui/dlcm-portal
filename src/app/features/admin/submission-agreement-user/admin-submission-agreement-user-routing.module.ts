/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-agreement-user-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminSubmissionAgreementUserListRoutable} from "@admin/submission-agreement-user/components/routables/submission-agreement-user-list/admin-submission-agreement-user-list.routable";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {DlcmRoutes} from "@app/shared/models/dlcm-route.model";
import {AppRoutesEnum} from "@shared/enums/routes.enum";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminSubmissionAgreementUserListRoutable,
    data: {},
    children: [],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminSubmissionAgreementUserRoutingModule {
}
