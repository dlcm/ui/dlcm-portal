/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminArchiveAclState} from "@admin/archive-acl/stores/admin-archive-acl.state";
import {AdminArchiveTypeState} from "@admin/archive-type/stores/admin-archive-type.state";
import {AdminDisseminationPolicyState} from "@admin/dissemination-policy/stores/admin-dissemination-policy.state";
import {AdminFundingAgenciesState} from "@admin/funding-agencies/stores/admin-funding-agencies.state";
import {AdminFundingAgenciesOrganizationalUnitState} from "@admin/funding-agencies/stores/organizational-unit/admin-organizational-unit-funding-agencies.state";
import {AdminInstitutionState} from "@admin/institution/stores/admin-institution.state";
import {AdminInstitutionPersonRoleState} from "@admin/institution/stores/institution-person-role/admin-institution-person-role.state";
import {AdminInstitutionOrganizationalUnitState} from "@admin/institution/stores/organizational-unit/admin-institution-organizational-unit.state";
import {AdminLanguageState} from "@admin/language/stores/admin-language.state";
import {AdminLicenseState} from "@admin/license/stores/admin-license.state";
import {AdminMetadataTypeState} from "@admin/metadata-type/stores/admin-metadata-type.state";
import {AdminNotificationState} from "@admin/notification/stores/admin-notification.state";
import {AdminNotificationStatusHistoryState} from "@admin/notification/stores/status-history/admin-notification-status-history.state";
import {AdminOrganizationalUnitAdditionalFieldsFormState} from "@admin/organizational-unit/stores/additional-fields-form/admin-organizational-unit-additional-fields-form.state";
import {AdminOrganizationalUnitState} from "@admin/organizational-unit/stores/admin-organizational-unit.state";
import {AdminOrganizationalUnitDisseminationPolicyState} from "@admin/organizational-unit/stores/dissemination-policy/admin-organizational-unit-dissemination-policy.state";
import {AdminOrganizationalUnitFundingAgencyState} from "@admin/organizational-unit/stores/funding-agency/admin-organizational-unit-funding-agency.state";
import {AdminOrganizationalUnitInstitutionState} from "@admin/organizational-unit/stores/institution/admin-organizational-unit-institution.state";
import {AdminOrganizationalUnitPersonInheritedRoleState} from "@admin/organizational-unit/stores/organizational-unit-person-inherited-role/admin-organizational-unit-person-inherited-role.state";
import {AdminOrganizationalUnitPersonRoleState} from "@admin/organizational-unit/stores/organizational-unit-person-role/admin-organizational-unit-person-role.state";
import {AdminOrganizationalUnitSubjectAreaState} from "@admin/organizational-unit/stores/subject-area/admin-organizational-unit-subject-area-state.service";
import {AdminPersonState} from "@admin/person/stores/admin-person.state";
import {AdminPersonInstitutionsState} from "@admin/person/stores/institutions/admin-people-institutions.state";
import {AdminPersonInstitutionRoleState} from "@admin/person/stores/person-institution-role/admin-person-institution-role.state";
import {AdminPersonOrgUnitRoleState} from "@admin/person/stores/person-orgunit-role/admin-person-orgunit-role.state";
import {AdminPreservationPolicyState} from "@admin/preservation-policy/stores/admin-preservation-policy.state";
import {AdminRoleState} from "@admin/role/stores/admin-role.state";
import {AdminScheduledTaskState} from "@admin/scheduled-task/stores/admin-scheduled-task.state";
import {AdminSubjectAreaState} from "@admin/subject-area/stores/admin-subject-area-state.service";
import {AdminSubmissionAgreementState} from "@admin/submission-agreement/stores/admin-submission-agreement.state";
import {AdminSubmissionPolicyState} from "@admin/submission-policy/stores/admin-submission-policy.state";
import {AdminUserState} from "@admin/user/stores/admin-user.state";
import {NgModule} from "@angular/core";
import {AdminRoutingModule} from "@app/features/admin/admin-routing.module";
import {AdminHomeRoutable} from "@app/features/admin/components/routables/admin-home/admin-home.routable";
import {AdminOrganizationalUnitPreservationPolicyState} from "@app/features/admin/organizational-unit/stores/preservation-policy/admin-organizational-unit-preservation-policy.state";
import {AdminOrganizationalUnitSubmissionPolicyState} from "@app/features/admin/organizational-unit/stores/submission-policy/admin-organizational-unit-submission-policy.state";
import {AdminState} from "@app/features/admin/stores/admin.state";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {
  AdminIndexFieldAliasState,
  AdminOaiMetadataPrefixState,
  AdminOaiSetState,
  AdminGlobalBannerState,
  SsrUtil,
} from "solidify-frontend";
import {AdminSubmissionAgreementUserState} from "./submission-agreement-user/stores/admin-submission-agreement-user.state";

const routables = [
  AdminHomeRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminState,
      AdminSubmissionAgreementState,
      AdminSubmissionAgreementUserState,
      AdminSubmissionPolicyState,
      AdminPreservationPolicyState,
      AdminDisseminationPolicyState,
      AdminArchiveTypeState,
      AdminLicenseState,
      AdminGlobalBannerState,
      AdminInstitutionState,
      AdminInstitutionOrganizationalUnitState,
      AdminInstitutionPersonRoleState,
      AdminOrganizationalUnitState,
      AdminOrganizationalUnitSubmissionPolicyState,
      AdminOrganizationalUnitPreservationPolicyState,
      AdminOrganizationalUnitDisseminationPolicyState,
      AdminOrganizationalUnitPersonRoleState,
      AdminOrganizationalUnitPersonInheritedRoleState,
      AdminOrganizationalUnitFundingAgencyState,
      AdminOrganizationalUnitInstitutionState,
      AdminOrganizationalUnitAdditionalFieldsFormState,
      AdminOrganizationalUnitSubjectAreaState,
      AdminUserState,
      AdminOaiMetadataPrefixState,
      AdminOaiSetState,
      AdminPersonState,
      AdminPersonOrgUnitRoleState,
      AdminPersonInstitutionRoleState,
      AdminRoleState,
      AdminNotificationState,
      AdminNotificationStatusHistoryState,
      AdminIndexFieldAliasState,
      AdminArchiveAclState,
      AdminFundingAgenciesState,
      AdminFundingAgenciesOrganizationalUnitState,
      AdminPersonInstitutionsState,
      AdminSubjectAreaState,
      AdminLanguageState,
      AdminMetadataTypeState,
      AdminScheduledTaskState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.adminModuleLoaded] = true;
    }
  }
}
