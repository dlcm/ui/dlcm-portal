/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-metadata-type.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminMetadataTypeAction,
  adminMetadataTypeActionNameSpace,
} from "@admin/metadata-type/stores/admin-metadata-type.action";
import {
  HttpClient,
  HttpEventType,
  HttpStatusCode,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {MetadataType} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {PreservationPlanningSipStateModel} from "@preservation-planning/sip/stores/preservation-planning-sip.state";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  map,
} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  DownloadService,
  isNullOrUndefined,
  isTrue,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceState,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
  StringUtil,
  UploadEventModel,
} from "solidify-frontend";

export interface AdminMetadataTypeStateModel extends ResourceStateModel<MetadataType> {
}

@Injectable()
@State<AdminMetadataTypeStateModel>({
  name: StateEnum.admin_metadataType,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class AdminMetadataTypeState extends ResourceState<AdminMetadataTypeStateModel, MetadataType> {
  private readonly _FILE_KEY: string = "file";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              protected readonly _httpClient: HttpClient) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminMetadataTypeActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminMetadataTypeDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminMetadataTypeDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminMetadataType,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.metadataType.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.metadataType.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.metadataType.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminMetadataTypes;
  }

  @Selector()
  static isLoading(state: AdminMetadataTypeStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminMetadataTypeStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminMetadataTypeStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminMetadataTypeStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminMetadataTypeStateModel): boolean {
    return true;
  }

  @Action(AdminMetadataTypeAction.Download)
  download(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: AdminMetadataTypeAction.Download): void {
    let ext = "txt";
    if (action.metadataType.metadataFormat === Enums.MetadataType.MetadataFormatEnum.XML) {
      ext = "xml";
    } else if (action.metadataType.metadataFormat === Enums.MetadataType.MetadataFormatEnum.JSON) {
      ext = "json";
    }
    const fileName = "metadata_type_" + StringUtil.convertToSnakeCase(action.metadataType.name) + "." + ext;
    this._downloadService.download(`${this._urlResource}/${action.metadataType.resId}/${ApiResourceNameEnum.SCHEMA}`, fileName);
  }

  @Action(AdminMetadataTypeAction.TestFile)
  testFile(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: AdminMetadataTypeAction.TestFile): Observable<void> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const formData = new FormData();
    formData.append(this._FILE_KEY, action.file, action.file.name);

    return this._apiService.upload(`${this._urlResource}/${action.id}/${ApiActionNameEnum.VALIDATE}`, formData)
      .pipe(
        map((event: UploadEventModel) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              return;
            case HttpEventType.Response:
              if (event.status === HttpStatusCode.Ok && isTrue(event.body)) {
                ctx.dispatch(new AdminMetadataTypeAction.TestFileSuccess(action));
              } else {
                ctx.dispatch(new AdminMetadataTypeAction.TestFileFail(action));
              }
              return;
            default:
              return;
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminMetadataTypeAction.TestFileFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminMetadataTypeAction.TestFileSuccess)
  testFileSuccess(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: AdminMetadataTypeAction.TestFileSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.sip.action.testFile.success"));
  }

  @Action(AdminMetadataTypeAction.TestFileFail)
  testFileFail(ctx: SolidifyStateContext<PreservationPlanningSipStateModel>, action: AdminMetadataTypeAction.TestFileFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.sip.action.testFile.fail"));
  }
}
