/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-metadata-type-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminMetadataTypeActionNameSpace} from "@admin/metadata-type/stores/admin-metadata-type.action";
import {AdminMetadataTypeStateModel} from "@admin/metadata-type/stores/admin-metadata-type.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {MetadataType} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-metadata-type-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-metadata-type-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminMetadataTypeListRoutable extends AbstractListRoutable<MetadataType, AdminMetadataTypeStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof MetadataType & string = "name";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_metadataType, adminMetadataTypeActionNameSpace, _injector, {}, StateEnum.admin);
  }

  conditionDisplayEditButton(model: MetadataType | undefined): boolean {
    return true;
  }

  conditionDisplayDeleteButton(model: MetadataType | undefined): boolean {
    return true;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "metadataFormat",
        header: LabelTranslateEnum.format,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.MetadataType.MetadataFormatEnumTranslate,
      },
      {
        field: "version",
        header: LabelTranslateEnum.version,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      // {
      //   field: "openMetadataTypeId",
      //   header: MARK_AS_TRANSLATABLE("admin.metadataType.table.header.openMetadataTypeId"),
      //   type: DataTableFieldTypeEnum.string,
      //   order: OrderEnum.none,
      //   isFilterable: false,
      //   isSortable: true,
      // },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }
}
