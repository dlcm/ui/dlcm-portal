/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-metadata-type-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminMetadataTypeAction,
  adminMetadataTypeActionNameSpace,
} from "@admin/metadata-type/stores/admin-metadata-type.action";
import {
  AdminMetadataTypeState,
  AdminMetadataTypeStateModel,
} from "@admin/metadata-type/stores/admin-metadata-type.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  Deposit,
  MetadataType,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedMetadataTypeActionNameSpace} from "@shared/stores/metadata-type/shared-metadata-type.action";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  ExtraButtonToolbar,
  isNullOrUndefined,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-metadata-type-detail-edit-routable",
  templateUrl: "./admin-metadata-type-detail-edit.routable.html",
  styleUrls: ["./admin-metadata-type-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminMetadataTypeDetailEditRoutable extends AbstractDetailEditCommonRoutable<MetadataType, AdminMetadataTypeStateModel> {
  @Select(AdminMetadataTypeState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminMetadataTypeState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedMetadataTypeActionNameSpace;

  readonly KEY_PARAM_NAME: keyof MetadataType & string = "name";
  readonly files: string = "files";

  listExtraButtons: ExtraButtonToolbar<Deposit>[] = [
    {
      color: "primary",
      icon: IconNameEnum.download,
      displayCondition: current => !this.isEdit,
      callback: () => this._download(),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.download,
      order: 40,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_metadataType, _injector, adminMetadataTypeActionNameSpace, StateEnum.admin);
  }

  _getSubResourceWithParentId(id: string): void {
  }

  private _download(): void {
    this._store.dispatch(new AdminMetadataTypeAction.Download(this.current));
  }

  testFileUpload(file: File): void {
    this._store.dispatch(new AdminMetadataTypeAction.TestFile(this._resId, file));
  }
}
