/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-metadata-type-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminMetadataTypeActionNameSpace} from "@admin/metadata-type/stores/admin-metadata-type.action";
import {
  AdminMetadataTypeState,
  AdminMetadataTypeStateModel,
} from "@admin/metadata-type/stores/admin-metadata-type.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  ViewChild,
} from "@angular/core";
import {MetadataType} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {sharedMetadataTypeActionNameSpace} from "@shared/stores/metadata-type/shared-metadata-type.action";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  AbstractFormPresentational,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-metadata-type-create-routable",
  templateUrl: "./admin-metadata-type-create.routable.html",
  styleUrls: ["./admin-metadata-type-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminMetadataTypeCreateRoutable extends AbstractCreateRoutable<MetadataType, AdminMetadataTypeStateModel> {
  @Select(AdminMetadataTypeState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminMetadataTypeState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedMetadataTypeActionNameSpace;

  @ViewChild("formPresentational")
  readonly formPresentational: AbstractFormPresentational<MetadataType>;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.admin_metadataType, _injector, adminMetadataTypeActionNameSpace, StateEnum.admin);
  }
}
