/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-metadata-type-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Output,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {Enums} from "@enums";
import {MetadataType} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  AbstractFormPresentational,
  HTMLInputEvent,
  isNotNullNorUndefined,
  KeyValue,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ObservableUtil,
  PropertyName,
  RegexUtil,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-metadata-type-form",
  templateUrl: "./admin-metadata-type-form.presentational.html",
  styleUrls: ["./admin-metadata-type-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminMetadataTypeFormPresentational extends AbstractFormPresentational<MetadataType> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();
  readonly files: string = "files";
  readonly maxSizeFile: number = 25000000;
  uploadedOrChangedFile: boolean = false;

  metadataFormatEnum: KeyValue[] = Enums.MetadataType.MetadataFormatEnumTranslate;

  @ViewChild("fileInput")
  fileInput: ElementRef;

  private readonly _fileUploadBS: BehaviorSubject<File | undefined> = new BehaviorSubject<File | undefined>(undefined);
  @Output("fileUploadChange")
  readonly fileUploadObs: Observable<File | undefined> = ObservableUtil.asObservable(this._fileUploadBS);

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              private readonly _notificationService: NotificationService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.url]: ["", [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.metadataFormat]: ["", [SolidifyValidator, Validators.required]],
      [this.formDefinition.version]: ["", [SolidifyValidator, Validators.required]],
      [this.formDefinition.metadataSchema]: ["", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(metadataTypes: MetadataType): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [metadataTypes.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [metadataTypes.description, [Validators.required, SolidifyValidator]],
      [this.formDefinition.url]: [metadataTypes.url, [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.metadataFormat]: [metadataTypes.metadataFormat, [SolidifyValidator, Validators.required]],
      [this.formDefinition.version]: [metadataTypes.version, [SolidifyValidator, Validators.required]],
      [this.formDefinition.metadataSchema]: [metadataTypes.metadataSchema, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(metadataTypes: MetadataType): MetadataType {
    return metadataTypes;
  }

  onFileUploadByInput(event: HTMLInputEvent): void {
    if (event.target.files.length === 0) {
      return;
    }
    this.onFileUpload(event.target.files);
  }

  onFileUpload(fileList: FileList | File[]): void {
    const file = fileList[0];
    if (!this.isFileAccepted(file)) {
      this._notificationService.showWarning(MARK_AS_TRANSLATABLE("admin.metadataType.fileNotSupported"));
      return;
    }
    if (file.size > this.maxSizeFile) {
      this._notificationService.showWarning(MARK_AS_TRANSLATABLE("admin.metadataType.maximumSize"));
      return;
    }
    const fileReader = new FileReader();
    fileReader.onload = () => {
      this.form.get(this.formDefinition.metadataSchema).setValue(fileReader.result);
      this._notificationService.showInformation(MARK_AS_TRANSLATABLE("admin.metadataType.readUploadedFileSuccessfully"));
    };
    fileReader.readAsText(file);
    this.uploadedOrChangedFile = true;
  }

  onTestFileUploadByInput(event: HTMLInputEvent): void {
    if (event.target.files.length === 0) {
      return;
    }
    this.onTestFileUpload(event.target.files);
    if (isNotNullNorUndefined(this.fileInput)) {
      this.fileInput.nativeElement.value = null;
    }
  }

  onTestFileUpload(fileList: FileList): void {
    const file = fileList[0];
    this._fileUploadBS.next(file);
  }

  isFileAccepted(file: File): boolean {
    return file.type.startsWith("text") || file.type === "application/json" || file.name.endsWith(".xsd");
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() description: string;
  @PropertyName() url: string;
  @PropertyName() metadataFormat: string;
  @PropertyName() version: string;
  @PropertyName() metadataSchema: string;
}
