/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-metadata-type-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminMetadataTypeCreateRoutable} from "@admin/metadata-type/components/routables/admin-metadata-type-create/admin-metadata-type-create.routable";
import {AdminMetadataTypeDetailEditRoutable} from "@admin/metadata-type/components/routables/admin-metadata-type-detail-edit/admin-metadata-type-detail-edit.routable";
import {AdminMetadataTypeListRoutable} from "@admin/metadata-type/components/routables/admin-metadata-type-list/admin-metadata-type-list.routable";
import {AdminMetadataTypeState} from "@admin/metadata-type/stores/admin-metadata-type.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";
import {
  CanDeactivateGuard,
  EmptyContainer,
} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminMetadataTypeListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.metadataTypeCreate,
    component: AdminMetadataTypeCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
    },
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: AdminRoutesEnum.metadataTypeDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminMetadataTypeDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminMetadataTypeState.currentTitle,
    },
    children: [
      {
        path: AdminRoutesEnum.metadataTypeEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminMetadataTypeRoutingModule {
}
