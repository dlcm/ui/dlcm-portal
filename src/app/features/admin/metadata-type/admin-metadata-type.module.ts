/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-metadata-type.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminMetadataTypeRoutingModule} from "@admin/metadata-type/admin-metadata-type-routing.module";
import {AdminMetadataTypeFormPresentational} from "@admin/metadata-type/components/presentationals/admin-metadata-type-form/admin-metadata-type-form.presentational";
import {AdminMetadataTypeCreateRoutable} from "@admin/metadata-type/components/routables/admin-metadata-type-create/admin-metadata-type-create.routable";
import {AdminMetadataTypeDetailEditRoutable} from "@admin/metadata-type/components/routables/admin-metadata-type-detail-edit/admin-metadata-type-detail-edit.routable";
import {AdminMetadataTypeListRoutable} from "@admin/metadata-type/components/routables/admin-metadata-type-list/admin-metadata-type-list.routable";
import {AdminMetadataTypeState} from "@admin/metadata-type/stores/admin-metadata-type.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminMetadataTypeCreateRoutable,
  AdminMetadataTypeDetailEditRoutable,
  AdminMetadataTypeListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminMetadataTypeFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminMetadataTypeRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminMetadataTypeState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminMetadataTypeModule {
}
