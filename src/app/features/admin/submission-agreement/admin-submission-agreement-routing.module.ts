/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-agreement-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminSubmissionAgreementCreateRoutable} from "@admin/submission-agreement/components/routables/admin-submission-agreement-create/admin-submission-agreement-create.routable";
import {AdminSubmissionAgreementDetailEditRoutable} from "@admin/submission-agreement/components/routables/admin-submission-agreement-detail-edit/admin-submission-agreement-detail-edit.routable";
import {AdminSubmissionAgreementListRoutable} from "@admin/submission-agreement/components/routables/admin-submission-agreement-list/admin-submission-agreement-list.routable";
import {AdminSubmissionAgreementState} from "@admin/submission-agreement/stores/admin-submission-agreement.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {ApplicationRoleGuardService} from "@shared/guards/application-role-guard.service";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminSubmissionAgreementListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.submissionAgreementDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminSubmissionAgreementDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminSubmissionAgreementState.currentTitle,
    },
    children: [
      {
        path: AdminRoutesEnum.submissionAgreementEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
  {
    path: AdminRoutesEnum.submissionAgreementCreate,
    component: AdminSubmissionAgreementCreateRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.create,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
    canDeactivate: [CanDeactivateGuard],
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminSubmissionAgreementRoutingModule {
}
