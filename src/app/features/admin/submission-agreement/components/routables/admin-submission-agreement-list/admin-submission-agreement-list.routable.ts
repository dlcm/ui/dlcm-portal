/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-agreement-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminSubmissionAgreementActionNameSpace} from "@admin/submission-agreement/stores/admin-submission-agreement.action";
import {AdminSubmissionAgreementStateModel} from "@admin/submission-agreement/stores/admin-submission-agreement.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {SubmissionAgreement} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-submission-agreement-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-submission-agreement-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminSubmissionAgreementListRoutable extends AbstractListRoutable<SubmissionAgreement, AdminSubmissionAgreementStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof SubmissionAgreement & string = "title";

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_submissionAgreement, adminSubmissionAgreementActionNameSpace, _injector, {
      canCreate: _securityService.isRootOrAdmin(),
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: SubmissionAgreement | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  conditionDisplayDeleteButton(model: SubmissionAgreement | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "title",
        header: LabelTranslateEnum.titleLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "version",
        header: LabelTranslateEnum.version,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        maxWidth: "200px",
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }
}
