/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-agreement-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminSubmissionAgreementAction} from "@admin/submission-agreement/stores/admin-submission-agreement.action";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {
  DataFile,
  SubmissionAgreement,
} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  AbstractFormPresentational,
  FormValidationHelper,
  isNotNullNorUndefined,
  PropertyName,
  ResourceFileNameSpace,
  SolidifyDataFileModel,
  SolidifyFileUploadInputRequiredValidator,
  SolidifyFileUploadStatus,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-submission-agreement-form",
  templateUrl: "./admin-submission-agreement-form.presentational.html",
  styleUrls: ["./admin-submission-agreement-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminSubmissionAgreementFormPresentational extends AbstractFormPresentational<SubmissionAgreement> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  adminSubmissionAgreementActionNameSpace: ResourceFileNameSpace = AdminSubmissionAgreementAction;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  dataFileAdapter: (dataFile: DataFile) => SolidifyDataFileModel = DataFileHelper.dataFileAdapter;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(FormValidationHelper.cleanErrorOnLinkedFormControlWhenValueChange(this.form, this.formDefinition.title, [this.formDefinition.version]));
    this.subscribe(FormValidationHelper.cleanErrorOnLinkedFormControlWhenValueChange(this.form, this.formDefinition.version, [this.formDefinition.title]));
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.title]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [SolidifyValidator]],
      [this.formDefinition.version]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.fileChange]: [undefined, [SolidifyFileUploadInputRequiredValidator, SolidifyValidator]],
    });
  }

  protected _bindFormTo(submissionAgreement: SubmissionAgreement): void {
    this.form = this._fb.group({
      [this.formDefinition.title]: [submissionAgreement.title, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [submissionAgreement.description, [SolidifyValidator]],
      [this.formDefinition.version]: [submissionAgreement.version, [Validators.required, SolidifyValidator]],
      [this.formDefinition.fileChange]: [isNotNullNorUndefined(submissionAgreement.submissionAgreementFile) ? SolidifyFileUploadStatus.UNCHANGED : undefined, [SolidifyFileUploadInputRequiredValidator, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(model: SubmissionAgreement): SubmissionAgreement {
    return model;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() title: string;
  @PropertyName() description: string;
  @PropertyName() version: string;
  @PropertyName() fileChange: string;
}
