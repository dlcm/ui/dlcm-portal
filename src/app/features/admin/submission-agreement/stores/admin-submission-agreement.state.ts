/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-agreement.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpEventType,
  HttpStatusCode,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {SubmissionAgreement} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  filter,
  map,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  BaseResourceWithFile,
  defaultResourceStateInitValue,
  DownloadService,
  isInstanceOf,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceStateModel,
  SolidifyFileUploadStatus,
  SolidifyFileUploadType,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  StoreUtil,
  UploadEventModel,
} from "solidify-frontend";
import {
  AdminSubmissionAgreementAction,
  adminSubmissionAgreementActionNameSpace,
} from "./admin-submission-agreement.action";

const ATTRIBUTE_SIGNED_DUA_FILE: keyof SubmissionAgreement = "submissionAgreementFile";

export interface AdminSubmissionAgreementStateModel extends ResourceStateModel<SubmissionAgreement> {
}

@Injectable()
@State<AdminSubmissionAgreementStateModel>({
  name: StateEnum.admin_submissionAgreement,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class AdminSubmissionAgreementState extends ResourceFileState<AdminSubmissionAgreementStateModel, SubmissionAgreement> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminSubmissionAgreementActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminSubmissionAgreementDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminSubmissionAgreementDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminSubmissionAgreement,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.submissionAgreement.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.submissionAgreement.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.submissionAgreement.notification.resource.update"),
      resourceFileApiActionNameUploadCustom: "upload-file",
      resourceFileApiActionNameDownloadCustom: "download-file",
      resourceFileApiActionNameDeleteCustom: "delete-file",
      customFileAttribute: ATTRIBUTE_SIGNED_DUA_FILE,
    }, _downloadService, ResourceFileStateModeEnum.custom, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminSubmissionAgreements;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminSubmissionAgreementStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminSubmissionAgreementStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminSubmissionAgreementStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.title;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminSubmissionAgreementStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminSubmissionAgreementStateModel): boolean {
    return true;
  }

  protected override _getListActionsUpdateSubResource(model: SubmissionAgreement, action: AdminSubmissionAgreementAction.Create | AdminSubmissionAgreementAction.Update, ctx: SolidifyStateContext<AdminSubmissionAgreementStateModel>): ActionSubActionCompletionsWrapper[] {
    if (isInstanceOf(action, AdminSubmissionAgreementAction.Create)) {
      return [];
    }
    action = action as AdminSubmissionAgreementAction.Update;
    const actions = ResourceFileState.getActionUpdateFile(model.resId, (action.modelFormControlEvent.model as BaseResourceWithFile).newPendingFile, null, adminSubmissionAgreementActionNameSpace, this._actions$);

    const submissionAgreementId = model.resId;

    const newSubmissionAgreement: SubmissionAgreement = action.modelFormControlEvent.model;
    const fileChange: SolidifyFileUploadType = newSubmissionAgreement.fileChange;

    if (fileChange === SolidifyFileUploadStatus.UNCHANGED) {
      // do nothing
    } else if (fileChange === SolidifyFileUploadStatus.DELETED) {
      // do nothing, not allow to delete
    } else if (isNotNullNorUndefined(fileChange)) {
      actions.push({
        action: new AdminSubmissionAgreementAction.UploadFile(submissionAgreementId, {
          file: fileChange as File,
        }),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminSubmissionAgreementAction.UploadFileSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminSubmissionAgreementAction.UploadFileFail)),
        ],
      });
    }

    return actions;
  }

  protected override _internalCreate(ctx: SolidifyStateContext<AdminSubmissionAgreementStateModel>, action: AdminSubmissionAgreementAction.Create): Observable<SubmissionAgreement> {
    ctx.dispatch(new AdminSubmissionAgreementAction.Clean(true));

    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const model = action.modelFormControlEvent?.model;
    const file = (model.fileChange as File);
    const formData = new FormData();
    formData.append(this._FILE_KEY, file, file.name);
    formData.append("title", model.title);
    formData.append("description", model.description);
    formData.append("version", model.version);
    formData.append("mimeType", file.type);

    return this._apiService.upload(`${this._urlResource}/${ApiActionNameEnum.UPLOAD_SUBMISSION_AGREEMENT}`, formData)
      .pipe(
        map((event: UploadEventModel) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              return;
            case HttpEventType.Response:
              if ([HttpStatusCode.Created, HttpStatusCode.Ok].includes(event.status) && isNotNullNorUndefined(event.body)) {
                return event.body as SubmissionAgreement;
              }
              return;
            default:
              return;
          }
        }),
        filter(submissionAgreement => isNotNullNorUndefined(submissionAgreement)),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          throw error;
        }),
        StoreUtil.catchValidationErrors(ctx as any, action.modelFormControlEvent as any, this._notificationService, this._optionsState.autoScrollToFirstValidationError),
      );
  }
}
