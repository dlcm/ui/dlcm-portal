/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-submission-agreement.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminSubmissionAgreementRoutingModule} from "@admin/submission-agreement/admin-submission-agreement-routing.module";
import {AdminSubmissionAgreementFormPresentational} from "@admin/submission-agreement/components/presentationals/admin-submission-agreement-form/admin-submission-agreement-form.presentational";
import {AdminSubmissionAgreementCreateRoutable} from "@admin/submission-agreement/components/routables/admin-submission-agreement-create/admin-submission-agreement-create.routable";
import {AdminSubmissionAgreementDetailEditRoutable} from "@admin/submission-agreement/components/routables/admin-submission-agreement-detail-edit/admin-submission-agreement-detail-edit.routable";
import {AdminSubmissionAgreementListRoutable} from "@admin/submission-agreement/components/routables/admin-submission-agreement-list/admin-submission-agreement-list.routable";
import {AdminSubmissionAgreementState} from "@admin/submission-agreement/stores/admin-submission-agreement.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminSubmissionAgreementListRoutable,
  AdminSubmissionAgreementDetailEditRoutable,
  AdminSubmissionAgreementCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminSubmissionAgreementFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminSubmissionAgreementRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminSubmissionAgreementState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminSubmissionAgreementModule {
}
