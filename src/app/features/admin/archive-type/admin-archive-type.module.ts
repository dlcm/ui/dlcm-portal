/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-archive-type.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminArchiveTypeRoutingModule} from "@admin/archive-type/admin-archive-type-routing.module";
import {AdminArchiveTypeFormPresentational} from "@admin/archive-type/components/presentationals/admin-archive-type-form/admin-archive-type-form.presentational";
import {AdminArchiveTypeCreateRoutable} from "@admin/archive-type/components/routables/admin-archive-type-create/admin-archive-type-create.routable";
import {AdminArchiveTypeDetailEditRoutable} from "@admin/archive-type/components/routables/admin-archive-type-detail-edit/admin-archive-type-detail-edit.routable";
import {AdminArchiveTypeListRoutable} from "@admin/archive-type/components/routables/admin-archive-type-list/admin-archive-type-list.routable";
import {AdminArchiveTypeState} from "@admin/archive-type/stores/admin-archive-type.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";

const routables = [
  AdminArchiveTypeListRoutable,
  AdminArchiveTypeDetailEditRoutable,
  AdminArchiveTypeCreateRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  AdminArchiveTypeFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminArchiveTypeRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminArchiveTypeState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminArchiveTypeModule {
}
