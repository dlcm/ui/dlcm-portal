/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-archive-type.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {DepositStateModel} from "@deposit/stores/deposit.state";
import {environment} from "@environments/environment";
import {ArchiveType} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedArchiveTypeAction} from "@shared/stores/archive-type/shared-archive-type.action";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";
import {
  AdminArchiveTypeAction,
  adminArchiveTypeActionNameSpace,
} from "./admin-archive-type.action";

export const defaultAdminArchiveTypeStateModel: () => AdminArchiveTypeStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
  });

export interface AdminArchiveTypeStateModel extends ResourceStateModel<ArchiveType> {
}

@Injectable()
@State<AdminArchiveTypeStateModel>({
  name: StateEnum.admin_archiveType,
  defaults: {
    ...defaultAdminArchiveTypeStateModel(),
  },
})
export class AdminArchiveTypeState extends ResourceState<AdminArchiveTypeStateModel, ArchiveType> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminArchiveTypeActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminArchiveTypeDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminArchiveTypeDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminArchiveType,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.archiveType.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.archiveType.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.archiveType.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminArchiveType;
  }

  @Selector()
  static isLoading(state: AdminArchiveTypeStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminArchiveTypeStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminArchiveTypeStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.typeName;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminArchiveTypeStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminArchiveTypeStateModel): boolean {
    return true;
  }

  @Action(AdminArchiveTypeAction.LoadResource)
  loadResource(ctx: SolidifyStateContext<DepositStateModel>, action: AdminArchiveTypeAction.LoadResource): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new SharedArchiveTypeAction.GetListMasterTypes(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedArchiveTypeAction.GetListMasterTypesSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedArchiveTypeAction.GetListMasterTypesFail)),
        ],
      },
      {
        action: new SharedArchiveTypeAction.GetListMasterTypesNames(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedArchiveTypeAction.GetListMasterTypesNamesSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedArchiveTypeAction.GetListMasterTypesNamesFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new AdminArchiveTypeAction.LoadResourceSuccess(action));
        } else {
          ctx.dispatch(new AdminArchiveTypeAction.LoadResourceFail(action));
        }
        return result.success;
      }),
    );
  }
}
