/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-archive-type-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {ArchiveType} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {tap} from "rxjs/operators";
import {
  AbstractFormPresentational,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isTrue,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-archive-type-form",
  templateUrl: "./admin-archive-type-form.presentational.html",
  styleUrls: ["./admin-archive-type-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminArchiveTypeFormPresentational extends AbstractFormPresentational<ArchiveType> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  @Input()
  listMasterTypesName: string[];

  private _listMasterTypes: ArchiveType[];

  @Input()
  set listMasterTypes(value: ArchiveType[]) {
    if (isNotNullNorUndefined(this.model) && this._isMasterType) {
      // Allow to avoid to select himself
      this._listMasterTypes = value.filter(t => t.resId !== this.model.resId);
    } else {
      this._listMasterTypes = value;
    }
  }

  get listMasterTypes(): ArchiveType[] {
    return this._listMasterTypes;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.typeName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.masterTypeId]: [undefined, [SolidifyValidator]],
      [this.formDefinition.isMasterType]: [true, [SolidifyValidator]],
    });
    this._addLogical();
  }

  protected _bindFormTo(archiveType: ArchiveType): void {
    this.form = this._fb.group({
      [this.formDefinition.typeName]: [archiveType.typeName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.masterTypeId]: [archiveType.masterType?.resId, [SolidifyValidator]],
      [this.formDefinition.isMasterType]: [this._isMasterType, [SolidifyValidator]],
    });
    this._addLogical();
  }

  private _addLogical(): void {
    const formControlIsMasterType = this.form.get(this.formDefinition.isMasterType);
    if (formControlIsMasterType) {
      this.subscribe(formControlIsMasterType.valueChanges.pipe(
        tap(isMasterType => {
          this._masterTypeLogical();
          this._changeDetectorRef.detectChanges();
        }),
      ));
      this._masterTypeLogical();
    }
  }

  private _masterTypeLogical(): void {
    const formControlMasterTypeId = this.form.get(this.formDefinition.masterTypeId);
    if (isNullOrUndefined(formControlMasterTypeId)) {
      return;
    }
    const isMasterType = this.form.get(this.formDefinition.isMasterType)?.value;
    if (isTrue(isMasterType)) {
      formControlMasterTypeId.setValidators([]);
      formControlMasterTypeId.setValue(undefined);
    } else {
      formControlMasterTypeId.setValidators([Validators.required]);
    }
    formControlMasterTypeId.updateValueAndValidity();
  }

  protected _treatmentBeforeSubmit(archiveType: ArchiveType): ArchiveType {
    const masterTypeResId = this.form.get(this.formDefinition.masterTypeId)?.value;
    if (isNotNullNorUndefinedNorWhiteString(masterTypeResId)) {
      archiveType.masterType = {
        resId: masterTypeResId,
      };
    }

    const isMasterType = this.form.get(this.formDefinition.isMasterType)?.value;
    if (isTrue(isMasterType)) {
      archiveType.masterType = undefined;
    }

    return archiveType;
  }

  private get _isMasterType(): boolean {
    return isNullOrUndefined(this.model.masterType);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() typeName: string;
  @PropertyName() masterTypeId: string;
  @PropertyName() isMasterType: string;
}
