/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-archive-type-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminArchiveTypeActionNameSpace} from "@admin/archive-type/stores/admin-archive-type.action";
import {
  AdminArchiveTypeState,
  AdminArchiveTypeStateModel,
} from "@admin/archive-type/stores/admin-archive-type.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {ArchiveType} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedArchiveTypeState} from "@shared/stores/archive-type/shared-archive-type.state";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-archive-type-detail-edit-routable",
  templateUrl: "./admin-archive-type-detail-edit.routable.html",
  styleUrls: ["./admin-archive-type-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminArchiveTypeDetailEditRoutable extends AbstractDetailEditCommonRoutable<ArchiveType, AdminArchiveTypeStateModel> {
  @Select(AdminArchiveTypeState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminArchiveTypeState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  listMasterTypesObs: Observable<ArchiveType[]> = MemoizedUtil.list(this._store, SharedArchiveTypeState);
  listMasterTypesNamesObs: Observable<string[]> = MemoizedUtil.select(this._store, SharedArchiveTypeState, state => state.listMasterTypesNames);

  readonly KEY_PARAM_NAME: keyof ArchiveType & string = "typeName";

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_archiveType, _injector, adminArchiveTypeActionNameSpace, StateEnum.admin);
    this.editAvailable = this._securityService.isRootOrAdmin();
    this.deleteAvailable = this._securityService.isRootOrAdmin();
  }

  _getSubResourceWithParentId(id: string): void {
  }
}
