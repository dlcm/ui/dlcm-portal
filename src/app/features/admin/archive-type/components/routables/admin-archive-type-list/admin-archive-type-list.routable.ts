/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-archive-type-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminArchiveTypeAction,
  adminArchiveTypeActionNameSpace,
} from "@admin/archive-type/stores/admin-archive-type.action";
import {
  AdminArchiveTypeState,
  AdminArchiveTypeStateModel,
} from "@admin/archive-type/stores/admin-archive-type.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {sharedArchiveTypeActionNameSpace} from "@app/shared/stores/archive-type/shared-archive-type.action";
import {ArchiveType} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedArchiveTypeState} from "@shared/stores/archive-type/shared-archive-type.state";
import {
  AbstractListRoutable,
  CookieConsentUtil,
  CookieType,
  DataTableFieldTypeEnum,
  isNotNullNorUndefined,
  isNullOrUndefined,
  LocalStorageHelper,
  MappingObjectUtil,
  MemoizedUtil,
  ObjectUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  ResourceNameSpace,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
  Sort,
} from "solidify-frontend";

const onlyMasterTypeStorageKey: LocalStorageEnum = LocalStorageEnum.adminShowOnlyArchiveMastersTypes;
const onlyMasterTypeFn: () => boolean = () => {
  const storageValue = LocalStorageHelper.getItem(onlyMasterTypeStorageKey);
  if (isNullOrUndefined(storageValue)) {
    return true;
  }
  return storageValue === SOLIDIFY_CONSTANTS.STRING_TRUE;
};

@Component({
  selector: "dlcm-admin-archive-type-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-archive-type-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminArchiveTypeListRoutable extends AbstractListRoutable<ArchiveType, AdminArchiveTypeStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof ArchiveType & string = "typeName";
  private readonly _KEY_MASTER_TYPE: string = "masterType.resId";

  override columnsSkippedToClear: string[] = [];

  private _onlyMasterType: boolean = undefined;

  protected get onlyMasterType(): boolean {
    if (isNotNullNorUndefined(this._onlyMasterType)) {
      return this._onlyMasterType;
    }
    return onlyMasterTypeFn();
  }

  protected set onlyMasterType(value: boolean) {
    this._onlyMasterType = value;
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, onlyMasterTypeStorageKey)) {
      LocalStorageHelper.setItem(onlyMasterTypeStorageKey, this.onlyMasterType + "");
    }
  }

  sharedArchiveTypeSort: Sort<ArchiveType> = {
    field: "typeName",
    order: OrderEnum.ascending,
  };
  sharedArchiveTypeActionNameSpace: ResourceNameSpace = sharedArchiveTypeActionNameSpace;
  sharedArchiveTypeState: typeof SharedArchiveTypeState = SharedArchiveTypeState;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_archiveType, adminArchiveTypeActionNameSpace, _injector, {
      canCreate: _securityService.isRootOrAdmin(),
      listExtraButtons: [
        {
          color: "primary",
          icon: null,
          labelToTranslate: (current) => LabelTranslateEnum.showOnlyMastersTypes,
          callback: (model, buttonElementRef, checked) => this._showOnlyMasterType(checked),
          order: 40,
          isToggleButton: true,
          isToggleChecked: onlyMasterTypeFn(),
        },
      ],
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: ArchiveType | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  conditionDisplayDeleteButton(model: ArchiveType | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  defineColumns(): void {
    this.columns = [
      ...(this._onlyMasterType ? [] : [{
        field: "masterType.typeName" as any,
        header: LabelTranslateEnum.masterType,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        resourceNameSpace: this.sharedArchiveTypeActionNameSpace,
        resourceState: this.sharedArchiveTypeState as any,
        resourceLabelKey: this.KEY_PARAM_NAME,
        filterableField: "masterType.resId" as any,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      }]),
      {
        field: "typeName",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "masterArchiveType",
        header: LabelTranslateEnum.masterType,
        type: DataTableFieldTypeEnum.boolean,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  _showOnlyMasterType(checked: boolean): void {
    let queryParameter = MemoizedUtil.queryParametersSnapshot(this._store, AdminArchiveTypeState);
    queryParameter = QueryParametersUtil.clone(queryParameter);
    if (checked) {
      queryParameter.paging.pageIndex = 0;
      this.updateQueryParameterWithMasterType(queryParameter, true);
      this.onlyMasterType = true;
    } else {
      this.updateQueryParameterWithMasterType(queryParameter, false);
      this.onlyMasterType = false;
    }
    this.defineColumns();

    this._store.dispatch(new AdminArchiveTypeAction.ChangeQueryParameters(queryParameter, true));
    this._changeDetector.detectChanges();
  }

  override onQueryParametersEvent(queryParameters: QueryParameters): void {
    if (this.onlyMasterType) {
      this.updateQueryParameterWithMasterType(queryParameters, true);
      this._store.dispatch(new AdminArchiveTypeAction.ChangeQueryParameters(queryParameters, true));
    } else {
      this.updateQueryParameterWithMasterType(queryParameters, false);
      this._store.dispatch(new AdminArchiveTypeAction.ChangeQueryParameters(queryParameters, true));
    }
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  private updateQueryParameterWithMasterType(queryParameters: QueryParameters, onlyMasterType: boolean): QueryParameters | undefined {
    queryParameters = ObjectUtil.clone(queryParameters);
    queryParameters.search = ObjectUtil.clone(queryParameters.search);

    if (onlyMasterType) {
      MappingObjectUtil.set(queryParameters.search.searchItems, this._KEY_MASTER_TYPE, "true");
      this.columnsSkippedToClear = [this._KEY_MASTER_TYPE];
    } else {
      if (MappingObjectUtil.get(queryParameters.search.searchItems, this._KEY_MASTER_TYPE) === "true") {
        MappingObjectUtil.delete(queryParameters.search.searchItems, this._KEY_MASTER_TYPE);
      }
      this.columnsSkippedToClear = [];
    }
    return queryParameters;
  }
}
