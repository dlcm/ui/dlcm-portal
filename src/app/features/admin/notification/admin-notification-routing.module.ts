/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-notification-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminNotificationDetailEditRoutable} from "@admin/notification/components/routables/admin-notification-detail-edit/admin-notification-detail-edit.routable";
import {AdminNotificationListRoutable} from "@admin/notification/components/routables/admin-notification-list/admin-notification-list.routable";
import {AdminNotificationState} from "@admin/notification/stores/admin-notification.state";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
} from "@shared/enums/routes.enum";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";
import {
  CanDeactivateGuard,
  EmptyContainer,
} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: AdminNotificationListRoutable,
    data: {},
  },
  {
    path: AdminRoutesEnum.notificationDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: AdminNotificationDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: AdminNotificationState.currentTitle,
    },
    children: [
      {
        path: AdminRoutesEnum.notificationEdit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
        },
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminNotificationRoutingModule {
}
