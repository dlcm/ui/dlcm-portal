/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-notification-archive-acl-external-dua.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {NotificationDlcm} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: "dlcm-admin-archive-acl-external-dua-dialog",
  templateUrl: "./admin-notification-archive-acl-external-dua.dialog.html",
  styleUrls: ["./admin-notification-archive-acl-external-dua.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminNotificationArchiveAclExternalDuaDialog extends SharedAbstractDialog<ArchiveACLExternalDuaData, boolean> {

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<AdminNotificationArchiveAclExternalDuaDialog>,
              private readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: ArchiveACLExternalDuaData) {
    super(_dialogRef, data);
  }

  onSubmit(): void {
    this.submit(true);
  }

  readonly KEY_CREATE_BUTTON: string;
}

export interface ArchiveACLExternalDuaData {
  notification: NotificationDlcm;
}
