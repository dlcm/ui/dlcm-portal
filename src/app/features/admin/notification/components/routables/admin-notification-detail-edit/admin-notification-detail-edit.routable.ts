/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-notification-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminArchiveAclAction} from "@admin/archive-acl/stores/admin-archive-acl.action";
import {
  AdminNotificationAction,
  adminNotificationActionNameSpace,
} from "@admin/notification/stores/admin-notification.action";
import {
  AdminNotificationState,
  AdminNotificationStateModel,
} from "@admin/notification/stores/admin-notification.state";
import {AdminNotificationStatusHistoryAction} from "@admin/notification/stores/status-history/admin-notification-status-history.action";
import {AdminNotificationStatusHistoryState} from "@admin/notification/stores/status-history/admin-notification-status-history.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  NotificationDlcm,
  Role,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {
  SharedNotificationAction,
  sharedNotificationActionNameSpace,
} from "@shared/stores/notification/shared-notification.action";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  DialogUtil,
  ExtraButtonToolbar,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  QueryParameters,
  ResourceNameSpace,
  StatusHistory,
  StatusHistoryDialog,
  StoreUtil,
  StringUtil,
} from "solidify-frontend";
import {SharedArchiveAction} from "@shared/stores/archive/shared-archive.action";
import {NotificationHelper} from "@preservation-space/notification/helper/notification.helper";
import {
  AdminNotificationArchiveAclExternalDuaDialog,
} from "@admin/notification/components/dialogs/admin-notification-archive-acl-external-dua/admin-notification-archive-acl-external-dua.dialog";
import {SharedNotificationHelper} from "@shared/helpers/shared-notification.helper";

@Component({
  selector: "dlcm-admin-role-detail-edit-routable",
  templateUrl: "./admin-notification-detail-edit.routable.html",
  styleUrls: ["./admin-notification-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminNotificationDetailEditRoutable extends AbstractDetailEditCommonRoutable<NotificationDlcm, AdminNotificationStateModel> {
  @Select(AdminNotificationState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminNotificationState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, AdminNotificationStatusHistoryState, state => state.history);
  isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AdminNotificationStatusHistoryState);
  queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, AdminNotificationStatusHistoryState, state => state.queryParameters);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);

  override readonly editAvailable: boolean = false;
  override readonly deleteAvailable: boolean = false;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedNotificationActionNameSpace;

  readonly KEY_PARAM_NAME: keyof NotificationDlcm & string = "notificationType";

  listExtraButtons: ExtraButtonToolbar<NotificationDlcm>[] = [
    {
      color: "primary",
      icon: IconNameEnum.navigate,
      displayCondition: current => current?.notificationStatus === Enums.Notification.StatusEnum.PENDING,
      callback: (current) => this._process(current),
      labelToTranslate: (current) => LabelTranslateEnum.process,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.navigate,
      displayCondition: current => current?.notificationType.notificationCategory === Enums.Notification.CategoryEnum.INFO,
      callback: (current) => this._process(current),
      labelToTranslate: (current) => LabelTranslateEnum.showObject,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.unapprove,
      displayCondition: current => current?.notificationStatus === Enums.Notification.StatusEnum.PENDING
        && current?.notificationType.notificationCategory === Enums.Notification.CategoryEnum.REQUEST,
      callback: (current) => this._setRefuse(current),
      labelToTranslate: (current) => LabelTranslateEnum.markAsRefused,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.markAsUnread,
      displayCondition: current => current?.notificationMark === Enums.Notification.MarkEnum.READ,
      callback: (current) => this._setUnread(current),
      labelToTranslate: (current) => LabelTranslateEnum.markAsUnread,
      order: 40,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              private readonly _notificationService: NotificationService,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_notification, _injector, adminNotificationActionNameSpace, StateEnum.admin);

    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(SharedNotificationAction.RefreshDetailEvent),
      tap(result => {
        this._store.dispatch(new AdminNotificationAction.GetById(result.action.notificationId));
      }),
    ));
  }

  private _setRefuse(notification: NotificationDlcm): void {
    this.subscribe(SharedNotificationHelper.getRefuseDialog(this._dialog, this._store, ViewModeEnum.detail, notification));
  }

  private _setUnread(notification: NotificationDlcm): void {
    this._store.dispatch(new SharedNotificationAction.SetUnread(notification.resId, ViewModeEnum.detail));
    this.backToList();
  }

  private _process(notification: NotificationDlcm): void {
    if (notification.notificationType.resId !== Enums.Notification.TypeEnum.ACCESS_DATASET_REQUEST) {
      this._processNotification(notification);
      return;
    }
    // Check if archive has an external dua
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedArchiveAction.GetById(notification.objectId),
      SharedArchiveAction.GetByIdSuccess,
      result => {
        if (result.model.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.EXTERNAL_DUA) {
          this.subscribe(DialogUtil.open(this._dialog, AdminNotificationArchiveAclExternalDuaDialog, {
              notification: notification,
            },
            {
              width: "max-content",
              maxWidth: "90vw",
              height: "min-content",
              takeOne: true,
            }, () => {
              this._processNotification(notification);
            }));
        } else {
          this._processNotification(notification);
        }
      }));
  }

  _getSubResourceWithParentId(id: string): void {
  }

  showHistory(): void {
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: this._resId,
      name: StringUtil.convertToPascalCase(this._state),
      statusHistory: this.historyObs,
      isLoading: this.isLoadingHistoryObs,
      queryParametersObs: this.queryParametersHistoryObs,
      state: AdminNotificationStatusHistoryAction,
      statusEnums: Enums.Notification.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }

  private _createArchiveAcl(notification: NotificationDlcm): void {
    this._store.dispatch(new AdminArchiveAclAction.CreateFromNotification(notification));
  }

  private _processNotification(notification: NotificationDlcm): void {
    if (notification.notificationType.resId === Enums.Notification.TypeEnum.ACCESS_DATASET_REQUEST) {
      this._createArchiveAcl(notification);
      return;
    }
    NotificationHelper.process(notification, this._store, this._actions$, this._notificationService);
  }
}
