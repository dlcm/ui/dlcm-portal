/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-notification-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminNotificationAction,
  adminNotificationActionNameSpace,
} from "@admin/notification/stores/admin-notification.action";
import {
  AdminNotificationState,
  AdminNotificationStateModel,
} from "@admin/notification/stores/admin-notification.state";
import {adminNotificationStatusHistoryActionNameSpace} from "@admin/notification/stores/status-history/admin-notification-status-history.action";
import {AdminNotificationStatusHistoryState} from "@admin/notification/stores/status-history/admin-notification-status-history.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {
  NotificationDlcm,
  OrganizationalUnit,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SharedNotificationAction} from "@shared/stores/notification/shared-notification.action";
import {sharedOrganizationalUnitActionNameSpace} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedOrganizationalUnitState} from "@shared/stores/organizational-unit/shared-organizational-unit.state";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {
  AbstractListRoutable,
  ButtonColorEnum,
  DataTableActions,
  DataTableBulkActions,
  DataTableFieldTypeEnum,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  OrderEnum,
  ResourceNameSpace,
  RouterExtensionService,
  Sort,
  StatusHistory,
} from "solidify-frontend";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {Navigate} from "@ngxs/router-plugin";
import {SharedNotificationHelper} from "@shared/helpers/shared-notification.helper";

@Component({
  selector: "dlcm-admin-notification-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-notification-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminNotificationListRoutable extends AbstractListRoutable<NotificationDlcm, AdminNotificationStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof NotificationDlcm & string = "notificationType";
  readonly KEY_QUERY_PARAMETER_NOTIFIED_ORGUNIT_ID: keyof NotificationDlcm | string = "notifiedOrgUnit.resId";

  historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, AdminNotificationStatusHistoryState, state => state.history);
  isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AdminNotificationStatusHistoryState);

  sharedOrgUnitSort: Sort<OrganizationalUnit> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedOrgUnitActionNameSpace: ResourceNameSpace = sharedOrganizationalUnitActionNameSpace;
  sharedOrganizationalUnitState: typeof SharedOrganizationalUnitState = SharedOrganizationalUnitState;
  override isMultiSelectable: boolean = true;
  isHighlightCondition: (notification: NotificationDlcm) => boolean = notification => notification.notificationMark === Enums.Notification.MarkEnum.UNREAD;

  protected _defineBulkActions(): DataTableBulkActions<NotificationDlcm>[] {
    return [
      {
        icon: IconNameEnum.approve,
        callback: (list: NotificationDlcm[]) => this._store.dispatch(new SharedNotificationAction.BulkSetRead(list)),
        labelToTranslate: () => LabelTranslateEnum.markAsRead,
        displayCondition: (list: NotificationDlcm[]) => list.filter(n => n?.notificationMark === Enums.Notification.MarkEnum.UNREAD).length > 0,
        color: ButtonColorEnum.primary,
      },
      {
        icon: IconNameEnum.markAsUnread,
        callback: (list: NotificationDlcm[]) => this._store.dispatch(new SharedNotificationAction.BulkSetUnread(list)),
        labelToTranslate: () => LabelTranslateEnum.markAsUnread,
        displayCondition: (list: NotificationDlcm[]) => list.filter(n => n?.notificationMark === Enums.Notification.MarkEnum.READ).length > 0,
        color: ButtonColorEnum.primary,
      },
    ];
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_notification, adminNotificationActionNameSpace, _injector, {
      canCreate: false,
      canGoBack: false,
      canRefresh: true,
      historyState: AdminNotificationStatusHistoryState,
      historyStateAction: adminNotificationStatusHistoryActionNameSpace,
      historyStatusEnumTranslate: Enums.Notification.StatusEnumTranslate,
    }, StateEnum.admin);

    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(SharedNotificationAction.RefreshListEvent),
      tap(() => {
        const queryParameters = MemoizedUtil.queryParametersSnapshot(this._store, AdminNotificationState);
        this._store.dispatch(new AdminNotificationAction.ChangeQueryParameters(queryParameters, true, true));
      }),
    ));
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.isMultiSelectable = true;
  }

  protected override _defineActions(): DataTableActions<NotificationDlcm>[] {
    return [
      {
        logo: IconNameEnum.approve,
        displayOnCondition: current => current?.notificationStatus === Enums.Notification.StatusEnum.PENDING
          && current?.notificationType.notificationCategory === Enums.Notification.CategoryEnum.REQUEST,
        callback: (current) => this._setApproved(current, ViewModeEnum.list),
        placeholder: (current) => LabelTranslateEnum.markAsDone,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.unapprove,
        displayOnCondition: current => current?.notificationStatus === Enums.Notification.StatusEnum.PENDING
          && current?.notificationType.notificationCategory === Enums.Notification.CategoryEnum.REQUEST,
        callback: (current) => this._setRefuse(current, ViewModeEnum.list),
        placeholder: (current) => LabelTranslateEnum.markAsRefused,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.markAsUnread,
        displayOnCondition: current => current?.notificationMark === Enums.Notification.MarkEnum.READ,
        callback: (current) => this._setUnread(current),
        placeholder: (current) => LabelTranslateEnum.markAsUnread,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.markAsRead,
        displayOnCondition: current => current?.notificationMark === Enums.Notification.MarkEnum.UNREAD,
        callback: (current) => this._setRead(current),
        placeholder: (current) => LabelTranslateEnum.markAsRead,
        isWrapped: true,
      },
    ];
  }

  private _setApproved(notification: NotificationDlcm, mode: ViewModeEnum | undefined): void {
    this._store.dispatch(new SharedNotificationAction.SetApproved(notification.resId, notification.notificationType.notificationCategory, mode));
  }

  private _setRefuse(notification: NotificationDlcm, mode: ViewModeEnum | undefined): void {
    this.subscribe(SharedNotificationHelper.getRefuseDialog(this._dialog, this._store, ViewModeEnum.list, notification));
  }

  private _setRead(notification: NotificationDlcm): void {
    this._store.dispatch(new SharedNotificationAction.SetRead(notification.resId, ViewModeEnum.detail));
  }

  private _setUnread(notification: NotificationDlcm): void {
    this._store.dispatch(new SharedNotificationAction.SetUnread(notification.resId, ViewModeEnum.detail));
  }

  private _goToDetailObject(model: NotificationDlcm): void {
    if (model.notificationMark === Enums.Notification.MarkEnum.UNREAD) {
      this._store.dispatch(new SharedNotificationAction.SetRead(model.resId, ViewModeEnum.detail));
    }
    this._store.dispatch(new Navigate([RoutesEnum.adminNotificationDetail, model.resId]));
  }

  override showDetail(model: NotificationDlcm): void {
    //if it is a notification request, it is redirected directly to the object. If not to the object detail
    this._goToDetailObject(model);
  }

  conditionDisplayEditButton(model: NotificationDlcm | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: NotificationDlcm | undefined): boolean {
    return false;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "notificationType.notificationCategory" as any,
        header: LabelTranslateEnum.category,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Notification.CategoryEnumTranslate,
      },
      {
        field: "notificationType.resId" as any,
        header: LabelTranslateEnum.type,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Notification.TypeEnumTranslate,
      },
      {
        field: "notificationStatus",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Notification.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "emitter.person.fullName" as any,
        header: LabelTranslateEnum.emitter,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
      },
      {
        field: "notifiedOrgUnit.name" as any,
        header: LabelTranslateEnum.notifiedOrganizationalUnit,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        searchableSingleSelectSort: this.sharedOrgUnitSort,
        filterableField: this.KEY_QUERY_PARAMETER_NOTIFIED_ORGUNIT_ID as any,
        sortableField: this.KEY_QUERY_PARAMETER_NOTIFIED_ORGUNIT_ID as any,
        resourceNameSpace: this.sharedOrgUnitActionNameSpace,
        resourceState: this.sharedOrganizationalUnitState as any,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }
}
