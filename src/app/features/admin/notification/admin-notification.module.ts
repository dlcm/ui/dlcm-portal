/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-notification.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminNotificationRoutingModule} from "@admin/notification/admin-notification-routing.module";
import {AdminNotificationDetailEditRoutable} from "@admin/notification/components/routables/admin-notification-detail-edit/admin-notification-detail-edit.routable";
import {AdminNotificationListRoutable} from "@admin/notification/components/routables/admin-notification-list/admin-notification-list.routable";
import {AdminNotificationState} from "@admin/notification/stores/admin-notification.state";
import {AdminNotificationStatusHistoryState} from "@admin/notification/stores/status-history/admin-notification-status-history.state";
import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {
  AdminNotificationArchiveAclExternalDuaDialog,
} from "@admin/notification/components/dialogs/admin-notification-archive-acl-external-dua/admin-notification-archive-acl-external-dua.dialog";

const routables = [
  AdminNotificationListRoutable,
  AdminNotificationDetailEditRoutable,
];
const containers = [];
const dialogs = [
  AdminNotificationArchiveAclExternalDuaDialog,
];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    AdminNotificationRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      AdminNotificationState,
      AdminNotificationStatusHistoryState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class AdminNotificationModule {
}
