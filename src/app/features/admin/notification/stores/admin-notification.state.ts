/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-notification.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminNotificationActionNameSpace} from "@admin/notification/stores/admin-notification.action";
import {
  AdminNotificationStatusHistoryState,
  AdminNotificationStatusHistoryStateModel,
} from "@admin/notification/stores/status-history/admin-notification-status-history.state";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {NotificationDlcm} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  StoreUtil,
} from "solidify-frontend";

const ATTRIBUTE_SIGNED_DUA_FILE: keyof NotificationDlcm = "signedDuaFile";

export interface AdminNotificationStateModel extends ResourceFileStateModel<NotificationDlcm> {
  admin_notification_statusHistory: AdminNotificationStatusHistoryStateModel;
}

@Injectable()
@State<AdminNotificationStateModel>({
  name: StateEnum.admin_notification,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    admin_notification_statusHistory: {...defaultStatusHistoryInitValue()},
  },
  children: [
    AdminNotificationStatusHistoryState,
  ],
})
export class AdminNotificationState extends ResourceFileState<AdminNotificationStateModel, NotificationDlcm> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
  ) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminNotificationActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminNotificationDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.adminNotificationDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminNotification,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.notifications.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.notifications.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.notifications.notification.resource.update"),
      downloadInMemory: false,
      resourceFileApiActionNameUploadCustom: ApiActionNameEnum.UPLOAD_DUA,
      resourceFileApiActionNameDownloadCustom: ApiActionNameEnum.DOWNLOAD_DUA,
      resourceFileApiActionNameDeleteCustom: ApiActionNameEnum.DELETE_DUA,
      customFileAttribute: ATTRIBUTE_SIGNED_DUA_FILE,
    }, _downloadService, ResourceFileStateModeEnum.custom, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminNotifications;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminNotificationStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminNotificationStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminNotificationStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.emitter["person"].fullName;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminNotificationStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminNotificationStateModel): boolean {
    return true;
  }
}
