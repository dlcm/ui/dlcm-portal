/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-institution-person-role.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {adminInstitutionPersonRoleActionNameSpace} from "@admin/institution/stores/institution-person-role/admin-institution-person-role.action";
import {InstitutionPersonRole} from "@admin/models/institution-person-role.model";
import {PersonRole} from "@admin/models/person-role.model";
import {Injectable} from "@angular/core";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";

import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultRelation3TiersStateInitValue,
  NotificationService,
  QueryParameters,
  Relation3TiersForm,
  Relation3TiersState,
  Relation3TiersStateModel,
} from "solidify-frontend";

export const defaultAdminInstitutionPersonRoleStateModel: () => AdminInstitutionPersonRoleStateModel = () =>
  ({
    ...defaultRelation3TiersStateInitValue(),
    queryParameters: new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo),
  });

export interface AdminInstitutionPersonRoleStateModel extends Relation3TiersStateModel<PersonRole> {
}

@Injectable()
@State<AdminInstitutionPersonRoleStateModel>({
  name: StateEnum.admin_institution_personRole,
  defaults: {
    ...defaultAdminInstitutionPersonRoleStateModel(),
  },
})
// InstitutionPersonController
export class AdminInstitutionPersonRoleState extends Relation3TiersState<AdminInstitutionPersonRoleStateModel, PersonRole, InstitutionPersonRole> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminInstitutionPersonRoleActionNameSpace,
      resourceName: ApiResourceNameEnum.PERSON,
      updateGrandChildListActionName: ApiActionNameEnum.SET_ROLE,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminInstitutions;
  }

  protected _convertResourceInForm(resource: PersonRole): Relation3TiersForm {
    return {id: resource.resId, listId: resource.roles.map(r => r.resId)};
  }
}
