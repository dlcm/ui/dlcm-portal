/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-institution.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminInstitutionAction,
  adminInstitutionActionNameSpace,
} from "@admin/institution/stores/admin-institution.action";
import {AdminInstitutionPersonRoleAction} from "@admin/institution/stores/institution-person-role/admin-institution-person-role.action";
import {
  AdminInstitutionPersonRoleState,
  AdminInstitutionPersonRoleStateModel,
  defaultAdminInstitutionPersonRoleStateModel,
} from "@admin/institution/stores/institution-person-role/admin-institution-person-role.state";
import {AdminInstitutionOrganizationalUnitAction} from "@admin/institution/stores/organizational-unit/admin-institution-organizational-unit.action";
import {
  AdminInstitutionOrganizationalUnitState,
  AdminInstitutionOrganizationalUnitStateModel,
} from "@admin/institution/stores/organizational-unit/admin-institution-organizational-unit.state";
import {Injectable} from "@angular/core";
import {AppAuthorizedInstitutionAction} from "@app/stores/authorized-institution/app-authorized-institution.action";
import {AppAuthorizedInstitutionState} from "@app/stores/authorized-institution/app-authorized-institution.state";
import {AppAuthorizedOrganizationalUnitAction} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {environment} from "@environments/environment";
import {
  Institution,
  OrganizationalUnit,
} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  CollectionTyped,
  defaultAssociationStateInitValue,
  defaultResourceFileStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isArray,
  isEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  Relation3TiersForm,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {AppMemberOrganizationalUnitAction} from "@app/stores/member-organizational-unit/app-member-organizational-unit.action";

export interface AdminInstitutionStateModel extends ResourceFileStateModel<Institution> {
  [StateEnum.admin_institution_organizationalUnit]: AdminInstitutionOrganizationalUnitStateModel;
  [StateEnum.admin_institution_personRole]: AdminInstitutionPersonRoleStateModel;
}

@Injectable()
@State<AdminInstitutionStateModel>({
    name: StateEnum.admin_institution,
    defaults: {
      ...defaultResourceFileStateInitValue(),
      [StateEnum.admin_institution_organizationalUnit]: {...defaultAssociationStateInitValue()},
      [StateEnum.admin_institution_personRole]: {...defaultAdminInstitutionPersonRoleStateModel()},
    },
    children: [
      AdminInstitutionPersonRoleState,
      AdminInstitutionOrganizationalUnitState,
    ],
  },
)
export class AdminInstitutionState extends ResourceFileState<AdminInstitutionStateModel, Institution> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              private readonly _securityService: SecurityService,
  ) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: adminInstitutionActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.adminInstitutionDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => {
        const routeDetail = RoutesEnum.adminInstitutionDetail + urlSeparator + resId;
        if (this._securityService.isRootOrAdmin()) {
          return routeDetail;
        }
        const listAuthorizedInstitution = MemoizedUtil.listSnapshot(this._store, AppAuthorizedInstitutionState);
        if (isEmptyArray(listAuthorizedInstitution)) {
          return RoutesEnum.preservationSpaceOrganizationalUnit;
        }
        const isCurrentInstitutionManaged = listAuthorizedInstitution?.findIndex(i => i.resId === resId) >= 0;
        if (isCurrentInstitutionManaged) {
          return routeDetail;
        }
        return RoutesEnum.adminInstitution;
      },
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminInstitution,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.institution.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.institution.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.institution.notification.resource.update"),
      updateSubResourceDispatchMethod: DispatchMethodEnum.SEQUENCIAL,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminInstitutions;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: AdminInstitutionStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminInstitutionStateModel): boolean {
    return this.isLoading(state)
      || StoreUtil.isLoadingState(state[StateEnum.admin_institution_organizationalUnit])
      || StoreUtil.isLoadingState(state[StateEnum.admin_institution_personRole]);
  }

  @Selector()
  static currentTitle(state: AdminInstitutionStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminInstitutionStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && isNotNullNorUndefined(state.current)
      && isNotNullNorUndefined(state[StateEnum.admin_institution_organizationalUnit].selected)
      && isNotNullNorUndefined(state[StateEnum.admin_institution_personRole].selected);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminInstitutionStateModel): boolean {
    return true;
  }

  @OverrideDefaultAction()
  @Action(AdminInstitutionAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<AdminInstitutionStateModel>, action: AdminInstitutionAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    if (isNotNullNorUndefined(action.model.logo)) {
      ctx.dispatch(new AdminInstitutionAction.GetFile(action.model.resId));
    }
  }

  protected override _getListActionsUpdateSubResource(model: Institution, action: AdminInstitutionAction.Create | AdminInstitutionAction.Update, ctx: SolidifyStateContext<AdminInstitutionStateModel>): ActionSubActionCompletionsWrapper[] {
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);

    const institutionId = model.resId;

    const newOrgUnitIds = action.modelFormControlEvent.model.organizationalUnits.map(p => p.resId);

    let concernCurrentUser = false;
    const currentPersonId = MemoizedUtil.currentSnapshot(this._store, AppUserState)?.person?.resId;

    const oldListPersonsRoles = MemoizedUtil.selectedSnapshot(this._store, AdminInstitutionPersonRoleState);
    if (oldListPersonsRoles?.findIndex(p => p.resId === currentPersonId) >= 0) {
      concernCurrentUser = true;
    }

    const newPersonRole = action.modelFormControlEvent.formControl.get("personRole").value as Relation3TiersForm[];
    newPersonRole.forEach((elem) => {
      if (elem.id === currentPersonId) {
        concernCurrentUser = true;
      }
      if (!isNullOrUndefined(elem.listId) && !isArray(elem.listId)) {
        elem.listId = [elem.listId];
      }
    });

    actions.push(...[
      {
        action: new AdminInstitutionOrganizationalUnitAction.Update(institutionId, newOrgUnitIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminInstitutionOrganizationalUnitAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminInstitutionOrganizationalUnitAction.UpdateFail)),
        ],
      },
      {
        action: new AdminInstitutionPersonRoleAction.Update(institutionId, newPersonRole),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AdminInstitutionPersonRoleAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AdminInstitutionPersonRoleAction.UpdateFail)),
        ],
      },
    ]);

    if (concernCurrentUser) {
      actions.push(...[
        {
          action: new AppAuthorizedOrganizationalUnitAction.GetAll(undefined, false, false),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllFail)),
          ],
        },
        {
          action: new AppAuthorizedInstitutionAction.GetAll(),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedInstitutionAction.GetAllSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedInstitutionAction.GetAllFail)),
          ],
        },
        {
          action: new AppMemberOrganizationalUnitAction.GetAll(undefined, false, false),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AppMemberOrganizationalUnitAction.GetAllSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AppMemberOrganizationalUnitAction.GetAllFail)),
          ],
        },
      ]);
    }

    return actions;
  }

  @Action(AdminInstitutionAction.LoadOnlyMyInstitutions)
  loadOnlyMyInstitutions(ctx: SolidifyStateContext<AdminInstitutionStateModel>, action: AdminInstitutionAction.LoadOnlyMyInstitutions): Observable<CollectionTyped<Institution>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });

    return this._apiService.getCollection<OrganizationalUnit>(ApiEnum.adminAuthorizedInstitutions, ctx.getState().queryParameters)
      .pipe(
        tap((collection: CollectionTyped<OrganizationalUnit>) => {
          ctx.dispatch(new AdminInstitutionAction.LoadOnlyMyInstitutionsSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new AdminInstitutionAction.LoadOnlyMyInstitutionsFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(AdminInstitutionAction.LoadOnlyMyInstitutionsSuccess)
  loadOnlyMyInstitutionsSuccess(ctx: SolidifyStateContext<AdminInstitutionStateModel>, action: AdminInstitutionAction.LoadOnlyMyInstitutionsSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    ctx.patchState({
      list: action.list._data,
      total: action.list._page.totalItems,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters,
    });
  }

  @Action(AdminInstitutionAction.LoadOnlyMyInstitutionsFail)
  loadOnlyMyInstitutionsFail(ctx: SolidifyStateContext<AdminInstitutionStateModel>, action: AdminInstitutionAction.LoadOnlyMyInstitutionsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
