/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-institution-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminInstitutionAction} from "@admin/institution/stores/admin-institution.action";
import {PersonRole} from "@admin/models/person-role.model";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {
  Institution,
  OrganizationalUnit,
  Role,
} from "@models";
import {OrgUnitModel} from "@shared/components/containers/shared-table-orgunit/shared-table-orgunit.container";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractApplicationFormPresentational,
  DataTableColumns,
  PropertyName,
  RegexUtil,
  ResourceFileNameSpace,
  SolidifyValidator,
} from "solidify-frontend";
import {AdminInstitutionState} from "../../../stores/admin-institution.state";

@Component({
  selector: "dlcm-admin-institution-form",
  templateUrl: "./admin-institution-form.presentational.html",
  styleUrls: ["./admin-institution-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminInstitutionFormPresentational extends AbstractApplicationFormPresentational<Institution> {
  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  columns: DataTableColumns<Institution>[];

  public formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  adminInstitutionActionNameSpace: ResourceFileNameSpace = AdminInstitutionAction;
  adminInstitutionState: typeof AdminInstitutionState = AdminInstitutionState;

  @Input()
  listOrgUnits: OrganizationalUnit[];

  @Input()
  selectedPersonRole: PersonRole[];

  @Input()
  listRole: Role[];

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              readonly securityService: SecurityService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.url]: ["", [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.description]: ["", [SolidifyValidator]],
      [this.formDefinition.emailSuffixes]: [[], [SolidifyValidator]],
      [this.formDefinition.orgUnit]: ["", [SolidifyValidator]],
      [this.formDefinition.personRole]: ["", [SolidifyValidator]],
      [this.formDefinition.rorId]: ["", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(institution: Institution): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [institution.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.url]: [institution.url, [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.description]: [institution.description, [SolidifyValidator]],
      [this.formDefinition.emailSuffixes]: [[...institution.emailSuffixes], [SolidifyValidator]],
      [this.formDefinition.orgUnit]: [this.listOrgUnits.map(f => f.resId), [SolidifyValidator]],
      [this.formDefinition.personRole]: ["", [SolidifyValidator]],
      [this.formDefinition.rorId]: [institution.rorId, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(institution: Institution): Institution {
    institution.organizationalUnits = [];
    if (this.form.get(this.formDefinition.orgUnit).value !== "") {
      const orgUnits = this.form.get(this.formDefinition.orgUnit).value as OrgUnitModel[];
      orgUnits.forEach((c) => {
        institution.organizationalUnits.push({resId: c.id});
      });
    }
    return institution;
  }

  navigate(value: string[]): void {
    this._navigateBS.next(value);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() url: string;
  @PropertyName() description: string;
  @PropertyName() emailSuffixes: string;
  @PropertyName() orgUnit: string;
  @PropertyName() personRole: string;
  @PropertyName() rorId: string;
}
