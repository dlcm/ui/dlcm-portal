/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-institution-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminInstitutionAction,
  adminInstitutionActionNameSpace,
} from "@admin/institution/stores/admin-institution.action";
import {
  AdminInstitutionState,
  AdminInstitutionStateModel,
} from "@admin/institution/stores/admin-institution.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {Institution} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  CookieConsentUtil,
  CookieType,
  DataTableColumns,
  DataTableFieldTypeEnum,
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
  LocalStorageHelper,
  MappingObjectUtil,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";

const onlyMyInstitutionsStorageKey: LocalStorageEnum = LocalStorageEnum.adminShowOnlyMyInstitutions;
const onlyMyInstitutionsFn: () => boolean = () => {
  const storageValue = LocalStorageHelper.getItem(onlyMyInstitutionsStorageKey);
  if (isNullOrUndefined(storageValue)) {
    return true;
  }
  return storageValue === SOLIDIFY_CONSTANTS.STRING_TRUE;
};

@Component({
  selector: "dlcm-admin-institution-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-institution-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminInstitutionListRoutable extends AbstractListRoutable<Institution, AdminInstitutionStateModel> {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof Institution & string = "name";

  readonly KEY_SHOW_MEMBERS: string = "showMembers";

  adminInstitutionState: typeof AdminInstitutionState = AdminInstitutionState;

  columnsSkippedToClear: string[] = [this.KEY_SHOW_MEMBERS];

  private _onlyMyInstitutions: boolean = undefined;

  protected get onlyMyInstitutions(): boolean {
    if (isNotNullNorUndefined(this._onlyMyInstitutions)) {
      return this._onlyMyInstitutions;
    }
    return onlyMyInstitutionsFn();
  }

  protected set onlyMyInstitutions(value: boolean) {
    this._onlyMyInstitutions = value;
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, onlyMyInstitutionsStorageKey)) {
      LocalStorageHelper.setItem(onlyMyInstitutionsStorageKey, this.onlyMyInstitutions + "");
    }
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_institution, adminInstitutionActionNameSpace, _injector, {
      canCreate: _securityService.isRootOrAdmin(),
      listExtraButtons: [
        {
          color: "primary",
          icon: null,
          labelToTranslate: (current) => LabelTranslateEnum.showOnlyMyInstitutions,
          callback: (model, buttonElementRef, checked) => this._showOnlyMyInstitution(checked),
          order: 40,
          isToggleButton: true,
          isToggleChecked: onlyMyInstitutionsFn(),
        },
      ],
    }, StateEnum.admin);
  }

  conditionDisplayEditButton(model: Institution | undefined): boolean {
    return this._securityService.isManagerOfInstitution(model.resId);
  }

  conditionDisplayDeleteButton(model: Institution | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  private _showOnlyMyInstitution(checked: boolean): void {
    if (checked) {
      //show only authorized org units
      const currentQueryParam = MemoizedUtil.queryParametersSnapshot(this._store, AdminInstitutionState);
      const newQueryParam = new QueryParameters(currentQueryParam?.paging?.pageSize);
      MappingObjectUtil.set(newQueryParam.search.searchItems, this.KEY_SHOW_MEMBERS, SOLIDIFY_CONSTANTS.STRING_TRUE);
      this._store.dispatch(new AdminInstitutionAction.LoadOnlyMyInstitutions(newQueryParam));
      this.onlyMyInstitutions = true;
    } else {
      // all org units
      const queryParameters = QueryParametersUtil.clone(MemoizedUtil.queryParametersSnapshot(this._store, AdminInstitutionState));
      MappingObjectUtil.delete(QueryParametersUtil.getSearchItems(queryParameters), this.KEY_PARAM_NAME);
      this._store.dispatch(new AdminInstitutionAction.GetAll());
      this.onlyMyInstitutions = false;
    }

    this.defineColumns();
    this._changeDetector.detectChanges();
  }

  override onQueryParametersEvent(queryParameters: QueryParameters): void {
    if (this.onlyMyInstitutions) {
      MappingObjectUtil.set(queryParameters.search.searchItems, this.KEY_SHOW_MEMBERS, SOLIDIFY_CONSTANTS.STRING_TRUE);
      this._store.dispatch(new AdminInstitutionAction.LoadOnlyMyInstitutions(queryParameters));
    } else {
      this._store.dispatch(new AdminInstitutionAction.ChangeQueryParameters(queryParameters, true));
    }
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "avatar" as any,
        header: LabelTranslateEnum.logo,
        order: OrderEnum.none,
        resourceNameSpace: adminInstitutionActionNameSpace,
        resourceState: this.adminInstitutionState as any,
        isFilterable: false,
        isSortable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.logo),
        isUser: false,
        skipIfAvatarMissing: true,
        idResource: (institution: Institution) => institution.resId,
        isLogoPresent: (institution: Institution) => isNotNullNorUndefined(institution.logo),
        width: "45px",
      } as DataTableColumns<Institution>,
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: isFalse(this.onlyMyInstitutions),
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "resId",
        header: "",
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        component: DataTableComponentHelper.get(DataTableComponentEnum.institutionMember),
        isFilterable: false,
        isSortable: false,
        width: "40px",
      },
    ];
  }
}
