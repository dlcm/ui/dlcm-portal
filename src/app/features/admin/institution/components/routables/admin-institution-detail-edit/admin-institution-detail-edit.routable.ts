/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-institution-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminInstitutionFormPresentational} from "@admin/institution/components/presentationals/admin-institution-form/admin-institution-form.presentational";
import {adminInstitutionActionNameSpace} from "@admin/institution/stores/admin-institution.action";
import {
  AdminInstitutionState,
  AdminInstitutionStateModel,
} from "@admin/institution/stores/admin-institution.state";
import {AdminInstitutionPersonRoleAction} from "@admin/institution/stores/institution-person-role/admin-institution-person-role.action";
import {AdminInstitutionPersonRoleState} from "@admin/institution/stores/institution-person-role/admin-institution-person-role.state";
import {AdminInstitutionOrganizationalUnitAction} from "@admin/institution/stores/organizational-unit/admin-institution-organizational-unit.action";
import {AdminInstitutionOrganizationalUnitState} from "@admin/institution/stores/organizational-unit/admin-institution-organizational-unit.state";
import {PersonRole} from "@admin/models/person-role.model";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  Institution,
  OrganizationalUnit,
  Role,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {SharedResourceRoleMemberContainerMode} from "@shared/components/containers/shared-resource-role-member/shared-resource-role-member-container.component";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {sharedInstitutionActionNameSpace} from "@shared/stores/institution/shared-institution.action";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  MemoizedUtil,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-institution-detail-edit-routable",
  templateUrl: "./admin-institution-detail-edit.routable.html",
  styleUrls: ["./admin-institution-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminInstitutionDetailEditRoutable extends AbstractDetailEditCommonRoutable<Institution, AdminInstitutionStateModel> {
  @Select(AdminInstitutionState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(AdminInstitutionState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  selectedOrgUnitsObs: Observable<OrganizationalUnit[]> = MemoizedUtil.selected(this._store, AdminInstitutionOrganizationalUnitState);
  selectedPersonRoleObs: Observable<PersonRole[]> = MemoizedUtil.selected(this._store, AdminInstitutionPersonRoleState);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedInstitutionActionNameSpace;

  @ViewChild("formPresentational")
  readonly formPresentational: AdminInstitutionFormPresentational;

  readonly KEY_PARAM_NAME: keyof Institution & string = "name";

  get sharedResourceRoleMemberContainerMode(): typeof SharedResourceRoleMemberContainerMode {
    return SharedResourceRoleMemberContainerMode;
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.admin_institution, _injector, adminInstitutionActionNameSpace, StateEnum.admin);
    this.editAvailable = this._securityService.isRootOrAdmin();
    this.deleteAvailable = this._securityService.isRootOrAdmin();
  }

  _getSubResourceWithParentId(id: string): void {
    if (this._securityService.isManagerOfInstitution(id)) {
      this.editAvailable = true;
    }
    this._store.dispatch(new AdminInstitutionOrganizationalUnitAction.GetAll(id));
    this._store.dispatch(new AdminInstitutionPersonRoleAction.GetAll(id));
  }

  navigate($event: string[]): void {
    this._store.dispatch(new Navigate($event));
  }
}
