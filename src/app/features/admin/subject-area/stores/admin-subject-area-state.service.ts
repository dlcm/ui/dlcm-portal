/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-subject-area-state.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AdminSubjectAreaAction,
  adminSubjectAreaActionNameSpace,
} from "@admin/subject-area/stores/admin-subject-area.action";
import {Injectable} from "@angular/core";
import {DepositStateModel} from "@deposit/stores/deposit.state";
import {environment} from "@environments/environment";
import {SubjectArea} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedLanguageAction} from "@shared/stores/language/shared-language.action";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {
  ApiService,
  defaultResourceStateInitValue,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ofSolidifyActionCompleted,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface AdminSubjectAreaStateModel extends ResourceStateModel<SubjectArea> {

}

@Injectable()
@State<AdminSubjectAreaStateModel>({
  name: StateEnum.admin_subjectArea,
  defaults: {
    ...defaultResourceStateInitValue(),
  },
})
export class AdminSubjectAreaState extends ResourceState<AdminSubjectAreaStateModel, SubjectArea> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: adminSubjectAreaActionNameSpace,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.adminSubjectArea,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.subjectArea.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.subjectArea.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("admin.subjectArea.notification.resource.update"),
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminSubjectAreas;
  }

  @Selector()
  static isLoading(state: AdminSubjectAreaStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: AdminSubjectAreaStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: AdminSubjectAreaStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isReadyToBeDisplayed(state: AdminSubjectAreaStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: AdminSubjectAreaStateModel): boolean {
    return true;
  }

  @Action(AdminSubjectAreaAction.LoadResource)
  loadResource(ctx: SolidifyStateContext<DepositStateModel>, action: AdminSubjectAreaAction.LoadResource): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new SharedLanguageAction.GetAll(null, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedLanguageAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedLanguageAction.GetAllFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new AdminSubjectAreaAction.LoadResourceSuccess(action));
        } else {
          ctx.dispatch(new AdminSubjectAreaAction.LoadResourceFail(action));
        }
        return result.success;
      }),
    );
  }
}
