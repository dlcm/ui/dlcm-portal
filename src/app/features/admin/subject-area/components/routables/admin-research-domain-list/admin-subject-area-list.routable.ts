/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-subject-area-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminSubjectAreaCreateEditDialog} from "@admin/subject-area/components/dialogs/admin-subject-area-create-edit/admin-subject-area-create-edit-dialog.component";
import {adminSubjectAreaActionNameSpace} from "@admin/subject-area/stores/admin-subject-area.action";
import {AdminSubjectAreaStateModel} from "@admin/subject-area/stores/admin-subject-area-state.service";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  Language,
  SubjectArea,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {SharedLanguageAction} from "@shared/stores/language/shared-language.action";
import {SharedLanguageState} from "@shared/stores/language/shared-language.state";
import {Observable} from "rxjs";
import {
  filter,
  take,
} from "rxjs/operators";
import {
  AbstractListRoutable,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  isNullOrUndefined,
  isTrue,
  isTruthyObject,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-subject-area-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./admin-subject-area-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminSubjectAreaListRoutable extends AbstractListRoutable<SubjectArea, AdminSubjectAreaStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToAdmin;
  readonly KEY_PARAM_NAME: keyof SubjectArea & string = "name";

  languagesObs: Observable<Language[]> = MemoizedUtil.list(this._store, SharedLanguageState);

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.admin_subjectArea, adminSubjectAreaActionNameSpace, _injector, {
      canCreate: _securityService.isRootOrAdmin(),
    }, StateEnum.admin);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._store.dispatch(new SharedLanguageAction.GetAll());
    this.subscribe(this.languagesObs.pipe(
        filter(languages => !isNullOrUndefined(languages)),
        take(1),
      ),
      languages => this._addLanguagesColumns(languages));
  }

  conditionDisplayEditButton(model: SubjectArea | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  conditionDisplayDeleteButton(model: SubjectArea | undefined): boolean {
    return this._securityService.isRootOrAdmin();
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "code",
        header: LabelTranslateEnum.code,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        width: "100px",
      },
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "source" as any,
        header: LabelTranslateEnum.source,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  private _addLanguagesColumns(listLanguages: Language[]): void {
    const languagesColumn: DataTableColumns<SubjectArea>[] = [];
    listLanguages.forEach(language => {
      languagesColumn.push({
        field: language.resId as any,
        header: MARK_AS_TRANSLATABLE(language.resId),
        order: OrderEnum.none,
        component: DataTableComponentHelper.get(DataTableComponentEnum.adminSubjectAreaLabel),
      });
    });
    this.columns = [...this.columns, ...languagesColumn];
  }

  override create(element: ElementRef): void {
    this.subscribe(DialogUtil.open(this._dialog, AdminSubjectAreaCreateEditDialog, {
        current: undefined,
      },
      {
        minWidth: "500px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          if (isTruthyObject(element)) {
            element.nativeElement.focus();
          }
          this.getAll();
        }
      },
      () => {
        if (isTruthyObject(element)) {
          element.nativeElement.focus();
        }
      }));
  }

  override goToEdit(model: SubjectArea): void {
    this.subscribe(DialogUtil.open(this._dialog, AdminSubjectAreaCreateEditDialog, {
        current: model,
      },
      {
        minWidth: "500px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this.getAll();
        }
      }));
  }

  override showDetail(model: SubjectArea): void {
    if (this._securityService.isRootOrAdmin()) {
      this.goToEdit(model);
    }
  }
}
