/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-subject-area-create-edit-dialog.component.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminSubjectAreaStateModel} from "@admin/subject-area/stores/admin-subject-area-state.service";
import {adminSubjectAreaActionNameSpace} from "@admin/subject-area/stores/admin-subject-area.action";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  Language,
  SubjectArea,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractCreateEditDialog} from "@shared/components/dialogs/shared-abstract-create-edit/shared-abstract-create-edit.dialog";
import {StateEnum} from "@shared/enums/state.enum";
import {CreateEditDialog} from "@shared/models/detail-edit-dialog.model";
import {SharedLanguageState} from "@shared/stores/language/shared-language.state";
import {sharedSubjectAreaActionNameSpace} from "@shared/stores/subject-area/shared-subject-area.action";
import {Observable} from "rxjs";
import {
  MemoizedUtil,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "dlcm-subject-area-create-edit-dialog",
  templateUrl: "./admin-subject-area-create-edit-dialog.component.html",
  styleUrls: ["./admin-subject-area-create-edit-dialog.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminSubjectAreaCreateEditDialog extends SharedAbstractCreateEditDialog<SubjectArea, AdminSubjectAreaStateModel, CreateEditDialog<SubjectArea>> {
  languagesObs: Observable<Language[]> = MemoizedUtil.list(this._store, SharedLanguageState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedSubjectAreaActionNameSpace;

  constructor(@Inject(MAT_DIALOG_DATA) protected readonly _data: CreateEditDialog<SubjectArea>,
              protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector,
              protected readonly _dialog: MatDialog,
              protected readonly _dialogRef: MatDialogRef<AdminSubjectAreaCreateEditDialog>) {
    super(_data, _store, _route, _actions$, _changeDetector, _dialog, _dialogRef, StateEnum.admin_subjectArea,
      adminSubjectAreaActionNameSpace, _injector, StateEnum.admin);
  }

  _getSubResourceWithParentId(id: string): void {
  }

  readonly KEY_CANCEL_BUTTON: string | undefined;
  readonly KEY_CREATE_BUTTON: string;
  readonly KEY_CREATE_TITLE: string | undefined;
  readonly KEY_UPDATE_BUTTON: string;
  readonly KEY_UPDATE_TITLE: string | undefined;
}
