/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - admin-subject-area-form-presentational.component.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {
  Label,
  Language,
  SubjectArea,
} from "@models";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  AbstractFormPresentational,
  isNullOrUndefined,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-admin-subject-area-form",
  templateUrl: "./admin-subject-area-form-presentational.component.html",
  styleUrls: ["./admin-subject-area-form-presentational.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminSubjectAreaFormPresentational extends AbstractFormPresentational<SubjectArea> {
  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  private _languages: Language[];

  @Input()
  set languages(value: Language[]) {
    this._languages = value;

    if (isNullOrUndefined(this.languages)) {
      return;
    }
    this.languages.forEach(language => {
      if (isNullOrUndefined(this.model) || isNullOrUndefined(this.model.labels)) {
        this._addControl(language.resId);
        return;
      }
      const existingValue = (this.model.labels as Label[]).find(label => label.language.resId === language.resId);
      if (isNullOrUndefined(existingValue)) {
        this._addControl(language.resId);
        return;
      }
      this._addControl(language.resId, existingValue.text);
    });
  }

  get languages(): Language[] {
    return this._languages;
  }

  private _addControl(languageResId: string, value: string | undefined = undefined): void {
    this.form.addControl(languageResId, this._fb.control(value));
  }

  public formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.source]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.code]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  protected _bindFormTo(subjectAreas: SubjectArea): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [subjectAreas.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.source]: [subjectAreas.source, [Validators.required, SolidifyValidator]],
      [this.formDefinition.code]: [subjectAreas.code, [Validators.required, SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(subjectAreas: SubjectArea): SubjectArea {
    const newLabels: Label[] = [];
    this.languages.forEach(language => {
      newLabels.push({
        text: subjectAreas[language.resId],
        language: language,
      });
    });
    return {
      code: subjectAreas.code,
      name: subjectAreas.name,
      source: subjectAreas.source,
      labels: newLabels,
    };
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() source: string;
  @PropertyName() code: string;
}
