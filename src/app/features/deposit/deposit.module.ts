/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {DepositFileUploadDialog} from "@app/features/deposit/components/dialogs/deposit-file-upload/deposit-file-upload.dialog";
import {DepositFormPresentational} from "@app/features/deposit/components/presentationals/deposit-form/deposit-form.presentational";
import {DepositListRoutable} from "@app/features/deposit/components/routables/deposit-list/deposit-list.routable";
import {DepositRoutingModule} from "@app/features/deposit/deposit-routing.module";
import {DepositDataFileState} from "@app/features/deposit/stores/data-file/deposit-data-file.state";
import {DepositState} from "@app/features/deposit/stores/deposit.state";
import {DepositPeopleState} from "@app/features/deposit/stores/people/deposit-people.state";
import {SharedModule} from "@app/shared/shared.module";
import {DepositCollectionContainer} from "@deposit/components/containers/deposit-collection/deposit-collection.container";
import {DepositFileContainer} from "@deposit/components/containers/deposit-file/deposit-file.container";
import {DepositUploadContainer} from "@deposit/components/containers/deposit-upload/deposit-upload.container";
import {DepositAssociateArchiveDialog} from "@deposit/components/dialogs/deposit-associate-archive/deposit-associate-archive.dialog";
import {DepositFileChangeDataCategoryDialog} from "@deposit/components/dialogs/deposit-file-change-data-category/deposit-file-change-data-category.dialog";
import {DepositFileMoveDialog} from "@deposit/components/dialogs/deposit-file-move/deposit-file-move.dialog";
import {DepositFileUploadArchiveDialog} from "@deposit/components/dialogs/deposit-file-upload-archive/deposit-file-upload-archive.dialog";
import {DepositOrderAuthorDialog} from "@deposit/components/dialogs/deposit-order-author/deposit-order-author.dialog";
import {DepositPersonAlternativeDialog} from "@deposit/components/dialogs/deposit-person-alternative/deposit-person-alternative.dialog";
import {DepositPersonDialog} from "@deposit/components/dialogs/deposit-person/deposit-person.dialog";
import {DepositRejectDialog} from "@deposit/components/dialogs/deposit-reject/deposit-reject.dialog";
import {DepositSubmitDialog} from "@deposit/components/dialogs/deposit-submit/deposit-submit.dialog";
import {DepositNewPersonFormPresentational} from "@deposit/components/presentationals/deposit-new-person-form/deposit-new-person-form.presentational";
import {DepositReadmeFormPresentational} from "@deposit/components/presentationals/deposit-readme-form/deposit-readme-form.presentational";
import {DepositCollectionDetailRoutable} from "@deposit/components/routables/deposit-collection-detail/deposit-collection-detail.routable";
import {DepositCollectionRoutable} from "@deposit/components/routables/deposit-collection/deposit-collection.routable";
import {DepositDataRoutable} from "@deposit/components/routables/deposit-data/deposit-data.routable";
import {DepositFileDetailRoutable} from "@deposit/components/routables/deposit-file-detail/deposit-file-detail.routable";
import {DepositFileRoutable} from "@deposit/components/routables/deposit-file/deposit-file.routable";
import {DepositHomeRoutable} from "@deposit/components/routables/deposit-home/deposit-home.routable";
import {DepositMetadataRoutable} from "@deposit/components/routables/deposit-metadata/deposit-metadata.routable";
import {DepositReadmeRoutable} from "@deposit/components/routables/deposit-readme/deposit-readme.routable";
import {DepositRootRoutable} from "@deposit/components/routables/deposit-root/deposit-root.routable";
import {DepositArchiveState} from "@deposit/stores/archive/deposit-archive.state";
import {DepositAuthorizedOrganizationalUnitState} from "@deposit/stores/authorized-organizational-unit/deposit-authorized-organizational-unit.state";
import {DepositCollectionState} from "@deposit/stores/collection/deposit-collection.state";
import {DepositCollectionStatusHistoryState} from "@deposit/stores/collection/status-history/deposit-collection-status-history.state";
import {DepositDataFileStatusHistoryState} from "@deposit/stores/data-file/status-history/deposit-data-file-status-history.state";
import {DepositOrganizationalUnitAdditionalFieldsFormState} from "@deposit/stores/organizational-unit/additional-fields-form/deposit-organizational-unit-additional-fields-form.state";
import {DepositOrganizationalUnitState} from "@deposit/stores/organizational-unit/deposit-organizational-unit.state";
import {DepositOrganizationalUnitSubjectAreaState} from "@deposit/stores/organizational-unit/subject-area/deposit-organizational-unit-subject-area.state";
import {DepositSipState} from "@deposit/stores/sip/deposit-sip.state";
import {DepositStatusHistoryState} from "@deposit/stores/status-history/deposit-status-history.state";
import {DepositSubjectAreaState} from "@deposit/stores/subject-area/deposit-subject-area.state";
import {DepositUploadState} from "@deposit/stores/upload/deposit-upload.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SsrUtil} from "solidify-frontend";
import {DepositFilesUploadInProgressPresentational} from "./components/presentationals/deposit-files-upload-in-progress/deposit-files-upload-in-progress.presentational";
import {DepositCreateRoutable} from "./components/routables/deposit-create/deposit-create.routable";
import {DepositDetailEditRoutable} from "./components/routables/deposit-detail-edit/deposit-detail-edit.routable";
import {DepositService} from "./services/deposit.service";
import {DepositDataFileMetadataTypeState} from "./stores/data-file/metadata-type/deposit-data-file-metadata-type.state";
import {DepositDuaDataFileState} from "./stores/dua-data-file/deposit-dua-data-file.state";

const routables = [
  DepositRootRoutable,
  DepositHomeRoutable,
  DepositListRoutable,
  DepositCreateRoutable,
  DepositDetailEditRoutable,
  DepositReadmeRoutable,
  DepositDataRoutable,
  DepositFileRoutable,
  DepositFileDetailRoutable,
  DepositCollectionRoutable,
  DepositCollectionDetailRoutable,
  DepositMetadataRoutable,
];
const containers = [
  DepositUploadContainer,
  DepositFileContainer,
  DepositCollectionContainer,
];
const dialogs = [
  DepositFileUploadDialog,
  DepositFileUploadArchiveDialog,
  DepositPersonDialog,
  DepositOrderAuthorDialog,
  DepositPersonAlternativeDialog,
  DepositAssociateArchiveDialog,
  DepositFileMoveDialog,
  DepositFileChangeDataCategoryDialog,
  DepositRejectDialog,
  DepositSubmitDialog,
];
const presentationals = [
  DepositFormPresentational,
  DepositReadmeFormPresentational,
  DepositFilesUploadInProgressPresentational,
  DepositNewPersonFormPresentational,
];
const states = [
  DepositState,
  DepositUploadState,
  DepositDataFileState,
  DepositDuaDataFileState,
  DepositPeopleState,
  DepositStatusHistoryState,
  DepositDataFileStatusHistoryState,
  DepositDataFileMetadataTypeState,
  DepositOrganizationalUnitState,
  DepositCollectionState,
  DepositCollectionStatusHistoryState,
  DepositArchiveState,
  DepositSipState,
  DepositAuthorizedOrganizationalUnitState,
  DepositOrganizationalUnitAdditionalFieldsFormState,
  DepositSubjectAreaState,
  DepositOrganizationalUnitSubjectAreaState,
];
const services = [
  DepositService,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    DepositRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      ...states,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [
    ...services,
  ],
})
export class DepositModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.depositModuleLoaded] = true;
    }
  }
}
