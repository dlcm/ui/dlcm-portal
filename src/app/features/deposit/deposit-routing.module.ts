/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {DepositCreateRoutable} from "@app/features/deposit/components/routables/deposit-create/deposit-create.routable";
import {DepositListRoutable} from "@app/features/deposit/components/routables/deposit-list/deposit-list.routable";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {DlcmRoutes} from "@app/shared/models/dlcm-route.model";
import {DepositCollectionDetailRoutable} from "@deposit/components/routables/deposit-collection-detail/deposit-collection-detail.routable";
import {DepositCollectionRoutable} from "@deposit/components/routables/deposit-collection/deposit-collection.routable";
import {DepositDataRoutable} from "@deposit/components/routables/deposit-data/deposit-data.routable";
import {DepositDetailEditRoutable} from "@deposit/components/routables/deposit-detail-edit/deposit-detail-edit.routable";
import {DepositFileDetailRoutable} from "@deposit/components/routables/deposit-file-detail/deposit-file-detail.routable";
import {DepositFileRoutable} from "@deposit/components/routables/deposit-file/deposit-file.routable";
import {DepositHomeRoutable} from "@deposit/components/routables/deposit-home/deposit-home.routable";
import {DepositMetadataRoutable} from "@deposit/components/routables/deposit-metadata/deposit-metadata.routable";
import {DepositReadmeRoutable} from "@deposit/components/routables/deposit-readme/deposit-readme.routable";
import {DepositRootRoutable} from "@deposit/components/routables/deposit-root/deposit-root.routable";
import {
  DepositTabStatusEnum,
  DepositTabStatusName,
} from "@deposit/enums/deposit-tab-status.enum";
import {DepositState} from "@deposit/stores/deposit.state";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {ApplicationAuthorizedOrgUnitService} from "@shared/guards/application-authorized-org-unit.service";
import {DepositRoleGuardDetailService} from "@shared/guards/deposit-role-guard-detail.service";
import {DepositRoleGuardEditService} from "@shared/guards/deposit-role-guard-edit.service";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
  MappingObject,
  MappingObjectUtil,
} from "solidify-frontend";

export const callbackDepositTabBreadcrumb: (params: MappingObject<string, DepositTabStatusEnum>) => string | undefined = (params: MappingObject<string, DepositTabStatusEnum>) => {
  const tabStatus = MappingObjectUtil.get(params, DepositRoutesEnum.paramTabWithoutPrefix as string);
  switch (tabStatus) {
    case DepositTabStatusEnum.all:
      return DepositTabStatusName.all;
    case DepositTabStatusEnum.inProgress:
      return DepositTabStatusName.inProgress;
    case DepositTabStatusEnum.inValidation:
      return DepositTabStatusName.inValidation;
    case DepositTabStatusEnum.approved:
      return DepositTabStatusName.approved;
    case DepositTabStatusEnum.completed:
      return DepositTabStatusName.completed;
    case DepositTabStatusEnum.rejected:
      return DepositTabStatusName.rejected;
    case DepositTabStatusEnum.inError:
      return DepositTabStatusName.inError;
  }
  return undefined;
};

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: DepositRootRoutable,
  },
  {
    path: AppRoutesEnum.paramIdOrgUnit,
    redirectTo: AppRoutesEnum.paramIdOrgUnit + AppRoutesEnum.separator + DepositTabStatusEnum.inProgress,
    pathMatch: "full",
  },
  {
    path: AppRoutesEnum.paramIdOrgUnit + AppRoutesEnum.separator + DepositRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId + AppRoutesEnum.separator + DepositRoutesEnum.edit,
    redirectTo: AppRoutesEnum.paramIdOrgUnit + AppRoutesEnum.separator + DepositRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId + AppRoutesEnum.separator + DepositRoutesEnum.metadata + AppRoutesEnum.separator + DepositRoutesEnum.edit,
    pathMatch: "full",
  },
  {
    path: AppRoutesEnum.paramIdOrgUnit + AppRoutesEnum.separator + DepositRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    redirectTo: AppRoutesEnum.paramIdOrgUnit + AppRoutesEnum.separator + DepositRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId + AppRoutesEnum.separator + DepositRoutesEnum.metadata,
    pathMatch: "full",
  },
  {
    path: AppRoutesEnum.paramIdOrgUnit,
    component: DepositHomeRoutable,
    data: {
      breadcrumbMemoizedSelector: DepositState.currentOrgUnitName,
      guards: [ApplicationAuthorizedOrgUnitService],
    },
    children: [
      {
        path: DepositRoutesEnum.create,
        component: DepositCreateRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.create,
        },
        canDeactivate: [CanDeactivateGuard],
      },
      {
        path: DepositRoutesEnum.paramTab,
        component: DepositListRoutable,
        data: {
          breadcrumb: callbackDepositTabBreadcrumb,
        },
      },
      {
        path: DepositRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
        component: DepositDetailEditRoutable,
        data: {
          breadcrumbMemoizedSelector: DepositState.currentTitle,
          guards: [DepositRoleGuardDetailService],
        },
        canActivate: [CombinedGuardService],
        children: [
          {
            path: DepositRoutesEnum.metadata,
            component: DepositMetadataRoutable,
            children: [
              {
                path: DepositRoutesEnum.edit,
                component: EmptyContainer,
                data: {
                  breadcrumb: LabelTranslateEnum.edit,
                  guards: [DepositRoleGuardEditService],
                },
                canActivate: [CombinedGuardService],
                canDeactivate: [CanDeactivateGuard],
              },
            ],
          },
          {
            path: DepositRoutesEnum.readme,
            component: DepositReadmeRoutable,
            children: [
              {
                path: DepositRoutesEnum.edit,
                component: EmptyContainer,
                data: {
                  breadcrumb: LabelTranslateEnum.edit,
                  guards: [DepositRoleGuardEditService],
                },
                canActivate: [CombinedGuardService],
                canDeactivate: [CanDeactivateGuard],
              },
            ],
          },
          {
            path: DepositRoutesEnum.data,
            component: DepositDataRoutable,
          },
          {
            path: DepositRoutesEnum.files,
            component: DepositFileRoutable,
            children: [
              {
                path: AppRoutesEnum.paramId,
                component: DepositFileDetailRoutable,
                data: {
                  breadcrumb: LabelTranslateEnum.detail,
                },
              },
            ],
          },
          {
            path: DepositRoutesEnum.collections,
            component: DepositCollectionRoutable,
            children: [
              {
                path: AppRoutesEnum.paramId,
                component: DepositCollectionDetailRoutable,
                data: {
                  breadcrumb: LabelTranslateEnum.detail,
                },
              },
            ],
          },
        ],
      },
    ],
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DepositRoutingModule {
}
