/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-people.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {depositPersonActionNameSpace} from "@app/features/deposit/stores/people/deposit-person.action";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {Person} from "@models";

import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  AssociationRemoteState,
  AssociationRemoteStateModel,
  defaultAssociationRemoteStateInitValue,
  NotificationService,
  QueryParameters,
} from "solidify-frontend";

export const defaultDepositPersonStateModel: () => DepositPersonStateModel = () =>
  ({
    ...defaultAssociationRemoteStateInitValue(),
    queryParameters: new QueryParameters(environment.maximalPageSizeToRetrievePaginationInfo),
  });

export interface DepositPersonStateModel extends AssociationRemoteStateModel<Person> {
}

@Injectable()
@State<DepositPersonStateModel>({
  name: StateEnum.deposit_person,
  defaults: {
    ...defaultDepositPersonStateModel(),
  },
})
export class DepositPeopleState extends AssociationRemoteState<DepositPersonStateModel, Person> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: depositPersonActionNameSpace,
      resourceName: ApiResourceNameEnum.CONTRIBUTOR,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.preIngestDeposits;
  }
}
