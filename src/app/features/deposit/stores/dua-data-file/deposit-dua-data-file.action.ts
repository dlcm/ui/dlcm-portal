/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-dua-data-file.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DepositDataFile} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  CompositionAction,
  CompositionNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.deposit_duaDataFile;

export namespace DepositDuaDataFileAction {
  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends CompositionAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends CompositionAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends CompositionAction.GetAllSuccess<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends CompositionAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends CompositionAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends CompositionAction.GetByIdSuccess<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends CompositionAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends CompositionAction.Update<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends CompositionAction.UpdateSuccess<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends CompositionAction.UpdateFail<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class Create extends CompositionAction.Create<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends CompositionAction.CreateSuccess<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends CompositionAction.CreateFail<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class Delete extends CompositionAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends CompositionAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends CompositionAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends CompositionAction.Clean {
  }

  export class GetDuaFile extends BaseAction {
    static readonly type: string = `[${state}] Get Dua File`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class GetDuaFileSuccess extends BaseSubActionSuccess<GetDuaFile> {
    static readonly type: string = `[${state}] Get Dua File Success`;

    constructor(public parentAction: GetDuaFile, public duaDataFile: DepositDataFile | undefined) {
      super(parentAction);
    }
  }

  export class GetDuaFileFail extends BaseSubActionFail<GetDuaFile> {
    static readonly type: string = `[${state}] Get Dua File Fail`;
  }
}

export const depositDuaDataFileActionNameSpace: CompositionNameSpace = DepositDuaDataFileAction;
