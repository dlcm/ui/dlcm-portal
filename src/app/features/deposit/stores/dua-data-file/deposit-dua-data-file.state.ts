/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-dua-data-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  DepositDuaDataFileAction,
  depositDuaDataFileActionNameSpace,
} from "@deposit/stores/dua-data-file/deposit-dua-data-file.action";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {DepositDataFile} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  DownloadService,
  isNonEmptyArray,
  MappingObjectUtil,
  NotificationService,
  QueryParameters,
  QueryParametersUtil,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export const defaultDepositDuaDataFileValue: () => DepositDuaDataFileStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
  });

export interface DepositDuaDataFileStateModel extends CompositionStateModel<DepositDataFile> {
}

@Injectable()
@State<DepositDuaDataFileStateModel>({
  name: StateEnum.deposit_duaDataFile,
  defaults: {
    ...defaultDepositDuaDataFileValue(),
  },
  children: [],
})
export class DepositDuaDataFileState extends CompositionState<DepositDuaDataFileStateModel, DepositDataFile> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _httpClient: HttpClient,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: depositDuaDataFileActionNameSpace,
      resourceName: ApiResourceNameEnum.DATAFILE,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.preIngestDeposits;
  }

  @Action(DepositDuaDataFileAction.GetDuaFile)
  getDuaFile(ctx: SolidifyStateContext<DepositDuaDataFileStateModel>, action: DepositDuaDataFileAction.GetDuaFile): Observable<DepositDuaDataFileAction.GetAllSuccess | DepositDuaDataFileAction.GetAllFail> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const queryParameters = new QueryParameters(1);
    const searchItems = QueryParametersUtil.getSearchItems(queryParameters);
    MappingObjectUtil.set(searchItems, "dataCategory", Enums.DataFile.DataCategoryAndType.DataCategoryEnum.INTERNAL);
    MappingObjectUtil.set(searchItems, "dataType", Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_DATA_USE_AGREEMENT);

    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new DepositDuaDataFileAction.GetAll(action.depositId, queryParameters),
      DepositDuaDataFileAction.GetAllSuccess,
      result => {
        const duaFile = isNonEmptyArray(result.list?._data) ? result.list._data[0] : undefined;
        ctx.dispatch(new DepositDuaDataFileAction.GetDuaFileSuccess(action, duaFile));
      },
      DepositDuaDataFileAction.GetAllFail,
      result => {
        ctx.dispatch(new DepositDuaDataFileAction.GetDuaFileFail(action));
      },
    );
  }

  @Action(DepositDuaDataFileAction.GetDuaFileSuccess)
  getDuaFileSuccess(ctx: SolidifyStateContext<DepositDuaDataFileStateModel>, action: DepositDuaDataFileAction.GetDuaFileSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.duaDataFile,
    });
  }

  @Action(DepositDuaDataFileAction.GetDuaFileFail)
  getDuaFileFail(ctx: SolidifyStateContext<DepositDuaDataFileStateModel>, action: DepositDuaDataFileAction.GetDuaFileFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: undefined,
    });
  }
}
