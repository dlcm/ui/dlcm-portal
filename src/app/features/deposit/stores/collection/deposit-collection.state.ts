/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-collection.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  DepositCollectionAction,
  depositCollectionActionNameSpace,
} from "@deposit/stores/collection/deposit-collection.action";
import {DepositCollectionStatusHistoryState} from "@deposit/stores/collection/status-history/deposit-collection-status-history.state";
import {DepositDataFileStateModel} from "@deposit/stores/data-file/deposit-data-file.state";
import {DepositAction} from "@deposit/stores/deposit.action";
import {environment} from "@environments/environment";
import {Aip} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  AppRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  AssociationRemoteState,
  AssociationRemoteStateModel,
  CollectionTyped,
  defaultAssociationRemoteStateInitValue,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  QueryParameters,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export const defaultDepositCollectionValue: () => DepositCollectionStateModel = () =>
  ({
    ...defaultAssociationRemoteStateInitValue(),
    deposit_collection_statusHistory: defaultStatusHistoryInitValue(),
    numberCollections: undefined,
  });

export interface DepositCollectionStateModel extends AssociationRemoteStateModel<Aip> {
  numberCollections: number | undefined;
}

@Injectable()
@State<DepositCollectionStateModel>({
  name: StateEnum.deposit_collection,
  defaults: {
    ...defaultDepositCollectionValue(),
  },
  children: [
    DepositCollectionStatusHistoryState,
  ],
})
export class DepositCollectionState extends AssociationRemoteState<DepositCollectionStateModel, Aip> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: depositCollectionActionNameSpace,
      resourceName: ApiResourceNameEnum.AIP,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.preIngestDeposits;
  }

  @Action(DepositCollectionAction.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<DepositCollectionStateModel>, action: DepositCollectionAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });
    ctx.dispatch(new DepositCollectionAction.GetAll(action.parentId, undefined, action.keepCurrentContext));
  }

  @Action(DepositCollectionAction.Refresh)
  refresh(ctx: SolidifyStateContext<DepositCollectionStateModel>, action: DepositCollectionAction.Refresh): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new DepositCollectionAction.GetAll(action.parentId, undefined, true),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositCollectionAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositCollectionAction.GetAllFail)),
        ],
      },
      {
        action: new DepositCollectionAction.GetNumberCollections(action.parentId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositCollectionAction.GetNumberCollectionsSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositCollectionAction.GetNumberCollectionsFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new DepositCollectionAction.RefreshSuccess(action));
        } else {
          ctx.dispatch(new DepositCollectionAction.RefreshFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(DepositCollectionAction.RefreshSuccess)
  refreshSuccess(ctx: SolidifyStateContext<DepositCollectionStateModel>, action: DepositCollectionAction.RefreshSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new DepositAction.ComputeModeTab());
  }

  @Action(DepositCollectionAction.RefreshFail)
  refreshFail(ctx: SolidifyStateContext<DepositCollectionStateModel>, action: DepositCollectionAction.RefreshFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @OverrideDefaultAction()
  @Action(DepositCollectionAction.DeleteSuccess)
  deleteSuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositCollectionAction.DeleteSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new DepositCollectionAction.Refresh((action.parentAction as DepositCollectionAction.Delete).parentId));
  }

  @Action(DepositCollectionAction.GoToAip)
  goToAip(ctx: SolidifyStateContext<DepositCollectionStateModel>, action: DepositCollectionAction.GoToAip): void {
    const pathAipDetail = RoutesEnum.preservationPlanningAip + urlSeparator + 1 + urlSeparator + SharedAipRoutesEnum.aipDetail + AppRoutesEnum.separator;
    const path = [pathAipDetail, action.aip.resId];
    ctx.dispatch(new Navigate(path));
  }

  @Action(DepositCollectionAction.AddAip)
  addAip(ctx: SolidifyStateContext<DepositCollectionStateModel>, action: DepositCollectionAction.AddAip): void {
    ctx.dispatch(new DepositCollectionAction.Create(action.parentId, action.aipIds));
    ctx.dispatch(new DepositCollectionAction.Refresh(action.parentId));
  }

  @Action(DepositCollectionAction.GetNumberCollections)
  getNumberCollections(ctx: SolidifyStateContext<DepositCollectionStateModel>, action: DepositCollectionAction.GetNumberCollections): Observable<CollectionTyped<Aip>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    const queryParameters = new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo);
    return this._apiService.getCollection<Aip>(`${this._urlResource}/${action.parentId}/${this._resourceName}`, queryParameters)
      .pipe(
        tap(collection => {
          ctx.dispatch(new DepositCollectionAction.GetNumberCollectionsSuccess(action, collection._page.totalItems));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositCollectionAction.GetNumberCollectionsFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositCollectionAction.GetNumberCollectionsSuccess)
  getNumberCollectionsSuccess(ctx: SolidifyStateContext<DepositCollectionStateModel>, action: DepositCollectionAction.GetNumberCollectionsSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      numberCollections: action.numberCollections,
    });
  }

  @Action(DepositCollectionAction.GetNumberCollectionsFail)
  getNumberCollectionsFail(ctx: SolidifyStateContext<DepositCollectionStateModel>, action: DepositCollectionAction.GetNumberCollectionsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
