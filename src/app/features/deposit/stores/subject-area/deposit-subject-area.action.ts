/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-subject-area.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {SubjectArea} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  AssociationRemoteAction,
  AssociationRemoteNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.deposit_subjectAreas;

export namespace DepositSubjectAreaAction {

  @TypeDefaultAction(state)
  export class GetAll extends AssociationRemoteAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends AssociationRemoteAction.GetAllSuccess<SubjectArea> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends AssociationRemoteAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends AssociationRemoteAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends AssociationRemoteAction.GetByIdSuccess<SubjectArea> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends AssociationRemoteAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends AssociationRemoteAction.Update {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends AssociationRemoteAction.UpdateSuccess {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends AssociationRemoteAction.UpdateFail {
  }

  @TypeDefaultAction(state)
  export class Create extends AssociationRemoteAction.Create {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends AssociationRemoteAction.CreateSuccess {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends AssociationRemoteAction.CreateFail {
  }

  @TypeDefaultAction(state)
  export class Delete extends AssociationRemoteAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends AssociationRemoteAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends AssociationRemoteAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends AssociationRemoteAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends AssociationRemoteAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends AssociationRemoteAction.DeleteListFail {
  }
}

export const depositSubjectAreaNamespace: AssociationRemoteNameSpace = DepositSubjectAreaAction;
