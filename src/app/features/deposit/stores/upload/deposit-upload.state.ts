/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-upload.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {DlcmFileUploadWrapper} from "@deposit/models/dlcm-file-upload-wrapper.model";
import {DepositDataFileAction} from "@deposit/stores/data-file/deposit-data-file.action";
import {
  DepositUploadAction,
  depositUploadActionNameSpace,
} from "@deposit/stores/upload/deposit-upload.action";
import {Enums} from "@enums";
import {DlcmEnvironment} from "@environments/environment.defaults.model";
import {
  DepositDataFile,
  SystemProperty,
} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  AppSystemPropertyState,
  defaultUploadStateInitValue,
  ENVIRONMENT,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MappingObjectUtil,
  MemoizedUtil,
  NotificationService,
  StringUtil,
  UploadState,
  UploadStateModel,
} from "solidify-frontend";

export interface DepositUploadStateModel extends UploadStateModel<DepositDataFile> {
}

@Injectable()
@State<DepositUploadStateModel>({
  name: StateEnum.deposit_upload,
  defaults: {
    ...defaultUploadStateInitValue(),
  },
  children: [],
})
export class DepositUploadState extends UploadState<DepositUploadStateModel, DepositDataFile> {
  private readonly _FILE_KEY: string = "file";
  private readonly _CATEGORY_KEY: string = "category";
  private readonly _TYPE_KEY: string = "type";
  private readonly _FOLDER_KEY: string = "folder";
  private readonly _METADATA_TYPE_KEY: string = "metadataType";
  private readonly _CHECKSUM_MD5: string = "checksumMd5";
  private readonly _CHECKSUM_MD5_ORIGIN: string = "checksumMd5Origin";
  private readonly _CHECKSUM_SHA1: string = "checksumSha1";
  private readonly _CHECKSUM_SHA1_ORIGIN: string = "checksumSha1Origin";
  private readonly _CHECKSUM_SHA256: string = "checksumSha256";
  private readonly _CHECKSUM_SHA256_ORIGIN: string = "checksumSha256Origin";
  private readonly _CHECKSUM_SHA512: string = "checksumSha512";
  private readonly _CHECKSUM_SHA512_ORIGIN: string = "checksumSha512Origin";
  private readonly _CHECKSUM_CRC32: string = "checksumCrc32";
  private readonly _CHECKSUM_CRC32_ORIGIN: string = "checksumCrc32Origin";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              @Inject(ENVIRONMENT) protected readonly _environment: DlcmEnvironment,
              @Inject(LABEL_TRANSLATE) protected readonly _labelTranslateInterface: LabelTranslateInterface,
              protected readonly _translate: TranslateService,
  ) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: depositUploadActionNameSpace,
      callbackAfterAllUploadFinished: action => _store.dispatch(new DepositDataFileAction.Refresh(action.parentId)),
      fileSizeLimit: () => +MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState<SystemProperty>, state => state.current.uploadFileSizeLimit),
    }, _environment, _labelTranslateInterface, _translate);
  }

  protected get _urlResource(): string {
    return ApiEnum.preIngestDeposits;
  }

  protected override _generateUploadFormData(action: DepositUploadAction.UploadFile): FormData {
    const formData = new FormData();
    const fileUploadWrapper = action.fileUploadWrapper as DlcmFileUploadWrapper;
    if (!action.isArchive) {
      formData.append(this._FOLDER_KEY, isNullOrUndefined(fileUploadWrapper.subDirectory) ? StringUtil.stringEmpty : fileUploadWrapper.subDirectory);
    }
    if (isNotNullNorUndefinedNorWhiteString(fileUploadWrapper.metadataType)) {
      formData.append(this._METADATA_TYPE_KEY, fileUploadWrapper.metadataType);
    }
    formData.append(this._FILE_KEY, fileUploadWrapper.file, fileUploadWrapper.file.name);
    formData.append(this._CATEGORY_KEY, fileUploadWrapper.dataCategory);
    formData.append(this._TYPE_KEY, fileUploadWrapper.dataType);
    MappingObjectUtil.forEach(fileUploadWrapper.checksums, (checksum, algo: Enums.DataFile.Checksum.AlgoEnum) => {
      const algoKey = this._getChecksumKey(algo);
      if (isNullOrUndefined(algoKey)) {
        return;
      }
      formData.append(algoKey, checksum);
    });
    MappingObjectUtil.forEach(fileUploadWrapper.checksumsOrigin, (origin: Enums.DataFile.Checksum.OriginEnum, algo: Enums.DataFile.Checksum.AlgoEnum) => {
      const originKey = this._getChecksumOriginKey(algo);
      if (isNullOrUndefined(originKey)) {
        return;
      }
      formData.append(originKey, origin);
    });
    return formData;
  }

  private _getChecksumKey(checksumAlgo: Enums.DataFile.Checksum.AlgoEnum): string | undefined {
    switch (checksumAlgo) {
      case Enums.DataFile.Checksum.AlgoEnum.MD5:
        return this._CHECKSUM_MD5;
      case Enums.DataFile.Checksum.AlgoEnum.SHA1:
        return this._CHECKSUM_SHA1;
      case Enums.DataFile.Checksum.AlgoEnum.SHA256:
        return this._CHECKSUM_SHA256;
      case Enums.DataFile.Checksum.AlgoEnum.SHA512:
        return this._CHECKSUM_SHA512;
      case Enums.DataFile.Checksum.AlgoEnum.CRC32:
        return this._CHECKSUM_CRC32;
    }
    return undefined;
  }

  private _getChecksumOriginKey(checksumAlgo: Enums.DataFile.Checksum.AlgoEnum): string | undefined {
    switch (checksumAlgo) {
      case Enums.DataFile.Checksum.AlgoEnum.MD5:
        return this._CHECKSUM_MD5_ORIGIN;
      case Enums.DataFile.Checksum.AlgoEnum.SHA1:
        return this._CHECKSUM_SHA1_ORIGIN;
      case Enums.DataFile.Checksum.AlgoEnum.SHA256:
        return this._CHECKSUM_SHA256_ORIGIN;
      case Enums.DataFile.Checksum.AlgoEnum.SHA512:
        return this._CHECKSUM_SHA512_ORIGIN;
      case Enums.DataFile.Checksum.AlgoEnum.CRC32:
        return this._CHECKSUM_CRC32_ORIGIN;
    }
    return undefined;
  }
}
