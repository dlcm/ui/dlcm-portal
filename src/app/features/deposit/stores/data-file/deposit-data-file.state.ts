/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-data-file.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {
  DepositDataFileAction,
  depositDataFileActionNameSpace,
} from "@app/features/deposit/stores/data-file/deposit-data-file.action";
import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {DepositDataFileHelper} from "@deposit/helpers/deposit-data-file.helper";
import {DepositDataFileMetadataTypeState} from "@deposit/stores/data-file/metadata-type/deposit-data-file-metadata-type.state";
import {
  DepositDataFileStatusHistoryState,
  DepositDataFileStatusHistoryStateModel,
} from "@deposit/stores/data-file/status-history/deposit-data-file-status-history.state";
import {DepositAction} from "@deposit/stores/deposit.action";
import {DepositState} from "@deposit/stores/deposit.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {DepositDataFile} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  defaultResourceStateInitValue,
  DownloadService,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  QueryParametersUtil,
  Result,
  ResultActionStatusEnum,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export const defaultDepositDataFileValue: () => DepositDataFileStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    deposit_dataFile_statusHistory: defaultStatusHistoryInitValue(),
    deposit_dataFile_metadataType: {
      ...defaultResourceStateInitValue(),
    },
    currentFolder: "" + DepositDataFileHelper.ROOT,
    folders: [],
    foldersWithIntermediateFolders: [],
    intermediateFolders: [],
    listCurrentStatus: {} as MappingObject<Enums.DataFile.StatusEnum, number>,
    isLoadingCurrentStatus: 0,
  });

export interface DepositDataFileStateModel extends CompositionStateModel<DepositDataFile> {
  deposit_dataFile_statusHistory: DepositDataFileStatusHistoryStateModel;
  currentFolder: string;
  folders: string[];
  foldersWithIntermediateFolders: string[];
  intermediateFolders: string[];
  listCurrentStatus: MappingObject<Enums.DataFile.StatusEnum, number>;
  isLoadingCurrentStatus: number;
}

@Injectable()
@State<DepositDataFileStateModel>({
  name: StateEnum.deposit_dataFile,
  defaults: {
    ...defaultDepositDataFileValue(),
  },
  children: [
    DepositDataFileStatusHistoryState,
    DepositDataFileMetadataTypeState,
  ],
})
export class DepositDataFileState extends CompositionState<DepositDataFileStateModel, DepositDataFile> {
  private readonly _KEY_RELATIVE_LOCATION: keyof DepositDataFile = "relativeLocation";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _httpClient: HttpClient,
              private readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: depositDataFileActionNameSpace,
      resourceName: ApiResourceNameEnum.DATAFILE,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.preIngestDeposits;
  }

  @Action(DepositDataFileAction.Resume)
  resume(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.Resume): Observable<Result> {
    return this._apiService.post<any, Result>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFile.resId}/${ApiActionNameEnum.RESUME}`)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            this._notificationService.showInformation(LabelTranslateEnum.resourceResumed);
            ctx.dispatch(new DepositDataFileAction.GetAll(action.parentId, undefined, true));
          } else {
            this._notificationService.showError(LabelTranslateEnum.unableResumedResource);
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          this._notificationService.showError(LabelTranslateEnum.unableResumedResource);
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(DepositDataFileAction.GetAll)
  getAll<U>(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.GetAll): Observable<CollectionTyped<U>> {
    let queryParameters = action.queryParameters;
    if (isNullOrUndefined(queryParameters)) {
      queryParameters = QueryParametersUtil.clone(ctx.getState().queryParameters);
    }
    const currentFolder = ctx.getState().currentFolder;
    if (isNullOrUndefined(currentFolder)) {
      MappingObjectUtil.delete(QueryParametersUtil.getSearchItems(queryParameters), this._KEY_RELATIVE_LOCATION);

    } else {
      MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), this._KEY_RELATIVE_LOCATION, ctx.getState().currentFolder);
    }
    ctx.patchState({
      queryParameters: queryParameters,
    });
    return super.getAll<U>(ctx, action);
  }

  @Action(DepositDataFileAction.Refresh)
  refresh(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.Refresh): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new DepositDataFileAction.GetAll(action.parentId, undefined, true),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositDataFileAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositDataFileAction.GetAllFail)),
        ],
      },
      {
        action: new DepositDataFileAction.GetListFolder(action.parentId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositDataFileAction.GetListFolderSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositDataFileAction.GetListFolderFail)),
        ],
      },
      {
        action: new DepositDataFileAction.GetListCurrentStatus(action.parentId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositDataFileAction.GetListCurrentStatusSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositDataFileAction.GetListCurrentStatusFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new DepositDataFileAction.RefreshSuccess(action));
        } else {
          ctx.dispatch(new DepositDataFileAction.RefreshFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(DepositDataFileAction.RefreshSuccess)
  refreshSuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.RefreshSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new DepositAction.ComputeModeTab());
  }

  @Action(DepositDataFileAction.RefreshFail)
  refreshFail(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.RefreshFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositDataFileAction.Download)
  download(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.Download): void {
    const url = `${this._urlResource}/${action.parentId}/${ApiResourceNameEnum.DATAFILE}/${action.dataFile.resId}/${ApiActionNameEnum.DL}`;
    const fileName = action.dataFile.fileName;
    const mimetype = action.dataFile.fileFormat?.contentType;
    if (environment.fileExtensionToCompleteWithRealExtensionOnDownload.findIndex(e => fileName.endsWith("." + e)) >= 0) {
      // Allow to download dua and thumbnail with good extension
      this.subscribe(this._downloadService.downloadInMemory(url, fileName, true, mimetype));
    } else {
      this._downloadService.download(url, fileName, action.dataFile.fileSize, true, mimetype);
    }
  }

  @Action(DepositDataFileAction.ChangeCurrentFolder)
  changeCurrentFolder(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ChangeCurrentFolder): void {
    ctx.patchState({
      currentFolder: action.newCurrentFolder,
    });
    const depositId = MemoizedUtil.currentSnapshot(this._store, DepositState).resId;
    if (!isNullOrUndefined(action.newCurrentFolder)) {
      ctx.dispatch(new DepositDataFileAction.GetListFolder(depositId));
    }
    if (action.refreshNewFolderContent) {
      let queryParameters = QueryParametersUtil.clone(ctx.getState().queryParameters);
      queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
      ctx.dispatch(new DepositDataFileAction.GetAll(depositId, queryParameters));
    }
  }

  @Action(DepositDataFileAction.ChangeCurrentCategory)
  ChangeCurrentCategory(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ChangeCurrentCategory): void {
    const depositId = MemoizedUtil.currentSnapshot(this._store, DepositState).resId;
    if (action.refreshNewCategory) {
      ctx.dispatch(new DepositDataFileAction.GetAll(depositId));
    }
  }

  @Action(DepositDataFileAction.GetListFolder)
  getListFolder(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.GetListFolder): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._httpClient.get<string[]>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${ApiActionNameEnum.LIST_FOLDERS}`)
      .pipe(
        tap((listFolder: string[]) => {
          ctx.dispatch(new DepositDataFileAction.GetListFolderSuccess(action, listFolder));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDataFileAction.GetListFolderFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDataFileAction.GetListFolderSuccess)
  getListFolderSuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.GetListFolderSuccess): void {
    const folders = [...action.folder];
    const foldersWithIntermediateFolders = action.folder;
    const intermediateFolders = DepositDataFileHelper.createIntermediateFolders(foldersWithIntermediateFolders);
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      folders,
      foldersWithIntermediateFolders,
      intermediateFolders,
    });
  }

  @Action(DepositDataFileAction.GetListFolderFail)
  getListFolderFail(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.GetListFolderFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @OverrideDefaultAction()
  @Action(DepositDataFileAction.DeleteSuccess)
  deleteSuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.DeleteSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    ctx.dispatch(new DepositDataFileAction.Refresh(action.parentId));
  }

  @Action(DepositDataFileAction.Validate)
  validate(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.Validate): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._httpClient.post<Result>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFile.resId}/${ApiActionNameEnum.VALIDATE}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositDataFileAction.ValidateSuccess(action));
          } else {
            ctx.dispatch(new DepositDataFileAction.ValidateFail(action, result));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDataFileAction.ValidateFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDataFileAction.ValidateSuccess)
  validateSuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ValidateSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("notification.deposit.file.validate.success"));
    ctx.dispatch(new DepositDataFileAction.Refresh(action.parentAction.parentId));
  }

  @Action(DepositDataFileAction.ValidateFail)
  validateFail(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ValidateFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.deposit.file.validate.fail"));
  }

  @Action(DepositDataFileAction.Move)
  move(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.Move): Observable<DepositDataFile> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._httpClient.patch<DepositDataFile>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFile.resId}`, {
      relativeLocation: action.dataFile.relativeLocation,
    } as DepositDataFile)
      .pipe(
        tap(result => {
          ctx.dispatch(new DepositDataFileAction.MoveSuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDataFileAction.MoveFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDataFileAction.MoveSuccess)
  moveSuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.MoveSuccess): void {
    const relativeLocation = action.parentAction.dataFile.relativeLocation;
    ctx.dispatch(new DepositDataFileAction.ChangeCurrentFolder(relativeLocation, true));
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("notification.deposit.file.move.success"));
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositDataFileAction.MoveFail)
  moveFail(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.MoveFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.deposit.file.move.fail"));
  }

  @Action(DepositDataFileAction.ChangeDataCategory)
  changeDataCategory(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ChangeDataCategory): Observable<DepositDataFile> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._httpClient.patch<DepositDataFile>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${action.dataFile.resId}`, {
      dataCategory: action.dataFile.dataCategory,
      dataType: action.dataFile.dataType,
      metadataType: !isNullOrUndefined(action.dataFile.metadataType) ? action.dataFile.metadataType : null,
    } as DepositDataFile)
      .pipe(
        tap(result => {
          ctx.dispatch(new DepositDataFileAction.ChangeDataCategorySuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDataFileAction.ChangeDataCategoryFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDataFileAction.ChangeDataCategorySuccess)
  changeDataCategorySuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ChangeDataCategorySuccess): void {
    const dataCategory = (action.parentAction as DepositDataFileAction.ChangeDataCategory).dataFile.dataCategory;
    const dataType = (action.parentAction as DepositDataFileAction.ChangeDataCategory).dataFile.dataType;
    const depositId = MemoizedUtil.currentSnapshot(this._store, DepositState).resId;

    ctx.dispatch(new DepositDataFileAction.ChangeCurrentCategory(dataCategory, dataType, true));
    ctx.dispatch(new DepositDataFileAction.Refresh(depositId));
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("notification.deposit.file.changeDataCategory.success"));
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositDataFileAction.ChangeDataCategoryFail)
  changeDataCategoryFail(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ChangeDataCategoryFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.deposit.file.changeDataCategory.fail"));
  }

  @Action(DepositDataFileAction.DeleteAll)
  deleteAll(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.DeleteAll): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.delete<string[]>(`${this._urlResource}/${action.parentId}/${this._resourceName}`, action.listResId)
      .pipe(
        tap(collection => {
          ctx.dispatch(new DepositDataFileAction.DeleteAllSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDataFileAction.DeleteAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDataFileAction.DeleteAllSuccess)
  deleteAllSuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.DeleteAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    ctx.dispatch(new DepositDataFileAction.Refresh(action.parentAction.parentId));
  }

  @Action(DepositDataFileAction.DeleteAllFail)
  deleteAllFail(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.DeleteAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositDataFileAction.ResumeAll)
  resumeAll(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ResumeAll): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<string[]>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${ApiActionNameEnum.RESUME}`, action.listResId)
      .pipe(
        tap(collection => {
          ctx.dispatch(new DepositDataFileAction.ResumeAllSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDataFileAction.ResumeAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDataFileAction.ResumeAllSuccess)
  resumeAllSuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ResumeAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    ctx.dispatch(new DepositDataFileAction.Refresh(action.parentAction.parentId));
  }

  @Action(DepositDataFileAction.ResumeAllFail)
  resumeAllFail(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ResumeAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositDataFileAction.ValidateAll)
  validateAll(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ValidateAll): Observable<string[]> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<string[]>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${ApiActionNameEnum.VALIDATE}`, action.listResId)
      .pipe(
        tap(collection => {
          ctx.dispatch(new DepositDataFileAction.ValidateAllSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDataFileAction.ValidateAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDataFileAction.ValidateAllSuccess)
  validateAllSuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ValidateAllSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters: QueryParametersUtil.resetToFirstPage(ctx.getState().queryParameters),
    });
    ctx.dispatch(new DepositDataFileAction.Refresh(action.parentAction.parentId));
  }

  @Action(DepositDataFileAction.ValidateAllFail)
  validateAllFail(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.ValidateAllFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositDataFileAction.DeleteFolder)
  deleteFolder(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.DeleteFolder): Observable<string> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return this._apiService.post<string>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${ApiActionNameEnum.DELETE_FOLDER}?${this._KEY_RELATIVE_LOCATION}=${action.fullFolderName}`)
      .pipe(
        tap(result => {
          ctx.dispatch(new DepositDataFileAction.DeleteFolderSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDataFileAction.DeleteFolderFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDataFileAction.DeleteFolderSuccess)
  deleteFolderSuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.DeleteFolderSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      currentFolder: "" + DepositDataFileHelper.ROOT,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("notification.deposit.folder.deleteWithSuccess"));
    ctx.dispatch(new DepositDataFileAction.Refresh(action.parentAction.parentId));
  }

  @Action(DepositDataFileAction.DeleteFolderFail)
  deleteFolderFail(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.DeleteFolderFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.deposit.folder.deleteFail"));
  }

  @Action(DepositDataFileAction.GetListCurrentStatus)
  getListCurrentStatus(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.GetListCurrentStatus): Observable<MappingObject<Enums.DataFile.StatusEnum, number>> {
    ctx.patchState({
      isLoadingCurrentStatus: ctx.getState().isLoadingCurrentStatus + 1,
    });

    return this._apiService.get<MappingObject<Enums.DataFile.StatusEnum, number>>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${ApiActionNameEnum.LIST_CURRENT_STATUS}`)
      .pipe(
        tap(result => {
          ctx.dispatch(new DepositDataFileAction.GetListCurrentStatusSuccess(action, result));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositDataFileAction.GetListCurrentStatusFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositDataFileAction.GetListCurrentStatusSuccess)
  getListCurrentStatusSuccess(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.GetListCurrentStatusSuccess): void {
    ctx.patchState({
      isLoadingCurrentStatus: ctx.getState().isLoadingCurrentStatus - 1,
      listCurrentStatus: action.listCurrentStatus,
    });
  }

  @Action(DepositDataFileAction.GetListCurrentStatusFail)
  getListCurrentStatusFail(ctx: SolidifyStateContext<DepositDataFileStateModel>, action: DepositDataFileAction.GetListCurrentStatusFail): void {
    ctx.patchState({
      isLoadingCurrentStatus: ctx.getState().isLoadingCurrentStatus - 1,
    });
  }
}
