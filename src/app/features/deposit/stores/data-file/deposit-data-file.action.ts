/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-data-file.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {DepositDataFile} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  CompositionAction,
  CompositionNameSpace,
  MappingObject,
  Result,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.deposit_dataFile;

export namespace DepositDataFileAction {
  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends CompositionAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends CompositionAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends CompositionAction.GetAllSuccess<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends CompositionAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends CompositionAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends CompositionAction.GetByIdSuccess<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends CompositionAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends CompositionAction.Update<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends CompositionAction.UpdateSuccess<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends CompositionAction.UpdateFail<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class Create extends CompositionAction.Create<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends CompositionAction.CreateSuccess<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends CompositionAction.CreateFail<DepositDataFile> {
  }

  @TypeDefaultAction(state)
  export class Delete extends CompositionAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends CompositionAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends CompositionAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends CompositionAction.Clean {
  }

  export class Resume {
    static readonly type: string = `[${state}] Resume`;

    constructor(public parentId: string, public dataFile: DepositDataFile) {
    }
  }

  export class ChangeCurrentFolder {
    static readonly type: string = `[${state}] Change Current Folder`;

    constructor(public newCurrentFolder: string, public refreshNewFolderContent: boolean = false) {
    }
  }

  export class ChangeCurrentCategory {
    static readonly type: string = `[${state}] Change Current Category`;

    constructor(public newCategory: string, public newType: string, public refreshNewCategory: boolean = false) {
    }
  }

  export class Refresh extends BaseAction {
    static readonly type: string = `[${state}] Refresh`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class RefreshSuccess extends BaseSubActionSuccess<Refresh> {
    static readonly type: string = `[${state}] Refresh Success`;
  }

  export class RefreshFail extends BaseSubActionFail<Refresh> {
    static readonly type: string = `[${state}] Refresh Fail`;
  }

  export class Download {
    static readonly type: string = `[${state}] Download`;

    constructor(public parentId: string, public dataFile: DepositDataFile) {
    }
  }

  export class GetListFolder extends BaseAction {
    static readonly type: string = `[${state}] Get List Folder`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class GetListFolderSuccess extends BaseSubActionSuccess<GetListFolder> {
    static readonly type: string = `[${state}] Get List Folder Success`;

    constructor(public parentAction: GetListFolder, public folder: string[]) {
      super(parentAction);
    }
  }

  export class GetListFolderFail extends BaseSubActionFail<GetListFolder> {
    static readonly type: string = `[${state}] Get List Folder Fail`;

    constructor(public parentAction: GetListFolder) {
      super(parentAction);
    }
  }

  export class Validate extends BaseAction {
    static readonly type: string = `[${state}] Validate`;

    constructor(public parentId: string, public dataFile: DepositDataFile) {
      super();
    }
  }

  export class ValidateSuccess extends BaseSubActionSuccess<Validate> {
    static readonly type: string = `[${state}] Validate Success`;
  }

  export class ValidateFail extends BaseSubActionFail<Validate> {
    static readonly type: string = `[${state}] Validate Fail`;

    constructor(public parentAction: Validate, public result: Result = undefined) {
      super(parentAction);
    }
  }

  export class Move extends BaseAction {
    static readonly type: string = `[${state}] Move`;

    constructor(public parentId: string, public dataFile: DepositDataFile) {
      super();
    }
  }

  export class MoveSuccess extends BaseSubActionSuccess<Move> {
    static readonly type: string = `[${state}] Move Success`;

    constructor(public parentAction: Move, public result: DepositDataFile) {
      super(parentAction);
    }
  }

  export class MoveFail extends BaseSubActionFail<Move> {
    static readonly type: string = `[${state}] Move Fail`;
  }

  export class ChangeDataCategory extends BaseAction {
    static readonly type: string = `[${state}] Change Data Category`;

    constructor(public parentId: string, public dataFile: DepositDataFile) {
      super();
    }
  }

  export class ChangeDataCategorySuccess extends BaseSubActionSuccess<ChangeDataCategory> {
    static readonly type: string = `[${state}] Change Data Category Success`;

    constructor(public parentAction: ChangeDataCategory, public result: DepositDataFile) {
      super(parentAction);
    }
  }

  export class ChangeDataCategoryFail extends BaseSubActionFail<ChangeDataCategory> {
    static readonly type: string = `[${state}] Change Data Category Fail`;
  }

  export class DeleteAll extends BaseAction {
    static readonly type: string = `[${state}] Delete All`;

    constructor(public parentId: string, public listResId: string[]) {
      super();
    }
  }

  export class DeleteAllSuccess extends BaseSubActionSuccess<DeleteAll> {
    static readonly type: string = `[${state}] Delete All Success`;
  }

  export class DeleteAllFail extends BaseSubActionFail<DeleteAll> {
    static readonly type: string = `[${state}] Delete All Fail`;
  }

  export class ResumeAll extends BaseAction {
    static readonly type: string = `[${state}] Resume All`;

    constructor(public parentId: string, public listResId: string[]) {
      super();
    }
  }

  export class ResumeAllSuccess extends BaseSubActionSuccess<ResumeAll> {
    static readonly type: string = `[${state}] Resume All Success`;
  }

  export class ResumeAllFail extends BaseSubActionFail<ResumeAll> {
    static readonly type: string = `[${state}] Resume All Fail`;
  }

  export class ValidateAll extends BaseAction {
    static readonly type: string = `[${state}] Validate All`;

    constructor(public parentId: string, public listResId: string[]) {
      super();
    }
  }

  export class ValidateAllSuccess extends BaseSubActionSuccess<ValidateAll> {
    static readonly type: string = `[${state}] Validate All Success`;
  }

  export class ValidateAllFail extends BaseSubActionFail<ValidateAll> {
    static readonly type: string = `[${state}] Validate All Fail`;
  }

  export class DeleteFolder extends BaseAction {
    static readonly type: string = `[${state}] Delete`;

    constructor(public parentId: string, public fullFolderName?: string) {
      super();
    }
  }

  export class DeleteFolderSuccess extends BaseSubActionSuccess<DeleteFolder> {
    static readonly type: string = `[${state}] Delete Folder Success`;
  }

  export class DeleteFolderFail extends BaseSubActionFail<DeleteFolder> {
    static readonly type: string = `[${state}] Delete Folder Fail`;
  }

  export class GetListCurrentStatus extends BaseAction {
    static readonly type: string = `[${state}] Get List Current Status`;

    constructor(public parentId: string) {
      super();
    }
  }

  export class GetListCurrentStatusSuccess extends BaseSubActionSuccess<GetListCurrentStatus> {
    static readonly type: string = `[${state}] Get List Current Status Folder Success`;

    constructor(public parentAction: GetListCurrentStatus, public listCurrentStatus: MappingObject<Enums.DataFile.StatusEnum, number>) {
      super(parentAction);
    }
  }

  export class GetListCurrentStatusFail extends BaseSubActionFail<GetListCurrentStatus> {
    static readonly type: string = `[${state}] Get List Current Status Folder Fail`;
  }

}

export const depositDataFileActionNameSpace: CompositionNameSpace = DepositDataFileAction;
