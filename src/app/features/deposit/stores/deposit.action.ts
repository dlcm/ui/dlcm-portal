/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {DepositTabStatusEnum} from "@deposit/enums/deposit-tab-status.enum";
import {Enums} from "@enums";
import {
  Deposit,
  DepositDataFile,
  FileList,
} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
  FileUploadWrapper,
  ResourceAction,
  ResourceFileAction,
  ResourceFileNameSpace,
  Result,
  SolidifyHttpErrorResponseModel,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.deposit;

export namespace DepositAction {
  @TypeDefaultAction(state)
  export class LoadResource extends ResourceAction.LoadResource {
  }

  @TypeDefaultAction(state)
  export class LoadResourceSuccess extends ResourceAction.LoadResourceSuccess {
  }

  @TypeDefaultAction(state)
  export class LoadResourceFail extends ResourceAction.LoadResourceFail {
  }

  @TypeDefaultAction(state)
  export class ChangeQueryParameters extends ResourceAction.ChangeQueryParameters {
  }

  @TypeDefaultAction(state)
  export class GetAll extends ResourceAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends ResourceAction.GetAllSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends ResourceAction.GetAllFail<Deposit> {
  }

  @TypeDefaultAction(state)
  export class GetByListId extends ResourceAction.GetByListId {
  }

  @TypeDefaultAction(state)
  export class GetByListIdSuccess extends ResourceAction.GetByListIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetByListIdFail extends ResourceAction.GetByListIdFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends ResourceAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends ResourceAction.GetByIdSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends ResourceAction.GetByIdFail<Deposit> {
  }

  @TypeDefaultAction(state)
  export class Create extends ResourceAction.Create<Deposit> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends ResourceAction.CreateSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends ResourceAction.CreateFail<Deposit> {
  }

  @TypeDefaultAction(state)
  export class Update extends ResourceAction.Update<Deposit> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends ResourceAction.UpdateSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends ResourceAction.UpdateFail<Deposit> {
  }

  @TypeDefaultAction(state)
  export class Delete extends ResourceAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends ResourceAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends ResourceAction.DeleteFail {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends ResourceAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends ResourceAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends ResourceAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class AddInList extends ResourceAction.AddInList<Deposit> {
  }

  @TypeDefaultAction(state)
  export class AddInListById extends ResourceAction.AddInListById {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdSuccess extends ResourceAction.AddInListByIdSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class AddInListByIdFail extends ResourceAction.AddInListByIdFail<Deposit> {
  }

  @TypeDefaultAction(state)
  export class RemoveInListById extends ResourceAction.RemoveInListById {
  }

  @TypeDefaultAction(state)
  export class RemoveInListByListId extends ResourceAction.RemoveInListByListId {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkList extends ResourceAction.LoadNextChunkList {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListSuccess extends ResourceAction.LoadNextChunkListSuccess<Deposit> {
  }

  @TypeDefaultAction(state)
  export class LoadNextChunkListFail extends ResourceAction.LoadNextChunkListFail {
  }

  @TypeDefaultAction(state)
  export class Clean extends ResourceAction.Clean {
  }

  @TypeDefaultAction(state)
  export class GetFile extends ResourceFileAction.GetFile {
  }

  @TypeDefaultAction(state)
  export class GetFileSuccess extends ResourceFileAction.GetFileSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileFail extends ResourceFileAction.GetFileFail {
  }

  @TypeDefaultAction(state)
  export class UploadFile extends ResourceFileAction.UploadFile {
    constructor(public resId: string, public fileUploadWrapper: FileUploadWrapper, public metadataVersion: Enums.MetadataVersion.MetadataVersionEnum) {
      super(resId, fileUploadWrapper);
    }
  }

  @TypeDefaultAction(state)
  export class UploadFileSuccess extends ResourceFileAction.UploadFileSuccess {
  }

  @TypeDefaultAction(state)
  export class UploadFileFail extends ResourceFileAction.UploadFileFail {
  }

  @TypeDefaultAction(state)
  export class GetFileByResId extends ResourceFileAction.GetFileByResId {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdSuccess extends ResourceFileAction.GetFileByResIdSuccess {
  }

  @TypeDefaultAction(state)
  export class GetFileByResIdFail extends ResourceFileAction.GetFileByResIdFail {
  }

  @TypeDefaultAction(state)
  export class DeleteFile extends ResourceFileAction.DeleteFile {
  }

  @TypeDefaultAction(state)
  export class DeleteFileSuccess extends ResourceFileAction.DeleteFileSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFileFail extends ResourceFileAction.DeleteFileFail {
  }

  export class CheckPhoto {
    static readonly type: string = `[${state}] Check Photo`;

    constructor(public depositId: string, public metadataVersion: Enums.MetadataVersion.MetadataVersionEnum) {
    }
  }

  export class Download {
    static readonly type: string = `[${state}] Download`;

    constructor(public id: string, public fullFolderName?: string) {
    }
  }

  export class GetReadmeDataFile extends BaseAction {
    static readonly type: string = `[${state}] Get Readme Data File`;

    constructor(public id: string) {
      super();
    }
  }

  export class GetReadmeDataFileSuccess extends BaseSubActionSuccess<GetReadmeDataFile> {
    static readonly type: string = `[${state}] Get Readme Data File Success`;

    constructor(public parentAction: GetReadmeDataFile, public depositDataFile: DepositDataFile) {
      super(parentAction);
    }
  }

  export class GetReadmeDataFileFail extends BaseSubActionFail<GetReadmeDataFile> {
    static readonly type: string = `[${state}] Get Readme Data File Fail`;
  }

  export class DownloadReadme extends BaseAction {
    static readonly type: string = `[${state}] Download Readme`;

    constructor(public id: string, public depositDataFile: DepositDataFile) {
      super();
    }
  }

  export class DownloadReadmeSuccess extends BaseSubActionSuccess<DownloadReadme> {
    static readonly type: string = `[${state}] Download Readme Success`;

    constructor(public parentAction: DownloadReadme, public blob: Blob) {
      super(parentAction);
    }
  }

  export class DownloadReadmeFail extends BaseSubActionFail<DownloadReadme> {
    static readonly type: string = `[${state}] Download Readme Fail`;

    constructor(public parentAction: DownloadReadme) {
      super(parentAction);
    }
  }

  export class CheckCompliance extends BaseAction {
    static readonly type: string = `[${state}] Check compliance Level`;

    constructor(public id: string) {
      super();
    }
  }

  export class CheckComplianceSuccess extends BaseSubActionSuccess<CheckCompliance> {
    static readonly type: string = `[${state}] Check compliance Level Success`;

    constructor(public parentAction: CheckCompliance) {
      super(parentAction);
    }
  }

  export class CheckComplianceFail extends BaseSubActionFail<CheckCompliance> {
    static readonly type: string = `[${state}] Check compliance Level Fail`;
  }

  export class StartMetadataEditing extends BaseAction {
    static readonly type: string = `[${state}] Start Metadata Editing`;

    constructor(public deposit: Deposit) {
      super();
    }
  }

  export class StartMetadataEditingSuccess extends BaseSubActionSuccess<StartMetadataEditing> {
    static readonly type: string = `[${state}] Start Metadata Editing Success`;

    constructor(public parentAction: StartMetadataEditing) {
      super(parentAction);
    }
  }

  export class StartMetadataEditingFail extends BaseSubActionFail<StartMetadataEditing> {
    static readonly type: string = `[${state}] Start Metadata Editing Fail`;
  }

  export class CancelMetadataEditing extends BaseAction {
    static readonly type: string = `[${state}] Cancel Metadata Editing`;

    constructor(public deposit: Deposit) {
      super();
    }
  }

  export class CancelMetadataEditingSuccess extends BaseSubActionSuccess<CancelMetadataEditing> {
    static readonly type: string = `[${state}] Cancel Metadata Editing Success`;

    constructor(public parentAction: CancelMetadataEditing) {
      super(parentAction);
    }
  }

  export class CancelMetadataEditingFail extends BaseSubActionFail<CancelMetadataEditing> {
    static readonly type: string = `[${state}] Cancel Metadata Editing Fail`;
  }

  export class SetOrganizationalUnit {
    static readonly type: string = `[${state}] Set Organizational Unit`;

    constructor(public organizationalUnitId: string) {
    }
  }

  export class Submit extends BaseAction {
    static readonly type: string = `[${state}] Submit`;

    constructor(public deposit: Deposit) {
      super();
    }
  }

  export class SubmitSuccess extends BaseSubActionSuccess<Submit> {
    static readonly type: string = `[${state}] Submit Success`;

    constructor(public parentAction: Submit, public result: Result) {
      super(parentAction);
    }
  }

  export class SubmitFail extends BaseSubActionFail<Submit> {
    static readonly type: string = `[${state}] Submit Fail`;

    constructor(public parentAction: Submit, public error?: SolidifyHttpErrorResponseModel) {
      super(parentAction);
    }
  }

  export class Approve extends BaseAction {
    static readonly type: string = `[${state}] Approve`;

    constructor(public deposit: Deposit) {
      super();
    }
  }

  export class ApproveSuccess extends BaseSubActionSuccess<Approve> {
    static readonly type: string = `[${state}] Approve Success`;

    constructor(public parentAction: Approve, public result: Result) {
      super(parentAction);
    }
  }

  export class ApproveFail extends BaseSubActionFail<Approve> {
    static readonly type: string = `[${state}] Approve Fail`;
  }

  export class Reject extends BaseAction {
    static readonly type: string = `[${state}] Reject`;

    constructor(public deposit: Deposit, public message: string) {
      super();
    }
  }

  export class RejectSuccess extends BaseSubActionSuccess<Reject> {
    static readonly type: string = `[${state}] Reject Success`;

    constructor(public parentAction: Reject, public result: Result) {
      super(parentAction);
    }
  }

  export class RejectFail extends BaseSubActionFail<Reject> {
    static readonly type: string = `[${state}] Reject Fail`;
  }

  export class BackToEdit extends BaseAction {
    static readonly type: string = `[${state}] BackToEdit`;

    constructor(public deposit: Deposit) {
      super();
    }
  }

  export class BackToEditSuccess extends BaseSubActionSuccess<BackToEdit> {
    static readonly type: string = `[${state}] BackToEdit Success`;

    constructor(public parentAction: BackToEdit, public result: Result) {
      super(parentAction);
    }
  }

  export class BackToEditFail extends BaseSubActionFail<BackToEdit> {
    static readonly type: string = `[${state}] BackToEdit Fail`;
  }

  export class ReserveDOI extends BaseAction {
    static readonly type: string = `[${state}] Reserve DOI`;

    constructor(public deposit: Deposit) {
      super();
    }
  }

  export class ReserveDOISuccess extends BaseSubActionSuccess<ReserveDOI> {
    static readonly type: string = `[${state}] Reserve DOI Success`;

    constructor(public parentAction: ReserveDOI, public deposit: Deposit) {
      super(parentAction);
    }
  }

  export class ReserveDOIFail extends BaseSubActionFail<ReserveDOI> {
    static readonly type: string = `[${state}] Reserve DOI Fail`;
  }

  export class RefreshAllCounterStatusTab extends BaseAction {
    static readonly type: string = `[${state}] Refresh All Counter Status Tab`;

    constructor(public listTabs: DepositTabStatusEnum[], public creatorExternalUid?: string) {
      super();
    }
  }

  export class RefreshAllCounterStatusTabSuccess extends BaseSubActionSuccess<RefreshAllCounterStatusTab> {
    static readonly type: string = `[${state}] Refresh All Counter Status Tab Success`;
  }

  export class RefreshAllCounterStatusTabFail extends BaseSubActionFail<RefreshAllCounterStatusTab> {
    static readonly type: string = `[${state}] Refresh All Counter Status Tab Fail`;
  }

  export class RefreshCounterStatusTab extends BaseAction {
    static readonly type: string = `[${state}] Refresh Counter Status Tab`;

    constructor(public tabEnum: DepositTabStatusEnum, public creatorExternalUid?: string) {
      super();
    }
  }

  export class RefreshCounterStatusTabSuccess extends BaseSubActionSuccess<RefreshCounterStatusTab> {
    static readonly type: string = `[${state}] Refresh Counter Status Tab Success`;

    constructor(public parentAction: RefreshCounterStatusTab, public counter: number) {
      super(parentAction);
    }
  }

  export class RefreshCounterStatusTabFail extends BaseSubActionFail<RefreshCounterStatusTab> {
    static readonly type: string = `[${state}] Refresh Counter Status Tab Fail`;
  }

  export class ChangeEditProperty {
    static readonly type: string = `[${state}] Change Edit Property`;

    constructor(public editMode: DepositEditModeEnum) {
    }
  }

  export class GetExcludedListFiles extends BaseAction {
    static readonly type: string = `[${state}] Get Excluded List Files`;
  }

  export class GetExcludedListFilesSuccess extends BaseSubActionSuccess<GetExcludedListFiles> {
    static readonly type: string = `[${state}] Get Excluded List Files Success`;

    constructor(public parentAction: GetExcludedListFiles, public fileList: FileList) {
      super(parentAction);
    }
  }

  export class GetExcludedListFilesFail extends BaseSubActionFail<GetExcludedListFiles> {
    static readonly type: string = `[${state}] Get Excluded List Files Fail`;
  }

  export class GetIgnoredListFiles extends BaseAction {
    static readonly type: string = `[${state}] Get Ignored List Files`;
  }

  export class GetIgnoredListFilesSuccess extends BaseSubActionSuccess<GetIgnoredListFiles> {
    static readonly type: string = `[${state}] Get Ignored List Files Success`;

    constructor(public parentAction: GetIgnoredListFiles, public fileList: FileList) {
      super(parentAction);
    }
  }

  export class GetIgnoredListFilesFail extends BaseSubActionFail<GetIgnoredListFiles> {
    static readonly type: string = `[${state}] Get Ignored List Files Fail`;
  }

  export class CanCreate extends BaseAction {
    static readonly type: string = `[${state}] Can Create`;

    constructor(public canCreate: boolean) {
      super();
    }
  }

  export class ComputeModeTab extends BaseAction {
    static readonly type: string = `[${state}] Compute Mode Tab`;
  }

  export class SetActiveListTabStatus extends BaseAction {
    static readonly type: string = `[${state}] Set Active List Tab Status`;

    constructor(public activeListTabStatus: DepositTabStatusEnum) {
      super();
    }
  }

  export class PutInError extends BaseAction {
    static readonly type: string = `[${state}] Put In Error`;

    constructor(public id: string) {
      super();
    }
  }

  export class PutInErrorSuccess extends BaseSubActionSuccess<PutInError> {
    static readonly type: string = `[${state}] Put In Error Success`;
  }

  export class PutInErrorFail extends BaseSubActionFail<PutInError> {
    static readonly type: string = `[${state}] Put In Error Fail`;
  }

  export class CheckSubmissionAgreement extends BaseAction {
    static readonly type: string = `[${state}] Check Submission Agreement`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class CheckSubmissionAgreementSuccess extends BaseSubActionSuccess<CheckSubmissionAgreement> {
    static readonly type: string = `[${state}] Check Submission Agreement Success`;

    constructor(public parentAction: CheckSubmissionAgreement) {
      super(parentAction);
    }
  }

  export class CheckSubmissionAgreementFail extends BaseSubActionFail<CheckSubmissionAgreement> {
    static readonly type: string = `[${state}] Check Submission Agreement Fail`;

    constructor(public parent: CheckSubmissionAgreement, public notFound: boolean) {
      super(parent);
    }
  }

  export class ApproveSubmissionAgreement extends BaseAction {
    static readonly type: string = `[${state}] Approve Submission Agreement`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class ApproveSubmissionAgreementSuccess extends BaseSubActionSuccess<ApproveSubmissionAgreement> {
    static readonly type: string = `[${state}] Approve Submission Agreement Success`;
  }

  export class ApproveSubmissionAgreementFail extends BaseSubActionFail<ApproveSubmissionAgreement> {
    static readonly type: string = `[${state}] Approve Submission Agreement Fail`;
  }

  export class GenerateAnonymizedDepositPage extends BaseAction {
    static readonly type: string = `[${state}] Generate Anonymized Deposit Page`;

    constructor(public depositId: string) {
      super();
    }
  }

  export class GenerateAnonymizedDepositPageSuccess extends BaseSubActionSuccess<GenerateAnonymizedDepositPage> {
    static readonly type: string = `[${state}] Generate Anonymized Deposit Page Success`;

    constructor(public parent: GenerateAnonymizedDepositPage, public url: string) {
      super(parent);
    }
  }

  export class GenerateAnonymizedDepositPageFail extends BaseSubActionFail<GenerateAnonymizedDepositPage> {
    static readonly type: string = `[${state}] Generate Anonymized Deposit Page Fail`;
  }

  export class GetAnonymizedDepositPage extends BaseAction {
    static readonly type: string = `[${state}] Get Anonymized Deposit Page`;

    constructor(public anonymizedDepositPageId: string) {
      super();
    }
  }

  export class GetAnonymizedDepositPageSuccess extends BaseSubActionSuccess<GetAnonymizedDepositPage> {
    static readonly type: string = `[${state}] Get Anonymized Deposit Page Success`;

    constructor(public parent: GetAnonymizedDepositPage, public url: string) {
      super(parent);
    }
  }

  export class GetAnonymizedDepositPageFail extends BaseSubActionFail<GetAnonymizedDepositPage> {
    static readonly type: string = `[${state}] Get Anonymized Deposit Page Fail`;
  }
}

export const depositActionNameSpace: ResourceFileNameSpace = DepositAction;
