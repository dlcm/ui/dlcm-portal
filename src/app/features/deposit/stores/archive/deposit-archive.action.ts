/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-archive.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Enums} from "@enums";
import {Archive} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubAction,
  CollectionTyped,
  QueryParameters,
} from "solidify-frontend";

const state = StateEnum.deposit_archive;

export namespace DepositArchiveAction {
  export class ChangeQueryParameters {
    static readonly type: string = `[${state}] Change Query Parameters`;

    constructor(public organizationalUnitId: string, public dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum, public queryParameters?: QueryParameters, public keepCurrentContext: boolean = false) {
    }
  }

  export class Search extends BaseAction {
    static readonly type: string = `[${state}] Search`;

    constructor(public resetPagination: boolean, public organizationalUnitId: string, public dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum, public search?: string, public searchModeIsQuery?: boolean, public queryParameters?: QueryParameters) {
      super();
    }
  }

  export class SearchSuccess extends BaseSubAction<Search> {
    static readonly type: string = `[${state}] Search Success`;

    constructor(public parentAction: Search, public collection?: CollectionTyped<Archive> | null | undefined) {
      super(parentAction);
    }
  }

  export class SearchFail extends BaseSubAction<Search> {
    static readonly type: string = `[${state}] Search Fail`;
  }

  export class Clean {
    static readonly type: string = `[${state}] Clean`;
  }
}
