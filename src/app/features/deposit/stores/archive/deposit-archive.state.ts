/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-archive.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Enums} from "@enums";
import {
  Archive,
  ArchiveMetadata,
  SearchCondition,
} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {urlSeparator} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ArchiveHelper} from "@shared/helpers/archive.helper";
import {MetadataEnum} from "@shared/models/business/archive-metadata.model";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  BasicState,
  CollectionTyped,
  defaultResourceStateInitValue,
  isEmptyString,
  isNonEmptyArray,
  isNullOrUndefined,
  MappingObjectUtil,
  NotificationService,
  QueryParametersUtil,
  ResourceStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  StoreUtil,
  StringUtil,
} from "solidify-frontend";
import {DepositArchiveAction} from "./deposit-archive.action";

export const DEPOSIT_ARCHIVE_SEARCH_ALL: string = "*";

export const defaultDepositArchiveStateModel: () => DepositArchiveStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
    search: StringUtil.stringEmpty,
    searchModeIsQuery: false,
  });

export interface DepositArchiveStateModel extends ResourceStateModel<Archive> {
  search: string;
  searchModeIsQuery: boolean;
}

@Injectable()
@State<DepositArchiveStateModel>({
  name: StateEnum.deposit_archive,
  defaults: {
    ...defaultDepositArchiveStateModel(),
  },
})
export class DepositArchiveState extends BasicState<DepositArchiveStateModel> {

  constructor(private readonly _apiService: ApiService,
              private readonly _store: Store,
              private readonly _httpClient: HttpClient,
              private readonly _notificationService: NotificationService,
              private readonly _actions$: Actions) {
    super();
  }

  protected get _urlResource(): string {
    return ApiEnum.accessPublicMetadata;
  }

  private readonly _PATH_FILTER: string = "query";
  private readonly _DEFAULT_SEARCH: string = DEPOSIT_ARCHIVE_SEARCH_ALL;
  private readonly _SEARCH_GLOBAL_PARAM: string = "globalSearch";
  private readonly _LIST_DATA_SENSITIVITY: Enums.DataSensitivity.DataSensitivityEnum[] = [
    Enums.DataSensitivity.DataSensitivityEnum.UNDEFINED,
    Enums.DataSensitivity.DataSensitivityEnum.BLUE,
    Enums.DataSensitivity.DataSensitivityEnum.GREEN,
    Enums.DataSensitivity.DataSensitivityEnum.YELLOW,
    Enums.DataSensitivity.DataSensitivityEnum.ORANGE,
    Enums.DataSensitivity.DataSensitivityEnum.RED,
  ];

  @Action(DepositArchiveAction.ChangeQueryParameters)
  changeQueryParameters(ctx: SolidifyStateContext<DepositArchiveStateModel>, action: DepositArchiveAction.ChangeQueryParameters): void {
    ctx.patchState({
      queryParameters: action.queryParameters,
    });
    ctx.dispatch(new DepositArchiveAction.Search(false, action.organizationalUnitId, action.dataSensitivity, ctx.getState().search, ctx.getState().searchModeIsQuery));
  }

  private _getDataSensitivityToCompatible(dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum): Enums.DataSensitivity.DataSensitivityEnum[] {
    const indexOfDataSensitivity = this._LIST_DATA_SENSITIVITY.indexOf(dataSensitivity);
    return this._LIST_DATA_SENSITIVITY.slice(0, indexOfDataSensitivity + 1);
  }

  @Action(DepositArchiveAction.Search)
  search(ctx: SolidifyStateContext<DepositArchiveStateModel>, action: DepositArchiveAction.Search): Observable<CollectionTyped<ArchiveMetadata>> {
    const searchString = this._getSearchStringToApply(ctx, action);
    let queryParameters = StoreUtil.getQueryParametersToApply(action.queryParameters, ctx);
    if (action.resetPagination) {
      queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      search: searchString,
      searchModeIsQuery: action.searchModeIsQuery,
      queryParameters: queryParameters,
    });

    const listSearchConditions: SearchCondition[] = [];
    listSearchConditions.push({
      field: MetadataEnum.aipOrganizationalUnit,
      value: action.organizationalUnitId,
      type: Enums.SearchCondition.Type.MATCH,
      booleanClauseType: Enums.SearchCondition.BooleanClauseType.MUST,
    });

    const dataSensitivityCompatible = this._getDataSensitivityToCompatible(action.dataSensitivity);
    if (isNonEmptyArray(dataSensitivityCompatible)) {
      listSearchConditions.push({
        field: Enums.Facet.Name.DATA_TAG_FACET,
        terms: dataSensitivityCompatible,
        type: Enums.SearchCondition.Type.TERM,
        booleanClauseType: Enums.SearchCondition.BooleanClauseType.MUST,
      });
    }

    if (action.searchModeIsQuery) {
      queryParameters = QueryParametersUtil.clone(queryParameters);
      MappingObjectUtil.set(QueryParametersUtil.getSearchItems(queryParameters), this._PATH_FILTER, isEmptyString(searchString) ? DEPOSIT_ARCHIVE_SEARCH_ALL : searchString);
    } else if (!isNullOrUndefined(searchString) && !isEmptyString(searchString)) {
      listSearchConditions.push({
        field: this._SEARCH_GLOBAL_PARAM,
        value: searchString,
        type: Enums.SearchCondition.Type.MATCH,
        booleanClauseType: Enums.SearchCondition.BooleanClauseType.MUST,
      } as SearchCondition);
    }

    return this._apiService.postQueryParameters<SearchCondition[], CollectionTyped<ArchiveMetadata>>(this._urlResource + urlSeparator + ApiActionNameEnum.SEARCH, listSearchConditions, queryParameters)
      .pipe(
        StoreUtil.cancelUncompleted(action, ctx, this._actions$, [DepositArchiveAction.Search]),
        tap((collection: CollectionTyped<ArchiveMetadata>) => {
          const collectionArchive = {
            _links: collection._links,
            _page: collection._page,
            _facets: collection._facets,
          } as CollectionTyped<Archive>;
          collectionArchive._data = ArchiveHelper.adaptListArchivesMetadataInArchive(collection._data);
          ctx.dispatch(new DepositArchiveAction.SearchSuccess(action, collectionArchive));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositArchiveAction.SearchFail(action));
          throw error;
        }),
      );
  }

  @Action(DepositArchiveAction.SearchSuccess)
  searchSuccess(ctx: SolidifyStateContext<DepositArchiveStateModel>, action: DepositArchiveAction.SearchSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.collection);

    ctx.patchState({
      list: isNullOrUndefined(action.collection) ? [] : action.collection._data,
      total: isNullOrUndefined(action.collection) ? 0 : action.collection._page.totalItems,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters,
    });
  }

  @Action(DepositArchiveAction.SearchFail)
  searchFail(ctx: SolidifyStateContext<DepositArchiveStateModel>): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      list: [],
      total: 0,
    });
  }

  private _getSearchStringToApply(ctx: SolidifyStateContext<DepositArchiveStateModel>, action: DepositArchiveAction.Search): string {
    if (isNullOrUndefined(action.search)) {
      return this._getDefaultSearchStringIfNotDefined(ctx);
    }
    return action.search;
  }

  private _getDefaultSearchStringIfNotDefined(ctx: SolidifyStateContext<DepositArchiveStateModel>): string {
    if (ctx.getState().search === null || ctx.getState().search === undefined) {
      return this._DEFAULT_SEARCH;
    }
    return ctx.getState().search;
  }

  @Action(DepositArchiveAction.Clean)
  clean(ctx: SolidifyStateContext<DepositArchiveStateModel>, action: DepositArchiveAction.Clean): void {
    ctx.patchState({
      ...defaultDepositArchiveStateModel(),
    });
  }
}
