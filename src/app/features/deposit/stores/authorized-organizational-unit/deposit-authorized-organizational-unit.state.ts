/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-authorized-organizational-unit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {AppAuthorizedOrganizationalUnitAction} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {
  DepositAuthorizedOrganizationalUnitAction,
  depositAuthorizedOrganizationalUnitNameSpace,
} from "@deposit/stores/authorized-organizational-unit/deposit-authorized-organizational-unit.action";
import {environment} from "@environments/environment";
import {OrganizationalUnit} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  ApiService,
  CollectionTyped,
  defaultResourceStateInitValue,
  MappingObjectUtil,
  NotificationService,
  ObjectUtil,
  OrderEnum,
  OverrideDefaultAction,
  QueryParameters,
  ResourceState,
  ResourceStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

const KEY_ROLE_SUPERIOR_TO_VISITOR: string = "roleSuperiorToVisitor";

export interface DepositAuthorizedOrganizationalUnitStateModel extends ResourceStateModel<OrganizationalUnit> {
}

export const defaultDepositAuthorizedOrgUnitValue: () => DepositAuthorizedOrganizationalUnitStateModel = () =>
  ({
    ...defaultResourceStateInitValue(),
    queryParameters: getQueryParameter(),
  });

// WARNING : IN SOME CASE WE NEED TO FILTER TO GET ONLY OPENED ORG UNIT
const getQueryParameter: () => QueryParameters = () => {
  const queryParameters = new QueryParameters(environment.defaultEnumValuePageSizeLazyLoad, {
    field: "name" as keyof OrganizationalUnit,
    order: OrderEnum.ascending,
  });
  MappingObjectUtil.set(queryParameters.search.searchItems, "openOnly", "true");
  MappingObjectUtil.set(queryParameters.search.searchItems, KEY_ROLE_SUPERIOR_TO_VISITOR, "true");

  return queryParameters;
};

@Injectable()
@State<DepositAuthorizedOrganizationalUnitStateModel>({
  name: StateEnum.deposit_authorizedOrganizationalUnit,
  defaults: {
    ...defaultDepositAuthorizedOrgUnitValue(),
  },
})
export class DepositAuthorizedOrganizationalUnitState extends ResourceState<DepositAuthorizedOrganizationalUnitStateModel, OrganizationalUnit> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: depositAuthorizedOrganizationalUnitNameSpace,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminAuthorizedOrganizationalUnits;
  }

  @OverrideDefaultAction()
  @Action(DepositAuthorizedOrganizationalUnitAction.GetAll)
  getAll(ctx: SolidifyStateContext<DepositAuthorizedOrganizationalUnitStateModel>, action: DepositAuthorizedOrganizationalUnitAction.GetAll): Observable<CollectionTyped<OrganizationalUnit>> {
    const queryParametersToApply: QueryParameters = this.addRoleSuperiorToVisitorToQueryParameters(StoreUtil.getQueryParametersToApply(action.queryParameters, ctx), true);
    ctx.patchState({
      queryParameters: queryParametersToApply,
    });

    return super.getAll(ctx, action);
  }

  @OverrideDefaultAction()
  @Action(DepositAuthorizedOrganizationalUnitAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<DepositAuthorizedOrganizationalUnitStateModel>, action: DepositAuthorizedOrganizationalUnitAction.GetByIdSuccess): void {
    super.getByIdSuccess(ctx, action);
    ctx.dispatch(new AppAuthorizedOrganizationalUnitAction.AddOrUpdateInList(action.model));
  }

  private addRoleSuperiorToVisitorToQueryParameters(queryParameters: QueryParameters, roleSuperiorToVisitor: boolean): QueryParameters | undefined {
    queryParameters = ObjectUtil.clone(queryParameters);
    queryParameters.search = ObjectUtil.clone(queryParameters.search);

    if (roleSuperiorToVisitor) {
      MappingObjectUtil.set(queryParameters.search.searchItems, KEY_ROLE_SUPERIOR_TO_VISITOR, "true");
    }

    return queryParameters;
  }
}
