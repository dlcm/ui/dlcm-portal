/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  HttpClient,
  HttpHeaders,
  HttpStatusCode,
} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {DataFileUploadHelper} from "@app/features/deposit/helpers/data-file-upload.helper";
import {DepositDataFileAction} from "@app/features/deposit/stores/data-file/deposit-data-file.action";
import {
  defaultDepositDataFileValue,
  DepositDataFileState,
  DepositDataFileStateModel,
} from "@app/features/deposit/stores/data-file/deposit-data-file.state";
import {
  DepositAction,
  depositActionNameSpace,
} from "@app/features/deposit/stores/deposit.action";
import {
  defaultDepositPersonStateModel,
  DepositPeopleState,
  DepositPersonStateModel,
} from "@app/features/deposit/stores/people/deposit-people.state";
import {DepositPersonAction} from "@app/features/deposit/stores/people/deposit-person.action";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {LocalModelAttributeEnum} from "@app/shared/enums/model-attribute.enum";
import {
  DepositRoutesEnum,
  RoutesEnum,
  urlSeparator,
} from "@app/shared/enums/routes.enum";
import {AppAuthorizedOrganizationalUnitState} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.state";
import {DepositFormPresentational} from "@deposit/components/presentationals/deposit-form/deposit-form.presentational";
import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {DepositTabStatusEnum} from "@deposit/enums/deposit-tab-status.enum";
import {ModeDepositTabEnum} from "@deposit/enums/mode-deposit-tab.enum";
import {DepositHelper} from "@deposit/helpers/deposit.helper";
import {DlcmFileUploadWrapper} from "@deposit/models/dlcm-file-upload-wrapper.model";
import {
  defaultDepositArchiveStateModel,
  DepositArchiveState,
  DepositArchiveStateModel,
} from "@deposit/stores/archive/deposit-archive.state";
import {
  defaultDepositAuthorizedOrgUnitValue,
  DepositAuthorizedOrganizationalUnitState,
  DepositAuthorizedOrganizationalUnitStateModel,
} from "@deposit/stores/authorized-organizational-unit/deposit-authorized-organizational-unit.state";
import {
  defaultDepositCollectionValue,
  DepositCollectionState,
  DepositCollectionStateModel,
} from "@deposit/stores/collection/deposit-collection.state";
import {DepositDuaDataFileAction} from "@deposit/stores/dua-data-file/deposit-dua-data-file.action";
import {
  defaultDepositDuaDataFileValue,
  DepositDuaDataFileState,
  DepositDuaDataFileStateModel,
} from "@deposit/stores/dua-data-file/deposit-dua-data-file.state";
import {DepositOrganizationalUnitAction} from "@deposit/stores/organizational-unit/deposit-organizational-unit.action";
import {
  defaultDepositOrganizationalUnitValue,
  DepositOrganizationalUnitState,
  DepositOrganizationalUnitStateModel,
} from "@deposit/stores/organizational-unit/deposit-organizational-unit.state";
import {DepositOrganizationalUnitSubjectAreaAction} from "@deposit/stores/organizational-unit/subject-area/deposit-organizational-unit-subject-area.action";
import {
  DepositSipState,
  DepositSipStateModel,
} from "@deposit/stores/sip/deposit-sip.state";
import {
  DepositStatusHistoryState,
  DepositStatusHistoryStateModel,
} from "@deposit/stores/status-history/deposit-status-history.state";
import {DepositSubjectAreaAction} from "@deposit/stores/subject-area/deposit-subject-area.action";
import {
  DepositSubjectAreaState,
  DepositSubjectAreaStateModel,
} from "@deposit/stores/subject-area/deposit-subject-area.state";
import {DepositUploadAction} from "@deposit/stores/upload/deposit-upload.action";
import {
  DepositUploadState,
  DepositUploadStateModel,
} from "@deposit/stores/upload/deposit-upload.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  DataFile,
  Deposit,
  DepositDataFile,
  FileList,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {LocalStateModel} from "@shared/models/local-state.model";
import {SecurityService} from "@shared/services/security.service";
import {SharedArchiveTypeAction} from "@shared/stores/archive-type/shared-archive-type.action";
import {SharedLanguageAction} from "@shared/stores/language/shared-language.action";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {SharedSubjectAreaAction} from "@shared/stores/subject-area/shared-subject-area.action";
import {
  Observable,
  of,
} from "rxjs";
import {
  catchError,
  map,
  switchMap,
  take,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  BaseResourceLogo,
  CollectionTyped,
  defaultAssociationRemoteStateInitValue,
  defaultResourceFileStateInitValue,
  defaultResourceStateInitValue,
  defaultUploadStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  ErrorHelper,
  isEmptyArray,
  isInstanceOf,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OrderEnum,
  OverrideDefaultAction,
  PollingHelper,
  QueryParameters,
  QueryParametersUtil,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  Result,
  ResultActionStatusEnum,
  SOLIDIFY_CONSTANTS,
  SolidifyFile,
  SolidifyFileUploadStatus,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface DepositStateModel extends ResourceFileStateModel<Deposit> {
  deposit_authorizedOrganizationalUnit: DepositAuthorizedOrganizationalUnitStateModel;
  deposit_dataFile: DepositDataFileStateModel;
  deposit_duaDataFile: DepositDuaDataFileStateModel;
  deposit_person: DepositPersonStateModel;
  deposit_upload: DepositUploadStateModel;
  deposit_collection: DepositCollectionStateModel;
  deposit_archive: DepositArchiveStateModel;
  deposit_sip: DepositSipStateModel;
  deposit_organizationalUnit: DepositOrganizationalUnitStateModel;
  deposit_subjectArea: DepositSubjectAreaStateModel;
  isLoadingDataFile: boolean;
  deposit_statusHistory: DepositStatusHistoryStateModel;
  tabCounters: MappingObject<DepositTabStatusEnum, number>;
  excludedFileList: FileList | undefined;
  ignoredFileList: FileList | undefined;
  canEdit: DepositEditModeEnum;
  canCreate: boolean;
  formPresentational: DepositFormPresentational | undefined;
  depositModeTabEnum: ModeDepositTabEnum;
  dataFileLogo: DataFile | undefined;
  activeListTabStatus: DepositTabStatusEnum;
  readmeDataFile: DepositDataFile | undefined;
  anonymousReviewUrl: string | undefined;
}

@Injectable()
@State<DepositStateModel>({
  name: StateEnum.deposit,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    deposit_authorizedOrganizationalUnit: defaultDepositAuthorizedOrgUnitValue(),
    queryParameters: new QueryParameters(environment.defaultPageSize),
    deposit_dataFile: defaultDepositDataFileValue(),
    deposit_duaDataFile: defaultDepositDuaDataFileValue(),
    deposit_person: {...defaultDepositPersonStateModel()},
    deposit_upload: {...defaultUploadStateInitValue()},
    deposit_collection: defaultDepositCollectionValue(),
    deposit_archive: {...defaultDepositArchiveStateModel()},
    deposit_sip: {...defaultResourceStateInitValue()},
    deposit_organizationalUnit: defaultDepositOrganizationalUnitValue(),
    deposit_subjectArea: {...defaultAssociationRemoteStateInitValue()},
    isLoadingDataFile: false,
    deposit_statusHistory: {...defaultStatusHistoryInitValue()},
    tabCounters: {} as MappingObject<DepositTabStatusEnum, number>,
    excludedFileList: undefined,
    ignoredFileList: undefined,
    canEdit: DepositEditModeEnum.none,
    canCreate: false,
    formPresentational: undefined,
    depositModeTabEnum: ModeDepositTabEnum.UNDEFINED,
    dataFileLogo: undefined,
    isLoadingFile: false,
    activeListTabStatus: DepositTabStatusEnum.inProgress,
    readmeDataFile: undefined,
    anonymousReviewUrl: undefined,
  },
  children: [
    DepositUploadState,
    DepositDataFileState,
    DepositDuaDataFileState,
    DepositPeopleState,
    DepositStatusHistoryState,
    DepositOrganizationalUnitState,
    DepositCollectionState,
    DepositArchiveState,
    DepositSipState,
    DepositAuthorizedOrganizationalUnitState,
    DepositSubjectAreaState,
  ],
})
export class DepositState extends ResourceFileState<DepositStateModel, Deposit> {
  private readonly _ZIP_EXTENSION: string = ".zip";
  private readonly _DEPOSIT_FILE_DOWNLOAD_PREFIX: string = "deposit_";
  private readonly _FOLDER_QUERY_PARAM: string = "folder";
  private readonly _DEPOSIT_REJECT_REASON: string = "reason";
  private readonly _KEY_DATA_CATEGORY: string = "dataCategory";
  private readonly _KEY_DATA_TYPE: string = "dataType";

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _securityService: SecurityService,
              protected readonly _downloadService: DownloadService,
              protected readonly _httpClient: HttpClient) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: depositActionNameSpace,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.deposit,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("deposit.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("deposit.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("deposit.notification.resource.update"),
      keepCurrentStateAfterUpdate: true,
      keepCurrentStateBeforeCreate: true,
      updateSubResourceDispatchMethod: DispatchMethodEnum.SEQUENCIAL,
    }, _downloadService, ResourceFileStateModeEnum.custom, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.preIngestDeposits;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: DepositStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static currentOrgUnitName(state: DepositStateModel): string | undefined {
    if (isNullOrUndefined(state.deposit_organizationalUnit) || isNullOrUndefined(state.deposit_organizationalUnit.current)) {
      return undefined;
    }
    return state.deposit_organizationalUnit.current.name;
  }

  @Selector()
  static currentTitle(state: DepositStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.title;
  }

  @Selector()
  static isLoadingWithDependency(state: DepositStateModel): boolean {
    return this.isLoading(state)
      || StoreUtil.isLoadingState(state.deposit_person)
      || StoreUtil.isLoadingState(state.deposit_subjectArea)
      || StoreUtil.isLoadingState(state.deposit_organizationalUnit)
      || StoreUtil.isLoadingState(state.deposit_organizationalUnit.deposit_organizationalUnit_subjectArea);
  }

  @Selector()
  static isReadyToBeDisplayed(state: DepositStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current)
      && !isNullOrUndefined(state.deposit_person.selected)
      && !isNullOrUndefined((state.deposit_subjectArea.selected));
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: DepositStateModel): boolean {
    return !isNullOrUndefined(state.deposit_organizationalUnit) && !isNullOrUndefined(state.deposit_organizationalUnit.current)
      && state.deposit_organizationalUnit.deposit_organizationalUnit_additionalFieldsForm.loaded
      && !isNullOrUndefined(state.deposit_organizationalUnit.deposit_organizationalUnit_subjectArea.selected);
  }

  @Action(DepositAction.LoadResource)
  loadResource(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.LoadResource): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new SharedLanguageAction.GetAll(null, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedLanguageAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedLanguageAction.GetAllFail)),
        ],
      },
      {
        action: new SharedArchiveTypeAction.GetAll(null, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedArchiveTypeAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedArchiveTypeAction.GetAllFail)),
        ],
      },
      {
        action: new SharedSubjectAreaAction.GetSource(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedSubjectAreaAction.GetSourceSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedSubjectAreaAction.GetSourceFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new DepositAction.LoadResourceSuccess(action));
        } else {
          ctx.dispatch(new DepositAction.LoadResourceFail(action));
        }
        return result.success;
      }),
    );
  }

  @Action(DepositAction.SetOrganizationalUnit)
  setOrganizationalUnit(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SetOrganizationalUnit): void {
    const listAuthorizedOrgUnit = MemoizedUtil.listSnapshot(this._store, AppAuthorizedOrganizationalUnitState);
    if (isNullOrUndefined(listAuthorizedOrgUnit) || isEmptyArray(listAuthorizedOrgUnit)) {
      return;
    }
    const canCreate = this._securityService.canCreateDepositOnOrgUnit(action.organizationalUnitId);
    ctx.dispatch(new DepositAction.CanCreate(canCreate));
    const orgUnit = listAuthorizedOrgUnit.find(o => o.resId === action.organizationalUnitId);
    ctx.patchState({
      tabCounters: {} as MappingObject<DepositTabStatusEnum, number>,
    });
    ctx.dispatch([
      new DepositOrganizationalUnitAction.GetByIdSuccess(action as any, orgUnit),
      new DepositOrganizationalUnitSubjectAreaAction.GetAll(orgUnit.resId),
    ]);
  }

  @OverrideDefaultAction()
  @Action(DepositAction.GetAllSuccess)
  getAllSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetAllSuccess): void {
    super.getAllSuccess(ctx, action);
    const searchItem = QueryParametersUtil.getSearchItems(ctx.getState().queryParameters);
    if (isNullOrUndefined(searchItem)) {
      return;
    }
    const tab = ctx.getState().activeListTabStatus;
    if (isNotNullNorUndefined(tab)) {
      const actionParent = new DepositAction.RefreshCounterStatusTab(tab, MappingObjectUtil.get(searchItem, DepositHelper.KEY_CREATION_WHO));
      ctx.dispatch(new DepositAction.RefreshCounterStatusTabSuccess(actionParent, action.list._page.totalItems));
    }
  }

  @OverrideDefaultAction()
  @Action(DepositAction.CreateSuccess)
  createSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CreateSuccess): void {
    super.createSuccess(ctx, action);
    ctx.dispatch([
      new Navigate([RoutesEnum.deposit, action.model.organizationalUnitId, DepositRoutesEnum.detail, action.model.resId, DepositRoutesEnum.data]),
      new DepositAction.SetOrganizationalUnit(action.model.organizationalUnitId),
      new DepositDuaDataFileAction.GetDuaFile(action.model.resId),
    ]);
  }

  @OverrideDefaultAction()
  @Action(DepositAction.UpdateSuccess)
  updateSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UpdateSuccess): void {
    super.updateSuccess(ctx, action);

    const depositId = action.model.resId;
    ctx.dispatch([
      new DepositPersonAction.GetAll(depositId),
      new DepositSubjectAreaAction.GetAll(depositId),
      new Navigate([RoutesEnum.deposit, action.model.organizationalUnitId, DepositRoutesEnum.detail, action.model.resId, DepositRoutesEnum.metadata]),
      new DepositAction.SetOrganizationalUnit(action.model.organizationalUnitId),
      new DepositDuaDataFileAction.GetDuaFile(depositId),
    ]);
  }

  protected override _getListActionsUpdateSubResource(model: Deposit, action: DepositAction.Create | DepositAction.Update, ctx: SolidifyStateContext<DepositStateModel>): ActionSubActionCompletionsWrapper[] {
    const logo: SolidifyFile = isNotNullNorUndefinedNorWhiteString(ctx.getState().file) ? {} : undefined;
    const actions = ResourceFileState.getActionUpdateFile(model.resId, (action.modelFormControlEvent.model as BaseResourceLogo).newPendingFile, logo, depositActionNameSpace, this._actions$);
    const actionUpload = actions.find(a => isInstanceOf(a.action, depositActionNameSpace.UploadFile));
    if (isNotNullNorUndefined(actionUpload)) {
      // Allow to provide metadata version
      actionUpload.action = new DepositAction.UploadFile(model.resId, {
        file: (action.modelFormControlEvent.model as BaseResourceLogo).newPendingFile as File,
      }, model.metadataVersion);
    }

    const depositId = model.resId;
    const selectedPerson = ctx.getState().deposit_person.selected;
    const oldAuthors: string[] = isNullOrUndefined(selectedPerson) ? [] : selectedPerson.map(p => p.resId);
    const newAuthors: string[] = action.modelFormControlEvent.model[LocalModelAttributeEnum.authors];

    const selectedSubjectAreas = ctx.getState().deposit_subjectArea.selected;
    let oldSubjectAreasIds: string[] = isNullOrUndefined(selectedSubjectAreas) ? [] : selectedSubjectAreas.map(s => s.resId);

    // if there is no subjectareas selected and it is the creation of a deposit, we should take the subject areas from org unit
    if (isEmptyArray(oldSubjectAreasIds) && action instanceof DepositAction.Create) {
      oldSubjectAreasIds = ctx.getState().deposit_organizationalUnit.deposit_organizationalUnit_subjectArea.selected.map(p => p.resId);
    }
    const newSubjectAreaIds: string[] = action.modelFormControlEvent.model.subjectAreas.map(p => p.resId);

    // Need to delete before submit new authors to update order
    actions.push(...[
      {
        action: new DepositPersonAction.DeleteList(depositId, oldAuthors),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositPersonAction.DeleteListSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositPersonAction.DeleteListFail)),
        ],
      },
      {
        action: new DepositPersonAction.Create(depositId, newAuthors),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositPersonAction.CreateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositPersonAction.CreateFail)),
        ],
      },
      {
        action: new DepositSubjectAreaAction.DeleteList(depositId, oldSubjectAreasIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositSubjectAreaAction.DeleteListSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositSubjectAreaAction.DeleteListFail)),
        ],
      },
      {
        action: new DepositSubjectAreaAction.Create(depositId, newSubjectAreaIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositSubjectAreaAction.CreateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositSubjectAreaAction.CreateFail)),
        ],
      },
    ]);

    const newDeposit = action.modelFormControlEvent.model;
    const newDuaFile = newDeposit.duaFileChange;
    const oldDuaFile = MemoizedUtil.currentSnapshot(this._store, DepositDuaDataFileState) as DepositDataFile;
    const isTypeCompatibleWithDuaFile = [Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA, Enums.Deposit.DataUsePolicyEnum.SIGNED_DUA].includes(newDeposit.dataUsePolicy);
    const isDuaFileToUpload = isTypeCompatibleWithDuaFile && isNotNullNorUndefined(newDuaFile) && newDuaFile !== SolidifyFileUploadStatus.DELETED && newDuaFile !== SolidifyFileUploadStatus.UNCHANGED;
    const isDuaFileToDelete = newDuaFile === SolidifyFileUploadStatus.DELETED
      || (isNotNullNorUndefined(newDuaFile) && newDuaFile !== SolidifyFileUploadStatus.UNCHANGED)
      || (!isTypeCompatibleWithDuaFile && isNotNullNorUndefined(oldDuaFile));

    if (isDuaFileToDelete) {
      if (isNotNullNorUndefined(oldDuaFile)) {
        actions.push({
          action: new DepositDataFileAction.Delete(depositId, oldDuaFile.resId),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(DepositDataFileAction.DeleteSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(DepositDataFileAction.DeleteFail)),
          ],
        });
      }
    }

    if (isDuaFileToUpload) {
      const fileUpload = {
        file: newDuaFile,
        dataCategory: Enums.DataFile.DataCategoryAndType.DataCategoryEnum.INTERNAL,
        dataType: Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_DATA_USE_AGREEMENT,
      } as DlcmFileUploadWrapper;
      actions.push({
        action: new DepositUploadAction.UploadFile(depositId, fileUpload),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositUploadAction.UploadFileSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositUploadAction.UploadFileFail)),
        ],
      });
    }

    return actions;
  }

  @Action(DepositAction.Submit)
  submit(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Submit): Observable<Result> {
    ctx.dispatch(new Navigate([RoutesEnum.deposit, action.deposit.organizationalUnitId, DepositRoutesEnum.detail, action.deposit.resId]));

    let submissionWithApproval = false;
    let submissionPolicy = action.deposit.submissionPolicy;
    if (isNullOrUndefined(submissionPolicy)) {
      submissionPolicy = action.deposit.organizationalUnit.defaultSubmissionPolicy;
    }
    if (!isNullOrUndefined(submissionPolicy)) {
      submissionWithApproval = submissionPolicy.submissionApproval;
    }

    let suffix = ApiActionNameEnum.APPROVE;
    if (isTrue(submissionWithApproval)) {
      suffix = ApiActionNameEnum.SUBMIT_FOR_APPROVAL;
    }
    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.deposit.resId + urlSeparator + suffix)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.SubmitSuccess(action, result));
          } else {
            ctx.dispatch(new DepositAction.SubmitFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.SubmitFail(action, error));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.SubmitSuccess)
  submitSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SubmitSuccess): void {
    this._redirectToDepositDetail(ctx, action.result.resId);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.notification.submit.success"));
  }

  @Action(DepositAction.SubmitFail)
  submitFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SubmitFail): void {
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.notification.submit.fail"), {errors: ErrorHelper.extractUserErrors(action.error)});
  }

  @Action(DepositAction.Approve)
  approve(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Approve): Observable<Result> {
    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.deposit.resId + urlSeparator + ApiActionNameEnum.APPROVE)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.ApproveSuccess(action, result));
          } else {
            ctx.dispatch(new DepositAction.ApproveFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.ApproveFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.ApproveSuccess)
  approveSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ApproveSuccess): void {
    this._redirectToDepositDetail(ctx, action.result.resId);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.notification.approve.success"));
  }

  @Action(DepositAction.ApproveFail)
  approveFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ApproveFail): void {
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.notification.approve.fail"));
  }

  @Action(DepositAction.Reject)
  reject(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Reject): Observable<Result> {
    return this._apiService.post<any, Result>(`${this._urlResource + urlSeparator + action.deposit.resId + urlSeparator + ApiActionNameEnum.REJECT}?${this._DEPOSIT_REJECT_REASON}=${action.message}`)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.RejectSuccess(action, result));
          } else {
            ctx.dispatch(new DepositAction.RejectFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.RejectFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.RejectSuccess)
  rejectSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RejectSuccess): void {
    this._redirectToDepositDetail(ctx, action.result.resId);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.notification.reject.success"));
  }

  @Action(DepositAction.RejectFail)
  rejectFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RejectFail): void {
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.notification.reject.fail"));
  }

  @Action(DepositAction.BackToEdit)
  backToEdit(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.BackToEdit): Observable<Result> {
    return this._apiService.post<Result>(this._urlResource + urlSeparator + action.deposit.resId + urlSeparator + ApiActionNameEnum.ENABLE_REVISION)
      .pipe(
        tap(result => ctx.dispatch(new DepositAction.BackToEditSuccess(action, result))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.BackToEditFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.BackToEditSuccess)
  backToEditSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.BackToEditSuccess): void {
    this._redirectToDepositDetail(ctx, action.result.resId);
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.notification.backToEdit.success"));
  }

  @Action(DepositAction.BackToEditFail)
  backToEditFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.BackToEditFail): void {
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.notification.backToEdit.fail"));
  }

  @Action(DepositAction.ReserveDOI)
  reserveDOI(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ReserveDOI): Observable<Deposit> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post(this._urlResource + urlSeparator + action.deposit.resId + urlSeparator + ApiActionNameEnum.RESERVE_DOI)
      .pipe(
        tap(deposit => ctx.dispatch(new DepositAction.ReserveDOISuccess(action, deposit))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.ReserveDOIFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.ReserveDOISuccess)
  reserveDOISuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ReserveDOISuccess): void {
    // We want to explicitly set to undefined before set the new value
    ctx.patchState({
      current: undefined,
    });
    ctx.patchState({
      current: action.deposit,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.notification.reserveDOI.success"));
  }

  @Action(DepositAction.ReserveDOIFail)
  reserveDOIFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ReserveDOIFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.notification.reserveDOI.fail"));
  }

  @Action(DepositAction.RefreshAllCounterStatusTab)
  refreshAllCounterStatusTab(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RefreshAllCounterStatusTab): Observable<boolean> {
    const listActionsWrappper = [];
    action.listTabs.forEach(tabEnum => {
      listActionsWrappper.push({
        action: new DepositAction.RefreshCounterStatusTab(tabEnum, action.creatorExternalUid),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.RefreshCounterStatusTabSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.RefreshCounterStatusTabFail)),
        ],
      });
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, listActionsWrappper)
      .pipe(
        map(result => {
          if (result.success) {
            ctx.dispatch(new DepositAction.RefreshAllCounterStatusTabSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.RefreshAllCounterStatusTabFail(action));
          }
          return result.success;
        }),
      );
  }

  @Action(DepositAction.RefreshCounterStatusTab)
  refreshCounterStatusTab(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RefreshCounterStatusTab): Observable<number> {
    const queryParameters = new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo);
    const searchItems = QueryParametersUtil.getSearchItems(queryParameters);
    if (isNotNullNorUndefined(ctx.getState().deposit_organizationalUnit?.current)) {
      MappingObjectUtil.set(searchItems, DepositHelper.KEY_ORGANIZATIONAL_UNIT as string, ctx.getState().deposit_organizationalUnit.current.resId);
    }
    const tab = DepositHelper.tabsStatus.find(t => t.tabEnum === action.tabEnum);
    if (isNonEmptyArray(tab?.depositStatusEnum)) {
      if (tab.depositStatusEnum.length === 1) {
        MappingObjectUtil.set(searchItems, DepositHelper.KEY_STATUS as string, tab.depositStatusEnum[0]);
      } else {
        MappingObjectUtil.set(searchItems, DepositHelper.KEY_STATUS_LIST, tab.depositStatusEnum.join(SOLIDIFY_CONSTANTS.COMMA));
      }
    }
    if (isNotNullNorUndefinedNorWhiteString(action.creatorExternalUid)) {
      MappingObjectUtil.set(searchItems, DepositHelper.KEY_CREATION_WHO, action.creatorExternalUid);
    }
    return this._apiService.getCollection<Deposit>(this._urlResource, queryParameters)
      .pipe(
        map((collection: CollectionTyped<Deposit>) => {
          const totalItem = collection._page.totalItems;
          ctx.dispatch(new DepositAction.RefreshCounterStatusTabSuccess(action, totalItem));
          return totalItem;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.RefreshCounterStatusTabFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.RefreshCounterStatusTabSuccess)
  refreshCounterStatusTabSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RefreshCounterStatusTabSuccess): void {
    const tabCounters = MappingObjectUtil.copy(ctx.getState().tabCounters);
    MappingObjectUtil.set(tabCounters, action.parentAction.tabEnum, action.counter);

    ctx.patchState({
      tabCounters: tabCounters,
    });
  }

  @Action(DepositAction.RefreshCounterStatusTabFail)
  refreshCounterStatusTabFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.RefreshCounterStatusTabFail): void {
  }

  @OverrideDefaultAction()
  @Action(DepositAction.Clean)
  clean(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Clean): void {
    const canCreate = ctx.getState().canCreate;
    const depositOrganizationalUnitStateModel = ctx.getState().deposit_organizationalUnit;
    const ignoredFileList = ctx.getState().ignoredFileList;
    const excludedFileList = ctx.getState().excludedFileList;
    const isLoadingCounter = ctx.getState().isLoadingCounter;
    super.clean(ctx, action);
    ctx.patchState({
      canCreate,
      deposit_organizationalUnit: depositOrganizationalUnitStateModel,
      ignoredFileList,
      excludedFileList,
      isLoadingCounter,
    });
  }

  private _redirectToDepositDetail(ctx: SolidifyStateContext<DepositStateModel>, depositId: string): void {
    ctx.dispatch(new Navigate([RoutesEnum.deposit, ctx.getState().deposit_organizationalUnit.current.resId, DepositRoutesEnum.detail, depositId]));
    ctx.dispatch(new DepositAction.GetById(depositId));
  }

  @Action(DepositAction.ChangeEditProperty)
  changeEditProperty(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ChangeEditProperty): void {
    ctx.patchState({
      canEdit: action.editMode,
    });
  }

  @Action(DepositAction.GetExcludedListFiles)
  getExcludedListFiles(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetExcludedListFiles): Observable<FileList> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.get<FileList>(this._urlResource + urlSeparator + ApiActionNameEnum.LIST_EXCLUDE_FILES)
      .pipe(
        tap(fileList => ctx.dispatch(new DepositAction.GetExcludedListFilesSuccess(action, fileList))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.GetExcludedListFilesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.GetExcludedListFilesSuccess)
  getExcludedListFilesSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetExcludedListFilesSuccess): void {
    ctx.patchState({
      excludedFileList: action.fileList,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.GetExcludedListFilesFail)
  getExcludedListFilesFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetExcludedListFilesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.GetIgnoredListFiles)
  getIgnoredListFiles(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetIgnoredListFiles): Observable<FileList> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.get<FileList>(this._urlResource + urlSeparator + ApiActionNameEnum.LIST_IGNORE_FILES)
      .pipe(
        tap(fileList => ctx.dispatch(new DepositAction.GetIgnoredListFilesSuccess(action, fileList))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.GetIgnoredListFilesFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.GetIgnoredListFilesSuccess)
  getIgnoredListFilesSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetIgnoredListFilesSuccess): void {
    ctx.patchState({
      ignoredFileList: action.fileList,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.GetIgnoredListFilesFail)
  getIgnoredListFilesFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetIgnoredListFilesFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.CanCreate)
  canCreate(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CanCreate): void {
    ctx.patchState({
      canCreate: action.canCreate,
    });
  }

  @Action(DepositAction.Download)
  download(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.Download): void {
    let fileName = this._DEPOSIT_FILE_DOWNLOAD_PREFIX + action.id;
    let url = `${this._urlResource}/${action.id}/${ApiActionNameEnum.DL}`;
    if (isNotNullNorUndefined(action.fullFolderName)) {
      url = url + `?${this._FOLDER_QUERY_PARAM}=${action.fullFolderName}`;
      fileName = fileName + action.fullFolderName;
    }
    this._downloadService.download(url, fileName + this._ZIP_EXTENSION);
  }

  @Action(DepositAction.CheckCompliance)
  checkCompliance(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckCompliance): Observable<Result> {
    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.id + urlSeparator + ApiActionNameEnum.CHECK_COMPLIANCE)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.CheckComplianceSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.CheckComplianceFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.CheckComplianceFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.CheckComplianceSuccess)
  checkComplianceSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckComplianceSuccess): void {
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.notification.checkCompliance.success"));
  }

  @Action(DepositAction.CheckComplianceFail)
  checkComplianceFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckComplianceFail): void {
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.notification.checkCompliance.fail"));
  }

  @Action(DepositAction.StartMetadataEditing)
  startMetadataEditing(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.StartMetadataEditing): Observable<Result> {
    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.deposit.resId + urlSeparator + ApiActionNameEnum.START_METADATA_EDITING)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.StartMetadataEditingSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.StartMetadataEditingFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.StartMetadataEditingFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.StartMetadataEditingSuccess)
  startMetadataEditingSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.StartMetadataEditingSuccess): void {
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.notification.startMetadataEditing.success"));
    this._redirectToDepositDetail(ctx, action.parentAction.deposit.resId);
  }

  @Action(DepositAction.StartMetadataEditingFail)
  startMetadataEditingFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.StartMetadataEditingFail): void {
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.notification.startMetadataEditing.fail"));
  }

  @Action(DepositAction.CancelMetadataEditing)
  cancelMetadataEditing(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CancelMetadataEditing): Observable<Result> {
    return this._apiService.post<any, Result>(this._urlResource + urlSeparator + action.deposit.resId + urlSeparator + ApiActionNameEnum.CANCEL_METADATA_EDITING)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.CancelMetadataEditingSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.CancelMetadataEditingFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.CancelMetadataEditingFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.CancelMetadataEditingSuccess)
  cancelMetadataEditingSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CancelMetadataEditingSuccess): void {
    this._notificationService.showSuccess(MARK_AS_TRANSLATABLE("deposit.notification.cancelMetadataEditing.success"));
    ctx.dispatch(new DepositPersonAction.GetAll(action.parentAction.deposit.resId));
    ctx.dispatch(new DepositSubjectAreaAction.GetAll(action.parentAction.deposit.resId));
    this._redirectToDepositDetail(ctx, action.parentAction.deposit.resId);
  }

  @Action(DepositAction.CancelMetadataEditingFail)
  cancelMetadataEditingFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CancelMetadataEditingFail): void {
    this._notificationService.showError(MARK_AS_TRANSLATABLE("deposit.notification.cancelMetadataEditing.fail"));
  }

  @Action(DepositAction.ComputeModeTab)
  computeModeTab(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ComputeModeTab): void {
    let mode: ModeDepositTabEnum;

    const url = this._store.selectSnapshot((s: LocalStateModel) => s.router.state.url);
    const currentDisplay = url.substring(url.lastIndexOf(urlSeparator) + 1, url.length);
    const baseUrl = url.substring(0, url.lastIndexOf(urlSeparator));
    const numberCollection = ctx.getState()?.deposit_collection?.numberCollections;
    const numberFiles = DataFileUploadHelper.numberFiles(this._store);

    let redirectionRouteSuffix = undefined;
    if (numberCollection > 0) {
      mode = ModeDepositTabEnum.COLLECTION;
      if (currentDisplay === DepositRoutesEnum.data || currentDisplay === DepositRoutesEnum.files) {
        redirectionRouteSuffix = DepositRoutesEnum.collections;
      }
    } else if (numberFiles > 0) {
      mode = ModeDepositTabEnum.FILE;
      if (currentDisplay === DepositRoutesEnum.data || currentDisplay === DepositRoutesEnum.collections) {
        redirectionRouteSuffix = DepositRoutesEnum.files;
      }
    } else {
      mode = ModeDepositTabEnum.FILE_OR_COLLECTION;
      if (currentDisplay === DepositRoutesEnum.files || currentDisplay === DepositRoutesEnum.collections) {
        redirectionRouteSuffix = DepositRoutesEnum.data;
      }
    }
    ctx.patchState({
      depositModeTabEnum: mode,
    });
    if (isNotNullNorUndefined(redirectionRouteSuffix)) {
      ctx.dispatch(new Navigate([baseUrl, redirectionRouteSuffix]));
    }
  }

  @Action(DepositAction.SetActiveListTabStatus)
  setActiveListTabStatus(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.SetActiveListTabStatus): void {
    ctx.patchState({
      activeListTabStatus: action.activeListTabStatus,
    });
  }

  @OverrideDefaultAction()
  @Action(DepositAction.UploadFile)
  uploadFile(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UploadFile): Observable<any> {
    if (isNotNullNorUndefined(ctx.getState().dataFileLogo)) {
      const observable = this._actions$.pipe(
        ofSolidifyActionCompleted(DepositAction.DeleteFileSuccess),
        take(1),
        switchMap(result => {
          if (isTrue(result.result.successful)) {
            return this._uploadPhotoReady(ctx, action);
          }
          return of(false);
        }));
      ctx.dispatch(new DepositAction.DeleteFile(action.resId, action.fileUploadWrapper.file));
      return observable;
    } else {
      return this._uploadPhotoReady(ctx, action);
    }
  }

  private _getThumbnailDataType(metadataVersion: Enums.MetadataVersion.MetadataVersionEnum): Enums.DataFile.DataCategoryAndType.DataTypeEnum {
    const isLegacyType = [
      Enums.MetadataVersion.MetadataVersionEnum._1_0,
      Enums.MetadataVersion.MetadataVersionEnum._1_1,
      Enums.MetadataVersion.MetadataVersionEnum._2_0,
      Enums.MetadataVersion.MetadataVersionEnum._2_1,
      Enums.MetadataVersion.MetadataVersionEnum._3_0,
    ].includes(metadataVersion);
    return isLegacyType ? Enums.DataFile.DataCategoryAndType.DataTypeEnum.DATASET_THUMBNAIL : Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_THUMBNAIL;
  }

  private _uploadPhotoReady(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.UploadFile): Observable<any> {
    const actionUploadDataFile = new DepositUploadAction.UploadFile(action.resId, {
      dataCategory: Enums.DataFile.DataCategoryAndType.DataCategoryEnum.INTERNAL,
      dataType: this._getThumbnailDataType(action.metadataVersion),
      metadataType: undefined,
      subDirectory: undefined,
      file: action.fileUploadWrapper.file,
    } as DlcmFileUploadWrapper);

    return StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      actionUploadDataFile,
      DepositUploadAction.UploadFileSuccess,
      resultAction => {
        const actionUploadDatafileSuccess = resultAction.solidifyFile;
        ctx.patchState({
          dataFileLogo: actionUploadDatafileSuccess,
        });
        ctx.dispatch([
          new DepositAction.UploadFileSuccess(action, actionUploadDatafileSuccess),
          new DepositAction.GetFile(actionUploadDatafileSuccess.resId),
        ]);
      },
      DepositUploadAction.UploadFileFail,
      resultAction => {
        ctx.dispatch(new DepositAction.UploadFileFail(action));
      },
    );
  }

  @Action(DepositAction.CheckPhoto)
  checkPhoto(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckPhoto): Observable<any> {
    const depositId = action.depositId;

    const queryParameters = new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo);
    const searchItems = QueryParametersUtil.getSearchItems(queryParameters);
    MappingObjectUtil.set(searchItems, this._KEY_DATA_CATEGORY, Enums.DataFile.DataCategoryAndType.DataCategoryEnum.INTERNAL);
    MappingObjectUtil.set(searchItems, this._KEY_DATA_TYPE, this._getThumbnailDataType(action.metadataVersion));
    return this._apiService.getCollection<DataFile>(this._urlResource + urlSeparator + depositId + urlSeparator + ApiResourceNameEnum.DATAFILE, queryParameters)
      .pipe(
        tap((collection: CollectionTyped<DataFile>) => {
          if (collection._data.length === 1) {
            ctx.patchState({
              dataFileLogo: collection._data[0],
            });

            ctx.dispatch(new DepositAction.GetFile(depositId));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.GetReadmeDataFile)
  getReadmeDataFile(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetReadmeDataFile): Observable<DepositDataFile> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const queryParameters = new QueryParameters(environment.minimalPageSizeToRetrievePaginationInfo, {
      order: OrderEnum.descending,
      field: "creation.when",
    });
    const searchItems = QueryParametersUtil.getSearchItems(queryParameters);
    MappingObjectUtil.set(searchItems, this._KEY_DATA_CATEGORY, Enums.DataFile.DataCategoryAndType.DataCategoryEnum.INTERNAL);
    MappingObjectUtil.set(searchItems, this._KEY_DATA_TYPE, Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_README);
    return this._apiService.getCollection<DepositDataFile>(`${this._urlResource}/${action.id}/${ApiResourceNameEnum.DATAFILE}`, queryParameters)
      .pipe(
        map((collection: CollectionTyped<DepositDataFile>) => {
          let readmeDataFile;
          if (collection._data.length === 1) {
            readmeDataFile = collection._data[0];
          }
          ctx.dispatch(new DepositAction.GetReadmeDataFileSuccess(action, readmeDataFile));
          return readmeDataFile;
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.GetReadmeDataFileFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositAction.GetReadmeDataFileSuccess)
  getReadmeDataFileSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetReadmeDataFileSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      readmeDataFile: action.depositDataFile,
    });
  }

  @Action(DepositAction.GetReadmeDataFileFail)
  getReadmeDataFileFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetReadmeDataFileFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.DownloadReadme)
  downloadReadme(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.DownloadReadme): Observable<Blob> {
    const url = `${this._urlResource}/${action.id}/${ApiResourceNameEnum.DATAFILE}/${action.depositDataFile.resId}/${ApiActionNameEnum.DL}`;
    const fileName = action.depositDataFile.fileName;
    const mimetype = action.depositDataFile.fileFormat?.contentType;
    return this._downloadService.downloadInMemory(url, fileName, false, mimetype).pipe(
      map(blob => {
        ctx.dispatch(new DepositAction.DownloadReadmeSuccess(action, blob));
        return blob;
      }),
      catchError((error: SolidifyHttpErrorResponseModel) => {
        ctx.dispatch(new DepositAction.DownloadReadmeFail(action));
        throw new SolidifyStateError(this, error);
      }),
    );
  }

  @OverrideDefaultAction()
  @Action(DepositAction.GetFile)
  getFile(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetFile): Observable<any> {
    ctx.patchState({
      isLoadingFile: true,
    });

    let isReady = false;

    return PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: 1,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: 5,
      continueUntil: () => !isReady,
      actionToDo: () => {
        const depositId = ctx.getState().current?.resId;
        const datafileId = ctx.getState().dataFileLogo?.resId;

        if (isNullOrUndefinedOrWhiteString(depositId) || isNullOrUndefinedOrWhiteString(datafileId)) {
          return;
        }

        const url = `${this._urlResource}/${depositId}/${ApiResourceNameEnum.DATAFILE}/${datafileId}`;
        this.subscribe(this._apiService.get(url)
          .pipe(
            tap((result: DataFile) => {
              if (result.status === Enums.DataFile.StatusEnum.READY) {
                isReady = true;
                this._getPhotoReady(depositId, datafileId, ctx, action);
              }
              if (result.status === Enums.DataFile.StatusEnum.IN_ERROR) {
                isReady = true;
                ctx.dispatch(new DepositAction.GetFileFail(action));
              }
              if (result.status === Enums.DataFile.StatusEnum.CLEANED) {
                isReady = true;
                ctx.dispatch(new DepositAction.GetFileFail(action));
              }
            }),
            catchError(e => {
              isReady = true;
              ctx.dispatch(new DepositAction.GetFileFail(action));
              throw new SolidifyStateError(this, e);
            }),
          ));
      },
    });
  }

  private _getPhotoReady(depositId: string, datafileId: string, ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetFile): void {
    const url = `${this._urlResource}/${depositId}/${ApiResourceNameEnum.DATAFILE}/${datafileId}/${ApiActionNameEnum.DL}`;
    let headers = new HttpHeaders();
    headers = headers.set("Content-Disposition", "attachment; filename=logo");

    this.subscribe(this._httpClient.get(url, {
      headers,
      responseType: "blob",
    }).pipe(
      tap((blobContent: Blob) => {
        ctx.dispatch(new DepositAction.GetFileSuccess(action, blobContent));
      }),
      catchError(e => {
        ctx.dispatch(new DepositAction.GetFileFail(action));
        throw new SolidifyStateError(this, e);
      }),
    ));
  }

  @OverrideDefaultAction()
  @Action(DepositAction.DeleteFile)
  deleteFile(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.DeleteFile): Observable<any> {
    const dataFileLogo = ctx.getState().dataFileLogo;
    if (isNullOrUndefined(dataFileLogo)) {
      return undefined;
    }
    const depositId = ctx.getState().current.resId;

    ctx.patchState({
      isLoadingFile: true,
    });

    return ctx.dispatch(new DepositDataFileAction.Delete(depositId, dataFileLogo.resId)).pipe(
      tap(() => {
        ctx.dispatch(new DepositAction.DeleteFileSuccess(action));
      }),
      catchError(e => {
        ctx.dispatch(new DepositAction.DeleteFileFail(action));
        throw new SolidifyStateError(this, e);
      }),
    );
  }

  @OverrideDefaultAction()
  @Action(DepositAction.DeleteFileSuccess)
  deleteFileSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.DeleteFileSuccess): void {
    ctx.patchState({
      isLoadingFile: false,
      dataFileLogo: undefined,
      file: undefined,
    });
  }

  @Action(DepositAction.PutInError)
  putInError(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.PutInError): Observable<Result> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._httpClient.post<Result>(`${this._urlResource}/${action.id}/${ApiActionNameEnum.PUT_IN_ERROR}`, null)
      .pipe(
        tap(result => {
          if (result?.status === ResultActionStatusEnum.EXECUTED) {
            ctx.dispatch(new DepositAction.PutInErrorSuccess(action));
          } else {
            ctx.dispatch(new DepositAction.PutInErrorFail(action));
          }
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.PutInErrorFail(action));
          throw error;
        }),
      );
  }

  @Action(DepositAction.PutInErrorSuccess)
  putInErrorSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.PutInErrorSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.deposit.action.putInError.success"));
    ctx.dispatch(new DepositAction.GetById(action.parentAction.id, true));
  }

  @Action(DepositAction.PutInErrorFail)
  putInErrorFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.PutInErrorFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showError(MARK_AS_TRANSLATABLE("notification.deposit.action.putInError.fail"));
  }

  @Action(DepositAction.CheckSubmissionAgreement)
  checkSubmissionAgreement(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckSubmissionAgreement): Observable<void> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.post<void>(`${this._urlResource}/${action.depositId}/${ApiActionNameEnum.CHECK_SUBMISSION_AGREEMENT}`)
      .pipe(
        tap(submissionAgreement => {
          ctx.dispatch(new DepositAction.CheckSubmissionAgreementSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          const notFound = error.status === HttpStatusCode.NotFound;
          ctx.dispatch(new DepositAction.CheckSubmissionAgreementFail(action, notFound));
          if (notFound) {
            return of(undefined);
          }
          throw error;
        }),
      );
  }

  @Action(DepositAction.CheckSubmissionAgreementSuccess)
  checkSubmissionAgreementSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckSubmissionAgreementSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.CheckSubmissionAgreementFail)
  checkSubmissionAgreementFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.CheckSubmissionAgreementFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.ApproveSubmissionAgreement)
  approveSubmissionAgreement(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ApproveSubmissionAgreement): Observable<any> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._httpClient.post<any>(`${this._urlResource}/${action.depositId}/${ApiActionNameEnum.APPROVE_SUBMISSION_AGREEMENT}`, null)
      .pipe(
        tap(object => {
          ctx.dispatch(new DepositAction.ApproveSubmissionAgreementSuccess(action));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.ApproveSubmissionAgreementFail(action));
          throw error;
        }),
      );
  }

  @Action(DepositAction.ApproveSubmissionAgreementSuccess)
  approveSubmissionAgreementSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ApproveSubmissionAgreementSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.ApproveSubmissionAgreementFail)
  approveSubmissionAgreementFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.ApproveSubmissionAgreementFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.GenerateAnonymizedDepositPage)
  generateAnonymizedDepositPage(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GenerateAnonymizedDepositPage): Observable<string> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._httpClient.post<string>(`${this._urlResource}/${action.depositId}/${ApiActionNameEnum.GENERATE_ANONYMIZED_DEPOSIT_PAGE}`, undefined, {
      responseType: "text" as any,
    })
      .pipe(
        tap(url => {
          ctx.dispatch(new DepositAction.GenerateAnonymizedDepositPageSuccess(action, url));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.GenerateAnonymizedDepositPageFail(action));
          throw error;
        }),
      );
  }

  @Action(DepositAction.GenerateAnonymizedDepositPageSuccess)
  generateAnonymizedDepositPageSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GenerateAnonymizedDepositPageSuccess): void {
    ctx.patchState({
      anonymousReviewUrl: action.url,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
    this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.deposit.action.generateAnonymizedDepositPage.success"));
  }

  @Action(DepositAction.GenerateAnonymizedDepositPageFail)
  generateAnonymizedDepositPageFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GenerateAnonymizedDepositPageFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.GetAnonymizedDepositPage)
  getAnonymizedDepositPage(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetAnonymizedDepositPage): Observable<string> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    const url = `${this._urlResource}/${action.anonymizedDepositPageId}/${ApiActionNameEnum.GET_ANONYMIZED_DEPOSIT_PAGE}`;
    return this._httpClient.head<string>(url)
      .pipe(
        tap(() => {
          ctx.dispatch(new DepositAction.GetAnonymizedDepositPageSuccess(action, url));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new DepositAction.GetAnonymizedDepositPageFail(action));
          throw environment.errorToSkipInErrorHandler;
        }),
      );
  }

  @Action(DepositAction.GetAnonymizedDepositPageSuccess)
  getAnonymizedDepositPageSuccess(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetAnonymizedDepositPageSuccess): void {
    ctx.patchState({
      anonymousReviewUrl: action.url,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  @Action(DepositAction.GetAnonymizedDepositPageFail)
  getAnonymizedDepositPageFail(ctx: SolidifyStateContext<DepositStateModel>, action: DepositAction.GetAnonymizedDepositPageFail): void {
    ctx.patchState({
      anonymousReviewUrl: undefined,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
