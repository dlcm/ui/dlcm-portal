/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-organizational-unit-additional-fields-form.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  DepositOrganizationalUnitAdditionalFieldsFormAction,
  depositOrganizationalUnitAdditionalFieldsFormActionNameSpace,
} from "@deposit/stores/organizational-unit/additional-fields-form/deposit-organizational-unit-additional-fields-form.action";
import {environment} from "@environments/environment";
import {AdditionalFieldsForm} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CompositionState,
  CompositionStateModel,
  defaultCompositionStateInitValue,
  NotificationService,
  QueryParameters,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
} from "solidify-frontend";
import GetCurrentMetadataFormFail = DepositOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormFail;
import GetCurrentMetadataFormSuccess = DepositOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormSuccess;

export const defaultDepositOrganizationalUnitAdditionalFieldsFormStateModel: () => DepositOrganizationalUnitAdditionalFieldsFormStateModel = () =>
  ({
    ...defaultCompositionStateInitValue(),
    queryParameters: new QueryParameters(20),
    loaded: false,
  });

export interface DepositOrganizationalUnitAdditionalFieldsFormStateModel extends CompositionStateModel<AdditionalFieldsForm> {
  loaded: boolean;
}

@Injectable()
@State<DepositOrganizationalUnitAdditionalFieldsFormStateModel>({
  name: StateEnum.deposit_organizationalUnit_additionalFieldsForm,
  defaults: {
    ...defaultDepositOrganizationalUnitAdditionalFieldsFormStateModel(),
  },
})
export class DepositOrganizationalUnitAdditionalFieldsFormState extends CompositionState<DepositOrganizationalUnitAdditionalFieldsFormStateModel, AdditionalFieldsForm> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: depositOrganizationalUnitAdditionalFieldsFormActionNameSpace,
      resourceName: ApiResourceNameEnum.ADDITIONAL_FIELDS_FORM,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOrganizationalUnits;
  }

  @Action(DepositOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataForm)
  getCurrentMetadataForm(ctx: SolidifyStateContext<DepositOrganizationalUnitAdditionalFieldsFormStateModel>, action: DepositOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataForm): Observable<AdditionalFieldsForm> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });
    return this._apiService.get<AdditionalFieldsForm>(`${this._urlResource}/${action.parentId}/${this._resourceName}/${ApiResourceNameEnum.CURRENT_VERSION}`)
      .pipe(
        tap(result => ctx.dispatch(new GetCurrentMetadataFormSuccess(action, result))),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new GetCurrentMetadataFormFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(DepositOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormSuccess)
  getCurrentMetadataFormSuccess(ctx: SolidifyStateContext<DepositOrganizationalUnitAdditionalFieldsFormStateModel>, action: DepositOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormSuccess): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      current: action.currentForm,
      loaded: true,
    });
  }

  @Action(DepositOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormFail)
  getCurrentMetadataFormFail(ctx: SolidifyStateContext<DepositOrganizationalUnitAdditionalFieldsFormStateModel>, action: DepositOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataFormFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
