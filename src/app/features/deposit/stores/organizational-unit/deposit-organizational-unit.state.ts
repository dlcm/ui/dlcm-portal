/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-organizational-unit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  defaultDepositOrganizationalUnitAdditionalFieldsFormStateModel,
  DepositOrganizationalUnitAdditionalFieldsFormState,
  DepositOrganizationalUnitAdditionalFieldsFormStateModel,
} from "@deposit/stores/organizational-unit/additional-fields-form/deposit-organizational-unit-additional-fields-form.state";
import {
  DepositOrganizationalUnitAction,
  depositOrganizationalUnitNameSpace,
} from "@deposit/stores/organizational-unit/deposit-organizational-unit.action";
import {environment} from "@environments/environment";
import {OrganizationalUnit} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultResourceFileStateInitValue,
  defaultAssociationStateInitValue,
  DownloadService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  NotificationService,
  OverrideDefaultAction,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyStateContext,
} from "solidify-frontend";
import {
  DepositOrganizationalUnitSubjectAreaState,
  DepositOrganizationalUnitSubjectAreaStateModel,
} from "@deposit/stores/organizational-unit/subject-area/deposit-organizational-unit-subject-area.state";

export const defaultDepositOrganizationalUnitValue: () => DepositOrganizationalUnitStateModel = () =>
  ({
    ...defaultResourceFileStateInitValue(),
    deposit_organizationalUnit_additionalFieldsForm: defaultDepositOrganizationalUnitAdditionalFieldsFormStateModel(),
    deposit_organizationalUnit_subjectArea: {...defaultAssociationStateInitValue()},
  });

export interface DepositOrganizationalUnitStateModel extends ResourceFileStateModel<OrganizationalUnit> {
  deposit_organizationalUnit_additionalFieldsForm: DepositOrganizationalUnitAdditionalFieldsFormStateModel;
  deposit_organizationalUnit_subjectArea: DepositOrganizationalUnitSubjectAreaStateModel;
}

@Injectable()
@State<DepositOrganizationalUnitStateModel>({
  name: StateEnum.deposit_organizationalUnit,
  defaults: {
    ...defaultDepositOrganizationalUnitValue(),
  },
  children: [
    DepositOrganizationalUnitAdditionalFieldsFormState,
    DepositOrganizationalUnitSubjectAreaState,
  ],
})
export class DepositOrganizationalUnitState extends ResourceFileState<DepositOrganizationalUnitStateModel, OrganizationalUnit> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: depositOrganizationalUnitNameSpace,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOrganizationalUnits;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static currentTitle(state: DepositOrganizationalUnitStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @OverrideDefaultAction()
  @Action(DepositOrganizationalUnitAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<DepositOrganizationalUnitStateModel>, action: DepositOrganizationalUnitAction.GetByIdSuccess): void {
    ctx.patchState({
      current: action.model,
    });
    if (isNotNullNorUndefined(action.model.logo)) {
      ctx.dispatch(new DepositOrganizationalUnitAction.GetFile(action.model.resId));
    } else {
      ctx.patchState({
        file: undefined,
      });
    }
  }
}
