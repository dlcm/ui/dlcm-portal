/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-collection.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnInit,
} from "@angular/core";
import {
  MatDialog,
  MatDialogRef,
} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {ModeDepositTabEnum} from "@deposit/enums/mode-deposit-tab.enum";
import {DepositCollectionAction} from "@deposit/stores/collection/deposit-collection.action";
import {DepositCollectionState} from "@deposit/stores/collection/deposit-collection.state";
import {depositCollectionStatusHistoryNamespace} from "@deposit/stores/collection/status-history/deposit-collection-status-history.action";
import {DepositCollectionStatusHistoryState} from "@deposit/stores/collection/status-history/deposit-collection-status-history.state";
import {depositActionNameSpace} from "@deposit/stores/deposit.action";
import {
  DepositState,
  DepositStateModel,
} from "@deposit/stores/deposit.state";
import {DepositOrganizationalUnitState} from "@deposit/stores/organizational-unit/deposit-organizational-unit.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Aip,
  Deposit,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedFileAndAipInformationContainer} from "@shared/components/containers/shared-file-and-aip-information/shared-file-and-aip-information.container";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  AbstractDetailEditRoutable,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  isNullOrUndefined,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  ResourceActionHelper,
  StatusHistory,
  StatusHistoryDialog,
  StoreUtil,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-collection-container",
  templateUrl: "./deposit-collection.container.html",
  styleUrls: ["./deposit-collection.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositCollectionContainer extends AbstractDetailEditRoutable<Deposit, DepositStateModel> implements OnInit {
  readonly KEY_PARAM_NAME: keyof Deposit & string = undefined;

  @Input()
  mode: ModeDepositTabEnum;

  isLoadingCollectionObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositCollectionState);
  listCollectionObs: Observable<Aip[]> = MemoizedUtil.selected(this._store, DepositCollectionState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, DepositCollectionState);

  dialogCollectionFileRef: MatDialogRef<SharedFileAndAipInformationContainer<Aip>>;

  columns: DataTableColumns<Aip>[];
  actions: DataTableActions<Aip>[] = [
    {
      logo: IconNameEnum.delete,
      callback: (aip: Aip) => this.deleteAip(this._resId, aip),
      placeholder: current => LabelTranslateEnum.delete,
      displayOnCondition: (aip: Aip) => MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.canEdit) !== DepositEditModeEnum.none,
    },
    {
      logo: IconNameEnum.internalLink,
      callback: (aip: Aip) => this._goToAip(aip),
      placeholder: current => LabelTranslateEnum.goToAip,
      displayOnCondition: (aip: Aip) => this._securityService.isRootOrAdmin(),
    },
  ];

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.deposit, _injector, depositActionNameSpace);

    this.columns = [
      {
        field: "info.name" as any,
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
      },
      {
        field: "info.dataSensitivity" as any,
        header: LabelTranslateEnum.sensitivity,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        filterEnum: Enums.DataSensitivity.DataSensitivityEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.sensitivity),
      },
      {
        field: "info.access" as any,
        header: LabelTranslateEnum.accessLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        filterEnum: Enums.Access.AccessEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.accessLevelWithEmbargo),
      },
      {
        field: "smartSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        filterableField: "archiveSize",
        isFilterable: false,
        isSortable: false,
        alignment: "right",
        width: "100px",
      },
      {
        field: "archiveFileNumber",
        header: LabelTranslateEnum.files,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        filterableField: "archiveFileNumber",
        isFilterable: false,
        isSortable: false,
        width: "80px",
      },
      {
        field: "info.status" as any,
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        filterEnum: Enums.Package.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "info.complianceLevel" as any,
        header: LabelTranslateEnum.complianceLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isSortable: false,
        isFilterable: false,
        translate: true,
        component: DataTableComponentHelper.get(DataTableComponentEnum.conformityLevelStar),
        filterEnum: Enums.ComplianceLevel.ComplianceLevelEnumTranslate,
        alignment: "center",
      },
    ];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._getCurrentModelOnParent();
  }

  private _getCurrentModelOnParent(): void {
    this._resId = this._route.snapshot.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._getDepositById(this._resId);
  }

  private _getDepositById(id: string): void {
    const depositInState = MemoizedUtil.currentSnapshot(this._store, DepositState);
    if (isNullOrUndefined(depositInState) || id !== depositInState.resId) {
      this._store.dispatch(ResourceActionHelper.getById(depositActionNameSpace, id, true));
    }
    this._getSubResourceWithParentId(id);
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new DepositCollectionAction.GetAll(id, null, true));
  }

  refresh(): void {
    this._store.dispatch(new DepositCollectionAction.Refresh(this._resId));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new DepositCollectionAction.ChangeQueryParameters(this._resId, queryParameters, true));
  }

  showDetail(aip: Aip): void {
    const orgUnitId = MemoizedUtil.currentSnapshot(this._store, DepositOrganizationalUnitState)?.resId;
    this._store.dispatch(new Navigate([AppRoutesEnum.deposit, orgUnitId, DepositRoutesEnum.detail, this._resId, DepositRoutesEnum.collections, aip.resId]));
  }

  private _goToAip(aip: Aip): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new DepositCollectionAction.GoToAip(aip) as any,
      DepositCollectionAction.GoToAip as any,
      resultAction => {
        if (this.dialogCollectionFileRef) {
          this.dialogCollectionFileRef.close();
        }
      }));
  }

  deleteAip(_resId: string, aip: Aip): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new DepositCollectionAction.Delete(this._resId, aip.resId),
      DepositCollectionAction.DeleteSuccess,
      resultAction => {
        if (this.dialogCollectionFileRef) {
          this.dialogCollectionFileRef.close();
        }
        this._store.dispatch(new DepositCollectionAction.Refresh(this._resId));
      }));
  }

  showHistory(aip: Aip): void {
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositCollectionStatusHistoryState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, DepositCollectionStatusHistoryState, state => state.queryParameters);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, DepositCollectionStatusHistoryState, state => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: aip.resId,
      name: StringUtil.convertToPascalCase(StateEnum.deposit_collection),
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: depositCollectionStatusHistoryNamespace,
      statusEnums: Enums.Deposit.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }
}
