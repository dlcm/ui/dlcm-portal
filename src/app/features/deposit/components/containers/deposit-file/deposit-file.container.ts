/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-file.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {DepositFileMoveDialog} from "@deposit/components/dialogs/deposit-file-move/deposit-file-move.dialog";
import {DepositFileUploadDialog} from "@deposit/components/dialogs/deposit-file-upload/deposit-file-upload.dialog";
import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {ModeDepositTabEnum} from "@deposit/enums/mode-deposit-tab.enum";
import {DepositDataFileHelper} from "@deposit/helpers/deposit-data-file.helper";
import {DlcmFileUploadWrapper} from "@deposit/models/dlcm-file-upload-wrapper.model";
import {DepositDataFileAction} from "@deposit/stores/data-file/deposit-data-file.action";
import {DepositDataFileState} from "@deposit/stores/data-file/deposit-data-file.state";
import {depositDataFileStatusHistoryNamespace} from "@deposit/stores/data-file/status-history/deposit-data-file-status-history.action";
import {DepositDataFileStatusHistoryState} from "@deposit/stores/data-file/status-history/deposit-data-file-status-history.state";
import {
  DepositAction,
  depositActionNameSpace,
} from "@deposit/stores/deposit.action";
import {
  DepositState,
  DepositStateModel,
} from "@deposit/stores/deposit.state";
import {DepositOrganizationalUnitState} from "@deposit/stores/organizational-unit/deposit-organizational-unit.state";
import {DepositUploadAction} from "@deposit/stores/upload/deposit-upload.action";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Deposit,
  DepositDataFile,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {FileViewModeEnum} from "@shared/enums/file-view-mode.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  map,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditRoutable,
  ButtonColorEnum,
  ConfirmDialog,
  DataTableActions,
  DataTableBulkActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DataTablePresentational,
  DeleteDialog,
  DialogUtil,
  FilePreviewDialog,
  FileVisualizerHelper,
  FileVisualizerService,
  isNonEmptyArray,
  isNullOrUndefined,
  isTrue,
  isUndefined,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  ResourceActionHelper,
  StatusHistory,
  StatusHistoryDialog,
  StringUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-file-container",
  templateUrl: "./deposit-file.container.html",
  styleUrls: ["./deposit-file.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFileContainer extends AbstractDetailEditRoutable<Deposit, DepositStateModel> implements OnInit {
  private readonly _KEY_QUERY_PARAMETERS: keyof DepositDataFile = "status";
  isLoadingDataFileObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositDataFileState);
  listDataFileObs: Observable<DepositDataFile[]> = MemoizedUtil.list(this._store, DepositDataFileState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, DepositDataFileState).pipe(
    tap(queryParameters => this.isInErrorStatusFilter = MappingObjectUtil.get(QueryParametersUtil.getSearchItems(queryParameters), this._KEY_QUERY_PARAMETERS) === Enums.DataFile.StatusEnum.IN_ERROR),
  );
  draggingDepositDataFile: DepositDataFile | undefined = undefined;
  intermediateFoldersObs: Observable<string[]> = MemoizedUtil.select(this._store, DepositDataFileState, state => state.intermediateFolders);
  foldersWithIntermediateFoldersObs: Observable<string[]> = MemoizedUtil.select(this._store, DepositDataFileState, state => state.foldersWithIntermediateFolders);
  currentFolderObs: Observable<string> = MemoizedUtil.select(this._store, DepositDataFileState, state => state.currentFolder);
  canEditObs: Observable<DepositEditModeEnum> = MemoizedUtil.select(this._store, DepositState, state => state.canEdit).pipe(
    distinctUntilChanged(),
    tap(canEdit => {
      if (canEdit !== DepositEditModeEnum.none) {
        this.actions = [...this.actions];
      }
    }), // Force method computeContext on datatable
  );

  @Input()
  mode: ModeDepositTabEnum;

  @ViewChild("dataTablePresentational")
  readonly dataTablePresentational: DataTablePresentational<DepositDataFile>;

  currentFileViewMode: FileViewModeEnum = FileViewModeEnum.FolderView;

  columns: DataTableColumns<DepositDataFile>[];

  actions: DataTableActions<DepositDataFile>[] = [
    {
      logo: IconNameEnum.notIgnore,
      callback: (depositDataFile: DepositDataFile) => this.validate(this._resId, depositDataFile),
      placeholder: current => LabelTranslateEnum.doNotIgnore,
      displayOnCondition: (depositDataFile: DepositDataFile) => depositDataFile.status === Enums.DataFile.StatusEnum.IGNORED_FILE,
    },
    {
      logo: IconNameEnum.download,
      callback: (depositDataFile: DepositDataFile) => this.downloadDataFile(this._resId, depositDataFile),
      placeholder: current => LabelTranslateEnum.download,
      displayOnCondition: current => current.status === Enums.DataFile.StatusEnum.PROCESSED || current.status === Enums.DataFile.StatusEnum.READY || current.status === Enums.DataFile.StatusEnum.VIRUS_CHECKED || current.status === Enums.DataFile.StatusEnum.IGNORED_FILE,
      isWrapped: false,
    },
    {
      logo: IconNameEnum.resume,
      callback: (depositDataFile: DepositDataFile) => this.resumeDataFile(this._resId, depositDataFile),
      placeholder: current => LabelTranslateEnum.resume,
      displayOnCondition: (depositDataFile: DepositDataFile) => depositDataFile.status === Enums.DataFile.StatusEnum.IN_ERROR,
    },
    {
      logo: IconNameEnum.delete,
      callback: (depositDataFile: DepositDataFile) => this._deleteDataFile(this._resId, depositDataFile),
      placeholder: current => LabelTranslateEnum.delete,
      displayOnCondition: (depositDataFile: DepositDataFile) => {
        const canEdit = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.canEdit);
        return canEdit === DepositEditModeEnum.full
          || (canEdit === DepositEditModeEnum.metadata && depositDataFile.dataCategory === Enums.DataFile.DataCategoryAndType.DataCategoryEnum.PACKAGE && depositDataFile.dataType === Enums.DataFile.DataCategoryAndType.DataTypeEnum.UPDATED_METADATA);
      },
    },
    {
      logo: IconNameEnum.preview,
      callback: (depositDataFile: DepositDataFile) => this._showPreview(depositDataFile),
      placeholder: current => LabelTranslateEnum.showPreview,
      displayOnCondition: (depositDataFile: DepositDataFile) => FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, {
        dataFile: DataFileHelper.dataFileAdapter(depositDataFile),
        fileExtension: FileVisualizerHelper.getFileExtension(depositDataFile.sourceData),
      }),
    },
    {
      logo: IconNameEnum.move,
      callback: (depositDataFile: DepositDataFile) => this.moveDataFileWithDialog(depositDataFile),
      placeholder: current => LabelTranslateEnum.move,
      displayOnCondition: (depositDataFile: DepositDataFile) => {
        const canEdit = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.canEdit);
        return canEdit === DepositEditModeEnum.full;
      },
    },
  ];

  bulkActions: DataTableBulkActions<DepositDataFile>[] = [
    {
      icon: IconNameEnum.delete,
      callback: (list: DepositDataFile[]) => this._bulkDeleteDataFile(this._resId, list.map(s => s.resId)),
      labelToTranslate: () => LabelTranslateEnum.deleteSelection,
      displayCondition: (list: DepositDataFile[]) => this.canEditObs.pipe(map(canEdit => canEdit === DepositEditModeEnum.full)),
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.resume,
      callback: (list: DepositDataFile[]) => this._bulkResumeDataFile(this._resId, list.map(s => s.resId)),
      labelToTranslate: () => LabelTranslateEnum.resumeSelection,
      displayCondition: (list: DepositDataFile[]) => this.canEditObs.pipe(map(canEdit => canEdit === DepositEditModeEnum.full)) && list.findIndex(d => d.status === Enums.DataFile.StatusEnum.IN_ERROR) !== -1,
      color: ButtonColorEnum.primary,
    },
    {
      icon: IconNameEnum.notIgnore,
      callback: (list: DepositDataFile[]) => this._bulkValidateDataFile(this._resId, list.map(s => s.resId)),
      labelToTranslate: () => LabelTranslateEnum.doNotIgnoreSelection,
      displayCondition: (list: DepositDataFile[]) => this.canEditObs.pipe(map(canEdit => canEdit === DepositEditModeEnum.full)) && list.findIndex(d => d.status === Enums.DataFile.StatusEnum.IGNORED_FILE) !== -1,
      color: ButtonColorEnum.primary,
    },
  ];

  columnsToSkippedFilter: keyof DepositDataFile[] | string[] = [
    DepositDataFileHelper.RELATIVE_LOCATION,
    "status",
  ];

  get fileViewModeEnum(): typeof FileViewModeEnum {
    return FileViewModeEnum;
  }

  get depositEnum(): typeof Enums.Deposit.StatusEnum {
    return Enums.Deposit.StatusEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  get modeDepositTabEnum(): typeof ModeDepositTabEnum {
    return ModeDepositTabEnum;
  }

  get depositEditModeEnum(): typeof DepositEditModeEnum {
    return DepositEditModeEnum;
  }

  readonly KEY_PARAM_NAME: keyof Deposit & string = undefined;
  isInErrorStatusFilter: boolean = false;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService,
              private readonly _fileVisualizerService: FileVisualizerService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.deposit, _injector, depositActionNameSpace);

    this.columns = [
      {
        field: "fileName",
        header: LabelTranslateEnum.fileName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.ascending,
        filterableField: "finalData" as any,
        sortableField: "finalData" as any,
        isSortable: true,
        isFilterable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: true,
      },
      {
        field: "status",
        header: StringUtil.stringEmpty,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        sortableField: "status" as any,
        filterableField: "status" as any,
        isSortable: false,
        isFilterable: false,
        translate: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.dataFileQuickStatus),
        width: "35px",
        alignment: "center",
        // filterEnum: ComplianceLevelEnumHelper.getListKeyValue(),
      },
      {
        field: "status",
        header: LabelTranslateEnum.status,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        sortableField: "status" as any,
        isSortable: true,
        isFilterable: true,
        translate: true,
        tooltip: (value) => Enums.DataFile.getStatusExplanation(value as Enums.DataFile.StatusEnum),
        filterEnum: Enums.DataFile.StatusEnumTranslate,
        component: DataTableComponentHelper.get(DataTableComponentEnum.status),
        alignment: "center",
      },
      {
        field: "complianceLevel",
        header: LabelTranslateEnum.complianceLevel,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        sortableField: "complianceLevel" as any,
        filterableField: "complianceLevel" as any,
        isSortable: true,
        isFilterable: false,
        translate: true,
        width: "185px",
        component: DataTableComponentHelper.get(DataTableComponentEnum.conformityLevelStar),
        filterEnum: Enums.ComplianceLevel.ComplianceLevelEnumTranslate,
        alignment: "center",
      },
      {
        field: "dataCategory",
        header: LabelTranslateEnum.dataCategory,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        filterEnum: Enums.DataFile.DataCategoryAndType.DataCategoryEnumTranslate,
        sortableField: "dataCategory" as any,
        filterableField: "dataCategory" as any,
        isSortable: true,
        isFilterable: true,
        translate: true,
      },
      {
        field: "smartSize",
        header: LabelTranslateEnum.size,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        sortableField: "fileSize" as any,
        filterableField: "fileSize" as any,
        isSortable: true,
        isFilterable: false,
        translate: true,
        alignment: "right",
        width: "100px",
      },
    ];
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._getCurrentModelOnParent();
  }

  private _getCurrentModelOnParent(): void {
    this._resId = this._route.snapshot.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._getDepositById(this._resId);
  }

  private _getDepositById(id: string): void {
    const depositInState = MemoizedUtil.currentSnapshot(this._store, DepositState);
    if (isNullOrUndefined(depositInState) || id !== depositInState.resId) {
      this._store.dispatch(ResourceActionHelper.getById(depositActionNameSpace, id, true));
    }
    this._getSubResourceWithParentId(id);
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new DepositDataFileAction.GetAll(id, undefined, true));
    this._store.dispatch(new DepositDataFileAction.GetListFolder(id));
  }

  onQueryParametersEvent(queryParameters: QueryParameters, withRefresh: boolean = true): void {
    this._store.dispatch(new DepositDataFileAction.ChangeQueryParameters(this._resId, queryParameters, true, withRefresh));
  }

  refresh(): void {
    this._store.dispatch(new DepositDataFileAction.Refresh(this._resId));
  }

  download($event: DepositDataFile): void {
    this._store.dispatch(new DepositDataFileAction.Download(this._resId, $event));
  }

  selectFolder(folderFullName: string | undefined): void {
    if (isUndefined(folderFullName)) {
      this.currentFileViewMode = FileViewModeEnum.FlatView;
    } else {
      this.currentFileViewMode = FileViewModeEnum.FolderView;
    }
    this._store.dispatch(new DepositDataFileAction.ChangeCurrentFolder(folderFullName, true));
  }

  downloadFolder(folderName: string): void {
    this._store.dispatch(new DepositAction.Download(this._resId, folderName));
  }

  deleteFolder(folderName: string): void {
    this.subscribe(DialogUtil.open(this._dialog, DeleteDialog, {
        name: folderName,
        resourceNameSpace: depositActionNameSpace,
        message: MARK_AS_TRANSLATABLE("deposit.dialog.deleteFolder.message"),
      },
      {
        minWidth: "500px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this._store.dispatch(new DepositDataFileAction.DeleteFolder(this._resId, folderName));
        }
      }));
  }

  showDetail(depositDataFile: DepositDataFile): void {
    const orgUnitId = MemoizedUtil.currentSnapshot(this._store, DepositOrganizationalUnitState)?.resId;
    this._store.dispatch(new Navigate([AppRoutesEnum.deposit, orgUnitId, DepositRoutesEnum.detail, this._resId, DepositRoutesEnum.files, depositDataFile.resId]));
  }

  private _bulkDeleteDataFile(parentId: string, listId: string[]): Observable<boolean> {
    return DialogUtil.open(this._dialog, DeleteDialog, {
        name: undefined,
        resourceNameSpace: depositActionNameSpace,
        message: MARK_AS_TRANSLATABLE("deposit.dialog.deleteList.message"),
      }, {
        minWidth: "500px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this._bulkDeleteDataFileWithoutConfirmation(parentId, listId);
        }
      }).pipe(
      map(result => isTrue(result)),
    );
  }

  private _bulkDeleteDataFileWithoutConfirmation(parentId: string, listId: string[]): void {
    this._store.dispatch(new DepositDataFileAction.DeleteAll(parentId, listId));
  }

  private _bulkResumeDataFile(parentId: string, listId: string[]): void {
    this._store.dispatch(new DepositDataFileAction.ResumeAll(parentId, listId));
  }

  private _bulkValidateDataFile(parentId: string, listId: string[]): void {
    this._store.dispatch(new DepositDataFileAction.ValidateAll(parentId, listId));
  }

  private _deleteDataFile(parentId: string, dataFile: DepositDataFile): void {
    const deleteData = this._storeDialogService.deleteData(StateEnum.deposit_dataFile);
    deleteData.name = dataFile.fileName;
    this.subscribe(DialogUtil.open(this._dialog, DeleteDialog, deleteData,
      {
        width: "400px",
      },
      isConfirmed => {
        if (isTrue(isConfirmed)) {
          this._store.dispatch(new DepositDataFileAction.Delete(parentId, dataFile.resId));
        }
      }));
  }

  private _showPreview(dataFile: DepositDataFile): void {
    DialogUtil.open(this._dialog, FilePreviewDialog, {
        fileInput: {
          dataFile: DataFileHelper.dataFileAdapter(dataFile),
        },
        fileDownloadUrl: `${ApiEnum.preIngestDeposits}/${dataFile.infoPackage.resId}/${ApiResourceNameEnum.DATAFILE}/${dataFile.resId}/${ApiActionNameEnum.DL}`,
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      });
  }

  moveDataFile(dataFile: DepositDataFile): void {
    this._store.dispatch(new DepositDataFileAction.Move(this._resId, {
      resId: dataFile.resId,
      relativeLocation: dataFile.relativeLocation,
    }));
  }

  moveDataFileWithDialog(dataFile: DepositDataFile): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositFileMoveDialog, {
        parentResId: this._resId,
        depositDataFile: dataFile,
      }, undefined,
      result => {
        if (isTrue(result)) {
          // this._getOrRefreshResource();
        }
      }));
  }

  validate(parentId: string, dataFile: DepositDataFile): void {
    this._store.dispatch(new DepositDataFileAction.Validate(parentId, dataFile));
  }

  downloadDataFile(parentId: string, dataFile: DepositDataFile): void {
    this._store.dispatch(new DepositDataFileAction.Download(parentId, dataFile));
  }

  resumeDataFile(parentId: string, dataFile: DepositDataFile): void {
    this._store.dispatch(new DepositDataFileAction.Resume(parentId, dataFile));
  }

  selectAndDelete(): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.file.bulkDelete.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.file.bulkDelete.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
    }, undefined, (isConfirmed: boolean) => {
      if (isTrue(isConfirmed)) {
        this.subscribe(this.dataTablePresentational.selectAllResult().pipe(
          take(1),
          tap(selection => {
            this._bulkDeleteDataFileWithoutConfirmation(this._resId, selection);
            this.dataTablePresentational.cleanSelection();
          }),
        ));
      }
    }));
  }

  uploadInFolder(fullFolderPath: string): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositFileUploadDialog, {
        subDirectory: fullFolderPath,
      }, {
        width: "500px",
        disableClose: true,
      },
      listFilesUploadWrapper => {
        if (isNonEmptyArray(listFilesUploadWrapper)) {
          listFilesUploadWrapper.forEach(f => this.upload(f));
        }
      }));
  }

  upload($event: DlcmFileUploadWrapper): void {
    this._store.dispatch(new DepositUploadAction.UploadFile(this._resId, $event));
  }

  selectAndResumeAll(): void {
    this.subscribe(this.dataTablePresentational.selectAllResult().pipe(
      take(1),
      tap(selectionList => {
        this._bulkResumeDataFile(this._resId, selectionList);
        this.dataTablePresentational.cleanSelection();
      }),
    ));
  }

  dragStart($event: DepositDataFile): void {
    this.draggingDepositDataFile = $event;
  }

  dragEnd($event: DepositDataFile): void {
    this.draggingDepositDataFile = undefined;
  }

  showHistory(depositDataFile: DepositDataFile): void {
    const isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositDataFileStatusHistoryState);
    const queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, DepositDataFileStatusHistoryState, state => state.queryParameters);
    const historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, DepositDataFileStatusHistoryState, state => state.history);
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: depositDataFile.infoPackage.resId,
      resourceResId: depositDataFile.resId,
      name: depositDataFile.fileName,
      statusHistory: historyObs,
      isLoading: isLoadingHistoryObs,
      queryParametersObs: queryParametersHistoryObs,
      state: depositDataFileStatusHistoryNamespace,
      statusEnums: Enums.DataFile.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }
}
