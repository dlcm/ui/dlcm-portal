/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-upload.container.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  DepositAssociateArchiveDialog,
  DepositAssociateArchiveDialogData,
} from "@deposit/components/dialogs/deposit-associate-archive/deposit-associate-archive.dialog";
import {DepositFileUploadArchiveDialog} from "@deposit/components/dialogs/deposit-file-upload-archive/deposit-file-upload-archive.dialog";
import {DepositFileUploadDialog} from "@deposit/components/dialogs/deposit-file-upload/deposit-file-upload.dialog";
import {DepositFormPresentational} from "@deposit/components/presentationals/deposit-form/deposit-form.presentational";
import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {ModeDepositTabEnum} from "@deposit/enums/mode-deposit-tab.enum";
import {DlcmFileUploadWrapper} from "@deposit/models/dlcm-file-upload-wrapper.model";
import {DepositCollectionAction} from "@deposit/stores/collection/deposit-collection.action";
import {DepositCollectionState} from "@deposit/stores/collection/deposit-collection.state";
import {DepositDataFileAction} from "@deposit/stores/data-file/deposit-data-file.action";
import {DepositDataFileState} from "@deposit/stores/data-file/deposit-data-file.state";
import {depositActionNameSpace} from "@deposit/stores/deposit.action";
import {
  DepositState,
  DepositStateModel,
} from "@deposit/stores/deposit.state";
import {DepositUploadAction} from "@deposit/stores/upload/deposit-upload.action";
import {DepositUploadState} from "@deposit/stores/upload/deposit-upload.state";
import {Enums} from "@enums";
import {
  Deposit,
  DepositDataFile,
  SystemProperty,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  AbstractDetailEditRoutable,
  AppSystemPropertyState,
  DialogUtil,
  isNonEmptyArray,
  MemoizedUtil,
  QueryParameters,
  StoreUtil,
  UploadFileStatus,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-upload-container",
  templateUrl: "./deposit-upload.container.html",
  styleUrls: ["./deposit-upload.container.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositUploadContainer extends AbstractDetailEditRoutable<Deposit, DepositStateModel> implements OnInit {
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, DepositDataFileState);
  uploadStatusObs: Observable<UploadFileStatus<DepositDataFile>[]> = MemoizedUtil.select(this._store, DepositUploadState, state => state.uploadStatus);
  canEditObs: Observable<DepositEditModeEnum> = MemoizedUtil.select(this._store, DepositState, state => state.canEdit);
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);

  @ViewChild("formPresentational")
  readonly formPresentational: DepositFormPresentational;

  readonly KEY_PARAM_NAME: keyof Deposit & string = undefined;

  get dataCategoryEnum(): typeof Enums.DataFile.DataCategoryAndType.DataCategoryEnum {
    return Enums.DataFile.DataCategoryAndType.DataCategoryEnum;
  }

  get dataTypeEnum(): typeof Enums.DataFile.DataCategoryAndType.DataTypeEnum {
    return Enums.DataFile.DataCategoryAndType.DataTypeEnum;
  }

  get modeDepositTabEnum(): typeof ModeDepositTabEnum {
    return ModeDepositTabEnum;
  }

  get depositEditModeEnum(): typeof DepositEditModeEnum {
    return DepositEditModeEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  @Input()
  mode: ModeDepositTabEnum;

  @Input()
  editMode: DepositEditModeEnum = DepositEditModeEnum.none;

  get getDefaultMainDataCategory(): Enums.DataFile.DataCategoryAndType.DataCategoryEnum | undefined {
    const systemProperty = MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState<SystemProperty>, state => state.current);
    if (systemProperty.researchDataSupported) {
      return Enums.DataFile.DataCategoryAndType.DataCategoryEnum.PRIMARY;
    }
    if (systemProperty.administrativeDataSupported) {
      return Enums.DataFile.DataCategoryAndType.DataCategoryEnum.ADMINISTRATIVE;
    }
    return undefined;
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.deposit, _injector, depositActionNameSpace);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._getCurrentModelOnParent();
  }

  protected _getSubResourceWithParentId(id: string): void {
  }

  private _getCurrentModelOnParent(): void {
    this._resId = this._route.snapshot.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  upload($event: DlcmFileUploadWrapper): void {
    this._store.dispatch(new DepositUploadAction.UploadFile(this._resId, $event));
  }

  uploadArchive($event: DlcmFileUploadWrapper): void {
    this._store.dispatch(new DepositUploadAction.UploadFile(this._resId, $event, true));
  }

  retry($event: UploadFileStatus<DepositDataFile>): void {
    this._store.dispatch(new DepositUploadAction.RetrySendFile(this._resId, $event));
  }

  cancel($event: UploadFileStatus<DepositDataFile>): void {
    this._store.dispatch(new DepositUploadAction.MarkAsCancelFileSending(this._resId, $event));
  }

  resume($event: DepositDataFile): void {
    this._store.dispatch(new DepositDataFileAction.Resume(this._resId, $event));
  }

  refresh(): void {
    this._store.dispatch(new DepositDataFileAction.Refresh(this._resId));
  }

  download($event: DepositDataFile): void {
    this._store.dispatch(new DepositDataFileAction.Download(this._resId, $event));
  }

  openModalUploadUpdatedFile(files?: File[], dataCategoryEnum?: Enums.DataFile.DataCategoryAndType.DataCategoryEnum, dataTypeEnum?: Enums.DataFile.DataCategoryAndType.DataTypeEnum, subDirectoryReadOnly?: boolean): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositFileUploadDialog, {
        files: files,
        subDirectory: MemoizedUtil.selectSnapshot(this._store, DepositDataFileState, state => state.currentFolder),
        subDirectoryReadOnly: subDirectoryReadOnly,
        dataCategoryEnum: dataCategoryEnum,
        dataCategoryEnumReadOnly: true,
        dataTypeEnum: dataTypeEnum,
        dataTypeEnumReadOnly: false,
        enableValuesFromEnum: [Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_THUMBNAIL, Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_README, Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_DATA_USE_AGREEMENT],
      }, {
        width: "500px",
        disableClose: true,
      },
      listFilesUploadWrapper => {
        if (isNonEmptyArray(listFilesUploadWrapper)) {
          listFilesUploadWrapper.forEach(f => this.upload(f));
        }
      }));
  }

  openModalUpload(files?: File[], dataCategoryEnum?: Enums.DataFile.DataCategoryAndType.DataCategoryEnum, dataTypeEnum?: Enums.DataFile.DataCategoryAndType.DataTypeEnum, subDirectoryReadOnly?: boolean, dataCategoryEnumReadOnly?: boolean, dataTypeEnumReadOnly?: boolean): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositFileUploadDialog, {
        files: files,
        subDirectory: MemoizedUtil.selectSnapshot(this._store, DepositDataFileState, state => state.currentFolder),
        subDirectoryReadOnly: subDirectoryReadOnly,
        dataCategoryEnum: dataCategoryEnum,
        dataCategoryEnumReadOnly: dataCategoryEnumReadOnly,
        dataTypeEnum: dataTypeEnum,
        dataTypeEnumReadOnly: dataTypeEnumReadOnly,
      }, {
        width: "500px",
        disableClose: true,
      },
      listFilesUploadWrapper => {
        if (isNonEmptyArray(listFilesUploadWrapper)) {
          listFilesUploadWrapper.forEach(f => this.upload(f));
        }
      }));
  }

  openModalUploadArchive(files?: File[], dataCategoryEnum?: Enums.DataFile.DataCategoryAndType.DataCategoryEnum): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositFileUploadArchiveDialog, {
        files: files,
        dataCategoryEnum: dataCategoryEnum,
      }, {
        width: "500px",
      },
      fileUploadWrapper => {
        this.uploadArchive(fileUploadWrapper);
      }));
  }

  openModalAip(): void {
    const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
    const selected = MemoizedUtil.selectedSnapshot(this._store, DepositCollectionState);
    this.subscribe(DialogUtil.open(this._dialog, DepositAssociateArchiveDialog,
      {
        listIdArchiveAlreadyLinked: selected.map(aip => aip.resId),
        dataSensitivity: deposit.dataSensitivity,
        organizationalUnitId: deposit.organizationalUnitId,
      } as DepositAssociateArchiveDialogData,
      {
        width: "90%",
      },
      listAipIds => {
        if (isNonEmptyArray(listAipIds)) {
          this.addAip(listAipIds);
        }
      }),
    );
  }

  addAip(aipIds: string[]): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new DepositCollectionAction.Create(this._resId, aipIds),
      DepositCollectionAction.CreateSuccess,
      resultAction => {
        this._store.dispatch(new DepositCollectionAction.Refresh(this._resId));
      }));
  }
}
