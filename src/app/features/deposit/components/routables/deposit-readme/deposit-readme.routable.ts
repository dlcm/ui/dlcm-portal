/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-readme.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
  OnInit,
  Signal,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {ReadmeWrapper} from "@app/features/deposit/models/readme-wrapper.model";
import {DepositDataFileAction} from "@app/features/deposit/stores/data-file/deposit-data-file.action";
import {DepositDataFile} from "@app/models";
import {ApiActionNameEnum} from "@app/shared/enums/api-action-name.enum";

import {ApiResourceNameEnum} from "@app/shared/enums/api-resource-name.enum";
import {IconNameEnum} from "@app/shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@app/shared/enums/label-translate.enum";
import {DepositFileUploadDialog} from "@deposit/components/dialogs/deposit-file-upload/deposit-file-upload.dialog";
import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {ModeDepositTabEnum} from "@deposit/enums/mode-deposit-tab.enum";
import {DlcmFileUploadWrapper} from "@deposit/models/dlcm-file-upload-wrapper.model";
import {DepositService} from "@deposit/services/deposit.service";
import {
  DepositAction,
  depositActionNameSpace,
} from "@deposit/stores/deposit.action";
import {
  DepositState,
  DepositStateModel,
} from "@deposit/stores/deposit.state";
import {DepositUploadAction} from "@deposit/stores/upload/deposit-upload.action";
import {Enums} from "@enums";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {LocalStateModel} from "@shared/models/local-state.model";
import {SecurityService} from "@shared/services/security.service";
import {
  Observable,
  of,
} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditRoutable,
  AbstractFileVisualizer,
  ButtonColorEnum,
  ConfirmDialog,
  DialogUtil,
  FileInput,
  FileVisualizerHelper,
  FileVisualizerService,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTrue,
  LABEL_TRANSLATE,
  LabelTranslateInterface,
  MARK_AS_TRANSLATABLE,
  MarkdownFileVisualizerService,
  MemoizedUtil,
  ModelFormControlEvent,
  RouterExtensionService,
  StoreUtil,
  TextFileVisualizerService,
} from "solidify-frontend";
import {DepositReadmeFormPresentational} from "../../presentationals/deposit-readme-form/deposit-readme-form.presentational";

@Component({
  selector: "dlcm-deposit-readme-routable",
  templateUrl: "./deposit-readme.routable.html",
  styleUrls: ["./deposit-readme.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositReadmeRoutable extends AbstractDetailEditRoutable<ReadmeWrapper, DepositStateModel> implements OnInit {
  isLoadingCounter: number = 0;
  readonly KEY_PARAM_NAME: keyof ReadmeWrapper;

  private _orgUnitResId: string;

  isTextFile: boolean = false;

  private _isReadyToBeDisplayed: boolean;

  set isReadyToBeDisplayed(value: boolean) {
    if (isTrue(value)) {
      this._registerFormInDepositService();
    }
    this._isReadyToBeDisplayed = value;
  }

  get isReadyToBeDisplayed(): boolean {
    return this._isReadyToBeDisplayed;
  }

  get isLoading(): boolean {
    return this.isLoadingCounter > 0;
  }

  isLoading$: Signal<boolean> = MemoizedUtil.isLoadingSignal(this._store, DepositState);

  isInDetailMode: boolean = false;

  canEditObs: Observable<DepositEditModeEnum> = MemoizedUtil.select(this._store, DepositState, state => state.canEdit);
  depositModeTabEnumObs: Observable<ModeDepositTabEnum> = MemoizedUtil.select(this._store, DepositState, state => state.depositModeTabEnum);

  get depositStatusEnum(): typeof Enums.Deposit.StatusEnum {
    return Enums.Deposit.StatusEnum;
  }

  get depositEditModeEnum(): typeof DepositEditModeEnum {
    return DepositEditModeEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  @ViewChild("formPresentational")
  readonly formPresentational: DepositReadmeFormPresentational;

  model: ReadmeWrapper;
  readmeDataFile: DepositDataFile;

  downloadUrl: string;
  fileInput: FileInput;
  visualizer: AbstractFileVisualizer;
  visualizerErrorMessage: string;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService,
              protected readonly _routerExt: RouterExtensionService,
              private readonly _depositService: DepositService,
              private readonly _fileVisualizerService: FileVisualizerService,
              @Inject(LABEL_TRANSLATE) private readonly _labelTranslateInterface: LabelTranslateInterface) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.deposit, _injector, depositActionNameSpace);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this._retrieveResIdFromUrl();
    this._getSubResourceWithParentId(this._resId);
    this._retrieveCurrentReadme();

    this.subscribe(this._store.select((s: LocalStateModel) => s.router.state.url)
      .pipe(
        distinctUntilChanged(),
        tap(url => {
          this.isInDetailMode = !url.endsWith(DepositRoutesEnum.files);
          this._changeDetector.detectChanges();
        }),
      ));
  }

  private _retrieveCurrentReadme(): void {
    this.isLoadingCounter++;
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new DepositAction.GetReadmeDataFile(this._resId),
      DepositAction.GetReadmeDataFileSuccess,
      resultReadmeDataFile => {
        this.readmeDataFile = resultReadmeDataFile.depositDataFile;
        if (isNullOrUndefined(this.readmeDataFile)) {
          this.isLoadingCounter--;
          this.isReadyToBeDisplayed = true;
          this.isTextFile = true;
          this._changeDetector.detectChanges();
          return;
        }
        if (![
          Enums.DataFile.StatusEnum.READY,
          Enums.DataFile.StatusEnum.IN_ERROR,
          Enums.DataFile.StatusEnum.IGNORED_FILE,
          Enums.DataFile.StatusEnum.CLEANED,
        ].includes(this.readmeDataFile.status)) {
          setTimeout(() => {
            this.isLoadingCounter--;
            this._retrieveCurrentReadme();
          }, 1000);
          return;
        }
        this.isLoadingCounter--;

        this.downloadUrl = `${ApiEnum.preIngestDeposits}/${this.readmeDataFile.infoPackage.resId}/${ApiResourceNameEnum.DATAFILE}/${this.readmeDataFile.resId}/${ApiActionNameEnum.DOWNLOAD}`;
        this.fileInput = {
          dataFile: {
            fileName: this.readmeDataFile.fileName,
            mimetype: this.readmeDataFile.fileFormat?.contentType,
            fileSize: this.readmeDataFile.fileSize,
            puid: this.readmeDataFile.fileFormat?.puid,
            status: this.readmeDataFile.status,
          },
        };
        this.visualizer = FileVisualizerHelper.getFileVisualizerThatCanHandle(this._fileVisualizerService.listFileVisualizer, this.fileInput);
        this.visualizerErrorMessage = FileVisualizerHelper.errorMessage(this._fileVisualizerService.listFileVisualizer, this.fileInput, this._labelTranslateInterface);
        this.isTextFile = this.visualizer instanceof TextFileVisualizerService || this.visualizer instanceof MarkdownFileVisualizerService;
        if (this.isTextFile) {
          this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new DepositAction.DownloadReadme(this._resId, this.readmeDataFile),
            DepositAction.DownloadReadmeSuccess,
            resultDownload => {
              try {
                const reader = new FileReader();
                reader.onloadend = (evt) => {
                  if (evt.target.readyState === FileReader.DONE) {
                    const arrayBuffer = evt.target.result;
                    const decoder = new TextDecoder();
                    this.model = {
                      resId: this.readmeDataFile.resId,
                      text: decoder.decode(arrayBuffer as any),
                    };
                    this.isReadyToBeDisplayed = true;
                    this._changeDetector.detectChanges();
                  }
                };
                reader.readAsArrayBuffer(resultDownload.blob);
              } catch (e) {

              }
            },
            DepositAction.DownloadReadmeFail,
            resultFail => {
              this.isReadyToBeDisplayed = true;
              this._changeDetector.detectChanges();
            }));
        } else {
          this.isReadyToBeDisplayed = true;
          this._changeDetector.detectChanges();
        }
      }),
    );
  }

  protected _getSubResourceWithParentId(id: string): void {
  }

  private _registerFormInDepositService(): void {
    setTimeout(() => {
      this._depositService.depositReadmeFormPresentational = this.formPresentational;
    }, 0);
  }

  update($event: ModelFormControlEvent<ReadmeWrapper>): Observable<any> {
    super.saveInProgress();
    const file = new Blob([$event.model.text], {type: "text/plain"}) as File;
    this._removePreviousAndUploadFile(file);
    return of(true);
  }

  private _removePreviousAndUploadFile(file: File): void {
    if (isNotNullNorUndefined(this.readmeDataFile)) {
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new DepositDataFileAction.Delete(this._resId, this.readmeDataFile.resId),
        DepositDataFileAction.DeleteSuccess,
        result => {
          this._uploadFile(file);
          this._changeDetector.detectChanges();
        }));
    } else {
      this._uploadFile(file);
    }
  }

  private _uploadFile(file: File): void {
    this.isLoadingCounter++;
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new DepositUploadAction.UploadFile(this._resId, {
        dataCategory: Enums.DataFile.DataCategoryAndType.DataCategoryEnum.INTERNAL,
        dataType: Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_README,
        metadataType: undefined,
        subDirectory: undefined,
        file: file,
      } as DlcmFileUploadWrapper),
      DepositUploadAction.UploadFileSuccess,
      result => {
        this.isLoadingCounter--;
        this._refreshCurrentPage();
        this._changeDetector.detectChanges();
      },
      DepositUploadAction.UploadFileFail,
      result => {
        this.isLoadingCounter--;
        this._refreshCurrentPage();
        this._changeDetector.detectChanges();
      }));
  }

  private _refreshCurrentPage(): void {
    this._routerExt.navigate([RoutesEnum.deposit, this._orgUnitResId, DepositRoutesEnum.detail, this._resId, DepositRoutesEnum.readme], {}, true);
  }

  switchToNewFile(): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.file.readme.dialog.switchToNewFile.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.file.readme.dialog.switchToNewFile.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
    }, undefined, (isConfirmed: boolean) => {
      if (isTrue(isConfirmed)) {
        this._store.dispatch(new DepositDataFileAction.Delete(this._resId, this.readmeDataFile.resId));
        this.isTextFile = true;
        this.readmeDataFile = undefined;
        this.edit();
        this._registerFormInDepositService();
      }
    }));
  }

  override delete(): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.file.readme.dialog.delete.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.file.readme.dialog.delete.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
    }, undefined, (isConfirmed: boolean) => {
      if (isTrue(isConfirmed)) {
        this._store.dispatch(new DepositDataFileAction.Delete(this._resId, this.readmeDataFile.resId));
        this.isTextFile = true;
        this.readmeDataFile = undefined;
        this._refreshCurrentPage();
      }
    }));
  }

  uploadFile(files?: File[]): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositFileUploadDialog, {
        files: files,
        subDirectory: "/",
        subDirectoryReadOnly: true,
        dataCategoryEnum: Enums.DataFile.DataCategoryAndType.DataCategoryEnum.INTERNAL,
        dataCategoryEnumReadOnly: true,
        dataTypeEnum: Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_README,
        dataTypeEnumReadOnly: true,
        onlyOneFile: true,
      }, {
        width: "500px",
        disableClose: true,
      },
      listFilesUploadWrapper => {
        if (listFilesUploadWrapper?.length === 1) {
          this._removePreviousAndUploadFile(listFilesUploadWrapper[0].file);
        }
      }));
  }

  downloadFile(): void {
    this._store.dispatch(new DepositDataFileAction.Download(this._resId, this.readmeDataFile));
  }

  protected override _retrieveResIdFromUrl(): void {
    this._orgUnitResId = this._route.parent.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdOrgUnitWithoutPrefixParam);
    this._resId = this._route.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  override edit(): void {
    if (this.isEdit) {
      return;
    }
    this._store.dispatch(new Navigate([RoutesEnum.deposit, this._orgUnitResId, DepositRoutesEnum.detail, this._resId, DepositRoutesEnum.readme, DepositRoutesEnum.edit], {}, {skipLocationChange: true}));
  }
}


