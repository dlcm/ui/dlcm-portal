/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-file-detail.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {DepositFileChangeDataCategoryDialog} from "@deposit/components/dialogs/deposit-file-change-data-category/deposit-file-change-data-category.dialog";
import {DepositFileMoveDialog} from "@deposit/components/dialogs/deposit-file-move/deposit-file-move.dialog";
import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {
  DepositDataFileAction,
  depositDataFileActionNameSpace,
} from "@deposit/stores/data-file/deposit-data-file.action";
import {DepositDataFileState} from "@deposit/stores/data-file/deposit-data-file.state";
import {depositDataFileStatusHistoryNamespace} from "@deposit/stores/data-file/status-history/deposit-data-file-status-history.action";
import {DepositDataFileStatusHistoryState} from "@deposit/stores/data-file/status-history/deposit-data-file-status-history.state";
import {DepositState} from "@deposit/stores/deposit.state";
import {Enums} from "@enums";
import {DepositDataFile} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractFileAipDetailRoutable} from "@shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  DialogUtil,
  ExtraButtonToolbar,
  isTrue,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-file-detail-routable",
  templateUrl: "./deposit-file-detail.routable.html",
  styleUrls: ["../../../../../shared/components/routables/shared-abstract-file-aip-detail/shared-abstract-file-aip-detail.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFileDetailRoutable extends SharedAbstractFileAipDetailRoutable<DepositDataFile> implements OnInit {
  canEditObs: Observable<DepositEditModeEnum> = MemoizedUtil.select(this._store, DepositState, state => state.canEdit);

  override _extraActions: ExtraButtonToolbar<DepositDataFile>[] = [
    {
      labelToTranslate: (current) => LabelTranslateEnum.doNotIgnore,
      color: "primary",
      icon: IconNameEnum.notIgnore,
      callback: current => this.validate(),
      displayCondition: current => current.status === Enums.DataFile.StatusEnum.IGNORED_FILE,
      order: 54,
    },
  ];

  get depositEditModeEnum(): typeof DepositEditModeEnum {
    return DepositEditModeEnum;
  }

  get dataCategoryEnum(): typeof Enums.DataFile.DataCategoryAndType.DataCategoryEnum {
    return Enums.DataFile.DataCategoryAndType.DataCategoryEnum;
  }

  get dataTypeEnum(): typeof Enums.DataFile.DataCategoryAndType.DataTypeEnum {
    return Enums.DataFile.DataCategoryAndType.DataTypeEnum;
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _storeDialogService: StoreDialogService,
              protected readonly _securityService: SecurityService) {
    super(_store,
      _route,
      _actions$,
      _dialog,
      _changeDetector,
      DepositDataFileState,
      depositDataFileActionNameSpace,
      DepositDataFileStatusHistoryState,
      depositDataFileStatusHistoryNamespace,
      _storeDialogService,
      _securityService);
  }

  changeDataCategoryWithDialog(): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositFileChangeDataCategoryDialog, {
        parentResId: this._parentId,
        depositDataFile: this.data.dataFile,
      }, undefined,
      result => {
        if (isTrue(result)) {
          this._getOrRefreshResource();
        }
      }));
  }

  moveDataFileWithDialog(): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositFileMoveDialog, {
        parentResId: this._parentId,
        depositDataFile: this.data.dataFile,
      }, undefined,
      result => {
        if (isTrue(result)) {
          this._getOrRefreshResource();
        }
      }));
  }

  validate(): void {
    this._store.dispatch(new DepositDataFileAction.Validate(this._parentId, this.data.dataFile));
  }
}
