/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {MatTabGroup} from "@angular/material/tabs";
import {
  ActivatedRoute,
  Router,
} from "@angular/router";
import {
  DepositAction,
  depositActionNameSpace,
} from "@app/features/deposit/stores/deposit.action";
import {
  DepositState,
  DepositStateModel,
} from "@app/features/deposit/stores/deposit.state";
import {DepositSipState} from "@app/features/deposit/stores/sip/deposit-sip.state";
import {AppState} from "@app/stores/app.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {DepositRejectDialog} from "@deposit/components/dialogs/deposit-reject/deposit-reject.dialog";
import {DepositSubmitDialog} from "@deposit/components/dialogs/deposit-submit/deposit-submit.dialog";
import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {ModeDepositTabEnum} from "@deposit/enums/mode-deposit-tab.enum";
import {DataFileUploadHelper} from "@deposit/helpers/data-file-upload.helper";
import {
  DepositHelper,
  DepositMode,
} from "@deposit/helpers/deposit.helper";
import {DepositService} from "@deposit/services/deposit.service";
import {DepositCollectionAction} from "@deposit/stores/collection/deposit-collection.action";
import {DepositCollectionState} from "@deposit/stores/collection/deposit-collection.state";
import {DepositDataFileAction} from "@deposit/stores/data-file/deposit-data-file.action";
import {DepositDataFileState} from "@deposit/stores/data-file/deposit-data-file.state";
import {DepositDuaDataFileAction} from "@deposit/stores/dua-data-file/deposit-dua-data-file.action";
import {DepositPersonAction} from "@deposit/stores/people/deposit-person.action";
import {DepositSipAction} from "@deposit/stores/sip/deposit-sip.action";
import {DepositStatusHistoryAction} from "@deposit/stores/status-history/deposit-status-history.action";
import {DepositStatusHistoryState} from "@deposit/stores/status-history/deposit-status-history.state";
import {DepositSubjectAreaAction} from "@deposit/stores/subject-area/deposit-subject-area.action";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {Deposit} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedCitationDialog} from "@shared/components/dialogs/shared-citation/shared-citation.dialog";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  BehaviorSubject,
  combineLatest,
  Observable,
  Subscription,
} from "rxjs";

import {
  distinctUntilChanged,
  filter,
  map,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditRoutable,
  AppBannerAction,
  AppBannerState,
  BannerColorEnum,
  ButtonColorEnum,
  ClipboardUtil,
  ConfirmDialog,
  DialogUtil,
  ExtraButtonToolbar,
  isFalse,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  MappingObject,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ObservableUtil,
  PollingHelper,
  QueryParameters,
  RouterExtensionService,
  SolidifyObject,
  SsrUtil,
  StatusHistory,
  StatusHistoryDialog,
  StoreUtil,
  StringUtil,
  Tab,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-detail-edit-routable",
  templateUrl: "./deposit-detail-edit.routable.html",
  styleUrls: ["./deposit-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositDetailEditRoutable extends AbstractDetailEditRoutable<Deposit, DepositStateModel> implements OnInit, OnDestroy {
  historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, DepositStatusHistoryState, state => state.history);
  isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositStatusHistoryState);
  queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, DepositStatusHistoryState, state => state.queryParameters);
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositState);
  listCurrentStatusObs: Observable<MappingObject<Enums.DataFile.StatusEnum, number>> = MemoizedUtil.select(this._store, DepositDataFileState, state => state.listCurrentStatus);
  numberCollectionObs: Observable<number | undefined> = MemoizedUtil.select(this._store, DepositCollectionState, state => state.numberCollections);
  depositModeTabEnumObs: Observable<ModeDepositTabEnum> = MemoizedUtil.select(this._store, DepositState, state => state.depositModeTabEnum);
  anonymousReviewUrlObs: Observable<string> = MemoizedUtil.select(this._store, DepositState, state => state.anonymousReviewUrl);

  mode: DepositMode;

  private _subscriptionCanSubmitAndComputeBannerMessage: Subscription;

  @ViewChild("matTabGroup")
  readonly matTabGroup: MatTabGroup;

  private get _rootUrl(): string[] {
    return [AppRoutesEnum.deposit, this._orgUnitResId, DepositRoutesEnum.detail, this._resId];
  }

  get modeDepositTabEnum(): typeof ModeDepositTabEnum {
    return ModeDepositTabEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get depositEditModeEnum(): typeof DepositEditModeEnum {
    return DepositEditModeEnum;
  }

  listTabs: Tab[] = [
    {
      id: TabEnum.METADATA,
      suffixUrl: DepositRoutesEnum.metadata,
      icon: IconNameEnum.metadata,
      titleToTranslate: LabelTranslateEnum.metadata,
      route: () => [...this._rootUrl, DepositRoutesEnum.metadata],
    },
    {
      id: TabEnum.DATA,
      suffixUrl: DepositRoutesEnum.data,
      icon: IconNameEnum.files,
      titleToTranslate: LabelTranslateEnum.data,
      route: () => [...this._rootUrl, DepositRoutesEnum.data],
      conditionDisplay: () => MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isInTourMode) || this.depositModeTabEnumObs.pipe(map(mode => mode === ModeDepositTabEnum.FILE_OR_COLLECTION)),
    },
    {
      id: TabEnum.FILES,
      suffixUrl: DepositRoutesEnum.files,
      icon: IconNameEnum.files,
      titleToTranslate: LabelTranslateEnum.files,
      route: () => [...this._rootUrl, DepositRoutesEnum.files],
      conditionDisplay: () => combineLatest([
        this.depositModeTabEnumObs.pipe(map(mode => mode === ModeDepositTabEnum.FILE)),
        MemoizedUtil.select(this._store, DepositState, state => state?.current?.dataFileNumber > 0 || isNotNullNorUndefined(state?.file)),
      ]).pipe(
        distinctUntilChanged(),
        map(([isFileMode, isFilePresent]) => isFileMode || isFilePresent),
      ),
    },
    {
      id: TabEnum.COLLECTIONS,
      suffixUrl: DepositRoutesEnum.collections,
      icon: IconNameEnum.files,
      titleToTranslate: LabelTranslateEnum.collections,
      route: () => [...this._rootUrl, DepositRoutesEnum.collections],
      conditionDisplay: () => this.depositModeTabEnumObs.pipe(map(mode => mode === ModeDepositTabEnum.COLLECTION)),
    },
    {
      id: TabEnum.README,
      suffixUrl: DepositRoutesEnum.readme,
      icon: IconNameEnum.readme,
      titleToTranslate: LabelTranslateEnum.readme,
      route: () => [...this._rootUrl, DepositRoutesEnum.readme],
      conditionDisplay: () => combineLatest([
        this.currentObs.pipe(map(current => current?.status)),
        MemoizedUtil.select(this._store, DepositState, state => state?.readmeDataFile),
      ]).pipe(
        distinctUntilChanged(),
        map(([status, isReadmePresent]) => ([Enums.Deposit.StatusEnum.IN_PROGRESS, Enums.Deposit.StatusEnum.EDITING_METADATA].includes(status) || isReadmePresent) && status !== Enums.Deposit.StatusEnum.CLEANED),
      ),
    },
  ];

  private _currentTab: Tab;

  messageParametersUnableSubmit: SolidifyObject | undefined;
  messageReasonUnableSubmit: string | undefined;

  canSubmitAction: boolean = false;
  canReserveDoiAction: boolean = false;

  canDoAlterationActionsBS: BehaviorSubject<DepositEditModeEnum | undefined> = new BehaviorSubject(DepositEditModeEnum.none);
  readonly canDoAlterationActionsObs: Observable<DepositEditModeEnum | undefined> = ObservableUtil.asObservable(this.canDoAlterationActionsBS);

  get canDoAlterationActions(): DepositEditModeEnum {
    return this.canDoAlterationActionsBS.value;
  }

  canDoValidationActionsBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject(false);
  readonly canDoValidationActionsObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this.canDoValidationActionsBS);

  get canDoValidationActions(): boolean {
    return this.canDoValidationActionsBS.value;
  }

  private _orgUnitResId: string | undefined = undefined;

  readonly KEY_PARAM_NAME: keyof Deposit & string = "title";

  private _shouldContinuePolling: boolean = false;

  isTour: boolean = false;
  listExtraButtons: ExtraButtonToolbar<Deposit>[] = [
    {
      color: "primary",
      typeButton: "flat-button",
      icon: IconNameEnum.save,
      labelToTranslate: (current) => LabelTranslateEnum.save,
      order: 40,
      callback: () => this.depositService?.formPresentational?.onSubmit(),
      displayCondition: (resource) => this._currentTab.id === TabEnum.METADATA && this.isEdit,
      disableCondition: () => isNullOrUndefined(this.depositService?.formPresentational) || this.depositService?.formPresentational?.form?.pristine || this.depositService?.formPresentational?.form?.invalid || this.depositService?.formPresentational?.formFormly?.invalid,
    },
    {
      color: "primary",
      typeButton: "flat-button",
      icon: IconNameEnum.save,
      labelToTranslate: (current) => LabelTranslateEnum.save,
      order: 40,
      callback: () => this.depositService?.depositReadmeFormPresentational?.onSubmit(),
      displayCondition: (resource) => this._currentTab.id === TabEnum.README && this.isEdit,
      disableCondition: () => isNullOrUndefined(this.depositService?.depositReadmeFormPresentational) || this.depositService?.depositReadmeFormPresentational?.form?.pristine || this.depositService?.depositReadmeFormPresentational?.form?.invalid,
    },
    {
      color: "primary",
      icon: IconNameEnum.download,
      displayCondition: current => isNotNullNorUndefined(current?.status) && current.status === Enums.Deposit.StatusEnum.COMPLETED,
      callback: () => this._download(),
      labelToTranslate: (current) => LabelTranslateEnum.download,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.checkCompliance,
      displayCondition: current => isNotNullNorUndefined(current?.status) && current.status === Enums.Deposit.StatusEnum.COMPLETED && this._securityService.isRoot(),
      callback: () => this._checkCompliance(),
      labelToTranslate: (current) => LabelTranslateEnum.checkCompliance,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.edit,
      displayCondition: current => isNotNullNorUndefined(current?.status)
        && [Enums.Deposit.StatusEnum.COMPLETED, Enums.Deposit.StatusEnum.CLEANED, Enums.Deposit.StatusEnum.EDITING_METADATA_REJECTED].includes(current.status)
        && (this._securityService.isRoot()
          || (this._securityService.isManagerOfOrgUnit(current.organizationalUnitId) || this._securityService.isStewardOfOrgUnit(current.organizationalUnitId))),
      callback: current => this._startMetadataEditing(current),
      labelToTranslate: current => LabelTranslateEnum.enableDepositEdition,
      order: 41,
    },
    {
      color: "primary",
      icon: IconNameEnum.abort,
      displayCondition: current => isNotNullNorUndefined(current?.status)
        && [Enums.Deposit.StatusEnum.EDITING_METADATA].includes(current.status)
        && !this.isEdit
        && (this._securityService.isRoot()
          || (this._securityService.isManagerOfOrgUnit(current.organizationalUnitId) || this._securityService.isStewardOfOrgUnit(current.organizationalUnitId))),
      callback: current => this._cancelMetadataEditing(current),
      labelToTranslate: (current) => LabelTranslateEnum.cancelDepositEdition,
      order: 41,
    },
    {
      color: "primary",
      icon: IconNameEnum.edit,
      displayCondition: current => !this.isEdit && isNotNullNorUndefined(current?.status)
        && [Enums.Deposit.StatusEnum.IN_VALIDATION, Enums.Deposit.StatusEnum.REJECTED, Enums.Deposit.StatusEnum.IN_ERROR].includes(current.status),
      callback: () => this._backToRevision(),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.returnToEditing,
      order: 21,
    },
    {
      color: "primary",
      icon: IconNameEnum.reserveDoi,
      displayCondition: current => this._displayReserveDOIButton(),
      callback: () => this._reserveDOI(),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.reserveDoi,
      tooltipToTranslate: MARK_AS_TRANSLATABLE("deposit.tooltips.doi"),
      order: 50,
    },
    {
      color: "primary",
      icon: IconNameEnum.anonymousReview,
      displayCondition: current => current?.status === Enums.Deposit.StatusEnum.IN_PROGRESS,
      callback: () => this._navigateToAnonymizedDepositPage(),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.anonymousReview,
      tooltipToTranslate: MARK_AS_TRANSLATABLE("deposit.tooltips.anonymousReview"),
      order: 50,
    },
    {
      color: "primary",
      icon: IconNameEnum.copyToClipboard,
      displayCondition: current => this.anonymousReviewUrlObs.pipe(map(anonymousReviewUrl => isNotNullNorUndefinedNorWhiteString(anonymousReviewUrl) && current?.status === Enums.Deposit.StatusEnum.IN_PROGRESS)),
      callback: () => this._copyAnonymizedDepositPage(),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.copyAnonymousReviewLink,
      order: 50,
    },
    {
      color: "primary",
      icon: IconNameEnum.regenerateAnonymousReview,
      displayCondition: current => this.anonymousReviewUrlObs.pipe(map(anonymousReviewUrl => isNotNullNorUndefinedNorWhiteString(anonymousReviewUrl) && current?.status === Enums.Deposit.StatusEnum.IN_PROGRESS)),
      callback: () => this._generateAnonymizedDepositPage(),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.regenerateAnonymousReview,
      tooltipToTranslate: MARK_AS_TRANSLATABLE("deposit.tooltips.regenerateAnonymousReview"),
      order: 50,
    },
    {
      color: "primary",
      icon: IconNameEnum.archive,
      displayCondition: current => this._isArchivePublishedObs(),
      navigate: current => new Navigate([`${RoutesEnum.archives}/${MemoizedUtil.currentSnapshot(this._store, DepositSipState).aipId}`]),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.seeArchive,
      order: 50,
    },
    {
      color: "primary",
      icon: IconNameEnum.submit,
      displayCondition: current => this._displaySubmitButton(),
      callback: () => this._submit(),
      disableCondition: current => isNullOrUndefined(current) || !this.canSubmitAction,
      labelToTranslate: (current) => LabelTranslateEnum.submit,
      typeButton: "flat-button",
      order: 51,
    },
    {
      color: "success",
      icon: IconNameEnum.approve,
      displayCondition: current => !this.isEdit && this.canDoValidationActions,
      callback: () => this._approve(),
      disableCondition: current => isNullOrUndefined(current) || (this._isDepositOfCurrentUser(current) && !this._securityService.isRootOrAdmin()),
      labelToTranslate: (current) => LabelTranslateEnum.approve,
      typeButton: "flat-button",
      order: 52,
    },
    {
      color: "warn",
      icon: IconNameEnum.unapprove,
      displayCondition: current => !this.isEdit && this.canDoValidationActions,
      callback: () => this._reject(),
      disableCondition: current => isNullOrUndefined(current) || (this._isDepositOfCurrentUser(current) && !this._securityService.isRootOrAdmin()),
      labelToTranslate: (current) => LabelTranslateEnum.reject,
      typeButton: "flat-button",
      order: 53,
    },
    {
      color: ButtonColorEnum.primary,
      icon: IconNameEnum.putInError,
      displayCondition: current => isNotNullNorUndefined(current)
        && this._securityService.isRoot()
        && current.status !== Enums.Package.StatusEnum.IN_ERROR
        && current.status !== Enums.Package.StatusEnum.COMPLETED,
      callback: () => this._putInError(),
      labelToTranslate: (current) => LabelTranslateEnum.putInError,
      order: 40,
    },
  ];

  smallButtons: ExtraButtonToolbar<any>[] = [
    {
      displayCondition: current => this._isArchivePublishedObs().pipe(
        map(isArchivePublished => !this.isEdit && isNotNullNorUndefined(current?.status)
          && [Enums.Deposit.StatusEnum.COMPLETED, Enums.Deposit.StatusEnum.CLEANED].includes(current.status) && isArchivePublished)),
      icon: IconNameEnum.quoteRight,
      callback: () => this.showCitationDialog(),
      labelToTranslate: () => LabelTranslateEnum.citeDepositInAPublication,
      color: ButtonColorEnum.primary,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _router: Router,
              protected readonly _routerExt: RouterExtensionService,
              private readonly _securityService: SecurityService,
              private readonly _notificationService: NotificationService,
              public readonly depositService: DepositService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.deposit, _injector, depositActionNameSpace);
  }

  ngOnInit(): void {
    this.mode = DepositHelper.determineMode(this._store, this._routerExt);
    super.ngOnInit();

    if (MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isInTourMode)) {
      this.isTour = true;
      return;
    }

    this.retrieveResource();
    this.retrieveCurrentModelWithUrl();
    this._computeEditableField();
    this._retrieveThumbnail();
    this._computeCurrentUserRight();
    this._createPollingListenerForFileReady();
    this._createPollingListenerCompletedStatus();
    this._createPollingListenerCompletedStatusForArchiveId();
  }

  private _createPollingListenerForFileReady(): void {
    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: environment.refreshDepositSubmittedIntervalInSecond,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: 60,
      stopRefreshAfterMaximumIntervalReached: true,
      resetIntervalWhenUserMouseEvent: true,
      doBeforeFilter: () => {
        const shouldContinuePollingBefore = this._shouldContinuePolling;
        this._computedShouldContinuePolling();
        if (shouldContinuePollingBefore && !this._shouldContinuePolling) {
          this._store.dispatch(new DepositDataFileAction.Refresh(this._resId));
        }
      },
      filter: () => this._shouldContinuePolling,
      actionToDo: () => {
        this._store.dispatch(new DepositDataFileAction.GetListCurrentStatus(this._resId));
      },
    }));
  }

  private _createPollingListenerCompletedStatus(): void {
    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: environment.refreshDepositSubmittedIntervalInSecond,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: 60,
      stopRefreshAfterMaximumIntervalReached: true,
      resetIntervalWhenUserMouseEvent: true,
      filter: () => this._statusShouldContinuePolling(),
      actionToDo: () => {
        let keepCurrentContext = true;
        const status = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.current?.status);
        if (status === Enums.Deposit.StatusEnum.CANCEL_EDITING_METADATA) {
          this._store.dispatch(new DepositPersonAction.GetAll(this._resId));
          this._store.dispatch(new DepositSubjectAreaAction.GetAll(this._resId));
          keepCurrentContext = false;
        }
        this._store.dispatch(new DepositAction.GetById(this._resId, keepCurrentContext));
      },
    }));
  }

  private _statusShouldContinuePolling(): boolean {
    const status = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.current?.status);
    return isNotNullNorUndefined(status) && [
      Enums.Deposit.StatusEnum.SUBMITTED,
      Enums.Deposit.StatusEnum.CHECKED,
      Enums.Deposit.StatusEnum.APPROVED,
      Enums.Deposit.StatusEnum.CANCEL_EDITING_METADATA,
    ].includes(status);
  }

  private _createPollingListenerCompletedStatusForArchiveId(): void {
    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: environment.refreshDepositSubmittedIntervalInSecond,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: 60,
      stopRefreshAfterMaximumIntervalReached: true,
      resetIntervalWhenUserMouseEvent: true,
      filter: () => this._statusShouldContinuePollingForArchiveId(),
      actionToDo: () => {
        const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
        const sip = MemoizedUtil.currentSnapshot(this._store, DepositSipState);
        if (deposit.resId !== this._resId || isNullOrUndefinedOrWhiteString(deposit.sipId) || sip?.depositId === this._resId) {
          return;
        }
        this._store.dispatch(new DepositSipAction.GetById(deposit.sipId));
      },
    }));
  }

  private _statusShouldContinuePollingForArchiveId(): boolean {
    const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
    const sip = MemoizedUtil.currentSnapshot(this._store, DepositSipState);
    return isNotNullNorUndefinedNorWhiteString(deposit?.sipId) && isNullOrUndefined(sip);
  }

  private _computedShouldContinuePolling(): void {
    this._shouldContinuePolling = DataFileUploadHelper.numberFilesPending(this._store) > 0
      && MemoizedUtil.selectSnapshot(this._store, DepositState, state => state?.current?.status !== Enums.Deposit.StatusEnum.CLEANED);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._cleanState();
    this._store.dispatch(new AppBannerAction.Hide());
    this._store.dispatch(new DepositSipAction.Clean());
  }

  protected _retrieveResIdFromUrl(): void {
    this._resId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
    this._orgUnitResId = this._route.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdOrgUnitWithoutPrefixParam);
  }

  private _computeEditableField(): void {
    this.subscribe(this.canDoAlterationActionsObs.pipe(
      distinctUntilChanged(),
      tap(editMode => {
        this._store.dispatch(new DepositAction.ChangeEditProperty(editMode));
      }),
    ));
    this._store.dispatch(new DepositAction.ChangeEditProperty(this.canDoAlterationActions));
  }

  private _retrieveThumbnail(): void {
    this.subscribe(this.currentObs.pipe(
      distinctUntilChanged(),
      filter(deposit => isNotNullNorUndefined(deposit) && deposit.resId === this._resId),
      take(1),
      tap(deposit => {
        this._store.dispatch(new DepositAction.CheckPhoto(deposit.resId, deposit.metadataVersion));
      }),
    ));
  }

  private _computeCurrentUserRight(): void {
    this.subscribe(this.currentObs.pipe(
      distinctUntilChanged(),
      filter(deposit => isNotNullNorUndefined(deposit) && deposit.resId === this._resId),
      tap(deposit => {
        if (isFalse(this.isEdit) && deposit.status === Enums.Deposit.StatusEnum.IN_PROGRESS) {
          this.canReserveDoiAction = isNullOrUndefined(deposit.doi);
        }

        if (deposit.status === Enums.Deposit.StatusEnum.CLEANED) {
          this._computeBannerMessage();
          return;
        }

        this._retrieveAnonymizedDepositPageUrlIfExist(deposit);
        this._computeCanDoValidatorAndAlterationWithCurrentRole(deposit);
        this._computeBannerMessage();
        this._changeDetector.detectChanges();

        this._watchFileAndCollectionToComputedCanSubmitActionAndBanner(deposit);
      }),
    ));
  }

  private _retrieveAnonymizedDepositPageUrlIfExist(deposit: Deposit): void {
    if (isNotNullNorUndefinedNorWhiteString(deposit.anonymizedDepositPageId)) {
      this._store.dispatch(new DepositAction.GetAnonymizedDepositPage(deposit.anonymizedDepositPageId));
    }
  }

  private _computeCanDoValidatorAndAlterationWithCurrentRole(deposit: Deposit): void {
    const canDoValidationActions = this._securityService.canApproveOrRejectDeposit(deposit);
    const canDoAlterations = this._securityService.canEditDeposit(deposit);
    this.canDoValidationActionsBS.next(canDoValidationActions);
    this.canDoAlterationActionsBS.next(canDoAlterations);
  }

  private _watchFileAndCollectionToComputedCanSubmitActionAndBanner(deposit: Deposit): void {
    if (isNotNullNorUndefined(this._subscriptionCanSubmitAndComputeBannerMessage) && isFalse(this._subscriptionCanSubmitAndComputeBannerMessage.closed)) {
      this._subscriptionCanSubmitAndComputeBannerMessage.unsubscribe();
      this._subscriptionCanSubmitAndComputeBannerMessage = null;
    }
    this._subscriptionCanSubmitAndComputeBannerMessage = this.subscribe(combineLatest([this.numberCollectionObs, this.listCurrentStatusObs])
      .pipe(
        distinctUntilChanged(),
        filter(([numberCollections, listCurrentStatus]) =>
          !isNullOrUndefined(numberCollections) &&
          !isNullOrUndefined(listCurrentStatus)),
        tap(([numberCollections, listCurrentStatus]) => {
          let canSubmitAction = deposit.status === Enums.Deposit.StatusEnum.IN_PROGRESS || deposit.status === Enums.Deposit.StatusEnum.EDITING_METADATA;
          this.messageReasonUnableSubmit = undefined;
          this.messageParametersUnableSubmit = undefined;
          if (DataFileUploadHelper.numberFiles(this._store) === 0 && numberCollections === 0) {
            this.messageReasonUnableSubmit = MARK_AS_TRANSLATABLE("deposit.error.noFileOrCollection");
            canSubmitAction = false;
          }
          const numberFilesPending = DataFileUploadHelper.numberFilesPending(this._store);
          const numberFilesIgnored = DataFileUploadHelper.numberFilesIgnored(this._store);
          const numberFilesInError = DataFileUploadHelper.numberFilesInError(this._store);
          const numberFilesExcluded = DataFileUploadHelper.numberFilesExcluded(this._store);

          if (numberFilesPending > 0) {
            this.messageReasonUnableSubmit = MARK_AS_TRANSLATABLE("deposit.error.fileNotReady");
            this.messageParametersUnableSubmit = {files: numberFilesPending};
            canSubmitAction = false;
          } else if (numberFilesIgnored > 0) {
            this.messageReasonUnableSubmit = MARK_AS_TRANSLATABLE("deposit.error.fileIgnoredToTreat");
            this.messageParametersUnableSubmit = {files: numberFilesIgnored};
            canSubmitAction = false;
          } else if (numberFilesExcluded > 0) {
            this.messageReasonUnableSubmit = MARK_AS_TRANSLATABLE("deposit.error.fileExcludedToRemove");
            this.messageParametersUnableSubmit = {files: numberFilesExcluded};
            canSubmitAction = false;
          } else if (numberFilesInError > 0) {
            this.messageReasonUnableSubmit = MARK_AS_TRANSLATABLE("deposit.error.fileInError");
            this.messageParametersUnableSubmit = {files: numberFilesInError};
            canSubmitAction = false;
          }

          this.canSubmitAction = canSubmitAction;
          this._computeBannerMessage();
        }),
      ));
  }

  private _displaySubmitButton(): boolean {
    return !this.isEdit && this.canDoAlterationActions !== DepositEditModeEnum.none && isFalse(MemoizedUtil.isLoadingSnapshot(this._store, DepositState));
  }

  private _displayReserveDOIButton(): boolean {
    return !this.isEdit && this.canDoAlterationActions === DepositEditModeEnum.full && this.canReserveDoiAction;
  }

  private _isArchivePublishedObs(): Observable<boolean> {
    return MemoizedUtil.current(this._store, DepositSipState).pipe(
      map(sip => {
        if (this.isEdit) {
          return false;
        }
        return isNotNullNorUndefinedNorWhiteString(sip?.aipId) && (sip.info?.status === Enums.Package.StatusEnum.COMPLETED || sip.info?.status === Enums.Package.StatusEnum.CLEANED);
      }),
    );
  }

  override edit(): void {
    this._store.dispatch(new Navigate([RoutesEnum.deposit, this._orgUnitResId, DepositRoutesEnum.detail, this._resId, DepositRoutesEnum.metadata, DepositRoutesEnum.edit], {}, {skipLocationChange: true}));
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new DepositSubjectAreaAction.GetAll(id));
    this._store.dispatch(new DepositPersonAction.GetAll(id));
    this._store.dispatch(new DepositDataFileAction.Refresh(id));
    this._store.dispatch(new DepositCollectionAction.Refresh(id));
    this._store.dispatch(new DepositDuaDataFileAction.GetDuaFile(id));
    this._store.dispatch(new DepositAction.GetReadmeDataFile(id));
  }

  private _submit(): void {
    const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);

    const submissionPolicy = deposit.submissionPolicy;

    if (isNullOrUndefinedOrWhiteString(submissionPolicy?.submissionAgreementType)
      || submissionPolicy.submissionAgreementType === Enums.SubmissionAgreementType.SubmissionAgreementTypeEnum.WITHOUT) {
      this._openSubmitConfirmationDialog(deposit, false);
      return;
    }

    if (submissionPolicy.submissionAgreementType === Enums.SubmissionAgreementType.SubmissionAgreementTypeEnum.FOR_EACH_DEPOSIT) {
      this._openSubmitConfirmationDialog(deposit, true);
      return;
    }

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new DepositAction.CheckSubmissionAgreement(deposit.resId),
      DepositAction.CheckSubmissionAgreementSuccess,
      resultAction => {
        this._openSubmitConfirmationDialog(deposit, false);
      },
      DepositAction.CheckSubmissionAgreementFail,
      resultAction => {
        this._openSubmitConfirmationDialog(deposit, resultAction.notFound);
      }),
    );
  }

  private _openSubmitConfirmationDialog(deposit: Deposit, needSubmissionAgreement: boolean): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositSubmitDialog, {
        deposit: deposit,
        needSubmissionAgreement: needSubmissionAgreement,
      }, {
        minWidth: "500px",
        width: isNotNullNorUndefined(deposit.submissionPolicy.submissionAgreement) ? "80%" : undefined,
        takeOne: true,
      },
      () => {
        if (!needSubmissionAgreement) {
          this._store.dispatch(new DepositAction.Submit(deposit));
          return;
        }
        this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
          new DepositAction.ApproveSubmissionAgreement(deposit.resId),
          DepositAction.ApproveSubmissionAgreementSuccess,
          result => {
            this._store.dispatch(new DepositAction.Submit(deposit));
          }),
        );
      }));
  }

  private _reserveDOI(): void {
    this.subscribe(this.currentObs.pipe(
      take(1),
      tap(deposit => this._store.dispatch(new DepositAction.ReserveDOI(deposit))),
    ));
  }

  private _copyAnonymizedDepositPage(): void {
    const anonymousReviewUrl = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.anonymousReviewUrl);
    if (ClipboardUtil.copyStringToClipboard(anonymousReviewUrl)) {
      this._notificationService.showInformation(LabelTranslateEnum.notificationCopiedToClipboard);
    }
  }

  private _navigateToAnonymizedDepositPage(): void {
    const anonymousReviewUrl = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.anonymousReviewUrl);
    if (isNotNullNorUndefinedNorWhiteString(anonymousReviewUrl)) {
      this._openAnonymizedDepositPage();
      return;
    }

    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: MARK_AS_TRANSLATABLE("deposit.dialog.peerReview.title"),
      messageToTranslate: MARK_AS_TRANSLATABLE("deposit.dialog.peerReview.message"),
      confirmButtonToTranslate: LabelTranslateEnum.proceed,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.primary,
    }, undefined, (isConfirmed: boolean) => {
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
        new DepositAction.GenerateAnonymizedDepositPage(this._resId),
        DepositAction.GenerateAnonymizedDepositPageSuccess,
        result => this._openAnonymizedDepositPage()));
    }));
  }

  private _openAnonymizedDepositPage(): void {
    const anonymousReviewUrl = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.anonymousReviewUrl);
    SsrUtil.window.open(anonymousReviewUrl, "_blank");
  }

  private _generateAnonymizedDepositPage(): void {
    this._store.dispatch(new DepositAction.GenerateAnonymizedDepositPage(this._resId));
  }

  showHistory(): void {
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: this._resId,
      name: StringUtil.convertToPascalCase(this._state),
      statusHistory: this.historyObs,
      isLoading: this.isLoadingHistoryObs,
      queryParametersObs: this.queryParametersHistoryObs,
      state: DepositStatusHistoryAction,
      statusEnums: Enums.Deposit.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }

  showCitationDialog(): void {
    const archiveId = MemoizedUtil.currentSnapshot(this._store, DepositSipState)?.aipId;
    DialogUtil.open(this._dialog, SharedCitationDialog, {
      archiveId: archiveId,
      archiveTitle: this.current.title,
    }, {
      width: "800px",
    });
  }

  private _approve(): void {
    this.subscribe(this.currentObs.pipe(
      take(1),
      tap(deposit => this._store.dispatch(new DepositAction.Approve(deposit))),
    ));
  }

  private _reject(): void {
    this.subscribe(this.currentObs.pipe(
      take(1),
      tap(deposit => {
        this.subscribe(DialogUtil.open(this._dialog, DepositRejectDialog, {}, {
            minWidth: "500px",
            takeOne: true,
          },
          message => {
            this._store.dispatch(new DepositAction.Reject(deposit, message));
          }));
      }),
    ));
  }

  private _putInError(): void {
    this._store.dispatch(new DepositAction.PutInError(this._resId));
  }

  private _backToRevision(): void {
    this.subscribe(this.currentObs.pipe(
      take(1),
      tap(deposit => this._store.dispatch(new DepositAction.BackToEdit(deposit))),
    ));
  }

  backToList(): void {
    if (this.mode === DepositMode.DEPOSIT) {
      const depositTabStatus = MemoizedUtil.selectSnapshot(this._store, DepositState, state => state.activeListTabStatus);
      this._store.dispatch(new Navigate([AppRoutesEnum.deposit, this._orgUnitResId, depositTabStatus]));
    } else {
      this._store.dispatch(new Navigate([RoutesEnum.preservationPlanningDeposit]));
    }
  }

  override backToDetail(): void {
    if (this._currentTab.id === TabEnum.METADATA) {
      super.backToDetail([...this._rootUrl, DepositRoutesEnum.metadata]);
    } else if (this._currentTab.id === TabEnum.README) {
      super.backToDetail([...this._rootUrl, DepositRoutesEnum.readme]);
    }
  }

  setCurrentTab($event: Tab): void {
    this._currentTab = $event;
    this._computeBannerMessage();
  }

  private _download(): void {
    this._store.dispatch(new DepositAction.Download(this._resId));
  }

  private _checkCompliance(): void {
    this._store.dispatch(new DepositAction.CheckCompliance(this._resId));
  }

  private _startMetadataEditing(deposit: Deposit): void {
    this._store.dispatch(new DepositAction.StartMetadataEditing(deposit));
  }

  private _cancelMetadataEditing(deposit: Deposit): void {
    this._store.dispatch(new DepositAction.CancelMetadataEditing(deposit));
  }

  private _computeBannerMessage(): void {
    if (this.isEdit) {
      return;
    }
    const bannerInfo = this.getBannerDisplayInfo();
    const bannerStateModel = MemoizedUtil.selectSnapshot(this._store, AppBannerState, state => state);
    if (bannerInfo.display === bannerStateModel.display
      && bannerInfo.color === bannerStateModel.color
      && bannerInfo.message === bannerStateModel.message
      && JSON.stringify(bannerInfo.parameters) === JSON.stringify(bannerStateModel.parameters)
    ) {
      return;
    }
    if (bannerInfo.display) {
      this._store.dispatch(new AppBannerAction.Show(bannerInfo.message, bannerInfo.color, bannerInfo.parameters));
    } else {
      this._store.dispatch(new AppBannerAction.Hide());
    }
  }

  getBannerDisplayInfo(): BannerDisplayInfo {
    const bannerMessage = {} as BannerDisplayInfo;
    bannerMessage.parameters = undefined;
    bannerMessage.display = true;

    const deposit = MemoizedUtil.currentSnapshot(this._store, DepositState);
    const depositStatus = deposit?.status;

    if (depositStatus === Enums.Deposit.StatusEnum.IN_PROGRESS && this.canDoAlterationActions === DepositEditModeEnum.none) {
      bannerMessage.display = false;
      return bannerMessage;
    }

    if (isFalse(this.canSubmitAction) && !isNullOrUndefined(this.messageReasonUnableSubmit)) {
      bannerMessage.message = this.messageReasonUnableSubmit;
      bannerMessage.parameters = this.messageParametersUnableSubmit;
      bannerMessage.color = BannerColorEnum.warn;
      return bannerMessage;
    }

    if (isNullOrUndefined(depositStatus)) {
      bannerMessage.display = false;
      return bannerMessage;
    }

    if (depositStatus === Enums.Deposit.StatusEnum.IN_ERROR) {
      bannerMessage.message = MARK_AS_TRANSLATABLE("deposit.alert.status.error");
      bannerMessage.color = BannerColorEnum.error;
      return bannerMessage;
    }

    if (depositStatus === Enums.Deposit.StatusEnum.REJECTED) {
      bannerMessage.message = MARK_AS_TRANSLATABLE("deposit.alert.status.rejected");
      bannerMessage.color = BannerColorEnum.error;
      return bannerMessage;
    }

    if (depositStatus === Enums.Deposit.StatusEnum.IN_VALIDATION && this._isDepositOfCurrentUser(deposit) && !this._securityService.isRootOrAdmin()) {
      bannerMessage.message = MARK_AS_TRANSLATABLE("deposit.alert.status.approvalImpossibleForDepositOfCurrentUser");
      bannerMessage.color = BannerColorEnum.info;
      return bannerMessage;
    }

    if (depositStatus === Enums.Deposit.StatusEnum.COMPLETED || depositStatus === Enums.Deposit.StatusEnum.CLEANED) {
      bannerMessage.message = MARK_AS_TRANSLATABLE("deposit.alert.status.completed");
      bannerMessage.color = BannerColorEnum.success;
      return bannerMessage;
    }

    if (depositStatus === Enums.Deposit.StatusEnum.APPROVED ||
      depositStatus === Enums.Deposit.StatusEnum.CHECKED ||
      depositStatus === Enums.Deposit.StatusEnum.SUBMITTED) {
      bannerMessage.message = MARK_AS_TRANSLATABLE("deposit.alert.status.processed");
      bannerMessage.color = BannerColorEnum.success;
      return bannerMessage;
    }

    bannerMessage.display = false;
    return bannerMessage;
  }

  private _isDepositOfCurrentUser(deposit: Deposit): boolean {
    return deposit?.creation?.who === MemoizedUtil.currentSnapshot(this._store, AppUserState)?.externalUid;
  }
}

enum TabEnum {
  METADATA = "METADATA",
  README = "README",
  DATA = "DATA",
  FILES = "FILES",
  COLLECTIONS = "COLLECTIONS",
}

interface BannerDisplayInfo {
  color: BannerColorEnum | undefined;
  message: string | undefined;
  display: boolean;
  parameters: SolidifyObject | undefined;
}
