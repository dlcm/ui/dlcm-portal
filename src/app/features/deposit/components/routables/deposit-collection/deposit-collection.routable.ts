/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-collection.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {ModeDepositTabEnum} from "@deposit/enums/mode-deposit-tab.enum";
import {depositActionNameSpace} from "@deposit/stores/deposit.action";
import {
  DepositState,
  DepositStateModel,
} from "@deposit/stores/deposit.state";
import {Enums} from "@enums";
import {Deposit} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DepositRoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {LocalStateModel} from "@shared/models/local-state.model";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditRoutable,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-collection-routable",
  templateUrl: "./deposit-collection.routable.html",
  styleUrls: ["./deposit-collection.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositCollectionRoutable extends AbstractDetailEditRoutable<Deposit, DepositStateModel> implements OnInit {
  isInDetailMode: boolean = false;

  canEditObs: Observable<DepositEditModeEnum> = MemoizedUtil.select(this._store, DepositState, state => state.canEdit);
  depositModeTabEnumObs: Observable<ModeDepositTabEnum> = MemoizedUtil.select(this._store, DepositState, state => state.depositModeTabEnum);

  get depositStatusEnum(): typeof Enums.Deposit.StatusEnum {
    return Enums.Deposit.StatusEnum;
  }

  get modeDepositTabEnum(): typeof ModeDepositTabEnum {
    return ModeDepositTabEnum;
  }

  get depositEditModeEnum(): typeof DepositEditModeEnum {
    return DepositEditModeEnum;
  }

  readonly KEY_PARAM_NAME: keyof Deposit & string;

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              protected readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.deposit, _injector, depositActionNameSpace);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._retrieveResIdFromUrl();
    this._getSubResourceWithParentId(this._resId);

    this.subscribe(this._store.select((s: LocalStateModel) => s.router.state.url)
      .pipe(
        distinctUntilChanged(),
        tap(url => {
          this.isInDetailMode = !url.endsWith(DepositRoutesEnum.collections);
          this._changeDetector.detectChanges();
        }),
      ));
  }

  protected _getSubResourceWithParentId(id: string): void {
  }
}


