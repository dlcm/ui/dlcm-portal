/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-metadata.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {AppState} from "@app/stores/app.state";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {DepositFormPresentational} from "@deposit/components/presentationals/deposit-form/deposit-form.presentational";
import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {ModeDepositTabEnum} from "@deposit/enums/mode-deposit-tab.enum";
import {DepositService} from "@deposit/services/deposit.service";
import {depositActionNameSpace} from "@deposit/stores/deposit.action";
import {
  DepositState,
  DepositStateModel,
} from "@deposit/stores/deposit.state";
import {DepositDuaDataFileAction} from "@deposit/stores/dua-data-file/deposit-dua-data-file.action";
import {DepositDuaDataFileState} from "@deposit/stores/dua-data-file/deposit-dua-data-file.state";
import {DepositOrganizationalUnitAdditionalFieldsFormAction} from "@deposit/stores/organizational-unit/additional-fields-form/deposit-organizational-unit-additional-fields-form.action";
import {DepositOrganizationalUnitAdditionalFieldsFormState} from "@deposit/stores/organizational-unit/additional-fields-form/deposit-organizational-unit-additional-fields-form.state";
import {DepositOrganizationalUnitState} from "@deposit/stores/organizational-unit/deposit-organizational-unit.state";
import {DepositOrganizationalUnitSubjectAreaAction} from "@deposit/stores/organizational-unit/subject-area/deposit-organizational-unit-subject-area.action";
import {DepositPeopleState} from "@deposit/stores/people/deposit-people.state";
import {DepositSipState} from "@deposit/stores/sip/deposit-sip.state";
import {DepositSubjectAreaState} from "@deposit/stores/subject-area/deposit-subject-area.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  AdditionalFieldsForm,
  ArchiveType,
  Deposit,
  DepositDataFile,
  Language,
  OrganizationalUnit,
  Person,
  PreservationPolicy,
  Sip,
  SubjectArea,
  SubmissionPolicy,
  SystemProperty,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedArchiveTypeState} from "@shared/stores/archive-type/shared-archive-type.state";
import {SharedLanguageState} from "@shared/stores/language/shared-language.state";
import {SharedOrgUnitPreservationPolicyAction} from "@shared/stores/organizational-unit/preservation-policy/shared-organizational-unit-preservation-policy.action";
import {SharedOrganizationalUnitPreservationPolicyState} from "@shared/stores/organizational-unit/preservation-policy/shared-organizational-unit-preservation-policy.state";
import {SharedOrgUnitAction} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedOrgUnitSubmissionPolicyAction} from "@shared/stores/organizational-unit/submission-policy/shared-organizational-unit-submission-policy.action";
import {SharedOrganizationalUnitSubmissionPolicyState} from "@shared/stores/organizational-unit/submission-policy/shared-organizational-unit-submission-policy.state";
import {Observable} from "rxjs";
import {
  filter,
  skipWhile,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditRoutable,
  AppSystemPropertyState,
  FileVisualizerHelper,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
  PollingHelper,
  SolidifyDataFileModel,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-metadata-routable",
  templateUrl: "./deposit-metadata.routable.html",
  styleUrls: ["./deposit-metadata.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class DepositMetadataRoutable extends AbstractDetailEditRoutable<Deposit, DepositStateModel> implements OnInit, OnDestroy, AfterViewInit {
  @Select(DepositState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(DepositState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  @Select(AppState.currentUserApplicationRoleResId) currentUserApplicationRoleObs: Observable<Enums.UserApplicationRole.UserApplicationRoleEnum>;
  personConnectedObs: Observable<Person> = MemoizedUtil.current(this._store, AppPersonState);
  languagesObs: Observable<Language[]> = MemoizedUtil.list(this._store, SharedLanguageState);
  selectedPersonObs: Observable<Person[]> = MemoizedUtil.selected(this._store, DepositPeopleState);
  currentDepositObs: Observable<Deposit> = MemoizedUtil.current(this._store, DepositState);
  listPreservationPoliciesObs: Observable<PreservationPolicy[]> = MemoizedUtil.selected(this._store, SharedOrganizationalUnitPreservationPolicyState);
  listSubmissionPoliciesObs: Observable<SubmissionPolicy[]> = MemoizedUtil.selected(this._store, SharedOrganizationalUnitSubmissionPolicyState);
  canEditObs: Observable<DepositEditModeEnum> = MemoizedUtil.select(this._store, DepositState, state => state.canEdit);
  orgUnitObs: Observable<OrganizationalUnit> = MemoizedUtil.select(this._store, DepositOrganizationalUnitState, (state) => state.current);
  additionalFieldsFormObs: Observable<AdditionalFieldsForm> = MemoizedUtil.current(this._store, DepositOrganizationalUnitAdditionalFieldsFormState);
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);
  logoOrgUnitObs: Observable<string> = MemoizedUtil.select(this._store, DepositOrganizationalUnitState, state => state.file);
  depositModeTabEnumObs: Observable<ModeDepositTabEnum> = MemoizedUtil.select(this._store, DepositState, state => state.depositModeTabEnum);
  dataFileDuaObs: Observable<DepositDataFile> = MemoizedUtil.current(this._store, DepositDuaDataFileState);
  sipObs: Observable<Sip> = MemoizedUtil.current(this._store, DepositSipState);
  listArchivesMasterTypesObs: Observable<ArchiveType[]> = MemoizedUtil.list(this._store, SharedArchiveTypeState);
  listSubjectAreasObs: Observable<SubjectArea[]> = MemoizedUtil.selected(this._store, DepositSubjectAreaState);
  currentLanguageObs: Observable<Enums.Language.LanguageEnum> = MemoizedUtil.select(this._store, AppState, state => state.appLanguage);

  private _orgUnitResId: string;

  @ViewChild("formPresentational")
  readonly formPresentational: DepositFormPresentational;

  readonly KEY_PARAM_NAME: keyof Deposit & string = "title";

  isTour: boolean = false;

  get depositEditModeEnum(): typeof DepositEditModeEnum {
    return DepositEditModeEnum;
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _depositService: DepositService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.deposit, _injector, depositActionNameSpace);
  }

  ngOnInit(): void {
    super.ngOnInit();

    if (MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isInTourMode)) {
      this.isTour = true;
      this._changeDetector.detectChanges();
      return;
    }

    this.subscribe(PollingHelper.startPollingObs({
      initialIntervalRefreshInSecond: environment.refreshDepositSubmittedIntervalInSecond,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: 60,
      stopRefreshAfterMaximumIntervalReached: false,
      resetIntervalWhenUserMouseEvent: true,
      filter: () => {
        if (this.isEdit) {
          return false;
        }
        const duaFile = MemoizedUtil.currentSnapshot(this._store, DepositDuaDataFileState) as DepositDataFile;
        if (isNullOrUndefined(duaFile)) {
          return false;
        }
        if (FileVisualizerHelper.canHandleByStatus({dataFile: duaFile as SolidifyDataFileModel})) {
          return false;
        }
        if (duaFile.status === Enums.DataFile.StatusEnum.IN_ERROR) {
          return false;
        }
        return true;
      },
      actionToDo: () => {
        this._store.dispatch(new DepositDuaDataFileAction.GetDuaFile(this._resId));
      },
    }));

    this._retrieveResIdFromUrl();
    this.initData();
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();

    this.subscribe(this.isReadyToBeDisplayedObs.pipe(
      filter(ready => ready),
      tap(ready => {
        setTimeout(() => {
          this._depositService.formPresentational = this.formPresentational;
        }, 0);
      })),
    );
  }

  protected _retrieveResIdFromUrl(): void {
    this._orgUnitResId = this._route.parent.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdOrgUnitWithoutPrefixParam);
    this._resId = this._route.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  private _adaptPolicies(organizationalUnitId: string): void {
    this._store.dispatch(new SharedOrgUnitAction.GetById(organizationalUnitId));
    this._store.dispatch(new SharedOrgUnitPreservationPolicyAction.GetAll(organizationalUnitId));
    this._store.dispatch(new SharedOrgUnitSubmissionPolicyAction.GetAll(organizationalUnitId));
  }

  private _adaptAdditionalFieldsForm(deposit: Deposit): void {
    if (isNotNullNorUndefined(deposit.additionalFieldsValues) && isNotNullNorUndefined(deposit.additionalFieldsFormId)) {
      this._store.dispatch(new DepositOrganizationalUnitAdditionalFieldsFormAction.GetById(deposit.organizationalUnitId, deposit.additionalFieldsFormId));
    }
  }

  initData(): void {
    this.subscribe(this.currentDepositObs.pipe(
      skipWhile((deposit: Deposit) => deposit === null || deposit === undefined),
      take(1),
      tap(deposit => {
        this._adaptPolicies(deposit.organizationalUnitId);
        this._adaptAdditionalFieldsForm(deposit);
      })));
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._store.dispatch(new SharedOrgUnitPreservationPolicyAction.Clear());
    this._store.dispatch(new SharedOrgUnitSubmissionPolicyAction.Clear());
    this._store.dispatch(new DepositOrganizationalUnitSubjectAreaAction.Clear());
  }

  override edit(): void {
    if (this.isEdit) {
      return;
    }
    this._store.dispatch(new Navigate([RoutesEnum.deposit, this._orgUnitResId, DepositRoutesEnum.detail, this._resId, DepositRoutesEnum.metadata, DepositRoutesEnum.edit], {}, {skipLocationChange: true}));
  }

  protected _getSubResourceWithParentId(id: string): void {
  }
}
