/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {AppState} from "@app/stores/app.state";
import {DepositAction} from "@deposit/stores/deposit.action";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {AppRoutesEnum} from "@shared/enums/routes.enum";
import {
  isNullOrUndefined,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-home-routable",
  templateUrl: "./deposit-home.routable.html",
  styleUrls: ["./deposit-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositHomeRoutable extends SharedAbstractRoutable implements OnInit {
  private _orgUnitResId: string;

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _route: ActivatedRoute) {
    super();
  }

  ngOnInit(): void {
    if (MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isInTourMode)) {
      return;
    }
    this._retrieveCurrentModelWithUrl();
    this.subscribe(StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(this._store, [
      {
        action: new DepositAction.GetExcludedListFiles(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.GetExcludedListFilesSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.GetExcludedListFilesFail)),
        ],
      },
      {
        action: new DepositAction.GetIgnoredListFiles(),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.GetIgnoredListFilesSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(DepositAction.GetIgnoredListFilesFail)),
        ],
      },
    ]));
  }

  protected _retrieveCurrentModelWithUrl(): void {
    this._retrieveResIdFromUrl();
    this._store.dispatch(new DepositAction.SetOrganizationalUnit(this._orgUnitResId));
  }

  protected _retrieveResIdFromUrl(): void {
    this._orgUnitResId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdOrgUnitWithoutPrefixParam);
    if (isNullOrUndefined(this._orgUnitResId)) {
      this._orgUnitResId = this._route.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdOrgUnitWithoutPrefixParam);
    }
  }
}

