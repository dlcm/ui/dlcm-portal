/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-root.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
} from "@angular/core";
import {AppAuthorizedOrganizationalUnitAction} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {AppAuthorizedOrganizationalUnitState} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.state";
import {DepositTabStatusEnum} from "@deposit/enums/deposit-tab-status.enum";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractRoutable} from "@shared/components/routables/shared-abstract/shared-abstract.routable";
import {
  AppRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {DlcmUserPreferencesUtil} from "@shared/utils/dlcm-user-preferences.util";
import {tap} from "rxjs/operators";
import {
  isEmptyArray,
  isFalse,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-root-routable",
  templateUrl: "./deposit-root.routable.html",
  styleUrls: ["./deposit-root.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositRootRoutable extends SharedAbstractRoutable implements OnInit {
  noOrgUnit: boolean = false;

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              private readonly _notificationService: NotificationService) {
    super();
  }

  ngOnInit(): void {
    this.subscribe(StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(this._store, [
      {
        action: new AppAuthorizedOrganizationalUnitAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllFail)),
        ],
      },
    ]).pipe(
      tap(result => {
        if (isFalse(result.success)) {
          return;
        }

        const listAuthorizedOrgUnit = MemoizedUtil.listSnapshot(this._store, AppAuthorizedOrganizationalUnitState);
        if (isNullOrUndefined(listAuthorizedOrgUnit) || isEmptyArray(listAuthorizedOrgUnit)) {
          this.noOrgUnit = true;
          this._notificationService.showInformation(MARK_AS_TRANSLATABLE("notification.deposit.noOrgUnit"));
          this._store.dispatch(new Navigate([AppRoutesEnum.preservationSpace]));
          return;
        }
        const preferredOrgUnit = DlcmUserPreferencesUtil.getPreferredOrgUnitInDepositMenu(listAuthorizedOrgUnit);
        if (isNullOrUndefined(preferredOrgUnit)) {
          return;
        }
        this._store.dispatch(new Navigate([RoutesEnum.deposit, preferredOrgUnit.resId, DepositTabStatusEnum.inProgress]));
      }),
    ));
  }
}

