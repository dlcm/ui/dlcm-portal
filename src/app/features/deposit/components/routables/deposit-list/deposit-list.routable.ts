/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {MatTabChangeEvent} from "@angular/material/tabs";
import {
  ActivatedRoute,
  NavigationCancel,
  NavigationEnd,
  Router,
} from "@angular/router";
import {
  DepositAction,
  depositActionNameSpace,
} from "@app/features/deposit/stores/deposit.action";
import {
  DepositState,
  DepositStateModel,
} from "@app/features/deposit/stores/deposit.state";
import {
  AppRoutesEnum,
  DepositRoutesEnum,
  RoutesEnum,
} from "@app/shared/enums/routes.enum";
import {AppState} from "@app/stores/app.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {DepositTabStatusEnum} from "@deposit/enums/deposit-tab-status.enum";
import {DepositHelper} from "@deposit/helpers/deposit.helper";
import {DepositTabStatus} from "@deposit/models/deposit-tab-status.model";
import {depositAuthorizedOrganizationalUnitNameSpace} from "@deposit/stores/authorized-organizational-unit/deposit-authorized-organizational-unit.action";
import {DepositAuthorizedOrganizationalUnitState} from "@deposit/stores/authorized-organizational-unit/deposit-authorized-organizational-unit.state";
import {depositStatusHistoryNamespace} from "@deposit/stores/status-history/deposit-status-history.action";
import {DepositStatusHistoryState} from "@deposit/stores/status-history/deposit-status-history.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  Deposit,
  OrganizationalUnit,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SecurityService} from "@shared/services/security.service";
import {DlcmUserPreferencesUtil} from "@shared/utils/dlcm-user-preferences.util";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
  tap,
} from "rxjs/operators";
import {
  AbstractListRoutable,
  CookieConsentService,
  CookieConsentUtil,
  CookieType,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  FormValidationHelper,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrEmpty,
  isTrue,
  isUndefined,
  LocalStorageHelper,
  MappingObjectUtil,
  MemoizedUtil,
  OrderEnum,
  PollingHelper,
  PropertyName,
  QueryParameters,
  QueryParametersUtil,
  ResourceNameSpace,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
  SolidifyValidator,
  Sort,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-list-routable",
  templateUrl: "./deposit-list.routable.html",
  styleUrls: ["./deposit-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositListRoutable extends AbstractListRoutable<Deposit, DepositStateModel> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  isInTourModeObs: Observable<boolean> = MemoizedUtil.select(this._store, AppState, state => state.isInTourMode);

  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.createDeposit;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof Deposit & string = "title";

  depositAuthorizedOrganizationalUnitSort: Sort = {
    field: "name",
    order: OrderEnum.ascending,
  };
  depositAuthorizedOrganizationalUnitNameSpace: ResourceNameSpace = depositAuthorizedOrganizationalUnitNameSpace;
  depositAuthorizedOrganizationalUnitState: typeof DepositAuthorizedOrganizationalUnitState = DepositAuthorizedOrganizationalUnitState;

  form: FormGroup;

  lastSelectionOrgUnitLocalStorageKey: LocalStorageEnum = undefined;

  private _orgUnitResId: string;
  private _isInitialized: boolean = false;
  readonly listTabStatus: DepositTabStatus[] = DepositHelper.tabsStatus;
  currentTabStatus: DepositTabStatus | undefined;
  selectedTabIndex: number;

  columnsSkippedToClear: string[] = [DepositHelper.KEY_ORGANIZATIONAL_UNIT, DepositHelper.KEY_STATUS];

  canCreateObs: Observable<boolean> = MemoizedUtil.select(this._store, DepositState, state => state.canCreate);

  showOnlyMyDeposits: boolean;

  private _getColumnStatus(isFilterable: boolean): DataTableColumns<Deposit> {
    return {
      field: DepositHelper.KEY_STATUS,
      header: LabelTranslateEnum.status,
      type: DataTableFieldTypeEnum.singleSelect,
      order: OrderEnum.none,
      isFilterable: isFilterable,
      isSortable: true,
      translate: true,
      filterEnum: Enums.Deposit.StatusEnumTranslate,
      component: DataTableComponentHelper.get(DataTableComponentEnum.status),
      minWidth: "130px",
      maxWidth: "1800px",
      width: "130px",
      alignment: "center",
    };
  };

  private _INTERVAL_REFRESH_IN_SECOND: number = environment.refreshTabStatusCounterIntervalInSecond;

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              protected readonly _router: Router,
              private readonly _cookieConsentService: CookieConsentService,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.deposit, depositActionNameSpace, _injector, {
      historyState: DepositStatusHistoryState,
      historyStateAction: depositStatusHistoryNamespace,
      historyStatusEnumTranslate: Enums.Deposit.StatusEnumTranslate,
    });
    this.cleanIsNeeded = true;
    this._initShowOnlyMyDepositsToggle();

    this.subscribe(this._cookieConsentService.watchCookiePreferenceUpdateObs(CookieType.localStorage, LocalStorageEnum.singleSelectLastSelectionOrgUnit)
      .pipe(
        tap(p => {
          this._setLastSelectionOrgUnitKeyIfFeatureEnabled();
          this._changeDetector.detectChanges();
        }),
      ));
    this._setLastSelectionOrgUnitKeyIfFeatureEnabled();
  }

  ngOnInit(): void {
    // Allow to preserve query parameters (pagination, sort, filter) when come from detail page
    if (isNotNullNorUndefined(this._routerExt.getPreviousUrl())) {
      if (this._routerExt.getPreviousUrl().includes(DepositRoutesEnum.detail)) {
        this.cleanIsNeeded = false;
      } else {
        this.clean();
      }
    }

    super.ngOnInit();

    this._retrieveOrgUnitInUrl();

    if (MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isInTourMode)) {
      this.form = this._fb.group({
        [this.formDefinition.organizationalUnitId]: [this._orgUnitResId, [Validators.required, SolidifyValidator]],
      });

      return;
    }
    this._updateCurrentTab();
    this.subscribe(this._observeRoutingUpdate());

    this.form = this._fb.group({
      [this.formDefinition.organizationalUnitId]: [this._orgUnitResId, [Validators.required, SolidifyValidator]],
    });

    this.subscribe(this._route.url.pipe(distinctUntilChanged()),
      value => {
        // TAB or ORGUNIT change
        this._retrieveOrgUnitInUrl();
        if (this._isInitialized === true) {
          let queryParameters = QueryParametersUtil.clone(MemoizedUtil.queryParametersSnapshot(this._store, DepositState));
          queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
          this.onQueryParametersEvent(queryParameters, false);
        }
        this._changeDetector.detectChanges();
      });

    this.subscribe(this.form.get(this.formDefinition.organizationalUnitId).valueChanges.pipe(
      distinctUntilChanged(),
      tap(resId => {
        // ORGUNIT CHANGE
        this._store.dispatch(new Navigate([RoutesEnum.deposit, resId, this.currentTabStatus.tabEnum]));
        this._store.dispatch(new DepositAction.SetOrganizationalUnit(resId));
        this._refreshCounter();
        this._changeDetector.detectChanges();
      }),
    ));

    this._isInitialized = true;

    this.subscribe(PollingHelper.startPollingObs({
      waitingTimeBeforeStartInSecond: 0,
      initialIntervalRefreshInSecond: this._INTERVAL_REFRESH_IN_SECOND,
      resetIntervalWhenUserMouseEvent: true,
      incrementInterval: true,
      maximumIntervalRefreshInSecond: 60 * 10,
      stopRefreshAfterMaximumIntervalReached: true,
      actionToDo: () => this._refreshCounter(),
    }));
  }

  private _initShowOnlyMyDepositsToggle(): void {
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.depositShowOnlyMine)) {
      this.showOnlyMyDeposits = LocalStorageHelper.getItem(LocalStorageEnum.depositShowOnlyMine) !== SOLIDIFY_CONSTANTS.STRING_FALSE;
    } else {
      this.showOnlyMyDeposits = true;
    }
  }

  toggleShowOnlyMyDeposits(checked: boolean): void {
    this.showOnlyMyDeposits = checked;
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.depositShowOnlyMine)) {
      LocalStorageHelper.setItem(LocalStorageEnum.depositShowOnlyMine, this.showOnlyMyDeposits + "");
    }
    let queryParameters = MemoizedUtil.queryParametersSnapshot(this._store, DepositState);
    queryParameters = QueryParametersUtil.clone(queryParameters);
    this._updateQueryParametersWithOnlyMyDeposits(queryParameters);
    this._store.dispatch(new DepositAction.ChangeQueryParameters(queryParameters));
    this._refreshCounter();
    this._changeDetector.detectChanges();
  }

  getTabCounter(tabEnum: DepositTabStatusEnum): Observable<number> {
    return MemoizedUtil.select(this._store, DepositState, state => MappingObjectUtil.get(state.tabCounters, String(tabEnum)));
  }

  private _setLastSelectionOrgUnitKeyIfFeatureEnabled(): void {
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.singleSelectLastSelectionOrgUnit)) {
      this.lastSelectionOrgUnitLocalStorageKey = LocalStorageEnum.singleSelectLastSelectionOrgUnit;
    } else {
      this.lastSelectionOrgUnitLocalStorageKey = undefined;
    }
  }

  private _refreshCounter(): void {
    const listTabWithCounter = this.listTabStatus.filter(t => isTrue(t.displayCounter)).map(tabStatus => tabStatus.tabEnum);
    const queryParameters = MemoizedUtil.queryParametersSnapshot(this._store, DepositState);
    const creatorExternalUid = MappingObjectUtil.get(QueryParametersUtil.getSearchItems(queryParameters), DepositHelper.KEY_CREATION_WHO);
    this._store.dispatch(new DepositAction.RefreshAllCounterStatusTab(listTabWithCounter, creatorExternalUid));
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  private _retrieveOrgUnitInUrl(): void {
    this._orgUnitResId = this._route.snapshot.parent.paramMap.get(AppRoutesEnum.paramIdOrgUnitWithoutPrefixParam);
    this.detailRouteToInterpolate = `${AppRoutesEnum.deposit}/${this._orgUnitResId}/${DepositRoutesEnum.detail}/\{${SOLIDIFY_CONSTANTS.RES_ID}\}`;
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  private _observeRoutingUpdate(): Observable<any> {
    return this._router.events
      .pipe(
        filter(event => event instanceof NavigationCancel || event instanceof NavigationEnd),
        distinctUntilChanged(),
        tap(event => {
          this._updateCurrentTab();
        }),
      );
  }

  private _updateCurrentTab(): void {
    const tab: DepositTabStatusEnum = this._route.snapshot.paramMap.get(DepositRoutesEnum.paramTabWithoutPrefix) as DepositTabStatusEnum;
    this.selectedTabIndex = this.listTabStatus.findIndex(t => t.tabEnum === tab);
    if (this.selectedTabIndex !== -1) {
      this.currentTabStatus = this.listTabStatus[this.selectedTabIndex];
    }

    this._store.dispatch(new DepositAction.SetActiveListTabStatus(this.currentTabStatus.tabEnum));
  }

  private _download(deposit: Deposit): void {
    this._store.dispatch(new DepositAction.Download(deposit.resId));
  }

  private _startMetadataEditing(deposit: Deposit): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new DepositAction.StartMetadataEditing(deposit),
      DepositAction.StartMetadataEditingSuccess, action => {
        setTimeout(() => {
          let queryParameters = QueryParametersUtil.clone(MemoizedUtil.queryParametersSnapshot(this._store, DepositState));
          queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
          this.onQueryParametersEvent(queryParameters, false);
          this._refreshCounter();
          this._changeDetector.detectChanges();
        }, 100);
      }));
  }

  private _cancelMetadataEditing(deposit: Deposit): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new DepositAction.CancelMetadataEditing(deposit),
      DepositAction.CancelMetadataEditingSuccess, action => {
        setTimeout(() => {
          let queryParameters = QueryParametersUtil.clone(MemoizedUtil.queryParametersSnapshot(this._store, DepositState));
          queryParameters = QueryParametersUtil.resetToFirstPage(queryParameters);
          this.onQueryParametersEvent(queryParameters, false);
          this._refreshCounter();
          this._changeDetector.detectChanges();
        }, 100);
      }));
  }

  onTabChanged(tabEvent: MatTabChangeEvent): void {
    const index = tabEvent.index;
    this.currentTabStatus = this.listTabStatus[index];
    this.selectedTabIndex = index;
    this._store.dispatch(new Navigate([AppRoutesEnum.deposit, this._orgUnitResId, this.currentTabStatus.tabEnum]));
  }

  conditionDisplayEditButton(model: Deposit | undefined): boolean {
    return this.conditionDisplayDeleteButton(model);
  }

  conditionDisplayDeleteButton(model: Deposit | undefined): boolean {
    if (isUndefined(model)) {
      return true;
    }
    return model.status === Enums.Deposit.StatusEnum.IN_PROGRESS || model.status === Enums.Deposit.StatusEnum.IN_ERROR;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "title",
        header: LabelTranslateEnum.title,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "publicationDate",
        header: LabelTranslateEnum.publicationDate,
        type: DataTableFieldTypeEnum.date,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  protected override _defineActions(): DataTableActions<Deposit>[] {
    return [
      {
        logo: IconNameEnum.download,
        callback: (model: Deposit) => this._download(model),
        placeholder: current => LabelTranslateEnum.download,
        displayOnCondition: (model: Deposit) => !isNullOrUndefined(model) && !isNullOrUndefined(model.status) && model.status === Enums.Package.StatusEnum.COMPLETED,
        isWrapped: false,
      },
      {
        logo: IconNameEnum.edit,
        displayOnCondition: (model: Deposit) => isNotNullNorUndefined(model?.status)
          && [Enums.Deposit.StatusEnum.COMPLETED, Enums.Deposit.StatusEnum.CLEANED, Enums.Deposit.StatusEnum.EDITING_METADATA_REJECTED].includes(model.status)
          && (this._securityService.isRoot()
            || (this._securityService.isManagerOfOrgUnit(model.organizationalUnitId) || this._securityService.isStewardOfOrgUnit(model.organizationalUnitId))),
        callback: current => this._startMetadataEditing(current),
        placeholder: current => LabelTranslateEnum.enableDepositEdition,
      },
      {
        logo: IconNameEnum.abort,
        displayOnCondition: (model: Deposit) => isNotNullNorUndefined(model?.status)
          && model.status === Enums.Deposit.StatusEnum.EDITING_METADATA
          && (this._securityService.isRoot()
            || (this._securityService.isManagerOfOrgUnit(model.organizationalUnitId) || this._securityService.isStewardOfOrgUnit(model.organizationalUnitId))),
        callback: current => this._cancelMetadataEditing(current),
        placeholder: current => LabelTranslateEnum.cancelDepositEdition,
      },
    ];
  }

  override goToEdit(deposit: Deposit): void {
    this._store.dispatch(new Navigate([AppRoutesEnum.deposit, deposit.organizationalUnitId, DepositRoutesEnum.detail, deposit.resId, DepositRoutesEnum.edit], {}, {skipLocationChange: true}));
  }

  showDetail(deposit: Deposit): void {
    this._store.dispatch(new Navigate([AppRoutesEnum.deposit, deposit.organizationalUnitId, DepositRoutesEnum.detail, deposit.resId]));
  }

  create(element: ElementRef): void {
    this._store.dispatch(new Navigate([RoutesEnum.deposit, this._orgUnitResId, DepositRoutesEnum.create], {}, {skipLocationChange: true}));
  }

  private _updateQueryParameterWithOrgUnit(queryParameters: QueryParameters, orgUnit: OrganizationalUnit | undefined): QueryParameters | undefined {
    if (isNullOrUndefined(orgUnit)) {
      return queryParameters;
    }
    DlcmUserPreferencesUtil.setPreferredOrgUnitInDepositMenu(orgUnit.resId);
    MappingObjectUtil.set(queryParameters.search.searchItems, DepositHelper.KEY_ORGANIZATIONAL_UNIT, orgUnit.resId);
    return queryParameters;
  }

  private _updateQueryParameterWithStatus(queryParameters: QueryParameters): QueryParameters | undefined {
    if (isNullOrUndefined(this.currentTabStatus)) {
      return queryParameters;
    }
    const indexOfStatusColumn = this.columns.findIndex(c => c.field === DepositHelper.KEY_STATUS);
    const depositStatus = this.currentTabStatus.depositStatusEnum;
    const isAllTab = isNullOrUndefinedOrEmpty(depositStatus);

    if (isAllTab) {
      const comeFromOtherTab = indexOfStatusColumn !== -1 ? !this.columns[indexOfStatusColumn].isFilterable : true;
      if (comeFromOtherTab) {
        MappingObjectUtil.delete(queryParameters.search.searchItems, DepositHelper.KEY_STATUS);
      }
      MappingObjectUtil.delete(queryParameters.search.searchItems, DepositHelper.KEY_STATUS_LIST);
    } else if (depositStatus.length === 1) {
      MappingObjectUtil.set(queryParameters.search.searchItems, DepositHelper.KEY_STATUS, depositStatus[0]);
      MappingObjectUtil.delete(queryParameters.search.searchItems, DepositHelper.KEY_STATUS_LIST);
    } else {
      MappingObjectUtil.delete(queryParameters.search.searchItems, DepositHelper.KEY_STATUS);
      MappingObjectUtil.set(queryParameters.search.searchItems, DepositHelper.KEY_STATUS_LIST, depositStatus.join(SOLIDIFY_CONSTANTS.COMMA));
    }

    if (indexOfStatusColumn !== -1) {
      if (isAllTab || (isNonEmptyArray(depositStatus) && depositStatus.length > 1)) {
        this.columns[indexOfStatusColumn].isFilterable = isAllTab;
      } else {
        this.columns.splice(indexOfStatusColumn, 1);
      }
    } else if (isAllTab || isNonEmptyArray(depositStatus) && depositStatus.length > 1) {
      this.columns.push(this._getColumnStatus(isAllTab));
    }
  }

  private _updateQueryParametersWithOnlyMyDeposits(queryParameters: QueryParameters): QueryParameters | undefined {
    if (this.showOnlyMyDeposits) {
      const currentUserId = MemoizedUtil.currentSnapshot(this._store, AppUserState)?.externalUid;
      MappingObjectUtil.set(queryParameters.search.searchItems, DepositHelper.KEY_CREATION_WHO, currentUserId);
      queryParameters.paging.pageIndex = 0;
    } else {
      MappingObjectUtil.delete(queryParameters.search.searchItems, DepositHelper.KEY_CREATION_WHO);
    }
    return queryParameters;
  }

  onQueryParametersEvent(queryParameters: QueryParameters, needCopy: boolean = true): void {
    if (needCopy) {
      queryParameters = QueryParametersUtil.clone(queryParameters);
    }
    this._updateQueryParameterWithOrgUnit(queryParameters, {resId: this._orgUnitResId});
    this._updateQueryParameterWithStatus(queryParameters);
    this._updateQueryParametersWithOnlyMyDeposits(queryParameters);
    super.onQueryParametersEvent(queryParameters);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() organizationalUnitId: string;
}
