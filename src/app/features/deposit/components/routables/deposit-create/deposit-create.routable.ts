/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {depositActionNameSpace} from "@app/features/deposit/stores/deposit.action";
import {
  DepositState,
  DepositStateModel,
} from "@app/features/deposit/stores/deposit.state";
import {AppState} from "@app/stores/app.state";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {DepositFormPresentational} from "@deposit/components/presentationals/deposit-form/deposit-form.presentational";
import {DepositOrganizationalUnitAdditionalFieldsFormAction} from "@deposit/stores/organizational-unit/additional-fields-form/deposit-organizational-unit-additional-fields-form.action";
import {DepositOrganizationalUnitAdditionalFieldsFormState} from "@deposit/stores/organizational-unit/additional-fields-form/deposit-organizational-unit-additional-fields-form.state";
import {DepositOrganizationalUnitState} from "@deposit/stores/organizational-unit/deposit-organizational-unit.state";
import {DepositOrganizationalUnitSubjectAreaAction} from "@deposit/stores/organizational-unit/subject-area/deposit-organizational-unit-subject-area.action";
import {DepositOrganizationalUnitSubjectAreaState} from "@deposit/stores/organizational-unit/subject-area/deposit-organizational-unit-subject-area.state";
import {Enums} from "@enums";
import {
  AdditionalFieldsForm,
  ArchiveType,
  Deposit,
  Language,
  OrganizationalUnit,
  Person,
  PreservationPolicy,
  SubjectArea,
  SubmissionPolicy,
  SystemProperty,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SharedArchiveTypeState} from "@shared/stores/archive-type/shared-archive-type.state";
import {SharedLanguageState} from "@shared/stores/language/shared-language.state";
import {SharedOrgUnitPreservationPolicyAction} from "@shared/stores/organizational-unit/preservation-policy/shared-organizational-unit-preservation-policy.action";
import {SharedOrganizationalUnitPreservationPolicyState} from "@shared/stores/organizational-unit/preservation-policy/shared-organizational-unit-preservation-policy.state";
import {SharedOrgUnitSubmissionPolicyAction} from "@shared/stores/organizational-unit/submission-policy/shared-organizational-unit-submission-policy.action";
import {SharedOrganizationalUnitSubmissionPolicyState} from "@shared/stores/organizational-unit/submission-policy/shared-organizational-unit-submission-policy.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  filter,
} from "rxjs/operators";
import {
  AbstractCreateRoutable,
  AppSystemPropertyState,
  ExtraButtonToolbar,
  isNotNullNorUndefined,
  MemoizedUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-create-routable",
  templateUrl: "./deposit-create.routable.html",
  styleUrls: ["./deposit-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositCreateRoutable extends AbstractCreateRoutable<Deposit, DepositStateModel> implements OnInit, OnDestroy {
  @Select(DepositState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(DepositState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;
  languagesObs: Observable<Language[]> = MemoizedUtil.list(this._store, SharedLanguageState);
  listPreservationPoliciesObs: Observable<PreservationPolicy[]> = MemoizedUtil.selected(this._store, SharedOrganizationalUnitPreservationPolicyState);
  listSubmissionPoliciesObs: Observable<SubmissionPolicy[]> = MemoizedUtil.selected(this._store, SharedOrganizationalUnitSubmissionPolicyState);
  listArchivesMasterTypesObs: Observable<ArchiveType[]> = MemoizedUtil.list(this._store, SharedArchiveTypeState);
  listSubjectAreasObs: Observable<SubjectArea[]> = MemoizedUtil.selected(this._store, DepositOrganizationalUnitSubjectAreaState);
  personConnectedObs: Observable<Person> = MemoizedUtil.current(this._store, AppPersonState);
  additionalFieldsFormObs: Observable<AdditionalFieldsForm> = MemoizedUtil.current(this._store, DepositOrganizationalUnitAdditionalFieldsFormState);
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);
  currentLanguageObs: Observable<Enums.Language.LanguageEnum> = MemoizedUtil.select(this._store, AppState, state => state.appLanguage);

  @ViewChild("formPresentational")
  readonly formPresentational: DepositFormPresentational;
  orgUnitObs: Observable<OrganizationalUnit> = MemoizedUtil.select(this._store, DepositOrganizationalUnitState, (state) => state.current);
  logoOrgUnitObs: Observable<string> = MemoizedUtil.select(this._store, DepositOrganizationalUnitState, state => state.file);

  listExtraButtons: ExtraButtonToolbar<Deposit>[] = [
    {
      color: "primary",
      typeButton: "flat-button",
      icon: IconNameEnum.save,
      labelToTranslate: (current) => LabelTranslateEnum.save,
      order: 40,
      dataTest: DataTestEnum.depositSave,
      callback: () => this.formPresentational.onSubmit(),
      displayCondition: (resource) => true,
      disableCondition: () => this.formPresentational?.form?.pristine || this.formPresentational?.form?.invalid || this.formPresentational?.formFormly?.invalid,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector) {
    super(_store, _actions$, _changeDetector, StateEnum.deposit, _injector, depositActionNameSpace);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.subscribe(this.orgUnitObs.pipe(
      distinctUntilChanged(),
      filter(orgUnit => isNotNullNorUndefined(orgUnit)),
    ), orgUnit => {
      this._store.dispatch(new DepositOrganizationalUnitAdditionalFieldsFormAction.GetCurrentMetadataForm(orgUnit.resId));
      this._store.dispatch(new DepositOrganizationalUnitSubjectAreaAction.GetAll(orgUnit.resId));
      this._store.dispatch(new SharedOrgUnitPreservationPolicyAction.GetAll(orgUnit.resId));
      this._store.dispatch(new SharedOrgUnitSubmissionPolicyAction.GetAll(orgUnit.resId));
    });
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._store.dispatch(new SharedOrgUnitPreservationPolicyAction.Clear());
    this._store.dispatch(new SharedOrgUnitSubmissionPolicyAction.Clear());
    this._store.dispatch(new DepositOrganizationalUnitSubjectAreaAction.Clear());
  }
}
