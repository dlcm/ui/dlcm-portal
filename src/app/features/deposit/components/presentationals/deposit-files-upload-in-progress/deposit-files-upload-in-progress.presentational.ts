/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-files-upload-in-progress.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  Output,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {DlcmFileUploadWrapper} from "@deposit/models/dlcm-file-upload-wrapper.model";
import {DepositDataFile} from "@models";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  FileUploadStatusEnum,
  Guid,
  ObservableUtil,
  UploadFileStatus,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-files-upload-in-progress",
  templateUrl: "./deposit-files-upload-in-progress.presentational.html",
  styleUrls: ["./deposit-files-upload-in-progress.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFilesUploadInProgressPresentational extends SharedAbstractPresentational implements OnInit {
  @Input()
  listFilesUploading: UploadFileStatus<DepositDataFile>[];

  private readonly _cancelBS: BehaviorSubject<UploadFileStatus<DepositDataFile> | undefined> = new BehaviorSubject<UploadFileStatus<DepositDataFile> | undefined>(undefined);
  @Output("cancelChange")
  readonly cancelObs: Observable<UploadFileStatus<DepositDataFile> | undefined> = ObservableUtil.asObservable(this._cancelBS);

  private readonly _retryBS: BehaviorSubject<UploadFileStatus<DepositDataFile> | undefined> = new BehaviorSubject<UploadFileStatus<DepositDataFile> | undefined>(undefined);
  @Output("retryChange")
  readonly retryObs: Observable<UploadFileStatus<DepositDataFile> | undefined> = ObservableUtil.asObservable(this._retryBS);

  private readonly _uploadBS: BehaviorSubject<DlcmFileUploadWrapper | undefined> = new BehaviorSubject<DlcmFileUploadWrapper | undefined>(undefined);
  @Output("uploadChange")
  readonly uploadObs: Observable<DlcmFileUploadWrapper | undefined> = ObservableUtil.asObservable(this._uploadBS);

  private readonly _uploadArchiveBS: BehaviorSubject<DlcmFileUploadWrapper | undefined> = new BehaviorSubject<DlcmFileUploadWrapper | undefined>(undefined);
  @Output("uploadArchiveChange")
  readonly uploadArchiveObs: Observable<DlcmFileUploadWrapper | undefined> = ObservableUtil.asObservable(this._uploadArchiveBS);

  constructor(private readonly _dialog: MatDialog) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    // TEST CODE FOR DESIGN
    // this.listFilesUploading = [
    //   {
    //     progressPercentage: 10,
    //     status: FileUploadStatusEnum.inProgress,
    //     fileUploadWrapper: {
    //       file: {
    //         name: "Test file name.txt",
    //       } as File,
    //     } as FileUploadWrapper,
    //   } as UploadFileStatus,
    //   {
    //     progressPercentage: 50,
    //     status: FileUploadStatusEnum.inProgress,
    //     fileUploadWrapper: {
    //       file: {
    //         name: "Test file name 2.txt",
    //       } as File,
    //     } as FileUploadWrapper,
    //   } as UploadFileStatus,
    //   {
    //     progressPercentage: 100,
    //     status: FileUploadStatusEnum.completed,
    //     fileUploadWrapper: {
    //       file: {
    //         name: "Test file name completed.txt",
    //       } as File,
    //     } as FileUploadWrapper,
    //   } as UploadFileStatus,
    //   {
    //     progressPercentage: 75,
    //     status: FileUploadStatusEnum.inError,
    //     errorMessageToTranslate: "error.upload.duplicateDataFiles",
    //     fileUploadWrapper: {
    //       file: {
    //         name: "Test file name inError.txt",
    //       } as File,
    //     } as FileUploadWrapper,
    //   } as UploadFileStatus,
    // ];
  }

  getCurrentNumberOfFileInProgress(): number {
    return this.listFilesUploading.filter(f => f.status === FileUploadStatusEnum.inProgress).length;
  }

  cancel(uploadStatus: UploadFileStatus<DepositDataFile>): void {
    this._cancelBS.next(uploadStatus);
  }

  retry(uploadStatus: UploadFileStatus<DepositDataFile>): void {
    this._retryBS.next(uploadStatus);
  }

  trackByFn(index: number, item: UploadFileStatus<DepositDataFile>): Guid {
    return item.guid;
  }
}
