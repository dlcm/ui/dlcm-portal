/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-new-person-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {BaseFormDefinition} from "@app/shared/models/base-form-definition.model";
import {Person} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AbstractFormPresentational,
  OrcidService,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-new-person-form",
  templateUrl: "./deposit-new-person-form.presentational.html",
  styleUrls: ["./deposit-new-person-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositNewPersonFormPresentational extends AbstractFormPresentational<Person> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly orcidService: OrcidService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _bindFormTo(person: Person): void {
    this.form = this._fb.group({
      [this.formDefinition.firstName]: [person.firstName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: [person.lastName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.orcid]: [person.orcid, [SolidifyValidator]],
    });
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.firstName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.orcid]: ["", [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(person: Person): Person {
    return person;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() firstName: string;
  @PropertyName() lastName: string;
  @PropertyName() orcid: string;
}
