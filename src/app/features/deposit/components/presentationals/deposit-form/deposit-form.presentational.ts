/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {DataFileHelper} from "@app/shared/helpers/data-file.helper";
import {BaseFormDefinition} from "@app/shared/models/base-form-definition.model";
import {KeyValueOption} from "@app/shared/models/key-value-option.model";
import {sharedLicenseActionNameSpace} from "@app/shared/stores/license/shared-license.action";
import {sharedPersonActionNameSpace} from "@app/shared/stores/person/shared-person.action";
import {sharedSubjectAreaActionNameSpace} from "@app/shared/stores/subject-area/shared-subject-area.action";
import {DepositOrderAuthorDialog} from "@deposit/components/dialogs/deposit-order-author/deposit-order-author.dialog";
import {DepositPersonDialog} from "@deposit/components/dialogs/deposit-person/deposit-person.dialog";
import {DepositEditModeEnum} from "@deposit/enums/deposit-edit-mode.enum";
import {ModeDepositTabEnum} from "@deposit/enums/mode-deposit-tab.enum";
import {depositActionNameSpace} from "@deposit/stores/deposit.action";
import {DepositState} from "@deposit/stores/deposit.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  AdditionalFieldsForm,
  ArchiveType,
  DataFile,
  Deposit,
  Label,
  Language,
  License,
  OrganizationalUnit,
  Person,
  PreservationPolicy,
  Sip,
  SubjectArea,
  SubmissionPolicy,
} from "@models";
import {FormlyFieldConfig} from "@ngx-formly/core";
import {TranslateService} from "@ngx-translate/core";
import {SharedLicenseOverlayPresentational} from "@shared/components/presentationals/shared-license-overlay/shared-license-overlay.presentational";
import {
  SharedPersonOverlayExtra,
  SharedPersonOverlayPresentational,
} from "@shared/components/presentationals/shared-person-overlay/shared-person-overlay.presentational";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedArchiveTypeState} from "@shared/stores/archive-type/shared-archive-type.state";
import {SharedLicenseState} from "@shared/stores/license/shared-license.state";
import {
  SharedPersonState,
  SharedPersonStateModel,
} from "@shared/stores/person/shared-person.state";
import {SharedSubjectAreaState} from "@shared/stores/subject-area/shared-subject-area.state";
import {
  debounceTime,
  merge,
} from "rxjs";
import {
  distinctUntilChanged,
  map,
  tap,
} from "rxjs/operators";
import {
  AbstractApplicationFormPresentational,
  BreakpointService,
  CookieConsentService,
  CookieConsentUtil,
  CookieType,
  DateService,
  DateUtil,
  DialogUtil,
  DownloadService,
  EnumUtil,
  FileInput,
  FilePreviewDialog,
  FileStatusEnum,
  FileVisualizerHelper,
  FileVisualizerService,
  FormValidationHelper,
  isEmptyArray,
  isNonEmptyArray,
  isNonEmptyString,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  isTrue,
  KeyValue,
  MappingObject,
  MappingObjectUtil,
  OrcidService,
  OrderEnum,
  OverlayPositionEnum,
  PropertyName,
  ResourceFileNameSpace,
  ResourceNameSpace,
  SearchableMultiSelectPresentational,
  SolidifyDataFileModel,
  SolidifyFileUploadInputRequiredValidator,
  SolidifyFileUploadStatus,
  SolidifyObject,
  SolidifyValidator,
  Sort,
  SsrUtil,
  StringUtil,
  Type,
} from "solidify-frontend";
import MetadataVersionEnum = Enums.MetadataVersion.MetadataVersionEnum;

@Component({
  selector: "dlcm-deposit-form",
  templateUrl: "./deposit-form.presentational.html",
  styleUrls: ["./deposit-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFormPresentational extends AbstractApplicationFormPresentational<Deposit> implements OnInit {
  readonly DATE_FORMAT_EXPECTED: string;

  matrixAccessSensitivityDataUsePolicy: MappingObject<Enums.Access.AccessEnum, MappingObject<Enums.DataSensitivity.DataSensitivityEnum, Enums.Deposit.DataUsePolicyEnum[]>> = {
    [Enums.Access.AccessEnum.PUBLIC]: {
      [Enums.DataSensitivity.DataSensitivityEnum.UNDEFINED]: [
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.BLUE]: [
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
    },
    [Enums.Access.AccessEnum.RESTRICTED]: {
      [Enums.DataSensitivity.DataSensitivityEnum.UNDEFINED]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.BLUE]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.GREEN]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.YELLOW]: [
        Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA,
      ],
    },
    [Enums.Access.AccessEnum.CLOSED]: {
      [Enums.DataSensitivity.DataSensitivityEnum.UNDEFINED]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.BLUE]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.GREEN]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.YELLOW]: [
        Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.ORANGE]: [
        Enums.Deposit.DataUsePolicyEnum.SIGNED_DUA,
        Enums.Deposit.DataUsePolicyEnum.EXTERNAL_DUA,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.RED]: [
        Enums.Deposit.DataUsePolicyEnum.SIGNED_DUA,
        Enums.Deposit.DataUsePolicyEnum.EXTERNAL_DUA,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.CRIMSON]: [
        Enums.Deposit.DataUsePolicyEnum.SIGNED_DUA,
        Enums.Deposit.DataUsePolicyEnum.EXTERNAL_DUA,
      ],
    },
  } as MappingObject<Enums.Access.AccessEnum, MappingObject<Enums.DataSensitivity.DataSensitivityEnum, Enums.Deposit.DataUsePolicyEnum[]>>;

  matrixMetadataBefore3_1AccessSensitivityDataUsePolicy: MappingObject<Enums.Access.AccessEnum, MappingObject<Enums.DataSensitivity.DataSensitivityEnum, Enums.Deposit.DataUsePolicyEnum[]>> = {
    [Enums.Access.AccessEnum.PUBLIC]: {
      [Enums.DataSensitivity.DataSensitivityEnum.UNDEFINED]: [
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.BLUE]: [
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
    },
    [Enums.Access.AccessEnum.RESTRICTED]: {
      [Enums.DataSensitivity.DataSensitivityEnum.UNDEFINED]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.BLUE]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.GREEN]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.YELLOW]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
    },
    [Enums.Access.AccessEnum.CLOSED]: {
      [Enums.DataSensitivity.DataSensitivityEnum.UNDEFINED]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.BLUE]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.GREEN]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.YELLOW]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.ORANGE]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.RED]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
      [Enums.DataSensitivity.DataSensitivityEnum.CRIMSON]: [
        Enums.Deposit.DataUsePolicyEnum.NONE,
        Enums.Deposit.DataUsePolicyEnum.LICENSE,
      ],
    },
  } as MappingObject<Enums.Access.AccessEnum, MappingObject<Enums.DataSensitivity.DataSensitivityEnum, Enums.Deposit.DataUsePolicyEnum[]>>;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  accessEnumValues: KeyValue[] = Enums.Access.AccessEnumTranslate;

  embargoEnumValues: KeyValue[] = [];

  additionalFieldsValues: SolidifyObject;

  formFormly: FormGroup;

  @Input()
  listArchiveMasterTypes: ArchiveType[];

  @Input()
  orgUnitLogo: string;

  @Input()
  isTourMode: boolean = false;

  @Input()
  isMetadataEdit: boolean = false;

  @Input()
  sip: Sip;

  @ViewChild("fileDuaInput")
  fileDuaInput: ElementRef;

  addFieldsForm: AdditionalFieldsForm;

  @Input("overlayPosition")
  overlayPosition: OverlayPositionEnum = OverlayPositionEnum.bottom;

  get depositActionNameSpace(): ResourceFileNameSpace {
    return depositActionNameSpace;
  }

  get depositState(): typeof DepositState {
    return DepositState;
  }

  get accessEnum(): typeof Enums.Access.AccessEnum {
    return Enums.Access.AccessEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  get packageStatusEnum(): typeof Enums.Package.StatusEnum {
    return Enums.Package.StatusEnum;
  }

  get dataUsePolicyShowEnum(): typeof Enums.Deposit.DataUsePolicyShowEnum {
    return Enums.Deposit.DataUsePolicyShowEnum;
  }

  get duaTypeEnum(): typeof Enums.Deposit.DuaTypeEnum {
    return Enums.Deposit.DuaTypeEnum;
  }

  @Input()
  set additionalFieldsForm(additionalFieldsForm: AdditionalFieldsForm) {
    if (isNotNullNorUndefined(additionalFieldsForm)) {
      this.addFieldsForm = additionalFieldsForm;
      this.fields = JSON.parse(additionalFieldsForm.description);
    }
  }

  fields: FormlyFieldConfig[];

  @Input()
  defaultPlatformLicenseId: string;

  @Input()
  defaultIdentifierType: Enums.Deposit.DepositIdentifierTypeEnum;

  @Input()
  isReady: boolean = false;

  @Input()
  languages: Language[];

  @Input()
  listSubmissionPolicies: SubmissionPolicy[];

  @Input()
  listPreservationPolicies: PreservationPolicy[];

  @Input()
  listSubjectAreas: SubjectArea[];

  private _currentLanguageUpperCase: string;

  @Input()
  set currentLanguage(value: Enums.Language.LanguageEnum) {
    this._currentLanguageUpperCase = value?.toUpperCase();
  }

  private _selectedPersons: Person[];

  @Input()
  set selectedPersons(value: Person[]) {
    this._selectedPersons = value;

    const authorsFormControl = this.form?.get(this.formDefinition.authors);
    if (isNotNullNorUndefined(authorsFormControl)) {
      authorsFormControl.setValue(this.selectedPersons?.map(p => p.resId));
    }
  }

  get selectedPersons(): Person[] {
    return this._selectedPersons;
  }

  @Input()
  personConnected: Person;

  @Input()
  currentApplicationRole: Enums.UserApplicationRole.UserApplicationRoleEnum;

  @Input()
  orgUnit: OrganizationalUnit;

  @Input()
  editMode: DepositEditModeEnum;

  private _tabMode: ModeDepositTabEnum;

  @Input()
  set tabMode(value: ModeDepositTabEnum) {
    this._tabMode = value;
  }

  get tabMode(): ModeDepositTabEnum {
    return this._tabMode;
  }

  duaDownloadUrl: string;

  private _dataFileDua: DataFile;

  @Input()
  set dataFileDua(value: DataFile) {
    this.duaDownloadUrl = isNullOrUndefined(value) ? null : `${ApiEnum.preIngestDeposits}/${value.infoPackage.resId}/${ApiResourceNameEnum.DATAFILE}/${value.resId}/${ApiActionNameEnum.DL}`;
    this._dataFileDua = value;
  }

  get dataFileDua(): DataFile {
    return this._dataFileDua;
  }

  @ViewChild("authorMultiSearchableSelect")
  authorMultiSearchableSelect: SearchableMultiSelectPresentational<SharedPersonStateModel, Person>;

  defaultLicenseId: string;
  commonValueLicenseId: string[];

  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;

  get userApplicationRoleEnum(): typeof Enums.UserApplicationRole.UserApplicationRoleEnum {
    return Enums.UserApplicationRole.UserApplicationRoleEnum;
  }

  get depositIdentifierTypeEnum(): typeof Enums.Deposit.DepositIdentifierTypeEnum {
    return Enums.Deposit.DepositIdentifierTypeEnum;
  }

  get modeDepositTabEnum(): typeof ModeDepositTabEnum {
    return ModeDepositTabEnum;
  }

  get depositEditModeEnum(): typeof DepositEditModeEnum {
    return DepositEditModeEnum;
  }

  sharedPersonSort: Sort<Person> = {
    field: "lastName",
    order: OrderEnum.ascending,
  };
  sharedPersonActionNameSpace: ResourceNameSpace = sharedPersonActionNameSpace;
  sharedPersonState: typeof SharedPersonState = SharedPersonState;

  sharedLicenseSort: Sort<License> = {
    field: "title",
    order: OrderEnum.ascending,
  };
  sharedLicenseActionNameSpace: ResourceNameSpace = sharedLicenseActionNameSpace;
  sharedLicenseState: typeof SharedLicenseState = SharedLicenseState;
  licenseCallback: (value: License) => string = (value: License) => value.openLicenseId + " (" + value.title + ")";

  sharedSubjectAreaSort: Sort<SubjectArea> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedSubjectAreaActionNameSpace: ResourceNameSpace = sharedSubjectAreaActionNameSpace;
  sharedSubjectAreaState: typeof SharedSubjectAreaState = SharedSubjectAreaState;
  subjectAreaLabelCallback: (value: SubjectArea) => string = (value: SubjectArea) => {
    let name = value.name;
    const label: Label = value?.labels?.find((l: Label) => l.language?.resId === this._currentLanguageUpperCase);
    if (isNotNullNorUndefined(label) && isNonEmptyString(label.text)) {
      name = label.text;
    }
    return `[${value.source}] ${name}`;
  };

  lastSelectionContributorLocalStorageKey: LocalStorageEnum = undefined;

  personOverlayComponent: Type<SharedPersonOverlayPresentational> = SharedPersonOverlayPresentational;
  personOverlayExtra: SharedPersonOverlayExtra = {
    retrieveInstitution: true,
  };
  licenseOverlayComponent: Type<SharedLicenseOverlayPresentational> = SharedLicenseOverlayPresentational;

  archiveTypeMasterLabel: (value: ArchiveType) => string = value => SharedArchiveTypeState.labelCallback(value, this._translateService);

  protected override _listFieldNameToDisplayErrorInToast: string[] = [
    this.formDefinition.organizationalUnitId,
  ];

  get metadataVersionEnum(): typeof Enums.MetadataVersion.MetadataVersionEnum {
    return Enums.MetadataVersion.MetadataVersionEnum;
  }

  listSensitivityOptions: KeyValueOption[];
  listDuaPolicyOptions: KeyValueOption[];
  listDuaTypeOptions: KeyValueOption[];

  dataFileAdapter: (dataFile: DataFile) => SolidifyDataFileModel = DataFileHelper.dataFileAdapter;

  get isRequiredDuaFile(): boolean {
    return this.form.get(this.formDefinition.duaFileChange)?.hasValidator(SolidifyFileUploadInputRequiredValidator);
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              private readonly _dialog: MatDialog,
              public readonly breakpointService: BreakpointService,
              private readonly _translateService: TranslateService,
              private readonly _cookieConsentService: CookieConsentService,
              private readonly _fileVisualizerService: FileVisualizerService,
              private readonly _dateService: DateService,
              private readonly _downloadService: DownloadService,
              private readonly _orcidService: OrcidService,
              private readonly _securityService: SecurityService,
  ) {
    super(_changeDetectorRef, _elementRef, _injector);

    this.DATE_FORMAT_EXPECTED = this._dateService.getInputDateFormatExpected();

    this.subscribe(this._cookieConsentService.watchCookiePreferenceUpdateObs(CookieType.localStorage, LocalStorageEnum.multiSelectLastSelectionContributor)
      .pipe(
        tap(p => {
          this._setLastSelectionContributorIfFeatureEnabled();
          this._changeDetectorRef.detectChanges();
        }),
      ));
    this._setLastSelectionContributorIfFeatureEnabled();
  }

  ngOnInit(): void {
    if (this.isTourMode) {
      this._bindFormTo({
        title: "Deposit Tour",
        additionalFieldsValues: "{}",
        status: Enums.Deposit.StatusEnum.IN_PROGRESS,
      } as any);
      return;
    }
    super.ngOnInit();

    this._cleanDataSensitivityWhenAccessLevelChange();
    this._cleanDuaPolicyWhenDataSensitivityChange();
    this._cleanDuaTypeAndLicenseWhenDataPolicyChange();
    this._disableDuaPolicyWhenDataSensitivityUndefined();
    this._licenseRequiredIfDataPolicyIsLicense();
    this._duaTypeRequiredIfDataPolicyIsDuaChange();
    this._duaFileRequiredIfDuaTypeNeedFileChange();
    this._validationEmbargo();
    this._cleanErrorOnLinkedInputAccessDataSensitivityAndDataUsePolicy();
    this._computeOptionsSensitivityDataUsePolicyAndDuaTypeWhenValueChange();
    this.defaultLicenseId = this.orgUnit?.defaultLicense?.resId ?? this.defaultPlatformLicenseId;
    this.commonValueLicenseId = [this.defaultLicenseId];
  }

  private _computeOptionsSensitivityDataUsePolicyAndDuaTypeWhenValueChange(): void {
    this._computeOptionsSensitivityDataUsePolicyAndDuaType();
    this.subscribe(merge(
      this.form.get(this.formDefinition.access).valueChanges,
      this.form.get(this.formDefinition.dataSensitivity).valueChanges,
      this.form.get(this.formDefinition.dataUsePolicy).valueChanges,
    )
      .pipe(
        debounceTime(10),
        map(() => {
          this._computeOptionsSensitivityDataUsePolicyAndDuaType();
        }),
      ));
  }

  private _computeOptionsSensitivityDataUsePolicyAndDuaType(): void {
    this.listSensitivityOptions = [];
    this.listDuaPolicyOptions = [];
    this.listDuaTypeOptions = [];

    const accessLevelSelected: Enums.Access.AccessEnum = this.form.get(this.formDefinition.access)?.value;
    const dataSensitivitySelected: Enums.DataSensitivity.DataSensitivityEnum = this.form.get(this.formDefinition.dataSensitivity)?.value;

    const matrix = this.metadataVersionSupportDua ? this.matrixAccessSensitivityDataUsePolicy : this.matrixMetadataBefore3_1AccessSensitivityDataUsePolicy;
    const sensitivityMapAvailable = MappingObjectUtil.get(matrix, accessLevelSelected);
    const sensitivityKeyAvailable = MappingObjectUtil.keys(sensitivityMapAvailable);

    this.listSensitivityOptions = (Enums.DataSensitivity.DataSensitivityEnumTranslate as KeyValueOption[])
      .map(s => {
        const dataSensitivity = s.key as Enums.DataSensitivity.DataSensitivityEnum;
        s.enabled = sensitivityKeyAvailable.includes(dataSensitivity);
        return s;
      });

    if (isNullOrUndefinedOrWhiteString(dataSensitivitySelected)) {
      return;
    }

    const dataUsePolicyAvailable = MappingObjectUtil.get(sensitivityMapAvailable, dataSensitivitySelected);
    this.listDuaPolicyOptions = (Enums.Deposit.DataUsePolicyShowEnumTranslate as KeyValueOption[])
      .map(s => {
        const dataUsePolicyShow = s.key as Enums.Deposit.DataUsePolicyShowEnum;
        let isEnabled = false;
        if (dataUsePolicyShow === Enums.Deposit.DataUsePolicyShowEnum.NONE) {
          isEnabled = dataUsePolicyAvailable.includes(Enums.Deposit.DataUsePolicyEnum.NONE);
        } else if (dataUsePolicyShow === Enums.Deposit.DataUsePolicyShowEnum.LICENSE) {
          isEnabled = dataUsePolicyAvailable.includes(Enums.Deposit.DataUsePolicyEnum.LICENSE);
        } else if (dataUsePolicyShow === Enums.Deposit.DataUsePolicyShowEnum.DUA) {
          isEnabled = dataUsePolicyAvailable.includes(Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA)
            || dataUsePolicyAvailable.includes(Enums.Deposit.DataUsePolicyEnum.SIGNED_DUA)
            || dataUsePolicyAvailable.includes(Enums.Deposit.DataUsePolicyEnum.EXTERNAL_DUA);
        }
        s.enabled = isEnabled;
        return s;
      });

    this.listDuaTypeOptions = (Enums.Deposit.DuaTypeEnumTranslate as KeyValueOption[])
      .map(s => {
        const dataUseType = s.key as Enums.Deposit.DuaTypeEnum;
        let isEnabled = false;
        if (dataUseType === Enums.Deposit.DuaTypeEnum.CLICK_THROUGH_DUA) {
          isEnabled = dataUsePolicyAvailable.includes(Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA);
        } else if (dataUseType === Enums.Deposit.DuaTypeEnum.SIGNED_DUA) {
          isEnabled = dataUsePolicyAvailable.includes(Enums.Deposit.DataUsePolicyEnum.SIGNED_DUA);
        } else if (dataUseType === Enums.Deposit.DuaTypeEnum.EXTERNAL_DUA) {
          isEnabled = dataUsePolicyAvailable.includes(Enums.Deposit.DataUsePolicyEnum.EXTERNAL_DUA);
        }
        s.enabled = isEnabled;
        return s;
      });
  }

  private _setLastSelectionContributorIfFeatureEnabled(): void {
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, LocalStorageEnum.multiSelectLastSelectionContributor)) {
      this.lastSelectionContributorLocalStorageKey = LocalStorageEnum.multiSelectLastSelectionContributor;
    } else {
      this.lastSelectionContributorLocalStorageKey = undefined;
    }
  }

  protected override _modelUpdated(oldValue: Deposit | undefined, newValue: Deposit): void {
    super._modelUpdated(oldValue, newValue);
    if (isNotNullNorUndefined(newValue) && isNotNullNorUndefined(this.form)) {
      const fcStatus = this.form.get(this.formDefinition.status);
      if (isNotNullNorUndefined(fcStatus)) {
        fcStatus.setValue(newValue.status);
      }
      const fcArk = this.form.get(this.formDefinition.ark);
      if (isNotNullNorUndefined(fcArk)) {
        fcArk.setValue(newValue.ark);
      }
      const fcDoi = this.form.get(this.formDefinition.doi);
      if (isNotNullNorUndefined(fcDoi)) {
        fcDoi.setValue(newValue.doi);
      }
    }
  }

  private _cleanErrorOnLinkedInputAccessDataSensitivityAndDataUsePolicy(): void {
    this.subscribe(FormValidationHelper.cleanErrorOnLinkedFormControlWhenValueChange(this.form, this.formDefinition.access, [this.formDefinition.dataSensitivity, this.formDefinition.dataUsePolicy, this.formDefinition.duaType]));
    this.subscribe(FormValidationHelper.cleanErrorOnLinkedFormControlWhenValueChange(this.form, this.formDefinition.dataSensitivity, [this.formDefinition.access, this.formDefinition.dataUsePolicy, this.formDefinition.duaType]));
    this.subscribe(FormValidationHelper.cleanErrorOnLinkedFormControlWhenValueChange(this.form, this.formDefinition.dataUsePolicy, [this.formDefinition.access, this.formDefinition.dataSensitivity, this.formDefinition.duaType]));
    this.subscribe(FormValidationHelper.cleanErrorOnLinkedFormControlWhenValueChange(this.form, this.formDefinition.duaType, [this.formDefinition.access, this.formDefinition.dataSensitivity, this.formDefinition.dataUsePolicy]));
  }

  private _cleanDataSensitivityWhenAccessLevelChange(): void {
    this.subscribe(this.form.get(this.formDefinition.access).valueChanges.pipe(
      distinctUntilChanged(),
      tap((accessLevel: Enums.Access.AccessEnum) => {
        this.form.get(this.formDefinition.dataSensitivity).setValue(undefined);
      }),
    ));
  }

  private _cleanDuaPolicyWhenDataSensitivityChange(): void {
    this.subscribe(this.form.get(this.formDefinition.dataSensitivity).valueChanges.pipe(
      distinctUntilChanged(),
      tap((dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum) => {
        this.form.get(this.formDefinition.dataUsePolicy).setValue(undefined);
      }),
    ));
  }

  get metadataVersionSupportDua(): boolean {
    return isNullOrUndefined(this.model) || ![
      MetadataVersionEnum._1_0,
      MetadataVersionEnum._1_1,
      MetadataVersionEnum._2_0,
      MetadataVersionEnum._2_1,
      MetadataVersionEnum._3_0,
    ].includes(this.model?.metadataVersion);
  }

  private _cleanDuaTypeAndLicenseWhenDataPolicyChange(): void {
    this.subscribe(this.form.get(this.formDefinition.dataUsePolicy).valueChanges.pipe(
      distinctUntilChanged(),
      tap((dataUsePolicy: Enums.Deposit.DataUsePolicyShowEnum) => {
        this.form.get(this.formDefinition.duaType).setValue(undefined);
      }),
    ));
  }

  private _disableDuaPolicyWhenDataSensitivityUndefined(): void {
    const dataUsePolicyFormControl = this.form.get(this.formDefinition.dataUsePolicy);
    this.subscribe(this.form.get(this.formDefinition.dataSensitivity).valueChanges.pipe(
      distinctUntilChanged(),
      tap((dataUsePolicy: Enums.DataSensitivity.DataSensitivityEnum | undefined) => {

        if (isNullOrUndefinedOrWhiteString(dataUsePolicy)) {
          if (!dataUsePolicyFormControl.disabled) {
            dataUsePolicyFormControl.disable();
          }
        } else {
          if (!dataUsePolicyFormControl.enabled) {
            dataUsePolicyFormControl.enable();
          }
        }
      }),
    ));
  }

  private _licenseRequiredIfDataPolicyIsLicense(): void {
    const licenseFormControl = this.form.get(this.formDefinition.licenseId);
    this.subscribe(this.form.get(this.formDefinition.dataUsePolicy).valueChanges.pipe(
      distinctUntilChanged(),
      tap((dataUsePolicy: Enums.Deposit.DataUsePolicyShowEnum) => {
        if (dataUsePolicy === Enums.Deposit.DataUsePolicyShowEnum.LICENSE) {
          licenseFormControl.setValidators([Validators.required]);
        } else {
          licenseFormControl.setValidators([]);
        }
        licenseFormControl.updateValueAndValidity();
      }),
    ));
  }

  private _duaTypeRequiredIfDataPolicyIsDuaChange(): void {
    const dataUsePolicyFormControl = this.form.get(this.formDefinition.dataUsePolicy);
    this.subscribe(dataUsePolicyFormControl.valueChanges.pipe(
      distinctUntilChanged(),
      tap((dataUsePolicy: Enums.Deposit.DataUsePolicyShowEnum) => {
        this._duaTypeRequiredIfDataPolicyIsDua(dataUsePolicy);
      }),
    ));
    this._duaTypeRequiredIfDataPolicyIsDua(dataUsePolicyFormControl.value);
  }

  private _duaTypeRequiredIfDataPolicyIsDua(dataUsePolicy: Enums.Deposit.DataUsePolicyShowEnum): void {
    const duaTypeFormControl = this.form.get(this.formDefinition.duaType);
    if (dataUsePolicy === Enums.Deposit.DataUsePolicyShowEnum.DUA) {
      duaTypeFormControl.setValidators([Validators.required]);
    } else {
      duaTypeFormControl.setValidators([]);
    }
    duaTypeFormControl.updateValueAndValidity();
  }

  private _duaFileRequiredIfDuaTypeNeedFileChange(): void {
    const duaTypeFormControl = this.form.get(this.formDefinition.duaType);
    this.subscribe(duaTypeFormControl.valueChanges.pipe(
      distinctUntilChanged(),
      tap((duaType: Enums.Deposit.DuaTypeEnum) => {
        this._duaFileRequiredIfDuaTypeNeedFile(duaType);
      }),
    ));
    this._duaFileRequiredIfDuaTypeNeedFile(duaTypeFormControl.value);
  }

  private _duaFileRequiredIfDuaTypeNeedFile(duaType: Enums.Deposit.DuaTypeEnum): void {
    const duaFileChangeFormControl = this.form.get(this.formDefinition.duaFileChange);
    if ([Enums.Deposit.DuaTypeEnum.CLICK_THROUGH_DUA, Enums.Deposit.DuaTypeEnum.SIGNED_DUA].includes(duaType)) {
      duaFileChangeFormControl.setValidators([SolidifyFileUploadInputRequiredValidator]);
    } else {
      duaFileChangeFormControl.setValidators([]);
    }
    duaFileChangeFormControl.updateValueAndValidity();
  }

  protected _initNewForm(): void {
    this._computeEmbargoAccessLevelList(Enums.Access.AccessEnum.PUBLIC);
    const formDesc = {
      [this.formDefinition.organizationalUnitId]: [this.orgUnit.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.title]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.languageId]: [""],
      [this.formDefinition.publicationDate]: [new Date(), [Validators.required, SolidifyValidator]],
      [this.formDefinition.collectionBegin]: [""],
      [this.formDefinition.collectionEnd]: [""],
      [this.formDefinition.access]: [Enums.Access.AccessEnum.PUBLIC, [Validators.required, SolidifyValidator]],
      [this.formDefinition.dataSensitivity]: [Enums.DataSensitivity.DataSensitivityEnum.BLUE, [Validators.required, SolidifyValidator]],
      [this.formDefinition.dataUsePolicy]: [Enums.Deposit.DataUsePolicyEnum.LICENSE, [Validators.required, SolidifyValidator]],
      [this.formDefinition.duaType]: [undefined, [Validators.required, SolidifyValidator]],
      [this.formDefinition.duaFileChange]: [undefined, [SolidifyValidator]],
      [this.formDefinition.licenseId]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.hasEmbargo]: [false],
      [this.formDefinition.submissionPolicyId]: [this.orgUnit?.defaultSubmissionPolicy?.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.preservationPolicyId]: [this.orgUnit?.defaultPreservationPolicy?.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.authors]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.keywords]: [[...this.orgUnit?.keywords], [SolidifyValidator]],
      [this.formDefinition.archiveTypeMasterId]: [undefined, [SolidifyValidator]],
      [this.formDefinition.archiveTypeId]: [undefined, [SolidifyValidator]],
      [this.formDefinition.ark]: [null, {disabled: true}],
      [this.formDefinition.doi]: [null, {disabled: true}],
      [this.formDefinition.isIdenticalTo]: ["", [SolidifyValidator]],
      [this.formDefinition.isReferencedBy]: [[], [SolidifyValidator]],
      [this.formDefinition.isObsoletedBy]: ["", [SolidifyValidator]],
      [this.formDefinition.embargoAccess]: [null],
      [this.formDefinition.embargoNumberMonths]: [""],
      [this.formDefinition.contentStructurePublic]: [false, [SolidifyValidator]],
      [this.formDefinition.subjectAreas]: [this.listSubjectAreas?.map(i => i.resId), [SolidifyValidator]],
    };

    if (isNotNullNorUndefined(this.addFieldsForm)) {
      formDesc[this.formDefinition.additionalFieldsFormId] = [this.addFieldsForm.resId, [Validators.required, SolidifyValidator]];
    }

    this._computePreviewSubmissionAgreement(this.orgUnit?.defaultSubmissionPolicy);

    this.form = this._fb.group(formDesc);
    this.formFormly = this._fb.group({});
  }

  protected _bindFormTo(deposit: Deposit): void {
    this._computeEmbargoAccessLevelList(deposit.access);
    this._computePreviewSubmissionAgreement(deposit.submissionPolicy);
    let dataUsePolicy;
    let duaType;
    if (deposit.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.CLICK_THROUGH_DUA
      || deposit.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.SIGNED_DUA
      || deposit.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.EXTERNAL_DUA) {
      dataUsePolicy = Enums.Deposit.DataUsePolicyShowEnum.DUA;
      duaType = deposit.dataUsePolicy;
    } else { // License
      dataUsePolicy = deposit.dataUsePolicy;
      duaType = undefined;
    }
    let formDesc = {
      [this.formDefinition.organizationalUnitId]: [deposit.organizationalUnitId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.title]: [deposit.title, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [deposit.description, [Validators.required, SolidifyValidator]],
      [this.formDefinition.languageId]: [deposit.languageId],
      [this.formDefinition.publicationDate]: [deposit.publicationDate, [Validators.required, SolidifyValidator]],
      [this.formDefinition.collectionBegin]: [DateUtil.convertOffsetDateTimeIso8601ToDate(deposit.collectionBegin), [SolidifyValidator]],
      [this.formDefinition.collectionEnd]: [DateUtil.convertOffsetDateTimeIso8601ToDate(deposit.collectionEnd), [SolidifyValidator]],
      [this.formDefinition.access]: [deposit.access, [Validators.required, SolidifyValidator]],
      [this.formDefinition.dataSensitivity]: [deposit.dataSensitivity, [Validators.required, SolidifyValidator]],
      [this.formDefinition.dataUsePolicy]: [dataUsePolicy, [Validators.required, SolidifyValidator]],
      [this.formDefinition.duaType]: [duaType, [Validators.required, SolidifyValidator]],
      [this.formDefinition.duaFileChange]: [isNotNullNorUndefined(this.dataFileDua) ? SolidifyFileUploadStatus.UNCHANGED : undefined, [SolidifyFileUploadInputRequiredValidator, SolidifyValidator]],
      [this.formDefinition.licenseId]: [deposit.licenseId, [SolidifyValidator]],
      [this.formDefinition.hasEmbargo]: [deposit.hasEmbargo],
      [this.formDefinition.submissionPolicyId]: [isNullOrUndefined(deposit.submissionPolicyId) ? this.orgUnit?.defaultSubmissionPolicy?.resId : deposit.submissionPolicyId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.preservationPolicyId]: [isNullOrUndefined(deposit.preservationPolicyId) ? this.orgUnit?.defaultPreservationPolicy?.resId : deposit.preservationPolicyId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.authors]: [this.selectedPersons?.map(p => p.resId), [Validators.required, SolidifyValidator]],
      [this.formDefinition.keywords]: [isNullOrUndefined(deposit.keywords) ? [] : [...deposit.keywords], [SolidifyValidator]],
      [this.formDefinition.archiveTypeMasterId]: [deposit.archiveType?.masterType?.resId, [SolidifyValidator]],
      [this.formDefinition.archiveTypeId]: [deposit.archiveTypeId, [SolidifyValidator]],
      [this.formDefinition.ark]: [deposit.ark, {disabled: true}],
      [this.formDefinition.doi]: [deposit.doi, {disabled: true}],
      [this.formDefinition.isIdenticalTo]: [deposit.isIdenticalTo, [SolidifyValidator]],
      [this.formDefinition.isReferencedBy]: [isNullOrUndefined(deposit.isReferencedBy) ? [] : [...deposit.isReferencedBy], [SolidifyValidator]],
      [this.formDefinition.isObsoletedBy]: [deposit.isObsoletedBy, [SolidifyValidator]],
      [this.formDefinition.embargoAccess]: [isNullOrUndefined(deposit.embargo) ? "" : deposit.embargo.access],
      [this.formDefinition.embargoNumberMonths]: [isNullOrUndefined(deposit.embargo) ? "" : deposit.embargo.months],
      [this.formDefinition.embargoStartDate]: [isNullOrUndefined(deposit.embargo?.startDate) ? "" : DateUtil.convertDateToDateTimeString(new Date(deposit.embargo.startDate))],
      [this.formDefinition.status]: [deposit.status, [Validators.required, SolidifyValidator]],
      [this.formDefinition.contentStructurePublic]: [deposit.contentStructurePublic, [SolidifyValidator]],
      [this.formDefinition.subjectAreas]: [this.listSubjectAreas?.map(i => i.resId), [SolidifyValidator]],
    };

    if (isNotNullNorUndefined(deposit.additionalFieldsFormId)) {
      formDesc = {
        ...formDesc,
        [this.formDefinition.additionalFieldsFormId]: [deposit.additionalFieldsFormId, [Validators.required, SolidifyValidator]],
      };
    }

    this.form = this._fb.group(formDesc);

    this.formFormly = this._fb.group({});
    this.additionalFieldsValues = JSON.parse(deposit.additionalFieldsValues);

    this.isValidWhenDisable = this.form.valid;
  }

  protected _treatmentBeforeSubmit(deposit: Deposit): Deposit {
    deposit.publicationDate = DateUtil.convertToLocalDateDateSimple(deposit.publicationDate);
    deposit.collectionBegin = DateUtil.convertToOffsetDateTimeIso8601(deposit.collectionBegin);
    deposit.collectionEnd = DateUtil.convertToOffsetDateTimeIso8601(deposit.collectionEnd);
    deposit.collectionEnd = DateUtil.convertToOffsetDateTimeIso8601(deposit.collectionEnd);
    if (isNotNullNorUndefined(this.formFormly.value) && MappingObjectUtil.size(this.formFormly.value) > 0) {
      deposit.additionalFieldsValues = JSON.stringify(this.formFormly.value);
    } else {
      deposit.additionalFieldsValues = null;
    }
    if (deposit.hasEmbargo) {
      deposit.embargo = {
        access: this.form.get(this.formDefinition.embargoAccess).value === "" ? null : this.form.get(this.formDefinition.embargoAccess).value,
        months: this.form.get(this.formDefinition.embargoNumberMonths).value === "" ? null : this.form.get(this.formDefinition.embargoNumberMonths).value,
      };
    } else {
      deposit.embargo = null;
    }
    if (deposit.access === Enums.Access.AccessEnum.PUBLIC) {
      deposit.contentStructurePublic = true;
    }
    deposit.archiveTypeId = this.form.get(this.formDefinition.archiveTypeId).value === undefined ? null : this.form.get(this.formDefinition.archiveTypeId).value;
    if (isNullOrUndefinedOrWhiteString(deposit.archiveTypeId)) {
      deposit.archiveType = null;
    } else {
      deposit.archiveType = this.listArchiveMasterTypes.find(c => c.resId === deposit.archiveTypeId);
    }

    deposit.subjectAreas = [];
    if (isNonEmptyArray(this.form.get(this.formDefinition.subjectAreas).value)) {
      this.form.get(this.formDefinition.subjectAreas).value.forEach(resId => {
        deposit.subjectAreas.push({resId: resId});
      });
    }

    if (this.form.get(this.formDefinition.dataUsePolicy).value === Enums.Deposit.DataUsePolicyShowEnum.DUA) {
      deposit.dataUsePolicy = this.form.get(this.formDefinition.duaType).value;
    }

    if (this.form.get(this.formDefinition.dataUsePolicy).value !== Enums.Deposit.DataUsePolicyShowEnum.LICENSE) {
      deposit[this.formDefinition.licenseId] = null;
    }
    delete deposit[this.formDefinition.duaType];
    delete deposit[this.formDefinition.embargoNumberMonths];
    delete deposit[this.formDefinition.embargoAccess];
    delete deposit[this.formDefinition.status];
    delete deposit[this.formDefinition.archiveTypeMasterId];
    return deposit;
  }

  private _validationEmbargo(): void {
    const embargoDurationFormControl = this.form.get(this.formDefinition.embargoNumberMonths);
    const embargoAccessFormControl = this.form.get(this.formDefinition.embargoAccess);
    this.subscribe(this.form.get(this.formDefinition.hasEmbargo).valueChanges.pipe(
      distinctUntilChanged(),
      tap(hasEmbargo => {
        if (hasEmbargo === true) {
          embargoDurationFormControl.setValidators([Validators.required, Validators.min(1)]);
          embargoAccessFormControl.setValidators([Validators.required]);
        } else {
          embargoDurationFormControl.setValidators([]);
          embargoAccessFormControl.setValidators([]);
        }
        embargoDurationFormControl.updateValueAndValidity();
        embargoAccessFormControl.updateValueAndValidity();
      }),
    ));
  }

  protected _disableSpecificField(): void {
    this.form.get(this.formDefinition.ark).disable();
    this.form.get(this.formDefinition.doi).disable();
    if (isTrue(this.isMetadataEdit)) {
      if (!this._securityService.isRoot()) {
        this.form.get(this.formDefinition.preservationPolicyId).disable();
      }
      this.form.get(this.formDefinition.submissionPolicyId).disable();
    }
  }

  addPeople(): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositPersonDialog, {}, {
      width: "90%",
    }, person => {
      this.addAuthor(person);
    }));
  }

  addMeAsAuthor(): void {
    this.addAuthor(this.personConnected);
  }

  addAuthor(person: Person): void {
    this.authorMultiSearchableSelect.add(person);
  }

  personConnectedAlreadyInAuthor(): boolean {
    return this.form.get(this.formDefinition.authors).value.includes(this.personConnected.resId);
  }

  navigateToPerson(person: Person): void {
    this.navigate([RoutesEnum.adminPersonDetail, person.resId]);
  }

  navigateToSubjectArea(): void {
    this.navigate([RoutesEnum.adminSubjectArea]);
  }

  extraInfoCallback(person: Person): void {
    this._orcidService.openOrcidPage(person.orcid);
  }

  extraInfoPlaceholderCallback(person: Person): string | undefined {
    if (isNullOrUndefined(person) || isNullOrUndefinedOrWhiteString(person.orcid)) {
      return undefined;
    }
    return person.verifiedOrcid ? LabelTranslateEnum.orcidAuthenticated : LabelTranslateEnum.orcidNotAuthenticated;
  }

  orderAuthors(listContributor: Person[]): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositOrderAuthorDialog, {
        authorsId: this.form.get(this.formDefinition.authors).value,
        persons: listContributor,
      },
      {
        width: "500px",
      },
      authorsIdSorted => {
        if (isNonEmptyArray(authorsIdSorted)) {
          const formControl = this.form.get(this.formDefinition.authors);
          formControl.setValue(authorsIdSorted);
          formControl.markAsDirty();
        }
      }));
  }

  formlyChange(): void {
    if (this.form.dirty) {
      return;
    }
    this.form.markAsDirty();
    this._changeDetectorRef.detectChanges();
  }

  adaptEmbargoAccessLevel(value: Enums.Access.AccessEnum, fromCheckbox: boolean = false): void {
    this._computeEmbargoAccessLevelList(value);
    const fcAccessLevel = this.form.get(this.formDefinition.access);
    const fcEmbargoAccess = this.form.get(this.formDefinition.embargoAccess);

    if (fcEmbargoAccess.value === fcAccessLevel.value) {
      this.form.get(this.formDefinition.hasEmbargo).setValue(false);
      fcEmbargoAccess.setValue(null);
      return;
    }

    if (isEmptyArray(this.embargoEnumValues)) {
      this.form.get(this.formDefinition.hasEmbargo).setValue(false);
    } else if (fromCheckbox && isTrue(this.form.get(this.formDefinition.hasEmbargo).value) && this.embargoEnumValues.length === 1) {
      fcEmbargoAccess.setValue(this.embargoEnumValues[0].key);
    }
  }

  private _computeEmbargoAccessLevelList(value: Enums.Access.AccessEnum): void {
    this.embargoEnumValues = Enums.Access.EmbargoAccessLevelEnumList(value);
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  get statusEnum(): typeof Enums.Deposit.StatusEnum {
    return Enums.Deposit.StatusEnum;
  }

  get depositStatusEnumToTranslate(): typeof Enums.Deposit.StatusEnumTranslate {
    return Enums.Deposit.StatusEnumTranslate;
  }

  logoChange(blob: Blob | undefined | null): void {
    this.currentLogoBlob = blob;
    if (this.readonly) {
      this.enterInEditMode();
    }
    if (!this.form.dirty) {
      setTimeout(() => {
        this.form.markAsDirty();
        this._dirtyBS.next(this.form.dirty);
        this._changeDetectorRef.detectChanges();
      }, 0);
    }
  }

  goToDoi(doi: string, mouseEvent?: MouseEvent): void {
    if (isNotNullNorUndefined(mouseEvent)) {
      mouseEvent.stopPropagation();
    }
    SsrUtil.window.open(environment.doiLink + doi);
  }

  get displayLicenseSelect(): boolean {
    return this.form.get(this.formDefinition.dataUsePolicy).value === Enums.Deposit.DataUsePolicyShowEnum.LICENSE;
  }

  cleanCollectionPeriod(): void {
    this.form.get(this.formDefinition.collectionBegin).setValue("");
    this.form.get(this.formDefinition.collectionEnd).setValue("");
  }

  submissionAgreementDownloadUrl: string;
  submissionAgreementFileInput: FileInput;
  canPreviewSubmissionAgreement: boolean;

  computePreviewSubmissionAgreementWithSubmissionPolicyId(submissionPolicyId: string): void {
    const submissionPolicy = this.listSubmissionPolicies.find(s => s.resId === submissionPolicyId);
    this._computePreviewSubmissionAgreement(submissionPolicy);
  }

  private _computePreviewSubmissionAgreement(submissionPolicy: SubmissionPolicy): void {
    this.submissionAgreementDownloadUrl = null;
    this.submissionAgreementFileInput = null;
    this.canPreviewSubmissionAgreement = false;

    const submissionAgreementFile = submissionPolicy?.submissionAgreement?.submissionAgreementFile;

    if (isNullOrUndefined(submissionAgreementFile)) {
      return;
    }
    this.submissionAgreementDownloadUrl = `${ApiEnum.adminSubmissionAgreements}/${submissionPolicy.submissionAgreement.resId}/${ApiActionNameEnum.DOWNLOAD_FILE}`;
    this.submissionAgreementFileInput = {
      dataFile: {
        fileName: submissionAgreementFile.fileName,
        mimetype: submissionAgreementFile.mimeType,
        fileSize: submissionAgreementFile.fileSize,
        status: FileStatusEnum.READY,
      },
    };
    this.canPreviewSubmissionAgreement = FileVisualizerHelper.canHandle(this._fileVisualizerService.listFileVisualizer, this.submissionAgreementFileInput);

  }

  downloadSubmissionAgreement(): void {
    this.subscribe(this._downloadService.downloadInMemory(this.submissionAgreementDownloadUrl, StringUtil.replaceNonIso8859_1Chars(this.submissionAgreementFileInput.dataFile.fileName)));
  }

  previewSubmissionAgreement(): void {
    this.subscribe(DialogUtil.open(this._dialog, FilePreviewDialog, {
        fileInput: this.submissionAgreementFileInput,
        fileDownloadUrl: this.submissionAgreementDownloadUrl,
      },
      {
        width: "max-content",
        maxWidth: "90vw",
        height: "min-content",
      }));
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() organizationalUnitId: string;
  @PropertyName() title: string;
  @PropertyName() description: string;
  @PropertyName() languageId: string;
  @PropertyName() publicationDate: string;
  @PropertyName() collectionBegin: string;
  @PropertyName() collectionEnd: string;
  @PropertyName() authors: string;
  @PropertyName() access: string;
  @PropertyName() dataSensitivity: string;
  @PropertyName() hasEmbargo: string;
  @PropertyName() embargoAccess: string;
  @PropertyName() embargoNumberMonths: string;
  @PropertyName() embargoStartDate: string;
  @PropertyName() licenseId: string;
  @PropertyName() submissionPolicyId: string;
  @PropertyName() preservationPolicyId: string;
  @PropertyName() keywords: string;
  @PropertyName() subjectAreas: string;
  @PropertyName() archiveTypeMasterId: string;
  @PropertyName() archiveTypeId: string;
  @PropertyName() ark: string;
  @PropertyName() doi: string;
  @PropertyName() isIdenticalTo: string;
  @PropertyName() isReferencedBy: string;
  @PropertyName() isObsoletedBy: string;
  @PropertyName() additionalFieldsFormId: string;
  @PropertyName() status: string;
  @PropertyName() dataUsePolicy: string;
  @PropertyName() duaType: string;
  @PropertyName() contentStructurePublic: string;
  @PropertyName() duaFileChange: string;
}
