/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-readme-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  OnInit,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {ReadmeWrapper} from "@deposit/models/readme-wrapper.model";
import {TranslateService} from "@ngx-translate/core";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AbstractFormPresentational,
  BaseFormDefinition,
  BreakpointService,
  CookieConsentService,
  DateService,
  DownloadService,
  FileVisualizerService,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-readme-form",
  templateUrl: "./deposit-readme-form.presentational.html",
  styleUrls: ["./deposit-readme-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositReadmeFormPresentational extends AbstractFormPresentational<ReadmeWrapper> implements OnInit {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              private readonly _dialog: MatDialog,
              public readonly breakpointService: BreakpointService,
              private readonly _translateService: TranslateService,
              private readonly _cookieConsentService: CookieConsentService,
              private readonly _fileVisualizerService: FileVisualizerService,
              private readonly _dateService: DateService,
              private readonly _downloadService: DownloadService,
  ) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.text]: ["", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(model: ReadmeWrapper): void {
    this.form = this._fb.group({
      [this.formDefinition.text]: [model.text, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(model: ReadmeWrapper): ReadmeWrapper {
    return model;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() text: string;
}
