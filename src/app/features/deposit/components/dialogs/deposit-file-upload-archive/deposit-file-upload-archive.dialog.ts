/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-file-upload-archive.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  OnInit,
  ViewChild,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {
  AbstractDepositFileUploadDialog,
  AbstractDepositFileUploadDialogData,
} from "@deposit/components/dialogs/abstract-deposit-file-upload/abstract-deposit-file-upload.dialog";
import {DlcmFileUploadWrapper} from "@deposit/models/dlcm-file-upload-wrapper.model";
import {Store} from "@ngxs/store";
import {
  EnumUtil,
  HTMLInputEvent,
  isNotNullNorUndefined,
  isNullOrUndefined,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-file-upload-archive-dialog",
  templateUrl: "./deposit-file-upload-archive.dialog.html",
  styleUrls: ["./deposit-file-upload-archive.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFileUploadArchiveDialog extends AbstractDepositFileUploadDialog<DepositFileUploadArchiveDialogData, DlcmFileUploadWrapper> implements OnInit {
  archiveToUpload: File;

  @ViewChild("fileInput")
  fileInput: ElementRef;

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  constructor(protected readonly _dialogRef: MatDialogRef<DepositFileUploadArchiveDialog>,
              protected readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositFileUploadArchiveDialogData,
              protected readonly _store: Store) {
    super(_dialogRef, _fb, data, _store);
  }

  onFileChange(event: HTMLInputEvent): void {
    if (event.target.files.length > 0) {
      const files: FileList = event.target.files;
      this.archiveToUpload = files[0];
    }
    if (isNotNullNorUndefined(this.fileInput)) {
      this.fileInput.nativeElement.value = null;
    }
  }

  onSubmit(): void {
    const base = this.form.value as DlcmFileUploadWrapper;
    const fileUploadWrapper = {} as DlcmFileUploadWrapper;
    Object.assign(fileUploadWrapper, base);
    fileUploadWrapper.file = this.archiveToUpload;
    this.submit(fileUploadWrapper);
  }

  delete(file: File): void {
    this.archiveToUpload = undefined;
  }

  isFileNameInvalid(): boolean {
    if (isNullOrUndefined(this.archiveToUpload)) {
      return false;
    }
    const fileName = this.archiveToUpload.name;
    return this.REGEX_INVALID.test(fileName);
  }

  override dropFile(fileList: FileList | File[]): void {
    this.archiveToUpload = fileList[0];
  }
}

export interface DepositFileUploadArchiveDialogData extends AbstractDepositFileUploadDialogData {
}
