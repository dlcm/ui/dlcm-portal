/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-reject.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {DepositState} from "@deposit/stores/deposit.state";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {Observable} from "rxjs/index";
import {
  FormValidationHelper,
  MemoizedUtil,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-reject-dialog",
  templateUrl: "./deposit-reject.dialog.html",
  styleUrls: ["./deposit-reject.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositRejectDialog extends SharedAbstractDialog<DepositRejectDialogData, string> implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositState);

  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<DepositRejectDialog>,
              private readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositRejectDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.message]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  onSubmit(): void {
    this.submit(this.form.get(this.formDefinition.message).value);
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }
}

export interface DepositRejectDialogData {
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() message: string;
}
