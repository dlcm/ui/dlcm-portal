/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-submit.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {Enums} from "@enums";
import {Deposit} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {sharedLicenseActionNameSpace} from "@shared/stores/license/shared-license.action";
import {SharedLicenseState} from "@shared/stores/license/shared-license.state";
import {
  EnumUtil,
  FileInput,
  FileStatusEnum,
  isNullOrUndefined,
  ResourceNameSpace,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-submit-dialog",
  templateUrl: "./deposit-submit.dialog.html",
  styleUrls: ["./deposit-submit.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositSubmitDialog extends SharedAbstractDialog<DepositSubmitDialogData, boolean> {
  sharedLicenceState: typeof SharedLicenseState = SharedLicenseState;
  sharedLicenceStateNameSpace: ResourceNameSpace = sharedLicenseActionNameSpace;

  formControlAcceptSubmissionAgreement: FormControl;
  submissionAgreementDownloadUrl: string;
  submissionAgreementFileInput: FileInput;

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<DepositSubmitDialog>,
              private readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositSubmitDialogData) {
    super(_dialogRef, data);
    const submissionPolicy = this.data.deposit.submissionPolicy;
    if (this.data.needSubmissionAgreement) {
      const submissionAgreement = submissionPolicy.submissionAgreement;
      const submissionAgreementFile = submissionAgreement.submissionAgreementFile;
      this.formControlAcceptSubmissionAgreement = this._fb.control(false, [Validators.requiredTrue]);
      this.submissionAgreementDownloadUrl = `${ApiEnum.adminSubmissionAgreements}/${submissionAgreement.resId}/${ApiActionNameEnum.DOWNLOAD_FILE}`;
      this.submissionAgreementFileInput = {
        dataFile: {
          fileName: submissionAgreementFile.fileName,
          mimetype: submissionAgreementFile.mimeType,
          fileSize: submissionAgreementFile.fileSize,
          status: FileStatusEnum.READY,
        },
      };
    }
  }

  onSubmit(): void {
    this.submit(true);
  }

  accessEnumTranslate(value: string): string {
    return EnumUtil.getLabel(Enums.Access.AccessEnumTranslate, value);
  }

  sensitivityEnumTranslate(value: string): string {
    return EnumUtil.getLabel(Enums.DataSensitivity.DataSensitivityEnumTranslate, value);
  }

  dataUsePolicyEnumTranslate(value: string): string {
    return EnumUtil.getLabel(Enums.Deposit.DataUsePolicyEnumTranslate, value);
  }

  isDuaCompatibleWithMetadataVersion(): boolean {
    return !(this.data.deposit.metadataVersion === Enums.MetadataVersion.MetadataVersionEnum._1_0 ||
      this.data.deposit.metadataVersion === Enums.MetadataVersion.MetadataVersionEnum._1_1 ||
      this.data.deposit.metadataVersion === Enums.MetadataVersion.MetadataVersionEnum._2_0 ||
      this.data.deposit.metadataVersion === Enums.MetadataVersion.MetadataVersionEnum._2_1 ||
      this.data.deposit.metadataVersion === Enums.MetadataVersion.MetadataVersionEnum._3_0);
  }

  get getEmbargoEndDate(): Date {
    const start = new Date();
    const months = this.data.deposit?.embargo?.months;
    if (isNullOrUndefined(start) || isNullOrUndefined(months)) {
      return undefined;
    }
    const startDate = new Date(start);
    return new Date(startDate.setMonth(startDate.getMonth() + months));
  }
}

export interface DepositSubmitDialogData {
  deposit: Deposit;
  needSubmissionAgreement: boolean;
}
