/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-person-alternative.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {Person} from "@models";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {isNullOrUndefined} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-alternative-dialog",
  templateUrl: "./deposit-person-alternative.dialog.html",
  styleUrls: ["./deposit-person-alternative.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositPersonAlternativeDialog extends SharedAbstractDialog<DepositPersonAlternativeDialogData, AlternativeDepositPersonSelected> {
  selectedDepositPerson: AlternativeDepositPersonSelected;

  constructor(protected readonly _dialogRef: MatDialogRef<DepositPersonAlternativeDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositPersonAlternativeDialogData) {
    super(_dialogRef, data);
  }

  select(selected: Person, isNew: boolean): void {
    this.selectedDepositPerson = {
      selected: selected,
      isNew: isNew,
    };
  }

  isSelected(current: Person): boolean {
    if (isNullOrUndefined(this.selectedDepositPerson)) {
      return false;
    }
    return current === this.selectedDepositPerson.selected;
  }
}

export interface DepositPersonAlternativeDialogData {
  newPerson: Person;
  listExistingPerson: Person[];
}

export interface AlternativeDepositPersonSelected {
  selected: Person;
  isNew: boolean;
}
