/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-associate-archive.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {DepositArchiveAction} from "@deposit/stores/archive/deposit-archive.action";
import {DepositArchiveState} from "@deposit/stores/archive/deposit-archive.state";
import {Enums} from "@enums";
import {Archive} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {ArchiveDataTableHelper} from "@shared/helpers/archive-data-table.helper";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {Observable} from "rxjs";
import {
  map,
  tap,
} from "rxjs/operators";
import {
  DataTableColumns,
  FormValidationHelper,
  isEmptyArray,
  isNullOrUndefined,
  MemoizedUtil,
  PropertyName,
  QueryParameters,
} from "solidify-frontend";

@Component({
  selector: "dlcm-associate-archive-dialog",
  templateUrl: "./deposit-associate-archive.dialog.html",
  styleUrls: ["./deposit-associate-archive.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositAssociateArchiveDialog extends SharedAbstractDialog<DepositAssociateArchiveDialogData, string[]> implements OnInit, OnDestroy {
  form: FormGroup;
  formDefinition: DepositAipUploadFormComponentFormDefinition = new DepositAipUploadFormComponentFormDefinition();

  listArchiveObs: Observable<Archive[]> = MemoizedUtil.select(this._store, DepositArchiveState, state => state.list).pipe(
    map(list => {
      const listIdArchiveAlreadyLinked = this.data.listIdArchiveAlreadyLinked;
      if (isNullOrUndefined(list) || isEmptyArray(list) || isNullOrUndefined(listIdArchiveAlreadyLinked) || isEmptyArray(listIdArchiveAlreadyLinked)) {
        return list;
      }
      return list.filter(archive => !listIdArchiveAlreadyLinked.includes(archive.resId));
    }),
  );
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, DepositArchiveState, state => state.queryParameters);
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositArchiveState);
  searchModeIsQueryObs: Observable<boolean> = MemoizedUtil.select(this._store, DepositArchiveState, state => state.searchModeIsQuery);

  selectedArchives: Archive[] = [];

  get selectedArchivesIds(): string[] {
    return this.selectedArchives.map(a => a.resId);
  }

  searchObs: Observable<string>;

  columns: DataTableColumns[] = ArchiveDataTableHelper.searchColumns;

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  constructor(protected readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<DepositAssociateArchiveDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositAssociateArchiveDialogData,
              protected readonly _fb: FormBuilder) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.search]: [""],
      [this.formDefinition.searchModeIsQuery]: [false],
    });

    this.searchTextChange();

    this.subscribe(this.form.get(this.formDefinition.searchModeIsQuery).valueChanges.pipe(
      tap(searchModeIsQuery => {
          this._search();
        },
      ),
    ));
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this._store.dispatch(new DepositArchiveAction.Clean());
  }

  searchTextChange(searchTerm: string = undefined): void {
    this.form.get(this.formDefinition.search).setValue(searchTerm);
    this._search();
  }

  private _search(): void {
    const searchTerm = this.form.get(this.formDefinition.search).value;
    const searchModeIsQuery = this.form.get(this.formDefinition.searchModeIsQuery).value;
    this._store.dispatch(new DepositArchiveAction.Search(true, this.data.organizationalUnitId, this.data.dataSensitivity, searchTerm, searchModeIsQuery));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new DepositArchiveAction.ChangeQueryParameters(this.data.organizationalUnitId, this.data.dataSensitivity, queryParameters));
  }

  onSubmit(): void {
    this.submit(this.selectedArchivesIds);
  }

  selectionChange(archive: Archive): void {
    const indexOf = this.selectedArchives.findIndex(a => a.resId === archive.resId);
    if (indexOf !== -1) {
      this.removeArchive(indexOf);
    } else {
      this.selectedArchives.push(archive);
    }
  }

  removeArchive(index: number): void {
    this.selectedArchives.splice(index, 1);
  }
}

export class DepositAipUploadFormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() search: string;
  @PropertyName() searchModeIsQuery: string;
}

export interface DepositAssociateArchiveDialogData {
  listIdArchiveAlreadyLinked: string[];
  dataSensitivity: Enums.DataSensitivity.DataSensitivityEnum;
  organizationalUnitId: string;
}
