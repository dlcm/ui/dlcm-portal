/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-order-author.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  CdkDragDrop,
  moveItemInArray,
} from "@angular/cdk/drag-drop";
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {Person} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {isNullOrUndefined} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-order-dialog",
  templateUrl: "./deposit-order-author.dialog.html",
  styleUrls: ["./deposit-order-author.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositOrderAuthorDialog extends SharedAbstractDialog<DepositOrderAuthorDialogWrapper, string[]> implements OnInit {
  listAuthorsSorted: Person[];

  constructor(protected readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<DepositOrderAuthorDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositOrderAuthorDialogWrapper) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.listAuthorsSorted = [];
    this.data.authorsId.forEach(id => {
      const author = this.data.persons.find(person => person.resId === id);
      if (isNullOrUndefined(author)) {
        return;
      }
      this.listAuthorsSorted.push(author);
    });
  }

  validate(): void {
    this.submit(this.listAuthorsSorted.map(a => a.resId));
  }

  trackByFn(index: number, person: Person): string {
    return person.resId;
  }

  up(index: number, person: Person): void {
    if (index === 0) {
      return;
    }
    this.listAuthorsSorted.splice(index, 1);
    this.listAuthorsSorted.splice(index - 1, 0, person);
  }

  down(index: number, person: Person): void {
    if (index === this.listAuthorsSorted.length) {
      return;
    }
    this.listAuthorsSorted.splice(index, 1);
    this.listAuthorsSorted.splice(index + 1, 0, person);
  }

  drop(event: CdkDragDrop<Person[]>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(this.listAuthorsSorted, event.previousIndex, event.currentIndex);
    }
  }
}

export interface DepositOrderAuthorDialogWrapper {
  persons: Person[];
  authorsId: string[];
}
