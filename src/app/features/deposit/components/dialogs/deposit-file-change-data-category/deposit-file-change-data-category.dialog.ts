/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-file-change-data-category.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {DepositDataFileAction} from "@deposit/stores/data-file/deposit-data-file.action";
import {DepositDataFileState} from "@deposit/stores/data-file/deposit-data-file.state";
import {depositDataFileMetadataTypeActionNameSpace} from "@deposit/stores/data-file/metadata-type/deposit-data-file-metadata-type.action";
import {DepositDataFileMetadataTypeState} from "@deposit/stores/data-file/metadata-type/deposit-data-file-metadata-type.state";
import {Enums} from "@enums";
import {
  DepositDataFile,
  MetadataType,
} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SharedDataCategoryAction} from "@shared/stores/data-category/shared-data-category.action";
import {SharedDataCategoryState} from "@shared/stores/data-category/shared-data-category.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  EnumUtil,
  FormValidationHelper,
  isNonEmptyString,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  MemoizedUtil,
  PropertyName,
  ResourceNameSpace,
  SolidifyValidator,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-file-change-data-category-dialog",
  templateUrl: "./deposit-file-change-data-category.dialog.html",
  styleUrls: ["./deposit-file-change-data-category.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositFileChangeDataCategoryDialog extends SharedAbstractDialog<DepositFileChangeDataCategoryDialogData, boolean> implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, DepositDataFileState);

  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  metadataTypeLabelCallback: (value: MetadataType) => string = value => value.fullName;

  depositDataFileMetadataTypeActionNameSpace: ResourceNameSpace = depositDataFileMetadataTypeActionNameSpace;
  depositDataFileMetadataTypeState: typeof DepositDataFileMetadataTypeState = DepositDataFileMetadataTypeState;

  listDataCategoriesObs: Observable<string[]> = MemoizedUtil.select(this._store, SharedDataCategoryState, state => state.listDataCategories);
  listDataTypesObs: Observable<string[]> = MemoizedUtil.select(this._store, SharedDataCategoryState, state => state.listDataTypes);

  get enumsDataCategoryAndType(): typeof Enums.DataFile.DataCategoryAndType {
    return Enums.DataFile.DataCategoryAndType;
  }

  get enumUtil(): typeof EnumUtil {
    return EnumUtil;
  }

  isFormValid: boolean = true;

  get dataTypeEnum(): typeof Enums.DataFile.DataCategoryAndType.DataTypeEnum {
    return Enums.DataFile.DataCategoryAndType.DataTypeEnum;
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<DepositFileChangeDataCategoryDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositFileChangeDataCategoryDialogData,
              private readonly _fb: FormBuilder,
              private readonly _changeDetector: ChangeDetectorRef,
  ) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();

    this._store.dispatch(new SharedDataCategoryAction.GetAllDataCategories());

    this.form = this._fb.group({
      [this.formDefinition.dataCategory]: [undefined, [Validators.required, SolidifyValidator]],
      [this.formDefinition.dataType]: [undefined, [Validators.required, SolidifyValidator]],
      [this.formDefinition.metadataType]: [this.data.depositDataFile.metadataType?.resId, [
        ...(isNotNullNorUndefinedNorWhiteString(this.data.depositDataFile.metadataType?.resId) ? [Validators.required] : []),
        SolidifyValidator]],
    });

    this.subscribe(this.form.get(this.formDefinition.dataCategory).valueChanges.pipe(
      distinctUntilChanged(),
      tap(dataCategory => {
        this._getDataTypesByCategory(dataCategory);
      }),
    ));

    this.subscribe(this.form.get(this.formDefinition.dataType).valueChanges.pipe(
      tap(value => {
        const metadataFormControl = this.form.get(this.formDefinition.metadataType);
        if (value === Enums.DataFile.DataCategoryAndType.DataTypeEnum.CUSTOM_METADATA) {
          metadataFormControl.setValidators([Validators.required, SolidifyValidator]);
        } else {
          metadataFormControl.setValidators([SolidifyValidator]);
        }
        metadataFormControl.updateValueAndValidity();
      }),
    ));

    this.form.get(this.formDefinition.dataCategory).setValue(this.data.depositDataFile.dataCategory);
    this.form.get(this.formDefinition.dataType).setValue(this.data.depositDataFile.dataType);
  }

  private _getDataTypesByCategory(category: string): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedDataCategoryAction.GetAllDataTypesByCategory(category),
      SharedDataCategoryAction.GetAllDataTypesByCategorySuccess,
      result => {
        this._changeDetector.detectChanges();
      }));
  }

  onSubmit(): void {
    let metadata: MetadataType = null;
    if (!isNullOrUndefined(this.form.get(this.formDefinition.metadataType).value) && isNonEmptyString(this.form.get(this.formDefinition.metadataType).value)) {
      metadata = {
        resId: this.form.get(this.formDefinition.metadataType).value,
      } as MetadataType;
    }

    const action = new DepositDataFileAction.ChangeDataCategory(this.data.parentResId, {
      resId: this.data.depositDataFile.resId,
      dataCategory: this.form.get(this.formDefinition.dataCategory).value,
      dataType: this.form.get(this.formDefinition.dataType).value,
      metadataType: !isNullOrUndefined(metadata) ? metadata : undefined,
    });

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      action,
      DepositDataFileAction.ChangeDataCategorySuccess,
      resultAction => {
        this.submit(true);
      }));
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  selectCategory(): void {
    this.isFormValid = false;
  }

  selectDataType(): void {
    this.isFormValid = true;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() dataCategory: string;
  @PropertyName() dataType: string;
  @PropertyName() metadataType: string;
}

export interface DepositFileChangeDataCategoryDialogData {
  parentResId: string;
  depositDataFile: DepositDataFile;
}
