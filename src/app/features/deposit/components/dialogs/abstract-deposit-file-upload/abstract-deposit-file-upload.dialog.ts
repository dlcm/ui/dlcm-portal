/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - abstract-deposit-file-upload.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {BaseFormDefinition} from "@app/shared/models/base-form-definition.model";
import {depositDataFileMetadataTypeActionNameSpace} from "@deposit/stores/data-file/metadata-type/deposit-data-file-metadata-type.action";
import {DepositDataFileMetadataTypeState} from "@deposit/stores/data-file/metadata-type/deposit-data-file-metadata-type.state";
import {Enums} from "@enums";
import {
  MetadataType,
  SystemProperty,
} from "@models";
import {Store} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {SharedDataCategoryAction} from "@shared/stores/data-category/shared-data-category.action";
import {SharedDataCategoryState} from "@shared/stores/data-category/shared-data-category.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  AppSystemPropertyState,
  FormValidationHelper,
  isNotNullNorUndefinedNorEmptyArray,
  isTrue,
  MemoizedUtil,
  PropertyName,
  ResourceNameSpace,
} from "solidify-frontend";

@Directive()
export abstract class AbstractDepositFileUploadDialog<TData extends AbstractDepositFileUploadDialogData, UResult> extends SharedAbstractDialog<TData, UResult> implements OnInit {
  form: FormGroup;
  formDefinition: AbstractDepositFileUploadFormComponentFormDefinition = new AbstractDepositFileUploadFormComponentFormDefinition();

  get dataTypeEnum(): typeof Enums.DataFile.DataCategoryAndType.DataTypeEnum {
    return Enums.DataFile.DataCategoryAndType.DataTypeEnum;
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  metadataTypeLabelCallback: (value: MetadataType) => string = value => value.fullName;

  readonly REGEX_INVALID: RegExp = new RegExp("[~!@#$%^&*`;?,\\\\]");
  readonly files: string = "files";

  depositDataFileMetadataTypeActionNameSpace: ResourceNameSpace = depositDataFileMetadataTypeActionNameSpace;
  depositDataFileMetadataTypeState: typeof DepositDataFileMetadataTypeState = DepositDataFileMetadataTypeState;

  listDataCategoriesObs: Observable<string[]> = MemoizedUtil.select(this._store, SharedDataCategoryState, state => state.listDataCategories);
  listDataTypesObs: Observable<string[]> = MemoizedUtil.select(this._store, SharedDataCategoryState, state => state.listDataTypes);

  get enumsDataFile(): typeof Enums.DataFile.DataCategoryAndType {
    return Enums.DataFile.DataCategoryAndType;
  }

  get defaultDataCategoryType(): Enums.DataFile.DataCategoryAndType.DataCategoryEnum | undefined {
    const systemProperty = MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState<SystemProperty>, state => state.current);
    if (systemProperty.researchDataSupported) {
      return Enums.DataFile.DataCategoryAndType.DataCategoryEnum.PRIMARY;
    }
    if (systemProperty.administrativeDataSupported) {
      return Enums.DataFile.DataCategoryAndType.DataCategoryEnum.ADMINISTRATIVE;
    }
    return undefined;
  }

  get defaultDataType(): Enums.DataFile.DataCategoryAndType.DataTypeEnum {
    const systemProperty = MemoizedUtil.selectSnapshot(this._store, AppSystemPropertyState<SystemProperty>, state => state.current);
    if (systemProperty.researchDataSupported) {
      return Enums.DataFile.DataCategoryAndType.DataTypeEnum.REFERENCE;
    }
    if (systemProperty.administrativeDataSupported) {
      return Enums.DataFile.DataCategoryAndType.DataTypeEnum.DOCUMENT;
    }
    return undefined;
  }

  constructor(protected readonly _dialogRef: MatDialogRef<AbstractDepositFileUploadDialog<TData, UResult>>,
              protected readonly _fb: FormBuilder,
              protected readonly _data: TData,
              protected readonly _store: Store,
  ) {
    super(_dialogRef, _data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._store.dispatch(new SharedDataCategoryAction.GetAllDataCategories());

    const defaultDataCategory = this._data.dataCategoryEnum ?? this.defaultDataCategoryType;
    const defaultDataType = this._data.dataTypeEnum ?? this.defaultDataType;

    this.form = this._fb.group({
      [this.formDefinition.dataCategory]: [undefined, [Validators.required]],
      [this.formDefinition.dataType]: [undefined, [Validators.required]],
      [this.formDefinition.metadataType]: [""],
    });

    const formControlDataCategory = this.form.get(this.formDefinition.dataCategory);
    const formControlDataType = this.form.get(this.formDefinition.dataType);

    this.subscribe(formControlDataCategory.valueChanges.pipe(
      distinctUntilChanged(),
      tap(dataCategory => {
        this._store.dispatch(new SharedDataCategoryAction.GetAllDataTypesByCategory(dataCategory));
      }),
    ));

    this.subscribe(formControlDataType.valueChanges.pipe(
      tap(value => {
        const metadataFormControl = this.form.get(this.formDefinition.metadataType);
        if (value === Enums.DataFile.DataCategoryAndType.DataTypeEnum.CUSTOM_METADATA) {
          metadataFormControl.setValidators([Validators.required]);
        } else {
          metadataFormControl.setValidators([]);
        }
        metadataFormControl.updateValueAndValidity();
      }),
    ));

    formControlDataCategory.setValue(defaultDataCategory);
    formControlDataType.setValue(defaultDataType);

    if (isTrue(this.data.dataCategoryEnumReadOnly)) {
      formControlDataCategory.disable();
    }
    if (isTrue(this.data.dataTypeEnumReadOnly)) {
      formControlDataType.disable();
    }

    if (isNotNullNorUndefinedNorEmptyArray(this.data.files)) {
      this.dropFile(this.data.files);
    }
  }

  abstract dropFile(files: File[]): void;

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }

  abstract onFileChange(event: Event & any): void;

  getDate(lastModified: number): Date {
    return new Date(lastModified);
  }

  abstract onSubmit(): void;

  abstract delete(file: File, index?: number): void;
}

export class AbstractDepositFileUploadFormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() dataCategory: string;
  @PropertyName() dataType: string;
  @PropertyName() metadataType: string;
}

export interface AbstractDepositFileUploadDialogData {
  files?: File[];
  dataCategoryEnum?: Enums.DataFile.DataCategoryAndType.DataCategoryEnum;
  dataCategoryEnumReadOnly?: boolean;
  dataTypeEnum?: Enums.DataFile.DataCategoryAndType.DataTypeEnum;
  dataTypeEnumReadOnly?: boolean;
  enableValuesFromEnum?: string[];
}
