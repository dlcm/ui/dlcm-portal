/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-person.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from "@angular/material/dialog";
import {DepositPersonAlternativeDialog} from "@deposit/components/dialogs/deposit-person-alternative/deposit-person-alternative.dialog";
import {DepositNewPersonFormPresentational} from "@deposit/components/presentationals/deposit-new-person-form/deposit-new-person-form.presentational";
import {Person} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {SharedPersonAction} from "@shared/stores/person/shared-person.action";
import {SharedPersonState} from "@shared/stores/person/shared-person.state";
import {Observable} from "rxjs";
import {
  DialogUtil,
  isEmptyArray,
  isNullOrUndefined,
  MemoizedUtil,
  ModelFormControlEvent,
  NotificationService,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-deposit-person-dialog",
  templateUrl: "./deposit-person.dialog.html",
  styleUrls: ["./deposit-person.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DepositPersonDialog extends SharedAbstractDialog<DepositPersonDialogData, Person> {
  isLoadingPersonObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedPersonState);

  @ViewChild("formPresentational")
  readonly formPresentational: DepositNewPersonFormPresentational;

  constructor(protected readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<DepositPersonDialog>,
              private readonly _notificationService: NotificationService,
              private readonly _dialog: MatDialog,
              private readonly _actions$: Actions,
              @Inject(MAT_DIALOG_DATA) public readonly data: DepositPersonDialogData) {
    super(_dialogRef, data);
  }

  savePerson($event: ModelFormControlEvent<Person>): void {
    this._checkSimilarPerson($event);
  }

  private _checkSimilarPerson($event: ModelFormControlEvent<Person>): void {
    const action = new SharedPersonAction.Search($event.model);
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, action,
      SharedPersonAction.SearchSuccess,
      result => {
        const listPersonMatching = MemoizedUtil.selectSnapshot(this._store, SharedPersonState, state => state.listPersonMatching);
        if (isNullOrUndefined(listPersonMatching) || isEmptyArray(listPersonMatching)) {
          this._createPerson($event);
        } else {
          this._openAlternativeDialog($event, listPersonMatching);
        }
      }));
  }

  private _createPerson($event: ModelFormControlEvent<Person>): void {
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedPersonAction.Create($event),
      SharedPersonAction.CreateSuccess,
      resultAction => {
        this.submit(resultAction.model);
      }));
  }

  private _openAlternativeDialog(newPerson: ModelFormControlEvent<Person>, listMatchingPerson: Person[]): void {
    this.subscribe(DialogUtil.open(this._dialog, DepositPersonAlternativeDialog, {
        listExistingPerson: listMatchingPerson,
        newPerson: newPerson.model,
      }, undefined,
      depositPersonSelected => {
        if (depositPersonSelected.isNew) {
          this._createPerson(newPerson);
        } else {
          this.submit(depositPersonSelected.selected);
        }
      }));
  }
}

export interface DepositPersonDialogData {
}
