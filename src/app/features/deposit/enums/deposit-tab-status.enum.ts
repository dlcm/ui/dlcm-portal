/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit-tab-status.enum.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {MARK_AS_TRANSLATABLE} from "solidify-frontend";

export enum DepositTabStatusEnum {
  inProgress = "in-progress",
  inValidation = "in-validation",
  approved = "approved",
  completed = "completed",
  rejected = "rejected",
  inError = "in-error",
  all = "all",
}

export class DepositTabStatusName {
  static inProgress: string = MARK_AS_TRANSLATABLE("deposit.tabsStatus.inProgress");
  static inValidation: string = MARK_AS_TRANSLATABLE("deposit.tabsStatus.inValidation");
  static approved: string = MARK_AS_TRANSLATABLE("deposit.tabsStatus.approved");
  static completed: string = MARK_AS_TRANSLATABLE("deposit.tabsStatus.completed");
  static rejected: string = MARK_AS_TRANSLATABLE("deposit.tabsStatus.rejected");
  static inError: string = MARK_AS_TRANSLATABLE("deposit.tabsStatus.inError");
  static all: string = LabelTranslateEnum.all;
}
