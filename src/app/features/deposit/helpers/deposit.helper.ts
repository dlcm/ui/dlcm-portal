/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - deposit.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ActivatedRoute} from "@angular/router";
import {
  DepositTabStatusEnum,
  DepositTabStatusName,
} from "@deposit/enums/deposit-tab-status.enum";
import {DepositTabStatus} from "@deposit/models/deposit-tab-status.model";
import {Enums} from "@enums";
import {Deposit} from "@models";
import {Store} from "@ngxs/store";
import {
  DepositRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {
  ButtonColorEnum,
  RouterExtensionService,
} from "solidify-frontend";

export class DepositHelper {
  static readonly KEY_ORGANIZATIONAL_UNIT: keyof Deposit & string = "organizationalUnitId";
  static readonly KEY_CREATION_WHO: string = "creation.who";
  static readonly KEY_STATUS: keyof Deposit & string = "status";
  static readonly KEY_STATUS_LIST: string = "statusList";

  static readonly tabsStatus: DepositTabStatus[] = [
    {
      tabEnum: DepositTabStatusEnum.inProgress,
      depositStatusEnum: [Enums.Deposit.StatusEnum.IN_PROGRESS, Enums.Deposit.StatusEnum.EDITING_METADATA],
      labelToTranslate: DepositTabStatusName.inProgress,
      displayCounter: true,
    },
    {
      tabEnum: DepositTabStatusEnum.inValidation,
      depositStatusEnum: [Enums.Deposit.StatusEnum.IN_VALIDATION],
      labelToTranslate: DepositTabStatusName.inValidation,
      displayCounter: true,
    },
    {
      tabEnum: DepositTabStatusEnum.completed,
      depositStatusEnum: [Enums.Deposit.StatusEnum.COMPLETED, Enums.Deposit.StatusEnum.CLEANED, Enums.Deposit.StatusEnum.EDITING_METADATA_REJECTED],
      labelToTranslate: DepositTabStatusName.completed,
      displayCounter: false,
    },
    {
      tabEnum: DepositTabStatusEnum.inError,
      depositStatusEnum: [Enums.Deposit.StatusEnum.IN_ERROR, Enums.Deposit.StatusEnum.COMPLIANCE_ERROR],
      labelToTranslate: DepositTabStatusName.inError,
      displayCounter: true,
      color: ButtonColorEnum.warn,
    },
    {
      tabEnum: DepositTabStatusEnum.all,
      depositStatusEnum: undefined,
      labelToTranslate: DepositTabStatusName.all,
      displayCounter: false,
    },
  ];

  static getTabRouteSelected(route: ActivatedRoute): DepositRoutesEnum | undefined {
    const children = route.snapshot.children;
    if (children.length > 0 && children[0].url.length > 0) {
      const mode = children[0].url[0].path;
      return mode as DepositRoutesEnum;
    }
    return undefined;
  }

  static determineMode(store: Store, routerExt: RouterExtensionService): DepositMode {
    store.selectSnapshot(state => state.router.state.url);
    if (routerExt.getPreviousUrl()?.includes(RoutesEnum.preservationPlanningDeposit)) {
      return DepositMode.PRESERVATION_PLANNING_DEPOSIT;
    } else {
      return DepositMode.DEPOSIT;
    }
  }
}

export enum DepositMode {
  DEPOSIT = "DEPOSIT",
  PRESERVATION_PLANNING_DEPOSIT = "PRESERVATION_PLANNING_DEPOSIT"
}
