/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - data-file-upload.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {DepositDataFileState} from "@deposit/stores/data-file/deposit-data-file.state";
import {Enums} from "@enums";
import {Store} from "@ngxs/store";
import {
  MappingObjectUtil,
  MemoizedUtil,
} from "solidify-frontend";

export class DataFileUploadHelper {
  private static readonly _LIST_PENDING_STATUS: Enums.DataFile.StatusEnum[] = [
    Enums.DataFile.StatusEnum.RECEIVED,
    Enums.DataFile.StatusEnum.CHANGE_RELATIVE_LOCATION,
    Enums.DataFile.StatusEnum.TO_PROCESS,
    Enums.DataFile.StatusEnum.PROCESSED,
    Enums.DataFile.StatusEnum.FILE_FORMAT_IDENTIFIED,
    Enums.DataFile.StatusEnum.FILE_FORMAT_UNKNOWN,
    Enums.DataFile.StatusEnum.VIRUS_CHECKED,
    Enums.DataFile.StatusEnum.CLEANING,
  ];

  static numberFiles(store: Store): number {
    const listCurrentStatus = MemoizedUtil.selectSnapshot(store, DepositDataFileState, state => state.listCurrentStatus);
    let pendingCounter = 0;
    MappingObjectUtil.forEach(listCurrentStatus, (value, key: Enums.DataFile.StatusEnum) => {
      pendingCounter += value;
    });
    return pendingCounter;
  }

  static numberFilesInError(store: Store): number {
    const listCurrentStatus = MemoizedUtil.selectSnapshot(store, DepositDataFileState, state => state.listCurrentStatus);
    return MappingObjectUtil.get(listCurrentStatus, Enums.DataFile.StatusEnum.IN_ERROR) ?? 0;
  }

  static numberFilesIgnored(store: Store): number {
    const listCurrentStatus = MemoizedUtil.selectSnapshot(store, DepositDataFileState, state => state.listCurrentStatus);
    return MappingObjectUtil.get(listCurrentStatus, Enums.DataFile.StatusEnum.IGNORED_FILE) ?? 0;
  }

  static numberFilesExcluded(store: Store): number {
    const listCurrentStatus = MemoizedUtil.selectSnapshot(store, DepositDataFileState, state => state.listCurrentStatus);
    return MappingObjectUtil.get(listCurrentStatus, Enums.DataFile.StatusEnum.EXCLUDED_FILE) ?? 0;
  }

  static numberFilesPending(store: Store): number {
    const listCurrentStatus = MemoizedUtil.selectSnapshot(store, DepositDataFileState, state => state.listCurrentStatus);
    let pendingCounter = 0;
    MappingObjectUtil.forEach(listCurrentStatus, (value, key: Enums.DataFile.StatusEnum) => {
      if (this._LIST_PENDING_STATUS.indexOf(key) !== -1) {
        pendingCounter += value;
      }
    });
    return pendingCounter;
  }
}
