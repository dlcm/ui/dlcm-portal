/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-contributor-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {DlcmRoutes} from "@app/shared/models/dlcm-route.model";
import {PreservationSpaceContributorDetailEditRoutable} from "@preservation-space/contributor/components/routables/contributor-detail-edit/preservation-space-contributor-detail-edit.routable";
import {PreservationSpaceContributorListRoutable} from "@preservation-space/contributor/components/routables/contributor-list/preservation-space-contributor-list.routable";
import {PreservationSpaceContributorState} from "@preservation-space/contributor/stores/preservation-space-contributor.state";
import {
  AppRoutesEnum,
  ContributorRoutesEnum,
} from "@shared/enums/routes.enum";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    component: PreservationSpaceContributorListRoutable,
    data: {},
    children: [],
  },
  {
    path: ContributorRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: PreservationSpaceContributorDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: PreservationSpaceContributorState.currentTitle,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreservationSpaceContributorRoutingModule {
}
