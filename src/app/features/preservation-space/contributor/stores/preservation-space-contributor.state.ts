/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-contributor.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {Contributor} from "@models";
import {
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {
  defaultPreservationSpaceContributorDepositStateModel,
  PreservationSpaceContributorDepositState,
  PreservationSpaceContributorDepositStateModel,
} from "@preservation-space/contributor/stores/contributor-deposit/preservation-space-contributor-deposit.state";
import {preservationSpaceContributorActionNameSpace} from "@preservation-space/contributor/stores/preservation-space-contributor.action";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  ApiService,
  defaultResourceFileStateInitValue,
  DownloadService,
  isNullOrUndefined,
  NotificationService,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  StoreUtil,
} from "solidify-frontend";

export interface PreservationSpaceContributorStateModel extends ResourceFileStateModel<Contributor> {
  preservationSpace_contributor_deposit: PreservationSpaceContributorDepositStateModel;
}

@Injectable()
@State<PreservationSpaceContributorStateModel>({
  name: StateEnum.preservationSpace_contributor,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    preservationSpace_contributor_deposit: defaultPreservationSpaceContributorDepositStateModel(),
  },
  children: [
    PreservationSpaceContributorDepositState,
  ],
})
export class PreservationSpaceContributorState extends ResourceFileState<PreservationSpaceContributorStateModel, Contributor> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              private readonly _securityService: SecurityService) {
    super(_apiService, _store, _notificationService, _actions$, {nameSpace: preservationSpaceContributorActionNameSpace},
      _downloadService, ResourceFileStateModeEnum.avatar, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.preIngestContributors;
  }

  protected get _urlFileResource(): string {
    return ApiEnum.adminPeople;
  }

  @Selector()
  static isLoadingWithDependency(state: PreservationSpaceContributorStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static isLoading(state: PreservationSpaceContributorStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static currentTitle(state: PreservationSpaceContributorStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.fullName;
  }

  @Selector()
  static isReadyToBeDisplayed(state: PreservationSpaceContributorStateModel): boolean {
    return !isNullOrUndefined(state.current);
  }
}
