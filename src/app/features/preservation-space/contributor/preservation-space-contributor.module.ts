/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-contributor.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {PreservationSpaceContributorFormPresentational} from "@preservation-space/contributor/components/presentationals/contributor-form/preservation-space-contributor-form.presentational";
import {PreservationSpaceContributorDetailEditRoutable} from "@preservation-space/contributor/components/routables/contributor-detail-edit/preservation-space-contributor-detail-edit.routable";
import {PreservationSpaceContributorListRoutable} from "@preservation-space/contributor/components/routables/contributor-list/preservation-space-contributor-list.routable";
import {PreservationSpaceContributorRoutingModule} from "@preservation-space/contributor/preservation-space-contributor-routing.module";
import {PreservationSpaceContributorDepositState} from "@preservation-space/contributor/stores/contributor-deposit/preservation-space-contributor-deposit.state";

const routables = [
  PreservationSpaceContributorListRoutable,
  PreservationSpaceContributorDetailEditRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  PreservationSpaceContributorFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    PreservationSpaceContributorRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      PreservationSpaceContributorDepositState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationSpaceContributorModule {
}
