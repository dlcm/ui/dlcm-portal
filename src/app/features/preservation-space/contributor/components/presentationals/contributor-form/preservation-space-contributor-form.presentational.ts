/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-contributor-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Output,
} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {AppUserAction} from "@app/stores/user/app-user.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {Contributor} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {ImageDisplayModeEnum} from "@shared/enums/image-display-mode.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SecurityService} from "@shared/services/security.service";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  AbstractFormPresentational,
  BreakpointService,
  isNullOrUndefined,
  ObservableUtil,
  PropertyName,
  ResourceFileNameSpace,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-space-contributor-form",
  templateUrl: "./preservation-space-contributor-form.presentational.html",
  styleUrls: ["./preservation-space-contributor-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceContributorFormPresentational extends AbstractFormPresentational<Contributor> {
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  userAvatarActionNameSpace: ResourceFileNameSpace = AppUserAction;
  userState: typeof AppUserState = AppUserState;

  protected readonly _navigatePersonBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("navigatePerson")
  readonly navigatePersonObs: Observable<void | undefined> = ObservableUtil.asObservable(this._navigatePersonBS);

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get imageDisplayModeEnum(): typeof ImageDisplayModeEnum {
    return ImageDisplayModeEnum;
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly securityService: SecurityService,
              public readonly breakpointService: BreakpointService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.lastName]: [""],
      [this.formDefinition.firstName]: [""],
      [this.formDefinition.orcid]: [""],
    });
  }

  protected _bindFormTo(contributor: Contributor): void {
    this.form = this._fb.group({
      [this.formDefinition.lastName]: [contributor.lastName],
      [this.formDefinition.firstName]: [contributor.firstName],
      [this.formDefinition.orcid]: [contributor.orcid],
    });
  }

  protected _treatmentBeforeSubmit(model: Contributor): Contributor {
    return undefined;
  }

  navigateTo($event?: MouseEvent): void {
    this._navigatePersonBS.next();
    if (!isNullOrUndefined($event)) {
      $event.stopPropagation();
    }
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() lastName: string;
  @PropertyName() firstName: string;
  @PropertyName() orcid: string;
}
