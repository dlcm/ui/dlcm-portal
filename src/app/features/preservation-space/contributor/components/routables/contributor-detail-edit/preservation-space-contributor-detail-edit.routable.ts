/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-contributor-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {appAuthorizedOrganizationalUnitNameSpace} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {AppAuthorizedOrganizationalUnitState} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.state";
import {
  Contributor,
  Deposit,
  OrganizationalUnit,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {PreservationSpaceContributorDepositAction} from "@preservation-space/contributor/stores/contributor-deposit/preservation-space-contributor-deposit.action";
import {PreservationSpaceContributorDepositState} from "@preservation-space/contributor/stores/contributor-deposit/preservation-space-contributor-deposit.state";
import {preservationSpaceContributorActionNameSpace} from "@preservation-space/contributor/stores/preservation-space-contributor.action";
import {
  PreservationSpaceContributorState,
  PreservationSpaceContributorStateModel,
} from "@preservation-space/contributor/stores/preservation-space-contributor.state";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  DepositRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {sharedContributorActionNameSpace} from "@shared/stores/contributor/shared-contributor.action";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  DataTableColumns,
  DataTableFieldTypeEnum,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  ResourceNameSpace,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-space-contributor-detail-edit-routable",
  templateUrl: "./preservation-space-contributor-detail-edit.routable.html",
  styleUrls: ["./preservation-space-contributor-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceContributorDetailEditRoutable extends AbstractDetailEditCommonRoutable<Contributor, PreservationSpaceContributorStateModel> implements OnInit {
  @Select(PreservationSpaceContributorState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(PreservationSpaceContributorState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  listDepositObs: Observable<Deposit[]> = MemoizedUtil.list(this._store, PreservationSpaceContributorDepositState);
  queryParametersDepositObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, PreservationSpaceContributorDepositState);
  isLoadingDepositObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationSpaceContributorDepositState);

  appAuthorizedOrganizationalUnitSort: Sort<OrganizationalUnit> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  appAuthorizedOrganizationalUnitNameSpace: ResourceNameSpace = appAuthorizedOrganizationalUnitNameSpace;
  appAuthorizedOrganizationalUnitState: typeof AppAuthorizedOrganizationalUnitState = AppAuthorizedOrganizationalUnitState;

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedContributorActionNameSpace;

  override readonly editAvailable: boolean = false;

  override readonly deleteAvailable: boolean = false;

  readonly KEY_PARAM_NAME: keyof Contributor & string = undefined;
  columns: DataTableColumns[] = [
    {
      field: "title",
      header: LabelTranslateEnum.title,
      type: DataTableFieldTypeEnum.string,
      order: OrderEnum.none,
      isFilterable: false,
      isSortable: false,
    },
    {
      field: "organizationalUnitId" as any,
      header: LabelTranslateEnum.organizationalUnit,
      type: DataTableFieldTypeEnum.searchableSingleSelect,
      order: OrderEnum.none,
      component: DataTableComponentHelper.get(DataTableComponentEnum.organizationalUnitName),
      isFilterable: false,
      isSortable: false,
      resourceNameSpace: this.appAuthorizedOrganizationalUnitNameSpace,
      resourceState: this.appAuthorizedOrganizationalUnitState as any,
      searchableSingleSelectSort: this.appAuthorizedOrganizationalUnitSort,
    },
    {
      field: "publicationDate",
      header: LabelTranslateEnum.publicationDate,
      type: DataTableFieldTypeEnum.date,
      order: OrderEnum.none,
      isFilterable: false,
      isSortable: false,
    },
    {
      field: "creation.when" as any,
      header: LabelTranslateEnum.created,
      type: DataTableFieldTypeEnum.datetime,
      order: OrderEnum.none,
      isFilterable: true,
      isSortable: true,
    },
    {
      field: "lastUpdate.when" as any,
      header: LabelTranslateEnum.updated,
      type: DataTableFieldTypeEnum.datetime,
      order: OrderEnum.descending,
      isFilterable: true,
      isSortable: true,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.preservationSpace_contributor, _injector, preservationSpaceContributorActionNameSpace, StateEnum.preservationSpace);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new PreservationSpaceContributorDepositAction.GetAll(id));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    this._store.dispatch(new PreservationSpaceContributorDepositAction.ChangeQueryParameters(this._resId, queryParameters, true));
  }

  goToDeposit(deposit: Deposit): void {
    this._store.dispatch(new Navigate([RoutesEnum.deposit, deposit.organizationalUnitId, DepositRoutesEnum.detail, deposit.resId]));
  }

  navigateToPerson(): void {
    this.navigate([RoutesEnum.adminPersonDetail, this._resId]);
  }

}
