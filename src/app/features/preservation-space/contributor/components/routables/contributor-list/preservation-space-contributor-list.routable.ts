/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-contributor-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  Aip,
  Contributor,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {preservationSpaceContributorActionNameSpace} from "@preservation-space/contributor/stores/preservation-space-contributor.action";
import {
  PreservationSpaceContributorState,
  PreservationSpaceContributorStateModel,
} from "@preservation-space/contributor/stores/preservation-space-contributor.state";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {UserInfoDataTableColumn} from "@shared/models/user-info-data-table-column.model";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  DataTableFieldTypeEnum,
  isNotNullNorUndefined,
  OrderEnum,
  RouterExtensionService,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-space-contributor-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./preservation-space-contributor-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceContributorListRoutable extends AbstractListRoutable<Contributor, PreservationSpaceContributorStateModel> {
  readonly KEY_CREATE_BUTTON: string = undefined;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof Contributor & string = "resId";

  override stickyTopPosition: number = 0;

  preservationSpaceContributorState: typeof PreservationSpaceContributorState = PreservationSpaceContributorState;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.preservationSpace_contributor, preservationSpaceContributorActionNameSpace, _injector,
      {
        canCreate: false,
        canRefresh: true,
        canGoBack: false,
      }, StateEnum.preservationSpace);
  }

  conditionDisplayEditButton(model: Contributor | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: Contributor | undefined): boolean {
    return false;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "avatar",
        header: LabelTranslateEnum.logo,
        order: OrderEnum.none,
        resourceNameSpace: preservationSpaceContributorActionNameSpace,
        resourceState: this.preservationSpaceContributorState as any,
        isFilterable: false,
        isSortable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.logo),
        isUser: true,
        idResource: (contributor: Contributor) => contributor.resId,
        isLogoPresent: (contributor: Contributor) => isNotNullNorUndefined(contributor.avatar),
        width: "45px",
      } as UserInfoDataTableColumn<Contributor>,
      {
        field: "lastName",
        header: LabelTranslateEnum.lastName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.ascending,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "firstName",
        header: LabelTranslateEnum.firstName,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.ascending,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "orcid",
        header: LabelTranslateEnum.orcid,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.ascending,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "depositNumber",
        header: LabelTranslateEnum.deposits,
        type: DataTableFieldTypeEnum.number,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.date,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.date,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  override showDetail(model: Aip): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationSpaceContributorDetail, model.resId]));
  }
}
