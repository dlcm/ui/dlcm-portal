/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  RouterOutlet,
} from "@angular/router";
import {PreservationSpaceOrganizationalUnitPersonRoleState} from "@app/features/preservation-space/organizational-unit/stores/organizational-unit-person-role/preservation-space-organizational-unit-person-role.state";
import {preservationSpaceOrganizationalUnitActionNameSpace} from "@app/features/preservation-space/organizational-unit/stores/preservation-space-organizational-unit.action";
import {
  PreservationSpaceOrganizationalUnitState,
  PreservationSpaceOrganizationalUnitStateModel,
} from "@app/features/preservation-space/organizational-unit/stores/preservation-space-organizational-unit.state";
import {AppState} from "@app/stores/app.state";
import {AppMemberOrganizationalUnitAction} from "@app/stores/member-organizational-unit/app-member-organizational-unit.action";
import {AppMemberOrganizationalUnitState} from "@app/stores/member-organizational-unit/app-member-organizational-unit.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {HomeHelper} from "@home/helpers/home.helper";
import {PortalSearch} from "@home/models/portal-search.model";
import {
  DisseminationPolicy,
  Institution,
  OrganizationalUnit,
  PreservationPolicy,
  Role,
  SubjectArea,
  SubmissionPolicy,
  SystemProperty,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {PreservationSpaceOrganizationalUnitRequestAccessDialog} from "@preservation-space/organizational-unit/components/dialogs/organizational-unit-request-access/preservation-space-organizational-unit-request-access.dialog";
import {PreservationSpaceOrganizationalUnitDisseminationPolicyAction} from "@preservation-space/organizational-unit/stores/dissemination-policy/preservation-space-organizational-unit-dissemination-policy.action";
import {PreservationSpaceOrganizationalUnitDisseminationPolicyState} from "@preservation-space/organizational-unit/stores/dissemination-policy/preservation-space-organizational-unit-dissemination-policy.state";
import {PreservationSpaceOrganizationalUnitInstitutionAction} from "@preservation-space/organizational-unit/stores/institution/preservation-space-organizational-unit-institution.action";
import {PreservationSpaceOrganizationalUnitInstitutionState} from "@preservation-space/organizational-unit/stores/institution/preservation-space-organizational-unit-institution.state";
import {PreservationSpaceOrganizationalUnitPersonInheritedRoleState} from "@preservation-space/organizational-unit/stores/organizational-unit-person-inherited-role/preservation-space-organizational-unit-person-inherited-role.state";
import {PreservationSpaceOrganizationalUnitPersonRoleAction} from "@preservation-space/organizational-unit/stores/organizational-unit-person-role/preservation-space-organizational-unit-person-role.action";
import {PreservationSpaceOrganizationalUnitPreservationPolicyAction} from "@preservation-space/organizational-unit/stores/preservation-policy/preservation-space-organizational-unit-preservation-policy.action";
import {PreservationSpaceOrganizationalUnitPreservationPolicyState} from "@preservation-space/organizational-unit/stores/preservation-policy/preservation-space-organizational-unit-preservation-policy.state";
import {PreservationSpaceOrganizationalUnitSubjectAreaState} from "@preservation-space/organizational-unit/stores/subject-area/preservation-space-organizational-unit-subject-area-state.service";
import {PreservationSpaceOrganizationalUnitSubjectAreaAction} from "@preservation-space/organizational-unit/stores/subject-area/preservation-space-organizational-unit-subject-area.action";
import {PreservationSpaceOrganizationalUnitSubmissionPolicyAction} from "@preservation-space/organizational-unit/stores/submission-policy/preservation-space-organizational-unit-submission-policy.action";
import {PreservationSpaceOrganizationalUnitSubmissionPolicyState} from "@preservation-space/organizational-unit/stores/submission-policy/preservation-space-organizational-unit-submission-policy.state";
import {SharedResourceRoleMemberContainerMode} from "@shared/components/containers/shared-resource-role-member/shared-resource-role-member-container.component";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {PreservationSpaceOrganizationalUnitRoutesEnum} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedDisseminationPolicyState} from "@shared/stores/dissemination-policy/shared-dissemination-policy.state";
import {sharedOrganizationalUnitActionNameSpace} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedPreservationPolicyState} from "@shared/stores/preservation-policy/shared-preservation-policy.state";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {SharedSubjectAreaAction} from "@shared/stores/subject-area/shared-subject-area.action";
import {SharedSubjectAreaState} from "@shared/stores/subject-area/shared-subject-area.state";
import {SharedSubmissionPolicyState} from "@shared/stores/submission-policy/shared-submission-policy.state";
import {Observable} from "rxjs";
import {
  distinctUntilChanged,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  AppSystemPropertyState,
  ButtonColorEnum,
  ConfirmDialog,
  DialogUtil,
  isNotNullNorUndefined,
  MappingObject,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  ResourceNameSpace,
  ScrollService,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-space-organizational-unit-detail-edit-routable",
  templateUrl: "./preservation-space-organizational-unit-detail-edit.routable.html",
  styleUrls: ["./preservation-space-organizational-unit-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceOrganizationalUnitDetailEditRoutable extends AbstractDetailEditCommonRoutable<OrganizationalUnit, PreservationSpaceOrganizationalUnitStateModel> implements OnInit, OnDestroy {
  @Select(PreservationSpaceOrganizationalUnitState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(PreservationSpaceOrganizationalUnitState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  currentUserIsManagerObs: Observable<boolean> = MemoizedUtil.select(this._store, PreservationSpaceOrganizationalUnitState, state => state.currentUserIsManager);
  currentUserIsMemberOrgUnitObs: Observable<boolean> = MemoizedUtil.select(this._store, PreservationSpaceOrganizationalUnitState, state => state.currentUserIsMemberOrgUnit);
  listSubmissionPoliciesObs: Observable<SubmissionPolicy[]> = MemoizedUtil.list(this._store, SharedSubmissionPolicyState);
  listPreservationPoliciesObs: Observable<PreservationPolicy[]> = MemoizedUtil.list(this._store, SharedPreservationPolicyState);
  listDisseminationPoliciesObs: Observable<DisseminationPolicy[]> = MemoizedUtil.list(this._store, SharedDisseminationPolicyState);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);
  selectedPersonRoleObs: Observable<PersonRole[]> = MemoizedUtil.selected(this._store, PreservationSpaceOrganizationalUnitPersonRoleState);
  listInheritedPersonRolesObs: Observable<PersonRole[]> = MemoizedUtil.select(this._store, PreservationSpaceOrganizationalUnitPersonInheritedRoleState, state => state.list);
  selectedInstitutionObs: Observable<Institution[]> = MemoizedUtil.selected(this._store, PreservationSpaceOrganizationalUnitInstitutionState);
  selectedSubjectAreasObs: Observable<SubjectArea[]> = MemoizedUtil.selected(this._store, PreservationSpaceOrganizationalUnitSubjectAreaState);
  subjectAreaSourcesObs: Observable<string[]> = MemoizedUtil.select(this._store, SharedSubjectAreaState, state => state.sources);
  selectedSubmissionPoliciesObs: Observable<SubmissionPolicy[]> = MemoizedUtil.selected(this._store, PreservationSpaceOrganizationalUnitSubmissionPolicyState);
  selectedPreservationPoliciesObs: Observable<PreservationPolicy[]> = MemoizedUtil.selected(this._store, PreservationSpaceOrganizationalUnitPreservationPolicyState);
  selectedDisseminationPoliciesObs: Observable<DisseminationPolicy[]> = MemoizedUtil.selected(this._store, PreservationSpaceOrganizationalUnitDisseminationPolicyState);
  systemPropertyObs: Observable<SystemProperty> = MemoizedUtil.select(this._store, AppSystemPropertyState, state => state.current);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedOrganizationalUnitActionNameSpace;

  readonly KEY_PARAM_NAME: keyof OrganizationalUnit & string = "name";

  override readonly deleteAvailable: boolean = false;

  isManageAcl: boolean = false;

  isTour: boolean = false;

  canLeaveOrganizationalUnit: boolean = false;

  @ViewChild(RouterOutlet) outlet: RouterOutlet;

  get currentPersonId(): string {
    return MemoizedUtil.currentSnapshot(this._store, AppUserState)?.person?.resId;
  }

  get sharedResourceRoleMemberContainerMode(): typeof SharedResourceRoleMemberContainerMode {
    return SharedResourceRoleMemberContainerMode;
  }

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _securityService: SecurityService,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _scrollService: ScrollService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.preservationSpace_organizationalUnit, _injector, preservationSpaceOrganizationalUnitActionNameSpace, StateEnum.preservationSpace);
  }

  ngOnInit(): void {
    if (MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isInTourMode)) {
      this.isTour = true;
      return;
    }

    super.ngOnInit();

    this._store.dispatch(new SharedSubjectAreaAction.GetSource());

    this.subscribe(this.urlStateObs
      .pipe(
        distinctUntilChanged(),
        tap(urlState => {
          if (urlState) {
            const url = urlState.url;
            this.isManageAcl = url.includes(PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl);
            this._changeDetector.detectChanges();
            if (this.isManageAcl) {
              this._scrollService.scrollToTop(true);
            }
          }
        }),
      ));

    const listMemberOrgUnit = MemoizedUtil.listSnapshot(this._store, AppMemberOrganizationalUnitState);
    this.canLeaveOrganizationalUnit = isNotNullNorUndefined(listMemberOrgUnit?.find(p => p.resId === this._resId)?.roleFromOrganizationalUnit);
  }

  _getSubResourceWithParentId(id: string): void {
    if (this._securityService.isManagerOfOrgUnit(id)) {
      this._store.dispatch(new PreservationSpaceOrganizationalUnitSubmissionPolicyAction.GetAll(id));
      this._store.dispatch(new PreservationSpaceOrganizationalUnitPreservationPolicyAction.GetAll(id));
      this._store.dispatch(new PreservationSpaceOrganizationalUnitDisseminationPolicyAction.GetAll(id));
      this._store.dispatch(new PreservationSpaceOrganizationalUnitSubjectAreaAction.GetAll(id));
    }
    this._store.dispatch(new PreservationSpaceOrganizationalUnitInstitutionAction.GetAll(id));
  }

  requestToBeMember(): void {
    DialogUtil.open(this._dialog, PreservationSpaceOrganizationalUnitRequestAccessDialog, {
      orgUnitId: this._resId,
    }, {
      minWidth: "500px",
    });
  }

  leaveOrganizationalUnit(): void {
    this.subscribe(DialogUtil.open(this._dialog, ConfirmDialog, {
      titleToTranslate: LabelTranslateEnum.leaveTheOrganizationalUnit,
      messageToTranslate: MARK_AS_TRANSLATABLE("preservationSpace.organizationalUnit.dialog.confirmLeaveTheOrganizationalUnit.message"),
      confirmButtonToTranslate: LabelTranslateEnum.yesImSure,
      cancelButtonToTranslate: LabelTranslateEnum.cancel,
      colorConfirm: ButtonColorEnum.warn,
    }, undefined, () => {
      const action = new PreservationSpaceOrganizationalUnitPersonRoleAction.Delete(this._resId, this.currentPersonId, []);
      this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, action,
        PreservationSpaceOrganizationalUnitPersonRoleAction.DeleteSuccess,
        () => {
          this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$, new AppMemberOrganizationalUnitAction.GetAll(),
            AppMemberOrganizationalUnitAction.GetAllSuccess,
            () => {
              this.backToList();
            }));
        }));
    }));
  }

  seeArchive(orgUnit: OrganizationalUnit): void {
    const portalSearch = {
      facetsSelected: {[Enums.Facet.Name.ORG_UNIT_FACET]: [orgUnit.name]} as MappingObject<Enums.Facet.Name, string[]>,
    } as PortalSearch;

    HomeHelper.navigateToSearch(this._store, portalSearch);
  }
}
