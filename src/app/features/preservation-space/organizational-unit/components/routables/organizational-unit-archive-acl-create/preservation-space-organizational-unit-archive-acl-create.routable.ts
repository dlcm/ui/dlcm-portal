/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-archive-acl-create.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminArchiveAclStateModel} from "@admin/archive-acl/stores/admin-archive-acl.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {ArchiveACL} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {
  PreservationSpaceOrganizationalUnitArchiveAclAction,
  preservationSpaceOrganizationalUnitArchiveAclActionNameSpace,
} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.action";
import {PreservationSpaceOrganizationalUnitArchiveAclState} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.state";
import {
  AppRoutesEnum,
  PreservationSpaceOrganizationalUnitRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {Observable} from "rxjs";
import {
  AbstractCreateRoutable,
  MemoizedUtil,
  ModelFormControlEvent,
  StoreUtil,
  UrlQueryParamHelper,
} from "solidify-frontend";

@Component({
  selector: "dlcm-apreservation-space-organizational-unit-acl-create-routable",
  templateUrl: "./preservation-space-organizational-unit-archive-acl-create.routable.html",
  styleUrls: ["./preservation-space-organizational-unit-archive-acl-create.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceOrganizationalUnitArchiveAclCreateRoutable extends AbstractCreateRoutable<ArchiveACL, AdminArchiveAclStateModel> implements OnInit {
  isLoadingWithDependencyObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationSpaceOrganizationalUnitArchiveAclState);
  @Select(PreservationSpaceOrganizationalUnitArchiveAclState.isReadyToBeDisplayedInCreateMode) isReadyToBeDisplayedInCreateModeObs: Observable<boolean>;
  orgUnitId: string;

  constructor(protected readonly _store: Store,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _injector: Injector,
              protected readonly _route: ActivatedRoute) {
    super(_store, _actions$, _changeDetector, StateEnum.preservationSpace_organizationalUnit_archiveAcl, _injector, preservationSpaceOrganizationalUnitArchiveAclActionNameSpace as any, StateEnum.preservationSpace_organizationalUnit);
  }

  ngOnInit(): void {
    this._retrieveResIdFromUrl();
    this.urlQueryParameters = UrlQueryParamHelper.getQueryParamMappingObjectFromCurrentUrl();
  }

  protected _retrieveResIdFromUrl(): void {
    this.orgUnitId = this._route.snapshot.parent.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  override create(modelFormControlEvent: ModelFormControlEvent<ArchiveACL>): void {
    super.saveInProgress();

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new PreservationSpaceOrganizationalUnitArchiveAclAction.Create(modelFormControlEvent),
      PreservationSpaceOrganizationalUnitArchiveAclAction.CreateSuccess,
      resultAction => {
        this._store.dispatch(new Navigate([RoutesEnum.preservationSpaceOrganizationalUnitDetail, this.orgUnitId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl, PreservationSpaceOrganizationalUnitRoutesEnum.manageAclDetail, resultAction.model.resId]));
      }));
  }

  override backToListNavigate(): Navigate {
    return new Navigate([RoutesEnum.preservationSpaceOrganizationalUnitDetail, this.orgUnitId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl]);
  }

  protected override _cleanState(): void {
  }
}
