/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-archive-acl-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {AdminArchiveAclStateModel} from "@admin/archive-acl/stores/admin-archive-acl.state";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {ArchiveACL} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {
  PreservationSpaceOrganizationalUnitArchiveAclAction,
  preservationSpaceOrganizationalUnitArchiveAclActionNameSpace,
} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.action";
import {PreservationSpaceOrganizationalUnitArchiveAclState} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.state";
import {
  AppRoutesEnum,
  PreservationSpaceOrganizationalUnitRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  filter,
  take,
  tap,
} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  FileVisualizerService,
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTrue,
  MemoizedUtil,
  ModelFormControlEvent,
  ofSolidifyActionCompleted,
  StoreUtil,
  UrlUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-space-organizational-unit-acl-detail-edit-routable",
  templateUrl: "./preservation-space-organizational-unit-archive-acl-detail-edit.routable.html",
  styleUrls: ["./preservation-space-organizational-unit-archive-acl-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceOrganizationalUnitArchiveAclDetailEditRoutable extends AbstractDetailEditCommonRoutable<ArchiveACL, AdminArchiveAclStateModel> implements OnInit {
  isLoadingWithDependencyObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationSpaceOrganizationalUnitArchiveAclState);
  @Select(PreservationSpaceOrganizationalUnitArchiveAclState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  currentObs: Observable<ArchiveACL> = MemoizedUtil.current(this._store, PreservationSpaceOrganizationalUnitArchiveAclState).pipe(
    tap(current => this.current = current),
  );

  readonly KEY_PARAM_NAME: keyof ArchiveACL & string = "aipId";

  get orgUnitId(): string {
    return this._route.snapshot.parent.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  override readonly editAvailable: boolean = this._securityService.isStewardOfOrgUnit(this.orgUnitId, true);

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _fileVisualizerService: FileVisualizerService,
              private readonly _securityService: SecurityService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.preservationSpace_organizationalUnit_archiveAcl, _injector, preservationSpaceOrganizationalUnitArchiveAclActionNameSpace as any, StateEnum.preservationSpace_organizationalUnit);
  }

  ngOnInit(): void {
    this._retrieveResIdFromUrl();
    super.ngOnInit();
  }

  protected _getSubResourceWithParentId(id: string): void {
  }

  protected _retrieveResIdFromUrl(): void {
    this._resId = this._route.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  protected get isDeletable(): boolean {
    const archive: ArchiveACL = MemoizedUtil.selectSnapshot(this._store, PreservationSpaceOrganizationalUnitArchiveAclState, state => state.current);
    if (isNotNullNorUndefined(archive)) {
      return this._securityService.isStewardOfOrgUnit(this.orgUnitId, true) && isFalse(archive.deleted);
    }
    return false;
  }

  override edit(): void {
    if (this.isEdit) {
      return;
    }
    if (!this.editAvailable) {
      return;
    }
    this._store.dispatch(new Navigate([RoutesEnum.preservationSpaceOrganizationalUnitDetail, this.orgUnitId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl, PreservationSpaceOrganizationalUnitRoutesEnum.manageAclDetail, this._resId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAclEdit], {}, {skipLocationChange: true}));
  }

  override update(modelFormControlEvent: ModelFormControlEvent<ArchiveACL>): Observable<PreservationSpaceOrganizationalUnitArchiveAclAction.UpdateSuccess | PreservationSpaceOrganizationalUnitArchiveAclAction.UpdateFail> {
    super.saveInProgress();

    const observable = StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new PreservationSpaceOrganizationalUnitArchiveAclAction.Update(modelFormControlEvent),
      PreservationSpaceOrganizationalUnitArchiveAclAction.UpdateSuccess,
      resultAction => {
        this.backToDetail();
      });
    this.subscribe(observable);
    return observable;
  }

  protected override _cleanState(): void {
  }

  override backToListNavigate(): Navigate {
    return new Navigate([RoutesEnum.preservationSpaceOrganizationalUnitDetail, this.orgUnitId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl]);
  }

  override backToDetail(path: string[] = undefined): void {
    const fromUrl = UrlUtil.getCurrentUrlFromStoreRouterState(this._store);
    this.subscribe(this._store.dispatch(new Navigate(isNullOrUndefined(path) ? [RoutesEnum.preservationSpaceOrganizationalUnitDetail, this.orgUnitId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl, PreservationSpaceOrganizationalUnitRoutesEnum.manageAclDetail, this._resId] : path))
      .pipe(
        tap(() => {
          const toUrl = UrlUtil.getCurrentUrlFromStoreRouterState(this._store);
          if (toUrl !== fromUrl) {
            // this.formPresentational.resetFormToInitialValue();
            // TODO : Fix don't need to get by id model from backend but fix resetFormToInitialValue with component user role org unit
            // TODO : Problem currently if redirect to detail page via breadcrumb in case of Deposit
            this.retrieveCurrentModelWithUrl();
          }
        }),
      ));
  }

  override delete(): void {
    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitArchiveAclAction.DeleteSuccess),
      take(1),
      filter(result => isTrue(result.result.successful)),
      tap(result => {
        this.backToList();
      }),
    ));

    super.delete();
  }
}
