/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {
  PreservationSpaceOrganizationalUnitAction,
  preservationSpaceOrganizationalUnitActionNameSpace,
} from "@app/features/preservation-space/organizational-unit/stores/preservation-space-organizational-unit.action";
import {
  PreservationSpaceOrganizationalUnitState,
  PreservationSpaceOrganizationalUnitStateModel,
} from "@app/features/preservation-space/organizational-unit/stores/preservation-space-organizational-unit.state";
import {AppState} from "@app/stores/app.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {OrganizationalUnit} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {PreservationSpaceInstitutionState} from "@preservation-space/institution/stores/preservation-space-institution.state";
import {PreservationSpaceOrganizationalUnitRequestAccessDialog} from "@preservation-space/organizational-unit/components/dialogs/organizational-unit-request-access/preservation-space-organizational-unit-request-access.dialog";
import {
  OrgunitRequestCreationDialogResult,
  PreservationSpaceOrganizationalUnitRequestCreationDialog,
} from "@preservation-space/organizational-unit/components/dialogs/organizational-unit-request-creation/preservation-space-organizational-unit-request-creation.dialog";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {LocalStorageEnum} from "@shared/enums/local-storage.enum";
import {
  PreservationSpaceOrganizationalUnitRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {SharedNotificationAction} from "@shared/stores/notification/shared-notification.action";
import {
  AbstractListRoutable,
  CookieConsentUtil,
  CookieType,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DialogUtil,
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  LocalStorageHelper,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
  SsrUtil,
} from "solidify-frontend";

const onlyMyOrgUnitsStorageKey: LocalStorageEnum = LocalStorageEnum.adminShowOnlyMyInstitutions;
const onlyMyOrgUnitsFn: () => boolean = () => {
  const storageValue = LocalStorageHelper.getItem(onlyMyOrgUnitsStorageKey);
  if (isNullOrUndefined(storageValue)) {
    return true;
  }
  return storageValue === SOLIDIFY_CONSTANTS.STRING_TRUE;
};

@Component({
  selector: "dlcm-preservation-space-organizational-unit-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./preservation-space-organizational-unit-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceOrganizationalUnitListRoutable extends AbstractListRoutable<OrganizationalUnit, PreservationSpaceOrganizationalUnitStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof OrganizationalUnit & string = "name";

  preservationSpaceOrganizationalUnitState: typeof PreservationSpaceOrganizationalUnitState = PreservationSpaceOrganizationalUnitState;

  private _onlyMyOrgUnits: boolean = undefined;

  protected get onlyMyOrgUnits(): boolean {
    if (this._securityService.isRootOrAdmin()) {
      return false;
    }
    if (isNotNullNorUndefined(this._onlyMyOrgUnits)) {
      return this._onlyMyOrgUnits;
    }
    return onlyMyOrgUnitsFn();
  }

  protected set onlyMyOrgUnits(value: boolean) {
    this._onlyMyOrgUnits = value;
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, onlyMyOrgUnitsStorageKey)) {
      LocalStorageHelper.setItem(onlyMyOrgUnitsStorageKey, this.onlyMyOrgUnits + "");
    }
  }

  override stickyTopPosition: number = 0;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _translate: TranslateService,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.preservationSpace_organizationalUnit, preservationSpaceOrganizationalUnitActionNameSpace, _injector, {
      canCreate: false,
      canGoBack: false,
      listExtraButtons: [
        {
          color: "primary",
          icon: IconNameEnum.sendRequest,
          labelToTranslate: (current) => MARK_AS_TRANSLATABLE("organizationalUnit.button.askCreationOrgunit"),
          callback: () => this.requestCreationOrgUnit(),
          order: 40,
          dataTest: DataTestEnum.preservationSpaceOrgUnitButtonAskCreationOrgUnit,
        },
        {
          color: "primary",
          icon: null,
          labelToTranslate: (current) => LabelTranslateEnum.showOnlyMyOrganisationalUnits,
          callback: (model, buttonElementRef, checked) => this._showOnlyMyOrgUnit(checked),
          order: 40,
          isToggleButton: true,
          isToggleChecked: onlyMyOrgUnitsFn(),
          displayCondition: current => !_securityService.isRootOrAdmin(),
        },
      ],
    }, StateEnum.preservationSpace);
  }

  private _showOnlyMyOrgUnit(checked: boolean): void {
    if (checked) {
      //show only authorized org units
      const currentQueryParam = MemoizedUtil.queryParametersSnapshot(this._store, PreservationSpaceOrganizationalUnitState);
      const newQueryParam = new QueryParameters(currentQueryParam?.paging?.pageSize);
      this._store.dispatch(new PreservationSpaceOrganizationalUnitAction.LoadOnlyMyOrgUnits(newQueryParam));
      this.onlyMyOrgUnits = true;
    } else {
      // all org units
      // remove filter by name if applied
      const queryParameters = QueryParametersUtil.clone(MemoizedUtil.queryParametersSnapshot(this._store, PreservationSpaceInstitutionState));
      MappingObjectUtil.delete(QueryParametersUtil.getSearchItems(queryParameters), this.KEY_PARAM_NAME);
      this._store.dispatch(new PreservationSpaceOrganizationalUnitAction.GetAll(queryParameters));
      this.onlyMyOrgUnits = false;
    }

    this.defineColumns();
    this._changeDetector.detectChanges();
  }

  override onQueryParametersEvent(queryParameters: QueryParameters): void {
    if (this.onlyMyOrgUnits) {
      this._store.dispatch(new PreservationSpaceOrganizationalUnitAction.LoadOnlyMyOrgUnits(queryParameters));
    } else {
      this._store.dispatch(new PreservationSpaceOrganizationalUnitAction.ChangeQueryParameters(queryParameters, true));
    }
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  requestCreationOrgUnit(): void {
    if (isNullOrUndefinedOrWhiteString(environment.externalUrlForRequestOrganizationalUnitCreation)) {
      this.subscribe(DialogUtil.open(this._dialog, PreservationSpaceOrganizationalUnitRequestCreationDialog, {}, {
        minWidth: "500px",
      }, result => {
        this._sendRequestCreationOrgUnit(result);
      }));
    } else {
      const appLanguage = MemoizedUtil.selectSnapshot(this._store, AppState, state => state.appLanguage);
      let link = MappingObjectUtil.get(environment.externalUrlForRequestOrganizationalUnitCreation, appLanguage);
      if (isNullOrUndefined(link) && MappingObjectUtil.size(environment.externalUrlForRequestOrganizationalUnitCreation) > 0) {
        link = MappingObjectUtil.values(environment.externalUrlForRequestOrganizationalUnitCreation)[0];
      }
      SsrUtil.window?.open(link, "_blank");
    }
  }

  private _sendRequestCreationOrgUnit(result: OrgunitRequestCreationDialogResult): void {
    this._store.dispatch(new SharedNotificationAction.Create({
      model: {
        emitter: MemoizedUtil.currentSnapshot(this._store, AppUserState),
        message: result.message,
        objectId: result.orgUnitName,
        notificationType: {
          resId: Enums.Notification.TypeEnum.CREATE_ORGUNIT_REQUEST,
        },
      },
    }));
  }

  protected _defineActions(): DataTableActions<OrganizationalUnit>[] {
    return [
      {
        logo: IconNameEnum.edit,
        callback: model => this.goToEdit(model),
        placeholder: current => MARK_AS_TRANSLATABLE("crud.list.action.goToEdit"),
        displayOnCondition: model => this.conditionDisplayEditButton(model),
        isWrapped: false,
      },
      {
        logo: IconNameEnum.deposit,
        callback: model => this.goToDeposit(model),
        placeholder: current => MARK_AS_TRANSLATABLE("crud.list.action.goToDeposit"),
        displayOnCondition: model => this._securityService.isRootOrAdmin() || this._securityService.isMemberOfOrgUnit(model.resId),
        isWrapped: true,
      },
      {
        logo: IconNameEnum.sendRequest,
        callback: model => this.requestToBeMember(model.resId),
        placeholder: current => MARK_AS_TRANSLATABLE("organizationalUnit.requestToBeMember"),
        displayOnCondition: model => !this._securityService.isRootOrAdmin() && !this._securityService.isMemberOfOrgUnit(model.resId),
        isWrapped: true,
      },
      {
        logo: IconNameEnum.archiveAcl,
        callback: model => this.goToManageArchiveAcl(model),
        placeholder: current => LabelTranslateEnum.manageArchiveAcl,
        displayOnCondition: model => this._securityService.isStewardOfOrgUnit(model.resId),
        isWrapped: true,
      },
    ];
  }

  conditionDisplayEditButton(model: OrganizationalUnit | undefined): boolean {
    if (isNullOrUndefined(model)) {
      return true;
    }
    return this._securityService.isRootOrAdmin();
  }

  conditionDisplayDeleteButton(model: OrganizationalUnit | undefined): boolean {
    return false;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "avatar" as any,
        header: LabelTranslateEnum.logo,
        order: OrderEnum.none,
        resourceNameSpace: preservationSpaceOrganizationalUnitActionNameSpace,
        resourceState: this.preservationSpaceOrganizationalUnitState as any,
        isFilterable: false,
        isSortable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.logo),
        isUser: false,
        skipIfAvatarMissing: true,
        idResource: (organizationalUnit: OrganizationalUnit) => organizationalUnit.resId,
        isLogoPresent: (organizationalUnit: OrganizationalUnit) => isNotNullNorUndefined(organizationalUnit.logo),
        width: "45px",
      } as DataTableColumns<OrganizationalUnit>,
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.ascending,
        isFilterable: true,
        isSortable: isFalse(this.onlyMyOrgUnits),
        dataTest: DataTestEnum.preservationSpaceOrgUnitListSearchName,
      },
      {
        field: "openingDate",
        header: LabelTranslateEnum.opening,
        type: DataTableFieldTypeEnum.date,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: !this.onlyMyOrgUnits,
      },
      {
        field: "closingDate" as any,
        header: LabelTranslateEnum.closing,
        type: DataTableFieldTypeEnum.date,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: !this.onlyMyOrgUnits,
      },
      {
        field: "resId",
        header: "",
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        component: DataTableComponentHelper.get(DataTableComponentEnum.organizationalUnitMember),
        isFilterable: false,
        isSortable: false,
        width: "40px",
      },
    ];
  }

  goToDeposit(orgUnit: OrganizationalUnit): void {
    this._store.dispatch(new Navigate([RoutesEnum.deposit, orgUnit.resId]));
  }

  requestToBeMember(orgUnitResId: string): void {
    DialogUtil.open(this._dialog, PreservationSpaceOrganizationalUnitRequestAccessDialog, {
      orgUnitId: orgUnitResId,
    }, {
      minWidth: "500px",
    });
  }

  goToManageArchiveAcl(orgUnit: OrganizationalUnit): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationSpaceOrganizationalUnitDetail, orgUnit.resId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl]));
  }
}
