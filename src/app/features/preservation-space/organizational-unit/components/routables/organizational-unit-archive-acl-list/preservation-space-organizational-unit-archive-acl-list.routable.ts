/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-archive-acl-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {environment} from "@environments/environment";
import {
  Archive,
  ArchiveACL,
  User,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {PreservationSpaceOrganizationalUnitArchiveAclAction} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.action";
import {PreservationSpaceOrganizationalUnitArchiveAclState} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.state";
import {PreservationSpaceOrganizationalUnitAction} from "@preservation-space/organizational-unit/stores/preservation-space-organizational-unit.action";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  PreservationSpaceOrganizationalUnitRoutesEnum,
  PreservationSpaceRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {UserHelper} from "@shared/helpers/user.helper";
import {SecurityService} from "@shared/services/security.service";
import {StoreDialogService} from "@shared/services/store-dialog.service";
import {sharedArchiveActionNameSpace} from "@shared/stores/archive/shared-archive.action";
import {
  PARAM_QUERY_ORGUNIT,
  PARAM_QUERY_SEARCH,
  SharedArchiveState,
} from "@shared/stores/archive/shared-archive.state";
import {sharedUserActionNameSpace} from "@shared/stores/user/shared-user.action";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {
  AbstractRoutable,
  DataTableActions,
  DataTableColumns,
  DataTableFieldTypeEnum,
  DataTablePresentational,
  DeleteDialog,
  DialogUtil,
  isNullOrUndefined,
  MappingObject,
  MappingObjectUtil,
  MemoizedUtil,
  ofSolidifyActionCompleted,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  ResourceNameSpace,
  RouterExtensionService,
  SharedAbstractListRoutableOption,
  SOLIDIFY_CONSTANTS,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-space-organizational-unit-acl-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./preservation-space-organizational-unit-archive-acl-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceOrganizationalUnitArchiveAclListRoutable extends AbstractRoutable implements OnInit {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = LabelTranslateEnum.backToOrganizationalUnit;
  readonly KEY_PARAM_NAME: keyof ArchiveACL & string = "aipId";

  displayActionCopyIdToClipboard: boolean = true;
  displayActionShowHistory: boolean = false;

  detailRouteToInterpolate: string;
  skipInitialQuery: boolean = false;
  stickyTopPosition: number = environment.defaultStickyDatatableHeight;
  columnsSkippedToClear: string[] = [];
  isMultiSelectable: boolean = false;

  isHighlightCondition: (current: ArchiveACL) => boolean | undefined;

  actions: DataTableActions<ArchiveACL>[] = [
    {
      logo: IconNameEnum.edit,
      callback: (model: ArchiveACL) => this.goToEdit(model),
      placeholder: current => LabelTranslateEnum.edit,
      displayOnCondition: (model: ArchiveACL) => this.conditionDisplayEditButton(model),
    },
    {
      logo: IconNameEnum.delete,
      callback: (model: ArchiveACL) => this.delete(model),
      placeholder: current => LabelTranslateEnum.delete,
      displayOnCondition: (model: ArchiveACL) => this.conditionDisplayDeleteButton(model),
    },
  ];
  listNewId: string[];

  @ViewChild("dataTablePresentational")
  readonly dataTablePresentational: DataTablePresentational<ArchiveACL>;

  sharedArchiveNameSpace: ResourceNameSpace = sharedArchiveActionNameSpace;
  sharedArchiveState: typeof SharedArchiveState = SharedArchiveState;

  sharedUserSort: Sort<User> = {
    field: "lastName",
    order: OrderEnum.ascending,
  };
  sharedUserNameSpace: ResourceNameSpace = sharedUserActionNameSpace;
  sharedUserState: typeof SharedUserState = SharedUserState;
  extraQueryParameters: MappingObject<string, string>;

  columns: DataTableColumns<ArchiveACL>[];

  get orgUnitId(): string {
    return this._route.parent.parent.snapshot.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  canCreateEditDelete: boolean = this._securityService.isStewardOfOrgUnit(this.orgUnitId, true);

  options: SharedAbstractListRoutableOption<ArchiveACL> = {
    canCreate: this.canCreateEditDelete,
    canGoBack: true,
    canRefresh: true,
  };

  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationSpaceOrganizationalUnitArchiveAclState);
  listObs: Observable<ArchiveACL[]> = MemoizedUtil.list(this._store, PreservationSpaceOrganizationalUnitArchiveAclState);
  queryParametersObs: Observable<QueryParameters> = MemoizedUtil.queryParameters(this._store, PreservationSpaceOrganizationalUnitArchiveAclState);
  private _orgUnitId: string;

  constructor(private readonly _store: Store,
              private readonly _changeDetector: ChangeDetectorRef,
              private readonly _route: ActivatedRoute,
              private readonly _routerExt: RouterExtensionService,
              private readonly _actions$: Actions,
              private readonly _dialog: MatDialog,
              private readonly _storeDialogService: StoreDialogService,
              private readonly _securityService: SecurityService) {
    super();
  }

  ngOnInit(): void {
    super.ngOnInit();
    this._retrieveResIdFromUrl();
    this.detailRouteToInterpolate = `${RoutesEnum.preservationSpaceOrganizationalUnitDetail}/${this._orgUnitId}/${PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl}/${PreservationSpaceOrganizationalUnitRoutesEnum.manageAclDetail}/\{${SOLIDIFY_CONSTANTS.RES_ID}\}`;
    this._store.dispatch(new PreservationSpaceOrganizationalUnitAction.GetById(this._orgUnitId));

    this.extraQueryParameters = {[PARAM_QUERY_ORGUNIT]: this._orgUnitId};
    this.columns = [
      {
        field: "aipId",
        header: LabelTranslateEnum.archive,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        resourceNameSpace: this.sharedArchiveNameSpace,
        resourceState: this.sharedArchiveState as any,
        resourceLabelCallback: (value: Archive) => value.title,
        resourceLabelKey: PARAM_QUERY_SEARCH,
        component: DataTableComponentHelper.get(DataTableComponentEnum.archive),
        extraSearchQueryParam: this.extraQueryParameters,
      },
      {
        field: "user.person.fullName" as any,
        header: LabelTranslateEnum.user,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        resourceNameSpace: this.sharedUserNameSpace,
        resourceState: this.sharedUserState as any,
        resourceLabelKey: "lastName",
        resourceLabelCallback: (value: User) => value.lastName + ", " + value.firstName,
        resourceExtraLabelSecondLineCallback: UserHelper.extraInfoSecondLineLabelCallback,
        searchableSingleSelectSort: this.sharedUserSort,
        filterableField: "user.resId" as any,
        sortableField: "user.resId" as any,
      },
      {
        field: "expiration",
        header: LabelTranslateEnum.expiration,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
    ];
  }

  protected _retrieveResIdFromUrl(): void {
    this._orgUnitId = this._route.snapshot.parent.parent.paramMap.get(AppRoutesEnum.paramIdWithoutPrefixParam);
  }

  conditionDisplayEditButton(model: ArchiveACL | undefined): boolean {
    return this.canCreateEditDelete;
  }

  conditionDisplayDeleteButton(model: ArchiveACL | undefined): boolean {
    return this.canCreateEditDelete;
  }

  goToEdit(model: ArchiveACL): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationSpaceOrganizationalUnitDetail, this._orgUnitId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl, PreservationSpaceOrganizationalUnitRoutesEnum.manageAclDetail, model.resId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAclEdit], {}, {skipLocationChange: true}));
  }

  delete(model: ArchiveACL): void {
    const data = this._storeDialogService.deleteData(StateEnum.preservationSpace_organizationalUnit_archiveAcl);
    data.name = model[this.KEY_PARAM_NAME]?.toString();
    data.resId = model.resId;

    DialogUtil.open(this._dialog, DeleteDialog, data, {
      width: "400px",
    });
    this.subscribe(this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitArchiveAclAction.DeleteSuccess))
      .pipe(
        tap(result => {
          if (result.result.successful) {
            this.getAll();
          }
        }),
      ));
  }

  create(element: ElementRef): void {
    this._store.dispatch(new Navigate([
      AppRoutesEnum.preservationSpace,
      PreservationSpaceRoutesEnum.organizationalUnit,
      PreservationSpaceOrganizationalUnitRoutesEnum.detail,
      this._orgUnitId,
      PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl,
      PreservationSpaceOrganizationalUnitRoutesEnum.manageAclCreate], {} /*{skipLocationChange: true}*/));
  }

  getAll(queryParameters?: QueryParameters): void {
    this._store.dispatch(new PreservationSpaceOrganizationalUnitArchiveAclAction.GetAll(queryParameters, true));
  }

  showDetail(model: ArchiveACL): void {
    this._store.dispatch(new Navigate([RoutesEnum.preservationSpaceOrganizationalUnitDetail, this._orgUnitId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl, PreservationSpaceOrganizationalUnitRoutesEnum.manageAclDetail, model.resId]));
  }

  onQueryParametersEvent(queryParameters: QueryParameters): void {
    if (isNullOrUndefined(queryParameters)) {
      queryParameters = new QueryParameters();
    }
    const searchItem = QueryParametersUtil.getSearchItems(queryParameters);
    MappingObjectUtil.set(searchItem, "organizationalUnit.resId", this._orgUnitId);
    this._store.dispatch(new PreservationSpaceOrganizationalUnitArchiveAclAction.ChangeQueryParameters(queryParameters, true));
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  back(): void {
    this._store.dispatch(this.backNavigate());
  }

  backNavigate(): Navigate {
    return new Navigate([RoutesEnum.preservationSpaceOrganizationalUnitDetail + "/" + this._orgUnitId]);
  }

  navigateNavigate(navigate: Navigate): void {
    this._store.dispatch(navigate);
  }

  showHistory(archiveACL: ArchiveACL): void {
  }
}
