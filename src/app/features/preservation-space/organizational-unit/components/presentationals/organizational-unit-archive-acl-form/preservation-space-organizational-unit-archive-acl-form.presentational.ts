/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-archive-acl-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
} from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  Validators,
} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {Enums} from "@enums";
import {
  Archive,
  ArchiveACL,
  DataFile,
  User,
} from "@models";
import {NotificationHelper} from "@preservation-space/notification/helper/notification.helper";
import {PreservationSpaceOrganizationalUnitArchiveAclAction} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.action";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {DataFileHelper} from "@shared/helpers/data-file.helper";
import {UserHelper} from "@shared/helpers/user.helper";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SecurityService} from "@shared/services/security.service";
import {sharedArchiveActionNameSpace} from "@shared/stores/archive/shared-archive.action";
import {
  PARAM_QUERY_ORGUNIT,
  PARAM_QUERY_SEARCH,
  SharedArchiveState,
} from "@shared/stores/archive/shared-archive.state";
import {sharedUserActionNameSpace} from "@shared/stores/user/shared-user.action";
import {SharedUserState} from "@shared/stores/user/shared-user.state";
import {Observable} from "rxjs";
import {
  filter,
  tap,
} from "rxjs/operators";
import {
  AbstractFormPresentational,
  BreakpointService,
  DateUtil,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTruthyObject,
  MappingObject,
  MappingObjectUtil,
  OrderEnum,
  PropertyName,
  ResourceFileNameSpace,
  ResourceNameSpace,
  SolidifyDataFileModel,
  SolidifyFileUploadInputRequiredValidator,
  SolidifyFileUploadStatus,
  SolidifyValidator,
  Sort,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-space-organizational-unit-archive-acl-form",
  templateUrl: "./preservation-space-organizational-unit-archive-acl-form.presentational.html",
  styleUrls: ["./preservation-space-organizational-unit-archive-acl-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceOrganizationalUnitArchiveAclFormPresentational extends AbstractFormPresentational<ArchiveACL> implements OnInit {
  readonly DEFAULT_DURATION_IN_MONTH: number = 3;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  sharedArchiveNameSpace: ResourceNameSpace = sharedArchiveActionNameSpace;
  sharedArchiveState: typeof SharedArchiveState = SharedArchiveState;

  @Input()
  orgUnitId: string;

  @Input()
  urlQueryParameters: MappingObject<string, string | undefined>;

  extraQueryParameters: MappingObject<string, string>;
  searchKey: string = PARAM_QUERY_SEARCH;

  sharedUserSort: Sort<User> = {
    field: "lastName",
    order: OrderEnum.ascending,
  };
  sharedUserNameSpace: ResourceNameSpace = sharedUserActionNameSpace;
  sharedUserState: typeof SharedUserState = SharedUserState;
  labelUserCallback: (user: User) => string = user => user.lastName + ", " + user.firstName;
  extraInfoSecondLineLabelCallback: (user: User) => string = UserHelper.extraInfoSecondLineLabelCallback;

  aipFormControl: AbstractControl;
  userFormControl: AbstractControl;

  preservationSpaceOrganizationalUnitArchiveAclActionNameSpace: ResourceFileNameSpace = PreservationSpaceOrganizationalUnitArchiveAclAction;

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get isInCreationMode(): boolean {
    return isNullOrUndefined(this.model);
  }

  dataFileAdapter: (dataFile: DataFile) => SolidifyDataFileModel = DataFileHelper.dataFileAdapter;

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              protected readonly _route: ActivatedRoute,
              public readonly breakpointService: BreakpointService,
              public readonly securityService: SecurityService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.extraQueryParameters = {[PARAM_QUERY_ORGUNIT]: this.orgUnitId};
    this.aipFormControl = this.form.get(this.formDefinition.aipId);
    this.userFormControl = this.form.get(this.formDefinition.user);

    this.subscribe(this._observableRemoveErrorField(this.aipFormControl));
    this.subscribe(this._observableRemoveErrorField(this.userFormControl));
  }

  private _observableRemoveErrorField(formControl: AbstractControl): Observable<string> {
    return formControl.valueChanges.pipe(
      filter(() => isTruthyObject(formControl.errors)),
      tap(() => {
        this._removeErrorOnAipAndUserField();
      }),
    );
  }

  override _disableSpecificField(): void {
    if (!this.isInCreationMode) {
      this.form.get(this.formDefinition.fileChange).disable();
      this.form.get(this.formDefinition.aipId).disable();
      this.form.get(this.formDefinition.user).disable();
      if (!this.securityService.isRootOrAdmin()) {
        this.form.get(this.formDefinition.expiration).disable();
      }
    }
  }

  private _removeErrorOnAipAndUserField(): void {
    this._removeErrorOnFormControl(this.aipFormControl);
    this._removeErrorOnFormControl(this.userFormControl);
    this._changeDetectorRef.detectChanges();
  }

  private _removeErrorOnFormControl(formControl: AbstractControl): void {
    formControl.setErrors(null);
    formControl.markAsTouched();
    formControl.updateValueAndValidity();
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.aipId]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.user]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.expiration]: ["", [SolidifyValidator]],
      [this.formDefinition.fileChange]: [undefined, [SolidifyValidator]],
    });
    this._addUserAndArchiveForNotificationRequest();
  }

  protected _bindFormTo(archiveAcl: ArchiveACL): void {
    this.form = this._fb.group({
      [this.formDefinition.aipId]: [archiveAcl.aipId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.user]: [archiveAcl.user.resId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.expiration]: [DateUtil.convertOffsetDateTimeIso8601ToDate(archiveAcl.expiration), [SolidifyValidator]],
      [this.formDefinition.fileChange]: [isNotNullNorUndefined(archiveAcl.signedDuaFile) ? SolidifyFileUploadStatus.UNCHANGED : undefined, [SolidifyValidator]],
    });
  }

  protected _treatmentBeforeSubmit(archiveAcl: ArchiveACL): ArchiveACL {
    const result = {
      resId: this.model?.resId,
      aipId: this.form.get(this.formDefinition.aipId).value,
      organizationalUnit: {
        resId: this.orgUnitId,
      },
      user: {
        resId: this.form.get(this.formDefinition.user).value,
      },
      expiration: DateUtil.convertToOffsetDateTimeIso8601(this.form.get(this.formDefinition.expiration).value),
    } as ArchiveACL;
    if (this.isInCreationMode && this.isDuaRequired) {
      result.duaFileChange = this.form.get(this.formDefinition.fileChange).value;
    }
    return result;
  }

  private _addUserAndArchiveForNotificationRequest(): void {
    if (isNullOrUndefined(this.urlQueryParameters) || MappingObjectUtil.size(this.urlQueryParameters) === 0) {
      return;
    }

    const userId = MappingObjectUtil.get(this.urlQueryParameters, NotificationHelper.KEY_USER_ID);
    const archiveId = MappingObjectUtil.get(this.urlQueryParameters, NotificationHelper.KEY_ARCHIVE_ID);

    if (isNullOrUndefined(userId) || isNullOrUndefined(archiveId)) {
      return;
    }
    this.form = this._fb.group({
      [this.formDefinition.aipId]: [archiveId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.user]: [userId, [Validators.required, SolidifyValidator]],
      [this.formDefinition.expiration]: ["", [SolidifyValidator]],
    });

    this.form.markAsDirty();
    this.form.markAsUntouched();
    this.form.updateValueAndValidity();
  }

  archiveSelected(archive: Archive): void {
    this._computeIsDuaRequired(archive);
  }

  private _computeIsDuaRequired(archive: Archive | undefined): void {
    const isDuaRequired = archive?.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.SIGNED_DUA;
    const formControlDuaFile = this.form.get(this.formDefinition.fileChange);
    if (isNullOrUndefined(formControlDuaFile)) {
      return;
    }
    if (isDuaRequired) {
      formControlDuaFile.setValidators([SolidifyFileUploadInputRequiredValidator, SolidifyValidator]);
    } else {
      formControlDuaFile.setValidators([SolidifyValidator]);
    }
    formControlDuaFile.updateValueAndValidity();
  }

  get isDuaRequired(): boolean {
    return this.form.get(this.formDefinition.fileChange)?.hasValidator(SolidifyFileUploadInputRequiredValidator);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() aipId: string;
  @PropertyName() organizationalUnitId: string;
  @PropertyName() user: string;
  @PropertyName() expiration: string;
  @PropertyName() fileChange: string;
}
