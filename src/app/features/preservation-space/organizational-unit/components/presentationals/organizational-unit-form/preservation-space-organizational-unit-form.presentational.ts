/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
  OnInit,
  Output,
  ViewChild,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {NotificationHelper} from "@app/features/preservation-space/notification/helper/notification.helper";
import {environment} from "@environments/environment";
import {
  DisseminationPolicy,
  Institution,
  License,
  OrganizationalUnit,
  OrganizationalUnitDisseminationPolicyContainer,
  PreservationPolicy,
  Role,
  SubjectArea,
  SubmissionPolicy,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {PreservationSpaceOrganizationalUnitAction} from "@preservation-space/organizational-unit/stores/preservation-space-organizational-unit.action";
import {PreservationSpaceOrganizationalUnitState} from "@preservation-space/organizational-unit/stores/preservation-space-organizational-unit.state";
import {SharedInstitutionOverlayPresentational} from "@shared/components/presentationals/shared-institution-overlay/shared-institution-overlay.presentational";
import {SharedLicenseOverlayPresentational} from "@shared/components/presentationals/shared-license-overlay/shared-license-overlay.presentational";
import {SharedOrganizationalUnitDisseminationPolicyParametersPresentational} from "@shared/components/presentationals/shared-organizational-unit-dissemination-policy-parameters/shared-organizational-unit-dissemination-policy-parameters.presentational";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  PreservationSpaceOrganizationalUnitRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SecurityService} from "@shared/services/security.service";
import {sharedInstitutionActionNameSpace} from "@shared/stores/institution/shared-institution.action";
import {SharedInstitutionState} from "@shared/stores/institution/shared-institution.state";
import {sharedLicenseActionNameSpace} from "@shared/stores/license/shared-license.action";
import {
  SharedLicenseState,
  SharedLicenseStateModel,
} from "@shared/stores/license/shared-license.state";
import {sharedSubjectAreaActionNameSpace} from "@shared/stores/subject-area/shared-subject-area.action";
import {SharedSubjectAreaState} from "@shared/stores/subject-area/shared-subject-area.state";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  AbstractApplicationFormPresentational,
  BreakpointService,
  DateUtil,
  isEmptyString,
  isNotNullNorUndefined,
  isNotNullNorUndefinedNorWhiteString,
  isNullOrUndefined,
  isNullOrUndefinedOrWhiteString,
  MappingObject,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  ObservableUtil,
  OrderEnum,
  PropertyName,
  RegexUtil,
  ResourceFileNameSpace,
  ResourceNameSpace,
  SearchableSingleSelectPresentational,
  SolidifyValidator,
  Sort,
  Type,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-space-organizational-unit-form",
  templateUrl: "./preservation-space-organizational-unit-form.presentational.html",
  styleUrls: ["./preservation-space-organizational-unit-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceOrganizationalUnitFormPresentational extends AbstractApplicationFormPresentational<OrganizationalUnit> implements OnInit {
  private readonly _KEY_SOURCE: keyof SubjectArea = "source";
  private readonly _KEY_DEFAULT: string = "default";
  readonly TIME_BEFORE_DISPLAY_TOOLTIP: number = environment.timeBeforeDisplayTooltip;
  readonly classBypassEdit: string = environment.classBypassEdit;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  organizationalUnitActionNameSpace: ResourceFileNameSpace = PreservationSpaceOrganizationalUnitAction;
  organizationalUnitState: typeof PreservationSpaceOrganizationalUnitState = PreservationSpaceOrganizationalUnitState;

  @Input()
  canLeaveOrganizationalUnit: boolean;

  @Input()
  subjectDomainSources: string[];

  @Input()
  listSubmissionPolicies: SubmissionPolicy[];

  @Input()
  listPreservationPolicies: PreservationPolicy[];

  @Input()
  listDisseminationPolicies: DisseminationPolicy[];

  @Input()
  selectedInstitutions: Institution[];

  @Input()
  selectedSubjectAreas: SubjectArea[];

  private _selectedSubmissionPolicies: SubmissionPolicy[];

  @Input()
  set selectedSubmissionPolicies(value: SubmissionPolicy[]) {
    this._selectedSubmissionPolicies = this._setDefaultPolicyInfo(value, this.model?.defaultSubmissionPolicy?.resId);
  }

  get selectedSubmissionPolicies(): SubmissionPolicy[] {
    return this._selectedSubmissionPolicies;
  }

  private _selectedPreservationPolicies: PreservationPolicy[];

  @Input()
  set selectedPreservationPolicies(value: PreservationPolicy[]) {
    this._selectedPreservationPolicies = this._setDefaultPolicyInfo(value, this.model?.defaultPreservationPolicy?.resId);
  }

  get selectedPreservationPolicies(): PreservationPolicy[] {
    return this._selectedPreservationPolicies;
  }

  @Input()
  selectedDisseminationPolicies: OrganizationalUnitDisseminationPolicyContainer[];

  @Input()
  selectedPersonRole: PersonRole[];

  @Input()
  inheritedSelectedPersonRole: PersonRole[];

  @Input()
  listRole: Role[];

  @Input()
  isManager: boolean;

  @Input()
  isMemberOrgUnit: boolean;

  @Input()
  urlQueryParameters: MappingObject<string, string | undefined>;

  @Input()
  isTour: boolean;

  @Input()
  defaultPlatformLicenseId: string;

  subjectAreaLabelCallback: (value: SubjectArea) => string = (value: SubjectArea) => `[${value.source}] ${value.name}`;

  institutionLabelCallback: (value: Institution) => string = (value: Institution) => `${value.name}`;

  licenseCallback: (value: License) => string = (value: License) => value.openLicenseId + " (" + value.title + ")";

  protected readonly _requestToBeMemberBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("requestToBeMemberChange")
  readonly requestToBeMemberObs: Observable<void> = ObservableUtil.asObservable(this._requestToBeMemberBS);

  protected readonly _leaveOrganizationalUnitBS: BehaviorSubject<void> = new BehaviorSubject<void>(undefined);
  @Output("leaveOrganizationalUnitChange")
  readonly leaveOrganizationalUnitObs: Observable<void> = ObservableUtil.asObservable(this._leaveOrganizationalUnitBS);

  protected readonly _seeArchiveBS: BehaviorSubject<OrganizationalUnit> = new BehaviorSubject<OrganizationalUnit>(undefined);
  @Output("seeArchiveChange")
  readonly seeArchiveObs: Observable<OrganizationalUnit> = ObservableUtil.asObservable(this._seeArchiveBS);

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get dataTestEnum(): typeof DataTestEnum {
    return DataTestEnum;
  }

  @ViewChild("organizationalUnitDisseminationPolicyParametersPresentational")
  organizationalUnitDisseminationPolicyParametersPresentational: SharedOrganizationalUnitDisseminationPolicyParametersPresentational;

  @ViewChild("licenseSingleSearchableSelect")
  licenseSingleSearchableSelect: SearchableSingleSelectPresentational<SharedLicenseStateModel, License>;

  sharedInstitutionSort: Sort<Institution> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedInstitutionActionNameSpace: ResourceNameSpace = sharedInstitutionActionNameSpace;
  sharedInstitutionState: typeof SharedInstitutionState = SharedInstitutionState;

  sharedSubjectAreaSort: Sort<SubjectArea> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedSubjectAreaActionNameSpace: ResourceNameSpace = sharedSubjectAreaActionNameSpace;
  sharedSubjectAreaState: typeof SharedSubjectAreaState = SharedSubjectAreaState;

  sharedLicenseSort: Sort<License> = {
    field: "title",
    order: OrderEnum.ascending,
  };
  sharedLicenseActionNameSpace: ResourceNameSpace = sharedLicenseActionNameSpace;
  sharedLicenseState: typeof SharedLicenseState = SharedLicenseState;

  institutionOverlayComponent: Type<SharedInstitutionOverlayPresentational> = SharedInstitutionOverlayPresentational;
  licenseOverlayComponent: Type<SharedLicenseOverlayPresentational> = SharedLicenseOverlayPresentational;

  extraSearchParameterSource: MappingObject<string, string> = {};
  personIdAutomaticallyAdded: string | undefined = undefined;

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  get canAccessToAdmin(): boolean {
    return this.securityService.isRootOrAdmin() || this.securityService.isManagerOfAnyInstitution();
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly breakpointService: BreakpointService,
              private readonly _notificationService: NotificationService,
              public readonly securityService: SecurityService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  ngOnInit(): void {
    if (this.isTour) {
      this._bindFormTo({
        name: "Tour organizational unit",
      });
      return;
    }
    super.ngOnInit();
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: ["", [SolidifyValidator]],
      [this.formDefinition.url]: ["", [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.openingDate]: [""],
      [this.formDefinition.closingDate]: [""],
      [this.formDefinition.personRole]: ["", [SolidifyValidator]],
      [this.formDefinition.inheritedPersonRole]: ["", [SolidifyValidator]],
      [this.formDefinition.filterSubjectAreas]: ["", [SolidifyValidator]],
      [this.formDefinition.subjectAreas]: ["", [SolidifyValidator]],
      [this.formDefinition.keywords]: [[], [SolidifyValidator]],
      [this.formDefinition.institutions]: ["", [SolidifyValidator]],
      [this.formDefinition.submissionPolicies]: [[], [Validators.required, SolidifyValidator]],
      [this.formDefinition.preservationPolicies]: [[], [Validators.required, SolidifyValidator]],
      [this.formDefinition.disseminationPolicies]: [[], [SolidifyValidator]],
      [this.formDefinition.defaultSubmissionPolicy]: [undefined, [Validators.required, SolidifyValidator]],
      [this.formDefinition.defaultPreservationPolicy]: [undefined, [Validators.required, SolidifyValidator]],
      [this.formDefinition.defaultLicense]: ["", [SolidifyValidator]],
    });
  }

  protected _bindFormTo(organizationalUnit: OrganizationalUnit): void {
    this.form = this._fb.group({
      [this.formDefinition.name]: [organizationalUnit.name, [Validators.required, SolidifyValidator]],
      [this.formDefinition.description]: [organizationalUnit.description, [SolidifyValidator]],
      [this.formDefinition.url]: [organizationalUnit.url, [SolidifyValidator, Validators.pattern(RegexUtil.url)]],
      [this.formDefinition.openingDate]: [organizationalUnit.openingDate],
      [this.formDefinition.closingDate]: [organizationalUnit.closingDate],
      [this.formDefinition.personRole]: ["", [SolidifyValidator]],
      [this.formDefinition.inheritedPersonRole]: ["", [SolidifyValidator]],
      [this.formDefinition.filterSubjectAreas]: ["", [SolidifyValidator]],
      [this.formDefinition.subjectAreas]: [organizationalUnit.subjectAreas?.map(r => r.resId), [SolidifyValidator]],
      [this.formDefinition.keywords]: [isNullOrUndefined(organizationalUnit.keywords) ? [] : [...organizationalUnit.keywords], [SolidifyValidator]],
      [this.formDefinition.institutions]: [this.selectedInstitutions?.map(r => r.resId), [SolidifyValidator]],
      [this.formDefinition.subjectAreas]: [this.selectedSubjectAreas?.map(r => r.resId), [SolidifyValidator]],
      [this.formDefinition.submissionPolicies]: [this.selectedSubmissionPolicies.map(s => s.resId), [Validators.required, SolidifyValidator]],
      [this.formDefinition.preservationPolicies]: [this.selectedPreservationPolicies.map(p => p.resId), [Validators.required, SolidifyValidator]],
      [this.formDefinition.disseminationPolicies]: [this.selectedDisseminationPolicies?.map(d => d.joinResource.compositeKey) ?? [], [SolidifyValidator]],
      [this.formDefinition.defaultSubmissionPolicy]: [(organizationalUnit.defaultSubmissionPolicy ? organizationalUnit.defaultSubmissionPolicy.resId : undefined), [Validators.required, SolidifyValidator]],
      [this.formDefinition.defaultPreservationPolicy]: [(organizationalUnit.defaultPreservationPolicy ? organizationalUnit.defaultPreservationPolicy.resId : undefined), [Validators.required, SolidifyValidator]],
      [this.formDefinition.defaultLicense]: [organizationalUnit?.defaultLicense?.resId, [SolidifyValidator]],
    });

    this._addRoleAskingInNotificationRequest();
  }

  private _addRoleAskingInNotificationRequest(): void {
    if (isNullOrUndefined(this.urlQueryParameters) || MappingObjectUtil.size(this.urlQueryParameters) === 0) {
      return;
    }
    const roleId = MappingObjectUtil.get(this.urlQueryParameters, NotificationHelper.KEY_ROLE_ID);
    const personId = MappingObjectUtil.get(this.urlQueryParameters, NotificationHelper.KEY_PERSON_ID);
    if (isNullOrUndefined(roleId) || isNullOrUndefined(personId)) {
      return;
    }
    this.personIdAutomaticallyAdded = personId;
    const existingPerson = this.selectedPersonRole.find(p => p.resId === personId);
    if (isNullOrUndefined(existingPerson)) {
      this.selectedPersonRole = [...this.selectedPersonRole, {
        resId: personId,
        roles: [{
          resId: roleId,
        } as any],
      }];
      this.form.markAsDirty();
      this._notificationService.showInformation(MARK_AS_TRANSLATABLE("organizationalUnit.notification.personAddedToOrgUnit"));
    } else {
      this._notificationService.showInformation(MARK_AS_TRANSLATABLE("organizationalUnit.notification.personAlreadyInOrgUnit"));
    }
  }

  protected _treatmentBeforeSubmit(organizationalUnit: OrganizationalUnit): OrganizationalUnit {
    delete organizationalUnit[this.formDefinition.filterSubjectAreas];
    organizationalUnit.openingDate = DateUtil.convertToLocalDateDateSimple(organizationalUnit.openingDate);
    organizationalUnit.closingDate = DateUtil.convertToLocalDateDateSimple(organizationalUnit.closingDate);
    organizationalUnit.subjectAreas = [];
    if (this.form.get(this.formDefinition.subjectAreas).value !== "" && isNotNullNorUndefined(this.form.get(this.formDefinition.subjectAreas).value)) {
      this.form.get(this.formDefinition.subjectAreas).value.forEach(resId => {
        organizationalUnit.subjectAreas.push({resId: resId});
      });
    }
    organizationalUnit.institutions = [];
    if (this.form.get(this.formDefinition.institutions).value !== "" && isNotNullNorUndefined(this.form.get(this.formDefinition.institutions).value)) {
      this.form.get(this.formDefinition.institutions).value.forEach(resId => {
        organizationalUnit.institutions.push({resId: resId});
      });
    }
    const defaultSubmissionPolicyId = this.form.get(this.formDefinition.defaultSubmissionPolicy).value;
    if (isNotNullNorUndefinedNorWhiteString(defaultSubmissionPolicyId)) {
      organizationalUnit.defaultSubmissionPolicy = {resId: defaultSubmissionPolicyId};
    }
    const defaultPreservationPolicyId = this.form.get(this.formDefinition.defaultPreservationPolicy).value;
    if (isNotNullNorUndefinedNorWhiteString(defaultPreservationPolicyId)) {
      organizationalUnit.defaultPreservationPolicy = {resId: defaultPreservationPolicyId};
    }
    organizationalUnit.preservationPolicies = [];
    this.form.get(this.formDefinition.preservationPolicies).value.forEach(resId => {
      organizationalUnit.preservationPolicies.push({resId: resId});
    });
    organizationalUnit.submissionPolicies = [];
    this.form.get(this.formDefinition.submissionPolicies).value.forEach(resId => {
      organizationalUnit.submissionPolicies.push({resId: resId});
    });
    organizationalUnit.disseminationPolicies = this.organizationalUnitDisseminationPolicyParametersPresentational.listOrganizationalUnitDisseminationPolicyContainer;

    const defaultLicenseResId = this.form.get(this.formDefinition.defaultLicense).value;
    if (this.licenseSingleSearchableSelect.isDefaultValueSelected || isNullOrUndefinedOrWhiteString(defaultLicenseResId)) {
      organizationalUnit.defaultLicense = null;
    } else {
      organizationalUnit.defaultLicense = {resId: defaultLicenseResId};
    }
    return organizationalUnit;
  }

  private _setDefaultPolicyInfo(list: any[], defaultPolicyResId: string | undefined): any[] {
    if (isNullOrUndefined(list)) {
      return [];
    }
    list = [...list];
    const defaultValue = defaultPolicyResId;
    if (isNullOrUndefined(defaultValue)) {
      return list;
    }
    const defaultItemIndex = list.findIndex(i => i.resId === defaultValue);
    if (defaultItemIndex === -1) {
      return list;
    }
    const defaultItem = {...list[defaultItemIndex]};
    defaultItem[this._KEY_DEFAULT] = true;
    list[defaultItemIndex] = defaultItem;
    return list;
  }

  navigateToSubmissionPolicy(submissionPolicy: SubmissionPolicy): void {
    this.navigate([RoutesEnum.adminSubmissionPolicyDetail, submissionPolicy.resId]);
  }

  navigateToPreservationPolicy(preservationPolicy: PreservationPolicy): void {
    this.navigate([RoutesEnum.adminPreservationPolicyDetail, preservationPolicy.resId]);
  }

  navigateToLicense(licenseId: string): void {
    this.navigate([RoutesEnum.adminLicenseDetail, licenseId]);
  }

  requestToBeMember(): void {
    this._requestToBeMemberBS.next();
  }

  leaveOrganizationalUnit(): void {
    this._leaveOrganizationalUnitBS.next();
  }

  goToReceivedRequestNotification(): void {
    this._navigateWithQueryParamBS.next(new Navigate([RoutesEnum.preservationSpaceNotificationInbox], NotificationHelper.getUrlQueryParam(this.model.resId)));
  }

  goToSentRequestNotification(): void {
    this._navigateWithQueryParamBS.next(new Navigate([RoutesEnum.preservationSpaceNotificationSent], NotificationHelper.getUrlQueryParam(this.model.resId)));
  }

  goToDeposit(): void {
    this._navigateBS.next([RoutesEnum.deposit, this.model.resId]);
  }

  goToArchive(): void {
    this._seeArchiveBS.next(this.model);
  }

  goToManageArchiveAcl(): void {
    this._navigateBS.next([RoutesEnum.preservationSpaceOrganizationalUnitDetail, this.model.resId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl]);
  }

  filterSubjectArea(source: string): void {
    this._computeExtraSearchParameterCategory(source);
  }

  private _computeExtraSearchParameterCategory(value: string): void {
    if (isNullOrUndefined(value) || isEmptyString(value)) {
      MappingObjectUtil.delete(this.extraSearchParameterSource, this._KEY_SOURCE as string);
      return;
    }
    MappingObjectUtil.set(this.extraSearchParameterSource, this._KEY_SOURCE as string, value);
  }

  navigateToInstitution(institution: Institution): void {
    this._navigateBS.next([RoutesEnum.preservationSpaceInstitutionDetail, institution.resId]);
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() name: string;
  @PropertyName() description: string;
  @PropertyName() url: string;
  @PropertyName() closingDate: string;
  @PropertyName() openingDate: string;
  @PropertyName() personRole: string;
  @PropertyName() inheritedPersonRole: string;
  @PropertyName() filterSubjectAreas: string;
  @PropertyName() subjectAreas: string;
  @PropertyName() keywords: string;
  @PropertyName() preservationPolicy: string;
  @PropertyName() submissionPolicies: string;
  @PropertyName() preservationPolicies: string;
  @PropertyName() disseminationPolicies: string;
  @PropertyName() defaultSubmissionPolicy: string;
  @PropertyName() defaultPreservationPolicy: string;
  @PropertyName() institutions: string;
  @PropertyName() defaultLicense: string;
}
