/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-request-creation.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SharedNotificationState} from "@shared/stores/notification/shared-notification.state";
import {Observable} from "rxjs";
import {
  FormValidationHelper,
  MemoizedUtil,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-space-organizational-unit-request-creation-dialog",
  templateUrl: "./preservation-space-organizational-unit-request-creation.dialog.html",
  styleUrls: ["./preservation-space-organizational-unit-request-creation.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceOrganizationalUnitRequestCreationDialog extends SharedAbstractDialog<PreservationSpaceOrganizationalUnitRequestCreationDialogData, OrgunitRequestCreationDialogResult> implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedNotificationState);

  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<PreservationSpaceOrganizationalUnitRequestCreationDialog>,
              private readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: PreservationSpaceOrganizationalUnitRequestCreationDialogData) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.orgUnitName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.message]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  onSubmit(): void {
    this.submit(this.form.value as OrgunitRequestCreationDialogResult);
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() orgUnitName: string;
  @PropertyName() message: string;
}

export interface PreservationSpaceOrganizationalUnitRequestCreationDialogData {
}

export interface OrgunitRequestCreationDialogResult {
  orgUnitName: string;
  message: string;
}
