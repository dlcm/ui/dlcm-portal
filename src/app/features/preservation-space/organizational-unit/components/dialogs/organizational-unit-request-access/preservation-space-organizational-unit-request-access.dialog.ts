/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-request-access.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {AppUserState} from "@app/stores/user/app-user.state";
import {Enums} from "@enums";
import {Role} from "@models";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {SecurityService} from "@shared/services/security.service";
import {SharedNotificationAction} from "@shared/stores/notification/shared-notification.action";
import {SharedNotificationState} from "@shared/stores/notification/shared-notification.state";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {Observable} from "rxjs";
import {
  FormValidationHelper,
  MemoizedUtil,
  PropertyName,
  SolidifyValidator,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-space-organizational-unit-request-access-dialog",
  templateUrl: "./preservation-space-organizational-unit-request-access.dialog.html",
  styleUrls: ["./preservation-space-organizational-unit-request-access.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceOrganizationalUnitRequestAccessDialog extends SharedAbstractDialog<PreservationSpaceOrganizationalUnitRequestAccessDialogData, OrgunitRequestAccessDialogResult> implements OnInit {
  isLoadingObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, SharedNotificationState);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);

  form: FormGroup;
  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  constructor(private readonly _store: Store,
              private readonly _actions$: Actions,
              protected readonly _dialogRef: MatDialogRef<PreservationSpaceOrganizationalUnitRequestAccessDialog>,
              private readonly _fb: FormBuilder,
              @Inject(MAT_DIALOG_DATA) public readonly data: PreservationSpaceOrganizationalUnitRequestAccessDialogData,
              private readonly _changeDetector: ChangeDetectorRef,
              readonly securityService: SecurityService,
  ) {
    super(_dialogRef, data);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.form = this._fb.group({
      [this.formDefinition.roleId]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.message]: ["", [Validators.required, SolidifyValidator]],
    });
  }

  validate(): void {
    const formValue = this.form.value as OrgunitRequestAccessDialogResult;

    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(
      this._store,
      this._actions$,
      new SharedNotificationAction.Create({
        model: {
          emitter: MemoizedUtil.currentSnapshot(this._store, AppUserState),
          notifiedOrgUnit: {
            resId: this.data.orgUnitId,
          },
          objectId: formValue.roleId,
          message: formValue.message,
          notificationType: {
            resId: Enums.Notification.TypeEnum.JOIN_ORGUNIT_REQUEST,
          },
        },
        formControl: this.form,
        changeDetectorRef: this._changeDetector,
      }),
      SharedNotificationAction.CreateSuccess,
      result => {
        this._store.dispatch(new SharedNotificationAction.UpdatePendingNotification(this.data.orgUnitId, result.model));
        this.submit(formValue);
      }));
  }

  getFormControl(key: string): FormControl {
    return FormValidationHelper.getFormControl(this.form, key);
  }

  get formValidationHelper(): typeof FormValidationHelper {
    return FormValidationHelper;
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() roleId: string;
  @PropertyName() message: string;
}

export interface PreservationSpaceOrganizationalUnitRequestAccessDialogData {
  orgUnitId: string;
}

export interface OrgunitRequestAccessDialogResult {
  roleId: string;
  message: string;
}
