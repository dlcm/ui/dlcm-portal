/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-dissemination-policy.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  DisseminationPolicy,
  OrganizationalUnitDisseminationPolicy,
  OrganizationalUnitDisseminationPolicyContainer,
} from "@models";
import {StateEnum} from "@shared/enums/state.enum";
import {
  MultiRelation2TiersAction,
  MultiRelation2TiersNameSpace,
  TypeDefaultAction,
} from "solidify-frontend";

const state = StateEnum.preservationSpace_organizationalUnit_disseminationPolicy;

export namespace PreservationSpaceOrganizationalUnitDisseminationPolicyAction {

  @TypeDefaultAction(state)
  export class GetAll extends MultiRelation2TiersAction.GetAll {
  }

  @TypeDefaultAction(state)
  export class GetAllSuccess extends MultiRelation2TiersAction.GetAllSuccess<DisseminationPolicy> {
  }

  @TypeDefaultAction(state)
  export class GetAllFail extends MultiRelation2TiersAction.GetAllFail {
  }

  @TypeDefaultAction(state)
  export class GetById extends MultiRelation2TiersAction.GetById {
  }

  @TypeDefaultAction(state)
  export class GetByIdSuccess extends MultiRelation2TiersAction.GetByIdSuccess<DisseminationPolicy> {
  }

  @TypeDefaultAction(state)
  export class GetByIdFail extends MultiRelation2TiersAction.GetByIdFail {
  }

  @TypeDefaultAction(state)
  export class Update extends MultiRelation2TiersAction.Update<OrganizationalUnitDisseminationPolicyContainer, OrganizationalUnitDisseminationPolicy> {
  }

  @TypeDefaultAction(state)
  export class UpdateSuccess extends MultiRelation2TiersAction.UpdateSuccess<OrganizationalUnitDisseminationPolicyContainer, OrganizationalUnitDisseminationPolicy> {
  }

  @TypeDefaultAction(state)
  export class UpdateFail extends MultiRelation2TiersAction.UpdateFail<OrganizationalUnitDisseminationPolicyContainer, OrganizationalUnitDisseminationPolicy> {
  }

  @TypeDefaultAction(state)
  export class UpdateRelation extends MultiRelation2TiersAction.UpdateRelation<OrganizationalUnitDisseminationPolicy> {
  }

  @TypeDefaultAction(state)
  export class UpdateRelationSuccess extends MultiRelation2TiersAction.UpdateRelationSuccess<OrganizationalUnitDisseminationPolicy> {
  }

  @TypeDefaultAction(state)
  export class UpdateRelationFail extends MultiRelation2TiersAction.UpdateRelationFail<OrganizationalUnitDisseminationPolicy> {
  }

  @TypeDefaultAction(state)
  export class Create extends MultiRelation2TiersAction.Create<OrganizationalUnitDisseminationPolicy> {
  }

  @TypeDefaultAction(state)
  export class CreateSuccess extends MultiRelation2TiersAction.CreateSuccess<OrganizationalUnitDisseminationPolicyContainer, OrganizationalUnitDisseminationPolicy> {
  }

  @TypeDefaultAction(state)
  export class CreateFail extends MultiRelation2TiersAction.CreateFail<OrganizationalUnitDisseminationPolicy> {
  }

  @TypeDefaultAction(state)
  export class DeleteList extends MultiRelation2TiersAction.DeleteList {
  }

  @TypeDefaultAction(state)
  export class DeleteListSuccess extends MultiRelation2TiersAction.DeleteListSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteListFail extends MultiRelation2TiersAction.DeleteListFail {
  }

  @TypeDefaultAction(state)
  export class Delete extends MultiRelation2TiersAction.Delete {
  }

  @TypeDefaultAction(state)
  export class DeleteSuccess extends MultiRelation2TiersAction.DeleteSuccess {
  }

  @TypeDefaultAction(state)
  export class DeleteFail extends MultiRelation2TiersAction.DeleteFail {
  }
}

export const preservationSpaceOrganizationalUnitDisseminationPolicyActionNameSpace: MultiRelation2TiersNameSpace = PreservationSpaceOrganizationalUnitDisseminationPolicyAction;
