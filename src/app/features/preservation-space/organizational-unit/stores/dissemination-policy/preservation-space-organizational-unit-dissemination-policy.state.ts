/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-dissemination-policy.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {
  OrganizationalUnitDisseminationPolicy,
  OrganizationalUnitDisseminationPolicyContainer,
} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {preservationSpaceOrganizationalUnitDisseminationPolicyActionNameSpace} from "@preservation-space/organizational-unit/stores/dissemination-policy/preservation-space-organizational-unit-dissemination-policy.action";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  defaultMultiRelation2TiersStateInitValue,
  MultiRelation2TiersState,
  MultiRelation2TiersStateModel,
  NotificationService,
} from "solidify-frontend";

export interface PreservationSpaceOrganizationalUnitDisseminationPolicyStateModel extends MultiRelation2TiersStateModel<OrganizationalUnitDisseminationPolicyContainer> {
}

@Injectable()
@State<PreservationSpaceOrganizationalUnitDisseminationPolicyStateModel>({
  name: StateEnum.preservationSpace_organizationalUnit_disseminationPolicy,
  defaults: {
    ...defaultMultiRelation2TiersStateInitValue(),
  },
})
export class PreservationSpaceOrganizationalUnitDisseminationPolicyState extends MultiRelation2TiersState<PreservationSpaceOrganizationalUnitDisseminationPolicyStateModel, OrganizationalUnitDisseminationPolicyContainer, OrganizationalUnitDisseminationPolicy> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationSpaceOrganizationalUnitDisseminationPolicyActionNameSpace,
      resourceName: ApiResourceNameEnum.DISSEMINATION_POLICY,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOrganizationalUnits;
  }
}
