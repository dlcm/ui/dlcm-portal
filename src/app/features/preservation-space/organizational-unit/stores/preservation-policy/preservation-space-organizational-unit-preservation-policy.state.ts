/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-preservation-policy.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {OrganizationalUnitPreservationPolicy} from "@admin/models/organizational-unit-preservation-policy.model";
import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {PreservationPolicy} from "@models";
import {
  Action,
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {
  PreservationSpaceOrganizationalUnitPreservationPolicyAction,
  preservationSpaceOrganizationalUnitPreservationPolicyActionNameSpace,
} from "@preservation-space/organizational-unit/stores/preservation-policy/preservation-space-organizational-unit-preservation-policy.action";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  Observable,
  of,
} from "rxjs";
import {
  map,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  defaultRelation2TiersStateInitValue,
  isNotNullNorUndefined,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  Relation2TiersState,
  Relation2TiersStateModel,
  SolidifyStateContext,
  StoreUtil,
} from "solidify-frontend";

export interface PreservationSpaceOrganizationalUnitPreservationPolicyStateModel extends Relation2TiersStateModel<PreservationPolicy> {
}

@Injectable()
@State<PreservationSpaceOrganizationalUnitPreservationPolicyStateModel>({
  name: StateEnum.preservationSpace_organizationalUnit_preservationPolicy,
  defaults: {
    ...defaultRelation2TiersStateInitValue(),
  },
})
export class PreservationSpaceOrganizationalUnitPreservationPolicyState extends Relation2TiersState<PreservationSpaceOrganizationalUnitPreservationPolicyStateModel, PreservationPolicy, OrganizationalUnitPreservationPolicy> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationSpaceOrganizationalUnitPreservationPolicyActionNameSpace,
      resourceName: ApiResourceNameEnum.PRES_POLICY,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminOrganizationalUnits;
  }

  @OverrideDefaultAction()
  @Action(PreservationSpaceOrganizationalUnitPreservationPolicyAction.Update)
  update(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitPreservationPolicyStateModel>, action: PreservationSpaceOrganizationalUnitPreservationPolicyAction.Update): Observable<boolean> {
    return super._internalUpdate(ctx, action)
      .pipe(
        tap((isSuccess: boolean) => {
          if (!isSuccess) {
            ctx.dispatch(new PreservationSpaceOrganizationalUnitPreservationPolicyAction.UpdateFail(action, action.parentId));
            return;
          }
          this._updateSubResource(action, ctx)
            .subscribe(success => {
              if (success) {
                ctx.dispatch(new PreservationSpaceOrganizationalUnitPreservationPolicyAction.UpdateSuccess(action, action.parentId));
              } else {
                ctx.dispatch(new PreservationSpaceOrganizationalUnitPreservationPolicyAction.UpdateFail(action, action.parentId));
              }
            });
        }),
      );
  }

  protected _updateSubResource(action: PreservationSpaceOrganizationalUnitPreservationPolicyAction.Update, ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitPreservationPolicyStateModel>): Observable<boolean> {
    if (action.oldDefaultResId !== action.newDefaultResId && isNotNullNorUndefined(action.newDefaultResId)) {
      return StoreUtil.dispatchSequentialActionAndWaitForSubActionsCompletion(ctx, [{
        action: new PreservationSpaceOrganizationalUnitPreservationPolicyAction.UpdateRelation(action.parentId, action.newDefaultResId, {defaultPolicy: true} as OrganizationalUnitPreservationPolicy),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitPreservationPolicyAction.UpdateRelationSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitPreservationPolicyAction.UpdateRelationFail)),
        ],
      }]).pipe(
        map(result => result.success),
      );
    }
    return of(true);
  }
}
