/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {PreservationSpaceOrganizationalUnitPersonRoleAction} from "@app/features/preservation-space/organizational-unit/stores/organizational-unit-person-role/preservation-space-organizational-unit-person-role.action";
import {
  defaultPreservationSpaceOrganizationalUnitPersonRoleStateModel,
  PreservationSpaceOrganizationalUnitPersonRoleState,
  PreservationSpaceOrganizationalUnitPersonRoleStateModel,
} from "@app/features/preservation-space/organizational-unit/stores/organizational-unit-person-role/preservation-space-organizational-unit-person-role.state";
import {
  PreservationSpaceOrganizationalUnitAction,
  preservationSpaceOrganizationalUnitActionNameSpace,
} from "@app/features/preservation-space/organizational-unit/stores/preservation-space-organizational-unit.action";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {
  AppRoutesEnum,
  RoutesEnum,
  urlSeparator,
} from "@app/shared/enums/routes.enum";
import {AppAuthorizedOrganizationalUnitAction} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.action";
import {AppMemberOrganizationalUnitAction} from "@app/stores/member-organizational-unit/app-member-organizational-unit.action";
import {AppUserState} from "@app/stores/user/app-user.state";
import {environment} from "@environments/environment";
import {OrganizationalUnit} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {
  PreservationSpaceOrganizationalUnitArchiveAclState,
  PreservationSpaceOrganizationalUnitArchiveAclStateModel,
} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.state";
import {PreservationSpaceOrganizationalUnitDisseminationPolicyAction} from "@preservation-space/organizational-unit/stores/dissemination-policy/preservation-space-organizational-unit-dissemination-policy.action";
import {
  PreservationSpaceOrganizationalUnitDisseminationPolicyState,
  PreservationSpaceOrganizationalUnitDisseminationPolicyStateModel,
} from "@preservation-space/organizational-unit/stores/dissemination-policy/preservation-space-organizational-unit-dissemination-policy.state";
import {PreservationSpaceOrganizationalUnitInstitutionAction} from "@preservation-space/organizational-unit/stores/institution/preservation-space-organizational-unit-institution.action";
import {
  PreservationSpaceOrganizationalUnitInstitutionState,
  PreservationSpaceOrganizationalUnitInstitutionStateModel,
} from "@preservation-space/organizational-unit/stores/institution/preservation-space-organizational-unit-institution.state";
import {PreservationSpaceOrganizationalUnitPersonInheritedRoleAction} from "@preservation-space/organizational-unit/stores/organizational-unit-person-inherited-role/preservation-space-organizational-unit-person-inherited-role.action";
import {
  defaultPreservationSpaceOrganizationalUnitPersonInheritedRoleStateModel,
  PreservationSpaceOrganizationalUnitPersonInheritedRoleState,
  PreservationSpaceOrganizationalUnitPersonInheritedRoleStateModel,
} from "@preservation-space/organizational-unit/stores/organizational-unit-person-inherited-role/preservation-space-organizational-unit-person-inherited-role.state";
import {PreservationSpaceOrganizationalUnitPreservationPolicyAction} from "@preservation-space/organizational-unit/stores/preservation-policy/preservation-space-organizational-unit-preservation-policy.action";
import {
  PreservationSpaceOrganizationalUnitPreservationPolicyState,
  PreservationSpaceOrganizationalUnitPreservationPolicyStateModel,
} from "@preservation-space/organizational-unit/stores/preservation-policy/preservation-space-organizational-unit-preservation-policy.state";
import {
  PreservationSpaceOrganizationalUnitSubjectAreaState,
  PreservationSpaceOrganizationalUnitSubjectAreaStateModel,
} from "@preservation-space/organizational-unit/stores/subject-area/preservation-space-organizational-unit-subject-area-state.service";
import {PreservationSpaceOrganizationalUnitSubjectAreaAction} from "@preservation-space/organizational-unit/stores/subject-area/preservation-space-organizational-unit-subject-area.action";
import {PreservationSpaceOrganizationalUnitSubmissionPolicyAction} from "@preservation-space/organizational-unit/stores/submission-policy/preservation-space-organizational-unit-submission-policy.action";
import {
  PreservationSpaceOrganizationalUnitSubmissionPolicyState,
  PreservationSpaceOrganizationalUnitSubmissionPolicyStateModel,
} from "@preservation-space/organizational-unit/stores/submission-policy/preservation-space-organizational-unit-submission-policy.state";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {SharedDisseminationPolicyAction} from "@shared/stores/dissemination-policy/shared-dissemination-policy.action";
import {SharedPreservationPolicyAction} from "@shared/stores/preservation-policy/shared-preservation-policy.action";
import {SharedSubmissionPolicyAction} from "@shared/stores/submission-policy/shared-submission-policy.action";
import {Observable} from "rxjs";
import {
  catchError,
  map,
  tap,
} from "rxjs/operators";
import {
  ActionSubActionCompletionsWrapper,
  ApiService,
  ArrayUtil,
  CollectionTyped,
  defaultAssociationStateInitValue,
  defaultMultiRelation2TiersStateInitValue,
  defaultRelation2TiersStateInitValue,
  defaultResourceFileStateInitValue,
  defaultResourceStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isArray,
  isFalse,
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isTrue,
  isUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OverrideDefaultAction,
  Relation3TiersForm,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

export interface PreservationSpaceOrganizationalUnitStateModel extends ResourceFileStateModel<OrganizationalUnit> {
  [StateEnum.preservationSpace_organizationalUnit_personRole]: PreservationSpaceOrganizationalUnitPersonRoleStateModel;
  [StateEnum.preservationSpace_organizationalUnit_personInheritedRole]: PreservationSpaceOrganizationalUnitPersonInheritedRoleStateModel;
  [StateEnum.preservationSpace_organizationalUnit_archiveAcl]: PreservationSpaceOrganizationalUnitArchiveAclStateModel;
  [StateEnum.preservationSpace_organizationalUnit_submissionPolicy]: PreservationSpaceOrganizationalUnitSubmissionPolicyStateModel;
  [StateEnum.preservationSpace_organizationalUnit_preservationPolicy]: PreservationSpaceOrganizationalUnitPreservationPolicyStateModel;
  [StateEnum.preservationSpace_organizationalUnit_disseminationPolicy]: PreservationSpaceOrganizationalUnitDisseminationPolicyStateModel;
  [StateEnum.preservationSpace_organizationalUnit_institution]: PreservationSpaceOrganizationalUnitInstitutionStateModel;
  [StateEnum.preservationSpace_organizationalUnit_subjectArea]: PreservationSpaceOrganizationalUnitSubjectAreaStateModel;
  currentUserIsManager: boolean;
  currentUserIsMemberOrgUnit: boolean;
}

@Injectable()
@State<PreservationSpaceOrganizationalUnitStateModel>({
  name: StateEnum.preservationSpace_organizationalUnit,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    currentUserIsManager: undefined,
    currentUserIsMemberOrgUnit: undefined,
    [StateEnum.preservationSpace_organizationalUnit_personRole]: {...defaultPreservationSpaceOrganizationalUnitPersonRoleStateModel()},
    [StateEnum.preservationSpace_organizationalUnit_personInheritedRole]: {...defaultPreservationSpaceOrganizationalUnitPersonInheritedRoleStateModel()},
    [StateEnum.preservationSpace_organizationalUnit_archiveAcl]: {...defaultResourceStateInitValue()},
    [StateEnum.preservationSpace_organizationalUnit_submissionPolicy]: {...defaultRelation2TiersStateInitValue()},
    [StateEnum.preservationSpace_organizationalUnit_preservationPolicy]: {...defaultRelation2TiersStateInitValue()},
    [StateEnum.preservationSpace_organizationalUnit_disseminationPolicy]: {...defaultMultiRelation2TiersStateInitValue()},
    [StateEnum.preservationSpace_organizationalUnit_institution]: {...defaultAssociationStateInitValue()},
    [StateEnum.preservationSpace_organizationalUnit_subjectArea]: {...defaultAssociationStateInitValue()},
  },
  children: [
    PreservationSpaceOrganizationalUnitPersonRoleState,
    PreservationSpaceOrganizationalUnitPersonInheritedRoleState,
    PreservationSpaceOrganizationalUnitArchiveAclState,
    PreservationSpaceOrganizationalUnitDisseminationPolicyState,
    PreservationSpaceOrganizationalUnitSubmissionPolicyState,
    PreservationSpaceOrganizationalUnitPreservationPolicyState,
    PreservationSpaceOrganizationalUnitInstitutionState,
    PreservationSpaceOrganizationalUnitSubjectAreaState,
  ],
})
export class PreservationSpaceOrganizationalUnitState extends ResourceFileState<PreservationSpaceOrganizationalUnitStateModel, OrganizationalUnit> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              private readonly _securityService: SecurityService,
  ) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: preservationSpaceOrganizationalUnitActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.preservationSpaceOrganizationalUnitDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.preservationSpaceOrganizationalUnitDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessDeleteAction: RoutesEnum.preservationSpaceOrganizationalUnit,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("organizationalUnit.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("organizationalUnit.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("organizationalUnit.notification.resource.update"),
      keepCurrentStateAfterUpdate: true,
      updateSubResourceDispatchMethod: DispatchMethodEnum.SEQUENCIAL,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    const accessResourceApiEnum = ApiEnum.accessOrganizationalUnits;
    const adminResourceApiEnum = ApiEnum.adminOrganizationalUnits;

    const orgUnitId = this._store.selectSnapshot(state => state.router.state.root.children[0].children[0].children[0].children[0].params[AppRoutesEnum.paramIdWithoutPrefixParam]);
    return this._securityService.isManagerOfOrgUnit(orgUnitId) ? adminResourceApiEnum : accessResourceApiEnum;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static currentUserIsManager(state: PreservationSpaceOrganizationalUnitStateModel): boolean | undefined {
    return state.currentUserIsManager;
  }

  @Selector()
  static currentUserIsMemberOrgUnit(state: PreservationSpaceOrganizationalUnitStateModel): boolean | undefined {
    return state.currentUserIsMemberOrgUnit;
  }

  @Selector()
  static currentTitle(state: PreservationSpaceOrganizationalUnitStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isLoading(state: PreservationSpaceOrganizationalUnitStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: PreservationSpaceOrganizationalUnitStateModel): boolean {
    return this.isLoading(state)
      || isUndefined(this.currentUserIsManager(state))
      || isUndefined(this.currentUserIsMemberOrgUnit(state))
      || StoreUtil.isLoadingState(state.preservationSpace_organizationalUnit_submissionPolicy)
      || StoreUtil.isLoadingState(state.preservationSpace_organizationalUnit_preservationPolicy)
      || StoreUtil.isLoadingState(state.preservationSpace_organizationalUnit_disseminationPolicy)
      || StoreUtil.isLoadingState(state.preservationSpace_organizationalUnit_personRole)
      || StoreUtil.isLoadingState(state.preservationSpace_organizationalUnit_institution);

  }

  @Selector()
  static isReadyToBeDisplayed(state: PreservationSpaceOrganizationalUnitStateModel): boolean {
    return isNotNullNorUndefined(state.current)
      && !isUndefined(this.currentUserIsManager(state))
      && !isUndefined(this.currentUserIsMemberOrgUnit(state))
      && (isFalse(this.currentUserIsMemberOrgUnit(state)) ||
        isNotNullNorUndefined(state.preservationSpace_organizationalUnit_personRole.selected)
      )
      && (isFalse(this.currentUserIsManager(state)) || (
        isNotNullNorUndefined(state.preservationSpace_organizationalUnit_preservationPolicy.selected)
        && isNotNullNorUndefined(state.preservationSpace_organizationalUnit_disseminationPolicy.selected)
        && isNotNullNorUndefined(state.preservationSpace_organizationalUnit_submissionPolicy.selected)
      ))
      && isNotNullNorUndefined(state.preservationSpace_organizationalUnit_institution.selected);
  }

  @Action(PreservationSpaceOrganizationalUnitAction.LoadResource)
  loadResource(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>, action: PreservationSpaceOrganizationalUnitAction.LoadResource): Observable<boolean> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
    });

    return StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(ctx, [
      {
        action: new SharedPreservationPolicyAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPreservationPolicyAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedPreservationPolicyAction.GetAllFail)),
        ],
      },
      {
        action: new SharedSubmissionPolicyAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedSubmissionPolicyAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedSubmissionPolicyAction.GetAllFail)),
        ],
      },
      {
        action: new SharedDisseminationPolicyAction.GetAll(undefined, false, false),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(SharedDisseminationPolicyAction.GetAllSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(SharedDisseminationPolicyAction.GetAllFail)),
        ],
      },
    ]).pipe(
      map(result => {
        if (result.success) {
          ctx.dispatch(new PreservationSpaceOrganizationalUnitAction.LoadResourceSuccess(action));
        } else {
          ctx.dispatch(new PreservationSpaceOrganizationalUnitAction.LoadResourceFail(action));
        }
        return result.success;
      }));
  }

  @OverrideDefaultAction()
  @Action(PreservationSpaceOrganizationalUnitAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>, action: PreservationSpaceOrganizationalUnitAction.GetByIdSuccess): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    ctx.dispatch(new PreservationSpaceOrganizationalUnitAction.ComputeCurrentUserAffiliation());

    if (isNotNullNorUndefined(action.model.logo)) {
      ctx.dispatch(new PreservationSpaceOrganizationalUnitAction.GetFile(action.model.resId));
    }
  }

  @Action(PreservationSpaceOrganizationalUnitAction.ComputeCurrentUserAffiliation)
  computeCurrentUserAffiliation(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>, action: PreservationSpaceOrganizationalUnitAction.ComputeCurrentUserAffiliation): void | boolean {
    const isManager = this._securityService.isManagerOfOrgUnit(ctx.getState().current.resId);
    const isMemberOrgUnit = this._securityService.isRootOrAdmin() || this._securityService.isMemberOfOrgUnit(ctx.getState().current.resId);
    ctx.dispatch(new PreservationSpaceOrganizationalUnitAction.SaveCurrentUserAffiliationAndRetrievePersonRoleIfMember(isManager, isMemberOrgUnit));
    return isManager;
  }

  @Action(PreservationSpaceOrganizationalUnitAction.SaveCurrentUserAffiliation)
  saveCurrentUserAffiliation(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>, action: PreservationSpaceOrganizationalUnitAction.SaveCurrentUserAffiliation): void {
    ctx.patchState({
      currentUserIsManager: action.isManager,
      currentUserIsMemberOrgUnit: action.isMemberOrgUnit,
    });
  }

  @Action(PreservationSpaceOrganizationalUnitAction.SaveCurrentUserAffiliationAndRetrievePersonRoleIfMember)
  saveCurrentUserAffiliationAndRetrievePersonRoleIfMember(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>, action: PreservationSpaceOrganizationalUnitAction.SaveCurrentUserAffiliationAndRetrievePersonRoleIfMember): void {
    ctx.dispatch(new PreservationSpaceOrganizationalUnitAction.SaveCurrentUserAffiliation(action.isManager, action.isMemberOrgUnit));
    if (isTrue(action.isMemberOrgUnit)) {
      const orgUnitId = ctx.getState().current.resId;
      this._store.dispatch(new PreservationSpaceOrganizationalUnitPersonRoleAction.GetAll(orgUnitId));
      this._store.dispatch(new PreservationSpaceOrganizationalUnitPersonInheritedRoleAction.GetAll(orgUnitId));
    }
  }

  @Action(PreservationSpaceOrganizationalUnitAction.LoadOnlyMyOrgUnits)
  loadOnlyMyOrgUnits(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>, action: PreservationSpaceOrganizationalUnitAction.LoadOnlyMyOrgUnits): Observable<CollectionTyped<OrganizationalUnit>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });

    return this._apiService.getCollection<OrganizationalUnit>(ApiEnum.adminAuthorizedOrganizationalUnits, ctx.getState().queryParameters)
      .pipe(
        tap((collection: CollectionTyped<OrganizationalUnit>) => {
          ctx.dispatch(new PreservationSpaceOrganizationalUnitAction.LoadOnlyMyOrgUnitsSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationSpaceOrganizationalUnitAction.LoadOnlyMyOrgUnitsFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(PreservationSpaceOrganizationalUnitAction.LoadOnlyMyOrgUnitsSuccess)
  loadOnlyMyOrgUnitsSuccess(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>, action: PreservationSpaceOrganizationalUnitAction.LoadOnlyMyOrgUnitsSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    ctx.patchState({
      list: action.list._data,
      total: action.list._page.totalItems,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters,
    });
  }

  @Action(PreservationSpaceOrganizationalUnitAction.LoadOnlyMyOrgUnitsFail)
  loadOnlyMyOrgUnitsFail(ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>, action: PreservationSpaceOrganizationalUnitAction.LoadOnlyMyOrgUnitsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }

  protected override _getListActionsUpdateSubResource(model: OrganizationalUnit, action: PreservationSpaceOrganizationalUnitAction.Create | PreservationSpaceOrganizationalUnitAction.Update, ctx: SolidifyStateContext<PreservationSpaceOrganizationalUnitStateModel>): ActionSubActionCompletionsWrapper[] {
    const actions = super._getListActionsUpdateSubResource(model, action, ctx);

    const orgUnitId = model.resId;

    const newSubjectAreasIds = action.modelFormControlEvent.model.subjectAreas.map(p => p.resId);

    let oldInstitutionsIds = [];
    const newInstitutionsIds = action.modelFormControlEvent.model.institutions.map(p => p.resId);

    let oldDefaultPreservationPolicyId = undefined;
    const newDefaultPreservationPolicyId = action.modelFormControlEvent.model.defaultPreservationPolicy?.resId;
    const newPreservationPoliciesIds = action.modelFormControlEvent.model.preservationPolicies.map(p => p.resId);

    let oldDefaultSubmissionPolicyId = undefined;
    const newDefaultSubmissionPolicyId = action.modelFormControlEvent.model.defaultSubmissionPolicy?.resId;
    const newSubmissionPoliciesIds = action.modelFormControlEvent.model.submissionPolicies.map(p => p.resId);

    const oldOrgUnit = ctx.getState().current;
    if (isNotNullNorUndefined(oldOrgUnit)) {
      oldDefaultPreservationPolicyId = oldOrgUnit.defaultPreservationPolicy?.resId;
      oldDefaultSubmissionPolicyId = oldOrgUnit.defaultSubmissionPolicy?.resId;
      const oldInstitutions = MemoizedUtil.selectedSnapshot(this._store, PreservationSpaceOrganizationalUnitInstitutionState);
      oldInstitutionsIds = oldInstitutions?.map(i => i.resId) ?? [];
    }

    let concernCurrentUser = false;
    const currentPersonId = MemoizedUtil.currentSnapshot(this._store, AppUserState)?.person?.resId;

    const oldListPersonsRoles = MemoizedUtil.selectedSnapshot(this._store, PreservationSpaceOrganizationalUnitPersonRoleState);
    if (oldListPersonsRoles.findIndex(p => p.resId === currentPersonId) >= 0) {
      concernCurrentUser = true;
    }

    const newPersonRole = action.modelFormControlEvent.formControl.get("personRole").value as Relation3TiersForm[];
    newPersonRole.forEach((elem) => {
      if (elem.id === currentPersonId) {
        concernCurrentUser = true;
      }
      if (!isNullOrUndefined(elem.listId) && !isArray(elem.listId)) {
        elem.listId = [elem.listId];
      }
    });

    actions.push(...[
      {
        action: new PreservationSpaceOrganizationalUnitPreservationPolicyAction.Update(orgUnitId, newPreservationPoliciesIds, oldDefaultPreservationPolicyId, newDefaultPreservationPolicyId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitPreservationPolicyAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitPreservationPolicyAction.UpdateFail)),
        ],
      },
      {
        action: new PreservationSpaceOrganizationalUnitSubmissionPolicyAction.Update(orgUnitId, newSubmissionPoliciesIds, oldDefaultSubmissionPolicyId, newDefaultSubmissionPolicyId),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitSubmissionPolicyAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitSubmissionPolicyAction.UpdateFail)),
        ],
      },
      {
        action: new PreservationSpaceOrganizationalUnitDisseminationPolicyAction.Update(orgUnitId, action.modelFormControlEvent.model.disseminationPolicies),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitDisseminationPolicyAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitDisseminationPolicyAction.UpdateFail)),
        ],
      },
      {
        action: new PreservationSpaceOrganizationalUnitPersonRoleAction.Update(orgUnitId, newPersonRole),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitPersonRoleAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitPersonRoleAction.UpdateFail)),
        ],
      },
      {
        action: new PreservationSpaceOrganizationalUnitInstitutionAction.Update(orgUnitId, newInstitutionsIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitInstitutionAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitInstitutionAction.UpdateFail)),
        ],
      },
      {
        action: new PreservationSpaceOrganizationalUnitSubjectAreaAction.Update(orgUnitId, newSubjectAreasIds),
        subActionCompletions: [
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitSubjectAreaAction.UpdateSuccess)),
          this._actions$.pipe(ofSolidifyActionCompleted(PreservationSpaceOrganizationalUnitSubjectAreaAction.UpdateFail)),
        ],
      },
    ]);

    const diffInstitution = ArrayUtil.diff(oldInstitutionsIds, newInstitutionsIds);
    if (concernCurrentUser || isNonEmptyArray(diffInstitution.diff)) {
      actions.push(...[
        {
          action: new AppAuthorizedOrganizationalUnitAction.GetAll(undefined, false, false),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AppAuthorizedOrganizationalUnitAction.GetAllFail)),
          ],
        },
        {
          action: new AppMemberOrganizationalUnitAction.GetAll(undefined, false, false),
          subActionCompletions: [
            this._actions$.pipe(ofSolidifyActionCompleted(AppMemberOrganizationalUnitAction.GetAllSuccess)),
            this._actions$.pipe(ofSolidifyActionCompleted(AppMemberOrganizationalUnitAction.GetAllFail)),
          ],
        },
      ]);
    }

    return actions;
  }
}
