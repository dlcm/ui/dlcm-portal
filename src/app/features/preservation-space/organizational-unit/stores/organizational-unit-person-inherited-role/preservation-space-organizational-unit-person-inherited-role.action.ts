/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-person-inherited-role.action.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseAction,
  BaseSubActionFail,
  BaseSubActionSuccess,
} from "solidify-frontend";

const state = StateEnum.preservationSpace_organizationalUnit_personInheritedRole;

export namespace PreservationSpaceOrganizationalUnitPersonInheritedRoleAction {
  export class GetAll extends BaseAction {
    static readonly type: string = `[${state}] Get All`;

    constructor(public orgUnitId: string) {
      super();
    }
  }

  export class GetAllSuccess extends BaseSubActionSuccess<GetAll> {
    static readonly type: string = `[${state}] Get All Success`;

    constructor(public parentAction: GetAll, public list: PersonRole[]) {
      super(parentAction);
    }
  }

  export class GetAllFail extends BaseSubActionFail<GetAll> {
    static readonly type: string = `[${state}] Get All Fail`;
  }
}

export const preservationSpaceOrganizationalUnitPersonInheritedRoleActionNameSpace = PreservationSpaceOrganizationalUnitPersonInheritedRoleAction;
