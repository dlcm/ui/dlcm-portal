/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {PreservationSpaceOrganizationalUnitRoutingModule} from "@app/features/preservation-space/organizational-unit/preservation-space-organizational-unit-routing.module";
import {PreservationSpaceOrganizationalUnitPersonRoleState} from "@app/features/preservation-space/organizational-unit/stores/organizational-unit-person-role/preservation-space-organizational-unit-person-role.state";
import {PreservationSpaceOrganizationalUnitState} from "@app/features/preservation-space/organizational-unit/stores/preservation-space-organizational-unit.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {PreservationSpaceOrganizationalUnitRequestCreationDialog} from "@preservation-space/organizational-unit/components/dialogs/organizational-unit-request-creation/preservation-space-organizational-unit-request-creation.dialog";
import {PreservationSpaceOrganizationalUnitArchiveAclFormPresentational} from "@preservation-space/organizational-unit/components/presentationals/organizational-unit-archive-acl-form/preservation-space-organizational-unit-archive-acl-form.presentational";
import {PreservationSpaceOrganizationalUnitFormPresentational} from "@preservation-space/organizational-unit/components/presentationals/organizational-unit-form/preservation-space-organizational-unit-form.presentational";
import {PreservationSpaceOrganizationalUnitArchiveAclCreateRoutable} from "@preservation-space/organizational-unit/components/routables/organizational-unit-archive-acl-create/preservation-space-organizational-unit-archive-acl-create.routable";
import {PreservationSpaceOrganizationalUnitArchiveAclDetailEditRoutable} from "@preservation-space/organizational-unit/components/routables/organizational-unit-archive-acl-detail-edit/preservation-space-organizational-unit-archive-acl-detail-edit.routable";
import {PreservationSpaceOrganizationalUnitArchiveAclListRoutable} from "@preservation-space/organizational-unit/components/routables/organizational-unit-archive-acl-list/preservation-space-organizational-unit-archive-acl-list.routable";
import {PreservationSpaceOrganizationalUnitDetailEditRoutable} from "@preservation-space/organizational-unit/components/routables/organizational-unit-detail-edit/preservation-space-organizational-unit-detail-edit.routable";
import {PreservationSpaceOrganizationalUnitListRoutable} from "@preservation-space/organizational-unit/components/routables/organizational-unit-list/preservation-space-organizational-unit-list.routable";
import {PreservationSpaceOrganizationalUnitArchiveAclState} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.state";
import {PreservationSpaceOrganizationalUnitInstitutionState} from "@preservation-space/organizational-unit/stores/institution/preservation-space-organizational-unit-institution.state";
import {PreservationSpaceOrganizationalUnitPersonInheritedRoleState} from "@preservation-space/organizational-unit/stores/organizational-unit-person-inherited-role/preservation-space-organizational-unit-person-inherited-role.state";
import {PreservationSpaceOrganizationalUnitSubjectAreaState} from "@preservation-space/organizational-unit/stores/subject-area/preservation-space-organizational-unit-subject-area-state.service";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SharedModule} from "@shared/shared.module";
import {SsrUtil} from "solidify-frontend";
import {PreservationSpaceOrganizationalUnitRequestAccessDialog} from "./components/dialogs/organizational-unit-request-access/preservation-space-organizational-unit-request-access.dialog";

const routables = [
  PreservationSpaceOrganizationalUnitDetailEditRoutable,
  PreservationSpaceOrganizationalUnitListRoutable,
  PreservationSpaceOrganizationalUnitArchiveAclListRoutable,
  PreservationSpaceOrganizationalUnitArchiveAclDetailEditRoutable,
  PreservationSpaceOrganizationalUnitArchiveAclCreateRoutable,
];
const containers = [];
const dialogs = [
  PreservationSpaceOrganizationalUnitRequestAccessDialog,
  PreservationSpaceOrganizationalUnitRequestCreationDialog,
];
const presentationals = [
  PreservationSpaceOrganizationalUnitFormPresentational,
  PreservationSpaceOrganizationalUnitArchiveAclFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    PreservationSpaceOrganizationalUnitRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      PreservationSpaceOrganizationalUnitState,
      PreservationSpaceOrganizationalUnitPersonRoleState,
      PreservationSpaceOrganizationalUnitPersonInheritedRoleState,
      PreservationSpaceOrganizationalUnitArchiveAclState,
      PreservationSpaceOrganizationalUnitInstitutionState,
      PreservationSpaceOrganizationalUnitSubjectAreaState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationSpaceOrganizationalUnitModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.preservationSpaceOrganizationalUnitModuleLoaded] = true;
    }
  }
}
