/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-organizational-unit-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {Enums} from "@enums";
import {PreservationSpaceOrganizationalUnitArchiveAclCreateRoutable} from "@preservation-space/organizational-unit/components/routables/organizational-unit-archive-acl-create/preservation-space-organizational-unit-archive-acl-create.routable";
import {PreservationSpaceOrganizationalUnitArchiveAclDetailEditRoutable} from "@preservation-space/organizational-unit/components/routables/organizational-unit-archive-acl-detail-edit/preservation-space-organizational-unit-archive-acl-detail-edit.routable";
import {PreservationSpaceOrganizationalUnitArchiveAclListRoutable} from "@preservation-space/organizational-unit/components/routables/organizational-unit-archive-acl-list/preservation-space-organizational-unit-archive-acl-list.routable";
import {PreservationSpaceOrganizationalUnitDetailEditRoutable} from "@preservation-space/organizational-unit/components/routables/organizational-unit-detail-edit/preservation-space-organizational-unit-detail-edit.routable";
import {PreservationSpaceOrganizationalUnitListRoutable} from "@preservation-space/organizational-unit/components/routables/organizational-unit-list/preservation-space-organizational-unit-list.routable";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  AppRoutesEnum,
  PreservationSpaceOrganizationalUnitRoutesEnum,
} from "@shared/enums/routes.enum";
import {OrganizationalUnitComputeIsManagerGuardService} from "@shared/guards/organizational-unit-compute-is-manager.service";
import {OrganizationalUnitRoleGuardService} from "@shared/guards/organizational-unit-role-guard.service";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";
import {
  CanDeactivateGuard,
  CombinedGuardService,
  EmptyContainer,
} from "solidify-frontend";
import {PreservationSpaceOrganizationalUnitState} from "./stores/preservation-space-organizational-unit.state";

const routes: DlcmRoutes = [
  {
    path: "",
    component: PreservationSpaceOrganizationalUnitListRoutable,
    data: {},
  },
  {
    path: PreservationSpaceOrganizationalUnitRoutesEnum.detail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: PreservationSpaceOrganizationalUnitDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: PreservationSpaceOrganizationalUnitState.currentTitle,
      guards: [OrganizationalUnitComputeIsManagerGuardService],
    },
    canActivate: [CombinedGuardService],
    children: [
      {
        path: PreservationSpaceOrganizationalUnitRoutesEnum.edit,
        component: EmptyContainer,
        data: {
          breadcrumb: LabelTranslateEnum.edit,
          orgUnitPermissionNeed: [Enums.Role.RoleEnum.MANAGER],
          guards: [OrganizationalUnitRoleGuardService],
        },
        canActivate: [CombinedGuardService],
        canDeactivate: [CanDeactivateGuard],
      },
      {
        path: PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl,
        data: {
          breadcrumb: LabelTranslateEnum.archiveAcl,
          orgUnitPermissionNeed: [Enums.Role.RoleEnum.STEWARD, Enums.Role.RoleEnum.MANAGER],
          guards: [OrganizationalUnitRoleGuardService],
        },
        canActivate: [CombinedGuardService],
        children: [
          {
            path: "",
            component: PreservationSpaceOrganizationalUnitArchiveAclListRoutable,
          },
          {
            path: PreservationSpaceOrganizationalUnitRoutesEnum.manageAclDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
            component: PreservationSpaceOrganizationalUnitArchiveAclDetailEditRoutable,
            data: {
              // breadcrumb: LabelTranslateEnum.archiveAcl,
            },
            children: [
              {
                path: AdminRoutesEnum.archiveAclEdit,
                component: EmptyContainer,
                data: {
                  breadcrumb: LabelTranslateEnum.edit,
                },
                canDeactivate: [CanDeactivateGuard],
              },
            ],
          },
          {
            component: PreservationSpaceOrganizationalUnitArchiveAclCreateRoutable,
            path: PreservationSpaceOrganizationalUnitRoutesEnum.manageAclCreate,
            data: {
              breadcrumb: LabelTranslateEnum.create,
            },
            canDeactivate: [CanDeactivateGuard],
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreservationSpaceOrganizationalUnitRoutingModule {
}
