/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-institution-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {LocalStorageEnum} from "@app/shared/enums/local-storage.enum";
import {Institution} from "@models";
import {TranslateService} from "@ngx-translate/core";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {
  PreservationSpaceInstitutionAction,
  preservationSpaceInstitutionActionNameSpace,
} from "@preservation-space/institution/stores/preservation-space-institution.action";
import {DataTableComponentEnum} from "@shared/enums/data-table-component.enum";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {DataTableComponentHelper} from "@shared/helpers/data-table-component.helper";
import {SecurityService} from "@shared/services/security.service";
import {
  AbstractListRoutable,
  CookieConsentUtil,
  CookieType,
  DataTableColumns,
  DataTableFieldTypeEnum,
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
  LocalStorageHelper,
  MappingObjectUtil,
  MemoizedUtil,
  OrderEnum,
  QueryParameters,
  QueryParametersUtil,
  RouterExtensionService,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";
import {
  PreservationSpaceInstitutionState,
  PreservationSpaceInstitutionStateModel,
} from "../../../stores/preservation-space-institution.state";

const onlyMyInstitutionsStorageKey: LocalStorageEnum = LocalStorageEnum.preservationSpaceInstitutionShowOnlyMine;
const onlyMyInstitutionsFn: () => boolean = () => {
  const storageValue = LocalStorageHelper.getItem(onlyMyInstitutionsStorageKey);
  if (isNullOrUndefined(storageValue)) {
    return true;
  }
  return storageValue === SOLIDIFY_CONSTANTS.STRING_TRUE;
};

@Component({
  selector: "dlcm-preservation-space-institution-list-routable",
  templateUrl: "../../../../../../../../node_modules/solidify-frontend/lib/application/components/routables/abstract-list/abstract-list.routable.html",
  styleUrls: ["./preservation-space-institution-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceInstitutionListRoutable extends AbstractListRoutable<Institution, PreservationSpaceInstitutionStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = undefined;
  readonly KEY_PARAM_NAME: keyof Institution & string = "name";

  readonly KEY_SHOW_MEMBERS: string = "showMembers";

  preservationSpaceInstitutionState: typeof PreservationSpaceInstitutionState = PreservationSpaceInstitutionState;

  columnsSkippedToClear: string[] = [this.KEY_SHOW_MEMBERS];

  private _onlyMyInstitutions: boolean = undefined;

  protected get onlyMyInstitutions(): boolean {
    if (isNotNullNorUndefined(this._onlyMyInstitutions)) {
      return this._onlyMyInstitutions;
    }
    return onlyMyInstitutionsFn();
  }

  protected set onlyMyInstitutions(value: boolean) {
    this._onlyMyInstitutions = value;
    if (CookieConsentUtil.isFeatureAuthorized(CookieType.localStorage, onlyMyInstitutionsStorageKey)) {
      LocalStorageHelper.setItem(onlyMyInstitutionsStorageKey, this.onlyMyInstitutions + "");
    }
  }

  override stickyTopPosition: number = 0;

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _translate: TranslateService,
              private readonly _securityService: SecurityService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.preservationSpace_institution, preservationSpaceInstitutionActionNameSpace, _injector, {
      canCreate: false,
      canGoBack: false,
      listExtraButtons: [
        {
          color: "primary",
          icon: null,
          labelToTranslate: (current) => LabelTranslateEnum.showOnlyMyInstitutions,
          callback: (model, buttonElementRef, checked) => this._showOnlyMyInstitutions(checked),
          order: 40,
          isToggleButton: true,
          isToggleChecked: onlyMyInstitutionsFn(),
          displayCondition: current => !_securityService.isRootOrAdmin(),
        },
      ],
    }, StateEnum.preservationSpace);
  }

  private _showOnlyMyInstitutions(checked: boolean): void {
    if (checked) {
      //show only authorized institutions
      const currentQueryParam = MemoizedUtil.queryParametersSnapshot(this._store, PreservationSpaceInstitutionState);
      const newQueryParam = new QueryParameters(currentQueryParam?.paging?.pageSize);
      MappingObjectUtil.set(newQueryParam.search.searchItems, this.KEY_SHOW_MEMBERS, SOLIDIFY_CONSTANTS.STRING_TRUE);
      this._store.dispatch(new PreservationSpaceInstitutionAction.LoadOnlyMyInstitutions(newQueryParam));
      this.onlyMyInstitutions = true;
    } else {
      // all org units
      // remove filter by name if applied
      const queryParameters = QueryParametersUtil.clone(MemoizedUtil.queryParametersSnapshot(this._store, PreservationSpaceInstitutionState));
      MappingObjectUtil.delete(QueryParametersUtil.getSearchItems(queryParameters), this.KEY_PARAM_NAME);
      this._store.dispatch(new PreservationSpaceInstitutionAction.GetAll(queryParameters));
      this.onlyMyInstitutions = false;
    }

    this.defineColumns();
    this._changeDetector.detectChanges();
  }

  override onQueryParametersEvent(queryParameters: QueryParameters): void {
    if (this.onlyMyInstitutions) {
      MappingObjectUtil.set(queryParameters.search.searchItems, this.KEY_SHOW_MEMBERS, SOLIDIFY_CONSTANTS.STRING_TRUE);
      this._store.dispatch(new PreservationSpaceInstitutionAction.LoadOnlyMyInstitutions(queryParameters));
    } else {
      this._store.dispatch(new PreservationSpaceInstitutionAction.ChangeQueryParameters(queryParameters, true));
    }
    this._changeDetector.detectChanges(); // Allow to display spinner the first time
  }

  override goToEdit(model: Institution): void {
    this._store.dispatch(new Navigate([RoutesEnum.adminInstitutionDetail, model.resId, AdminRoutesEnum.institutionEdit]));
  }

  conditionDisplayEditButton(model: Institution | undefined): boolean {
    return this._securityService.isManagerOfInstitution(model?.resId);
  }

  conditionDisplayDeleteButton(model: Institution | undefined): boolean {
    return false;
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "avatar" as any,
        header: LabelTranslateEnum.logo,
        order: OrderEnum.none,
        resourceNameSpace: preservationSpaceInstitutionActionNameSpace,
        resourceState: this.preservationSpaceInstitutionState as any,
        isFilterable: false,
        isSortable: false,
        component: DataTableComponentHelper.get(DataTableComponentEnum.logo),
        skipIfAvatarMissing: true,
        idResource: (institution: Institution) => institution.resId,
        isLogoPresent: (institution: Institution) => isNotNullNorUndefined(institution.logo),
        width: "45px",
      } as DataTableColumns<Institution>,
      {
        field: "name",
        header: LabelTranslateEnum.nameLabel,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.ascending,
        isFilterable: true,
        isSortable: isFalse(this.onlyMyInstitutions),
        dataTest: DataTestEnum.preservationSpaceOrgUnitListSearchName,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "lastUpdate.when" as any,
        header: LabelTranslateEnum.updated,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: true,
        isSortable: true,
      },
      {
        field: "resId",
        header: "",
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        component: DataTableComponentHelper.get(DataTableComponentEnum.institutionMember),
        isFilterable: false,
        isSortable: false,
        width: "40px",
      },
    ];
  }
}
