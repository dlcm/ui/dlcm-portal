/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-institution-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {PersonRole} from "@admin/models/person-role.model";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {
  ActivatedRoute,
  RouterOutlet,
} from "@angular/router";
import {AppState} from "@app/stores/app.state";
import {AppAuthorizedInstitutionState} from "@app/stores/authorized-institution/app-authorized-institution.state";
import {AppUserState} from "@app/stores/user/app-user.state";
import {
  Deposit,
  Institution,
  OrganizationalUnit,
  Role,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {PreservationSpaceInstitutionPersonRoleAction} from "@preservation-space/institution/stores/institution-person-role/preservation-space-institution-person-role.action";
import {preservationSpaceInstitutionActionNameSpace} from "@preservation-space/institution/stores/preservation-space-institution.action";
import {SharedResourceRoleMemberContainerMode} from "@shared/components/containers/shared-resource-role-member/shared-resource-role-member-container.component";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AdminRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {sharedInstitutionActionNameSpace} from "@shared/stores/institution/shared-institution.action";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {Observable} from "rxjs";
import {
  AbstractDetailEditCommonRoutable,
  ExtraButtonToolbar,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
  ResourceNameSpace,
  ScrollService,
} from "solidify-frontend";
import {PreservationSpaceInstitutionPersonRoleState} from "../../../stores/institution-person-role/preservation-space-institution-person-role.state";
import {PreservationSpaceInstitutionOrganizationalUnitAction} from "../../../stores/organizational-unit/preservation-space-institution-organizational-unit.action";
import {PreservationSpaceInstitutionOrganizationalUnitState} from "../../../stores/organizational-unit/preservation-space-institution-organizational-unit.state";
import {
  PreservationSpaceInstitutionState,
  PreservationSpaceInstitutionStateModel,
} from "../../../stores/preservation-space-institution.state";

@Component({
  selector: "dlcm-preservation-space-institution-detail-edit-routable",
  templateUrl: "./preservation-space-institution-detail-edit.routable.html",
  styleUrls: ["./preservation-space-institution-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceInstitutionDetailEditRoutable extends AbstractDetailEditCommonRoutable<Institution, PreservationSpaceInstitutionStateModel> implements OnInit, OnDestroy {
  @Select(PreservationSpaceInstitutionState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(PreservationSpaceInstitutionState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;
  currentUserIsManager: Observable<boolean> = MemoizedUtil.select(this._store, PreservationSpaceInstitutionState, state => state.currentUserIsManager);
  currentUserIsMember: Observable<boolean> = MemoizedUtil.select(this._store, PreservationSpaceInstitutionState, state => state.currentUserIsMember);
  selectedOrgUnitsObs: Observable<OrganizationalUnit[]> = MemoizedUtil.selected(this._store, PreservationSpaceInstitutionOrganizationalUnitState);
  selectedPersonRoleObs: Observable<PersonRole[]> = MemoizedUtil.selected(this._store, PreservationSpaceInstitutionPersonRoleState);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedInstitutionActionNameSpace;

  readonly KEY_PARAM_NAME: keyof Institution & string = "name";

  override readonly deleteAvailable: boolean = false;

  isTour: boolean = false;

  canLeaveInstitution: boolean = false;

  @ViewChild(RouterOutlet) outlet: RouterOutlet;

  get currentPersonId(): string {
    return MemoizedUtil.currentSnapshot(this._store, AppUserState)?.person?.resId;
  }

  get sharedResourceRoleMemberContainerMode(): typeof SharedResourceRoleMemberContainerMode {
    return SharedResourceRoleMemberContainerMode;
  }

  listExtraButtons: ExtraButtonToolbar<Deposit>[] = [
    {
      color: "primary",
      icon: IconNameEnum.edit,
      displayCondition: current => this._securityService.isManagerOfInstitution(current?.resId),
      callback: current => this._store.dispatch(new Navigate([RoutesEnum.adminInstitutionDetail, current.resId, AdminRoutesEnum.institutionEdit])),
      disableCondition: current => isNullOrUndefined(current),
      labelToTranslate: (current) => LabelTranslateEnum.edit,
      order: 20,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _securityService: SecurityService,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _scrollService: ScrollService) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.preservationSpace_institution, _injector, preservationSpaceInstitutionActionNameSpace, StateEnum.preservationSpace);
    this.editAvailable = false;
    this.deleteAvailable = false;
  }

  ngOnInit(): void {
    if (MemoizedUtil.selectSnapshot(this._store, AppState, state => state.isInTourMode)) {
      this.isTour = true;
      return;
    }

    super.ngOnInit();

    const listAuthorizedInstitution = MemoizedUtil.listSnapshot(this._store, AppAuthorizedInstitutionState);
    this.canLeaveInstitution = isNotNullNorUndefined(listAuthorizedInstitution?.find(p => p.resId === this._resId)?.role);
  }

  _getSubResourceWithParentId(id: string): void {
    this._store.dispatch(new PreservationSpaceInstitutionOrganizationalUnitAction.GetAll(id));
    if (this._securityService.isRootOrAdmin()
      || this._securityService.isMemberWithOrWithoutRoleOnInstitution(id)) {
      this._store.dispatch(new PreservationSpaceInstitutionPersonRoleAction.GetAll(id));
    }
  }
}
