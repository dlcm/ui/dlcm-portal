/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-institution.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {PreservationSpaceInstitutionFormPresentational} from "@preservation-space/institution/components/presentationals/institution-form/preservation-space-institution-form.presentational";
import {PreservationSpaceInstitutionDetailEditRoutable} from "@preservation-space/institution/components/routables/institution-detail-edit/preservation-space-institution-detail-edit.routable";
import {PreservationSpaceInstitutionListRoutable} from "@preservation-space/institution/components/routables/institution-list/preservation-space-institution-list.routable";
import {PreservationSpaceInstitutionPersonRoleState} from "@preservation-space/institution/stores/institution-person-role/preservation-space-institution-person-role.state";
import {PreservationSpaceInstitutionOrganizationalUnitState} from "@preservation-space/institution/stores/organizational-unit/preservation-space-institution-organizational-unit.state";
import {PreservationSpaceInstitutionState} from "@preservation-space/institution/stores/preservation-space-institution.state";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SharedModule} from "@shared/shared.module";
import {SsrUtil} from "solidify-frontend";
import {PreservationSpaceInstitutionRoutingModule} from "./preservation-space-institution-routing.module";

const routables = [
  PreservationSpaceInstitutionDetailEditRoutable,
  PreservationSpaceInstitutionListRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [
  PreservationSpaceInstitutionFormPresentational,
];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    PreservationSpaceInstitutionRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      PreservationSpaceInstitutionState,
      PreservationSpaceInstitutionPersonRoleState,
      PreservationSpaceInstitutionOrganizationalUnitState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationSpaceInstitutionModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.preservationSpaceInstitutionModuleLoaded] = true;
    }
  }
}
