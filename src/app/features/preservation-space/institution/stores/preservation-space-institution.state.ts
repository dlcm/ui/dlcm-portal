/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-institution.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {ApiEnum} from "@app/shared/enums/api.enum";
import {environment} from "@environments/environment";
import {Institution} from "@models";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {
  defaultPreservationSpaceInstitutionPersonRoleStateModel,
  PreservationSpaceInstitutionPersonRoleState,
  PreservationSpaceInstitutionPersonRoleStateModel,
} from "@preservation-space/institution/stores/institution-person-role/preservation-space-institution-person-role.state";
import {
  PreservationSpaceInstitutionOrganizationalUnitState,
  PreservationSpaceInstitutionOrganizationalUnitStateModel,
} from "@preservation-space/institution/stores/organizational-unit/preservation-space-institution-organizational-unit.state";
import {StateEnum} from "@shared/enums/state.enum";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultAssociationStateInitValue,
  defaultResourceFileStateInitValue,
  DispatchMethodEnum,
  DownloadService,
  isFalse,
  isNotNullNorUndefined,
  isNullOrUndefined,
  isUndefined,
  NotificationService,
  OverrideDefaultAction,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";
import {
  PreservationSpaceInstitutionAction,
  preservationSpaceInstitutionActionNameSpace,
} from "./preservation-space-institution.action";

export interface PreservationSpaceInstitutionStateModel extends ResourceFileStateModel<Institution> {
  [StateEnum.preservationSpace_institution_organizationalUnit]: PreservationSpaceInstitutionOrganizationalUnitStateModel;
  [StateEnum.preservationSpace_institution_personRole]: PreservationSpaceInstitutionPersonRoleStateModel;
  currentUserIsManager: boolean;
  currentUserIsMember: boolean;
}

@Injectable()
@State<PreservationSpaceInstitutionStateModel>({
  name: StateEnum.preservationSpace_institution,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    currentUserIsManager: undefined,
    currentUserIsMember: undefined,
    [StateEnum.preservationSpace_institution_organizationalUnit]: {...defaultAssociationStateInitValue()},
    [StateEnum.preservationSpace_institution_personRole]: {...defaultPreservationSpaceInstitutionPersonRoleStateModel()},
  },
  children: [
    PreservationSpaceInstitutionPersonRoleState,
    PreservationSpaceInstitutionOrganizationalUnitState,
  ],
})
export class PreservationSpaceInstitutionState extends ResourceFileState<PreservationSpaceInstitutionStateModel, Institution> {

  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService,
              private readonly _securityService: SecurityService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: preservationSpaceInstitutionActionNameSpace,
      updateSubResourceDispatchMethod: DispatchMethodEnum.SEQUENCIAL,
    }, _downloadService, ResourceFileStateModeEnum.logo, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminInstitutions;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static currentUserIsManager(state: PreservationSpaceInstitutionStateModel): boolean | undefined {
    return state.currentUserIsManager;
  }

  @Selector()
  static currentUserIsMember(state: PreservationSpaceInstitutionStateModel): boolean | undefined {
    return state.currentUserIsMember;
  }

  @Selector()
  static currentTitle(state: PreservationSpaceInstitutionStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.name;
  }

  @Selector()
  static isLoading(state: PreservationSpaceInstitutionStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: PreservationSpaceInstitutionStateModel): boolean {
    return this.isLoading(state)
      || isUndefined(this.currentUserIsManager(state))
      || isUndefined(this.currentUserIsMember(state))
      || StoreUtil.isLoadingState(state[StateEnum.preservationSpace_institution_organizationalUnit])
      || StoreUtil.isLoadingState(state[StateEnum.preservationSpace_institution_personRole]);

  }

  @Selector()
  static isReadyToBeDisplayed(state: PreservationSpaceInstitutionStateModel): boolean {
    return isNotNullNorUndefined(state.current)
      && !isUndefined(this.currentUserIsManager(state))
      && !isUndefined(this.currentUserIsMember(state))
      && isNotNullNorUndefined(state[StateEnum.preservationSpace_institution_organizationalUnit].selected)
      && (isFalse(this.currentUserIsMember(state)) || !isNullOrUndefined(state.preservationSpace_institution_personRole.selected));
  }

  @OverrideDefaultAction()
  @Action(PreservationSpaceInstitutionAction.GetByIdSuccess)
  getByIdSuccess(ctx: SolidifyStateContext<PreservationSpaceInstitutionStateModel>, action: PreservationSpaceInstitutionAction.GetByIdSuccess): void {
    ctx.patchState({
      current: action.model,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });

    ctx.dispatch(new PreservationSpaceInstitutionAction.ComputeCurrentUserIsManager());
    ctx.dispatch(new PreservationSpaceInstitutionAction.ComputeCurrentUserIsMember());

    if (isNotNullNorUndefined(action.model.logo)) {
      ctx.dispatch(new PreservationSpaceInstitutionAction.GetFile(action.model.resId));
    }
  }

  @Action(PreservationSpaceInstitutionAction.ComputeCurrentUserIsManager)
  computeCurrentUserIsManager(ctx: SolidifyStateContext<PreservationSpaceInstitutionStateModel>, action: PreservationSpaceInstitutionAction.ComputeCurrentUserIsManager): void | boolean {
    const isManager = this._securityService.isManagerOfOrgUnit(ctx.getState().current.resId);
    ctx.patchState({
      currentUserIsManager: isManager,
    });
  }

  @Action(PreservationSpaceInstitutionAction.ComputeCurrentUserIsMember)
  computeCurrentUserIsMember(ctx: SolidifyStateContext<PreservationSpaceInstitutionStateModel>, action: PreservationSpaceInstitutionAction.ComputeCurrentUserIsManager): void | boolean {
    const institutionId = ctx.getState().current.resId;
    const isMember = this._securityService.isRootOrAdmin()
      || this._securityService.isMemberWithOrWithoutRoleOnInstitution(institutionId);
    ctx.patchState({
      currentUserIsMember: isMember,
    });
  }

  @Action(PreservationSpaceInstitutionAction.LoadOnlyMyInstitutions)
  loadOnlyMyInstitutions(ctx: SolidifyStateContext<PreservationSpaceInstitutionStateModel>, action: PreservationSpaceInstitutionAction.LoadOnlyMyInstitutions): Observable<CollectionTyped<Institution>> {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: StoreUtil.getQueryParametersToApply(action.queryParameters, ctx),
    });

    return this._apiService.getCollection<Institution>(ApiEnum.adminAuthorizedInstitutions, ctx.getState().queryParameters)
      .pipe(
        tap((collection: CollectionTyped<Institution>) => {
          ctx.dispatch(new PreservationSpaceInstitutionAction.LoadOnlyMyInstitutionsSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationSpaceInstitutionAction.LoadOnlyMyInstitutionsFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @Action(PreservationSpaceInstitutionAction.LoadOnlyMyInstitutionsSuccess)
  loadOnlyMyInstitutionsSuccess(ctx: SolidifyStateContext<PreservationSpaceInstitutionStateModel>, action: PreservationSpaceInstitutionAction.LoadOnlyMyInstitutionsSuccess): void {
    const queryParameters = StoreUtil.updateQueryParameters(ctx, action.list);

    ctx.patchState({
      list: action.list._data,
      total: action.list._page.totalItems,
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
      queryParameters,
    });
  }

  @Action(PreservationSpaceInstitutionAction.LoadOnlyMyInstitutionsFail)
  loadOnlyMyInstitutionsFail(ctx: SolidifyStateContext<PreservationSpaceInstitutionStateModel>, action: PreservationSpaceInstitutionAction.LoadOnlyMyInstitutionsFail): void {
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter - 1,
    });
  }
}
