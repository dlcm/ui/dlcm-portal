/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-institution-organizational-unit.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {environment} from "@environments/environment";
import {OrganizationalUnit} from "@models";
import {
  Actions,
  State,
  Store,
} from "@ngxs/store";
import {preservationSpaceInstitutionOrganizationalUnitNamespace} from "@preservation-space/institution/stores/organizational-unit/preservation-space-institution-organizational-unit.action";
import {ApiResourceNameEnum} from "@shared/enums/api-resource-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {
  ApiService,
  AssociationState,
  AssociationStateModel,
  defaultAssociationStateInitValue,
  NotificationService,
} from "solidify-frontend";

export interface PreservationSpaceInstitutionOrganizationalUnitStateModel extends AssociationStateModel<OrganizationalUnit> {
}

@Injectable()
@State<PreservationSpaceInstitutionOrganizationalUnitStateModel>({
  name: StateEnum.preservationSpace_institution_organizationalUnit,
  defaults: {
    ...defaultAssociationStateInitValue(),
  },
})
export class PreservationSpaceInstitutionOrganizationalUnitState extends AssociationState<PreservationSpaceInstitutionOrganizationalUnitStateModel, OrganizationalUnit> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions) {
    super(_apiService, _store, _notificationService, _actions$, environment, {
      nameSpace: preservationSpaceInstitutionOrganizationalUnitNamespace,
      resourceName: ApiResourceNameEnum.ORG_UNIT,
    });
  }

  protected get _urlResource(): string {
    return ApiEnum.adminInstitutions;
  }
}
