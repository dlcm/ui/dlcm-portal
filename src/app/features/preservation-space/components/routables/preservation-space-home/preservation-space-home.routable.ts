/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-home.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Input,
} from "@angular/core";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {
  PreservationSpaceRoutesEnum,
  RoutesEnum,
} from "@app/shared/enums/routes.enum";
import {LocalStateModel} from "@app/shared/models/local-state.model";
import {AppNotificationInboxState} from "@app/stores/notification-inbox/app-notification-inbox.state";
import {Enums} from "@enums";
import {Navigate} from "@ngxs/router-plugin";
import {Store} from "@ngxs/store";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {Observable} from "rxjs";
import {
  MemoizedUtil,
  Tab,
} from "solidify-frontend";

@Component({
  selector: "dlcm-preservation-space-home-routable",
  templateUrl: "./preservation-space-home.routable.html",
  styleUrls: ["./preservation-space-home.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceHomeRoutable extends SharedAbstractPresentational {
  isLoadingObs: Observable<boolean>;

  @Input()
  isLoading: boolean;

  userRolesObs: Enums.UserApplicationRole.UserApplicationRoleEnum[];

  private _currentTab: Tab;

  private get _rootUrl(): string[] {
    return [RoutesEnum.preservationSpace];
  }

  preservationSpaceTabs: Tab[] = [
    {
      id: "ORGANIZATIONAL_UNIT",
      icon: IconNameEnum.organizationalUnit,
      titleToTranslate: LabelTranslateEnum.organizationalUnits,
      suffixUrl: PreservationSpaceRoutesEnum.organizationalUnit,
      route: () => [...this._rootUrl, PreservationSpaceRoutesEnum.organizationalUnit],
      dataTest: DataTestEnum.preservationSpaceTabOrgUnit,
    },
    {
      id: "INSTITUTION",
      icon: IconNameEnum.institutions,
      titleToTranslate: LabelTranslateEnum.institutions,
      suffixUrl: PreservationSpaceRoutesEnum.institution,
      route: () => [...this._rootUrl, PreservationSpaceRoutesEnum.institution],
      dataTest: DataTestEnum.preservationSpaceTabInstitution,
    },
    {
      id: "CONTRIBUTOR",
      icon: IconNameEnum.contributor,
      titleToTranslate: LabelTranslateEnum.contributors,
      suffixUrl: PreservationSpaceRoutesEnum.contributor,
      route: () => [...this._rootUrl, PreservationSpaceRoutesEnum.contributor],
      dataTest: DataTestEnum.preservationSpaceTabContributor,
    },
    {
      id: "NOTIFICATION_RECEIVED",
      icon: IconNameEnum.requestInbox,
      titleToTranslate: LabelTranslateEnum.notifications,
      suffixUrl: PreservationSpaceRoutesEnum.notificationInbox,
      route: () => [...this._rootUrl, PreservationSpaceRoutesEnum.notificationInbox],
      numberNew: () => MemoizedUtil.total(this._store, AppNotificationInboxState),
      dataTest: DataTestEnum.preservationSpaceTabRequestReceived,
    },
    {
      id: "NOTIFICATION_SENT",
      icon: IconNameEnum.requestSent,
      titleToTranslate: LabelTranslateEnum.sentRequests,
      suffixUrl: PreservationSpaceRoutesEnum.notificationSent,
      route: () => [...this._rootUrl, PreservationSpaceRoutesEnum.notificationSent],
      dataTest: DataTestEnum.preservationSpaceTabRequestSent,
    },
  ];

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  constructor(private readonly _store: Store) {
    super();
    this.userRolesObs = this._store.selectSnapshot((state: LocalStateModel) => state.application.userRoles);
  }

  navigate(path: RoutesEnum): void {
    this._store.dispatch(new Navigate([path]));
  }

  setCurrentTab($event: Tab): void {
    this._currentTab = $event;
  }

}
