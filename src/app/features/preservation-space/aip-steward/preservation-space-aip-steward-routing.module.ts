/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-aip-steward-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {PreservationSpaceAipStewardNotFoundRoutable} from "@preservation-space/aip-steward/components/routables/aip-steward-not-found/preservation-space-aip-steward-not-found.routable";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  AppRoutesEnum,
  PreservationSpaceAipStewardRoutesEnum,
} from "@shared/enums/routes.enum";
import {SharedAipCollectionDetailRoutable} from "@shared/features/aip/components/routables/aip-collection-detail/shared-aip-collection-detail.routable";
import {SharedAipCollectionRoutable} from "@shared/features/aip/components/routables/aip-collection/shared-aip-collection.routable";
import {SharedAipDetailEditRoutable} from "@shared/features/aip/components/routables/aip-detail-edit/shared-aip-detail-edit.routable";
import {SharedAipFileDetailRoutable} from "@shared/features/aip/components/routables/aip-file-detail/shared-aip-file-detail.routable";
import {SharedAipFileRoutable} from "@shared/features/aip/components/routables/aip-file/shared-aip-file.routable";
import {SharedAipMetadataRoutable} from "@shared/features/aip/components/routables/aip-metadata/shared-aip-metadata.routable";
import {SharedAipSharedModule} from "@shared/features/aip/shared-aip-shared.module";
import {SharedAipState} from "@shared/features/aip/stores/shared-aip.state";
import {DlcmRoutes} from "@shared/models/dlcm-route.model";

const routes: DlcmRoutes = [
  {
    path: PreservationSpaceAipStewardRoutesEnum.aipStewardNotFound + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: PreservationSpaceAipStewardNotFoundRoutable,
  },
  {
    path: PreservationSpaceAipStewardRoutesEnum.aipStewardDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    redirectTo: PreservationSpaceAipStewardRoutesEnum.aipStewardDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId + AppRoutesEnum.separator + PreservationSpaceAipStewardRoutesEnum.aipStewardMetadata,
    pathMatch: "full",
  },
  {
    path: PreservationSpaceAipStewardRoutesEnum.aipStewardDetail + AppRoutesEnum.separator + AppRoutesEnum.paramId,
    component: SharedAipDetailEditRoutable,
    data: {
      breadcrumbMemoizedSelector: SharedAipState.currentAipName,
      noBreadcrumbLink: true,
    },
    children: [
      {
        path: PreservationSpaceAipStewardRoutesEnum.aipStewardMetadata,
        component: SharedAipMetadataRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.metadata,
          noBreadcrumbLink: true,
        },
      },
      {
        path: PreservationSpaceAipStewardRoutesEnum.aipStewardCollection,
        component: SharedAipCollectionRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.collection,
          noBreadcrumbLink: true,
        },
        children: [
          {
            path: AppRoutesEnum.paramId,
            component: SharedAipCollectionDetailRoutable,
            data: {
              breadcrumb: LabelTranslateEnum.detail,
            },
          },
        ],
      },
      {
        path: PreservationSpaceAipStewardRoutesEnum.aipStewardFiles,
        component: SharedAipFileRoutable,
        data: {
          breadcrumb: LabelTranslateEnum.files,
          noBreadcrumbLink: true,
        },
        children: [
          {
            path: AppRoutesEnum.paramId,
            component: SharedAipFileDetailRoutable,
            data: {
              breadcrumb: LabelTranslateEnum.detail,
            },
          },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [SharedAipSharedModule.forChild(routes)],
  exports: [SharedAipSharedModule],
})
export class PreservationSpaceAipStewardRoutingModule {
}
