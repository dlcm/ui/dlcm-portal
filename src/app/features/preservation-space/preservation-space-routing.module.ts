/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {PreservationSpaceHomeRoutable} from "@app/features/preservation-space/components/routables/preservation-space-home/preservation-space-home.routable";
import {
  AppRoutesEnum,
  PreservationSpaceRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {DlcmRoutes} from "@app/shared/models/dlcm-route.model";
import {ApplicationRolePermissionEnum} from "@shared/enums/application-role-permission.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {ApplicationRoleGuardService} from "@shared/guards/application-role-guard.service";
import {CombinedGuardService} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.root,
    redirectTo: PreservationSpaceRoutesEnum.organizationalUnit,
    pathMatch: "full",
  },
  {
    path: AppRoutesEnum.root,
    component: PreservationSpaceHomeRoutable,
    data: {},
    children: [
      {
        path: PreservationSpaceRoutesEnum.organizationalUnit,
        // @ts-ignore Dynamic import
        loadChildren: () => import("./organizational-unit/preservation-space-organizational-unit.module").then(m => m.PreservationSpaceOrganizationalUnitModule),
        data: {
          breadcrumb: LabelTranslateEnum.organizationalUnits,
          permission: ApplicationRolePermissionEnum.userPermission,
          guards: [ApplicationRoleGuardService],
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: PreservationSpaceRoutesEnum.institution,
        // @ts-ignore Dynamic import
        loadChildren: () => import("./institution/preservation-space-institution.module").then(m => m.PreservationSpaceInstitutionModule),
        data: {
          breadcrumb: LabelTranslateEnum.institutions,
          permission: ApplicationRolePermissionEnum.userPermission,
          guards: [ApplicationRoleGuardService],
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: PreservationSpaceRoutesEnum.contributor,
        // @ts-ignore Dynamic import
        loadChildren: () => import("./contributor/preservation-space-contributor.module").then(m => m.PreservationSpaceContributorModule),
        data: {
          breadcrumb: LabelTranslateEnum.contributors,
          guards: [ApplicationRoleGuardService],
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: PreservationSpaceRoutesEnum.notificationInbox,
        // @ts-ignore Dynamic import
        loadChildren: () => import("./notification/preservation-space-notification.module").then(m => m.PreservationSpaceNotificationModule),
        data: {
          breadcrumb: LabelTranslateEnum.notifications,
          guards: [ApplicationRoleGuardService],
        },
        canActivate: [CombinedGuardService],
      },
      {
        path: PreservationSpaceRoutesEnum.notificationSent,
        // @ts-ignore Dynamic import
        loadChildren: () => import("./notification/preservation-space-notification.module").then(m => m.PreservationSpaceNotificationModule),
        data: {
          breadcrumb: LabelTranslateEnum.sentRequests,
          guards: [ApplicationRoleGuardService],
        },
        canActivate: [CombinedGuardService],
      },
    ],
  },
  {
    path: PreservationSpaceRoutesEnum.aipSteward,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./aip-steward/preservation-space-aip-steward.module").then(m => m.PreservationSpaceAipStewardModule),
    data: {
      breadcrumb: LabelTranslateEnum.aip,
      noBreadcrumbLink: true,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreservationSpaceRoutingModule {
}
