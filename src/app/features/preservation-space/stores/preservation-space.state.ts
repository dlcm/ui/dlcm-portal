/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {
  PreservationSpaceNotificationState,
  PreservationSpaceNotificationStateModel,
} from "@app/features/preservation-space/notification/stores/preservation-space-notification.state";
import {
  State,
  Store,
} from "@ngxs/store";
import {
  PreservationSpaceContributorState,
  PreservationSpaceContributorStateModel,
} from "@preservation-space/contributor/stores/preservation-space-contributor.state";
import {
  PreservationSpaceInstitutionState,
  PreservationSpaceInstitutionStateModel,
} from "@preservation-space/institution/stores/preservation-space-institution.state";
import {
  PreservationSpaceOrganizationalUnitState,
  PreservationSpaceOrganizationalUnitStateModel,
} from "@preservation-space/organizational-unit/stores/preservation-space-organizational-unit.state";
import {StateEnum} from "@shared/enums/state.enum";
import {
  BaseStateModel,
  defaultResourceStateInitValue,
} from "solidify-frontend";

export interface PreservationSpaceStateModel extends BaseStateModel {
  preservationSpace_contributor: PreservationSpaceContributorStateModel;
  preservationSpace_notification: PreservationSpaceNotificationStateModel;
  preservationSpace_organizationalUnit: PreservationSpaceOrganizationalUnitStateModel;
  preservationSpace_institution: PreservationSpaceInstitutionStateModel;
}

@Injectable()
@State<PreservationSpaceStateModel>({
  name: StateEnum.preservationSpace,
  defaults: {
    ...defaultResourceStateInitValue(),
    preservationSpace_contributor: null,
    preservationSpace_notification: null,
    preservationSpace_organizationalUnit: null,
    preservationSpace_institution: null,
  },
  children: [
    PreservationSpaceContributorState,
    PreservationSpaceNotificationState,
    PreservationSpaceOrganizationalUnitState,
    PreservationSpaceInstitutionState,
  ],
})
export class PreservationSpaceState {
  constructor(protected readonly _store: Store) {
  }
}
