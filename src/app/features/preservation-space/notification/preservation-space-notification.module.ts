/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-notification.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {PreservationSpaceNotificationListRoutable} from "@app/features/preservation-space/notification/components/routables/preservation-space-notification-list/preservation-space-notification-list.routable";
import {PreservationSpaceNotificationStatusHistoryState} from "@app/features/preservation-space/notification/stores/status-history/preservation-space-notification-status-history.state";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {PreservationSpaceNotificationRoutingModule} from "@preservation-space/notification/preservation-space-notification-routing.module";
import {SharedModule} from "@shared/shared.module";
import {PreservationSpaceNotificationDetailEditRoutable} from "./components/routables/preservation-space-notification-detail-edit/preservation-space-notification-detail-edit.routable";
import {PreservationSpaceNotificationState} from "./stores/preservation-space-notification.state";

const routables = [
  PreservationSpaceNotificationListRoutable,
  PreservationSpaceNotificationDetailEditRoutable,
];
const containers = [];
const dialogs = [];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    PreservationSpaceNotificationRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      PreservationSpaceNotificationState,
      PreservationSpaceNotificationStatusHistoryState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationSpaceNotificationModule {
}
