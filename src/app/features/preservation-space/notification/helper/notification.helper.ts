/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - notification.helper.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {ActivatedRoute} from "@angular/router";
import {NotificationModeEnum} from "@app/features/preservation-space/notification/enums/notification-mode.enum";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {NotificationDlcm} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {PreservationSpaceOrganizationalUnitArchiveAclAction} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.action";
import {
  DepositRoutesEnum,
  PreservationSpaceOrganizationalUnitRoutesEnum,
  PreservationSpaceRoutesEnum,
  RoutesEnum,
  SharedAipRoutesEnum,
} from "@shared/enums/routes.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {SharedNotificationAction} from "@shared/stores/notification/shared-notification.action";
import {
  isNotNullNorUndefined,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  SolidifyObject,
} from "solidify-frontend";

export class NotificationHelper {
  static readonly KEY_ORGUNIT_ID: keyof NotificationDlcm = "notifiedOrgUnit";
  static readonly KEY_ROLE_ID: string = "roleId";
  static readonly KEY_PERSON_ID: string = "personId";
  static readonly KEY_ORGUNIT_NAME: string = "orgUnitName";
  static readonly KEY_ARCHIVE_ID: string = "archiveId";
  static readonly KEY_USER_ID: string = "userId";

  static getUrlQueryParam(orgUnitId: string): SolidifyObject {
    if (isNotNullNorUndefined(orgUnitId)) {
      return {
        [this.KEY_ORGUNIT_ID]: orgUnitId,
      };
    }
    return {};
  }

  static getMode(route: ActivatedRoute): NotificationModeEnum {
    const url = route.snapshot.parent.url.toString();
    let mode = NotificationModeEnum.inbox;
    if (url === PreservationSpaceRoutesEnum.notificationSent) {
      mode = NotificationModeEnum.sent;
    }
    return mode;
  }

  static process(notification: NotificationDlcm, store: Store, actions: Actions, notificationService: NotificationService): void {
    switch (notification.notificationType?.resId) {
      case Enums.Notification.TypeEnum.JOIN_ORGUNIT_REQUEST:
        this._processJoinOrgUnitRequest(notification, store);
        break;
      case Enums.Notification.TypeEnum.COMPLETED_ARCHIVE_INFO:
      case Enums.Notification.TypeEnum.MY_COMPLETED_ARCHIVE_INFO:
        this._processArchiveRequest(notification, store);
        break;
      case Enums.Notification.TypeEnum.VALIDATE_DEPOSIT_REQUEST:
      case Enums.Notification.TypeEnum.IN_ERROR_DEPOSIT_INFO:
      case Enums.Notification.TypeEnum.CREATED_DEPOSIT_INFO:
      case Enums.Notification.TypeEnum.MY_APPROVED_DEPOSIT_INFO:
      case Enums.Notification.TypeEnum.MY_COMPLETED_DEPOSIT_INFO:
      case Enums.Notification.TypeEnum.MY_INDIRECT_COMPLETED_DEPOSIT_INFO:
        this._processDepositRequest(notification, store);
        break;
      case Enums.Notification.TypeEnum.CREATE_ORGUNIT_REQUEST:
        this._processCreateOrgUnitRequest(notification, store);
        break;
      case Enums.Notification.TypeEnum.IN_ERROR_AIP_INFO:
        this._processAipInError(notification, store);
        break;
      case Enums.Notification.TypeEnum.IN_ERROR_DOWNLOADED_AIP_INFO:
        this._processAipDownloadedInError(notification, store);
        break;
      case Enums.Notification.TypeEnum.IN_ERROR_DIP_INFO:
        this._processDipInError(notification, store);
        break;
      case Enums.Notification.TypeEnum.IN_ERROR_SIP_INFO:
        this._processSipInError(notification, store);
        break;
      case Enums.Notification.TypeEnum.ACCESS_DATASET_REQUEST:
        this._processAccessDataSetRequest(notification, store, actions);
        break;
      case Enums.Notification.TypeEnum.APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST:
        this._processApproveDisposalRequestByOrgUnit(notification, store);
        break;
      case Enums.Notification.TypeEnum.APPROVE_DISPOSAL_REQUEST:
        this._processApproveDisposalRequest(notification, store);
        break;
      default:
        notificationService.showInformation(MARK_AS_TRANSLATABLE("preservationSpace.notifications.notification.processActionNotAvailableForThisType"), {type: notification.notificationType?.resId});
        break;
    }
  }

  private static _processJoinOrgUnitRequest(notification: NotificationDlcm, store: Store): void {
    store.dispatch(this._getNavigateToOrgUnitToAddMember(store, notification.notifiedOrgUnit.resId, notification.emitter.person.resId, notification.objectId /* contain roleId */));
  }

  private static _processDepositRequest(notification: NotificationDlcm, store: Store): void {
    store.dispatch(new Navigate([RoutesEnum.deposit, notification.notifiedOrgUnit.resId, DepositRoutesEnum.detail, notification.objectId])); // contain deposit id
  }

  private static _processArchiveRequest(notification: NotificationDlcm, store: Store): void {
    store.dispatch(new Navigate([RoutesEnum.archives, notification.objectId])); // contain archive id
  }

  private static _processAipInError(notification: NotificationDlcm, store: Store): void {
    store.dispatch(new Navigate([RoutesEnum.preservationPlanningAip, environment.defaultStorageIndex + 1, SharedAipRoutesEnum.aipDetail, notification.objectId])); // contain aip id
  }

  private static _processAipDownloadedInError(notification: NotificationDlcm, store: Store): void {
    store.dispatch(new Navigate([RoutesEnum.preservationPlanningAipDownloadedDetail, notification.objectId])); // contain aip downloaded id
  }

  private static _processSipInError(notification: NotificationDlcm, store: Store): void {
    store.dispatch(new Navigate([RoutesEnum.preservationPlanningSipDetail, notification.objectId])); // contain object id
  }

  private static _processDipInError(notification: NotificationDlcm, store: Store): void {
    store.dispatch(new Navigate([RoutesEnum.preservationPlanningDipDetail, notification.objectId])); // contain dip id
  }

  private static _processCreateOrgUnitRequest(notification: NotificationDlcm, store: Store): void {
    store.dispatch(new Navigate([RoutesEnum.adminOrganizationalUnitCreate], {
      [NotificationHelper.KEY_ROLE_ID]: Enums.Role.RoleEnum.MANAGER,
      [NotificationHelper.KEY_PERSON_ID]: notification.emitter.person.resId,
      [NotificationHelper.KEY_ORGUNIT_NAME]: notification.objectId, // contain org unit name
    }, {
      skipLocationChange: false,
    }));
  }

  private static _processAccessDataSetRequest(notification: NotificationDlcm, store: Store, actions: Actions): void {
    store.dispatch([
      new PreservationSpaceOrganizationalUnitArchiveAclAction.CreateFromNotification(notification),
      new SharedNotificationAction.SetApproved(notification.resId, notification.notificationType.notificationCategory, ViewModeEnum.list),
    ]);
  }

  private static _processApproveDisposalRequestByOrgUnit(notification: NotificationDlcm, store: Store): void {
    store.dispatch(new Navigate([RoutesEnum.preservationSpaceAipStewardDetail, notification.objectId]));
  }

  private static _processApproveDisposalRequest(notification: NotificationDlcm, store: Store): void {
    store.dispatch(new Navigate([RoutesEnum.preservationPlanningAip, 1, SharedAipRoutesEnum.aipDetail, notification.objectId]));
  }

  private static _getNavigateToAclToAddAcl(store: Store, orgUnitId: string, userId: string, archiveId: string): Navigate {
    return new Navigate([RoutesEnum.preservationSpaceOrganizationalUnitDetail, orgUnitId, PreservationSpaceOrganizationalUnitRoutesEnum.manageAcl, PreservationSpaceOrganizationalUnitRoutesEnum.manageAclCreate], {
      [NotificationHelper.KEY_USER_ID]: userId,
      [NotificationHelper.KEY_ARCHIVE_ID]: archiveId,
    });
  }

  private static _getNavigateToOrgUnitToAddMember(store: Store, orgUnitId: string, personId: string, roleId: string): Navigate {
    return new Navigate([RoutesEnum.preservationSpaceOrganizationalUnitDetail, orgUnitId, PreservationSpaceOrganizationalUnitRoutesEnum.edit], {
      [NotificationHelper.KEY_ROLE_ID]: roleId,
      [NotificationHelper.KEY_PERSON_ID]: personId,
    });
  }
}
