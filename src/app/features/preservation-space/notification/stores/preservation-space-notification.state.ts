/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-notification.state.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {Injectable} from "@angular/core";
import {NotificationModeEnum} from "@app/features/preservation-space/notification/enums/notification-mode.enum";
import {
  PreservationSpaceNotificationAction,
  preservationSpaceNotificationActionNameSpace,
} from "@app/features/preservation-space/notification/stores/preservation-space-notification.action";
import {
  PreservationSpaceNotificationStatusHistoryState,
  PreservationSpaceNotificationStatusHistoryStateModel,
} from "@app/features/preservation-space/notification/stores/status-history/preservation-space-notification-status-history.state";
import {environment} from "@environments/environment";
import {NotificationDlcm} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Action,
  Actions,
  Selector,
  State,
  Store,
} from "@ngxs/store";
import {ApiActionNameEnum} from "@shared/enums/api-action-name.enum";
import {ApiEnum} from "@shared/enums/api.enum";
import {
  RoutesEnum,
  urlSeparator,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {defaultStatusHistoryInitValue} from "@shared/stores/status-history/status-history.state";
import {
  Observable,
  pipe,
} from "rxjs";
import {
  catchError,
  tap,
} from "rxjs/operators";
import {
  ApiService,
  CollectionTyped,
  defaultResourceFileStateInitValue,
  DownloadService,
  isNullOrUndefined,
  MappingObjectUtil,
  MARK_AS_TRANSLATABLE,
  NotificationService,
  OverrideDefaultAction,
  QueryParametersUtil,
  ResourceFileState,
  ResourceFileStateModeEnum,
  ResourceFileStateModel,
  SolidifyHttpErrorResponseModel,
  SolidifyStateContext,
  SolidifyStateError,
  StoreUtil,
} from "solidify-frontend";

const ATTRIBUTE_SIGNED_DUA_FILE: keyof NotificationDlcm = "signedDuaFile";

export interface PreservationSpaceNotificationStateModel extends ResourceFileStateModel<NotificationDlcm> {
  mode: NotificationModeEnum;
  orgUnitId: string | undefined;
  preservationSpace_notification_statusHistory: PreservationSpaceNotificationStatusHistoryStateModel;
}

@Injectable()
@State<PreservationSpaceNotificationStateModel>({
  name: StateEnum.preservationSpace_notification,
  defaults: {
    ...defaultResourceFileStateInitValue(),
    mode: NotificationModeEnum.inbox,
    orgUnitId: undefined,
    preservationSpace_notification_statusHistory: {...defaultStatusHistoryInitValue()},
  },
  children: [
    PreservationSpaceNotificationStatusHistoryState,
  ],
})
export class PreservationSpaceNotificationState extends ResourceFileState<PreservationSpaceNotificationStateModel, NotificationDlcm> {
  constructor(protected readonly _apiService: ApiService,
              protected readonly _store: Store,
              protected readonly _notificationService: NotificationService,
              protected readonly _actions$: Actions,
              protected readonly _downloadService: DownloadService) {
    super(_apiService, _store, _notificationService, _actions$, {
      nameSpace: preservationSpaceNotificationActionNameSpace,
      routeRedirectUrlAfterSuccessCreateAction: (resId: string) => RoutesEnum.preservationSpaceNotificationInboxDetail + urlSeparator + resId,
      routeRedirectUrlAfterSuccessUpdateAction: (resId: string) => RoutesEnum.preservationSpaceNotificationInboxDetail + urlSeparator + resId,
      notificationResourceCreateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("preservationSpace.notifications.notification.resource.create"),
      notificationResourceDeleteSuccessTextToTranslate: MARK_AS_TRANSLATABLE("preservationSpace.notifications.notification.resource.delete"),
      notificationResourceUpdateSuccessTextToTranslate: MARK_AS_TRANSLATABLE("preservationSpace.notifications.notification.resource.update"),
      downloadInMemory: false,
      resourceFileApiActionNameUploadCustom: ApiActionNameEnum.UPLOAD_DUA,
      resourceFileApiActionNameDownloadCustom: ApiActionNameEnum.DOWNLOAD_DUA,
      resourceFileApiActionNameDeleteCustom: ApiActionNameEnum.DELETE_DUA,
      customFileAttribute: ATTRIBUTE_SIGNED_DUA_FILE,
    }, _downloadService, ResourceFileStateModeEnum.custom, environment);
  }

  protected get _urlResource(): string {
    return ApiEnum.adminNotifications;
  }

  protected get _urlFileResource(): string {
    return this._urlResource;
  }

  @Selector()
  static isLoading(state: PreservationSpaceNotificationStateModel): boolean {
    return StoreUtil.isLoadingState(state);
  }

  @Selector()
  static isLoadingWithDependency(state: PreservationSpaceNotificationStateModel): boolean {
    return this.isLoading(state);
  }

  @Selector()
  static currentTitle(state: PreservationSpaceNotificationStateModel): string | undefined {
    if (isNullOrUndefined(state.current)) {
      return undefined;
    }
    return state.current.emitter["person"].fullName;
  }

  @Selector()
  static isReadyToBeDisplayed(state: PreservationSpaceNotificationStateModel): boolean {
    return this.isReadyToBeDisplayedInCreateMode
      && !isNullOrUndefined(state.current);
  }

  @Selector()
  static isReadyToBeDisplayedInCreateMode(state: PreservationSpaceNotificationStateModel): boolean {
    return true;
  }

  @Action(PreservationSpaceNotificationAction.SetMode)
  setMode(ctx: SolidifyStateContext<PreservationSpaceNotificationStateModel>, action: PreservationSpaceNotificationAction.SetMode): void {
    ctx.patchState({
      mode: action.mode,
      orgUnitId: action.orgUnitId,
    });
  }

  @OverrideDefaultAction()
  @Action(PreservationSpaceNotificationAction.GetAll)
  getAll(ctx: SolidifyStateContext<PreservationSpaceNotificationStateModel>, action: PreservationSpaceNotificationAction.GetAll): Observable<CollectionTyped<NotificationDlcm>> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        list: undefined,
        total: 0,
      };
    }
    let queryParameters = StoreUtil.getQueryParametersToApply(action.queryParameters, ctx);
    const orgUnitId = ctx.getState().orgUnitId;
    if (!isNullOrUndefined(orgUnitId)) {
      queryParameters = QueryParametersUtil.clone(queryParameters);
      const search = QueryParametersUtil.getSearchItems(queryParameters);
      MappingObjectUtil.set(search, "notifiedOrgUnit.resId", orgUnitId);
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      queryParameters: queryParameters,
      ...reset,
    });
    let mode = ApiActionNameEnum.INBOX;
    if (ctx.getState().mode === NotificationModeEnum.sent) {
      mode = ApiActionNameEnum.SENT;
    }
    return this._apiService.getCollection<NotificationDlcm>(this._urlResource + urlSeparator + mode, ctx.getState().queryParameters)
      .pipe(
        action.cancelIncomplete ? StoreUtil.cancelUncompleted(action, ctx, this._actions$, [this._nameSpace.GetAll, Navigate]) : pipe(),
        tap((collection: CollectionTyped<NotificationDlcm>) => {
          ctx.dispatch(new PreservationSpaceNotificationAction.GetAllSuccess(action, collection));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationSpaceNotificationAction.GetAllFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(PreservationSpaceNotificationAction.GetById)
  getById(ctx: SolidifyStateContext<PreservationSpaceNotificationStateModel>, action: PreservationSpaceNotificationAction.GetById): Observable<NotificationDlcm> {
    let reset = {};
    if (!action.keepCurrentContext) {
      reset = {
        current: undefined,
      };
    }
    ctx.patchState({
      isLoadingCounter: ctx.getState().isLoadingCounter + 1,
      ...reset,
    });

    let mode = ApiActionNameEnum.INBOX;
    if (ctx.getState().mode === NotificationModeEnum.sent) {
      mode = ApiActionNameEnum.SENT;
    }
    return this._apiService.getById<NotificationDlcm>(this._urlResource + urlSeparator + mode, action.id)
      .pipe(
        tap((model: NotificationDlcm) => {
          ctx.dispatch(new PreservationSpaceNotificationAction.GetByIdSuccess(action, model));
        }),
        catchError((error: SolidifyHttpErrorResponseModel) => {
          ctx.dispatch(new PreservationSpaceNotificationAction.GetByIdFail(action));
          throw new SolidifyStateError(this, error);
        }),
      );
  }

  @OverrideDefaultAction()
  @Action(PreservationSpaceNotificationAction.Clean)
  clean(ctx: SolidifyStateContext<PreservationSpaceNotificationStateModel>, action: PreservationSpaceNotificationAction.Clean): void {
    const orgUnitId = ctx.getState().orgUnitId;
    const mode = ctx.getState().mode;
    super.clean(ctx, action);
    ctx.patchState({
      orgUnitId,
      mode,
    });
  }
}
