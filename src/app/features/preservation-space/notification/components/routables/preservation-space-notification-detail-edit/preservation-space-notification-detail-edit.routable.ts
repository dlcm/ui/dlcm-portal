/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-notification-detail-edit.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {NotificationModeEnum} from "@app/features/preservation-space/notification/enums/notification-mode.enum";
import {NotificationHelper} from "@app/features/preservation-space/notification/helper/notification.helper";
import {
  PreservationSpaceNotificationState,
  PreservationSpaceNotificationStateModel,
} from "@app/features/preservation-space/notification/stores/preservation-space-notification.state";
import {PreservationSpaceNotificationStatusHistoryAction} from "@app/features/preservation-space/notification/stores/status-history/preservation-space-notification-status-history.action";
import {PreservationSpaceNotificationStatusHistoryState} from "@app/features/preservation-space/notification/stores/status-history/preservation-space-notification-status-history.state";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {
  NotificationDlcm,
  Role,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {PreservationSpaceNotificationArchiveAclExternalDuaDialog} from "@preservation-space/notification/components/dialogs/preservation-space-notification-archive-acl-external-dua/preservation-space-notification-archive-acl-external-dua.dialog";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  PreservationSpaceRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {SharedArchiveAction} from "@shared/stores/archive/shared-archive.action";
import {
  SharedNotificationAction,
  sharedNotificationActionNameSpace,
} from "@shared/stores/notification/shared-notification.action";
import {SharedRoleState} from "@shared/stores/role/shared-role.state";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {
  AbstractDetailEditCommonRoutable,
  DialogUtil,
  ExtraButtonToolbar,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  QueryParameters,
  ResourceNameSpace,
  StatusHistory,
  StatusHistoryDialog,
  StoreUtil,
  StringUtil,
} from "solidify-frontend";
import {
  PreservationSpaceNotificationAction,
  preservationSpaceNotificationActionNameSpace,
} from "../../../stores/preservation-space-notification.action";
import {SharedNotificationHelper} from "@shared/helpers/shared-notification.helper";

@Component({
  selector: "dlcm-preservation-space-notification-detail-edit-routable",
  templateUrl: "./preservation-space-notification-detail-edit.routable.html",
  styleUrls: ["./preservation-space-notification-detail-edit.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceNotificationDetailEditRoutable extends AbstractDetailEditCommonRoutable<NotificationDlcm, PreservationSpaceNotificationStateModel> implements OnInit {
  @Select(PreservationSpaceNotificationState.isLoadingWithDependency) isLoadingWithDependencyObs: Observable<boolean>;
  @Select(PreservationSpaceNotificationState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  historyObs: Observable<StatusHistory[]> = MemoizedUtil.select(this._store, PreservationSpaceNotificationStatusHistoryState, state => state.history);
  isLoadingHistoryObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, PreservationSpaceNotificationStatusHistoryState);
  queryParametersHistoryObs: Observable<QueryParameters> = MemoizedUtil.select(this._store, PreservationSpaceNotificationStatusHistoryState, state => state.queryParameters);
  listRoleObs: Observable<Role[]> = MemoizedUtil.list(this._store, SharedRoleState);

  override checkAvailableResourceNameSpace: ResourceNameSpace = sharedNotificationActionNameSpace;

  override readonly editAvailable: boolean = false;
  override readonly deleteAvailable: boolean = false;

  mode: NotificationModeEnum;
  modeObs: Observable<NotificationModeEnum> = MemoizedUtil.select(this._store, PreservationSpaceNotificationState, state => state.mode);
  readonly KEY_PARAM_NAME: keyof NotificationDlcm & string = "notificationType";

  listExtraButtons: ExtraButtonToolbar<NotificationDlcm>[] = [
    {
      color: "primary",
      icon: IconNameEnum.navigate,
      displayCondition: current => this.mode === NotificationModeEnum.inbox && current?.notificationStatus === Enums.Notification.StatusEnum.PENDING,
      callback: (current) => this._process(current),
      labelToTranslate: (current) => LabelTranslateEnum.process,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.navigate,
      displayCondition: current => this.mode === NotificationModeEnum.inbox && current?.notificationType.notificationCategory === Enums.Notification.CategoryEnum.INFO,
      callback: (current) => this._process(current),
      labelToTranslate: (current) => LabelTranslateEnum.showObject,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.unapprove,
      displayCondition: current => this.mode === NotificationModeEnum.inbox && current?.notificationStatus === Enums.Notification.StatusEnum.PENDING
        && current?.notificationType.notificationCategory === Enums.Notification.CategoryEnum.REQUEST,
      callback: (current) => this._setRefuse(current),
      labelToTranslate: (current) => LabelTranslateEnum.markAsRefused,
      order: 40,
    },
    {
      color: "primary",
      icon: IconNameEnum.markAsUnread,
      displayCondition: current => this.mode === NotificationModeEnum.inbox && current?.notificationMark === Enums.Notification.MarkEnum.READ,
      callback: (current) => this._setUnread(current),
      labelToTranslate: (current) => LabelTranslateEnum.markAsUnread,
      order: 40,
    },
  ];

  constructor(protected readonly _store: Store,
              protected readonly _route: ActivatedRoute,
              protected readonly _actions$: Actions,
              protected readonly _changeDetector: ChangeDetectorRef,
              private readonly _notificationService: NotificationService,
              public readonly _dialog: MatDialog,
              protected readonly _injector: Injector) {
    super(_store, _route, _actions$, _changeDetector, _dialog, StateEnum.preservationSpace_notification, _injector, preservationSpaceNotificationActionNameSpace, StateEnum.preservationSpace);

    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(SharedNotificationAction.RefreshDetailEvent),
      tap(result => {
        this._store.dispatch(new PreservationSpaceNotificationAction.GetById(result.action.notificationId));
      }),
    ));
  }

  ngOnInit(): void {
    this._retrieveModeFromUrl();
    this.subscribe(this.modeObs, mode => this.mode = mode);
    super.ngOnInit();
  }

  _getSubResourceWithParentId(id: string): void {
  }

  private _retrieveModeFromUrl(): void {
    this._store.dispatch(new PreservationSpaceNotificationAction.SetMode(NotificationHelper.getMode(this._route), undefined));
  }

  override backToListNavigate(): Navigate {
    const url = this._route.snapshot.parent.url.toString();
    this.mode = NotificationModeEnum.inbox;
    let route = RoutesEnum.preservationSpaceNotificationInbox;
    if (url === PreservationSpaceRoutesEnum.notificationSent) {
      route = RoutesEnum.preservationSpaceNotificationSent;
      this.mode = NotificationModeEnum.sent;
    }
    const orgUnitId = MemoizedUtil.selectSnapshot(this._store, PreservationSpaceNotificationState, state => state.orgUnitId);
    return new Navigate([route], NotificationHelper.getUrlQueryParam(orgUnitId));
  }

  private _setRefuse(notification: NotificationDlcm): void {
    this.subscribe(SharedNotificationHelper.getRefuseDialog(this._dialog, this._store, ViewModeEnum.detail, notification));
  }

  private _setUnread(notification: NotificationDlcm): void {
    this._store.dispatch(new SharedNotificationAction.SetUnread(notification.resId, ViewModeEnum.detail));
    this.backToList();
  }

  private _process(notification: NotificationDlcm): void {
    if (notification.notificationType.resId !== Enums.Notification.TypeEnum.ACCESS_DATASET_REQUEST) {
      this._processNotification(notification);
      return;
    }
    // Check if archive has an external dua
    this.subscribe(StoreUtil.dispatchActionAndWaitForSubActionCompletion(this._store, this._actions$,
      new SharedArchiveAction.GetById(notification.objectId),
      SharedArchiveAction.GetByIdSuccess,
      result => {
        if (result.model.dataUsePolicy === Enums.Deposit.DataUsePolicyEnum.EXTERNAL_DUA) {
          this.subscribe(DialogUtil.open(this._dialog, PreservationSpaceNotificationArchiveAclExternalDuaDialog, {
              notification: notification,
            },
            {
              width: "max-content",
              maxWidth: "90vw",
              height: "min-content",
              takeOne: true,
            }, () => {
              this._processNotification(notification);
            }));
        } else {
          this._processNotification(notification);
        }
      }));
  }

  showHistory(): void {
    DialogUtil.open(this._dialog, StatusHistoryDialog, {
      parentId: null,
      resourceResId: this._resId,
      name: StringUtil.convertToPascalCase(this._state),
      statusHistory: this.historyObs,
      isLoading: this.isLoadingHistoryObs,
      queryParametersObs: this.queryParametersHistoryObs,
      state: PreservationSpaceNotificationStatusHistoryAction,
      statusEnums: Enums.Notification.StatusEnumTranslate,
    }, {
      width: environment.modalWidth,
    });
  }

  private _processNotification(notification: NotificationDlcm): void {
    NotificationHelper.process(notification, this._store, this._actions$, this._notificationService);
  }
}
