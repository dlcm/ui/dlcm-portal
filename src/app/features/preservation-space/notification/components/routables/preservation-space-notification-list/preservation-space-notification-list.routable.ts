/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space-notification-list.routable.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  OnInit,
} from "@angular/core";
import {MatDialog} from "@angular/material/dialog";
import {ActivatedRoute} from "@angular/router";
import {NotificationModeEnum} from "@app/features/preservation-space/notification/enums/notification-mode.enum";
import {NotificationHelper} from "@app/features/preservation-space/notification/helper/notification.helper";
import {
  PreservationSpaceNotificationState,
  PreservationSpaceNotificationStateModel,
} from "@app/features/preservation-space/notification/stores/preservation-space-notification.state";
import {Enums} from "@enums";
import {
  NotificationDlcm,
  OrganizationalUnit,
} from "@models";
import {Navigate} from "@ngxs/router-plugin";
import {
  Actions,
  Store,
} from "@ngxs/store";
import {preservationSpaceNotificationStatusHistoryActionNameSpace} from "@preservation-space/notification/stores/status-history/preservation-space-notification-status-history.action";
import {PreservationSpaceNotificationStatusHistoryState} from "@preservation-space/notification/stores/status-history/preservation-space-notification-status-history.state";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {
  PreservationSpaceRoutesEnum,
  RoutesEnum,
} from "@shared/enums/routes.enum";
import {StateEnum} from "@shared/enums/state.enum";
import {ViewModeEnum} from "@shared/enums/view-mode.enum";
import {SharedNotificationHelper} from "@shared/helpers/shared-notification.helper";
import {SharedNotificationAction} from "@shared/stores/notification/shared-notification.action";
import {sharedOrganizationalUnitActionNameSpace} from "@shared/stores/organizational-unit/shared-organizational-unit.action";
import {SharedOrganizationalUnitState} from "@shared/stores/organizational-unit/shared-organizational-unit.state";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";
import {
  AbstractListRoutable,
  ButtonColorEnum,
  DataTableActions,
  DataTableBulkActions,
  DataTableFieldTypeEnum,
  isNullOrUndefined,
  MARK_AS_TRANSLATABLE,
  MemoizedUtil,
  NotificationService,
  ofSolidifyActionCompleted,
  OrderEnum,
  ResourceNameSpace,
  RouterExtensionService,
  Sort,
} from "solidify-frontend";
import {
  PreservationSpaceNotificationAction,
  preservationSpaceNotificationActionNameSpace,
} from "../../../stores/preservation-space-notification.action";

@Component({
  selector: "dlcm-preservation-space-notification-list-routable",
  templateUrl: "./preservation-space-notification-list.routable.html",
  styleUrls: ["./preservation-space-notification-list.routable.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PreservationSpaceNotificationListRoutable extends AbstractListRoutable<NotificationDlcm, PreservationSpaceNotificationStateModel> implements OnInit {
  readonly KEY_CREATE_BUTTON: string = LabelTranslateEnum.create;
  readonly KEY_BACK_BUTTON: string | undefined = MARK_AS_TRANSLATABLE("preservationSpace.notifications.button.goBackToPreservationSpace");
  readonly KEY_PARAM_NAME: keyof NotificationDlcm & string = "notificationType";
  readonly KEY_QUERY_PARAMETER_NOTIFIED_ORGUNIT_ID: keyof NotificationDlcm | string = "notifiedOrgUnit.resId";

  protected _resId: string;

  orgUnitIdObs: Observable<string> = MemoizedUtil.select(this._store, PreservationSpaceNotificationState, state => state.orgUnitId);
  modeObs: Observable<NotificationModeEnum> = MemoizedUtil.select(this._store, PreservationSpaceNotificationState, state => state.mode);
  mode: NotificationModeEnum;

  sharedOrgUnitSort: Sort<OrganizationalUnit> = {
    field: "name",
    order: OrderEnum.ascending,
  };
  sharedOrgUnitActionNameSpace: ResourceNameSpace = sharedOrganizationalUnitActionNameSpace;
  sharedOrganizationalUnitState: typeof SharedOrganizationalUnitState = SharedOrganizationalUnitState;
  isHighlightCondition: (notification: NotificationDlcm) => boolean = notification => notification.notificationMark === Enums.Notification.MarkEnum.UNREAD && this.mode === NotificationModeEnum.inbox;

  get notificationModeEnum(): typeof NotificationModeEnum {
    return NotificationModeEnum;
  }

  protected _defineBulkActions(): DataTableBulkActions<NotificationDlcm>[] {
    return [
      {
        icon: IconNameEnum.approve,
        callback: (list: NotificationDlcm[]) => this._store.dispatch(new SharedNotificationAction.BulkSetRead(list)),
        labelToTranslate: () => LabelTranslateEnum.markAsRead,
        displayCondition: (list: NotificationDlcm[]) => this.mode === NotificationModeEnum.inbox && list.filter(n => n?.notificationMark === Enums.Notification.MarkEnum.UNREAD).length > 0,
        color: ButtonColorEnum.primary,
      },
      {
        icon: IconNameEnum.markAsUnread,
        callback: (list: NotificationDlcm[]) => this._store.dispatch(new SharedNotificationAction.BulkSetUnread(list)),
        labelToTranslate: () => LabelTranslateEnum.markAsUnread,
        displayCondition: (list: NotificationDlcm[]) => this.mode === NotificationModeEnum.inbox && list.filter(n => n?.notificationMark === Enums.Notification.MarkEnum.READ).length > 0,
        color: ButtonColorEnum.primary,
      },
    ];
  }

  constructor(protected readonly _store: Store,
              protected readonly _changeDetector: ChangeDetectorRef,
              protected readonly _route: ActivatedRoute,
              protected readonly _routerExt: RouterExtensionService,
              protected readonly _actions$: Actions,
              protected readonly _dialog: MatDialog,
              protected readonly _injector: Injector,
              private readonly _notificationService: NotificationService) {
    super(_store, _changeDetector, _route, _routerExt, _actions$, _dialog, StateEnum.preservationSpace_notification, preservationSpaceNotificationActionNameSpace, _injector, {
      canCreate: false,
      canGoBack: false,
      canRefresh: true,
      historyState: PreservationSpaceNotificationStatusHistoryState,
      historyStateAction: preservationSpaceNotificationStatusHistoryActionNameSpace,
      historyStatusEnumTranslate: Enums.Notification.StatusEnumTranslate,
    }, StateEnum.preservationSpace);

    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(SharedNotificationAction.RefreshListEvent),
      tap(() => {
        const queryParameters = MemoizedUtil.queryParametersSnapshot(this._store, PreservationSpaceNotificationState);
        this._store.dispatch(new PreservationSpaceNotificationAction.ChangeQueryParameters(queryParameters, true, true));
      }),
    ));
  }

  ngOnInit(): void {
    this._retrieveModeFromUrl();
    super.ngOnInit();
    if (this.mode === NotificationModeEnum.sent) {
      this.isMultiSelectable = false;
    }
  }

  private _retrieveModeFromUrl(): void {
    let orgUnitId = undefined;
    const queryParam = this._route.snapshot.queryParamMap;
    if (queryParam.has(NotificationHelper.KEY_ORGUNIT_ID)) {
      this.columnsSkippedToClear = [this.KEY_QUERY_PARAMETER_NOTIFIED_ORGUNIT_ID];
      orgUnitId = queryParam.get(NotificationHelper.KEY_ORGUNIT_ID);
    }
    this.mode = NotificationHelper.getMode(this._route);
    this._store.dispatch(new PreservationSpaceNotificationAction.SetMode(this.mode, orgUnitId));
  }

  override showDetail(model: NotificationDlcm): void {
    //if it is a notification request, it is redirected directly to the object. If not to the object detail
    this._goToDetailObject(model);
  }

  conditionDisplayEditButton(model: NotificationDlcm | undefined): boolean {
    return false;
  }

  conditionDisplayDeleteButton(model: NotificationDlcm | undefined): boolean {
    return false;
  }

  private _defineActionsInbox(): DataTableActions<NotificationDlcm>[] {
    return [
      {
        logo: IconNameEnum.approve,
        displayOnCondition: current => this.mode === NotificationModeEnum.inbox && current?.notificationStatus === Enums.Notification.StatusEnum.PENDING
          && current?.notificationType.notificationCategory === Enums.Notification.CategoryEnum.REQUEST,
        callback: (current) => this._setApproved(current, ViewModeEnum.list),
        placeholder: (current) => LabelTranslateEnum.markAsDone,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.unapprove,
        displayOnCondition: current => this.mode === NotificationModeEnum.inbox && current?.notificationStatus === Enums.Notification.StatusEnum.PENDING
          && current?.notificationType.notificationCategory === Enums.Notification.CategoryEnum.REQUEST,
        callback: (current) => this._setRefuse(current),
        placeholder: (current) => LabelTranslateEnum.markAsRefused,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.markAsUnread,
        displayOnCondition: current => this.mode === NotificationModeEnum.inbox && current?.notificationMark === Enums.Notification.MarkEnum.READ,
        callback: (current) => this._setUnread(current),
        placeholder: (current) => LabelTranslateEnum.markAsUnread,
        isWrapped: true,
      },
      {
        logo: IconNameEnum.markAsRead,
        displayOnCondition: current => this.mode === NotificationModeEnum.inbox && current?.notificationMark === Enums.Notification.MarkEnum.UNREAD,
        callback: (current) => this._setRead(current),
        placeholder: (current) => LabelTranslateEnum.markAsRead,
        isWrapped: true,
      },
    ];
  }

  private _setApproved(notification: NotificationDlcm, mode: ViewModeEnum | undefined): void {
    this._store.dispatch(new SharedNotificationAction.SetApproved(notification.resId, notification.notificationType.notificationCategory, mode));
  }

  private _setRefuse(notification: NotificationDlcm): void {
    this.subscribe(SharedNotificationHelper.getRefuseDialog(this._dialog, this._store, ViewModeEnum.list, notification));
  }

  private _setRead(notification: NotificationDlcm): void {
    this._store.dispatch(new SharedNotificationAction.SetRead(notification.resId, ViewModeEnum.detail));
  }

  private _setUnread(notification: NotificationDlcm): void {
    this._store.dispatch(new SharedNotificationAction.SetUnread(notification.resId, ViewModeEnum.detail));
  }

  private _goToDetailObject(model: NotificationDlcm): void {
    const url = this._route.snapshot.parent.url.toString();
    let route = RoutesEnum.preservationSpaceNotificationInboxDetail;
    if (url === PreservationSpaceRoutesEnum.notificationSent) {
      route = RoutesEnum.preservationSpaceNotificationSentDetail;
    }
    if (this.mode === NotificationModeEnum.inbox && model.notificationMark === Enums.Notification.MarkEnum.UNREAD) {
      this._setRead(model);
    }
    this._store.dispatch(new Navigate([route, model.resId]));
  }

  protected _defineActions(): DataTableActions<NotificationDlcm>[] {
    if (this.mode === NotificationModeEnum.inbox) {
      return this._defineActionsInbox();
    } else {
      return [];
    }
  }

  defineColumns(): void {
    this.columns = [
      {
        field: "notificationType.notificationCategory" as any,
        header: LabelTranslateEnum.category,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Notification.CategoryEnumTranslate,
      },
      {
        field: "notificationType.resId" as any,
        header: LabelTranslateEnum.type,
        type: DataTableFieldTypeEnum.singleSelect,
        order: OrderEnum.none,
        isFilterable: true,
        isSortable: true,
        translate: true,
        filterEnum: Enums.Notification.TypeEnumTranslate,
      },
      {
        field: "emitter.person.fullName" as any,
        header: LabelTranslateEnum.emitter,
        type: DataTableFieldTypeEnum.string,
        order: OrderEnum.none,
        isFilterable: false,
        isSortable: false,
      },
      {
        field: "creation.when" as any,
        header: LabelTranslateEnum.created,
        type: DataTableFieldTypeEnum.datetime,
        order: OrderEnum.descending,
        isFilterable: false,
        isSortable: true,
      },
    ];

    const orgUnitId = MemoizedUtil.selectSnapshot(this._store, PreservationSpaceNotificationState, state => state.orgUnitId);
    if (isNullOrUndefined(orgUnitId)) {
      this.columns.push({
        field: "notifiedOrgUnit.name" as any,
        header: LabelTranslateEnum.notifiedOrganizationalUnit,
        type: DataTableFieldTypeEnum.searchableSingleSelect,
        order: OrderEnum.none,
        filterableField: this.KEY_QUERY_PARAMETER_NOTIFIED_ORGUNIT_ID as any,
        sortableField: this.KEY_QUERY_PARAMETER_NOTIFIED_ORGUNIT_ID as any,
        resourceNameSpace: this.sharedOrgUnitActionNameSpace,
        resourceState: this.sharedOrganizationalUnitState as any,
        searchableSingleSelectSort: this.sharedOrgUnitSort,
        isFilterable: true,
        isSortable: true,
      });
    }
  }
}
