/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - preservation-space.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {PreservationSpaceHomeRoutable} from "@app/features/preservation-space/components/routables/preservation-space-home/preservation-space-home.routable";
import {PreservationSpaceNotificationState} from "@app/features/preservation-space/notification/stores/preservation-space-notification.state";
import {PreservationSpaceNotificationStatusHistoryState} from "@app/features/preservation-space/notification/stores/status-history/preservation-space-notification-status-history.state";
import {PreservationSpaceRoutingModule} from "@app/features/preservation-space/preservation-space-routing.module";
import {PreservationSpaceState} from "@app/features/preservation-space/stores/preservation-space.state";
import {SharedModule} from "@app/shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";
import {NgxsModule} from "@ngxs/store";
import {PreservationSpaceContributorDepositState} from "@preservation-space/contributor/stores/contributor-deposit/preservation-space-contributor-deposit.state";
import {PreservationSpaceContributorState} from "@preservation-space/contributor/stores/preservation-space-contributor.state";
import {PreservationSpaceInstitutionPersonRoleState} from "@preservation-space/institution/stores/institution-person-role/preservation-space-institution-person-role.state";
import {PreservationSpaceInstitutionOrganizationalUnitState} from "@preservation-space/institution/stores/organizational-unit/preservation-space-institution-organizational-unit.state";
import {PreservationSpaceInstitutionState} from "@preservation-space/institution/stores/preservation-space-institution.state";
import {PreservationSpaceOrganizationalUnitArchiveAclState} from "@preservation-space/organizational-unit/stores/archive-acl/preservation-space-organizational-unit-archive-acl.state";
import {PreservationSpaceOrganizationalUnitDisseminationPolicyState} from "@preservation-space/organizational-unit/stores/dissemination-policy/preservation-space-organizational-unit-dissemination-policy.state";
import {PreservationSpaceOrganizationalUnitInstitutionState} from "@preservation-space/organizational-unit/stores/institution/preservation-space-organizational-unit-institution.state";
import {PreservationSpaceOrganizationalUnitPersonInheritedRoleState} from "@preservation-space/organizational-unit/stores/organizational-unit-person-inherited-role/preservation-space-organizational-unit-person-inherited-role.state";
import {PreservationSpaceOrganizationalUnitPersonRoleState} from "@preservation-space/organizational-unit/stores/organizational-unit-person-role/preservation-space-organizational-unit-person-role.state";
import {PreservationSpaceOrganizationalUnitPreservationPolicyState} from "@preservation-space/organizational-unit/stores/preservation-policy/preservation-space-organizational-unit-preservation-policy.state";
import {PreservationSpaceOrganizationalUnitState} from "@preservation-space/organizational-unit/stores/preservation-space-organizational-unit.state";
import {PreservationSpaceOrganizationalUnitSubjectAreaState} from "@preservation-space/organizational-unit/stores/subject-area/preservation-space-organizational-unit-subject-area-state.service";
import {PreservationSpaceOrganizationalUnitSubmissionPolicyState} from "@preservation-space/organizational-unit/stores/submission-policy/preservation-space-organizational-unit-submission-policy.state";
import {ModuleLoadedEnum} from "@shared/enums/module-loaded.enum";
import {SsrUtil} from "solidify-frontend";
import {
  PreservationSpaceNotificationArchiveAclExternalDuaDialog,
} from "@preservation-space/notification/components/dialogs/preservation-space-notification-archive-acl-external-dua/preservation-space-notification-archive-acl-external-dua.dialog";

const routables = [
  PreservationSpaceHomeRoutable,
];
const containers = [];
const dialogs = [
  PreservationSpaceNotificationArchiveAclExternalDuaDialog,
];
const presentationals = [];

@NgModule({
  declarations: [
    ...routables,
    ...containers,
    ...dialogs,
    ...presentationals,
  ],
  imports: [
    SharedModule,
    PreservationSpaceRoutingModule,
    TranslateModule.forChild({}),
    NgxsModule.forFeature([
      PreservationSpaceState,
      PreservationSpaceOrganizationalUnitState,
      PreservationSpaceOrganizationalUnitPersonRoleState,
      PreservationSpaceOrganizationalUnitPersonInheritedRoleState,
      PreservationSpaceOrganizationalUnitArchiveAclState,
      PreservationSpaceOrganizationalUnitSubmissionPolicyState,
      PreservationSpaceOrganizationalUnitPreservationPolicyState,
      PreservationSpaceOrganizationalUnitDisseminationPolicyState,
      PreservationSpaceOrganizationalUnitInstitutionState,
      PreservationSpaceOrganizationalUnitSubjectAreaState,
      PreservationSpaceInstitutionState,
      PreservationSpaceInstitutionPersonRoleState,
      PreservationSpaceInstitutionOrganizationalUnitState,
      PreservationSpaceContributorState,
      PreservationSpaceContributorDepositState,
      PreservationSpaceNotificationState,
      PreservationSpaceNotificationStatusHistoryState,
    ]),
  ],
  exports: [
    ...routables,
  ],
  providers: [],
})
export class PreservationSpaceModule {
  constructor() {
    if (SsrUtil.window) {
      SsrUtil.window[ModuleLoadedEnum.preservationSpaceModuleLoaded] = true;
    }
  }
}
