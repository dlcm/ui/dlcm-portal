/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - mol-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {DlcmEnvironment} from "@environments/environment.defaults.model";
import {
  AbstractFileVisualizer,
  ChemicalMoleculeVisualizationEnum,
  ENVIRONMENT,
  FileInput,
  SsrUtil,
} from "solidify-frontend";

declare const Jmol: any;

@Injectable({providedIn: "root"})
export class MolFileVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = true;
  isZoomSupported: boolean = false;
  type: string = "molPlugin";

  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DlcmEnvironment) {
    super(_environment,
      _environment.visualizationMolExtensions,
      _environment.visualizationMolContentType,
      _environment.visualizationMolMimeType,
      _environment.visualizationMolPronomId,
      _environment.visualizationMolMaxFileSizeInMegabytes);
  }

  override canHandle(fileInfo: FileInput): boolean {
    if (!super.canHandle(fileInfo)) {
      return false;
    }
    const canHandleByExtension = this.canHandleByExtension(fileInfo);
    const canHandleByMimeTypeContentType = this.canHandleByMimeTypeContentType(fileInfo);
    const canHandleByPuid = this.canHandleByPuid(fileInfo);
    return (canHandleByExtension && canHandleByMimeTypeContentType) || canHandleByPuid;
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return false;
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
  }

  openVisualizer(fileInfo: FileInput, domElement: Element): void {
    const molUrl = URL.createObjectURL(fileInfo.blob);
    const iframe = SsrUtil.window?.document?.createElement("iframe");
    iframe.style.overflow = "hidden";
    const envElement = JSON.stringify(this._environment.visualizationChemicalMoleculeMode);
    const twothreeDimension = JSON.stringify(ChemicalMoleculeVisualizationEnum.threeAndTwoDimensional);
    const threeDimension = JSON.stringify(ChemicalMoleculeVisualizationEnum.threeDimensionalOnly);
    const html = `
<button id="2d" mat-button color="primary" style="
    box-sizing: border-box;
    position: relative;
    user-select: none;
    cursor: pointer;
    outline: none;
    display: inline-block;
    white-space: nowrap;
    text-decoration: none;
    vertical-align: baseline;
    text-align: center;
    margin: 0;
    min-width: 64px;
    line-height: 36px;
    padding: 0 16px;
    border: 1px solid #0277bd;
    border-radius: 4px;
    overflow: visible;
    color: #0277bd;
    background-color: transparent;
"
onclick="Jmol.show2d(jmol, true)">2D</button>
<button id="3d" mat-button color="primary" style="
    box-sizing: border-box;
    position: relative;
    user-select: none;
    cursor: pointer;
    outline: none;
    display: inline-block;
    white-space: nowrap;
    text-decoration: none;
    vertical-align: baseline;
    text-align: center;
    margin: 0;
    min-width: 64px;
    line-height: 36px;
    padding: 0 16px;
    border: 1px solid #0277bd;
    border-radius: 4px;
    overflow: visible;
    color: #0277bd;
    background-color: transparent;
" onclick="Jmol.show2d(jmol, false)">3D</button>
<script src="${this._environment.baseHref}assets/jsmol/JSmol.min.js" type="text/javascript"></script>
<script src="${this._environment.baseHref}assets/jsmol/js/JSmolJME.js" type="text/javascript"></script>
<script src="${this._environment.baseHref}assets/jsmol/jsme/jsme/jsme.nocache.js" type="text/javascript"></script>
<script>
var jmolInfo = {
  color: "#FFFFFF",
  height: 375,
  width: 450,
  script: "set antialiasDisplay true;load ${molUrl} ;cartoon on;color cartoon structure;rotate z 118.48; rotate y 117.66; rotate z -47.64;;",
  use: "HTML5",
  j2sPath: "${this._environment.baseHref}assets/jsmol/j2s",
};
var jmeInfo = {
  use: "HTML5",
  height: 300,
  width: 450,
  visible: true,
  options : "depict"
};
if(${envElement} === ${twothreeDimension} || ${envElement} === ${threeDimension}){
  var jmol = Jmol.getApplet("jmol", jmolInfo);
if(${envElement} === ${twothreeDimension}){
    jmol._readyFunction = () => setTimeout(() => jme._loadFromJmol(jmol));
var jme = Jmol.getJMEApplet("jme", jmeInfo, jmol);
Jmol.show2d(jmol, true);
}
}

</script>
`;
    if (this._environment.visualizationChemicalMoleculeMode === ChemicalMoleculeVisualizationEnum.disabled) {
      this.closeVisualizer(fileInfo, domElement);
    } else {
      domElement.appendChild(iframe);
      iframe.id = "visualizationMolIfrane";
      iframe.contentWindow.document.open();
      iframe.contentWindow.document.write(html);
      iframe.contentWindow.document.close();
      iframe.width = "500px";
      iframe.height = "440px";
      iframe.style.border = "0";
      const x = SsrUtil.window?.document?.getElementById("visualizationMolIfrane") as any;
      const button2d = x.contentDocument.getElementById("2d");
      const button3d = x.contentDocument.getElementById("3d");
      if (this._environment.visualizationChemicalMoleculeMode === ChemicalMoleculeVisualizationEnum.threeDimensionalOnly) {
        button2d.style.display = "none";
        button3d.style.display = "none";
      }
      let y = (x.contentWindow || x.contentDocument);
      if (y.document) {

        y = y.document;

      }
      y.body.style.overflow = "hidden";
      y.body.style.height = "404 !important";
    }
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
  }
}
