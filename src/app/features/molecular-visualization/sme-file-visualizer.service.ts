/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - sme-file-visualizer.service.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Inject,
  Injectable,
} from "@angular/core";
import {DlcmEnvironment} from "@environments/environment.defaults.model";
import {
  AbstractFileVisualizer,
  ENVIRONMENT,
  FileInput,
  SsrUtil,
} from "solidify-frontend";

@Injectable({providedIn: "root"})
export class SmeFileVisualizerService extends AbstractFileVisualizer {
  isFullScreenSupported: boolean = true;
  isZoomSupported: boolean = false;
  type: string = "smePlugin";

  constructor(@Inject(ENVIRONMENT) protected readonly _environment: DlcmEnvironment) {
    super(_environment,
      _environment.visualizationSmeExtensions,
      _environment.visualizationSmeContentType,
      _environment.visualizationSmeMimeType,
      _environment.visualizationSmePronomId,
      _environment.visualizationSmeMaxFileSizeInMegabytes);
  }

  override canHandle(fileInfo: FileInput): boolean {
    if (!super.canHandle(fileInfo)) {
      return false;
    }
    const canHandleByExtension = this.canHandleByExtension(fileInfo);
    const canHandleByMimeTypeContentType = this.canHandleByMimeTypeContentType(fileInfo);
    const canHandleByPuid = this.canHandleByPuid(fileInfo);
    return (canHandleByExtension && canHandleByMimeTypeContentType) || canHandleByPuid;
  }

  isVisualizationOnGoing(fileInfo: FileInput, domElement: Element): boolean {
    return false;
  }

  closeVisualizer(fileInfo: FileInput, domElement: Element): void {
    domElement.remove();
  }

  openVisualizer(fileInfo: FileInput, domElement: Element): void {
    const iframe = SsrUtil.window?.document?.createElement("iframe");
    iframe.style.overflow = "hidden";
    const fr = new FileReader();
    fr.onloadend = ((e) => {
      const molText = JSON.stringify(fr.result);
      const html = `
<div id="jsme_container"></div>
<script type="text/javascript" src="${this._environment.baseHref}assets/jsme/jsme/jsme.nocache.js"></script>
<script>
    function jsmeOnLoad() {
        jsmeApplet = new JSApplet.JSME("jsme_container", "380px", "340px", {
           "options" : "depict"
        });
        jsmeApplet.setAfterStructureModifiedCallback(readMolFile());
}
function readMolFile() {
        var molTextResult = ${molText};
        jsmeApplet.readMolFile(molTextResult);
}
</script>
`;
      domElement.appendChild(iframe);
      iframe.id = "visualizationMolIfrane";
      iframe.contentWindow.document.open();
      iframe.contentWindow.document.write(html);
      iframe.contentWindow.document.close();
      iframe.width = "500px";
      iframe.height = "440px";
      iframe.style.border = "0";
      const x = SsrUtil.window?.document?.getElementById("visualizationMolIfrane") as any;
      let y = (x.contentWindow || x.contentDocument);
      if (y.document) {
        y = y.document;
      }
      y.body.style.overflow = "hidden";
      y.body.style.height = "404px !important";

    });

    fr.readAsText(fileInfo.blob);
  }

  doAction(fileInfo: FileInput, domElement: Element): void {
  }

  handleZoom(zoomEnable: boolean, fileInfo: FileInput, domElement: Element): void {
  }
}
