/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - app-routing.module.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {ApplicationRolePermissionEnum} from "@app/shared/enums/application-role-permission.enum";
import {
  AppRoutesEnum,
  HomePageRoutesEnum,
} from "@app/shared/enums/routes.enum";
import {ApplicationRoleGuardService} from "@app/shared/guards/application-role-guard.service";
import {DlcmRoutes} from "@app/shared/models/dlcm-route.model";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {IsManagerOfAtLeastOneInstitutionGuardService} from "@shared/guards/is-manager-of-at-least-one-institution-guard.service";
import {
  AboutRoutable,
  ChangelogRoutable,
  ColorCheckRoutable,
  CombinedGuardService,
  DevGuardService,
  IconAppSummaryRoutable,
  MaintenanceModeRoutable,
  OrcidRedirectRoutable,
  PageNotFoundRoutable,
  PreventLeaveGuardService,
  PreventLeaveMaintenanceGuardService,
  ReleaseNotesRoutable,
  ServerOfflineModeRoutable,
  UnableToLoadAppRoutable,
  UrlQueryParamHelper,
} from "solidify-frontend";

const routes: DlcmRoutes = [
  {
    path: AppRoutesEnum.home,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/home/home.module").then(m => m.HomeModule),
    data: {
      permission: ApplicationRolePermissionEnum.noPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.deposit,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/deposit/deposit.module").then(m => m.DepositModule),
    data: {
      breadcrumb: LabelTranslateEnum.deposit,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.preservationSpace,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/preservation-space/preservation-space.module").then(m => m.PreservationSpaceModule),
    data: {
      breadcrumb: LabelTranslateEnum.preservationSpace,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.admin,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/admin/admin.module").then(m => m.AdminModule),
    data: {
      breadcrumb: LabelTranslateEnum.administration,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [IsManagerOfAtLeastOneInstitutionGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.preservationPlanning,
    // @ts-ignore Dynamic import
    loadChildren: () => import("@preservation-planning/preservation-planning.module").then(m => m.PreservationPlanningModule),
    data: {
      breadcrumb: LabelTranslateEnum.preservationPlanning,
      permission: ApplicationRolePermissionEnum.adminPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.order,
    // @ts-ignore Dynamic import
    loadChildren: () => import("./features/order/order.module").then(m => m.OrderModule),
    data: {
      breadcrumb: LabelTranslateEnum.archiveOrders,
      permission: ApplicationRolePermissionEnum.userPermission,
      guards: [ApplicationRoleGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.icons,
    component: IconAppSummaryRoutable,
    data: {
      guards: [DevGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.colorCheck,
    component: ColorCheckRoutable,
    data: {
      guards: [DevGuardService],
    },
    canActivate: [CombinedGuardService],
  },
  {
    path: AppRoutesEnum.maintenance,
    component: MaintenanceModeRoutable,
    canDeactivate: [PreventLeaveMaintenanceGuardService],
  },
  {
    path: AppRoutesEnum.about,
    component: AboutRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.about,
    },
  },
  {
    path: AppRoutesEnum.releaseNotes,
    component: ReleaseNotesRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.releaseNotes,
    },
  },
  {
    path: AppRoutesEnum.changelog,
    component: ChangelogRoutable,
    data: {
      breadcrumb: LabelTranslateEnum.changelog,
    },
  },
  {
    path: AppRoutesEnum.serverOffline,
    component: ServerOfflineModeRoutable,
    canDeactivate: [PreventLeaveGuardService],
  },
  {
    path: AppRoutesEnum.unableToLoadApp,
    component: UnableToLoadAppRoutable,
    canDeactivate: [PreventLeaveGuardService],
  },
  {
    path: AppRoutesEnum.orcidRedirect,
    component: OrcidRedirectRoutable,
  },
  {
    path: AppRoutesEnum.root,
    redirectTo: "/" + AppRoutesEnum.home + UrlQueryParamHelper.getQueryParamOAuth2(),
    pathMatch: "full",
  },
  {
    path: AppRoutesEnum.index,
    redirectTo: "/" + AppRoutesEnum.home + UrlQueryParamHelper.getQueryParamOAuth2(),
    pathMatch: "full",
  },
  {
    path: `${AppRoutesEnum.archives}/${AppRoutesEnum.paramId}`,
    redirectTo: `${AppRoutesEnum.home}/${HomePageRoutesEnum.detail}/${AppRoutesEnum.paramId}`,
    pathMatch: "full",
  },
  {
    path: "**",
    component: PageNotFoundRoutable,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: "enabledBlocking",
  })],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
