/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - abstract-main-toolbar.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  Directive,
  Input,
  Output,
} from "@angular/core";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {ApplicationRolePermissionEnum} from "@app/shared/enums/application-role-permission.enum";
import {
  AppRoutesEnum,
  OrderRoutesEnum,
  RoutesEnum,
} from "@app/shared/enums/routes.enum";
import {ThemeEnum} from "@app/shared/enums/theme.enum";
import {PermissionUtil} from "@app/shared/utils/permission.util";
import {Enums} from "@enums";
import {environment} from "@environments/environment";
import {User} from "@models";
import {DataTestEnum} from "@shared/enums/data-test.enum";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {TourEnum} from "@shared/enums/tour.enum";
import {SecurityService} from "@shared/services/security.service";
import {
  BehaviorSubject,
  Observable,
} from "rxjs";
import {
  ObservableOrPromiseOrValue,
  ObservableUtil,
  SOLIDIFY_CONSTANTS,
} from "solidify-frontend";
import LanguageEnum = Enums.Language.LanguageEnum;

@Directive()
export abstract class AbstractMainToolbarPresentational extends SharedAbstractPresentational {
  @Input()
  logged: boolean = false;

  @Input()
  currentPath: string;

  @Input()
  user: User;

  @Input()
  photoUser: string;

  institutionUrl: string = environment.institutionUrl;

  @Input()
  numberArchiveInCart: number = 0;

  @Input()
  numberMyOrderReadyNew: number = 0;

  @Input()
  numberPendingRequestNotificationInbox: number = 0;

  @Input()
  userRoles: Enums.UserApplicationRole.UserApplicationRoleEnum[];

  // Theme
  private readonly _themeBS: BehaviorSubject<ThemeEnum | undefined> = new BehaviorSubject<ThemeEnum | undefined>(undefined);
  @Output("themeChange")
  readonly themeObs: Observable<ThemeEnum | undefined> = ObservableUtil.asObservable(this._themeBS);

  private readonly _userGuideOpenBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("userGuideOpen")
  readonly userGuideOpenObs: Observable<void | undefined> = ObservableUtil.asObservable(this._userGuideOpenBS);

  // Dark mode
  private readonly _darkModeBS: BehaviorSubject<boolean | undefined> = new BehaviorSubject<boolean | undefined>(undefined);
  @Output("darkModeChange")
  readonly darkModeObs: Observable<boolean | undefined> = ObservableUtil.asObservable(this._darkModeBS);

  isProduction: boolean = environment.production;
  isDemoMode: boolean = environment.isDemoMode;
  isOpen: boolean = false;

  private get _theme(): ThemeEnum | undefined {
    return this._themeBS.getValue();
  }

  private set _theme(value: ThemeEnum | undefined) {
    this._themeBS.next(value);
  }

  get theme(): ThemeEnum | undefined {
    return this._theme;
  }

  @Input()
  set theme(theme: ThemeEnum | undefined) {
    this._theme = theme;
  }

  private get _language(): LanguageEnum | undefined {
    return this._languageBS.getValue();
  }

  private set _language(value: LanguageEnum | undefined) {
    this._languageBS.next(value);
  }

  get language(): LanguageEnum | undefined {
    return this._language;
  }

  @Input()
  set language(language: LanguageEnum | undefined) {
    this._language = language;
  }

  private get _darkMode(): boolean | undefined {
    return this._darkModeBS.getValue();
  }

  private set _darkMode(value: boolean | undefined) {
    this._darkModeBS.next(value);
  }

  get darkMode(): boolean | undefined {
    return this._darkMode;
  }

  @Input()
  set darkMode(darkMode: boolean | undefined) {
    this._darkMode = darkMode;
  }

  // Navigate
  protected readonly _navigateBS: BehaviorSubject<string | undefined> = new BehaviorSubject<string | undefined>(undefined);
  @Output("navigateChange")
  readonly navigateObs: Observable<string | undefined> = ObservableUtil.asObservable(this._navigateBS);

  // Language
  private readonly _languageBS: BehaviorSubject<LanguageEnum | undefined> = new BehaviorSubject<LanguageEnum | undefined>(undefined);
  @Output("languageChange")
  readonly languageObs: Observable<LanguageEnum | undefined> = ObservableUtil.asObservable(this._languageBS);

  // Logout
  private readonly _logoutBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("logoutChange")
  readonly logoutObs: Observable<void | undefined> = ObservableUtil.asObservable(this._logoutBS);

  // Login
  private readonly _loginBS: BehaviorSubject<void | undefined> = new BehaviorSubject<void | undefined>(undefined);
  @Output("loginChange")
  readonly loginObs: Observable<void | undefined> = ObservableUtil.asObservable(this._loginBS);

  private _listMenus: MenuToolbar[] = [
    {
      path: () => RoutesEnum.homePage,
      rootModulePath: RoutesEnum.homePage,
      labelToTranslate: LabelTranslateEnum.home,
      isVisible: () => true,
      icon: IconNameEnum.home,
      dataTest: DataTestEnum.linkMenuHome,
    },
    {
      path: () => {
        if (this.userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.admin) || this.userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.root)) {
          return AppRoutesEnum.order;
        } else {
          return AppRoutesEnum.order + SOLIDIFY_CONSTANTS.URL_SEPARATOR + OrderRoutesEnum.myOrder;
        }
      },
      rootModulePath: RoutesEnum.order,
      labelToTranslate: LabelTranslateEnum.archiveOrders,
      isVisible: () => PermissionUtil.isUserHavePermission(this.logged, ApplicationRolePermissionEnum.userPermission, this.userRoles),
      icon: IconNameEnum.order,
      badgeCounter: () => "" + this.numberMyOrderReadyNew,
      badgeDescription: "Counter of new order ready in my order",
      badgeHidden: () => this.numberMyOrderReadyNew === 0,
      dataTest: DataTestEnum.linkMenuOrder,
      tourAnchor: TourEnum.mainMenuOrder,
    },
    {
      path: () => RoutesEnum.deposit,
      rootModulePath: RoutesEnum.deposit,
      labelToTranslate: LabelTranslateEnum.deposit,
      isVisible: () => PermissionUtil.isUserHavePermission(this.logged, ApplicationRolePermissionEnum.userPermission, this.userRoles),
      icon: IconNameEnum.deposit,
      dataTest: DataTestEnum.linkMenuDeposit,
      tourAnchor: TourEnum.mainMenuDeposit,
    },
    {
      path: () => this.numberPendingRequestNotificationInbox === 0 ? RoutesEnum.preservationSpace : RoutesEnum.preservationSpaceNotificationInbox,
      rootModulePath: RoutesEnum.preservationSpace,
      labelToTranslate: LabelTranslateEnum.preservationSpace,
      isVisible: () => PermissionUtil.isUserHavePermission(this.logged, ApplicationRolePermissionEnum.userPermission, this.userRoles),
      icon: IconNameEnum.preservationSpace,
      badgeCounter: () => "" + this.numberPendingRequestNotificationInbox,
      badgeDescription: "Counter of pending request notification in inbox",
      badgeHidden: () => this.numberPendingRequestNotificationInbox === 0,
      dataTest: DataTestEnum.linkMenuPreservationSpace,
      tourAnchor: TourEnum.mainMenuPreservationSpace,
    },
  ];

  private _listMenusAdmin: MenuToolbar[] = [
    {
      path: () => RoutesEnum.preservationPlanning,
      rootModulePath: RoutesEnum.preservationPlanning,
      labelToTranslate: LabelTranslateEnum.preservationPlanning,
      isVisible: () => this._securityService.isRootOrAdmin(),
      icon: IconNameEnum.preservationPlanning,
      dataTest: DataTestEnum.linkMenuPreservationPlanning,
    },
    {
      path: () => RoutesEnum.admin,
      rootModulePath: RoutesEnum.admin,
      labelToTranslate: LabelTranslateEnum.administration,
      isVisible: () => this._securityService.isRootOrAdmin() || this._securityService.isManagerOfAnyInstitution(),
      icon: IconNameEnum.administration,
      dataTest: DataTestEnum.linkMenuAdmin,
    },
  ];

  constructor(protected readonly _securityService: SecurityService) {
    super();
  }

  get routesEnum(): typeof RoutesEnum {
    return RoutesEnum;
  }

  get tourEnum(): typeof TourEnum {
    return TourEnum;
  }

  getListMenuUser(): MenuToolbar[] {
    return this._listMenus.filter(m => m.isVisible());
  }

  getListMenuAdmin(): MenuToolbar[] {
    return this._listMenusAdmin.filter(m => m.isVisible());
  }

  getListMenuUserAndAdminMenu(): MenuToolbar[] {
    return [...this.getListMenuUser(), ...this.getListMenuAdmin()];
  }

  navigate(path: string): void {
    this._navigateBS.next(path);
  }

  toggleMenu(): void {
    this.isOpen = !this.isOpen;
  }

  openUserGuide(): void {
    this._userGuideOpenBS.next();
  }

  openCart(): void {
    this.navigate(RoutesEnum.orderMyOrderDraft);
  }

  openOrder(): void {
    if (this.userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.admin) || this.userRoles.includes(Enums.UserApplicationRole.UserApplicationRoleEnum.root)) {
      this.navigate(AppRoutesEnum.order);
    } else {
      this.navigate(AppRoutesEnum.order + SOLIDIFY_CONSTANTS.URL_SEPARATOR + OrderRoutesEnum.myOrder);
    }
  }

  login(): void {
    this._loginBS.next();
  }

  logout(): void {
    this._logoutBS.next();
  }
}

export interface MenuToolbar {
  path?: () => string;
  rootModulePath: string;
  labelToTranslate: string;
  icon: IconNameEnum;
  isVisible: () => ObservableOrPromiseOrValue<boolean>;
  badgeCounter?: () => string;
  badgeDescription?: string;
  badgeHidden?: () => boolean;
  dataTest?: DataTestEnum;
  tourAnchor?: TourEnum;
}
