/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - person-form.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Injector,
  Input,
} from "@angular/core";
import {
  FormBuilder,
  Validators,
} from "@angular/forms";
import {
  NotificationType,
  Person,
} from "@models";
import {IconNameEnum} from "@shared/enums/icon-name.enum";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {BaseFormDefinition} from "@shared/models/base-form-definition.model";
import {
  AbstractFormPresentational,
  isNullOrUndefined,
  OrcidService,
  PersonWithEmail,
  PropertyName,
  SolidifyValidator,
} from "solidify-frontend";

@Component({
  selector: "dlcm-person-form",
  templateUrl: "./person-form.presentational.html",
  styleUrls: ["./person-form.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersonFormPresentational extends AbstractFormPresentational<Person> {
  @Input()
  selectedNotificationType: NotificationType[];

  @Input()
  isApproverOfAnyOrgUnit: boolean;

  @Input()
  isStewardOfAnyOrgUnit: boolean;

  @Input()
  email: string;

  formDefinition: FormComponentFormDefinition = new FormComponentFormDefinition();

  get labelTranslateEnum(): typeof LabelTranslateEnum {
    return LabelTranslateEnum;
  }

  get iconNameEnum(): typeof IconNameEnum {
    return IconNameEnum;
  }

  get personWithEmail(): PersonWithEmail {
    return {
      firstName: this.form.get(this.formDefinition.firstName).value,
      lastName: this.form.get(this.formDefinition.lastName).value,
      email: this.email,
    };
  }

  constructor(protected readonly _changeDetectorRef: ChangeDetectorRef,
              protected readonly _elementRef: ElementRef,
              protected readonly _injector: Injector,
              private readonly _fb: FormBuilder,
              public readonly orcidService: OrcidService) {
    super(_changeDetectorRef, _elementRef, _injector);
  }

  protected _bindFormTo(person: Person): void {
    this.form = this._fb.group({
      [this.formDefinition.firstName]: [person.firstName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: [person.lastName, [Validators.required, SolidifyValidator]],
      [this.formDefinition.orcid]: [person.orcid],
      [this.formDefinition.notificationTypes]: [isNullOrUndefined(this.selectedNotificationType) ? [] : this.selectedNotificationType.map(n => n.resId)],
    });
  }

  protected _initNewForm(): void {
    this.form = this._fb.group({
      [this.formDefinition.firstName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.lastName]: ["", [Validators.required, SolidifyValidator]],
      [this.formDefinition.orcid]: [""],
      [this.formDefinition.notificationTypes]: [[]],
    });
  }

  protected _treatmentBeforeSubmit(person: Person): Person {
    person.notificationTypes = [];
    const listNotificationTypes = this.form.get(this.formDefinition.notificationTypes).value;
    listNotificationTypes?.forEach(resId => {
      person.notificationTypes.push({resId: resId});
    });
    return person;
  }

  protected override _disableSpecificField(): void {
    this.form.get(this.formDefinition.orcid).disable();
  }
}

class FormComponentFormDefinition extends BaseFormDefinition {
  @PropertyName() firstName: string;
  @PropertyName() lastName: string;
  @PropertyName() orcid: string;
  @PropertyName() notificationTypes: string;
}
