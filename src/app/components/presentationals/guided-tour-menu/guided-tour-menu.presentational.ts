/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - guided-tour-menu.presentational.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  ViewChild,
} from "@angular/core";
import {MatMenu} from "@angular/material/menu";
import {SharedAbstractPresentational} from "@app/shared/components/presentationals/shared-abstract/shared-abstract.presentational";
import {Store} from "@ngxs/store";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {RoutesEnum} from "@shared/enums/routes.enum";
import {StepTourSectionNameEnum} from "@shared/enums/step-tour-section-name.enum";
import {AppTourService} from "@shared/services/app-tour.service";
import {IStepOption} from "ngx-ui-tour-md-menu";
import {MARK_AS_TRANSLATABLE} from "solidify-frontend";

@Component({
  selector: "dlcm-guided-tour-menu",
  templateUrl: "./guided-tour-menu.presentational.html",
  styleUrls: ["./guided-tour-menu.presentational.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  exportAs: "menuGuidedTour",
})
export class GuidedTourMenuPresentational extends SharedAbstractPresentational {
  @ViewChild(MatMenu, {static: true}) menu: MatMenu;

  listMenu: Menu[] = [
    {
      titleToTranslate: MARK_AS_TRANSLATABLE("tour.name.main"),
      stepTourSectionName: StepTourSectionNameEnum.main,
    },
    {
      titleToTranslate: LabelTranslateEnum.deposit,
      stepTourSectionName: StepTourSectionNameEnum.deposit,
      navigationEnd: RoutesEnum.deposit,
    },
    {
      titleToTranslate: MARK_AS_TRANSLATABLE("tour.name.depositMetadata"),
      stepTourSectionName: StepTourSectionNameEnum.depositMetadata,
      navigationEnd: RoutesEnum.deposit,
    },
    {
      titleToTranslate: MARK_AS_TRANSLATABLE("tour.name.depositData"),
      stepTourSectionName: StepTourSectionNameEnum.depositData,
      navigationEnd: RoutesEnum.deposit,
    },
    {
      titleToTranslate: LabelTranslateEnum.preservationSpace,
      stepTourSectionName: StepTourSectionNameEnum.preservationSpace,
    },
    {
      titleToTranslate: LabelTranslateEnum.organizationalUnit,
      stepTourSectionName: StepTourSectionNameEnum.organizationalUnit,
      navigationEnd: RoutesEnum.preservationSpaceOrganizationalUnit,
    },
    {
      titleToTranslate: LabelTranslateEnum.search,
      stepTourSectionName: StepTourSectionNameEnum.search,
    },
  ];

  constructor(private readonly _appTourService: AppTourService,
              private readonly _store: Store) {
    super();
  }

  runTour(menu: Menu): void {
    this._appTourService.runTour(menu.stepTourSectionName, menu.extraOptions, menu.navigationEnd);
  }
}

interface Menu {
  titleToTranslate: string;
  stepTourSectionName: StepTourSectionNameEnum;
  extraOptions?: IStepOption;
  navigationEnd?: string;
}
