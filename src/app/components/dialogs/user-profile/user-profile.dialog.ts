/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - user-profile.dialog.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from "@angular/core";
import {
  MAT_DIALOG_DATA,
  MatDialogRef,
} from "@angular/material/dialog";
import {AppAuthorizedOrganizationalUnitState} from "@app/stores/authorized-organizational-unit/app-authorized-organizational-unit.state";
import {AppPersonAction} from "@app/stores/person/app-person.action";
import {AppPersonState} from "@app/stores/person/app-person.state";
import {AppPersonNotificationTypeAction} from "@app/stores/person/notification-type/app-person-notification-type.action";
import {
  AppPersonNotificationTypeState,
  defaultAppPersonNotificationTypeStateStateQueryParameters,
} from "@app/stores/person/notification-type/app-person-notification-type.state";
import {AppUserAction} from "@app/stores/user/app-user.action";
import {AppUserLogoAction} from "@app/stores/user/user-logo/app-user-logo.action";
import {AppUserLogoState} from "@app/stores/user/user-logo/app-user-logo.state";
import {
  NotificationType,
  Person,
  User,
} from "@models";
import {
  Actions,
  Select,
  Store,
} from "@ngxs/store";
import {SharedAbstractDialog} from "@shared/components/dialogs/shared-abstract/shared-abstract.dialog";
import {PersonFormPresentational} from "@app/components/presentationals/person-form/person-form.presentational";
import {SecurityService} from "@shared/services/security.service";
import {Observable} from "rxjs";
import {
  take,
  tap,
} from "rxjs/operators";
import {
  isNonEmptyArray,
  isNotNullNorUndefined,
  isNullOrUndefined,
  MemoizedUtil,
  ModelFormControlEvent,
  ofSolidifyActionCompleted,
  ResourceFileNameSpace,
  ResourceFileState,
  StoreUtil,
} from "solidify-frontend";

@Component({
  selector: "dlcm-user-profile-dialog",
  templateUrl: "./user-profile.dialog.html",
  styleUrls: ["./user-profile.dialog.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserProfileDialog extends SharedAbstractDialog<User, Person> implements OnInit {
  currentPersonObs: Observable<Person> = MemoizedUtil.current(this._store, AppPersonState);
  isLoadingPersonObs: Observable<boolean> = MemoizedUtil.isLoading(this._store, AppPersonState);
  selectedNotificationTypeObs: Observable<NotificationType[]> = MemoizedUtil.selected(this._store, AppPersonNotificationTypeState);
  @Select(AppPersonState.isReadyToBeDisplayed) isReadyToBeDisplayedObs: Observable<boolean>;

  userLogoActionNameSpace: ResourceFileNameSpace = AppUserLogoAction;
  userLogoState: typeof AppUserLogoState = AppUserLogoState;

  validateEnable: boolean = false;

  isStewardOfAnyOrgUnit: boolean;
  isApproverOfAnyOrgUnit: boolean;

  @ViewChild("formPresentational")
  readonly formPresentational: PersonFormPresentational;

  constructor(protected readonly _store: Store,
              protected readonly _dialogRef: MatDialogRef<UserProfileDialog>,
              @Inject(MAT_DIALOG_DATA) public readonly user: User,
              protected readonly _actions$: Actions,
              protected readonly _securityService: SecurityService) {
    super(_dialogRef, user);
  }

  ngOnInit(): void {
    // Not necessary to refresh current user and cause ExpressionChangedAfterItHasBeenCheckedError when open profile info in deposit-edit.routable (cf DLCM-795)
    // this.store.dispatch(new AppPersonAction.GetById(this.user.person.resId));
    this._store.dispatch(new AppPersonNotificationTypeAction.GetAll(this.personResId, defaultAppPersonNotificationTypeStateStateQueryParameters()));

    const listAuthorizedOrgUnit = MemoizedUtil.listSnapshot(this._store, AppAuthorizedOrganizationalUnitState);
    if (isNonEmptyArray(listAuthorizedOrgUnit)) {
      this.isStewardOfAnyOrgUnit = this._securityService.isStewardOfAnyOrgUnit(listAuthorizedOrgUnit);
    }
    if (isNonEmptyArray(listAuthorizedOrgUnit)) {
      this.isApproverOfAnyOrgUnit = this._securityService.isApproverOfAnyOrgUnit(listAuthorizedOrgUnit);
    }
  }

  savePersonInfo($event: ModelFormControlEvent<Person>): void {
    this._store.dispatch(new AppPersonAction.Update($event));
  }

  getUser(): User {
    if (isNullOrUndefined(this.formPresentational)) {
      return this.user;
    }
    return this.formPresentational.form.value as User;
  }

  get personResId(): string {
    return this.user.person.resId;
  }

  isAvatarPresent(): boolean {
    return isNotNullNorUndefined(this.user.person.avatar);
  }

  newAvatar: Blob | undefined | null = undefined;

  logoChange(blob: Blob): void {
    this.newAvatar = blob;
    this.validateEnable = true;
  }

  validate(): void {
    this.subscribe(this._actions$.pipe(
      ofSolidifyActionCompleted(AppPersonAction.UpdateSuccess),
      take(1),
      tap(result => {
        const person = MemoizedUtil.currentSnapshot(this._store, AppPersonState);
        const actions = ResourceFileState.getActionUpdateFile(person.resId, this.newAvatar, person.avatar, this.userLogoActionNameSpace, this._actions$);
        this.subscribe(StoreUtil.dispatchParallelActionAndWaitForSubActionsCompletion(this._store, actions).pipe(
          tap(resultLogo => {
            if (resultLogo.success) {
              this._store.dispatch(new AppUserAction.GetCurrentUser());
              this.submit(result.action.model);
            }
          }),
        ));
      }),
    ));
    this.formPresentational?.onSubmit();
  }
}
