/*-
 * %%----------------------------------------------------------------------------------------------
 * DLCM Technology - DLCM Portal - index.ts
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2025 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

/* eslint-disable no-restricted-imports */
import {AdditionalFieldsForm as AdditionalFieldsFormPartial} from "@app/generated-api/model/additional-fields-form.partial.model";
import {ArchivalInfoPackage as AipPartial} from "@app/generated-api/model/archival-info-package.partial.model";
import {DataFileChecksum as DataFileChecksumPartial} from "@app/generated-api/model/data-file-checksum.partial.model";
import {DepositDataFile as DepositDataFilePartial} from "@app/generated-api/model/deposit-data-file.partial.model";
import {GlobalBanner as GlobalBannerPartial} from "@app/generated-api/model/global-banner.partial.model";
import {JobExecutionReportLine as JobExecutionReportLinePartial} from "@app/generated-api/model/job-execution-report-line.partial.model";
import {JobExecution as JobExecutionPartial} from "@app/generated-api/model/job-execution.partial.model";
import {License as LicensePartial} from "@app/generated-api/model/license.partial.model";
import {MetadataType as MetadataTypePartial} from "@app/generated-api/model/metadata-type.partial.model";
import {NotificationType as NotificationTypePartial} from "@app/generated-api/model/notification-type.partial.model";
import {Notification as NotificationPartial} from "@app/generated-api/model/notification.partial.model";
import {Order as OrderPartial} from "@app/generated-api/model/order.partial.model";
import {PreservationJob as PreservationJobPartial} from "@app/generated-api/model/preservation-job.partial.model";
import {RequestType as RequestTypePartial} from "@app/generated-api/model/request-type.partial.model";
import {ScheduledTask as ScheduledTaskPartial} from "@app/generated-api/model/scheduled-task.partial.model";
import {SearchCriteria as SearchCriteriaPartial} from "@app/generated-api/model/search-criteria.partial.model";
import {SubmissionPolicy as SubmissionPolicyPartial} from "@app/generated-api/model/submission-policy.partial.model";
import {LabelTranslateEnum} from "@shared/enums/label-translate.enum";
import {KeyValueInfo} from "@shared/models/key-value-info.model";
import {
  KeyValue,
  MARK_AS_TRANSLATABLE,
  StatusModel,
  StringUtil,
  ColorHexaEnum,
} from "solidify-frontend/src/lib/core-resources";
import {
  FacetNamePartialEnum,
  HighlightJsLanguagePartialEnum,
} from "solidify-frontend/src/lib/core/enums";
import {Deposit as DepositPartial} from "../generated-api/model/deposit.partial.model";
import {DisseminationPolicy as DisseminationPolicyPartial} from "../generated-api/model/dissemination-policy.partial.model";
/* eslint-enable no-restricted-imports */

export namespace Enums {
  export namespace HighlightJS {
    type HighlightJsLanguageExtendEnum =
      "xml"
      | "json";
    const HighlightJsLanguageExtendEnum = {
      xml: "xml" as HighlightJsLanguageExtendEnum,
      json: "json" as HighlightJsLanguageExtendEnum,
    };

    export const HighlightJsLanguageEnum = {...HighlightJsLanguagePartialEnum, ...HighlightJsLanguageExtendEnum} as Omit<typeof HighlightJsLanguagePartialEnum, keyof typeof HighlightJsLanguageExtendEnum> & typeof HighlightJsLanguageExtendEnum;
    export type HighlightJsLanguageEnum = typeof HighlightJsLanguageEnum[keyof typeof HighlightJsLanguageEnum];
  }

  export namespace Module {
    export type Name =
      "access"
      | "admin"
      | "archivalStorage"
      | "authorization"
      | "dataManagement"
      | "index"
      | "ingest"
      | "preingest";
    export const Name = {
      access: "access" as Name,
      admin: "admin" as Name,
      archivalStorage: "archivalStorage" as Name,
      authorization: "authorization" as Name,
      dataManagement: "dataManagement" as Name,
      index: "index" as Name,
      ingest: "ingest" as Name,
      preingest: "preingest" as Name,
    };
  }

  export namespace Facet {
    type FacetNameExtendEnum =
      "metadata-versions"
      | "organizational-units"
      | "access-levels"
      | "data-tags"
      | "data-use-policies"
      | "contributors"
      | "formats"
      | "paths"
      | "types"
      | "retentions"
      | "archive-types"
      | "institutions"
      | "institution-descriptions";
    const FacetNameExtendEnum = {
      METADATA_VERSION_FACET: "metadata-versions" as FacetNameExtendEnum,
      ORG_UNIT_FACET: "organizational-units" as FacetNameExtendEnum,
      ACCESS_LEVEL_FACET: "access-levels" as FacetNameExtendEnum,
      DATA_TAG_FACET: "data-tags" as FacetNameExtendEnum,
      DATA_USE_POLICIES_FACET: "data-use-policies" as FacetNameExtendEnum,
      CREATOR_FACET: "contributors" as FacetNameExtendEnum,
      FORMAT_FACET: "formats" as FacetNameExtendEnum,
      PATH_FACET: "paths" as FacetNameExtendEnum,
      TYPE_FACET: "types" as FacetNameExtendEnum,
      RETENTION_FACET: "retentions" as FacetNameExtendEnum,
      ARCHIVE_FACET: "archive-types" as FacetNameExtendEnum,
      INSTITUION_FACET: "institutions" as FacetNameExtendEnum,
      INSTITUION_DESC_FACET: "institution-descriptions" as FacetNameExtendEnum,
    };
    export const Name = {...FacetNamePartialEnum, ...FacetNameExtendEnum} as Omit<typeof FacetNamePartialEnum, keyof typeof FacetNameExtendEnum> & typeof FacetNameExtendEnum;
    export type Name = typeof Name[keyof typeof Name];
  }

  export namespace SearchCriteria {
    export type OperationTypeEnum = SearchCriteriaPartial.OperationTypeEnum;
    export const OperationTypeEnum = SearchCriteriaPartial.OperationTypeEnum;
  }

  export namespace SearchCondition {
    export type Type =
      "MATCH"
      | "TERM"
      | "RANGE"
      | "QUERY"
      | "SIMPLE_QUERY"
      | "NESTED_BOOLEAN";

    export const Type = {
      MATCH: "MATCH" as Type,
      TERM: "TERM" as Type,
      RANGE: "RANGE" as Type,
      QUERY: "QUERY" as Type,
      SIMPLE_QUERY: "SIMPLE_QUERY" as Type,
      NESTED_BOOLEAN: "NESTED_BOOLEAN" as Type,
    };

    export type BooleanClauseType =
      "MUST"
      | "FILTER"
      | "SHOULD"
      | "MUST_NOT";

    export const BooleanClauseType = {
      MUST: "MUST" as BooleanClauseType,
      FILTER: "FILTER" as BooleanClauseType,
      SHOULD: "SHOULD" as BooleanClauseType,
      MUST_NOT: "MUST_NOT" as BooleanClauseType,
    };
  }

  export namespace Aip {
    export type AipContainerEnum = AipPartial.ArchiveContainerEnum;
    export const AipContainerEnum = AipPartial.ArchiveContainerEnum;

    export type AipCopySearchTypeEnum =
      "ALL_COPIES"
      | "ALL_COPIES_COMPLETED"
      | "SOME_COPY_MISSING"
      | "SOME_COPY_IN_ERROR"
      | "SOME_COPY_MISC_STATUS"
      | "SOME_COPY_NOT_UPDATED";
    export const AipCopySearchTypeEnum = {
      ALL_COPIES: "ALL_COPIES" as AipCopySearchTypeEnum,
      ALL_COPIES_COMPLETED: "ALL_COPIES_COMPLETED" as AipCopySearchTypeEnum,
      SOME_COPY_MISSING: "SOME_COPY_MISSING" as AipCopySearchTypeEnum,
      SOME_COPY_IN_ERROR: "SOME_COPY_IN_ERROR" as AipCopySearchTypeEnum,
      SOME_COPY_MISC_STATUS: "SOME_COPY_MISC_STATUS" as AipCopySearchTypeEnum,
      SOME_COPY_NOT_UPDATED: "SOME_COPY_NOT_UPDATED" as AipCopySearchTypeEnum,
    };
    export const AipCopySearchTypeEnumTranslate: KeyValue[] = [
      {
        key: AipCopySearchTypeEnum.ALL_COPIES_COMPLETED,
        value: MARK_AS_TRANSLATABLE("enum.aip.aipCopySearchType.allCopiesCompleted"),
      },
      {
        key: AipCopySearchTypeEnum.SOME_COPY_IN_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.aip.aipCopySearchType.someCopyInError"),
      },
      {
        key: AipCopySearchTypeEnum.SOME_COPY_MISSING,
        value: MARK_AS_TRANSLATABLE("enum.aip.aipCopySearchType.someCopyMissing"),
      },
      {
        key: AipCopySearchTypeEnum.SOME_COPY_MISC_STATUS,
        value: MARK_AS_TRANSLATABLE("enum.aip.aipCopySearchType.someCopyMiscStatus"),
      },
      {
        key: AipCopySearchTypeEnum.SOME_COPY_NOT_UPDATED,
        value: MARK_AS_TRANSLATABLE("enum.aip.aipCopySearchType.someCopyNotUpdated"),
      },
    ];
  }

  export namespace DataFile {
    export namespace Checksum {
      export type AlgoEnum = "CRC32" | "MD5" | "SHA1" | "SHA256" | "SHA512";
      export const AlgoEnum = {
        CRC32: "CRC32" as AlgoEnum,
        MD5: "MD5" as AlgoEnum,
        SHA1: "SHA1" as AlgoEnum,
        SHA256: "SHA256" as AlgoEnum,
        SHA512: "SHA512" as AlgoEnum,
      };

      export type OriginEnum = DataFileChecksumPartial.ChecksumOriginEnum;
      export const OriginEnum = DataFileChecksumPartial.ChecksumOriginEnum;

      export type TypeEnum = DataFileChecksumPartial.ChecksumTypeEnum;
      export const TypeEnum = DataFileChecksumPartial.ChecksumTypeEnum;

      export const OriginEnumTranslate: KeyValue[] = [
        {
          key: OriginEnum.DLCM,
          value: MARK_AS_TRANSLATABLE("checksumOriginEnum.dlcm"),
        },
        {
          key: OriginEnum.DLCM_TOMBSTONE,
          value: MARK_AS_TRANSLATABLE("checksumOriginEnum.dlcmTombstone"),
        },
        {
          key: OriginEnum.USER,
          value: MARK_AS_TRANSLATABLE("checksumOriginEnum.user"),
        },
        {
          key: OriginEnum.PORTAL,
          value: MARK_AS_TRANSLATABLE("checksumOriginEnum.portal"),
        },
      ];
    }

    export namespace DataCategoryAndType {
      export type DataCategoryEnum = DepositDataFilePartial.DataCategoryEnum;
      export const DataCategoryEnum = DepositDataFilePartial.DataCategoryEnum;
      export const DataCategoryEnumTranslate: KeyValue[] = [
        {
          key: DataCategoryEnum.PRIMARY,
          value: MARK_AS_TRANSLATABLE("dataCategoryEnum.primary"),
        },
        {
          key: DataCategoryEnum.SECONDARY,
          value: MARK_AS_TRANSLATABLE("dataCategoryEnum.secondary"),
        },
        {
          key: DataCategoryEnum.PACKAGE,
          value: MARK_AS_TRANSLATABLE("dataCategoryEnum.package"),
        },
        {
          key: DataCategoryEnum.SOFTWARE,
          value: MARK_AS_TRANSLATABLE("dataCategoryEnum.software"),
        },
        {
          key: DataCategoryEnum.INTERNAL,
          value: MARK_AS_TRANSLATABLE("dataCategoryEnum.internal"),
        },
      ];

      export type DataTypeEnum = DepositDataFilePartial.DataTypeEnum;
      export const DataTypeEnum = DepositDataFilePartial.DataTypeEnum;
      export const DataTypeEnumTranslate: KeyValue[] = [
        {
          key: DataTypeEnum.OBSERVATIONAL,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.observational"),
        },
        {
          key: DataTypeEnum.EXPERIMENTAL,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.experimental"),
        },
        {
          key: DataTypeEnum.SIMULATION,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.simulation"),
        },
        {
          key: DataTypeEnum.DERIVED,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.derived"),
        },
        {
          key: DataTypeEnum.REFERENCE,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.reference"),
        },
        {
          key: DataTypeEnum.DIGITALIZED,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.digitalized"),
        },
        {
          key: DataTypeEnum.PUBLICATION,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.publication"),
        },
        {
          key: DataTypeEnum.DATA_PAPER,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.dataPaper"),
        },
        {
          key: DataTypeEnum.DOCUMENTATION,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.documentation"),
        },
        {
          key: DataTypeEnum.INFORMATION_PACKAGE,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.informationPackage"),
        },
        {
          key: DataTypeEnum.METADATA,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.metadata"),
        },
        {
          key: DataTypeEnum.CUSTOM_METADATA,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.customMetadata"),
        },
        {
          key: DataTypeEnum.UPDATED_METADATA,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.updatedMetadata"),
        },
        {
          key: DataTypeEnum.CODE,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.code"),
        },
        {
          key: DataTypeEnum.BINARIES,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.binaries"),
        },
        {
          key: DataTypeEnum.VIRTUAL_MACHINE,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.virtualMachine"),
        },
        {
          key: DataTypeEnum.DATASET_THUMBNAIL,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.datasetThumbnail"),
        },
        {
          key: DataTypeEnum.ARCHIVE_THUMBNAIL,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.archiveThumbnail"),
        },
        {
          key: DataTypeEnum.ARCHIVE_README,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.archiveReadme"),
        },
        {
          key: DataTypeEnum.DATAFILE_THUMBNAIL,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.datafileThumbnail"),
        },
        {
          key: DataTypeEnum.ARCHIVE_DATA_USE_AGREEMENT,
          value: MARK_AS_TRANSLATABLE("dataTypeEnum.archiveDataUseAgreement"),
        },
      ];

      export const getDataTypeForDataCategory: (dataCategory: Enums.DataFile.DataCategoryAndType.DataCategoryEnum, currentTypeSelected?: Enums.DataFile.DataCategoryAndType.DataTypeEnum) => Enums.DataFile.DataCategoryAndType.DataTypeEnum[] = (dataCategory, currentTypeSelected) => {
        switch (dataCategory) {
          case Enums.DataFile.DataCategoryAndType.DataCategoryEnum.PRIMARY:
            return [
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.OBSERVATIONAL,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.EXPERIMENTAL,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.SIMULATION,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.DERIVED,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.REFERENCE,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.DIGITALIZED,
            ];
          case Enums.DataFile.DataCategoryAndType.DataCategoryEnum.SECONDARY:
            return [
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.PUBLICATION,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.DATA_PAPER,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.DOCUMENTATION,
            ];
          case Enums.DataFile.DataCategoryAndType.DataCategoryEnum.PACKAGE:
            const result = [
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.INFORMATION_PACKAGE,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.METADATA,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.CUSTOM_METADATA,
            ];
            if (currentTypeSelected === Enums.DataFile.DataCategoryAndType.DataTypeEnum.UPDATED_METADATA) {
              result.push(Enums.DataFile.DataCategoryAndType.DataTypeEnum.UPDATED_METADATA);
            }
            return result;
          case Enums.DataFile.DataCategoryAndType.DataCategoryEnum.SOFTWARE:
            return [
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.CODE,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.BINARIES,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.VIRTUAL_MACHINE,
            ];
          case Enums.DataFile.DataCategoryAndType.DataCategoryEnum.INTERNAL:
            return [
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.DATASET_THUMBNAIL, // deprecated
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_THUMBNAIL,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_README,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.DATAFILE_THUMBNAIL,
              Enums.DataFile.DataCategoryAndType.DataTypeEnum.ARCHIVE_DATA_USE_AGREEMENT,
            ];
        }
      };
    }

    export type StatusEnum = DepositDataFilePartial.StatusEnum;
    export const StatusEnum = DepositDataFilePartial.StatusEnum;
    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.IN_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.inError"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.READY,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.ready"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.RECEIVED,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.received"),
      },
      {
        key: StatusEnum.CHANGE_RELATIVE_LOCATION,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.changeRelativeLocation"),
      },
      {
        key: StatusEnum.TO_PROCESS,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.toProcess"),
      },
      {
        key: StatusEnum.PROCESSED,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.processed"),
      },
      {
        key: StatusEnum.FILE_FORMAT_IDENTIFIED,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.fileFormatIdentified"),
      },
      {
        key: StatusEnum.FILE_FORMAT_UNKNOWN,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.fileFormatUnknown"),
      },
      {
        key: StatusEnum.VIRUS_CHECKED,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.virusChecked"),
      },
      {
        key: StatusEnum.CLEANING,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.cleaning"),
      },
      {
        key: StatusEnum.EXCLUDED_FILE,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.excludedFile"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.IGNORED_FILE,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.ignoredFile"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.CLEANED,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.status.cleaned"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
    ];

    // TODO REFACTOR HERE TO MATCH WITH USUAL WAY
    export const getStatusExplanation: (status: StatusEnum) => string = status => {
      switch (status) {
        case StatusEnum.READY:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.ready");
        case StatusEnum.IN_ERROR:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.inError");
        case StatusEnum.EXCLUDED_FILE:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.excludedFile");
        case StatusEnum.IGNORED_FILE:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.ignoredFile");
        case StatusEnum.CLEANED:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.cleaned");
        default:
          return MARK_AS_TRANSLATABLE("dataFileQuickStatus.pending");
      }
    };
  }

  export namespace ComplianceLevel {
    export type ComplianceLevelEnum = DepositPartial.ComplianceLevelEnum;
    export const ComplianceLevelEnum = DepositPartial.ComplianceLevelEnum;
    export const ComplianceLevelEnumTranslate: KeyValue[] = [
      {
        key: ComplianceLevelEnum.FULL_COMPLIANCE,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.complianceLevel.fullCompliance"),
      },
      {
        key: ComplianceLevelEnum.AVERAGE_COMPLIANCE,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.complianceLevel.averageCompliance"),
      },
      {
        key: ComplianceLevelEnum.WEAK_COMPLIANCE,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.complianceLevel.weakCompliance"),
      },
      {
        key: ComplianceLevelEnum.NO_COMPLIANCE,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.complianceLevel.noCompliance"),
      },
      {
        key: ComplianceLevelEnum.NOT_ASSESSED,
        value: MARK_AS_TRANSLATABLE("enum.dataFile.complianceLevel.notAssessed"),
      },
    ];

    export enum ComplianceLevelNumberEnum {
      NOT_ASSESSED = 0,
      NO_COMPLIANCE = 1,
      WEAK_COMPLIANCE = 2,
      AVERAGE_COMPLIANCE = 3,
      FULL_COMPLIANCE = 4,
    }

    export class ConvertComplianceLevel {
      static convertStringToNumber(complianceLevelString: ComplianceLevelEnum): ComplianceLevelNumberEnum {
        switch (complianceLevelString) {
          case Enums.ComplianceLevel.ComplianceLevelEnum.NO_COMPLIANCE: {
            return Enums.ComplianceLevel.ComplianceLevelNumberEnum.NO_COMPLIANCE;
          }
          case Enums.ComplianceLevel.ComplianceLevelEnum.WEAK_COMPLIANCE: {
            return Enums.ComplianceLevel.ComplianceLevelNumberEnum.WEAK_COMPLIANCE;
          }
          case Enums.ComplianceLevel.ComplianceLevelEnum.AVERAGE_COMPLIANCE: {
            return Enums.ComplianceLevel.ComplianceLevelNumberEnum.AVERAGE_COMPLIANCE;
          }
          case Enums.ComplianceLevel.ComplianceLevelEnum.FULL_COMPLIANCE: {
            return Enums.ComplianceLevel.ComplianceLevelNumberEnum.FULL_COMPLIANCE;
          }
          case Enums.ComplianceLevel.ComplianceLevelEnum.NOT_ASSESSED:
          default:
            return Enums.ComplianceLevel.ComplianceLevelNumberEnum.NOT_ASSESSED;
        }
      }

      static convertNumberToString(complianceLevelString: ComplianceLevelNumberEnum): ComplianceLevelEnum {
        switch (complianceLevelString) {
          case Enums.ComplianceLevel.ComplianceLevelNumberEnum.NO_COMPLIANCE: {
            return Enums.ComplianceLevel.ComplianceLevelEnum.NO_COMPLIANCE;
          }
          case Enums.ComplianceLevel.ComplianceLevelNumberEnum.WEAK_COMPLIANCE: {
            return Enums.ComplianceLevel.ComplianceLevelEnum.WEAK_COMPLIANCE;
          }
          case Enums.ComplianceLevel.ComplianceLevelNumberEnum.AVERAGE_COMPLIANCE: {
            return Enums.ComplianceLevel.ComplianceLevelEnum.AVERAGE_COMPLIANCE;
          }
          case Enums.ComplianceLevel.ComplianceLevelNumberEnum.FULL_COMPLIANCE: {
            return Enums.ComplianceLevel.ComplianceLevelEnum.FULL_COMPLIANCE;
          }
          case Enums.ComplianceLevel.ComplianceLevelNumberEnum.NOT_ASSESSED:
          default:
            return Enums.ComplianceLevel.ComplianceLevelEnum.NOT_ASSESSED;
        }
      }
    }
  }

  export namespace Access {
    export type AccessEnum = DepositPartial.AccessEnum;
    export const AccessEnum = DepositPartial.AccessEnum;
    export const AccessEnumTranslate: KeyValue[] = [
      {
        key: AccessEnum.PUBLIC,
        value: MARK_AS_TRANSLATABLE("enum.access.public"),
      },
      {
        key: AccessEnum.RESTRICTED,
        value: MARK_AS_TRANSLATABLE("enum.access.restricted"),
      },
      {
        key: AccessEnum.CLOSED,
        value: MARK_AS_TRANSLATABLE("enum.access.closed"),
      },
    ];

    export const EmbargoAccessLevelEnum: KeyValue[] = AccessEnumTranslate;

    export const EmbargoAccessLevelEnumList: (access: AccessEnum) => KeyValue[] = access => {
      if (access === AccessEnum.PUBLIC) {
        return [
          {
            key: AccessEnum.RESTRICTED,
            value: MARK_AS_TRANSLATABLE("enum.access.restricted"),
          },
          {
            key: AccessEnum.CLOSED,
            value: MARK_AS_TRANSLATABLE("enum.access.closed"),
          },
        ];
      } else if (access === AccessEnum.RESTRICTED) {
        return [
          {
            key: AccessEnum.CLOSED,
            value: MARK_AS_TRANSLATABLE("enum.access.closed"),
          },
        ];
      } else {
        return [];
      }
    };
  }

  export namespace DataSensitivity {
    export type DataSensitivityEnum = DepositPartial.DataSensitivityEnum;
    export const DataSensitivityEnum = DepositPartial.DataSensitivityEnum;
    export const DataSensitivityEnumTranslate: KeyValueInfo[] = [
      {
        key: DataSensitivityEnum.UNDEFINED,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.undefined"),
      },
      {
        key: DataSensitivityEnum.BLUE,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.blue"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.blue"),
      },
      {
        key: DataSensitivityEnum.GREEN,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.green"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.green"),
      },
      {
        key: DataSensitivityEnum.YELLOW,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.yellow"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.yellow"),
      },
      {
        key: DataSensitivityEnum.ORANGE,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.orange"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.orange"),
      },
      {
        key: DataSensitivityEnum.RED,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.red"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.red"),
      },
      {
        key: DataSensitivityEnum.CRIMSON,
        value: MARK_AS_TRANSLATABLE("enum.dataSensitivity.crimson"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.dataSensitivityInfo.crimson"),
      },
    ];
  }

  export namespace MetadataVersion {
    export type MetadataVersionEnum = DepositPartial.MetadataVersionEnum;
    export const MetadataVersionEnum = DepositPartial.MetadataVersionEnum;
  }

  export namespace Deposit {
    export type DepositIdentifierTypeEnum =
      "DOI"
      | "ARK";
    export const DepositIdentifierTypeEnum = {
      DOI: "DOI" as DepositIdentifierTypeEnum,
      ARK: "ARK" as DepositIdentifierTypeEnum,
    };

    export type DataUsePolicyEnum = DepositPartial.DataUsePolicyEnum;
    export const DataUsePolicyEnum = DepositPartial.DataUsePolicyEnum;

    const dataUsePolicyEnumNoneTranslate = MARK_AS_TRANSLATABLE("enum.dataUsePolicy.none");
    const dataUsePolicyEnumLicenseTranslate = MARK_AS_TRANSLATABLE("enum.dataUsePolicy.license");
    const dataUsePolicyEnumClickThroughDuaTranslate = MARK_AS_TRANSLATABLE("enum.dataUsePolicy.clickThrough");
    const dataUsePolicyEnumSignedDuaTranslate = MARK_AS_TRANSLATABLE("enum.dataUsePolicy.signed");
    const dataUsePolicyEnumExternalDuaTranslate = MARK_AS_TRANSLATABLE("enum.dataUsePolicy.external");

    export const DataUsePolicyEnumTranslate: KeyValueInfo[] = [
      {
        key: DataUsePolicyEnum.NONE,
        value: dataUsePolicyEnumNoneTranslate,
      },
      {
        key: DataUsePolicyEnum.LICENSE,
        value: dataUsePolicyEnumLicenseTranslate,
      },
      {
        key: DataUsePolicyEnum.CLICK_THROUGH_DUA,
        value: dataUsePolicyEnumClickThroughDuaTranslate,
      },
      {
        key: DataUsePolicyEnum.SIGNED_DUA,
        value: dataUsePolicyEnumSignedDuaTranslate,
      },
      {
        key: DataUsePolicyEnum.EXTERNAL_DUA,
        value: dataUsePolicyEnumExternalDuaTranslate,
      },
    ];

    export type DataUsePolicyShowEnum = "NONE" | "LICENSE" | "DUA";
    export const DataUsePolicyShowEnum = {
      NONE: "NONE" as DataUsePolicyShowEnum,
      LICENSE: "LICENSE" as DataUsePolicyShowEnum,
      DUA: "DUA" as DataUsePolicyShowEnum,
    };
    export const DataUsePolicyShowEnumTranslate: KeyValueInfo[] = [
      {
        key: DataUsePolicyShowEnum.NONE,
        value: dataUsePolicyEnumNoneTranslate,
      },
      {
        key: DataUsePolicyShowEnum.LICENSE,
        value: dataUsePolicyEnumLicenseTranslate,
      },
      {
        key: DataUsePolicyShowEnum.DUA,
        value: MARK_AS_TRANSLATABLE("enum.dataUsePolicy.dua"),
      },
    ];

    export type DuaTypeEnum = "CLICK_THROUGH_DUA" | "SIGNED_DUA" | "EXTERNAL_DUA";
    export const DuaTypeEnum = {
      CLICK_THROUGH_DUA: "CLICK_THROUGH_DUA" as DuaTypeEnum,
      SIGNED_DUA: "SIGNED_DUA" as DuaTypeEnum,
      EXTERNAL_DUA: "EXTERNAL_DUA" as DuaTypeEnum,
    };
    export const DuaTypeEnumTranslate: KeyValueInfo[] = [
      {
        key: DuaTypeEnum.CLICK_THROUGH_DUA,
        value: dataUsePolicyEnumClickThroughDuaTranslate,
      },
      {
        key: DuaTypeEnum.SIGNED_DUA,
        value: dataUsePolicyEnumSignedDuaTranslate,
      },
      {
        key: DuaTypeEnum.EXTERNAL_DUA,
        value: dataUsePolicyEnumExternalDuaTranslate,
      },
    ];

    export type StatusEnum = DepositPartial.StatusEnum;
    export const StatusEnum = DepositPartial.StatusEnum;
    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.IN_PROGRESS,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.inProgress"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.IN_VALIDATION,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.inValidation"),
        backgroundColorHexa: ColorHexaEnum.blue,
      },
      {
        key: StatusEnum.REJECTED,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.rejected"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.IN_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.inError"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.CHECKED,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.checked"),
      },
      {
        key: StatusEnum.APPROVED,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.approved"),
        backgroundColorHexa: ColorHexaEnum.blue,
      },
      {
        key: StatusEnum.SUBMITTED,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.submitted"),
        backgroundColorHexa: ColorHexaEnum.blue,
      },
      {
        key: StatusEnum.COMPLETED,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.completed"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.CLEANING,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.cleaning"),
      },
      {
        key: StatusEnum.CLEANED,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.cleaned"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.DELETING,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.deleting"),
      },
      {
        key: StatusEnum.EDITING_METADATA,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.editingMetadata"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.EDITING_METADATA_REJECTED,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.editingMetadataRejected"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.CANCEL_EDITING_METADATA,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.cancelEditingMetadata"),
      },
      {
        key: StatusEnum.CHECKING_COMPLIANCE,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.checkingCompliance"),
      },
      {
        key: StatusEnum.CHECKING_COMPLIANCE,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.checkingCompliance"),
      },
      {
        key: StatusEnum.CHECKING_COMPLIANCE_CLEANED,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.checkingComplianceCleaned"),
      },
      {
        key: StatusEnum.COMPLIANCE_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.complianceError"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.UPGRADING_METADATA,
        value: MARK_AS_TRANSLATABLE("enum.deposit.status.upgradingMetadata"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
    ];
  }

  export namespace DisseminationPolicy {
    export type TypeEnum = DisseminationPolicyPartial.TypeEnum;
    export const TypeEnum = DisseminationPolicyPartial.TypeEnum;
    export const TypeEnumTranslate: KeyValue[] = [
      {
        key: DisseminationPolicy.TypeEnum.OAIS,
        value: MARK_AS_TRANSLATABLE("enum.disseminationPolicy.type.oais"),
      },
      {
        key: DisseminationPolicy.TypeEnum.IIIF,
        value: MARK_AS_TRANSLATABLE("enum.disseminationPolicy.type.iiif"),
      },
      {
        key: DisseminationPolicy.TypeEnum.HEDERA,
        value: MARK_AS_TRANSLATABLE("enum.disseminationPolicy.type.hedera"),
      },
      {
        key: DisseminationPolicy.TypeEnum.BASIC,
        value: MARK_AS_TRANSLATABLE("enum.disseminationPolicy.type.basic"),
      },
    ];

    export type DownloadFileNameEnum = DisseminationPolicyPartial.DownloadFileNameEnum;
    export const DownloadFileNameEnum = DisseminationPolicyPartial.DownloadFileNameEnum;
    export const DownloadFileNameEnumTranslate: KeyValue[] = [
      {
        key: DisseminationPolicy.DownloadFileNameEnum.ID,
        value: MARK_AS_TRANSLATABLE("enum.disseminationPolicy.downloadFileName.id"),
      },
      {
        key: DisseminationPolicy.DownloadFileNameEnum.NAME,
        value: MARK_AS_TRANSLATABLE("enum.disseminationPolicy.downloadFileName.name"),
      },
    ];
  }

  export namespace FormDescription {
    export type TypeEnum = AdditionalFieldsFormPartial.TypeEnum;
    export const TypeEnum = AdditionalFieldsFormPartial.TypeEnum;

    export const TypeEnumTranslate: KeyValue[] = [
      {
        key: TypeEnum.FORMLY,
        value: MARK_AS_TRANSLATABLE("formDescription.typeEnum.formly"),
      },
    ];
  }

  export namespace Language {
    export type LanguageEnum =
      "fr"
      | "de"
      | "en";
    export const LanguageEnum = {
      fr: "fr" as LanguageEnum,
      de: "de" as LanguageEnum,
      en: "en" as LanguageEnum,
    };

    export const LanguageEnumTranslate: KeyValue[] = [
      {
        key: LanguageEnum.en,
        value: MARK_AS_TRANSLATABLE("language.english"),
      },
      {
        key: LanguageEnum.de,
        value: MARK_AS_TRANSLATABLE("language.german"),
      },
      {
        key: LanguageEnum.fr,
        value: MARK_AS_TRANSLATABLE("language.french"),
      },
    ];
  }

  export namespace License {
    export type OdConformanceEnum = LicensePartial.OdConformanceEnum;
    export const OdConformanceEnum = LicensePartial.OdConformanceEnum;
    export const OdConformanceEnumTranslate: KeyValue[] = [
      {
        key: License.OdConformanceEnum.EMPTY,
        value: StringUtil.stringEmpty,
      },
      {
        key: License.OdConformanceEnum.APPROVED,
        value: MARK_AS_TRANSLATABLE("enum.license.odConformance.approved"),
      },
      {
        key: License.OdConformanceEnum.NOT_REVIEWED,
        value: MARK_AS_TRANSLATABLE("enum.license.odConformance.notReviewed"),
      },
      {
        key: License.OdConformanceEnum.REJECTED,
        value: MARK_AS_TRANSLATABLE("enum.license.odConformance.rejected"),
      },
    ];
    export type OsdConformanceEnum = LicensePartial.OsdConformanceEnum;
    export const OsdConformanceEnum = LicensePartial.OsdConformanceEnum;
    export const OsdConformanceEnumTranslate: KeyValue[] = [
      {
        key: License.OsdConformanceEnum.EMPTY,
        value: StringUtil.stringEmpty,
      },
      {
        key: License.OsdConformanceEnum.APPROVED,
        value: MARK_AS_TRANSLATABLE("enum.license.osdConformance.approved"),
      },
      {
        key: License.OsdConformanceEnum.NOT_REVIEWED,
        value: MARK_AS_TRANSLATABLE("enum.license.osdConformance.notReviewed"),
      },
      {
        key: License.OsdConformanceEnum.REJECTED,
        value: MARK_AS_TRANSLATABLE("enum.license.osdConformance.rejected"),
      },
    ];
    export type StatusEnum = LicensePartial.StatusEnum;
    export const StatusEnum = LicensePartial.StatusEnum;
    export const StatusEnumTranslate: KeyValue[] = [
      {
        key: License.StatusEnum.ACTIVE,
        value: MARK_AS_TRANSLATABLE("enum.license.status.active"),
      },
      {
        key: License.StatusEnum.SUPERSEDED,
        value: MARK_AS_TRANSLATABLE("enum.license.status.superseded"),
      },
      {
        key: License.StatusEnum.RETIRED,
        value: MARK_AS_TRANSLATABLE("enum.license.status.retired"),
      },
    ];
  }

  export namespace MetadataType {
    export type MetadataFormatEnum = MetadataTypePartial.MetadataFormatEnum;
    export const MetadataFormatEnum = MetadataTypePartial.MetadataFormatEnum;
    export const MetadataFormatEnumTranslate: KeyValue[] = [
      {
        key: MetadataType.MetadataFormatEnum.SCHEMA_LESS,
        value: MARK_AS_TRANSLATABLE("enum.metadataFormatEnum.schemaless"),
      },
      {
        key: MetadataType.MetadataFormatEnum.JSON,
        value: MARK_AS_TRANSLATABLE("enum.metadataFormatEnum.json"),
      },
      {
        key: MetadataType.MetadataFormatEnum.XML,
        value: MARK_AS_TRANSLATABLE("enum.metadataFormatEnum.xml"),
      },
      {
        key: MetadataType.MetadataFormatEnum.CUSTOM,
        value: MARK_AS_TRANSLATABLE("enum.metadataFormatEnum.custom"),
      },
    ];
  }

  export namespace Notification {
    export type StatusEnum = NotificationPartial.NotificationStatusEnum;
    export const StatusEnum = NotificationPartial.NotificationStatusEnum;
    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.APPROVED,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationStatus.approved"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.REFUSED,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationStatus.refused"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.PENDING,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationStatus.pending"),
        backgroundColorHexa: ColorHexaEnum.blue,
      },
      {
        key: StatusEnum.NON_APPLICABLE,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationStatus.nonApplicable"),
        backgroundColorHexa: ColorHexaEnum.blue,
      },
    ];

    export type MarkEnum = NotificationPartial.NotificationMarkEnum;
    export const MarkEnum = NotificationPartial.NotificationMarkEnum;
    export const MarkEnumTranslate: KeyValue[] = [
      {
        key: MarkEnum.READ,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationMark.read"),
      },
      {
        key: MarkEnum.UNREAD,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationMark.unread"),
      },
    ];

    export type CategoryEnum = NotificationTypePartial.NotificationCategoryEnum;
    export const CategoryEnum = NotificationTypePartial.NotificationCategoryEnum;
    export const CategoryEnumTranslate: KeyValue[] = [
      {
        key: CategoryEnum.REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationCategory.request"),
      },
      {
        key: CategoryEnum.INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationCategory.info"),
      },
    ];

    export type TypeEnum =
      "ACCESS_DATASET_REQUEST"
      | "JOIN_ORGUNIT_REQUEST"
      | "CREATE_ORGUNIT_REQUEST"
      | "IN_ERROR_SIP_INFO"
      | "IN_ERROR_AIP_INFO"
      | "VALIDATE_DEPOSIT_REQUEST"
      | "MY_APPROVED_DEPOSIT_INFO"
      | "MY_COMPLETED_DEPOSIT_INFO"
      | "MY_INDIRECT_COMPLETED_DEPOSIT_INFO"
      | "CREATED_DEPOSIT_INFO"
      | "IN_ERROR_DEPOSIT_INFO"
      | "IN_ERROR_DIP_INFO"
      | "IN_ERROR_DOWNLOADED_AIP_INFO"
      | "APPROVE_DISPOSAL_REQUEST"
      | "APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST"
      | "MY_COMPLETED_ARCHIVE_INFO"
      | "COMPLETED_ARCHIVE_INFO";
    export const TypeEnum = {
      ACCESS_DATASET_REQUEST: "ACCESS_DATASET_REQUEST" as TypeEnum,
      JOIN_ORGUNIT_REQUEST: "JOIN_ORGUNIT_REQUEST" as TypeEnum,
      CREATE_ORGUNIT_REQUEST: "CREATE_ORGUNIT_REQUEST" as TypeEnum,
      IN_ERROR_SIP_INFO: "IN_ERROR_SIP_INFO" as TypeEnum,
      IN_ERROR_AIP_INFO: "IN_ERROR_AIP_INFO" as TypeEnum,
      VALIDATE_DEPOSIT_REQUEST: "VALIDATE_DEPOSIT_REQUEST" as TypeEnum,
      MY_APPROVED_DEPOSIT_INFO: "MY_APPROVED_DEPOSIT_INFO" as TypeEnum,
      MY_COMPLETED_DEPOSIT_INFO: "MY_COMPLETED_DEPOSIT_INFO" as TypeEnum,
      MY_INDIRECT_COMPLETED_DEPOSIT_INFO: "MY_INDIRECT_COMPLETED_DEPOSIT_INFO" as TypeEnum,
      CREATED_DEPOSIT_INFO: "CREATED_DEPOSIT_INFO" as TypeEnum,
      IN_ERROR_DEPOSIT_INFO: "IN_ERROR_DEPOSIT_INFO" as TypeEnum,
      IN_ERROR_DIP_INFO: "IN_ERROR_DIP_INFO" as TypeEnum,
      IN_ERROR_DOWNLOADED_AIP_INFO: "IN_ERROR_DOWNLOADED_AIP_INFO" as TypeEnum,
      APPROVE_DISPOSAL_REQUEST: "APPROVE_DISPOSAL_REQUEST" as TypeEnum,
      APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST: "APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST" as TypeEnum,
      MY_COMPLETED_ARCHIVE_INFO: "MY_COMPLETED_ARCHIVE_INFO" as TypeEnum,
      COMPLETED_ARCHIVE_INFO: "COMPLETED_ARCHIVE_INFO" as TypeEnum,
    };
    export const TypeEnumTranslate: KeyValue[] = [
      {
        key: TypeEnum.ACCESS_DATASET_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.accessDatasetRequest"),
      },
      {
        key: TypeEnum.JOIN_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.joinOrgUnitRequest"),
      },
      {
        key: TypeEnum.CREATE_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.createOrgUnitRequest"),
      },
      {
        key: TypeEnum.IN_ERROR_SIP_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.inErrorSipInfo"),
      },
      {
        key: TypeEnum.IN_ERROR_AIP_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.inErrorAipInfo"),
      },
      {
        key: TypeEnum.VALIDATE_DEPOSIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.validateDepositRequest"),
      },
      {
        key: TypeEnum.MY_APPROVED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.myApprovedDepositInfo"),
      },
      {
        key: TypeEnum.MY_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.myCompletedDepositInfo"),
      },
      {
        key: TypeEnum.MY_INDIRECT_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.myIndirectCompletedDepositInfo"),
      },
      {
        key: TypeEnum.CREATED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.createdDepositInfo"),
      },
      {
        key: TypeEnum.IN_ERROR_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.inErrorDepositInfo"),
      },
      {
        key: TypeEnum.IN_ERROR_DIP_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.inErrorDipInfo"),
      },
      {
        key: TypeEnum.IN_ERROR_DOWNLOADED_AIP_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.inErrorDownloadedAipInfo"),
      },
      {
        key: TypeEnum.APPROVE_DISPOSAL_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.approveDisposalRequest"),
      },
      {
        key: TypeEnum.APPROVE_DISPOSAL_BY_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.approveDisposalByOrgunitRequest"),
      },
      {
        key: TypeEnum.MY_COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.myCompletedArchiveInfo"),
      },
      {
        key: TypeEnum.COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("notification.enum.notificationType.completedArchiveInfo"),
      },
    ];
  }

  export namespace Archive {
    export type RatingTypeEnum =
      "QUALITY"
      | "USEFULNESS";
    export const RatingTypeEnum = {
      QUALITY: "QUALITY" as RatingTypeEnum,
      USEFULNESS: "USEFULNESS" as RatingTypeEnum,
    };
    export const RatingTypeEnumTranslate: KeyValue[] = [
      {
        key: RatingTypeEnum.QUALITY,
        value: MARK_AS_TRANSLATABLE("home.archive.ranking.quality"),
      },
      {
        key: RatingTypeEnum.USEFULNESS,
        value: MARK_AS_TRANSLATABLE("home.archive.ranking.usefulness"),
      },
    ];
  }

  export namespace Order {
    export type QueryTypeEnum = OrderPartial.QueryTypeEnum;
    export const QueryTypeEnum = OrderPartial.QueryTypeEnum;
    export const QueryTypeEnumTranslate: KeyValue[] = [
      {
        key: QueryTypeEnum.DIRECT,
        value: MARK_AS_TRANSLATABLE("enum.queryType.direct"),
      },
      {
        key: QueryTypeEnum.SIMPLE,
        value: MARK_AS_TRANSLATABLE("enum.queryType.simple"),
      },
      {
        key: QueryTypeEnum.ADVANCED,
        value: MARK_AS_TRANSLATABLE("enum.queryType.advanced"),
      },
    ];
    export type StatusEnum = OrderPartial.StatusEnum;
    export const StatusEnum = OrderPartial.StatusEnum;
    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.IN_ERROR,
        value: MARK_AS_TRANSLATABLE("order.enum.status.inError"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.IN_PREPARATION,
        value: MARK_AS_TRANSLATABLE("order.enum.status.inPreparation"),
      },
      {
        key: StatusEnum.DOWNLOADING,
        value: MARK_AS_TRANSLATABLE("order.enum.status.downloading"),
      },
      {
        key: StatusEnum.SUBMITTED,
        value: MARK_AS_TRANSLATABLE("order.enum.status.submitted"),
      },
      {
        key: StatusEnum.IN_PROGRESS,
        value: MARK_AS_TRANSLATABLE("order.enum.status.inProgress"),
      },
      {
        key: StatusEnum.READY,
        value: MARK_AS_TRANSLATABLE("order.enum.status.ready"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.IN_DISSEMINATION_PREPARATION,
        value: MARK_AS_TRANSLATABLE("order.enum.status.inDisseminationPreparation"),
      },
    ];
  }

  export namespace RequestType {
    export type VerbEnum = RequestTypePartial.VerbEnum;
    export const VerbEnum = RequestTypePartial.VerbEnum;
  }

  export namespace ScheduledTask {
    export type TaskTypeEnum = ScheduledTaskPartial.TaskTypeEnum;
    export const TaskTypeEnum = ScheduledTaskPartial.TaskTypeEnum;

    export const TaskTypeEnumTranslate: KeyValue[] = [
      {
        key: TaskTypeEnum.DEPOSITS_TO_VALIDATE,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.depositsToValidate"),
      },
      {
        key: TaskTypeEnum.APPROVED_DEPOSITS_CREATOR,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.approvedDepositsCreator"),
      },
      {
        key: TaskTypeEnum.COMPLETED_DEPOSITS_CREATOR,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.completedDepositsCreator"),
      },
      {
        key: TaskTypeEnum.COMPLETED_DEPOSITS_CONTRIBUTORS,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.completedDepositsContributors"),
      },
      {
        key: TaskTypeEnum.DEPOSITS_IN_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.depositsInError"),
      },
      {
        key: TaskTypeEnum.COMPLETED_ARCHIVES_CREATOR,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.completedArchivesCreator"),
      },
      {
        key: TaskTypeEnum.COMPLETED_ARCHIVES_APPROVERS,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.completedArchivesApprovers"),
      },
      {
        key: TaskTypeEnum.ORG_UNIT_CREATION_TO_VALIDATE,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.orgUnitCreationToValidate"),
      },
      {
        key: TaskTypeEnum.JOIN_MEMBER_ORG_UNIT_TO_VALIDATE,
        value: MARK_AS_TRANSLATABLE("enum.scheduledTask.taskType.joinMemberOrgUnitToValidate"),
      },
    ];
  }

  export namespace Package {
    export type StatusEnum = AipPartial.PackageStatusEnum;
    export const StatusEnum = AipPartial.PackageStatusEnum;
    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.IN_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.package.status.inError"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.IN_PROGRESS,
        value: MARK_AS_TRANSLATABLE("enum.package.status.inProgress"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.COMPLETED,
        value: MARK_AS_TRANSLATABLE("enum.package.status.completed"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.IN_PREPARATION,
        value: MARK_AS_TRANSLATABLE("enum.package.status.inPreparation"),
      },
      {
        key: StatusEnum.READY,
        value: MARK_AS_TRANSLATABLE("enum.package.status.ready"),
      },
      {
        key: StatusEnum.CHECK_PENDING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.checkPending"),
      },
      {
        key: StatusEnum.CHECKING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.checking"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.CHECKED,
        value: MARK_AS_TRANSLATABLE("enum.package.status.checked"),
      },
      {
        key: StatusEnum.RESUBMITTING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.resubmitting"),
      },
      {
        key: StatusEnum.STORED,
        value: MARK_AS_TRANSLATABLE("enum.package.status.stored"),
      },
      {
        key: StatusEnum.INDEXING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.indexing"),
      },
      {
        key: StatusEnum.REINDEXING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.reindexing"),
      },
      {
        key: StatusEnum.DOWNLOADING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.downloading"),
      },
      {
        key: StatusEnum.RELOADED,
        value: MARK_AS_TRANSLATABLE("enum.package.status.reloaded"),
      },
      {
        key: StatusEnum.PRESERVATION_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.package.status.preservationError"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
      {
        key: StatusEnum.CLEANING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.cleaning"),
      },
      {
        key: StatusEnum.CLEANED,
        value: MARK_AS_TRANSLATABLE("enum.package.status.cleaned"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.DISPOSABLE,
        value: MARK_AS_TRANSLATABLE("enum.package.status.disposable"),
      },
      {
        key: StatusEnum.DISPOSAL_APPROVED_BY_ORGUNIT,
        value: MARK_AS_TRANSLATABLE("enum.package.status.disposalApprovedByOrgunit"),
      },
      {
        key: StatusEnum.DISPOSAL_APPROVED,
        value: MARK_AS_TRANSLATABLE("enum.package.status.disposalApproved"),
      },
      {
        key: StatusEnum.DISPOSED,
        value: MARK_AS_TRANSLATABLE("enum.package.status.disposed"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.TOMBSTONE_REPLICATION_PENDING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.tombstoneReplicationPending"),
      },
      {
        key: StatusEnum.REPLICATING_TOMBSTONE,
        value: MARK_AS_TRANSLATABLE("enum.package.status.replicatingTombstone"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.PACKAGE_REPLICATION_PENDING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.packageReplicationPending"),
      },
      {
        key: StatusEnum.REPLICATING_PACKAGE,
        value: MARK_AS_TRANSLATABLE("enum.package.status.replicatingPackage"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.UPDATING_RETENTION,
        value: MARK_AS_TRANSLATABLE("enum.package.status.updatingRetention"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.METADATA_EDITION_PENDING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.metadataEditionPending"),
      },
      {
        key: StatusEnum.EDITING_METADATA,
        value: MARK_AS_TRANSLATABLE("enum.package.status.editingMetadata"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.FIXITY_ERROR,
        value: MARK_AS_TRANSLATABLE("enum.package.status.fixityError"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.METADATA_UPGRADE_PENDING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.metadataUpgradePending"),
      },
      {
        key: StatusEnum.UPGRADING_METADATA,
        value: MARK_AS_TRANSLATABLE("enum.package.status.upgradingMetadata"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.COMPLIANCE_LEVEL_UPDATE_PENDING,
        value: MARK_AS_TRANSLATABLE("enum.package.status.complianceLevelUpdatePending"),
      },
      {
        key: StatusEnum.UPDATING_COMPLIANCE_LEVEL,
        value: MARK_AS_TRANSLATABLE("enum.package.status.updatingComplianceLevel"),
        backgroundColorHexa: ColorHexaEnum.orange,
      },
      {
        key: StatusEnum.COMPLIANCE_LEVEL_UPDATED,
        value: MARK_AS_TRANSLATABLE("enum.package.status.complianceLevelUpdated"),
      },
    ];

    export const statusIsCompleted: (status: StatusEnum) => boolean = status => (status === StatusEnum.COMPLETED || status === StatusEnum.DISPOSED);

    export const statusIsDisposable: (status: StatusEnum) => boolean = status => (status === StatusEnum.DISPOSABLE);
  }

  export namespace PreservationJob {
    export type JobRecurrenceEnum = PreservationJobPartial.JobRecurrenceEnum;
    export const JobRecurrenceEnum = PreservationJobPartial.JobRecurrenceEnum;
    export const JobRecurrenceEnumTranslate: KeyValue[] = [
      {
        key: JobRecurrenceEnum.DAILY,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobRecurrenceEnum.daily"),
      },
      {
        key: JobRecurrenceEnum.WEEKLY,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobRecurrenceEnum.weekly"),
      },
      {
        key: JobRecurrenceEnum.MONTHLY,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobRecurrenceEnum.monthly"),
      },
      {
        key: JobRecurrenceEnum.YEARLY,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobRecurrenceEnum.yearly"),
      },
      {
        key: JobRecurrenceEnum.ONCE,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobRecurrenceEnum.once"),
      },
    ];

    export type JobTypeEnum = PreservationJobPartial.JobTypeEnum;
    export const JobTypeEnum = PreservationJobPartial.JobTypeEnum;
    export const JobTypeEnumTranslate: KeyValue[] = [
      {
        key: JobTypeEnum.CLEAN_SUBMISSION,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.cleanSubmission"),
      },
      {
        key: JobTypeEnum.SIMPLE_CLEAN_SUBMISSION,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.simpleCleanSubmission"),
      },
      {
        key: JobTypeEnum.CHECK_COMPLIANCE_LEVEL,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.checkComplianceLevel"),
      },
      {
        key: JobTypeEnum.FIXITY,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.fixity"),
      },
      {
        key: JobTypeEnum.METADATA_MIGRATION,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.metadataMigration"),
      },
      {
        key: JobTypeEnum.COMPLIANCE_LEVEL_UPDATE,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.complianceLevelUpdate"),
      },
      {
        key: JobTypeEnum.PURGE_SUBMISSION_TEMP_FILES,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.purgeSubmissionTempFiles"),
      },
      {
        key: JobTypeEnum.PURGE_ORDER,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.purgeOrder"),
      },
      {
        key: JobTypeEnum.REINDEX,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.reindex"),
      },
      {
        key: JobTypeEnum.REINDEX_ALL,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.reindexAll"),
      },
      {
        key: JobTypeEnum.RELOAD,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.reload"),
      },
      {
        key: JobTypeEnum.REPLICATION,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.replication"),
      },
      {
        key: JobTypeEnum.REPLICATION_CHECK,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.replicationCheck"),
      },
      {
        key: JobTypeEnum.ARCHIVE_CHECK,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.archiveCheck"),
      },
      {
        key: JobTypeEnum.DISPOSAL,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.disposal"),
      },
      {
        key: JobTypeEnum.ARCHIVE_PRELOAD_SMALL,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.archivePreloadSmall"),
      },
      {
        key: JobTypeEnum.ARCHIVE_PRELOAD_BIG,
        value: MARK_AS_TRANSLATABLE("preservation.job.jobTypeEnum.archivePreloadBig"),
      },
    ];

    export type JobExecutionReportLineStatusEnum = JobExecutionReportLinePartial.StatusEnum;
    export const JobExecutionReportLineStatusEnum = JobExecutionReportLinePartial.StatusEnum;
    export const JobExecutionReportLineStatusEnumTranslate: StatusModel[] = [
      {
        key: JobExecutionReportLineStatusEnum.PROCESSED,
        value: MARK_AS_TRANSLATABLE("preservation.jobExecution.report.detail.jobStatusEnum.processed"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: JobExecutionReportLineStatusEnum.IGNORED,
        value: MARK_AS_TRANSLATABLE("preservation.jobExecution.report.detail.jobStatusEnum.ignored"),
      },
      {
        key: JobExecutionReportLineStatusEnum.ERROR,
        value: MARK_AS_TRANSLATABLE("preservation.jobExecution.report.detail.jobStatusEnum.error"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
    ];

    export type StatusEnum = JobExecutionPartial.StatusEnum;
    export const StatusEnum = JobExecutionPartial.StatusEnum;
    export const StatusEnumTranslate: StatusModel[] = [
      {
        key: StatusEnum.IN_PROGRESS,
        value: MARK_AS_TRANSLATABLE("preservation.jobExecution.jobStatusEnum.inProgress"),
      },
      {
        key: StatusEnum.COMPLETED,
        value: MARK_AS_TRANSLATABLE("preservation.jobExecution.jobStatusEnum.completed"),
        backgroundColorHexa: ColorHexaEnum.green,
      },
      {
        key: StatusEnum.READY,
        value: MARK_AS_TRANSLATABLE("preservation.jobExecution.jobStatusEnum.ready"),
      },
      {
        key: StatusEnum.IN_ERROR,
        value: MARK_AS_TRANSLATABLE("preservation.jobExecution.jobStatusEnum.inError"),
        backgroundColorHexa: ColorHexaEnum.red,
      },
    ];
  }

  export namespace RetentionPolicy {
    export type RetentionPolicyEnum =
      "days"
      | "years"
      | "centuries"
      | "forever";
    export const RetentionPolicyEnum = {
      days: "days" as RetentionPolicyEnum,
      years: "years" as RetentionPolicyEnum,
      centuries: "centuries" as RetentionPolicyEnum,
      forever: "forever" as RetentionPolicyEnum,
    };
    export const RetentionPolicyEnumTranslate: KeyValue[] = [
      {
        key: RetentionPolicyEnum.days,
        value: MARK_AS_TRANSLATABLE("admin.preservationPolicy.retention.days"),
      },
      {
        key: RetentionPolicyEnum.years,
        value: MARK_AS_TRANSLATABLE("admin.preservationPolicy.retention.years"),
      },
      {
        key: RetentionPolicyEnum.centuries,
        value: MARK_AS_TRANSLATABLE("admin.preservationPolicy.retention.centuries"),
      },
      {
        key: RetentionPolicyEnum.forever,
        value: LabelTranslateEnum.forever,
      },
    ];
  }

  export namespace Role {
    export type RoleEnum =
      "APPROVER"
      | "CREATOR"
      | "MANAGER"
      | "STEWARD"
      | "VISITOR";
    export const RoleEnum = {
      APPROVER: "APPROVER" as RoleEnum,
      CREATOR: "CREATOR" as RoleEnum,
      MANAGER: "MANAGER" as RoleEnum,
      STEWARD: "STEWARD" as RoleEnum,
      VISITOR: "VISITOR" as RoleEnum,
    };
  }

  export namespace UserApplicationRole {
    export type UserApplicationRoleEnum =
      "ROOT"
      | "ADMIN"
      | "USER"
      | "TRUSTED_CLIENT"
      | "GUEST";
    export const UserApplicationRoleEnum = {
      root: "ROOT" as UserApplicationRoleEnum,
      admin: "ADMIN" as UserApplicationRoleEnum,
      user: "USER" as UserApplicationRoleEnum,
      trusted_client: "TRUSTED_CLIENT" as UserApplicationRoleEnum,
      guest: "GUEST" as UserApplicationRoleEnum,
    };
    export const UserApplicationRoleEnumTranslate: KeyValue[] = [
      {
        key: UserApplicationRoleEnum.admin,
        value: MARK_AS_TRANSLATABLE("admin.user.roles.admin"),
      },
      {
        key: UserApplicationRoleEnum.root,
        value: MARK_AS_TRANSLATABLE("admin.user.roles.root"),
      },
      {
        key: UserApplicationRoleEnum.user,
        value: MARK_AS_TRANSLATABLE("admin.user.roles.user"),
      },
      {
        key: UserApplicationRoleEnum.trusted_client,
        value: MARK_AS_TRANSLATABLE("admin.user.roles.trustedClient"),
      },
    ];
  }

  export namespace GlobalBanner {
    export type TypeEnum = GlobalBannerPartial.TypeEnum;
    export const TypeEnum = GlobalBannerPartial.TypeEnum;
    export const TypeEnumTranslate: KeyValue[] = [
      {
        key: TypeEnum.CRITICAL,
        value: MARK_AS_TRANSLATABLE("enum.globalBanner.type.critical"),
      },
      {
        key: TypeEnum.WARNING,
        value: MARK_AS_TRANSLATABLE("enum.globalBanner.type.warning"),
      },
      {
        key: TypeEnum.INFO,
        value: MARK_AS_TRANSLATABLE("enum.globalBanner.type.info"),
      },
    ];
  }

  export namespace NotificationType {
    export type NotificationTypeEnum =
      "VALIDATE_DEPOSIT_REQUEST"
      | "MY_APPROVED_DEPOSIT_INFO"
      | "MY_COMPLETED_DEPOSIT_INFO"
      | "IN_ERROR_DEPOSIT_INFO"
      | "MY_INDIRECT_COMPLETED_DEPOSIT_INFO"
      | "MY_COMPLETED_ARCHIVE_INFO"
      | "COMPLETED_ARCHIVE_INFO"
      | "CREATE_ORGUNIT_REQUEST"
      | "JOIN_ORGUNIT_REQUEST"
      | "ACCESS_DATASET_REQUEST";
    export const NotificationTypeEnum = {
      VALIDATE_DEPOSIT_REQUEST: "VALIDATE_DEPOSIT_REQUEST" as NotificationTypeEnum,
      MY_APPROVED_DEPOSIT_INFO: "MY_APPROVED_DEPOSIT_INFO" as NotificationTypeEnum,
      MY_COMPLETED_DEPOSIT_INFO: "MY_COMPLETED_DEPOSIT_INFO" as NotificationTypeEnum,
      IN_ERROR_DEPOSIT_INFO: "IN_ERROR_DEPOSIT_INFO" as NotificationTypeEnum,
      MY_INDIRECT_COMPLETED_DEPOSIT_INFO: "MY_INDIRECT_COMPLETED_DEPOSIT_INFO" as NotificationTypeEnum,
      MY_COMPLETED_ARCHIVE_INFO: "MY_COMPLETED_ARCHIVE_INFO" as NotificationTypeEnum,
      COMPLETED_ARCHIVE_INFO: "COMPLETED_ARCHIVE_INFO" as NotificationTypeEnum,
      CREATE_ORGUNIT_REQUEST: "CREATE_ORGUNIT_REQUEST" as NotificationTypeEnum,
      JOIN_ORGUNIT_REQUEST: "JOIN_ORGUNIT_REQUEST" as NotificationTypeEnum,
      ACCESS_DATASET_REQUEST: "ACCESS_DATASET_REQUEST" as NotificationTypeEnum,
    };
    export const NotificationTypeEnumTranslate: KeyValue[] = [
      {
        key: NotificationTypeEnum.MY_APPROVED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myApprovedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.MY_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myCompletedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.MY_COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myCompletedArchiveInfo"),
      },
      {
        key: NotificationTypeEnum.MY_INDIRECT_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.myIndirectCompletedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.VALIDATE_DEPOSIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.validateDepositRequest"),
      },
      {
        key: NotificationTypeEnum.COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.completedArchiveInfo"),
      },
      {
        key: NotificationTypeEnum.IN_ERROR_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.inErrorDepositInfo"),
      },
      {
        key: NotificationTypeEnum.CREATE_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.createOrgUnitRequest"),
      },
      {
        key: NotificationTypeEnum.JOIN_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.joinOrgUnitRequest"),
      },
      {
        key: NotificationTypeEnum.ACCESS_DATASET_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.accessArchiveRequest"),
      },
    ];
    export const NotificationTypeShortEnumTranslate: KeyValue[] = [
      {
        key: NotificationTypeEnum.MY_APPROVED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myApprovedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.MY_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myCompletedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.MY_COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myCompletedArchiveInfo"),
      },
      {
        key: NotificationTypeEnum.MY_INDIRECT_COMPLETED_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.myIndirectCompletedDepositInfo"),
      },
      {
        key: NotificationTypeEnum.VALIDATE_DEPOSIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.validateDepositRequest"),
      },
      {
        key: NotificationTypeEnum.COMPLETED_ARCHIVE_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.completedArchiveInfo"),
      },
      {
        key: NotificationTypeEnum.IN_ERROR_DEPOSIT_INFO,
        value: MARK_AS_TRANSLATABLE("enum.notificationTypeShort.inErrorDepositInfo"),
      },
      {
        key: NotificationTypeEnum.CREATE_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.createOrgUnitRequest"),
      },
      {
        key: NotificationTypeEnum.JOIN_ORGUNIT_REQUEST,
        value: MARK_AS_TRANSLATABLE("enum.notificationType.joinOrgUnitRequest"),
      },
    ];
  }

  export namespace SubmissionAgreementType {
    export type SubmissionAgreementTypeEnum = SubmissionPolicyPartial.SubmissionAgreementTypeEnum;
    export const SubmissionAgreementTypeEnum = SubmissionPolicyPartial.SubmissionAgreementTypeEnum;

    export const SubmissionAgreementTypeEnumTranslate: KeyValueInfo[] = [
      {
        key: SubmissionAgreementTypeEnum.WITHOUT,
        value: MARK_AS_TRANSLATABLE("enum.submissionAgreementType.without"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.submissionAgreementTypeInfo.without"),
      },
      {
        key: SubmissionAgreementTypeEnum.ON_FIRST_DEPOSIT,
        value: MARK_AS_TRANSLATABLE("enum.submissionAgreementType.onFirstDeposit"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.submissionAgreementTypeInfo.onFirstDeposit"),
      },
      {
        key: SubmissionAgreementTypeEnum.FOR_EACH_DEPOSIT,
        value: MARK_AS_TRANSLATABLE("enum.submissionAgreementType.forEachDeposit"),
        infoToTranslate: MARK_AS_TRANSLATABLE("enum.submissionAgreementTypeInfo.forEachDeposit"),
      },
    ];
  }

  export type IdentifiersEnum =
    "CrossrefFunder"
    | "GRID"
    | "ISNI"
    | "ROR"
    | "SPDX"
    | "WEB"
    | "Wikidata";
  export const IdentifiersEnum = {
    CrossrefFunder: "CrossrefFunder" as IdentifiersEnum,
    GRID: "GRID" as IdentifiersEnum,
    ISNI: "ISNI" as IdentifiersEnum,
    ROR: "ROR" as IdentifiersEnum,
    SPDX: "SPDX" as IdentifiersEnum,
    WEB: "WEB" as IdentifiersEnum,
    Wikidata: "Wikidata" as IdentifiersEnum,
  };

  export type IdentifierTypeEnum =
    "RES_ID"
    | "ORCID"
    | "ROR_ID"
    | "SPDX_ID";
  export const IdentifierTypeEnum = {
    RES_ID: "RES_ID" as IdentifierTypeEnum,
    ORCID: "ORCID" as IdentifierTypeEnum,
    ROR_ID: "ROR_ID" as IdentifierTypeEnum,
    SPDX_ID: "SPDX_ID" as IdentifierTypeEnum,
  };
}
