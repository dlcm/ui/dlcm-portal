## Summary

(Summarize the new feature concisely)

## Use Case 

(Describe the details of the use case - main & secondary scenario)

## Actors

(Targeted users)

/title [FEATURE] a new feature
/promote_to_incident
/label ~feature ~new
